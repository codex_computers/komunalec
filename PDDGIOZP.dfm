object frmPDDGIOZP: TfrmPDDGIOZP
  Left = 0
  Top = 0
  Caption = #1050#1054#1052#1059#1053#1040#1051#1045#1062' ERK'
  ClientHeight = 164
  ClientWidth = 258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object AppBar1: TPanel
    Left = 0
    Top = 0
    Width = 258
    Height = 164
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object btn2: TcxButton
      Left = 48
      Top = 58
      Width = 161
      Height = 49
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' XML'
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btn2Click
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    Left = 80
    Top = 16
  end
  object XMLDocPDDGI: TXMLDocument
    Left = 176
    Top = 12
    DOMVendorDesc = 'MSXML'
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.CharSet = 'utf-8'
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.AcceptCharSet = 'utf-8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 16
    Top = 80
  end
end
