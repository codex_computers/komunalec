unit UnitPromena;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, StdCtrls, ComCtrls, ExtCtrls, Menus, cxLookAndFeelPainters,
  ActnList, cxButtons, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust, dxSkinSummer2008,
  dxSkinValentine, dxSkinXmas2008Blue, cxGraphics, cxLookAndFeels,
  dxSkinBlueprint, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinHighContrast, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSpringTime,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,iduri, IdBaseComponent,
  IdComponent, IdRawBase, IdRawClient, IdIcmpClient, FIBQuery, pFIBQuery,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, System.Actions,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;//,
 // dxSkinOffice2013White, System.Actions;

type
  TfrmPromena = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    cxLookUpComboBoxTipPartner: TcxTextEdit;
    cxTxtPartnerID: TcxTextEdit;
    cxTxtPartnerNaziv: TcxTextEdit;
    Label4: TLabel;
    txtCitKniga: TcxTextEdit;
    lblCitackaKniga: TLabel;
    lblRedenBrojOld: TLabel;
    cxTxtCitackaKniga: TcxTextEdit;
    cxTxtRedenBrojOld: TcxTextEdit;
    Label2: TLabel;
    cxtxtNaziv: TcxTextEdit;
    Label3: TLabel;
    Label5: TLabel;
    txtCitKnRb: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ActionList1: TActionList;
    aPromeni: TAction;
    aOtkazi: TAction;
    IdIcmpClient1: TIdIcmpClient;
    qCitackKniga: TpFIBQuery;
    procedure FormShow(Sender: TObject);
    procedure cxLookUpComboBoxTipPartnerExit(Sender: TObject);
    procedure cxTxtPartnerIDExit(Sender: TObject);
    procedure cxTxtPartnerNazivEnter(Sender: TObject);
    procedure setGridForLokacii(tip_partner:String;partner:String);
    procedure cxBtnPartneriClick(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure callFrmPartneri;
    procedure cxTxtPartnerNazivExit(Sender: TObject);
    procedure aPromeniExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    var citackaKniga, citackaKnigaRb ,naziv,lokacija:String;
  public
  constructor Create(Owner : TComponent; citackaKniga:String; citackaKnigaRb:String; lokacija:String; naziv:String);reintroduce; overload;
    { Public declarations }
  end;

var
  frmPromena: TfrmPromena;

implementation

uses Partner, dmKonekcija,
//dmLook,
dmMaticni, UnitDM1, UnitLokacii;

procedure TfrmPromena.aOtkaziExecute(Sender: TObject);
begin
    Close;
end;

procedure TfrmPromena.aPromeniExecute(Sender: TObject);
var citacka_kniga:string;
begin
   dm1.ProcPromCK.Close;
   DM1.ProcPromCK.ParamByName('id').AsString:=lokacija;
   DM1.ProcPromCK.ParamByName('id_pred').AsString:=DM1.TblKomLokaciiPoPartnerID.AsString;
  // DM1.ProcPromCitKn.ParamByName('operacija').AsString:='1';
   DM1.ProcPromCK.ExecProc;

   //�� ����� ��� � ������ ������� �����
   qCitackKniga.Close;
   qCitackKniga.ParamByName('lokacija').AsString := lokacija;
   qCitackKniga.ExecQuery;

   citacka_kniga := qCitackKniga.FldByName['citacka_kniga'].AsString;


    //edit vo tabelata KOM_PROMENA
 ///////////////////////////////////////
//       if st = dsEdit then
 //       begin
        //   if dm1.TblKomLokaciiAKTIVEN.Value = 1 then akt := '��' else akt := '��';
 if sos_int = '1' then   // od 1 vo 11,  duri da sredam , 01.08.2019
 begin
    ima_promena := '1';

//  if servis = '0' then
//    begin
//           CelLink:= TIdURI.URLEncode(link_edit+'&LOKACISKI_BROJ='+lokacija+'&CITACKA_KNIGA='+citacka_kniga);
//
//           dm1.tblPromena.Close;
//           DM1.tblPromena.Open;
//           dm1.tblPromena.Insert;
//           DM1.tblPromenaLINK.Value := CelLink;
//           DM1.tblPromena.Post;
//
//           //�� �� ������� ��� ���� �������� �����
//       //    if dm1.IsInternetConnected then
//           try
//        if not dm1.IsInternetConnected then
//        //   IdIcmpClient1.Ping ('www.google.com');
//
//           Except on E:Exception do
//     //      if not ima then
//            begin
//           //  ShowMessage('���� �������� ��������!');
////              DM1.TblKomLokacii.CloseOpen(true);
////              panelDole.Enabled := false;
////              enableGrid(true);
////              cxGrid1.SetFocus();
// //             Abort;
//            end
//
//
//           end;
//           begin
//             // da se prati linkot
//                 dm1.ZemiTextFile(cellink);
//             //    if (copy(Trim(vraten_text),2,1) = '1') then
//                 if pos('refreshed',vraten_text) > 0 then
//                 begin
//                   //update vo tabelata KOM_PROMENA
//                   dm1.tblPromena.Edit;
//                   dm1.tblPromenaPROMENA.Value := 1;
//                   dm1.tblPromena.Post;
//                 end;
//           end;
//       // end;
//
//
//
//   //     DM1.TblKomLokacii.CloseOpen(true);
//   //     panelDole.Enabled := false;
//   //     enableGrid(true);
//   //     cxGrid1.SetFocus();
//   // end;
//
//         ShowMessage('��������� � ���������!');
//         Close;
//  // FrmLokacija.cxControlCITACKA_KNIGA.EditValue:=txtCitKniga.Text;
//  // FrmLokacija.cxControlCITACKA_KNIGA_RB.EditValue:=txtCitKnRb.Text;
//  end;
  end;
end;

procedure TfrmPromena.callFrmPartneri;
var frmP:TfrmPartner;
begin
    //partnerSet := false;

    frmP :=TfrmPartner.Create(nil,false);

    //frmP.getEdit(TEdit(cxTxtPartnerTIP_PARTNER),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.getEdit(tedit(cxLookUpComboBoxTipPartner),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.ShowModal();
    frmP.Free();

    if frmP.ModalResult = mrOk then
    begin
        setGridForLokacii(VarToStr(cxLookUpComboBoxTipPartner.EditValue),cxTxtPartnerID.Text);
       // setUpLokationData();
    end;


end;

constructor TFrmPromena.Create(Owner : TComponent; citackaKniga:String; citackaKnigaRb:String; lokacija:String; naziv:String);
begin
   inherited Create(Owner);
    Self.CitackaKniga := citackaKniga;
    Self.citackaKnigaRb := citackaKnigaRb;

  //  Self.pred := pred;
    Self.naziv := naziv;
    Self.lokacija:=lokacija;

   // Self.redenBrojNew := citackaKnigaRbNew;
end;

{$R *.dfm}

procedure TfrmPromena.cxBtnPartneriClick(Sender: TObject);
begin
   callFrmPartneri();
end;

procedure TfrmPromena.cxLookUpComboBoxTipPartnerExit(Sender: TObject);
begin
if cxLookUpComboBoxTipPartner.Text<>'' then
       dm1.tblPartner.Filter:='TIP_PARTNER='+QuotedStr(cxLookUpComboBoxTipPartner.Text);
end;

procedure TfrmPromena.cxTxtPartnerIDExit(Sender: TObject);
begin
  if (cxTxtPartnerID.Text<>'') and (cxLookUpComboBoxTipPartner.Text<>'') then
  begin
     dm1.tblPartner.Filter:='ID='+QuotedStr(cxTxtPartnerID.Text);
     dm1.tblPartner.Filtered:=True;
  end
  else if cxLookUpComboBoxTipPartner.Text<>'' then
        dm1.tblPartner.Filtered:=True;
end;

procedure TfrmPromena.cxTxtPartnerNazivEnter(Sender: TObject);
begin
     dm1.tblPartner.ParamByName('tp').AsString:='%';
     dm1.tblPartner.open;
//  if dm1.tblPartner.Create=1 then
   if dm1.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([cxLookUpComboBoxTipPartner.Text,cxTxtPartnerID.text]),[]) then
    begin
      setGridForLokacii(dm1.tblPartnerTIP_PARTNER.AsString,dm1.tblPartnerID.AsString);
     // cxDateDatumOd.SetFocus;
    end
    else
    begin
        cxBtnPartneriClick(Sender);
    end;

end;

procedure TfrmPromena.cxTxtPartnerNazivExit(Sender: TObject);
begin
  if not dm1.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([cxLookUpComboBoxTipPartner.Text,cxTxtPartnerID.text]),[]) then
      cxBtnPartneriClick(Sender)
  else cxTxtPartnerNaziv.Text:=dm1.tblPartnerNAZIV.Value;
end;

procedure TfrmPromena.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case key of
        VK_RETURN:

            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
       // end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;

end;

procedure TfrmPromena.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm1.tblPartner.Close;
  dm1.tblPartner.Filtered:=false;
end;

procedure TfrmPromena.FormShow(Sender: TObject);
begin
  cxTxtCitackaKniga.Text:=citackaKniga;
  cxTxtRedenBrojOld.Text:=citackaKnigaRb;
  cxtxtNaziv.Text:=naziv;
end;

procedure TfrmPromena.setGridForLokacii(tip_partner, partner: String);
begin
    DM1.TblKomLokaciiPoPartner.Close;
    DM1.TblKomLokaciiPoPartner.ParamByName('TIP_PARTNER').AsString := tip_partner;
    DM1.TblKomLokaciiPoPartner.ParamByName('PARTNER').AsString := partner;

    DM1.TblKomLokaciiPoPartner.Open;
    txtCitKniga.Text:=DM1.TblKomLokaciiPoPartnerCITACKA_KNIGA.AsString;
    txtCitKnRb.Text:=DM1.TblKomLokaciiPoPartnerCITACKA_KNIGA_RB.AsString;
end;

end.
