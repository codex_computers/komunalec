object frmPeriod: TfrmPeriod
  Left = 0
  Top = 0
  Caption = #1055#1077#1088#1080#1086#1076
  ClientHeight = 218
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 386
    Height = 218
    Align = alClient
    TabOrder = 0
    object cxGroupBox1: TcxGroupBox
      Left = 64
      Top = 65
      Caption = ' '#1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1077#1088#1080#1086#1076' '
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Height = 89
      Width = 275
      object txtOdDatum: TcxDateEdit
        Left = 101
        Top = 22
        Hint = #1054#1076' '#1076#1072#1090#1091#1084' '
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 145
      end
      object txtDoDatum: TcxDateEdit
        Left = 101
        Top = 49
        Hint = #1044#1086' '#1076#1072#1090#1091#1084
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 145
      end
      object cxLabel1: TcxLabel
        Left = 66
        Top = 22
        Caption = #1054#1076
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.TextColor = clRed
        Style.IsFontAssigned = True
      end
      object cxLabel2: TcxLabel
        Left = 66
        Top = 45
        Caption = #1044#1086
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.TextColor = clRed
        Style.IsFontAssigned = True
      end
    end
    object cxButton1: TcxButton
      Left = 165
      Top = 176
      Width = 75
      Height = 25
      Action = aPecati
      LookAndFeel.NativeStyle = True
      TabOrder = 2
    end
    object cxButton2: TcxButton
      Left = 264
      Top = 176
      Width = 75
      Height = 25
      Action = aIzlez
      LookAndFeel.NativeStyle = True
      TabOrder = 3
    end
    object cxLabel3: TcxLabel
      Left = 69
      Top = 34
      AutoSize = False
      Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      Height = 17
      Width = 90
    end
    object txtTp: TcxImageComboBox
      Left = 165
      Top = 33
      Hint = #1040#1082#1086' '#1085#1077' '#1080#1079#1073#1077#1088#1077#1090#1077' '#1090#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088', '#1074#1072#1078#1080' '#1079#1072' '#1089#1080#1090#1077' '#1087#1072#1088#1090#1085#1077#1088#1080
      Properties.Items = <
        item
          ImageIndex = 0
        end
        item
          Description = #1060#1080#1079#1080#1095#1082#1080' '#1083#1080#1094#1072
          Tag = 5
          Value = 5
        end
        item
          Description = #1044#1091#1116#1072#1085#1080
          Tag = 6
          Value = 6
        end>
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 145
    end
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
      end>
    Left = 320
    Top = 40
    StyleName = 'Platform Default'
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      OnExecute = aPecatiExecute
    end
    object aDizajner: TAction
      Caption = 'aDizajner'
      ShortCut = 24697
      OnExecute = aDizajnerExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
  end
end
