unit UnitNovaVrednost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxLabel, cxTextEdit, ExtCtrls, StdCtrls, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TfrmNovaVrednost = class(TForm)
    Panel5: TPanel;
    cxLabel21: TcxLabel;
    txtNovaVrednost: TcxTextEdit;
    procedure txtPassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure txtNovaVrednostKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovaVrednost: TfrmNovaVrednost;

implementation

uses dmKonekcija, UnitDM1;

{$R *.dfm}

procedure TfrmNovaVrednost.FormShow(Sender: TObject);
begin
    txtNovaVrednost.SetFocus;
end;

procedure TfrmNovaVrednost.txtNovaVrednostKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if key=VK_RETURN then
    begin
        if txtNovaVrednost.Text <> '' then
    begin
        close;
    end;

    end
    else

    if key=VK_ESCAPE then
       Close;
end;

procedure TfrmNovaVrednost.txtPassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key=VK_RETURN then
    begin
//         dmkon.tblUseri.Open;
//         if(dmKon.tblUseri.Locate('PASS',VarArrayOf([txtPass.text]),[])) then
//           begin
//             if(dmKon.qSysUser.Locate('USERNAME',VarArrayOf([dmKon.tblUseriUSERNAME.AsString]),[])) then
//                begin
//                     dm.tblKelner.Close;
//                     dm.tblKelner.ParamByName('s').AsInteger:=Tag;
//                     dm.tblKelner.open;
//                     if dm.tblKelner.Locate('TIP_PARTNER;ID',VarArrayOf([dmKon.qSysUserTIP_PARTNER.AsString,dmKon.qSysUserPARTNER.AsString]),[]) then
//                     begin
//                        dm.tip_kelner:=dmKon.qSysUserTIP_PARTNER.Value;
//                        dm.kelner:=dmKon.qSysUserPARTNER.Value;
//                        dm.pass:=True;
//                     end
//                     else
//                     begin
//                       ShowMessage('���������� �� � ������ ���� ������!');
//                       dm.pass:=false;
//                     end;
//                     Close;
//                end;
//           end
//                else
//                begin
//                   ShowMessage('������ PASSWORD!');
//                   dm.pass:=false;
//                   txtPass.Text:='';
//                   Abort;
//                end;

    if txtNovaVrednost.Text <> '' then
    begin
        close;
    end;

    end
    else

    if key=VK_ESCAPE then
       Close;
end;

end.
