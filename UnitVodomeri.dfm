inherited FrmVodomeri: TFrmVodomeri
  Caption = #1042#1086#1076#1086#1084#1077#1088#1080' '#1087#1086' '#1083#1086#1082#1072#1094#1080#1112#1072
  ClientHeight = 686
  ClientWidth = 738
  ExplicitWidth = 754
  ExplicitHeight = 725
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 738
    Height = 323
    ExplicitWidth = 738
    ExplicitHeight = 323
    inherited cxGrid1: TcxGrid
      Width = 734
      Height = 319
      Font.Height = -13
      ExplicitWidth = 734
      ExplicitHeight = 319
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.DSKomVodomeri
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'LOKACIIN_ID'
          HeaderAlignmentHorz = taCenter
          Width = 83
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          HeaderAlignmentHorz = taCenter
          Width = 120
        end
        object cxGrid1DBTableView1VODOMERID: TcxGridDBColumn
          Caption = #1041#1088'.'#1074#1086#1076#1086#1084#1077#1088
          DataBinding.FieldName = 'VODOMERID'
          HeaderAlignmentHorz = taCenter
          Width = 134
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'STATUS'
          HeaderAlignmentHorz = taCenter
          Width = 79
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1055#1072#1091#1096#1072#1083
          DataBinding.FieldName = 'PAUSAL'
          HeaderAlignmentHorz = taCenter
          Width = 76
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1086#1076
          DataBinding.FieldName = 'DATUM_OD'
          HeaderAlignmentHorz = taCenter
          Width = 105
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          Caption = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072
          DataBinding.FieldName = 'DATUM_DO'
          HeaderAlignmentHorz = taCenter
          Width = 123
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 449
    Width = 738
    Height = 214
    ExplicitTop = 449
    ExplicitWidth = 738
    ExplicitHeight = 214
    inherited Label1: TLabel
      Left = 265
      Top = 35
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = #1064#1080#1092#1088#1072
      Font.Height = -13
      Visible = False
      ExplicitLeft = 265
      ExplicitTop = 35
      ExplicitWidth = 47
      ExplicitHeight = 16
    end
    object lblStatus: TLabel [1]
      Left = 262
      Top = 86
      Width = 50
      Height = 20
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1090#1072#1090#1091#1089
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPartnerId: TLabel [2]
      Left = 22
      Top = 35
      Width = 49
      Height = 16
      Caption = #1055#1072#1088#1090#1085#1077#1088
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblNazivPartner: TLabel [3]
      Left = 91
      Top = 35
      Width = 49
      Height = 16
      Caption = #1055#1072#1088#1090#1085#1077#1088
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblPartner: TLabel [4]
      Left = 6
      Top = 16
      Width = 49
      Height = 16
      Caption = #1055#1072#1088#1090#1085#1077#1088
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblSifraLokacija: TLabel [5]
      Left = 6
      Top = 67
      Width = 99
      Height = 16
      Caption = #1064#1080#1092#1088#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblPodatociKniga: TLabel [6]
      Left = 6
      Top = 106
      Width = 122
      Height = 16
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1082#1085#1080#1075#1072#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblSifraLokacija1: TLabel [7]
      Left = 30
      Top = 86
      Width = 99
      Height = 16
      Caption = #1064#1080#1092#1088#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCitackaKniga: TLabel [8]
      Left = 30
      Top = 125
      Width = 84
      Height = 16
      Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblCitackaKnigaRb: TLabel [9]
      Left = 117
      Top = 125
      Width = 106
      Height = 16
      Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072' '#1088#1073'.'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblTipStatus: TLabel [10]
      Left = 470
      Top = 84
      Width = 158
      Height = 16
      Caption = '1 - '#1040#1082#1090#1080#1074#1077#1085', 0 - '#1053#1077#1072#1082#1090#1080#1074#1077#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblPausal: TLabel [11]
      Left = 246
      Top = 112
      Width = 66
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1072#1091#1096#1072#1083
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblTipPausal: TLabel [12]
      Left = 470
      Top = 108
      Width = 75
      Height = 16
      Caption = '1 -'#1044#1072', 0 - '#1053#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [13]
      Left = 246
      Top = 136
      Width = 66
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1086#1076
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [14]
      Left = 246
      Top = 160
      Width = 66
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1088#1086#1084#1077#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [15]
      Left = 200
      Top = 60
      Width = 112
      Height = 20
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 339
      Top = 32
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.DSKomVodomeri
      Style.Font.Height = -13
      Style.IsFontAssigned = True
      TabOrder = 7
      Visible = False
      ExplicitLeft = 339
      ExplicitTop = 32
      ExplicitWidth = 106
      ExplicitHeight = 24
      Width = 106
    end
    inherited OtkaziButton: TcxButton
      Left = 602
      Top = 173
      TabOrder = 6
      Font.Height = -13
      ParentFont = False
      ExplicitLeft = 602
      ExplicitTop = 173
    end
    inherited ZapisiButton: TcxButton
      Left = 521
      Top = 173
      TabOrder = 5
      Font.Height = -13
      ParentFont = False
      ExplicitLeft = 521
      ExplicitTop = 173
    end
    object cxComboBoxPAUSAL: TcxDBComboBox
      Tag = 1
      Left = 339
      Top = 106
      BeepOnEnter = False
      DataBinding.DataField = 'PAUSAL'
      DataBinding.DataSource = DM1.DSKomVodomeri
      ParentFont = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        '0'
        '1')
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 106
    end
    object cxComboBoxSTATUS: TcxDBComboBox
      Tag = 1
      Left = 339
      Top = 81
      BeepOnEnter = False
      DataBinding.DataField = 'STATUS'
      DataBinding.DataSource = DM1.DSKomVodomeri
      ParentFont = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        '0'
        '1')
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 106
    end
    object txtDatum_od: TcxDBDateEdit
      Left = 339
      Top = 131
      DataBinding.DataField = 'DATUM_OD'
      DataBinding.DataSource = DM1.DSKomVodomeri
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 106
    end
    object txtDatum_do: TcxDBDateEdit
      Left = 340
      Top = 156
      DataBinding.DataField = 'DATUM_DO'
      DataBinding.DataSource = DM1.DSKomVodomeri
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 106
    end
    object txtBrVod: TcxDBTextEdit
      Left = 339
      Top = 56
      DataBinding.DataField = 'VODOMERID'
      DataBinding.DataSource = DM1.DSKomVodomeri
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 106
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 738
    ExplicitWidth = 738
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 663
    Width = 738
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080 +
          ' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 663
    ExplicitWidth = 738
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 136
    Top = 40
  end
  inherited PopupMenu1: TPopupMenu
    Left = 176
    Top = 40
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 216
    Top = 40
    inherited aBrisi: TAction
      ShortCut = 0
      Visible = False
    end
    object Action1: TAction
      Caption = 'aNov'
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.442388634270000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object qZemiID: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select max(v.id) id'
      'from kom_vodomeri v'
      'where v.lokaciin_id = :lokacija')
    Left = 336
    Top = 40
    qoStartTransaction = True
  end
  object IdIcmpClient1: TIdIcmpClient
    Host = 'www.google.com'
    Protocol = 1
    ProtocolIPv6 = 0
    IPVersion = Id_IPv4
    PacketSize = 1024
    Left = 448
    Top = 216
  end
end
