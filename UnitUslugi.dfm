inherited FrmUslugi: TFrmUslugi
  Caption = #1059#1089#1083#1091#1075#1080' '#1079#1072' '#1085#1072#1087#1083#1072#1090#1072
  ClientHeight = 599
  ClientWidth = 699
  ExplicitWidth = 715
  ExplicitHeight = 637
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 6
    Top = 79
    Width = 50
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    BiDiMode = bdRightToLeft
    Caption = #1053#1072#1079#1080#1074
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
  end
  inherited lPanel: TPanel
    Width = 699
    Height = 218
    ExplicitWidth = 699
    ExplicitHeight = 218
    inherited cxGrid1: TcxGrid
      Width = 695
      Height = 214
      ExplicitWidth = 695
      ExplicitHeight = 214
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.DSKomUslugi
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1091#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'VID_USLUGA'
          HeaderAlignmentHorz = taCenter
          Width = 65
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          HeaderAlignmentHorz = taCenter
          Width = 90
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
        end
        object cxGrid1DBTableView1KFL: TcxGridDBColumn
          DataBinding.FieldName = 'KFL'
        end
        object cxGrid1DBTableView1KD: TcxGridDBColumn
          DataBinding.FieldName = 'KD'
        end
        object cxGrid1DBTableView1KPL: TcxGridDBColumn
          DataBinding.FieldName = 'KPL'
        end
        object cxGrid1DBTableView1KU: TcxGridDBColumn
          DataBinding.FieldName = 'KU'
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 344
    Width = 699
    Height = 232
    ExplicitTop = 344
    ExplicitWidth = 699
    ExplicitHeight = 232
    inherited Label1: TLabel
      Left = 6
      Top = 29
      Width = 43
      BiDiMode = bdRightToLeft
      Caption = #1064#1080#1092#1088#1072' '
      ParentBiDiMode = False
      ExplicitLeft = 6
      ExplicitTop = 29
      ExplicitWidth = 43
    end
    object lblVidUsluga: TLabel [1]
      Left = 7
      Top = 54
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1042#1080#1076' '#1085#1072' '#1091#1089#1083#1091#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object lblNaziv: TLabel [2]
      Left = 6
      Top = 79
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1053#1072#1079#1080#1074
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object Label7: TLabel [3]
      Left = 6
      Top = 104
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1050#1060#1051
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object Konro: TLabel [4]
      Left = 6
      Top = 129
      Width = 38
      Height = 15
      AutoSize = False
      Caption = #1050#1044
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [5]
      Left = 6
      Top = 153
      Width = 44
      Height = 15
      AutoSize = False
      Caption = #1050#1055#1051
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [6]
      Left = 6
      Top = 177
      Width = 40
      Height = 15
      AutoSize = False
      Caption = #1050#1059
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [7]
      Left = 6
      Top = 201
      Width = 41
      Height = 15
      AutoSize = False
      Caption = #1056#1045
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 94
      Top = 26
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.DSKomUslugi
      ExplicitLeft = 94
      ExplicitTop = 26
      ExplicitWidth = 85
      Width = 85
    end
    inherited OtkaziButton: TcxButton
      Left = 601
      Top = 192
      TabOrder = 15
      ExplicitLeft = 601
      ExplicitTop = 192
    end
    inherited ZapisiButton: TcxButton
      Left = 520
      Top = 192
      TabOrder = 14
      ExplicitLeft = 520
      ExplicitTop = 192
    end
    object cxLookUpVID_USLUGA: TcxDBLookupComboBox
      Left = 150
      Top = 51
      DataBinding.DataField = 'VID_USLUGA'
      DataBinding.DataSource = DM1.DSKomUslugi
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1080#1074
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.DSKomVidoviUslugi
      TabOrder = 2
      OnKeyDown = OnKeyDownAllLookUp
      Width = 309
    end
    object cxControlVID_USLUGA: TcxDBTextEdit
      Tag = 1
      Left = 94
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'VID_USLUGA'
      DataBinding.DataSource = DM1.DSKomUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cxControlNAZIV: TcxDBTextEdit
      Left = 94
      Top = 76
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = DM1.DSKomUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 365
    end
    object txtKFL: TcxDBTextEdit
      Left = 94
      Top = 101
      DataBinding.DataField = 'KFL'
      DataBinding.DataSource = DM1.DSKomUslugi
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cbKFL: TcxDBLookupComboBox
      Left = 150
      Top = 101
      DataBinding.DataField = 'KFL'
      DataBinding.DataSource = DM1.DSKomUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'SIFRA'
      Properties.ListColumns = <
        item
          FieldName = 'SIFRA'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.dsKontenPlan
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 309
    end
    object txtKD: TcxDBTextEdit
      Left = 94
      Top = 125
      DataBinding.DataField = 'KD'
      DataBinding.DataSource = DM1.DSKomUslugi
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cbKD: TcxDBLookupComboBox
      Left = 150
      Top = 125
      DataBinding.DataField = 'KD'
      DataBinding.DataSource = DM1.DSKomUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'SIFRA'
      Properties.ListColumns = <
        item
          FieldName = 'SIFRA'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.dsKontenPlan
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 309
    end
    object txtKPL: TcxDBTextEdit
      Left = 94
      Top = 149
      DataBinding.DataField = 'KPL'
      DataBinding.DataSource = DM1.DSKomUslugi
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cbKPL: TcxDBLookupComboBox
      Left = 150
      Top = 149
      DataBinding.DataField = 'KPL'
      DataBinding.DataSource = DM1.DSKomUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'SIFRA'
      Properties.ListColumns = <
        item
          FieldName = 'SIFRA'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.dsKontenPlan
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 309
    end
    object txtKU: TcxDBTextEdit
      Left = 94
      Top = 173
      DataBinding.DataField = 'KU'
      DataBinding.DataSource = DM1.DSKomUslugi
      TabOrder = 10
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cbKU: TcxDBLookupComboBox
      Left = 150
      Top = 173
      DataBinding.DataField = 'KU'
      DataBinding.DataSource = DM1.DSKomUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'SIFRA'
      Properties.ListColumns = <
        item
          FieldName = 'SIFRA'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.dsKontenPlan
      TabOrder = 11
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 309
    end
    object txtRE: TcxDBTextEdit
      Left = 94
      Top = 197
      DataBinding.DataField = 'RE'
      DataBinding.DataSource = DM1.DSKomUslugi
      TabOrder = 12
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cbRE: TcxDBLookupComboBox
      Left = 150
      Top = 197
      DataBinding.DataField = 'RE'
      DataBinding.DataSource = DM1.DSKomUslugi
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsRE
      TabOrder = 13
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 309
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 699
    ExplicitWidth = 699
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 576
    Width = 699
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080',F9 - '#1047#1072#1087#1080#1096#1080', Es' +
          'c - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
    ExplicitTop = 576
    ExplicitWidth = 699
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 168
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 222
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 114
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.442301655090000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
