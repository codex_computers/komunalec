unit UnitUplati;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxBarPainter, cxClasses,
  dxBar, ComCtrls, ExtCtrls, ActnList, StdCtrls, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxDBEdit,DB, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxRadioGroup, cxBarEditItem, dxBarExtItems, cxGraphics, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, Menus, cxLookAndFeelPainters, cxButtons,
  dxSkinscxPCPainter, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxBarExtDBItems, FIBQuery,
  pFIBQuery, FIBDataSet, pFIBDataSet, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels,
  cxGroupBox, cxCheckBox, cxGridCustomPopupMenu, cxGridPopupMenu, cxLabel,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSpringTime, utils, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,FmtBcd, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  cxNavigator, dxCore, cxDateUtils, System.Actions, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, System.DateUtils, pFIBStoredProc;

type
  TFrmUplati = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    dxBarSubItem1: TdxBarSubItem;
    dxBarCombo1: TdxBarCombo;
    dxBarImageCombo1: TdxBarImageCombo;
    dxBarLookupCombo1: TdxBarLookupCombo;
    qPresmetka: TpFIBDataSet;
    dsPresmetka: TDataSource;
    qPresmetkaLOKACIIN_ID: TFIBIntegerField;
    qPresmetkaMESEC: TFIBIntegerField;
    qPresmetkaGODINA: TFIBIntegerField;
    qPresmetkaBROJ_FAKTURA: TFIBStringField;
    aBrisi: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    lblBrojFaktura: TLabel;
    cxTxtBrojFaktura: TcxTextEdit;
    Panel2: TPanel;
    lblMesec: TLabel;
    lblGodina: TLabel;
    lblPartner: TLabel;
    lblLokacija: TLabel;
    lblIznosPresmetka: TLabel;
    lblPlateno: TLabel;
    lblRazlika: TLabel;
    cxTxtMesec: TcxTextEdit;
    cxTxtGodina: TcxTextEdit;
    cxTxtNazivPartner: TcxTextEdit;
    cxTxtLokacija: TcxTextEdit;
    cxTxtIznosPresmetka: TcxTextEdit;
    panelPredhodnoVnesenaUplata: TPanel;
    lblPoslednoVnesenaUplata: TLabel;
    lblMesecPos: TLabel;
    lblGodinaPos: TLabel;
    lblIznosPos: TLabel;
    lblPartnerPos: TLabel;
    lblLokacijaPos: TLabel;
    lblIznosPlatenoPos: TLabel;
    cxTxtMesecPos: TcxTextEdit;
    cxTxtGodinaPos: TcxTextEdit;
    cxTxtIznosPresmetkaPos: TcxTextEdit;
    cxTxtNazivPartnerPos: TcxTextEdit;
    cxTxtLokacijaPos: TcxTextEdit;
    cxTxtIznosPlatenoPos: TcxTextEdit;
    cxTxtPlateno: TcxTextEdit;
    cxTxtRazlika: TcxTextEdit;
    TabSheet2: TTabSheet;
    panelGore: TPanel;
    panelPartner: TPanel;
    Label2: TLabel;
    cxLookUpComboBoxTipPartner: TcxLookupComboBox;
    cxTxtPartnerID: TcxTextEdit;
    cxBtnPartneri: TcxButton;
    cxTxtPartnerNaziv: TcxTextEdit;
    Panel3: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1BROJ_FAKTURA: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1MESEC: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOS_VKUPNO: TcxGridDBColumn;
    cxGrid2DBTableView1PLATENO: TcxGridDBColumn;
    cxGrid2DBTableView1RAZLIKA: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    cxTxtVkOdbereni: TcxTextEdit;
    cxTxtVkZadolzuvanje: TcxTextEdit;
    cxDateDatumUplata: TcxDateEdit;
    cxTxtZaVracanje: TcxTextEdit;
    TabSheet3: TTabSheet;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1SIFRA_PART: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1BROJ_FAKTURA: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM_UPLATA: TcxGridDBColumn;
    cxGrid3DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid3DBTableView1LOKACIJA: TcxGridDBColumn;
    cxGrid3DBTableView1ID_TU: TcxGridDBColumn;
    cxGrid3DBTableView1TIP_UPLATA: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    StatusBar2: TStatusBar;
    cxControlDATUM_UPLATA: TdxBarDateCombo;
    qBrojFaktura: TpFIBQuery;
    tblPoslednoZadolzuvanje: TpFIBDataSet;
    dsPoslednoZadolzuvanje: TDataSource;
    tblPoslednoZadolzuvanjeBROJ_FAKTURA: TFIBStringField;
    tblPoslednoZadolzuvanjeDATUM_PRESMETKA: TFIBDateTimeField;
    tblPoslednoZadolzuvanjeGODINA: TFIBIntegerField;
    tblPoslednoZadolzuvanjeMESEC: TFIBIntegerField;
    tblPoslednoZadolzuvanjeIZNOS_VKUPNO: TFIBIntegerField;
    tblPoslednoZadolzuvanjeNAZIV: TFIBStringField;
    tblPoslednoZadolzuvanjePLATENO: TFIBBCDField;
    tblPoslednoZadolzuvanjeRAZLIKA: TFIBBCDField;
    dsUplata: TDataSource;
    tblUplata: TpFIBDataSet;
    tblUplataBROJ_FAKTURA: TFIBStringField;
    tblUplataDATUM_PRESMETKA: TFIBDateTimeField;
    tblUplataGODINA: TFIBIntegerField;
    tblUplataMESEC: TFIBIntegerField;
    tblUplataIZNOS_VKUPNO: TFIBIntegerField;
    tblUplataNAZIV: TFIBStringField;
    tblUplataPLATENO: TFIBBCDField;
    tblUplataRAZLIKA: TFIBBCDField;
    panelControls: TPanel;
    lblIznosPlateno: TLabel;
    cxControlIZNOS: TcxDBTextEdit;
    qPresmetkaID: TFIBIntegerField;
    tblUplataID: TFIBIntegerField;
    dxBarLookupCombo3: TdxBarLookupCombo;
    rgTipPartner: TcxRadioGroup;
    cxGrid2DBTableViewIzberi: TcxGridDBColumn;
    cxMaskPlateno: TcxTextEdit;
    btZapisi: TcxButton;
    aNeplateni: TAction;
    aZapisi: TAction;
    aOznaciSite: TAction;
    cbSite: TcxCheckBox;
    txtDok: TcxTextEdit;
    Label4: TLabel;
    txtSaldo: TcxTextEdit;
    tblUplataSaldo: TpFIBDataSet;
    dsUplataSaldo: TDataSource;
    tblUplataSaldoID: TFIBIntegerField;
    tblUplataSaldoIZNOS: TFIBBCDField;
    tblUplataSaldoTIP_PARTNER: TFIBIntegerField;
    tblUplataSaldoPARTNER: TFIBIntegerField;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    tblUplataSaldoTIP: TFIBSmallIntField;
    tblUplataSaldoDOKUMENT: TFIBStringField;
    qDaliImaSaldo: TpFIBQuery;
    tblUplataSaldoDATUM_UPLATA: TFIBDateField;
    dsBrisiUplataSaldo: TDataSource;
    tblBrisiUplataSaldo: TpFIBDataSet;
    tblBrisiUplataSaldoID: TFIBIntegerField;
    tblBrisiUplataSaldoTIP_NALOG: TFIBStringField;
    tblBrisiUplataSaldoIZNOS: TFIBBCDField;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1DOKUMENT: TcxGridDBColumn;
    cxGrid3DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid3DBTableView1PARTNER_ID: TcxGridDBColumn;
    ptBrisiAvansi: TTabSheet;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1ID: TcxGridDBColumn;
    cxGrid4DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid4DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid4DBTableView1NAZIV_PARTNER: TcxGridDBColumn;
    cxGrid4DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid4DBTableView1TIP: TcxGridDBColumn;
    cxGrid4DBTableView1DOKUMENT: TcxGridDBColumn;
    cxGrid4DBTableView1DATUM_UPLATA: TcxGridDBColumn;
    cxGrid4DBTableView1TIP_NALOG: TcxGridDBColumn;
    cxGrid4DBTableView1NALOG: TcxGridDBColumn;
    cxGrid4DBTableView1ID_SALDO: TcxGridDBColumn;
    cxGridPopupMenu3: TcxGridPopupMenu;
    StatusBar3: TStatusBar;
    aBrisiSaldo: TAction;
    cxGrid4DBTableView1NAZIV_TIP_UPLATA: TcxGridDBColumn;
    lblDatumUplata: TcxLabel;
    lblVkupnoPlateno: TcxLabel;
    cxLabel2: TcxLabel;
    lblVkupno: TcxLabel;
    lblZaVracanjePoslednaUplata: TcxLabel;
    cxLabel1: TcxLabel;
    cxGrid3DBTableView1TIP_NALOG: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_PARTNER: TcxGridDBColumn;
    qBrisiUpaltaFirmi: TpFIBQuery;
    cxGrid4DBTableView1RE_FNG: TcxGridDBColumn;
    cxGrid4DBTableView1GODINA_FNG: TcxGridDBColumn;
    cxGrid4DBTableView1TIP_NALOG_FNG: TcxGridDBColumn;
    cxGrid4DBTableView1NALOG_FNG: TcxGridDBColumn;
    Label1: TLabel;
    cxLookUpComboBoxTipPartnerLok: TcxLookupComboBox;
    cxTxtPartnerIDLok: TcxTextEdit;
    tblPoslednoZadolzuvanjeTIP_PARTNER: TFIBIntegerField;
    tblPoslednoZadolzuvanjePARTNER: TFIBIntegerField;
    dxBarLookupComboNalog: TdxBarLookupCombo;
    tblNalog: TpFIBDataSet;
    dsNalog: TDataSource;
    tblNalogID: TFIBBCDField;
    tblNalogRE: TFIBIntegerField;
    tblNalogGODINA: TFIBIntegerField;
    tblNalogDATUM: TFIBDateField;
    tblNalogOPIS: TFIBStringField;
    tblNalogPROKNIZEN: TFIBSmallIntField;
    tblNalogOTVOREN: TFIBSmallIntField;
    tblNalogVID_NALOG: TFIBSmallIntField;
    tblNalogTS_INS: TFIBDateTimeField;
    tblNalogTS_UPD: TFIBDateTimeField;
    tblNalogUSR_INS: TFIBStringField;
    tblNalogUSR_UPD: TFIBStringField;
    cxGrid2DBTableView1LOKACIJA: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    tblNalogOPIS_NALOG: TFIBStringField;
    tblNalogTIP_NALOG: TFIBStringField;
    tblNalogNALOG: TFIBIntegerField;
    aRaskniziUplata: TAction;
    qUpdateUplata: TpFIBQuery;
    cxGrid3DBTableView1ID_FNG: TcxGridDBColumn;
    cxGrid2DBTableView1KONTO: TcxGridDBColumn;
    qKlucUplata: TpFIBQuery;
    cxGrid3DBTableView1O_REF_ID: TcxGridDBColumn;
    cxGrid3DBTableView1O_REF_ID2: TcxGridDBColumn;
    cxGrid2DBTableView1O_REF_ID: TcxGridDBColumn;
    cxGrid2DBTableView1O_REF_ID2: TcxGridDBColumn;
    cxGrid3DBTableView1ID_TEMP: TcxGridDBColumn;
    cxGrid2DBTableView1TUZBA: TcxGridDBColumn;
    cxGrid2DBTableView1DOGOVOR: TcxGridDBColumn;
    pBelFin: TpFIBStoredProc;
    cxGrid3DBTableView1NALOG: TcxGridDBColumn;
    qProveriNalog: TpFIBQuery;
    dxBarLookupCombo2: TdxBarLookupCombo;
    txtGodina: TcxBarEditItem;
    procedure aIzlezExecute(Sender: TObject);
    procedure onEnterAll(Sender: TObject);
    procedure onExitAll(Sender: TObject);
    procedure onKeyDownAll(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure getPresmetkaPoBrojUplata(broj_uplata:String);
    procedure FormShow(Sender: TObject);
    procedure enableCtrlPresmetkaData(enable:Boolean);
    procedure setPodatociPresmetkaText(broj_faktura:String;mesec:String;godina:String;iznos_presmetka:String;naziv:String;lokacija:String;iznos_plateno:String;razlika:String;tip_partner:string;partner:string);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure zapisiUplata(mesec:String;godina:String;lokacija:String;iznos_presmetka:String;presmetka:string);
    procedure PageControl1Change(Sender: TObject);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure callFrmPartneri();
    procedure getPartner();
    procedure cxBtnPartneriClick(Sender: TObject);
    procedure setGridForLokacii(tip_partner:String;partner:String);
    procedure setUpLokationData();
    procedure clearDataPartner();
    procedure setUpNeplateniZadolzuvanjaData(tip_partner:String;partner:String;lokacija:String);
    procedure cxGrid2DBTableView1SelectionChanged(
      Sender: TcxCustomGridTableView);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rasporediAndInsertMultipleUplati(vk_iznos:double);
    procedure popolniPoslednaUplataData(mesec:String;godina:String;iznos:String;partner:String;lokacija:String;iznos_plateno:String;datum:String);
    procedure aBrisiExecute(Sender: TObject);
    procedure cxTxtBrojFakturaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure cxControlDATUM_UPLATAExit(Sender: TObject);
    procedure cxControlDATUM_UPLATAChange(Sender: TObject);
    procedure cxGrid2DBTableView1Column1PropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGrid2DBTableViewIzberiPropertiesChange(Sender: TObject);
    procedure aNeplateniExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxDateDatumUplataExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aOznaciSiteExecute(Sender: TObject);
    procedure site(kako:Boolean);
    procedure cxCheckBox1Properties(Sender: TObject);
    procedure aBrisiSaldoExecute(Sender: TObject);
    procedure cxControlIZNOSPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxBtnPartneriClickLok(Sender: TObject);
    procedure cxLookUpComboBoxTipPartnerLokEnter(Sender: TObject);
    procedure aRaskniziUplataExecute(Sender: TObject);
    //procedure closeDataSetIfActive(dataSet:TDataSet);
  private
    { Private declarations }
  public
  last_datum_uplata:String;
    { Public declarations }
  vk,i,id_presmetka : Integer;
    suma_zadolzuvanje,VkZadolzuvanje,Plateno,ZaVracanje,saldo:double;
  end;

var
  FrmUplati: TFrmUplati;

implementation

uses UnitDM1, DaNe, dmKonekcija, Partner, UnitIzberiFaktura, dmMaticni, IzberiLokacija;

{$R *.dfm}

procedure TFrmUplati.aBrisiExecute(Sender: TObject);
begin
//    if dm1.tblBrisiUplatiTIP_NALOG.Value='' then
  //   begin
  //proveri dali nalogot e proknizan
    qProveriNalog.Close;
    qProveriNalog.ParamByName('id').Value := dm1.tblBrisiUplatiID_FNG.value;//tblNalogID.Value;
    qProveriNalog.ExecQuery;

    if qProveriNalog.FldByName['da'].isnull then
     begin
     if dm1.tblBrisiUplatiTIP_PARTNER.Value=1 then
      begin
      tblBrisiUplataSaldo.close;
      tblBrisiUplataSaldo.ParamByName('tip_partner').AsString:=dm1.tblBrisiUplatiTIP_PARTNER.AsString;  //cxGrid3DBTableView1TIP_PARTNER.EditValue;
      tblBrisiUplataSaldo.ParamByName('partner').AsString:=dm1.tblBrisiUplatiPARTNER_ID.AsString;  //cxGrid3DBTableView1PARTNER_ID.EditValue;
      tblBrisiUplataSaldo.ParamByName('tip').AsString:=DM1.TblKomTipUplataID.AsString;    //dm1.tblBrisiUplatiID_TU.AsString;;
      tblBrisiUplataSaldo.ParamByName('datum_uplata').AsString:=dm1.tblBrisiUplatiDATUM_UPLATA.AsString;  //cxControlDATUM_UPLATA.Text;
      tblBrisiUplataSaldo.ParamByName('dokument').AsString:=txtDok.Text; //dm1.tblBrisiUplatiDOKUMENT.AsString;      //cxGrid3DBTableView1DOKUMENT.EditValue;
      tblBrisiUplataSaldo.open;
      if tblBrisiUplataSaldo.RecordCount<=1 then
      begin
        //if (tblBrisiUplataSaldoTIP_NALOG.Value='') and (tblBrisiUplataSaldoIZNOS.Value=tblBrisiUplataSaldoOSTANATO.Value) then
        //begin
            //dm1.tblBrisiUplati.delete;
       frmDaNe := TfrmDaNe.Create(self,'������!','���� ��� �������? ',1);
        if (frmDaNe.ShowModal <> mrYes) then
                            Abort
        else
        begin
            if tblBrisiUplataSaldo.RecordCount=1 then
                tblBrisiUplataSaldo.Delete;


            //�� �� �������� ���� ������ ������ �� ��� �� ������, ����� �� ����� � ��������
            // �� ���� ��������
//            qBrisiUpaltaFirmi.Close;
//            qBrisiUpaltaFirmi.ParamByName('tip_partner').AsString:=dm1.tblBrisiUplatiTIP_PARTNER.AsString;  //cxGrid3DBTableView1TIP_PARTNER.EditValue;
//            qBrisiUpaltaFirmi.ParamByName('partner').AsString:=dm1.tblBrisiUplatiPARTNER_ID.AsString;  //cxGrid3DBTableView1PARTNER_ID.EditValue;
//            qBrisiUpaltaFirmi.ParamByName('tip').AsString:=dm1.tblBrisiUplatiID_TU.AsString;    //dm1.tblBrisiUplatiID_TU.AsString;;
//            qBrisiUpaltaFirmi.ParamByName('datum_uplata').AsString:=dm1.tblBrisiUplatiDATUM_UPLATA.AsString;  //cxControlDATUM_UPLATA.Text;
//            qBrisiUpaltaFirmi.ParamByName('dokument').AsString:=dm1.tblBrisiUplatiDOKUMENT.AsString;      //cxGrid3DBTableView1DOKUMENT.EditValue;
//            qBrisiUpaltaFirmi.ExecQuery;
              dm1.pBrisiUplata.Close;
              dm1.pBrisiUplata.ParamByName('IN_REF_ID2').AsString := dm1.tblBrisiUplatiO_REF_ID2.AsString;
              dm1.pBrisiUplata.ParamByName('id_in_temp').AsString := dm1.tblBrisiUplatiID_TEMP.AsString;
              DM1.pBrisiUplata.ParamByName('in_konto').AsString := dm1.tblBrisiUplatiO_KONTO.AsString;
             if dm1.tblBrisiUplatiID.AsString <> '' then
                 dm1.pBrisiUplata.ParamByName('id_in').AsString := dm1.tblBrisiUplatiID.AsString
             else
                 dm1.pBrisiUplata.ParamByName('id_in').AsString := '0';
              dm1.pBrisiUplata.ExecProc;

            //zapis vo FIN_UPLATA_TEMP


            dm1.tblBrisiUplati.FullRefresh;
        end;
      end
      else
//      if tblBrisiUplataSaldo.RecordCount=0 then
//      begin
//
//        //brisi gi site uplati po toj dokument, datum
//        //problem e so toa dali tie uplati se site neproknizani
//        //ako gi izbrisam site uplati so ne se proknizani, dali kje ja zadovoluvat sumata na dokumentot
//      end
//      else

      begin
          ShowMessage('�� ���� �� �� ������� ������!'+#10#13+' ��������� ����� � ��������� ��� � ������ �� ����� ������!');
          Abort;
      end;
     end
     //end
      else
      begin
         //dm1.tblBrisiUplati.delete;
          frmDaNe := TfrmDaNe.Create(self,'������!','���� ��� �������? ',1);
        if (frmDaNe.ShowModal <> mrYes) then
                            Abort
        else
        begin
          //���� �� ����� ���������� �� ������
           dm1.pBrisiUplata.Close;
           dm1.pBrisiUplata.ParamByName('IN_REF_ID2').AsString := dm1.tblBrisiUplatiO_REF_ID2.AsString;
           dm1.pBrisiUplata.ParamByName('id_in_temp').AsString := dm1.tblBrisiUplatiID_TEMP.AsString;
           DM1.pBrisiUplata.ParamByName('in_konto').AsString := dm1.tblBrisiUplatiO_KONTO.AsString;
           if dm1.tblBrisiUplatiID.AsString <> '' then
               dm1.pBrisiUplata.ParamByName('id_in').AsString := dm1.tblBrisiUplatiID.AsString;
         //  dm1.pBrisiUplata.ParamByName('id_in').AsString := dm1.tblBrisiUplatiID.AsString;
           dm1.pBrisiUplata.ExecProc;

          dm1.tblBrisiUplati.CloseOpen(True);
        end;

      end;
 // end;
 // else
 // begin
 //   ShowMessage('�������� � ����������. �� ���� �� �� �������! ');
   // Abort;
 end
 else
 begin
   ShowMessage('�������� � �� ��������� �����. �� ������ ����� �� �� ���������� ������� '+dm1.tblBrisiUplatiTIP_NALOG.AsString+'/'+dm1.tblBrisiUplatiNALOG.AsString+' ����� ��������� !!! ');
   Abort;
 end;
end;

procedure TFrmUplati.aBrisiSaldoExecute(Sender: TObject);
begin
   if (DM1.tblBrisiSaldoTIP_NALOG.AsString='') or (dm1.tblBrisiSaldoID_FNG.AsString='') then
   begin
     DM1.tblBrisiSaldo.Delete;
   end
   else
   begin
     ShowMessage('������� � ���������. �� ���� �� �� �������!');
     Abort;
   end;

end;

procedure TFrmUplati.aIzlezExecute(Sender: TObject);
begin
    //if DM1.TblKomUplati.State in [dsInsert,dsEdit] then
    if PageControl1.TabIndex = 0 then
    begin
    if DM1.TblKomUplati.Active then
    begin
        if DM1.TblKomUplati.State in [dsInsert,dsEdit] then
        begin
            DM1.TblKomUplati.Cancel;
        end;
        DM1.TblKomUplati.Close;

        setPodatociPresmetkaText('','','','','','','','','','');

        panelControls.Enabled := false;
        cxTxtBrojFaktura.Enabled := true;
        cxTxtBrojFaktura.SetFocus;
    end
    else
    begin
        Close();
    end;
    end
    else if PageControl1.TabIndex=1 then
    begin

        if not panelPartner.Enabled then
        begin
            DM1.TblKomLokaciiPoPartner.Close;
            DM1.TblNeplateniZadolzuvanjaPoLokacija.Close();
            cxDateDatumUplata.Enabled := false;
            cxMaskPlateno.Enabled := false;
            clearDataPartner();
            panelPartner.Enabled := true;
            cxLookUpComboBoxTipPartner.SetFocus;
        end
        else
        begin
            Close();
        end;

    end
    else
    begin
        if dm1.tblBrisiUplati.Active then
  //      begin
          if dm1.tblBrisiUplati.State in [dsInsert,dsEdit] then
          begin
             dm1.tblBrisiUplati.Cancel;
            cxGrid3.SetFocus;;
    //      end;
          end
        else
          Close;
    end;
end;

procedure TFrmUplati.aNeplateniExecute(Sender: TObject);
begin
     cxGrid2DBTableView1.DataController.Filter.BeginUpdate;
  try
    cxGrid2DBTableView1.DataController.Filter.Root.Clear;
    cxGrid2DBTableView1.DataController.Filter.Root.AddItem(cxGrid2DBTableView1RAZLIKA, foGreater, '0,00', '0,00');
  //  cxGrid2DBTableView1.DataController.Filter.Root.AddItem(cxGrid2DBTableView1IZNOS_VKUPNO, foNotEqual, null, '');
  finally
    cxGrid2DBTableView1.DataController.Filter.EndUpdate;
  end;
 //UnCheckAll(DenesniRN);
  cxGrid2DBTableView1.Controller.GoToFirst;
end;

procedure TFrmUplati.aOznaciSiteExecute(Sender: TObject);
var i:integer;
begin
  site(true);
end;

procedure TFrmUplati.aRaskniziUplataExecute(Sender: TObject);
begin
    //update na polinjata za rasknizuvanje
   if (DM1.tblBrisiUplatiID_FNG.AsString<>'') then
   begin
     qUpdateUplata.Close;
     qUpdateUplata.ParamByName('id').Value := DM1.tblBrisiUplatiID.Value;
     qUpdateUplata.ExecQuery;

     DM1.tblBrisiUplati.CloseOpen(true);
   end
   else
   begin
     ShowMessage('�������� �� � ����������. �� ���� �� �� ��������!');
     Abort;
   end;

end;

procedure TFrmUplati.aZapisiExecute(Sender: TObject);
var f:double;
begin
//if TEdit(sender).Name='cxMaskPlateno' then
           // begin
                if cxMaskPlateno.Text = '' then
                begin
                    ShowMessage('������ �� ������� ����� �� ������');
                    cxMaskPlateno.SetFocus;
                    Abort();
                end
                else
                if cxDateDatumUplata.Text='' then
                begin
                    ShowMessage('������ �� ������� ����� �� ������');
                    cxDateDatumUplata.SetFocus;
                    Abort();
                end
                else
                if dxBarLookupCombo1.Text='' then
                begin
                   ShowMessage('������ �� ������� ��� �� ������');
                   dxBarLookupCombo1.SetFocus;
                   Abort();
                end;
                   if dxBarLookupComboNalog.Text='' then
                begin
                   ShowMessage('������ �� �������� ����� �� ������');
                   dxBarLookupComboNalog.SetFocus;
                   Abort();
                end;
                // �� �������� ���� ��� �����
//                qDaliImaSaldo.Close;
//                qDaliImaSaldo.ParamByName('tip_partner').AsInteger:=dm1.TblKomLokaciiPoPartnerTIP_PARTNER.Value;
//                qDaliImaSaldo.ParamByName('partner').AsInteger:=DM1.TblKomLokaciiPoPartnerPARTNER_ID.AsInteger;
//                qDaliImaSaldo.ParamByName('tip').AsString:=DM1.TblKomTipUplataID.AsString;
//                qDaliImaSaldo.ParamByName('dokument').AsString:=txtDok.Text;
//                qDaliImaSaldo.ExecQuery;
//
//                if qDaliImaSaldo.FldByName['saldo'].AsInteger<>0 then
//                begin
//                   saldo:=qDaliImaSaldo.FldByName['saldo'].Value;
//                   f :=Plateno+saldo;
//                end
//                else
                   f :=Plateno;
                rasporediAndInsertMultipleUplati(f);
end;

procedure TFrmUplati.onEnterAll(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TFrmUplati.onExitAll(Sender: TObject);
begin
   if (TEdit(Sender).Name='cxTxtBrojFaktura') and (dxBarLookupCombo1.text<>'') and (cxControlDATUM_UPLATA.Text<>'') then
            begin
             if (cxTxtBrojFaktura.Text<>'')  then
           // begin
             //   Panel2.Enabled:=True;
             //   cxTxtLokacija.SetFocus;
          //  end
           //  else
             begin
                getPresmetkaPoBrojUplata(TEdit(Sender).Text);

                if (qPresmetka.RecordCount=0) then
                begin
                    ShowMessage('��������� �� �� ��� �� ������� �� ������!');
                    Panel2.Enabled:=True;
                    cxTxtLokacija.SetFocus;
                end;
             end;
            end;

    if (cxMaskPlateno=TcxTextEdit(Sender)) and (cxMaskPlateno.Text<>'') then
    begin
   //   if Plateno=0 then
      Plateno:=cxMaskPlateno.EditValue;
      ZaVracanje:=(Plateno+saldo)-VkZadolzuvanje;
      cxTxtZaVracanje.text:=FormatFloat('#,##0.00',ZaVracanje);
      cxMaskPlateno.Text:=FormatFloat('#,##0.00',Plateno);
    end;

//     if (cxLookUpComboBoxTipPartnerLok=TcxLookupComboBox(Sender)) and (cxLookUpComboBoxTipPartnerLok.Text<>'') then
//     // if cxLookUpComboBoxTipPartner.Text<>'' then
//       dmmat.tblPartner.Filter:='TIP_PARTNER='+QuotedStr(cxLookUpComboBoxTipPartnerLok.Text);
//
//
//     if (cxTxtPartnerIDLok.Text<>'') and (cxLookUpComboBoxTipPartnerLok.Text<>'') then
//        begin
//           dmmat.tblPartner.Filter:='ID='+QuotedStr(cxTxtPartnerIDLok.Text);
//           dmmat.tblPartner.Filtered:=True;
//        end
//        else if cxLookUpComboBoxTipPartnerLok.Text<>'' then
//        dmmat.tblPartner.Filtered:=True;
//

    if (cxTxtPartnerNaziv=TcxTextEdit(Sender)) and (cxTxtPartnerNaziv.Text<>'') then
    begin
      if DM1.TblKomLokacii.Active = true then
    begin
        DM1.TblKomLokacii.Close;
    end;
    DM1.TblKomLokacii.Close;
    DM1.TblKomLokacii.ParamByName('partner_id').AsString := cxTxtPartnerIDLok.text;
    DM1.TblKomLokacii.ParamByName('tip_partner').AsString := cxLookUpComboBoxTipPartnerLok.Text;
    DM1.TblKomLokacii.Open;

    //nova forma za izbor na lokacija za koja kje se pravi presmetka , ako ima povekje od edna lokacija
     dm1.TblKomLokacii.FetchAll;
     if dm1.TblKomLokacii.RecordCount>1 then
     begin
        frmIzberiLokacija:=TfrmIzberiLokacija.Create(Application);
        frmIzberiLokacija.ShowModal;
        cxTxtLokacija.Text := DM1.TblKomLokaciiID.AsString;
      //  izlez:=frmIzberiLokacija.Tag;
        frmIzberiLokacija.Free;
     end
     else
     begin
         cxTxtLokacija.Text := DM1.TblKomLokaciiID.AsString;
         tblPoslednoZadolzuvanje.Close;
         tblPoslednoZadolzuvanje.ParamByName('LOKACIJA').AsString:=cxTxtLokacija.Text;
         tblPoslednoZadolzuvanje.Open;
         if tblPoslednoZadolzuvanje.RecordCount>0 then
         begin
           cxTxtBrojFaktura.Text:=tblPoslednoZadolzuvanjeBROJ_FAKTURA.AsString;
           cxTxtMesec.Text:=tblPoslednoZadolzuvanjeMESEC.AsString;
           cxTxtGodina.Text:=tblPoslednoZadolzuvanjeGODINA.AsString;
           cxTxtNazivPartner.Text:=tblPoslednoZadolzuvanjeNAZIV.AsString;
           cxTxtIznosPresmetka.Text:=tblPoslednoZadolzuvanjeIZNOS_VKUPNO.AsString;
           cxTxtPlateno.Text:=tblPoslednoZadolzuvanjePLATENO.AsString;
           cxTxtRazlika.Text:=tblPoslednoZadolzuvanjeRAZLIKA.AsString;
           cxLookUpComboBoxTipPartnerLok.Text := tblPoslednoZadolzuvanjeTIP_PARTNER.AsString;
           cxTxtPartnerIDLok.Text := tblPoslednoZadolzuvanjePARTNER.AsString;
           dmMat.tblPartner.Filtered := False;
           cxTxtMesec.SetFocus;
          // getPresmetkaPoBrojUplata(cxTxtBrojFaktura.Text);
         end
         else
         begin
           ShowMessage('��� ������� �� ���������!!!');
           Abort;
         end;
     end;
    end;

    TEdit(Sender).Color:=clWhite;
end;

procedure TFrmUplati.onKeyDownAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var f:double;
begin
        case key of
        VK_RETURN:
        begin

            if (TEdit(Sender).Name='cxTxtBrojFaktura') and (dxBarLookupCombo1.text<>'') and (cxControlDATUM_UPLATA.Text<>'') then
            begin
             if cxTxtBrojFaktura.Text='' then
             begin
                Panel2.Enabled:=True;
                cxTxtLokacija.SetFocus;
             end
             else
             begin
                getPresmetkaPoBrojUplata(TEdit(Sender).Text);

                if (cxTxtMesec.Text='') and (qPresmetka.RecordCount=0) then
                begin
                    ShowMessage('��������� �� �� ��� �� ������� �� ������!');
                    Panel2.Enabled:=True;
                    cxTxtLokacija.SetFocus;
                end;
             end;
            end
            else if (TEdit(Sender).Name='cxTxtPartnerIDLok') and (cxTxtPartnerIDLok.Text = '')   then
            begin
                cxBtnPartneriClickLok(Sender);
                ActiveControl := cxTxtMesec;
            end
            else
            // begin
               if (TEdit(Sender).Name='cxTxtPartnerIDLok') and (cxTxtPartnerIDLok.Text<>'') and (cxLookUpComboBoxTipPartnerLok.Text<>'') then
                  begin
                     dmmat.tblPartner.Filter:='ID='+QuotedStr(cxTxtPartnerIDLok.Text);
                     dmmat.tblPartner.Filtered:=True;

                     setGridForLokacii(VarToStr(cxLookUpComboBoxTipPartnerLok.EditValue),cxTxtPartnerIDLok.Text);



                     cxTxtLokacija.Text := DM1.TblKomLokaciiPoPartnerID.AsString;
                     tblPoslednoZadolzuvanje.Close;
                     tblPoslednoZadolzuvanje.ParamByName('LOKACIJA').AsString:=cxTxtLokacija.Text;
                     tblPoslednoZadolzuvanje.Open;
                     if tblPoslednoZadolzuvanje.RecordCount>0 then
                     begin
                       cxTxtBrojFaktura.Text:=tblPoslednoZadolzuvanjeBROJ_FAKTURA.AsString;
                       cxTxtMesec.Text:=tblPoslednoZadolzuvanjeMESEC.AsString;
                       cxTxtGodina.Text:=tblPoslednoZadolzuvanjeGODINA.AsString;
                       cxTxtNazivPartner.Text:=tblPoslednoZadolzuvanjeNAZIV.AsString;
                       cxTxtIznosPresmetka.Text:=tblPoslednoZadolzuvanjeIZNOS_VKUPNO.AsString;
                       cxTxtPlateno.Text:=tblPoslednoZadolzuvanjePLATENO.AsString;
                       cxTxtRazlika.Text:=tblPoslednoZadolzuvanjeRAZLIKA.AsString;
                       cxLookUpComboBoxTipPartnerLok.Text := tblPoslednoZadolzuvanjeTIP_PARTNER.AsString;
                       cxTxtPartnerIDLok.Text := tblPoslednoZadolzuvanjePARTNER.AsString;
                       dmMat.tblPartner.Filtered := False;
                       cxTxtMesec.SetFocus;
                      // getPresmetkaPoBrojUplata(cxTxtBrojFaktura.Text);
                     end
                     else
                     begin
                       ShowMessage('��� ������� �� ���������!!!');
                       Abort;
                     end;

                    dmmat.tblPartner.Filtered:=False;

                     //cxBtnPartneriClickLok(Sender);
                     //cxTxtPartnerNaziv.Text := dmMat.tblPartnerNAZIV.Value;
                    //ActiveControl := cxTxtMesec;
                  end
                  //else if cxLookUpComboBoxTipPartnerLok.Text<>'' then
                  //
            // end


            else if (TEdit(Sender).Name='cxControlIZNOS') then
            begin
          //    if cxTxtRazlika.Text>'0' then
          //    begin
                if (TEdit(Sender).Text <> '') then
                begin   if (Validacija(Panel2) = false) then
                     begin
                       if cxTxtBrojFaktura.Text<>'' then
                       begin
                      //koga ima prebaruvanje na presmetka po lokacija, se sluci da ima na razlicni lokacii ist broj na faktura
                        qPresmetka.Close;
                        qpresmetka.ParamByName('bf').AsString:=cxTxtBrojFaktura.Text;
                        if cxTxtLokacija.Text<>'' then
                            qPresmetka.ParamByName('l').AsString:=cxTxtLokacija.Text
                        else
                            qPresmetka.ParamByName('l').AsString:='%';
                        qPresmetka.Open;
                       end
                       else
                       begin
                           ShowMessage('������� ��� �� �������!');
                           cxTxtBrojFaktura.SetFocus;
                           Abort;
                       end;

                    if StrToFloat(cxControlIZNOS.Text)>999999 then
                       begin
                          frmDaNe := TfrmDaNe.Create(self,'������ �����!','��������� ����� � ����� �����. ���� �� ���������� ��������? ',1);
                          if (frmDaNe.ShowModal <> mrYes) then
                            cxControlIZNOS.SetFocus
                          else
                          begin
                              zapisiUplata(cxTxtMesec.Text,cxTxtGodina.Text,cxTxtLokacija.Text,cxTxtIznosPresmetka.Text, qPresmetkaID.AsString);
                          end;
                       end
                   else
                    zapisiUplata(cxTxtMesec.Text,cxTxtGodina.Text,cxTxtLokacija.Text,cxTxtIznosPresmetka.Text,qPresmetkaID.AsString);
                end;
                end;
            //  end
             // else
           //   begin
            //      ShowMessage('��������� � �������!');
              //    Abort;
              //end;
            end
            else if TEdit(sender).Name='cxTxtPartnerID' then
            begin
                  if ( (TEdit(sender).Text = '') or (cxLookUpComboBoxTipPartner.Text = '')) then
                  begin
                      callFrmPartneri();
                  end
                  else
                  begin
                      getPartner();
                  end;
            end
           else  if TEdit(sender).Name='cxMaskPlateno' then
            begin
                if cxMaskPlateno.Text = '' then
                begin
                   ShowMessage('������ �� ������� ����� �� ������');
                   Abort();
                end;
                if (plateno+saldo)=0 then
                   Plateno:=StrToFloat(cxMaskPlateno.Text);
                ZaVracanje:=(Plateno+saldo)-VkZadolzuvanje;
                cxTxtZaVracanje.text:=FormatFloat('#,##0.00',ZaVracanje);
                ActiveControl:=btZapisi;
            end
//                f :=strtofloat(cxMaskPlateno.Text);
//                rasporediAndInsertMultipleUplati(f);
//            end
            else
        if TEdit(sender).Name='cxTxtLokacija' then
            begin
                if cxTxtLokacija.Text = '' then
                begin
                    cxLookUpComboBoxTipPartnerLok.SetFocus;
                 //   ShowMessage('������ �� ������� �������!');
                    Abort();
                end
                else
                begin
                     tblPoslednoZadolzuvanje.Close;
                     tblPoslednoZadolzuvanje.ParamByName('LOKACIJA').AsString:=cxTxtLokacija.Text;
                     tblPoslednoZadolzuvanje.Open;
                     if tblPoslednoZadolzuvanje.RecordCount>0 then
                     begin
                       cxTxtBrojFaktura.Text:=tblPoslednoZadolzuvanjeBROJ_FAKTURA.AsString;
                       cxTxtMesec.Text:=tblPoslednoZadolzuvanjeMESEC.AsString;
                       cxTxtGodina.Text:=tblPoslednoZadolzuvanjeGODINA.AsString;
                       cxTxtNazivPartner.Text:=tblPoslednoZadolzuvanjeNAZIV.AsString;
                       cxTxtIznosPresmetka.Text:=tblPoslednoZadolzuvanjeIZNOS_VKUPNO.AsString;
                       cxTxtPlateno.Text:=tblPoslednoZadolzuvanjePLATENO.AsString;
                       cxTxtRazlika.Text:=tblPoslednoZadolzuvanjeRAZLIKA.AsString;
                       cxLookUpComboBoxTipPartnerLok.Text := tblPoslednoZadolzuvanjeTIP_PARTNER.AsString;
                       cxTxtPartnerIDLok.Text := tblPoslednoZadolzuvanjePARTNER.AsString;
                       dmMat.tblPartner.Filtered := False;
                       cxTxtMesec.SetFocus;
                      // getPresmetkaPoBrojUplata(cxTxtBrojFaktura.Text);
                     end
                     else
                     begin
                       ShowMessage('��� ������� �� ���������!!!');
                       Abort;
                     end;
                     dmmat.tblPartner.Filtered:=False;
                end;
            end
            else if TEdit(sender).Name='cxTxtGodina' then
            begin
                if (cxTxtGodina.Text <> '') or (cxTxtMesec.Text<>'') then
                {begin
                    ShowMessage('������ �� ������� �������!');
                    Abort();
                end
                else}
                begin
                     tblUplata.Close;
                     tblUplata.ParamByName('LOKACIJA').AsString:=cxTxtLokacija.Text;
                     tblUplata.ParamByName('MESEC').AsString:=cxTxtMesec.Text;
                     tblUplata.ParamByName('GODINA').AsString:=cxTxtGodina.Text;
                     tblUplata.Open;
                     if tblUplata.RecordCount>0 then
                     begin
                       cxTxtBrojFaktura.Text:=tblUplataBROJ_FAKTURA.AsString;
                       cxTxtMesec.Text:=tblUplataMESEC.AsString;
                       cxTxtGodina.Text:=tblUplataGODINA.AsString;
                       cxTxtNazivPartner.Text:=tblUplataNAZIV.AsString;
                       cxTxtIznosPresmetka.Text:=tblUplataIZNOS_VKUPNO.AsString;
                       cxTxtPlateno.Text:=tblUplataPLATENO.AsString;
                       cxTxtRazlika.Text:=tblUplataRAZLIKA.AsString;
                       getPresmetkaPoBrojUplata(cxTxtBrojFaktura.Text);
                     end
                     else
                     begin
                       ShowMessage('��� ������� �� ����� ��� ������!!!');
                       Abort;
                     end;
                end;
            end
            else
            begin
                PostMessage(Handle,WM_NextDlgCtl,0,0)
            end;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
    end;
end;

procedure TFrmUplati.onKeyDownLookUpAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin

          if (cxLookUpComboBoxTipPartnerLok=TcxLookupComboBox(Sender)) and (cxLookUpComboBoxTipPartnerLok.Text<>'') then
     // if cxLookUpComboBoxTipPartner.Text<>'' then
           // dmMat.tblPartner.Filtered := False;
            dmmat.tblPartner.Filter:='TIP_PARTNER='+QuotedStr(cxLookUpComboBoxTipPartnerLok.Text);
            dmMat.tblPartner.Filtered:=True;
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;
end;

procedure TFrmUplati.PageControl1Change(Sender: TObject);
begin
    if PageControl1.TabIndex = 2 then
    begin
       dm1.tblBrisiUplati.Close;
       dm1.tblBrisiUplati.ParamByName('in_datum').AsString:=cxControlDATUM_UPLATA.Text;
       DM1.tblBrisiUplati.ParamByName('in_vraboten').AsString:=dmKon.user;
       DM1.tblBrisiUplati.ParamByName('in_re').AsInteger:=dmKon.re;
       DM1.tblBrisiUplati.ParamByName('in_god').AsInteger:=txtGodina.EditValue;
       dm1.tblBrisiUplati.Open;
    end
    else
    begin
        if PageControl1.TabIndex = 1 then
    begin
     if panelPartner.Enabled then
     begin
         cxLookUpComboBoxTipPartner.SetFocus;
         cxDateDatumUplata.Text := cxControlDATUM_UPLATA.Text;
     end;
    end
    else
     if PageControl1.TabIndex = 3 then
     begin
       dm1.tblBrisiSaldo.Close;
       DM1.tblBrisiSaldo.Open;
     end

     else
     if PageControl1.TabIndex = 0 then
     begin

     end;
    end;
end;

procedure TFrmUplati.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    {
    if DM1.TblKomUplati.Active then
    begin
        DM1.TblKomUplati.Close;
    end;

    if DM1.TblKomLokaciiPoPartner.Active then
    begin
        DM1.TblKomLokaciiPoPartner.Close;
    end;
    if DM1.TblNeplateniZadolzuvanjaPoLokacija.Active then
    begin
        DM1.TblNeplateniZadolzuvanjaPoLokacija.Close();
    end;
    }
    DM1.TblKomTipUplata.Close;
    DM1.closeDataSetIfActive(DM1.TblKomUplati);
    DM1.closeDataSetIfActive(DM1.TblKomLokaciiPoPartner);
    DM1.closeDataSetIfActive(DM1.TblNeplateniZadolzuvanjaPoLokacija);
    dm1.closeDataSetIfActive(dm1.tblBrisiUplati);
    Action := caFree;
end;

procedure TFrmUplati.FormCreate(Sender: TObject);
begin
   dm1.tblPartner.Close;
   dm1.tblPartner.ParamByName('tp').AsString:='%';
   dm1.tblPartner.Open;

   tblNalog.ParamByName('re').AsInteger := dmKon.re;
   tblNalog.Open;

end;

procedure TFrmUplati.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var vk_selectirani,i:integer;
begin
  // if (ActiveControl=cxGrid2) then
    if (key=VK_RETURN) and (cxGrid2DBTableViewIzberi.Editing) then
   begin
    vk_selectirani:=0;
    case key of
        VK_RETURN:
        begin
            cxDateDatumUplata.Text := cxControlDATUM_UPLATA.Text;//Now;
            cxDateDatumUplata.setFocus;

            with cxGrid2DBTableView1.DataController do
             //begin
              for I := 0 to RecordCount - 1 do
              begin
                if (DisplayTexts[i,cxGrid2DBTableViewIzberi.Index]= '�����') then
               // begin
                    vk_selectirani := vk_selectirani+1;
               // end;
                cxTxtVkOdbereni.Text := floattostr(vk_selectirani);
              end;
        end;
   end;
   end;
end;

procedure TFrmUplati.FormShow(Sender: TObject);
begin
    txtGodina.EditValue := YearOf(now);
    DM1.TblKomTipUplata.Open;
    PageControl1.TabIndex := 0;
    Panel2.Enabled:=False;
    panelControls.Enabled := false;
    cxTxtBrojFaktura.SetFocus;
end;

procedure TFrmUplati.getPresmetkaPoBrojUplata(broj_uplata:String);
var lokacija, godina, mesec,pres:String;
begin

//koga ima prebaruvanje na presmetka po lokacija, se sluci da ima na razlicni lokacii ist broj na faktura
    qPresmetka.Close;
    qpresmetka.ParamByName('bf').AsString:=broj_uplata;
    if cxTxtLokacija.Text<>'' then
        qPresmetka.ParamByName('l').AsString:=cxTxtLokacija.Text
    else
        qPresmetka.ParamByName('l').AsString:='%';
   // qPresmetka.ParamByName('i').AsInteger:=rgTipPartner.ItemIndex;
    qPresmetka.Open;

   qPresmetka.FetchAll;
    if qPresmetka.RecordCount>1 then
    begin
       ShowMessage('��������� ���� � �� ��� ��������� �������. '+#10#13+' �� ��������� ������� �� ���������!' );
       lokacija  := qPresmetkaLOKACIIN_ID.AsString;//Copy(broj_uplata,1,Length(broj_uplata)-6);
       mesec := qPresmetkaMESEC.AsString; //Copy(broj_uplata,Length(broj_uplata)-1,2);
       godina := qPresmetkaGODINA.AsString;
       pres:=qPresmetkaID.AsString;
    //   Panel2.Enabled:=True;
    //   cxTxtLokacija.SetFocus;
    end
   else
   if qPresmetka.RecordCount=1 then
  //  if qPresmetka.RecordCount<>0 then
    begin
        lokacija  := qPresmetkaLOKACIIN_ID.AsString;//Copy(broj_uplata,1,Length(broj_uplata)-6);
        mesec := qPresmetkaMESEC.AsString; //Copy(broj_uplata,Length(broj_uplata)-1,2);
        godina := qPresmetkaGODINA.AsString;
        pres:=qPresmetkaID.AsString; //(broj_uplata,Length(broj_uplata)-5,4);
    end
    else
    begin
        lokacija  := '0';
        godina := '0';
        mesec := '0';
    end;
// if qPresmetka.RecordCount<=1 then
// begin
    DM1.QGetPresmetkaPoBrojUplata.ParamByName('BROJ_FAKTURA').AsString := broj_uplata;
    DM1.QGetPresmetkaPoBrojUplata.ParamByName('LOKACIJA').AsString := lokacija;
    DM1.QGetPresmetkaPoBrojUplata.ParamByName('MESEC').AsString := mesec;
    DM1.QGetPresmetkaPoBrojUplata.ParamByName('GODINA').AsString := godina;
    DM1.QGetPresmetkaPoBrojUplata.ExecQuery;
    if (DM1.QGetPresmetkaPoBrojUplata.RecordCount > 0) then
    begin
          setPodatociPresmetkaText(DM1.QGetPresmetkaPoBrojUplata.FieldByName('BROJ_FAKTURA').AsString,DM1.QGetPresmetkaPoBrojUplata.FieldByName('MESEC').AsString,DM1.QGetPresmetkaPoBrojUplata.FieldByName('GODINA').AsString
          ,DM1.QGetPresmetkaPoBrojUplata.FieldByName('IZNOS_VKUPNO').AsString,DM1.QGetPresmetkaPoBrojUplata.FieldByName('NAZIV').AsString
          ,DM1.QGetPresmetkaPoBrojUplata.FieldByName('ID').AsString,DM1.QGetPresmetkaPoBrojUplata.FieldByName('PLATENO').AsString,DM1.QGetPresmetkaPoBrojUplata.FieldByName('RAZLIKA').AsString, DM1.QGetPresmetkaPoBrojUplata.FldByName['TIP_PARTNER'].AsString,dm1.QGetPresmetkaPoBrojUplata.FldByName['PARTNER'].AsString);

      if strtoint(cxTxtRazlika.Text)<0 then
      begin
         panelControls.Enabled := true;
        // panel2.Enabled:=True;
         cxControlIZNOS.SetFocus;

         DM1.TblKomUplati.Close;
         DM1.TblKomUplati.ParamByName('MESEC').AsString := cxTxtMesec.Text;
         DM1.TblKomUplati.ParamByName('GODINA').AsString := cxTxtGodina.Text;
         DM1.TblKomUplati.ParamByName('LOKACIJA').AsString := cxTxtLokacija.Text;

         DM1.TblKomUplati.Open;
         DM1.TblKomUplati.Insert;
         dm1.TblKomUplatiPRESMETKA_G_ID.AsString:=pres;
         DM1.TblKomUplatiDATUM_UPLATA.AsString := cxControlDATUM_UPLATA.Text;
         DM1.TblKomUplatiVRABOTEN.AsString := dmKon.user;
         DM1.TblKomUplatiIZNOS.AsInteger :=Round(Abs(StrToFloat(cxTxtRazlika.Text)));
         cxTxtBrojFaktura.Enabled := True;
         Panel2.Enabled:=false;
      end
      else
      begin
            frmDaNe := TfrmDaNe.Create(self,'������� �������!','��������� � �������! ���� ������ �� ��������� ������? ',1);
            if (frmDaNe.ShowModal <> mrYes) then
               begin
                       panelControls.Enabled := false;
                       Panel2.Enabled:=False;
                       setPodatociPresmetkaText('','','','','','','','','','');
                       cxTxtBrojFaktura.SetFocus;
               end
            else
            begin
               panelControls.Enabled := true;
               Panel2.Enabled:=true;
               cxControlIZNOS.SetFocus;

               dm1.tblProveriPromena.Close;
               DM1.tblProveriPromena.ParamByName('MESEC').AsString := cxTxtMesec.Text;
               DM1.tblProveriPromena.ParamByName('GODINA').AsString := cxTxtGodina.Text;
               DM1.tblProveriPromena.ParamByName('LOKACIJA').AsString := cxTxtLokacija.Text;
               dm1.tblProveriPromena.Open;

               if dm1.tblProveriPromena.RecordCount>0 then
               begin
                 DM1.TblKomUplati.Close;
                 DM1.TblKomUplati.ParamByName('MESEC').AsString := cxTxtMesec.Text;
                 DM1.TblKomUplati.ParamByName('GODINA').AsString := cxTxtGodina.Text;
                 DM1.TblKomUplati.ParamByName('LOKACIJA').AsString := cxTxtLokacija.Text;

                 DM1.TblKomUplati.Open;
                 DM1.TblKomUplati.Edit;
                 DM1.TblKomUplatiTS.AsDateTime := Now;
                 DM1.TblKomUplatiVRABOTEN.AsString := dmKon.user;
                 DM1.TblKomUplatiIZNOS.AsFloat := Abs(StrToFloat(cxControlIZNOS.Text));
                 dm1.TblKomUplatiDATUM_UPLATA.AsString:=cxControlDATUM_UPLATA.Text;
                 DM1.TblKomUplatiTIP.AsInteger := DM1.TblKomTipUplataID.AsInteger;
                 cxTxtBrojFaktura.Enabled := False;
            end

            else
            begin
              ShowMessage('�� ���� �� �� ������� ������� �� ������!');
              panelControls.Enabled := false;
              Panel2.Enabled:=False;
              setPodatociPresmetkaText('','','','','','','','','','');
              cxTxtBrojFaktura.SetFocus;
            end;
            end
      end;
    end
    {
    else
    begin
        ShowMessage('��������� �� �� ��� �� ������� �� ������!');
    end;
    }
 //end;
end;


procedure TFrmUplati.enableCtrlPresmetkaData(enable:Boolean);
begin
    cxTxtMesec.Enabled := enable;
    cxTxtGodina.Enabled := enable;
    cxTxtIznosPresmetka.Enabled := enable;
    cxTxtNazivPartner.Enabled := enable;
    cxTxtLokacija.Enabled := enable;
end;


procedure TFrmUplati.setPodatociPresmetkaText(broj_faktura:String;mesec:String;godina:String;iznos_presmetka:String;naziv:String;lokacija:String;iznos_plateno:String;razlika:String;tip_partner:string;partner:string);
begin
    cxTxtBrojFaktura.Text := broj_faktura;
    cxTxtMesec.Text := mesec;
    cxTxtGodina.Text := godina;
    if iznos_presmetka<>'' then
      cxTxtIznosPresmetka.Text := floattostr(Round(strtofloat(iznos_presmetka)))
    else
      cxTxtIznosPresmetka.Text := iznos_presmetka;
    cxTxtNazivPartner.Text := naziv;
    cxTxtLokacija.Text := lokacija;
    cxTxtPlateno.Text := iznos_plateno;
    if razlika<>'' then
      cxTxtRazlika.Text := floattostr(Round(strtofloat(razlika)))
    else
      cxTxtRazlika.Text := razlika;
    cxTxtPartnerIDLok.Text := partner;
    cxLookUpComboBoxTipPartnerLok.Text := tip_partner;
end;

procedure TFrmUplati.zapisiUplata(mesec:String;godina:String;lokacija:String;iznos_presmetka:String;presmetka:string);
var iznos_pl:Real;
begin
    if (Abs(StrToint(cxTxtRazlika.Text))>Round(StrTofloat(cxControlIZNOS.Text))) then
    begin
        frmDaNe := TfrmDaNe.Create(self,'������ ������!','������� �� �������� � ����� �� ������� �� �����������!���� ������� ������ �� �� �������� ��������?',1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            Abort();
        end;
    end;

    //28.11.2019, ������ ������ �� �� ���� ������� ��� ����� �� �� �����,
    //����� ���, ���� ��� ����� ������� �� ���� ����������, �� ���� ������ ������

    iznos_pl := StrToFloat(cxControlIZNOS.Text);

    pBelFin.Close;
    pBelFin.ParamByName('IN_TP').AsString :=cxLookUpComboBoxTipPartnerLok.Text;
    pBelFin.ParamByName('IN_P').AsString := cxTxtPartnerIDLok.Text;
    pBelFin.ParamByName('IN_RE').Value := dmKon.re;
    pBelFin.ParamByName('IN_GOD').Value := txtGodina.EditValue;
    pBelFin.ParamByName('IN_NALOG').Value := tblNalogID.Value;
    pBelFin.ParamByName('IN_DOKUMENT').Value := cxTxtBrojFaktura.Text;
    pBelFin.ParamByName('IN_D').Value := 1;
    pBelFin.ExecProc;


    DM1.TblKomUplatiMESEC.AsString := mesec;
    DM1.TblKomUplatiGODINA.AsString := godina;
    DM1.TblKomUplatiLOKACIIN_ID.AsString := lokacija;
    dm1.TblKomUplatiPRESMETKA_G_ID.AsString:= presmetka;
    DM1.TblKomUplatiRE.Value := dmKon.re;
    DM1.TblKomUplatiTIP_NALOG.Value := tblNalogTIP_NALOG.Value;
    DM1.TblKomUplatiNALOG.Value := tblNalogNALOG.Value;
    DM1.TblKomUplatiTS_KNIZENJE.Value := Now;
    DM1.TblKomUplatiID_FNG.Value := tblNalogID.Value;

    //DM1.TblKomUplatiTIP.AsInteger := dxBarCombo1.CurItemIndex;
    DM1.TblKomUplatiTIP.AsInteger := DM1.TblKomTipUplataID.AsInteger;

    DM1.TblKomUplati.Post;


     //zapis i vo TEMP uplati
      dm1.qInsertUplatiTemp.Close;
      DM1.qInsertUplatiTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
      DM1.qInsertUplatiTemp.ParamByName('TIP_PARTNER').AsString := cxLookUpComboBoxTipPartnerLok.Text;
      DM1.qInsertUplatiTemp.ParamByName('PARTNER').AsString := cxTxtPartnerIDLok.Text;
      DM1.qInsertUplatiTemp.ParamByName('DATUM_DOKUMENT').AsString := cxControlDATUM_UPLATA.Text;
      DM1.qInsertUplatiTemp.ParamByName('IZNOS').AsFloat := iznos_pl; //dm1.TblKomUplatiIZNOS.value;//StrToFloat(cxControlIZNOS.Text);
      DM1.qInsertUplatiTemp.ParamByName('GODINA').AsInteger := txtGodina.EditValue;
      DM1.qInsertUplatiTemp.ParamByName('TIP').AsString := DM1.TblKomTipUplataID.AsString;
      DM1.qInsertUplatiTemp.ParamByName('VRABOTEN').AsString := dmKon.user;
      DM1.qInsertUplatiTemp.ParamByName('DOKUMENT').AsString := cxTxtBrojFaktura.Text;
      DM1.qInsertUplatiTemp.ParamByName('RE').Value := tblNalogRE.Value;
      DM1.qInsertUplatiTemp.ParamByName('NALOG').Value := tblNalogNALOG.Value;
      DM1.qInsertUplatiTemp.ParamByName('TIP_NALOG').AsString := tblNalogTIP_NALOG.Value;
      DM1.qInsertUplatiTemp.ParamByName('TS_KNIZENJE').Value := Now;
      DM1.qInsertUplatiTemp.ParamByName('KONTO').AsString := pBelFin.FldByName['O_KONTO'].Value;
      DM1.qInsertUplatiTemp.ParamByName('LOKACIJA').AsString := cxTxtLokacija.Text;
      DM1.qInsertUplatiTemp.ParamByName('id_uplata').Value := dm1.TblKomUplatiID.value;
      DM1.qInsertUplatiTemp.ParamByName('REF_ID').AsString := pBelFin.FldByName['O_REF_ID'].Value; //(Values[i,cxGrid2DBTableView1O_REF_ID.Index]);
      DM1.qInsertUplatiTemp.ParamByName('REF_ID2').AsString := pBelFin.FldByName['O_REF_ID2'].Value; // (Values[i,cxGrid2DBTableView1O_REF_ID2.Index]);

      DM1.qInsertUplatiTemp.ExecQuery;

    popolniPoslednaUplataData(mesec,godina,iznos_presmetka,cxTxtNazivPartner.Text,lokacija,cxControlIZNOS.Text,cxControlDATUM_UPLATA.Text);

    aIzlez.Execute;
end;

procedure TFrmUplati.callFrmPartneri;
var frmP:TfrmPartner;
begin
    //partnerSet := false;
    frmP :=TfrmPartner.Create(nil,false);
    //frmP.getEdit(TEdit(cxTxtPartnerTIP_PARTNER),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.getEditC(cxLookUpComboBoxTipPartner,TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
   if cxLookUpComboBoxTipPartner.Text<>'' then
   begin
        frmP.cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
       try
        frmP.cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        frmP.cxGrid1DBTableView1.DataController.Filter.Root.AddItem(frmP.cxGrid1DBTableView1TIP_PARTNER, foEqual, cxLookUpComboBoxTipPartner.Text, cxLookUpComboBoxTipPartner.Text);
       finally
        frmP.cxGrid1DBTableView1.DataController.Filter.EndUpdate;
       end;
   end;
 //UnCheckAll(DenesniRN);
   // frmP.cxGrid1DBTableView1.Controller.GoToFirst;

    frmP.ShowModal();
    frmP.Free();

    if frmP.ModalResult = mrOk then
    begin
        setGridForLokacii(VarToStr(cxLookUpComboBoxTipPartner.EditValue),cxTxtPartnerID.Text);
        setUpLokationData();
    end;

end;

procedure TFrmUplati.cxBtnPartneriClick(Sender: TObject);
begin
    callFrmPartneri();
end;

procedure TFrmUplati.cxBtnPartneriClickLok(Sender: TObject);
var frmP:TfrmPartner;
begin
    //partnerSet := false;
    frmP :=TfrmPartner.Create(nil,false);
    //frmP.getEdit(TEdit(cxTxtPartnerTIP_PARTNER),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.getEditC(cxLookUpComboBoxTipPartnerLok,TEdit(cxTxtPartnerIDLok),Tedit(cxTxtPartnerNaziv));
    cxTxtPartnerNaziv.Text := dmMat.tblPartnerNAZIV.Value;
//   if cxLookUpComboBoxTipPartnerLok.Text<>'' then
//   begin
//        frmP.cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
//       try
//        frmP.cxGrid1DBTableView1.DataController.Filter.Root.Clear;
//        frmP.cxGrid1DBTableView1.DataController.Filter.Root.AddItem(frmP.cxGrid1DBTableView1TIP_PARTNER, foEqual, cxLookUpComboBoxTipPartner.Text, cxLookUpComboBoxTipPartner.Text);
//       finally
//        frmP.cxGrid1DBTableView1.DataController.Filter.EndUpdate;
//       end;
//   end;
 //UnCheckAll(DenesniRN);
   // frmP.cxGrid1DBTableView1.Controller.GoToFirst;

    frmP.ShowModal();
    frmP.Free();

    if frmP.ModalResult = mrOk then
    begin
        setGridForLokacii(VarToStr(cxLookUpComboBoxTipPartnerLok.EditValue),cxTxtPartnerIDLok.Text);
     //   setUpLokationData();
        cxTxtLokacija.Text := DM1.TblKomLokaciiPoPartnerID.AsString;
                             tblPoslednoZadolzuvanje.Close;
                     tblPoslednoZadolzuvanje.ParamByName('LOKACIJA').AsString:=cxTxtLokacija.Text;
                     tblPoslednoZadolzuvanje.Open;
                     if tblPoslednoZadolzuvanje.RecordCount>0 then
                     begin
                       cxTxtBrojFaktura.Text:=tblPoslednoZadolzuvanjeBROJ_FAKTURA.AsString;
                       cxTxtMesec.Text:=tblPoslednoZadolzuvanjeMESEC.AsString;
                       cxTxtGodina.Text:=tblPoslednoZadolzuvanjeGODINA.AsString;
                       cxTxtNazivPartner.Text:=tblPoslednoZadolzuvanjeNAZIV.AsString;
                       cxTxtIznosPresmetka.Text:=tblPoslednoZadolzuvanjeIZNOS_VKUPNO.AsString;
                       cxTxtPlateno.Text:=tblPoslednoZadolzuvanjePLATENO.AsString;
                       cxTxtRazlika.Text:=tblPoslednoZadolzuvanjeRAZLIKA.AsString;
                       cxLookUpComboBoxTipPartnerLok.Text := tblPoslednoZadolzuvanjeTIP_PARTNER.AsString;
                       cxTxtPartnerIDLok.Text := tblPoslednoZadolzuvanjePARTNER.AsString;
                       dmMat.tblPartner.Filtered := False;
                       cxTxtMesec.SetFocus;
                      // getPresmetkaPoBrojUplata(cxTxtBrojFaktura.Text);
                     end
                     else
                     begin
                       ShowMessage('��� ������� �� ���������!!!');
                       Abort;
                     end;
                     dmmat.tblPartner.Filtered:=False;
    end;

end;

procedure TFrmUplati.cxCheckBox1Properties(Sender: TObject);
begin
//  suma_zadolzuvanje:=0;
  if cbSite.Checked then
  begin
    site(true);
    cxGrid2DBTableViewIzberiPropertiesChange(Sender);
  end
  else
  begin
    site(false);
    cxTxtVkZadolzuvanje.Text := '';
  end;
end;

procedure TFrmUplati.cxControlDATUM_UPLATAChange(Sender: TObject);
begin
 if PageControl1.TabIndex = 2 then
     begin
       dm1.tblBrisiUplati.Close;
       dm1.tblBrisiUplati.ParamByName('in_datum').AsString:=cxControlDATUM_UPLATA.Text;
       DM1.tblBrisiUplati.ParamByName('in_vraboten').AsString:=dmkon.user;
       DM1.tblBrisiUplati.ParamByName('in_re').AsInteger:=dmKon.re;
       DM1.tblBrisiUplati.ParamByName('in_god').AsInteger:=txtGodina.EditValue;
       dm1.tblBrisiUplati.Open;
     end
     else
      if PageControl1.TabIndex = 1 then
          cxDateDatumUplata.Text := cxControlDATUM_UPLATA.Text;
end;

procedure TFrmUplati.cxControlDATUM_UPLATAExit(Sender: TObject);
begin
  { if PageControl1.TabIndex = 2 then
     begin
       dm1.tblBrisiUplati.Close;
       dm1.tblBrisiUplati.ParamByName('d').AsString:=cxControlDATUM_UPLATA.Text;
       DM1.tblBrisiUplati.ParamByName('user').Value:=dmkon.user;
       dm1.tblBrisiUplati.Open;
     end;}


end;

procedure TFrmUplati.cxControlIZNOSPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  test_bcd : FmtBcd.TBcd;
begin
//
  if not TryStrToBCD(DisplayValue, test_bcd) then
  begin
    ShowMessage('������! ��������� ����� "' + DisplayValue + '" �� � �� �������� ������!');
    DisplayValue := '';
    (Sender as TcxDBTextEdit).SetFocus;
    Abort;
//    (Sender as TcxDBTextEdit).SelectAll;
  end;
end;

procedure TFrmUplati.cxDateDatumUplataExit(Sender: TObject);
begin
   cxMaskPlateno.SetFocus;
   Abort;
end;

procedure TFrmUplati.cxGrid2DBTableView1Column1PropertiesEditValueChanged(
  Sender: TObject);
begin
  //  vk := 0;
   // suma_zadolzuvanje := 0;
    i := 0;
    //ShowMessage(IntToStr(cxGrid2DBTableView1.Controller.SelectedRecordCount));
     if cxGrid2DBTableViewIzberi.Properties.IsChanging then
         begin
              vk := vk+1;
              //suma_zadolzuvanje := suma_zadolzuvanje+cxGrid2DBTableView1.Controller.SelectedRecords[i].Values[4];
              suma_zadolzuvanje := suma_zadolzuvanje+Abs(cxGrid2DBTableView1.Controller.SelectedRecords[i].Values[6]);
         end;

    if i < 0 then
    begin
        i := 0;
    end;
    if suma_zadolzuvanje < 0 then
    begin
        suma_zadolzuvanje := 0;
    end;
   // cxTxtVkOdbereni.Text := IntToStr(i);
    VkZadolzuvanje:=suma_zadolzuvanje;
    cxTxtVkZadolzuvanje.Text := FormatFloat('#,##0.00',VkZadolzuvanje);    //FloatToStr(suma_zadolzuvanje);
end;

procedure TFrmUplati.cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var vk_selectirani,i:integer;
begin
   vk_selectirani:=0;
    case key of
        VK_RETURN:
        begin
            cxDateDatumUplata.Text := cxControlDATUM_UPLATA.Text;//Now;
            cxDateDatumUplata.setFocus;

            with cxGrid2DBTableView1.DataController do
             //begin
              for I := 0 to RecordCount - 1 do
              begin
                if (DisplayTexts[i,cxGrid2DBTableViewIzberi.Index]= '�����') then
                begin
                    vk_selectirani := vk_selectirani+1;
                end;
                cxTxtVkOdbereni.Text := floattostr(vk_selectirani);
              end;
        end;
    end;
end;

procedure TFrmUplati.cxGrid2DBTableView1SelectionChanged(
  Sender: TcxCustomGridTableView);
var vk,i ,vk_odbereni: Integer;
    suma_zadolzuvanje:double;
begin
    vk := 0;
    suma_zadolzuvanje := 0;
    i := 0;
    //ShowMessage(IntToStr(cxGrid2DBTableView1.Controller.SelectedRecordCount));
     for i:=0 to cxGrid2DBTableView1.Controller.SelectedRecordCount-1 do
         begin
              vk := vk+1;
              //suma_zadolzuvanje := suma_zadolzuvanje+cxGrid2DBTableView1.Controller.SelectedRecords[i].Values[4];
              suma_zadolzuvanje := suma_zadolzuvanje+Abs(cxGrid2DBTableView1.Controller.SelectedRecords[i].Values[6]);
         end;

    if i < 0 then
    begin
        i := 0;
    end;
    if suma_zadolzuvanje < 0 then
    begin
        suma_zadolzuvanje := 0;
    end;
  //  cxTxtVkOdbereni.Text := IntToStr(i);
    cxTxtVkZadolzuvanje.Text := FloatToStr(suma_zadolzuvanje);
end;

procedure TFrmUplati.cxGrid2DBTableViewIzberiPropertiesChange(Sender: TObject);
var suma:Double;
    i,vk_selectirani :integer;
begin
    suma_zadolzuvanje:=0;
    vk_selectirani := 0;
    with cxGrid2DBTableView1.DataController do
      for I := 0 to RecordCount - 1 do
      begin
        if (DisplayTexts[i,cxGrid2DBTableViewIzberi.Index]= '�����') then
        begin
            suma_zadolzuvanje:= suma_zadolzuvanje+ StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]);
        end;

      end;
    // suma_zadolzuvanje := abs(suma_zadolzuvanje);
     VkZadolzuvanje:=suma_zadolzuvanje;
     ZaVracanje:=(Plateno+saldo)-VkZadolzuvanje;
     cxTxtZaVracanje.Text := FormatFloat('#,##0.00', ZaVracanje);
     cxTxtVkZadolzuvanje.Text := FormatFloat('#,##0.00', suma_zadolzuvanje);
     with cxGrid2DBTableView1.DataController do
             //begin
              for I := 0 to RecordCount - 1 do
              begin
                if (DisplayTexts[i,cxGrid2DBTableViewIzberi.Index]= '�����') then
               // begin
                    vk_selectirani := vk_selectirani+1;
               // end;
                cxTxtVkOdbereni.Text := floattostr(vk_selectirani);
              end;
end;

procedure TFrmUplati.cxLookUpComboBoxTipPartnerLokEnter(Sender: TObject);
begin
dmMat.tblPartner.Filtered := False;
end;

procedure TFrmUplati.cxTxtBrojFakturaKeyPress(Sender: TObject; var Key: Char);
begin
    if (dxBarLookupCombo1.text='') or (cxControlDATUM_UPLATA.Text='') or (dxBarLookupComboNalog.text='')
    // or (rgTipPartner.ItemIndex=-1)
     then
    begin
        ShowMessage('��������� ��������� �� ���, ����� � ����� �� �������!');
        cxTxtBrojFaktura.SetFocus;
        Abort;
    end;
end;

procedure TFrmUplati.getPartner;
begin
    //partnerSet := false;
    DM1.QGetPartner.Params[0].AsString := cxTxtPartnerID.Text;
    DM1.QGetPartner.Params[1].AsString := VarToStr(cxLookUpComboBoxTipPartner.EditValue);
    DM1.QGetPartner.ExecQuery();
    if DM1.QGetPartner.RecordCount >= 1 then
    begin
          //partnerSet := true;
          cxTxtPartnerNaziv.Text := DM1.QGetPartner.FieldByName('naziv').AsString;
          setGridForLokacii(VarToStr(cxLookUpComboBoxTipPartner.EditValue),cxTxtPartnerID.Text);
          setUpLokationData();
    end
    else
    begin
          cxLookUpComboBoxTipPartner.ItemIndex := -1;
          cxTxtPartnerID.Text := '';
          callFrmPartneri();
    end;

end;


procedure TFrmUplati.setGridForLokacii(tip_partner:String;partner:String);
begin
    DM1.TblKomLokaciiPoPartner.Close;
    DM1.TblKomLokaciiPoPartner.ParamByName('TIP_PARTNER').AsString := tip_partner;
    DM1.TblKomLokaciiPoPartner.ParamByName('PARTNER').AsString := partner;

    DM1.TblKomLokaciiPoPartner.Open;
  //  saldo:=dm1.TblKomLokaciiPoPartnerSALDO.AsFloat;
   // txtSaldo.text:=FormatFloat('#,##0.00',saldo);
end;

procedure TFrmUplati.setUpLokationData();
begin
    panelPartner.Enabled := false;
   // if (DM1.TblKomLokaciiPoPartner.RecordCount = 1) then
 //   begin
        setUpNeplateniZadolzuvanjaData(VarToStr(cxLookUpComboBoxTipPartner.EditValue),cxTxtPartnerID.Text,DM1.TblKomLokaciiPoPartnerID.AsString);

        cxGrid2.SetFocus();
  //  end
   // else
   // begin
   //     ShowMessage('�������� ������� �� ������!');
    //    cxGrid1.SetFocus();
   // end;

end;

procedure TFrmUplati.clearDataPartner;
begin
    cxTxtPartnerNaziv.Text := '';
    cxTxtPartnerID.Text := '';
    cxTxtVkOdbereni.Text := '';
    cxTxtVkZadolzuvanje.Text := '';
    cxMaskPlateno.Text := '';
   // cxDateDatumUplata.Text := '';
    cxLookUpComboBoxTipPartner.Clear;
    cxTxtZaVracanje.text := '';
    plateno:=0;
    VkZadolzuvanje:=0;
    ZaVracanje:=0;
   // txtDok.Text:='';
    saldo:=0;
    txtsaldo.text:='';
    cbSite.Checked:=false;
end;

procedure TFrmUplati.setUpNeplateniZadolzuvanjaData(tip_partner:String;partner:String;lokacija:String);
var sender:TObject;
begin

    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('IN_TP').AsString := tip_partner;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('IN_P').AsString := partner;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('IN_RE').Value := dmKon.re;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('IN_GOD').Value := txtGodina.EditValue;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('IN_NALOG').Value := tblNalogID.Value;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('IN_D').Value := 0;
  //  DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('LOKACIJA').AsString := lokacija;

    DM1.TblNeplateniZadolzuvanjaPoLokacija.Open();
    aNeplateni.Execute;
    //cbSite.Checked:=True;
    //aOznaciSite.Execute;
    //cxCheckBox1Properties(Sender);
//    site(true);
   // cxGrid2DBTableViewIzberiPropertiesChange(Sender:tobject);

    if cxGrid2DBTableView1.DataController.FilteredRecordCount=0 then
    begin
      ShowMessage('���� ������� �� �������� �������!'+#10#13+' ��������� ����� �� ���� �����');

      //Abort;
    end;
  //  if DM1.TblNeplateniZadolzuvanjaPoLokacijaRAZLIKA.Value=0 then
   // begin
    //    ShowMessage('��������� � ��������!');
    //    Abort;
   // end
   // else
   // begin
       cxDateDatumUplata.Enabled := true;
       cxMaskPlateno.Enabled := true;
  //  end;
end;


procedure TFrmUplati.rasporediAndInsertMultipleUplati(vk_iznos:double);
var vneseni, i, vk_selectirani :Integer;
       suma_podeleno,momentalno, platen_del :double;
    kusur :String;
begin
    if (cxTxtVkZadolzuvanje.Text = '') and (cxGrid2DBTableView1.DataController.FilteredRecordCount<>0) then
    begin
        ShowMessage('������ ����������� ������ �� �������!');
        cxGrid2.SetFocus;
        Abort;
    end;
  try
    vk_selectirani:=0;
    momentalno := vk_iznos;
    //vk_selectirani := cxGrid2DBTableView1.Controller.SelectedRecordCount;
     with cxGrid2DBTableView1.DataController do
     //begin
      for I := 0 to RecordCount - 1 do
      begin
        if (DisplayTexts[i,cxGrid2DBTableViewIzberi.Index]= '�����') then
        begin
            vk_selectirani := vk_selectirani+1;
            if (momentalno>0) and (Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]))>0) then
            begin
          //  ShowMessage(DisplayTexts[i,cxGrid2DBTableView1LOKACIJA.Index]);
            if (DisplayTexts[i,cxGrid2DBTableView1LOKACIJA.Index] <> '')  then
              begin

                DM1.qInsertUplTemp.Close;
                DM1.qInsertUplTemp.ParamByName('PRESMETKA_G_ID').AsString := (Values[i,cxGrid2DBTableView1ID.Index]);
                DM1.qInsertUplTemp.ParamByName('DATUM').AsDateTime := cxDateDatumUplata.Date;
                //DM1.QInsertUplati.ParamByName('IZNOS').AsFloat := vk_iznos/vk_selectirani;
                if momentalno<Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index])) then
                begin
                  DM1.qInsertUplTemp.ParamByName('IZNOS').AsFloat := momentalno;
                  platen_del := momentalno;
                end
                else
                begin
                  DM1.qInsertUplTemp.ParamByName('IZNOS').AsFloat := Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
                  platen_del := Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
                end;
                DM1.qInsertUplTemp.ParamByName('GODINA').AsString := (Values[i,cxGrid2DBTableView1GODINA.Index]);
                DM1.qInsertUplTemp.ParamByName('LOKACIJA').AsString := (Values[i,cxGrid2DBTableView1LOKACIJA.Index]);
                DM1.qInsertUplTemp.ParamByName('MESEC').AsString := (Values[i,cxGrid2DBTableView1MESEC.Index]);
                DM1.qInsertUplTemp.ParamByName('TIP').AsString := DM1.TblKomTipUplataID.AsString;
                DM1.qInsertUplTemp.ParamByName('VRABOTEN').AsString := dmKon.user;

                DM1.qInsertUplTemp.ParamByName('DOKUMENT').AsString := txtDok.Text;
                DM1.qInsertUplTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
                DM1.qInsertUplTemp.ParamByName('RE').Value := tblNalogRE.Value;
                DM1.qInsertUplTemp.ParamByName('TIP_NALOG').AsString := tblNalogTIP_NALOG.Value;
                DM1.qInsertUplTemp.ParamByName('NALOG').Value := tblNalogNALOG.Value;

             //   DM1.QInsertUplati.ParamByName('TS_KNIZENJE').Value := Now;

             //   DM1.QInsertUplati.ExecQuery;

            //    DM1.qInsertUplatiTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
                DM1.qInsertUplTemp.ParamByName('TIP_PARTNER').AsString := cxLookUpComboBoxTipPartner.Text;
                DM1.qInsertUplTemp.ParamByName('PARTNER').AsString := cxTxtPartnerID.Text;
                DM1.qInsertUplTemp.ParamByName('BROJ_FAKTURA').AsString := (Values[i,cxGrid2DBTableView1BROJ_FAKTURA.Index]);
                DM1.qInsertUplTemp.ParamByName('DATUM_DOKUMENT').AsDateTime := cxDateDatumUplata.Date;

                DM1.qInsertUplTemp.ParamByName('REF_ID').AsString := (Values[i,cxGrid2DBTableView1O_REF_ID.Index]);
                DM1.qInsertUplTemp.ParamByName('REF_ID2').AsString := (Values[i,cxGrid2DBTableView1O_REF_ID2.Index]);
                DM1.qInsertUplTemp.ParamByName('KONTO').AsString := (Values[i,cxGrid2DBTableView1KONTO.Index]);
                DM1.qInsertUplTemp.ParamByName('tag').AsInteger := 2; //(Values[i,cxGrid2DBTableView1KONTO.Index]);
                DM1.qInsertUplTemp.ExecProc;

                suma_podeleno := suma_podeleno + Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
                momentalno := momentalno - Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
              end
              else
              begin

                DM1.qInsertUplTemp.Close;
              //  DM1.qInsertUplTemp.ParamByName('PRESMETKA_G_ID').AsString := (Values[i,cxGrid2DBTableView1ID.Index]);
                DM1.qInsertUplTemp.ParamByName('DATUM').AsDateTime := cxDateDatumUplata.Date;
                //DM1.QInsertUplati.ParamByName('IZNOS').AsFloat := vk_iznos/vk_selectirani;
                if momentalno<Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index])) then
                begin
                  DM1.qInsertUplTemp.ParamByName('IZNOS').AsFloat := momentalno;
                  platen_del := momentalno;
                end
                else
                begin
                  DM1.qInsertUplTemp.ParamByName('IZNOS').AsFloat := Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
            //      platen_del := Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
                end;
                DM1.qInsertUplTemp.ParamByName('TIP').AsString := DM1.TblKomTipUplataID.AsString;
                DM1.qInsertUplTemp.ParamByName('VRABOTEN').AsString := dmKon.user;

                DM1.qInsertUplTemp.ParamByName('DOKUMENT').AsString := txtDok.Text;
                DM1.qInsertUplTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
                DM1.qInsertUplTemp.ParamByName('RE').Value := tblNalogRE.Value;
                DM1.qInsertUplTemp.ParamByName('TIP_NALOG').AsString := tblNalogTIP_NALOG.Value;
                DM1.qInsertUplTemp.ParamByName('NALOG').Value := tblNalogNALOG.Value;

             //   DM1.QInsertUplati.ParamByName('TS_KNIZENJE').Value := Now;

             //   DM1.QInsertUplati.ExecQuery;

            //    DM1.qInsertUplatiTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
                DM1.qInsertUplTemp.ParamByName('TIP_PARTNER').AsString := cxLookUpComboBoxTipPartner.Text;
                DM1.qInsertUplTemp.ParamByName('PARTNER').AsString := cxTxtPartnerID.Text;
                DM1.qInsertUplTemp.ParamByName('BROJ_FAKTURA').AsString := (Values[i,cxGrid2DBTableView1BROJ_FAKTURA.Index]);
                DM1.qInsertUplTemp.ParamByName('DATUM_DOKUMENT').AsDateTime := cxDateDatumUplata.Date;

                DM1.qInsertUplTemp.ParamByName('REF_ID').AsString := (Values[i,cxGrid2DBTableView1O_REF_ID.Index]);
                DM1.qInsertUplTemp.ParamByName('REF_ID2').AsString := (Values[i,cxGrid2DBTableView1O_REF_ID2.Index]);
                DM1.qInsertUplTemp.ParamByName('KONTO').AsString := (Values[i,cxGrid2DBTableView1KONTO.Index]);
                DM1.qInsertUplTemp.ParamByName('tag').AsInteger := 1; //(Values[i,cxGrid2DBTableView1KONTO.Index]);
             //   DM1.qInsertUplatiTemp.ParamByName('id_uplata').Value := qKlucUplata.FldByName['ID'].Value;
                DM1.qInsertUplTemp.ExecProc;


//                DM1.qInsertUplatiTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
//                DM1.qInsertUplatiTemp.ParamByName('TIP_PARTNER').AsString := cxLookUpComboBoxTipPartner.Text;
//                DM1.qInsertUplatiTemp.ParamByName('PARTNER').AsString := cxTxtPartnerID.Text;
//                DM1.qInsertUplatiTemp.ParamByName('ID_FNG').Value := tblNalogID.Value;
//                DM1.qInsertUplatiTemp.ParamByName('DATUM_DOKUMENT').AsDateTime := cxDateDatumUplata.Date;
//              //DM1.QInsertUplati.ParamByName('IZNOS').AsFloat := vk_iznos/vk_selectirani;
//               if momentalno<Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index])) then
//                DM1.QInsertUplati.ParamByName('IZNOS').AsFloat := momentalno
//              else
//                DM1.qInsertUplatiTemp.ParamByName('IZNOS').AsFloat := Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
//                DM1.qInsertUplatiTemp.ParamByName('GODINA').AsInteger := yearof(Now);
//               // DM1.qInsertUplatiTemp.ParamByName('LOKACIJA').AsString := (Values[i,cxGrid2DBTableView1LOKACIJA.Index]);
//               // DM1.qInsertUplatiTemp.ParamByName('MESEC').AsString := (Values[i,cxGrid2DBTableView1MESEC.Index]);
//                DM1.qInsertUplatiTemp.ParamByName('TIP').AsString := DM1.TblKomTipUplataID.AsString;
//                DM1.qInsertUplatiTemp.ParamByName('VRABOTEN').AsString := dmKon.user;
//              //  DM1.qInsertUplatiTemp.ParamByName('PRESMETKA_G_ID').AsString := (Values[i,cxGrid2DBTableView1ID.Index]);
//                DM1.qInsertUplatiTemp.ParamByName('DOKUMENT').AsString := (Values[i,cxGrid2DBTableView1BROJ_FAKTURA.Index]);
//
//                DM1.qInsertUplatiTemp.ParamByName('RE').Value := tblNalogRE.Value;
//                DM1.qInsertUplatiTemp.ParamByName('NALOG').Value := tblNalogNALOG.Value;
//                DM1.qInsertUplatiTemp.ParamByName('TIP_NALOG').AsString := tblNalogTIP_NALOG.Value;
//                DM1.qInsertUplatiTemp.ParamByName('TS_KNIZENJE').Value := Now;
//                DM1.qInsertUplatiTemp.ParamByName('KONTO').AsString := (Values[i,cxGrid2DBTableView1KONTO.Index]);
//
//                DM1.qInsertUplatiTemp.ParamByName('LOKACIJA').AsString := '0';
//
//                DM1.qInsertUplatiTemp.ParamByName('REF_ID').AsString := (Values[i,cxGrid2DBTableView1O_REF_ID.Index]);
//                DM1.qInsertUplatiTemp.ParamByName('REF_ID2').AsString := (Values[i,cxGrid2DBTableView1O_REF_ID2.Index]);
//             //   DM1.qInsertUplatiTemp.ParamByName('id_uplata').AsString := '0';
//
//                DM1.qInsertUplatiTemp.ExecQuery;
                suma_podeleno := suma_podeleno + Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
                momentalno := momentalno - Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]));
              end;
            end;

        end;
      end;

   // dmKon.tBaza.CommitRetaining;
    kusur :=  FloatToStr(vk_iznos - VkZadolzuvanje);
    ZaVracanje:=vk_iznos - VkZadolzuvanje;

 // ��� ��� ����� ����, �� �� ������ �� ����� KOM_UPLATI_SALDO
    //insert - ��� ������ ���� ����� �� ��������, update - ��� ��� ���

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ///  Trgnata opcijata za zapish na vishok pari na saldo
 ///  17.02.2020 godina, po baranje na Slavica Danova -- vishokot pari racno kje go vnesuvale vo finansovo
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////

//    tblUplataSaldo.Close;
//    tblUplataSaldo.ParamByName('tip_partner').AsInteger:=DM1.TblKomLokaciiPoPartnerTIP_PARTNER.AsInteger;
//    tblUplataSaldo.ParamByName('partner').AsInteger:=DM1.TblKomLokaciiPoPartnerPARTNER_ID.AsInteger;
//    tblUplataSaldo.ParamByName('tip').AsString:=DM1.TblKomTipUplataID.AsString;
//    tblUplataSaldo.ParamByName('dokument').AsString:=txtDok.Text;
//    tblUplataSaldo.ParamByName('datum_uplata').AsString:=cxDateDatumUplata.text;
//    tblUplataSaldo.Open;
//
//     if (tblUplataSaldo.IsEmpty) and (ZaVracanje>0) then
//            begin
//               tblUplataSaldo.Insert;
//               tblUplataSaldoTIP_PARTNER.AsInteger:=DM1.TblKomLokaciiPoPartnerTIP_PARTNER.AsInteger;
//               tblUplataSaldoPARTNER.AsInteger:=DM1.TblKomLokaciiPoPartnerPARTNER_ID.AsInteger;
//               tblUplataSaldoIZNOS.AsFloat:=ZaVracanje;
//               //tblUplataSaldoOSTANATO.AsFloat:=ZaVracanje;
//               tblUplataSaldoTIP.AsString:=DM1.TblKomTipUplataID.AsString;
//               tblUplataSaldoDOKUMENT.AsString:=txtDok.Text;
//               tblUplataSaldoDATUM_UPLATA.AsString:=cxDateDatumUplata.Text;
//               tblUplataSaldo.Post;
//            end
//          else
//          if not (tblUplataSaldo.IsEmpty)  then
//          begin
//            if (ZaVracanje>0) then
//            begin
//               tblUplataSaldo.Edit;
//               tblUplataSaldoIZNOS.AsFloat:=tblUplataSaldoIZNOS.AsFloat+ZaVracanje;
//               //tblUplataSaldoOSTANATO.AsFloat:=tblUplataSaldoOSTANATO.AsFloat+ZaVracanje;
//               tblUplataSaldo.Post;
//            end
//           // else
//              // tblUplataSaldo.delete;
//          end;
    ShowMessage('�������� � ��������!');

    DM1.TblNeplateniZadolzuvanjaPoLokacija.CloseOpen(true);
    aNeplateni.Execute;
    //cxDateDatumUplata.Text := '';
    cxMaskPlateno.Text := '';
    cxTxtVkZadolzuvanje.Text := '';
    cxTxtZaVracanje.Text := '';
    cxTxtVkOdbereni.Text:='';
    plateno:=0;
    VkZadolzuvanje:=0;
    ZaVracanje:=0;
   // txtDok.Text:='';
    Saldo:=0;
    txtsaldo.text:='';
    cxGrid2.SetFocus;
    cbSite.Checked:=False;
  finally

  end;
end;


procedure TFrmUplati.popolniPoslednaUplataData(mesec:String;godina:String;iznos:String;partner:String;lokacija:String;iznos_plateno:String;datum:String);
begin
    cxTxtMesecPos.Text := mesec;
    cxTxtGodinaPos.Text := godina;
    cxTxtIznosPresmetkaPos.Text := FloatToStr(Round(strtofloat(iznos)));
    cxTxtNazivPartnerPos.Text := partner;
    cxTxtLokacijaPos.Text := lokacija;
    cxTxtIznosPlatenoPos.Text := FloatToStr(Round(strtofloat(iznos_plateno)));

end;

procedure TFrmUplati.site(kako:Boolean);   //procedura so koa gi stikliram - otstikliram site
var    suma, I:INTEGER;
begin
        with cxGrid2DBTableView1.DataController do
        for I := 0 to FilteredRecordCount - 1 do
        begin
          Values[FilteredRecordIndex[i] ,  cxGrid2DBTableViewIzberi.Index]:=  kako;
        end;
end;
end.
