unit UnitVodSostPoedinecno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, Menus, cxLookAndFeelPainters, dxSkinsdxBarPainter, FIBDataSet,
  pFIBDataSet, ActnList, dxBar, cxClasses, StdCtrls, cxButtons, cxTextEdit,
  cxContainer, cxDBEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxControls, cxGridCustomView, cxGrid, ExtCtrls, ComCtrls,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinSilver, dxSkinStardust, dxSkinSummer2008, dxSkinValentine,
  dxSkinXmas2008Blue, cxLookAndFeels, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSpringTime, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, System.Actions;

type
  TfrmVodSost = class(TForm)
    StatusBar1: TStatusBar;
    panelGlaven: TPanel;
    panelDolen: TPanel;
    panelDolenGore: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn;
    cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1VODOMER_STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1PAUSAL: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    panelDolenDole: TPanel;
    lblNovaSostojba: TLabel;
    lblStaraSostojba: TLabel;
    lblNaziv: TLabel;
    Label5: TLabel;
    cxControlNOVA_SOSTOJBA: TcxDBTextEdit;
    cxControlSTARA_SOSTOJBA: TcxDBTextEdit;
    cxControlNAZIV: TcxDBTextEdit;
    cxControlADRESA: TcxDBTextEdit;
    dxBarManager1: TdxBarManager;
    BarButtonIzlez: TdxBarButton;
    SubListAkcii: TdxBarSubItem;
    dxBarButtonZapisiPodatoci: TdxBarButton;
    dxBarButton: TdxBarButton;
    ActionList1: TActionList;
    aIzlez: TAction;
    aZapisiPodatoci: TAction;
    aAzuriraj: TAction;
    aLokacii: TAction;
    aAzurirajPoedinecno: TAction;
    aCenovnik: TAction;
    DSKomVodomeriSostojba: TDataSource;
    TblKomVodomeriSostojba: TpFIBDataSet;
    TblKomVodomeriSostojbaREON_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaCITACKA_KNIGA: TFIBIntegerField;
    TblKomVodomeriSostojbaCITACKA_KNIGA_RB: TFIBIntegerField;
    TblKomVodomeriSostojbaLOKACIIN_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaVODOMERI_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaSTARA_SOSTOJBA: TFIBIntegerField;
    TblKomVodomeriSostojbaNOVA_SOSTOJBA: TFIBIntegerField;
    TblKomVodomeriSostojbaRAZLIKA: TFIBBCDField;
    TblKomVodomeriSostojbaGODINA: TFIBIntegerField;
    TblKomVodomeriSostojbaMESEC: TFIBIntegerField;
    TblKomVodomeriSostojbaTIP_PARTNER: TFIBIntegerField;
    TblKomVodomeriSostojbaNAZIV: TFIBStringField;
    TblKomVodomeriSostojbaADRESA: TFIBStringField;
    TblKomVodomeriSostojbaLOKACIJA_AKTIVEN: TFIBStringField;
    TblKomVodomeriSostojbaVODOMER_STATUS: TFIBSmallIntField;
    TblKomVodomeriSostojbaPAUSAL: TFIBSmallIntField;
    TblKomVodomeriSostojbaBROJ_NA_CLENOVI: TFIBIntegerField;
    TblKomVodomeriSostojbaRB1: TFIBIntegerField;
    StatusBar2: TStatusBar;
    aEdinecnaSostojba: TAction;
    TblKomVodomeriSostojbaPARTNER: TFIBIntegerField;
    constructor Create(Owner : TComponent; tipPartner : string; partner : string; mesec : string; godina : string; citackaKniga : string; Reon : string);reintroduce; overload;
    procedure FormShow(Sender: TObject);
    procedure aAzurirajPoedinecnoExecute(Sender: TObject);
    procedure cxControlNOVA_SOSTOJBAExit(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure AllKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aEdinecnaSostojbaExecute(Sender: TObject);
  private
      var tipPartner,partner,mesec,godina,citackaKniga,reon:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVodSost: TfrmVodSost;

implementation

uses dmKonekcija, dmMaticni, UnitDM1;
{$R *.dfm}

procedure TfrmVodSost.aAzurirajPoedinecnoExecute(Sender: TObject);
begin
    panelDolenDole.Enabled:=true;
    TblKomVodomeriSostojba.Edit;
    cxControlNOVA_SOSTOJBA.SetFocus;
end;

procedure TfrmVodSost.aEdinecnaSostojbaExecute(Sender: TObject);
begin
    DM1.pKomVSEdinecna.Close;
    dm1.pKomVSEdinecna.ParamByName('reon_id').AsString:=reon;
    dm1.pKomVSEdinecna.ParamByName('citacka_kniga').AsString:=citackaKniga;
    dm1.pKomVSEdinecna.ParamByName('godina').AsString:=godina;
    dm1.pKomVSEdinecna.ParamByName('mesec').AsString:=mesec;
    dm1.pKomVSEdinecna.ParamByName('tp').AsString:=tipPartner;
    dm1.pKomVSEdinecna.ParamByName('p').AsString:=partner;
    dm1.pKomVSEdinecna.ExecProc;

    TblKomVodomeriSostojba.Close;
    TblKomVodomeriSostojba.ParamByName('tp').AsString:=tipPartner;
    TblKomVodomeriSostojba.ParamByName('p').AsString:=partner;
    TblKomVodomeriSostojba.ParamByName('mesec').AsString:=mesec;
    TblKomVodomeriSostojba.ParamByName('godina').AsString:=godina;
    TblKomVodomeriSostojba.ParamByName('citacka_kniga').AsString:=citackaKniga;
    TblKomVodomeriSostojba.ParamByName('reon_id').AsString:=reon;
    TblKomVodomeriSostojba.open;
end;

procedure TfrmVodSost.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmVodSost.AllKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case key of
        VK_RETURN:
        begin
                  PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_DOWN:
        begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
            Key := 0;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
    end;
end;

constructor TfrmVodSost.Create(Owner : TComponent; tipPartner : string; partner : string; mesec : string; godina : string; citackaKniga : string; Reon : string);
begin
inherited Create(Owner);
    Self.tipPartner := tipPartner;
    Self.partner := partner;
    Self.mesec := mesec;
    Self.godina := godina;
    Self.citackaKniga := citackaKniga;
    self.reon := reon;
end;


procedure TfrmVodSost.cxControlNOVA_SOSTOJBAExit(Sender: TObject);
begin
   TblKomVodomeriSostojba.Post;
end;

procedure TfrmVodSost.FormShow(Sender: TObject);
begin
    panelDolenDole.Enabled:=False;

    TblKomVodomeriSostojba.Close;
    TblKomVodomeriSostojba.ParamByName('tp').AsString:=tipPartner;
    TblKomVodomeriSostojba.ParamByName('p').AsString:=partner;
    TblKomVodomeriSostojba.ParamByName('mesec').AsString:=mesec;
    TblKomVodomeriSostojba.ParamByName('godina').AsString:=godina;
    TblKomVodomeriSostojba.ParamByName('citacka_kniga').AsString:=citackaKniga;
    TblKomVodomeriSostojba.ParamByName('reon_id').AsString:=reon;
    TblKomVodomeriSostojba.open;
end;

end.
