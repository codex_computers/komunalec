unit UnitPromeniRedBr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, dxSkinsCore, dxSkinsDefaultPainters, cxControls,
  cxContainer, cxEdit, cxTextEdit, ComCtrls, ActnList, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinBlueprint, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinHighContrast, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSpringTime, dxSkinTheAsphaltWorld,
  dxSkinVS2010, dxSkinWhiteprint;

type
  TFrmPromeniRedenBroj = class(TForm)
    panelGlaven: TPanel;
    lblCitackaKniga: TLabel;
    lblRedenBrojOld: TLabel;
    lblCitackaKnigaRbNew: TLabel;
    cxTxtCitackaKniga: TcxTextEdit;
    cxTxtRedenBrojOld: TcxTextEdit;
    cxTxtRedenBrojNew: TcxTextEdit;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    Label1: TLabel;
    cxTxtCitackaKnigaNew: TcxTextEdit;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure onTxtEnter(Sender: TObject);
    procedure onTxtExit(Sender: TObject);
    procedure onTxtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    var citackaKniga, citackaKnigaRb, citackaKnigaNew, redenBrojNew :String;
  public
    { Public declarations }
    constructor Create(Owner : TComponent; citackaKniga:String; citackaKnigaRb:String; citackaKnigaNew:String; citackaKnigaRbNew:String);reintroduce; overload;
  end;

var
  FrmPromeniRedenBroj: TFrmPromeniRedenBroj;

implementation

uses UnitDM1, UnitPromeniCitackaKnigaRb;

{$R *.dfm}
//------------------------------------------------------------------------------
procedure TFrmPromeniRedenBroj.aIzlezExecute(Sender: TObject);
begin
    close();
end;

constructor TFrmPromeniRedenBroj.Create(Owner : TComponent; citackaKniga:String; citackaKnigaRb:String; citackaKnigaNew:String; citackaKnigaRbNew:String);
begin
inherited Create(Owner);
    Self.citackaKniga := citackaKniga;
    Self.citackaKnigaRb := citackaKnigaRb;

  //  Self.pred := pred;
    Self.citackaKnigaNew := citackaKnigaNew;

    Self.redenBrojNew := citackaKnigaRbNew;
end;
procedure TFrmPromeniRedenBroj.onTxtEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TFrmPromeniRedenBroj.onTxtExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TFrmPromeniRedenBroj.onTxtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key  of
        VK_RETURN:
        begin

            if cxTxtRedenBrojNew.Text = '' then
            begin
                ShowMessage('������ ������� �������� �� ��� ����� ���!');
                Abort();
            end;

            if cxTxtRedenBrojNew.Text = cxTxtRedenBrojOld.Text then
            begin
                ShowMessage('��������� ����� ��� � ��� �� �����������!');
                Abort();
            end;

            DM1.ProcKomUpdateRedBr.ParamByName('CIT_KNIGA').AsString := citackaKniga;
            DM1.ProcKomUpdateRedBr.ParamByName('RB_CIT_KN').AsString := citackaKnigaRb;
            //DM1.ProcKomUpdateRedBr.ParamByName('PRED').AsString := pred;
            //1.ProcKomUpdateRedBr.ParamByName('SLED').AsString := sled;
            DM1.ProcKomUpdateRedBr.ParamByName('CIT_KNIGA_pred').AsString := cxTxtCitackaKnigaNew.Text;
            DM1.ProcKomUpdateRedBr.ParamByName('RB_CIT_KN_pred').AsString := cxTxtRedenBrojNew.Text;
            DM1.ProcKomUpdateRedBr.ParamByName('operacija').AsString := '1';


            DM1.ProcKomUpdateRedBr.Prepare();
            DM1.ProcKomUpdateRedBr.ExecProc();

            if DM1.ProcKomUpdateRedBr.ParamByName('operacija').AsInteger = 1 then
            begin
                ShowMessage('������� ��� � ������� ��������!');
                DM1.TblKomLokacii.CloseOpen(true);
                close();
            end
            else
            begin
                 ShowMessage('������� ��� �� � ��������! Mo�� ����� ������� ��������� ��� ��������� ��������');
            end; 
        end;  
    end;
end;

procedure TFrmPromeniRedenBroj.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFrmPromeniRedenBroj.FormShow(Sender: TObject);
begin
    cxTxtCitackaKniga.Text := citackaKniga;
    cxTxtRedenBrojOld.Text := citackaKnigaRb;
end;

end.
