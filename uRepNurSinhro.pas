unit uRepNurSinhro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,System.Diagnostics, Vcl.Graphics,
  inifiles, System.StrUtils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB,
  FIBDataSet, pFIBDataSet, //uDoctorCounter,
  Utils, //uWebServis,
  IdTCPConnection, IdTCPClient, IdHTTP, IdBaseComponent,
  IdComponent, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL,IdMultipartFormData,//uSkriptor,
  pFIBScripter, FIBQuery, pFIBQuery, System.JSON,
  pFIBStoredProc, System.DateUtils;

type
  TfrmRepNurSinhro = class(TForm)
    timer: TTimer;
    tblRepUpd: TpFIBDataSet;
    tblRepUpdVREP_ID: TFIBIntegerField;
    tblRepUpdKOREN: TFIBIntegerField;
    tblRepUpdID: TFIBIntegerField;
    tblRepUpdNASLOV: TFIBStringField;
    tblRepUpdBR: TFIBIntegerField;
    tblRepUpdTABELA_GRUPA: TFIBStringField;
    tblRepUpdREP_PRETPLATA: TFIBSmallIntField;
    tblRepUpdXML: TFIBBlobField;
    tblRepUpdDATA: TFIBBlobField;
    dsRepUpd: TDataSource;
    tblrr: TpFIBDataSet;
    tblrrMVREP_ID: TFIBIntegerField;
    tblnn: TpFIBDataSet;
    tblnnREC_ID: TFIBBCDField;
    tblNurUpd: TpFIBDataSet;
    tblNurUpdID: TFIBIntegerField;
    tblNurUpdREC_ID: TFIBBCDField;
    tblNurUpdVID: TFIBIntegerField;
    tblNurUpdIME: TFIBStringField;
    tblNurUpdOPIS: TFIBStringField;
    tblNurUpdSEL: TFIBBlobField;
    tblNurUpdVISINA: TFIBIntegerField;
    tblNurUpdSIRINA: TFIBIntegerField;
    tblNurUpdKEY_FIELD_NAMES: TFIBStringField;
    tblNurUpdLIST_FIELD_ITEM: TFIBStringField;
    tblNurUpdGRUPA: TFIBStringField;
    tblKanal: TpFIBDataSet;
    tblKanalKANAL: TFIBStringField;
    tbloo: TpFIBDataSet;
    tblooEMBG: TFIBStringField;
    tblooDIST: TFIBStringField;
    tblooEZBO: TFIBStringField;
    tblooPREZIME: TFIBStringField;
    tblooT_IME: TFIBStringField;
    tblooIME: TFIBStringField;
    tblooDATUM_RAGJANJE: TFIBDateField;
    tblooADRESA: TFIBStringField;
    tblooTELEFON: TFIBStringField;
    tblooLEKAR: TFIBIntegerField;
    tblooFAKSIMIL_STOMATOLOG: TFIBIntegerField;
    tblooFAKSIMIL_GINEKOLOG: TFIBIntegerField;
    tblooOSNOV_NA_OSIGURUVANJE: TFIBStringField;
    tblooPRAVO_NA_OSIGURUVANJE: TFIBStringField;
    tblooVAZNOST_NA_OSIGURUVANJE: TFIBDateField;
    tblooZ_FOND: TFIBSmallIntField;
    tblooZ_BROJ: TFIBIntegerField;
    tblooZ_CLEN: TFIBSmallIntField;
    tblooBZL: TFIBStringField;
    tblooNEMAGO: TFIBIntegerField;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdHTTP1: TIdHTTP;
    tblKds: TpFIBDataSet;
    tblKdsATRIB: TFIBStringField;
    tblKdsVRED: TFIBStringField;
    tblPubLager: TpFIBDataSet;
    tblPubLagerRE: TFIBIntegerField;
    tblPubLagerARTVID: TFIBIntegerField;
    tblPubLagerARTSIF: TFIBIntegerField;
    tblPubLagerBRPAK: TFIBIntegerField;
    tblPubLagerLAGER: TFIBIntegerField;
    tblPubLagerLAGPAK: TFIBBCDField;
    tblPubLagerPRODCENA: TFIBBCDField;
    tblPubLagerNABCENA: TFIBBCDField;
    tblPubLagerDDV: TFIBBCDField;
    tblPubLagerTS_UPD: TFIBDateTimeField;
    tblPubLagerPUB: TFIBSmallIntField;
    tblPubLagerERRORCODE: TFIBStringField;
    tblPubLagerARTNAZ: TFIBStringField;
    tblPubLagerATC10: TFIBStringField;
    tblPubLagerPRIZVODITEL: TFIBStringField;
    tblPubLagerBARKOD: TFIBStringField;
    fbSkript: TpFIBScripter;
    tblVremeE: TpFIBDataSet;
    tblVremeETSID: TFIBDateTimeField;
    tblVremeEVREME: TFIBBCDField;
    tblVremeESKOROE: TFIBFloatField;
    pRrlogIns: TpFIBStoredProc;
    tbl: TpFIBDataSet;
    tblID: TFIBIntegerField;
    tblOPIS: TFIBStringField;
    tblPOZITIV: TFIBSmallIntField;
    tblVALUTA: TFIBStringField;
    ds: TDataSource;
    tblS: TpFIBDataSet;
    tblSARTVID: TFIBIntegerField;
    tblSARTSIF: TFIBIntegerField;
    tblSARTNAZ: TFIBStringField;
    tblSBRPAK: TFIBIntegerField;
    tblSCENA: TFIBBCDField;
    tblSDDV: TFIBBCDField;
    tblSCENANAB: TFIBBCDField;
    tblSMARZA: TFIBBCDField;
    tblSIZNOSMARZA: TFIBBCDField;
    tblSDOPLATA: TFIBBCDField;
    tblSPROIZVOD: TFIBStringField;
    tblSPARTIC: TFIBBCDField;
    tblSREZIM_IZDAVANJE: TFIBStringField;
    tblSGENERIKA: TFIBStringField;
    tblSGENNAZIVID: TFIBStringField;
    tblSPOLNO_IME: TFIBStringField;
    tblSGEN_NAZIV: TFIBStringField;
    tblSVAZEOD: TFIBDateTimeField;
    dsS: TDataSource;
    tblZastit: TpFIBDataSet;
    qZap: TpFIBQuery;
    tblPrezCen: TpFIBDataSet;
    dsPrezCen: TDataSource;
    tblPrezCenDATUM: TFIBDateField;
    tblPrezCenCENOVNIK: TFIBIntegerField;
    tblPrezCenTS_INS: TFIBDateTimeField;
    tblIspratnici: TpFIBDataSet;
    dsIspratnici: TDataSource;
    qInsert: TpFIBQuery;
    tblIspratniciIZLEZID: TFIBBCDField;
    tblIspratniciDOKGOD: TFIBSmallIntField;
    tblIspratniciDOKRE: TFIBSmallIntField;
    tblIspratniciDOKTIP: TFIBSmallIntField;
    tblIspratniciDOKBROJ: TFIBIntegerField;
    tblIspratniciPARTIP: TFIBIntegerField;
    tblIspratniciPARSIF: TFIBIntegerField;
    tblIspratniciDATUMIZLEZ: TFIBDateField;
    tblIspratniciTS_UPD: TFIBDateTimeField;
    tblIspratniciTS_INS: TFIBDateTimeField;
    tblIspratniciAPTID_DO: TFIBBCDField;
    tblIspratniciARTVID: TFIBIntegerField;
    tblIspratniciARTSIF: TFIBIntegerField;
    tblIspratniciBARKOD: TFIBStringField;
    tblIspratniciKOL: TFIBBCDField;
    tblIspratniciBRPAK: TFIBIntegerField;
    tblIspratniciPOPUST: TFIBFloatField;
    tblIspratniciDDV: TFIBBCDField;
    tblIspratniciVCENA: TFIBBCDField;
    qUpdateIsp: TpFIBQuery;
    pPubLagerAll: TpFIBStoredProc;
    tblIspratniciROK: TFIBDateField;
    pOnaduj: TpFIBStoredProc;

    procedure timerTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//  procedure izvrti();
    procedure rrRing();
    procedure nnRing();
    procedure ooRing();
    procedure licRing();
    procedure licRingVaziDo();
    procedure logRing(ootime:Int64);
    procedure lagerRing();
    procedure lagerRing2();
    procedure ThreadDone(Sender: TObject);
    procedure tblNurUpdAfterPost(DataSet: TDataSet);
    procedure tblNurUpdBeforePost(DataSet: TDataSet);
    function  getVersion():string;
    procedure PrezemiCenovnik;
    procedure pripremaZaCenovnik(cenovn:String);
    procedure ListToJson();
    procedure PubLagerAll;
  private
    { Private declarations }
             ftemp,ftemp2,ftmpdir:  String;
  public
    { Public declarations }
    data:TStringList;
    pat:String;
    manual,racno:Boolean;
end;

type
  Tsinc = class(TThread)
  protected
    procedure Execute; override;
  public
    fe: Integer;
end;

var
  frmRepNurSinhro: TfrmRepNurSinhro;
  ThreadsRunning: Integer;

  agreURL,rrURL:String;

  reklaUrl:String;
  reklaFile:String;
  infoFile:String;
  infoUrl :String;
  ima : Boolean;

  function izvrti(Ptr : Pointer) : LongInt; stdcall;

implementation

{$R *.dfm}

uses unitDm1, dmKonekcija, DaNe;

procedure Tsinc.Execute;
var
  tm:TStopwatch;
  ibo:String;
begin
   ibo:='SincStart';
//   DBModule.Setup7set('rrFazLog',ibo);

   Inc(ThreadsRunning);

   try
     frmRepNurSinhro.pRrlogIns.ParamByName('vreme').AsInt64 := -1;
     frmRepNurSinhro.pRrlogIns.ExecProc;
   Except
     //
   end;

   tm := TStopwatch.Create();
   tm.Start;

   try
     frmRepNurSinhro.rrRing();
     ibo:=ibo+'.rrRing';
//     DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;
   try
     frmRepNurSinhro.nnRing();
     ibo:=ibo+'.nnRing';
//     DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;
   if (frmRepNurSinhro.racno) or (ima = false) then //24.01.2019 - da se prostapuva ednash pri pushtanje na Prima Farm i racno
   begin
   try
     frmRepNurSinhro.licRing();
     ibo:=ibo+'.licRing';
//     DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;
   end;


   if (frmRepNurSinhro.racno) or (ima = false) then  //24.01.2019 - da se prostapuva ednash pri pushtanje na Prima Farm i racno
   begin
   try
     frmRepNurSinhro.licRingVaziDo();
     ibo:=ibo+'.licRingVaziDo';
//     DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;
    ima := True;
   end;

   try
    frmRepNurSinhro.lagerRing2();
    ibo:=ibo+'.lagerRing2';
//    DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;

 {
   try
     frmRepNurSinhro.lagerRing();
     ibo:=ibo+'.lagerRing';
     DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;
}
 //  frmRepNurSinhro.ooRing();
   tm.Stop;
   try
      frmRepNurSinhro.logRing(tm.ElapsedMilliseconds);
      ibo:=ibo+'.logRing';
//      DBModule.Setup7set('rrFazLog',ibo);
   Except
   end;

   //   ShowMessage(tm.Elapsed);
   ibo:=ibo+'.'+tm.Elapsed;
//   DBModule.Setup7set('rrFazLog',ibo);

   try
    frmRepNurSinhro.pRrlogIns.ParamByName('vreme').AsInt64 := tm.ElapsedMilliseconds;
    frmRepNurSinhro.pRrlogIns.ExecProc;
   Except
   end;

{ staveno u procedura
   frmRepNurSinhro.fbSkript.Script.Clear;
   frmRepNurSinhro.fbSkript.Script.Add('insert into AP7_RRLOG(vreme) values('+IntToStr( tm.ElapsedMilliseconds)+');');
   try
      frmRepNurSinhro.fbSkript.ExecuteScript(1);
      frmRepNurSinhro.fbSkript.Transaction.CommitRetaining;
   Except
//      ShowMessage('Blesava greshka');
   end;
}
end;

procedure TfrmRepNurSinhro.FormCreate(Sender: TObject);
begin
  ima := false;  // 24.01.2019 - parametar, za kontrola na pezemanje na licencata i pristap do bazata Codex
  tblPrezCen.Open;
  if tblPrezCenDATUM.AsDateTime <> date then
  begin
      PrezemiCenovnik;
   //   pOnaduj.ParamByName('godina').AsInteger := YearOf(Now);
   //   pOnaduj.ParamByName('mesec').AsInteger := MonthOf(Now);
   //   pOnaduj.ParamByName('onaduj').AsInteger :=1;
   //   pOnaduj.ExecProc;
     // ima := True;
  end;



  manual:=false;
 // rrURL := dmKon.url('repRepUrl');
  //timer.Interval := 60*60*1000; // 60 min za produkcijski se odkomentira
 // timer.Interval := 5*1000;   //deset sek  treba dase iskomentira samoza test
 // ThreadsRunning := 0 ;
 // IdHTTP1.Request.UserAgent:='AiS7 Ventura:'+getVersion;

 // ListToJson();

end;

procedure TfrmRepNurSinhro.timerTimer(Sender: TObject);
var
  vremee:Boolean;
begin
//
//  if(ThreadsRunning<1)then
//  begin
//    dmKon.trajSi(true);
//    try
//      tblVremeE.Open;
//
//      if(tblVremeE.RecordCount>0)
////      if(tblVremeESKOROE.AsFloat<0)
//      then
//        vremee := false
//      else
//        vremee := true;
//
//      tblVremeE.Close;
//    except
//    end;
//    if(vremee or manual)then
//    begin
//      with Tsinc.Create(True) do
//      begin
//        Start;
//        OnTerminate := ThreadDone;
//      end;
//    end;
//    dmKon.trajSi(false);
//  end;
end;

procedure TfrmRepNurSinhro.ThreadDone(Sender: TObject);
begin

//  ShowMessage('GOTO >'+IntToStr(ThreadsRunning ));
//  Dec(ThreadsRunning);
end;

//procedure TfrmRepNurSinhro.izvrti();
function izvrti(Ptr : Pointer) : LongInt; stdcall;
begin
//   if(ThreadsRunning=0)then
//   begin
//     Inc(ThreadsRunning);
//     //licring
//     frmRepNurSinhro.rrRing();
//     frmRepNurSinhro.nnRing();
//     //frmRepNurSinhro.ooRing();
//     Dec(ThreadsRunning);
//   end;
end;

procedure TfrmRepNurSinhro.rrRing();
var
 url:String;
 mm:TStringList;
 rr:string;
 rep:TStringList;
 getkoData,xrpMemo,xrpData,tsPnt:TStringList;
 tmpfn:string;
 gdIni:TMemIniFile;
 iskluc:String;
 i:integer;
 j: Integer;
 kanal:string;
 sega:TDateTime;
 prvpat:Boolean;
begin
//  prvpat:=True;
//  sega:=now();
//  mm := TStringList.Create;
//  rep:= TStringList.Create;
//  getkoData:= TStringList.Create;
//  xrpMemo := TStringList.Create;
//  xrpData := TStringList.Create;
//
//  tblKanal.Open;
//  if(tblKanal.RecordCount=0)
//  then kanal:='0'
//  else kanal:=tblKanalKANAL.AsString;
//  tblKanal.Close;
//
//  tblrr.Close;
//  tblrr.Open;
//  if(tblrrMVREP_ID.IsNull)
//  then rr:='0'
//  else rr:= tblrrMVREP_ID.AsString;
//
//  url:=rrURL+'almanah.php?mvrep_id='+rr+'&kanal='+kanal;
//  mm.Clear;
//  mm.Delimiter := chr(10);
//  mm.DelimitedText := GetInetString(url);
//
//  for I:=0 to mm.Count-1 do
//  begin
//     rep.Clear;
//     tmpfn:=pat+'repTMP-'+mm.Strings[i]+'.cxrep';
//     GetInetFile(rrURL+'download.php?vrep_id='+mm.Strings[i],tmpfn);
//     rep.LoadFromFile(tmpfn);
//
//     iskluc:='';
//     for j:=0 to rep.Count-1 do
//     begin
//        if(Length(iskluc)>1)then
//        begin
//           if( Pos(iskluc, rep.Strings[j])=0 )
//           then tsPnt.Add(rep.Strings[j])
//           else iskluc:='';
//        end
//        else
//        begin
//           if(Pos('<getkoData',rep.Strings[j])>0)then
//           begin
//              iskluc:='</getkoData';
//              tsPnt:=getkoData;
//           end else
//           if(Pos('<xrpMemoField',rep.Strings[j])>0)then
//           begin
//              iskluc:='</xrpMemoField';
//              tsPnt:=xrpMemo;
//           end else
//           if(Pos('<xrpDATAField',rep.Strings[j])>0)then
//           begin
//              iskluc:='</xrpDATAField';
//              tsPnt:=xrpDATA;
//           end;
//        end;
//     end;
////     ShowMessage(getkoDATA.Text);
////     ShowMessage(xrpMEMO.Text);
////     ShowMessage(xrpDATA.Text);
//
//     gdIni  := TMemIniFile.Create(pat+'tmp1234.ini') ;
//     getkoData.Insert(0,'[sekcia]');
//     gdIni.SetStrings(getkoData);
//
//     tblRepUpd.Close;
//     tblRepUpd.ParamByName('repid').AsString := gdIni.ReadString('sekcia','rep_id', '');
//     tblRepUpd.Open;
//
//     if(tblRepUpd.RecordCount=0)
//     then tblRepUpd.Insert
//     else tblRepUpd.Edit;
//     try
//        tblRepUpdID.AsString := gdIni.ReadString('sekcia','rep_id', '');
//        tblRepUpdVREP_ID.AsString := mm.Strings[i];
//        tblRepUpdNASLOV.AsString := gdIni.ReadString('sekcia','rep_opis','');
//        tblRepUpdKOREN.AsString := gdIni.ReadString('sekcia','rep_koren','');
//        tblRepUpdTABELA_GRUPA.AsString := gdIni.ReadString('sekcia','rep_grupa','');
//        tblRepUpdBR.AsString := gdIni.ReadString('sekcia','rep_br','');
//        tblRepUpdXML.AsString  := xrpMEMO.Text;
//        tblRepUpdDATA.AsString := xrpDATA.Text;
//        tblRepUpd.Post;
//        if(prvpat)then
//        begin
////            Memo1.Lines.Add(UTF8Encode('<h2>��������� ���� ������ �� ���������:</h2>'));
////            Memo1.Lines.Add(UTF8Encode('<h4>'+DateTimeToStr(sega)+'</h4>'));
//            prvpat:=false;
//        end;
////        Memo1.Lines.Add(UTF8Encode('<div>'+tblRepUpdID.AsString+' - '+tblRepUpdNASLOV.AsString+'</div>'))
//     Except
//     end;
//
//     //tuka post i comit
//     getkoDATA.Clear;
//     xrpMEMO.Clear;
//     xrpDATA.Clear;
//  end;
//  //ShowMessage('gotovo');
end;

procedure TfrmRepNurSinhro.lagerRing2;
var
 url,tst:String;
 slData: TStringList;
 EDB,apSifra,mdb5:String;
 resp,zobfile,mckfile,secfile :String;
 nabcena,prodcena,ddv:String;
 naziv,proiz:UTF8String;
 indi:TIdHTTP;
 fsParams: TIdMultiPartFormDataStream ;
begin
//  slData := TStringList.Create;
//  slData.Clear;
//
////  ss := TStringStream.Create('', TEncoding.UTF8);
//  indi := TIdHTTP.Create(nil);
//  indi.AllowCookies:=True;
//  indi.Request.UserAgent := 'AiS7 Ventura:'+getVersion;
//  indi.Request.ContentType := 'multipart/form-data';
//
//  apSifra := dmKon.AptShifra;
//  EDB     := dmKon.EDB;
//  mdb5    := StrMD5('edb:'+EDB+'<->fzoid:'+apSifra);
//
//  url:='https://ws.codex.mk/scriptor/postfile.php';
//  zobfile:=dmKon.cxTmpDir+'lgr.'+apSifra+'.zob';
//  mckfile:=dmKon.cxTmpDir+'lgr.'+apSifra+'.mck';
//  secfile:=dmKon.cxTmpDir+'lgr.'+apSifra+'.sec';
//
//  //post parametri
//  fsParams := TIdMultiPartFormDataStream.Create;
//  fsParams.AddFormField('aptid',apSifra);
//  fsParams.AddFormField('danocen',EDB);
//  fsParams.AddFormField('klk',mdb5);
//
//  slData.Clear;
//  slData.Add(apSifra);
//  slData.Add(EDB);
//  slData.Add(mdb5);
//  slData.SaveToFile(secfile,TEncoding.UTF8);
//
//  slData.Clear;
//  slData.Add('aptid,re,artvid,artsif,artnaz,brpak,atc10,lager,prodcena,nabcena,ddv,ts_upd,pub,proizvoditel,barkod');
//  slData.SaveToFile(mckfile);//,TEncoding.UTF8);
//
//  dmKon.trajSi(true);
//  try
//     tblPubLager.Close;
//     tblPubLager.ParamByName('toptan').AsInteger :=1 ;
//     tblPubLager.Open;
//     FormatSettings.DecimalSeparator := '.';
//
//     slData.Clear;
//     tblPubLager.First;
//     while(not tblPubLager.Eof) do
//     begin
//         tst     := DateTimeToStr(tblPubLagerTS_UPD.AsDateTime,tfstd);
//         nabcena := FormatFloat('0.00',tblPubLagerNABCENA.AsFloat);
//         prodcena:= FormatFloat('0.00',tblPubLagerPRODCENA.AsFloat);
//         ddv     := FormatFloat('0',tblPubLagerDDV.AsFloat);
//         naziv   := ReplaceStr(tblPubLagerARTNAZ.AsString,',',' ');
//         proiz   := ReplaceStr(tblPubLagerPRIZVODITEL.AsString,',',' ');
//         slData.Add(apSifra+','
//                   +tblPubLagerRE.AsString+','
//                   +tblPubLagerARTVID.AsString+','
//                   +tblPubLagerARTSIF.AsString+','
//                   +naziv+','
//                   +tblPubLagerBRPAK.AsString+','
//                   +tblPubLagerATC10.AsString+','
//                   +tblPubLagerLAGER.AsString+','
//                   +prodcena+','
//                   +nabcena+','
//                   +ddv+','
//                   +tst+','
//                   +tblPubLagerPUB.AsString+','
//                   +proiz+','
//                   +tblPubLagerBARKOD.AsString
//                  );
//         tblPubLager.Next;
//     end;
//     tblPubLager.Close;
//     slData.SaveToFile(zobfile,TEncoding.UTF8);
//     slData.Free;
//
//     fsParams.AddFile('lager', zobfile);
//     fsParams.AddFile('select', mckfile);
//     fsParams.AddFile('securitate', secfile);
//     resp := indi.Post(url, fsParams);
//
//   Except on  E : Exception do
//     ShowMessage('AA'+E.Message);
//  end;
//  dmKon.trajSi(false);
//  fsParams.Clear;
//  fsParams.Free;
//  indi.Free;
end;

procedure TfrmRepNurSinhro.lagerRing;
var
 url:String;
 i:integer;
 j: Integer;
 sega:TDateTime;
 prvpat:Boolean;
 ss:TStringStream;
 code:integer;
 fsParams: TStringList;
 slData: TStringList;
 EDB:String;
 apSifra:String;
 mdb5:String;
 resp:string;
 dotuk:String;
begin
//  dotuk:='2014.01.01'; //voa od servisot
//  slData := TStringList.Create;
//  slData.Clear;
//  fsParams := TStringList.Create;
//  fsParams.Clear;
//  ss := TStringStream.Create('', TEncoding.UTF8);
//  IdHTTP1.Request.ContentType := 'application/x-www-form-urlencoded';
//  IdHTTP1.Response.CharSet := 'UTF-8';
//  fsParams := TStringList.Create;
//
//  apSifra := dmKon.AptShifra;
//  EDB     :=dmKon.viewFirmiDANOCEN.AsString;
//  mdb5    :=StrMD5('edb:'+EDB+'<->fzoid:'+apSifra);
//  url:='https://ws.codex.mk/licencator/lagerpost.php?danocen='+EDB+'&s_fzo='+apSifra+'&klk='+mdb5;
//
//  dmKon.trajSi(true);
//  try
//     tblPubLager.Close;
//     tblPubLager.ParamByName('toptan').AsInteger:= 0 ;
//     tblPubLager.Open;
//
////     slData.Add('re,artvid,artsif,brpak,lager,lagpak,prodcena,nabcena,ddv,ts_upd,pub');
//     tblPubLager.First;
//     while(not tblPubLager.Eof) do
//     begin
//         fsParams.Clear;
//         ss.Clear;
//         fsParams.Add('aptid= '  +apSifra);
//         fsParams.Add('re='      +tblPubLagerRE.AsString);
//         fsParams.Add('artvid='  +tblPubLagerARTVID.AsString);
//         fsParams.Add('artsif='  +tblPubLagerARTSIF.AsString);
//         fsParams.Add('artnaz='  +tblPubLagerARTNAZ.AsString);
//         fsParams.Add('brpak='   +tblPubLagerBRPAK.AsString);
//         fsParams.Add('lager='   +tblPubLagerLAGER.AsString);
//         fsParams.Add('prodcena='+tblPubLagerPRODCENA.AsString);
//         fsParams.Add('nabcena=' +tblPubLagerNABCENA.AsString);
//         fsParams.Add('ddv='     +tblPubLagerDDV.AsString);
//         fsParams.Add('ts_upd='  +tblPubLagerTS_UPD.AsString);
//         fsParams.Add('pub='     +tblPubLagerPUB.AsString);
//         fsParams.Add('atc10='   +tblPubLagerATC10.AsString);
//         try
//            IdHTTP1.Post(url,fsParams,ss);
//            resp:=ss.DataString;
//            code:=IdHTTP1.ResponseCode;
//            if code=200 then
//            begin
//                //DBModule.Setup7set('PoslednaLicenca',resp);
//                tblPubLager.Edit;
//                tblPubLagerERRORCODE.AsString := resp;
//                tblPubLager.Post;
//            end
//         except
//            on E: eIdHTTPProtocolexception do
//             begin
//                //ShowMessage(E.ErrorMessage);
//             end;
//         end;
//         tblPubLager.Next;
//     end;
//     tblPubLager.Close;
//   Except on  E : Exception do
//     //ShowMessage('AA'+E.Message);
//  end;
//  dmKon.trajSi(false);
//  slData.Free;
end;

procedure TfrmRepNurSinhro.licRing();
var
 url:String;
 i:integer;
 j: Integer;
 sega:TDateTime;
 prvpat:Boolean;
 ss:TStringStream;
 code:integer;
 fsParams: TStringList;
 EDB:String;
 SifraFzo:String;
 mdb5:String;
 resp:string;
begin
//  prvpat:=True;
//  sega:=now();
//  //sashki
//  if( not dmKon.viewFirmi.Active)then dmKon.viewFirmi.Open;
//
//  EDB  :=dmKon.viewFirmiDANOCEN.AsString;
//  SifraFzo:= dmKon.AptShifra;
//  mdb5 :=StrMD5('edb:'+EDB+'<->fzoid:'+SifraFzo);
//
//  //Parametro kodos
//  fsParams := TStringList.Create;
//  fsParams.Add('EDB='+EDB);
//  fsParams.Add('SifraFZO='+SifraFzo);
//  fsParams.Add('Naziv='+dmKon.viewFirmiPARTNERNAZIV.AsString);
//  fsParams.Add('Adresa='+dmKon.viewFirmiADRESA.AsString);
//  fsParams.Add('Mesto='+dmKon.viewFirmiMESTONAZIV.AsString);
//  fsParams.Add('TEL='+dmKon.viewFirmiTEL.AsString);
//  fsParams.Add('FAX='+dmKon.viewFirmiFAX.AsString);
//  fsParams.Add('AppVer='+ trim(GetVersion()));
//
////
//  dmKon.trajSi(true);
//  try
//    tblKds.Open;
//    tblKds.First;
//    while(not tblKds.Eof) do
//    begin
//      fsParams.Add(tblKdsATRIB.AsString+'='+tblKdsVRED.AsString);
//      tblKds.Next;
//    end;
//    tblKds.Close;
//  except
//    fsParams.Add('AP7_KDS=Missing!');
//  end;
//  fsParams.Add('DBName='+dmKon.fibBaza.DBName);
//  fsParams.Add('DBFileName='+dmKon.fibBaza.DBFileName);
//
//  dmKon.trajSi(false);
//
////  url:='http://ws.codex.mk/licencator/licenca2p.test.php?danocen='+EDB+'&s_fzo='+SifraFzo+'&klk='+mdb5;
//  //url:='http://ws.codex.mk:8055/licencator/licenca2p.php?danocen='+EDB+'&s_fzo='+SifraFzo+'&klk='+mdb5;
//  url:=url_ml+'licencator/licenca2p.php?danocen='+EDB+'&s_fzo='+SifraFzo+'&klk='+mdb5;
//      //  http://ws.codex.mk:8055/
//  try
//       ss := TStringStream.Create('', TEncoding.UTF8);
//       IdHTTP1.Request.ContentType := 'application/x-www-form-urlencoded';
//       IdHTTP1.Response.CharSet := 'UTF-8';
//       IdHTTP1.Post(url,fsParams,ss);
//       //fsParams.SaveToFile('aaaaaaa.txt');
////       IdHTTP1.Get(url);
//       resp:=ss.DataString;
//       code:=IdHTTP1.ResponseCode;
//       if code=200 then //tua proeri licenca
//       begin
//           DBModule.Setup7set('PoslednaLicenca',resp);
//       end
//  except
//       on E: eIdHTTPProtocolexception do
//       begin
//        ShowMessage(E.ErrorMessage);
//       end;
//  end;
//
//  fsParams.Free;
end;

procedure TfrmRepNurSinhro.licRingVaziDo();
var
 url:String;
 i:integer;
 j: Integer;
 sega:TDateTime;
 prvpat:Boolean;
 ss:TStringStream;
 code:integer;
 fsParams: TStringList;
 EDB:String;
 SifraFzo:String;
 mdb5:String;
 resp:string;
begin
//  prvpat:=True;
//  sega:=now();
//  //sashki
//  if( not dmKon.viewFirmi.Active)then dmKon.viewFirmi.Open;
//
//  EDB  :=dmKon.viewFirmiDANOCEN.AsString;
//  SifraFzo:= dmKon.AptShifra;
//  mdb5 :=StrMD5('edb:'+EDB+'<->fzoid:'+SifraFzo);
//
//  //Parametro kodos
//  fsParams := TStringList.Create;
//  fsParams.Add('EDB='+EDB);
//  fsParams.Add('SifraFZO='+SifraFzo);
//  fsParams.Add('Naziv='+dmKon.viewFirmiPARTNERNAZIV.AsString);
//  fsParams.Add('Adresa='+dmKon.viewFirmiADRESA.AsString);
//  fsParams.Add('Mesto='+dmKon.viewFirmiMESTONAZIV.AsString);
//  fsParams.Add('TEL='+dmKon.viewFirmiTEL.AsString);
//  fsParams.Add('FAX='+dmKon.viewFirmiFAX.AsString);
//  fsParams.Add('AppVer='+ trim(GetVersion()));
//
////
//  dmKon.trajSi(true);
//  try
//    tblKds.Open;
//    tblKds.First;
//    while(not tblKds.Eof) do
//    begin
//      fsParams.Add(tblKdsATRIB.AsString+'='+tblKdsVRED.AsString);
//      tblKds.Next;
//    end;
//    tblKds.Close;
//  except
//    fsParams.Add('AP7_KDS=Missing!');
//  end;
//  fsParams.Add('DBName='+dmKon.fibBaza.DBName);
//  fsParams.Add('DBFileName='+dmKon.fibBaza.DBFileName);
//
//  dmKon.trajSi(false);
//
////  url:='http://ws.codex.mk/licencator/licenca2p.test.php?danocen='+EDB+'&s_fzo='+SifraFzo+'&klk='+mdb5;
////  url:='http://ws.codex.mk:8055/licencator/licenca2_vazi_do.php?danocen='+EDB+'&s_fzo='+SifraFzo+'&klk='+mdb5;
//  url:=url_ml+'licencator/licenca2_vazi_do.php?danocen='+EDB+'&s_fzo='+SifraFzo+'&klk='+mdb5;
//  try
//       ss := TStringStream.Create('', TEncoding.UTF8);
//       IdHTTP1.Request.ContentType := 'application/x-www-form-urlencoded';
//       IdHTTP1.Response.CharSet := 'UTF-8';
//       IdHTTP1.Post(url,fsParams,ss);
//       //fsParams.SaveToFile('aaaaaaa.txt');
////       IdHTTP1.Get(url);
//       resp:=ss.DataString;
//       code:=IdHTTP1.ResponseCode;
//       if code=200 then //tua proeri licenca
//       begin
//           DBModule.Setup7set('PoslednaLicenca_vazi_do',resp);
//       end
//  except
//       on E: eIdHTTPProtocolexception do
//       begin
//        ShowMessage(E.ErrorMessage);
//       end;
//  end;
//
//  fsParams.Free;
end;

procedure TfrmRepNurSinhro.tblNurUpdAfterPost(DataSet: TDataSet);
begin
 // dmKon.trajSi(False);
end;

procedure TfrmRepNurSinhro.tblNurUpdBeforePost(DataSet: TDataSet);
begin
 // dmKon.trajSi(True);
end;

procedure TfrmRepNurSinhro.nnRing();
var
 url:String;
 mm:TStringList;
 nur:TStringList;
 malipolinja,nurko_select,tsPnt:TStringList;
 gdIni:TMemIniFile;
 tmpfn:string;
 iskluc:String;
 i:integer;
 j: Integer;
 kanal:string;
begin
//  mm := TStringList.Create;
//  nur:= TStringList.Create;
//  malipolinja:= TStringList.Create;
//  nurko_select := TStringList.Create;
//
//  tblKanal.Open;
//  if(tblKanal.RecordCount=0)
//  then kanal:='0'
//  else kanal:=tblKanalKANAL.AsString;
//  tblKanal.Close;
//
//  tblnn.Close;
//  tblnn.Open;
//  url:=rrURL+'nurmanah.php?rec_id='+tblnnREC_ID.AsString+'&kanal='+kanal;
//  mm.Clear;
//  GetInetMemo(url,mm);
//  for I:=0 to mm.Count-1 do
//  begin
//     nur.Clear;
//     tmpfn:=pat+'nurTMP-'+mm.Strings[i]+'.cxnur';
//     GetInetFile(rrURL+'nurdownload.php?rec_id='+mm.Strings[i],tmpfn);
//     nur.LoadFromFile(tmpfn);
//
//     iskluc:='';
//     for j:=0 to nur.Count-1 do
//     begin
//        if(Length(iskluc)>1)then
//        begin
//           if( Pos(iskluc, nur.Strings[j])=0 )
//           then tsPnt.Add(nur.Strings[j])
//           else iskluc:='';
//        end
//        else
//        begin
//           if(Pos('<malipolinja',nur.Strings[j])>0)then
//           begin
//              iskluc:='</malipolinja>';
//              tsPnt:=malipolinja;
//           end else
//           if(Pos('<nurko_select',nur.Strings[j])>0)then
//           begin
//              iskluc:='</nurko_select>';
//              tsPnt:=nurko_select;
//           end;
//        end;
//     end;
////     ShowMessage(getkoDATA.Text);
////     ShowMessage(xrpMEMO.Text);
////     ShowMessage(xrpDATA.Text);
//
//     gdIni  := TMemIniFile.Create(pat+'tmpnur1234.ini') ;
//     malipolinja.Insert(0,'[sekcia]');
//     gdIni.SetStrings(malipolinja);
//
//     tblNurUpd.Close;
//     tblNurUpd.ParamByName('nurid').AsString := gdIni.ReadString('sekcia','nur_id', '');
//     tblNurUpd.Open;
//
//     if(tblNurUpd.RecordCount=0)
//     then tblNurUpd.Insert
//     else tblNurUpd.Edit;
//
//     tblNurUpdID.AsString := gdIni.ReadString('sekcia','nur_id', '');
//     tblNurUpdREC_ID.AsString := mm.Strings[i];
//     try
//        tblNurUpdVID.AsString := gdIni.ReadString('sekcia','vid','');
//        tblNurUpdIME.AsString := gdIni.ReadString('sekcia','ime','');
//        tblNurUpdOPIS.AsString := gdIni.ReadString('sekcia','opis','');
//        tblNurUpdVISINA.AsString := gdIni.ReadString('sekcia','visina','');
//        tblNurUpdSIRINA.AsString := gdIni.ReadString('sekcia','sirina','');
//        tblNurUpdSEL.AsString := nurko_select.Text;
//        tblNurUpd.Post;
//     Except
//     end;
//     malipolinja.Clear;
//     nurko_select.Clear;
//  end;
//  //ShowMessage('gotovo');
end;

procedure TfrmRepNurSinhro.ooRing();
var
  slis:TStringList;
  s1,mURL:string;
  Fmt: TFormatSettings;
  emb:String;
begin
//  data := TStringList.Create;
//    s1 := dmKon.AptShifra+':'+dmKon.ffaksimil;
//  mURL := StringReplace(dmKon.url('pacOsUrl'),'korko:lozko',s1,[])+'pacient.php?embg=';
//  fmt.ShortDateFormat:='yyyy-mm-dd';
//  fmt.DateSeparator  :='-';
//  fmt.LongTimeFormat :='hh:nn:ss';
//
//  tbloo.Open;
//  tbloo.First;
//  repeat
//    try
//      data.Text := scGetInetString(mURL+tblooEMBG.AsString);
//      tbloo.Edit;
//      emb:=data.Values['embg'];
//      if(Length(emb)=13)then
//      if(StrComp(PChar(emb), PChar(tblooEMBG.AsString))=0)then
//      begin
//        tblooBZL.AsString := data.Values['bzl'];
//        slis := TStringList.Create;
//        slis.Delimiter:='-';
//        slis.DelimitedText:= data.Values['bzl'];
//        tblooZ_FOND.AsString := slis.Strings[0];
//        tblooZ_BROJ.AsString := slis.Strings[1];
//        tblooZ_CLEN.AsString := slis.Strings[2];
//        slis.Free;
//        tblooEZBO.AsString := data.Values['EZBO'];
//        tblooDATUM_RAGJANJE.AsDateTime :=  StrToDate(data.Values['datum_ragjanje'],Fmt);
//        tblooLEKAR.AsString := data.Values['faksimil_opstLekar'];
//        tblooFAKSIMIL_STOMATOLOG.AsString := data.Values['faksimil_stomatolog'];
//        tblooFAKSIMIL_GINEKOLOG.AsString  := data.Values['faksimil_ginekolog'];
//        tblooADRESA.AsString := data.Values['adresa'];
//        tblooOSNOV_NA_OSIGURUVANJE.AsString := data.Values['osnov_na_osiguruvanje'];
//        tblooPRAVO_NA_OSIGURUVANJE.AsString := data.Values['pravo_na_osiguruvanje'];
//        tblooVAZNOST_NA_OSIGURUVANJE.AsDateTime := StrToDate(data.Values['vaznost_na_osiguruvanje'],Fmt);
//        tblooNEMAGO.AsInteger := 0;
//      end
//      else
//      begin
//        if(tblooNEMAGO.IsNull)then
//          tblooNEMAGO.AsInteger:=0;
//        tblooNEMAGO.AsInteger := tblooNEMAGO.AsInteger+1;
//      end;
//      tbloo.Post;
//    except
//       //
//    end;
//    tbloo.Next;
//  until tbloo.Eof;
//  data.free;
//  //ShowMessage('gotovo');
end;

procedure TfrmRepNurSinhro.logRing(ootime:Int64);
var
  s1,mURL:string;
  emb:String;
  rez:string;
  abudaba:string;
  ip:string;
begin
//  //dmKon.BackupBazaZ;
//
//  Randomize;
//  abudaba:=IntToStr(Random(10000));
//  ip:=Trim(Ips[0]);     //Localna adresa deklarirana uWebServis
//  rez:='o';
//  s1 := dmKon.AptShifra+':'+dmKon.ffaksimil;
//  mURL := StringReplace(dmKon.url('pacOsUrl'),'korko:lozko',s1,[]);
//  mURL := mURL+'izvrt_log.php?ootime='+IntToStr(ootime)+'&appver='+getVersion()+'&ip='+ip+'&HostName='+HostName;
//  mURL := mURL+'&aptid='+dmKon.AptShifra+'&farma='+dmKon.ffaksimil+'&abba='+abudaba;
////ShowMessage(mURL);
//  try
//    rez:=scGetInetString(mURL);
//  except
//   // ShowMessage('rez'); //
//  end;
//  //ajdee
end;

function TfrmRepNurSinhro.getVersion():string;
var
  strFileName:String;          // Name of the file to check
begin
//  strFileName := paramStr( 0 );
 // Result := GetFileInfo(strFileName,'FileVersion');
end;

procedure TfrmRepNurSinhro.PrezemiCenovnik;
var
  cml:string;
  konstring:String;
  fts:String;
  url:String;
  csvred:String;
  i,bojko:Integer;
  ss:TStringList;
  cfko,zapko:String;
  zpatekFile:String;
  status: TStatusWindowHandle;
begin
//try
//   status := cxCreateStatusWindow();
//   tblS.Close;
//   tblS.ParamByName('vreme').AsDate:= Date;
//   tbl.Open;
//   tblS.Open;
//
//
//   //avtomatsko izvrsuvawe na Procedurata za Izvrsi korekcija, pri Kontrola na iznosi
//   //25.06.2019
//   pOnaduj.ParamByName('godina').AsInteger := YearOf(Now);
//   pOnaduj.ParamByName('mesec').AsInteger := MonthOf(Now);
//   pOnaduj.ParamByName('onaduj').AsInteger :=1;
//   pOnaduj.ExecProc;
//
//   konstring:= dmKon.fibBaza.DatabaseName;
////   dmKon.CloseTables;
////   dmKon.fibBaza.Connected:=false;                                              //vo narednite ver.kese skrie
////  if(tblID.AsInteger=1)then
////        frmDaNe := TfrmDaNe.Create(self,'��������� �� ��������� ���� �� ��������, �� ��� '+DateToStr(Date) ,
////                                        '���� �� ����������� �� �� ��������� ����������� �������� �� �������� �� ������ - ���������?'+#10#13+ chr(10)+'��� ��������� � ���� ����� �� ����� �� ��� ���!'+#10#13+ chr(10)+'������ �� ���� ��������� � �������� ��������! ', 1);
//////  else
//////        frmDaNe := TfrmDaNe.Create(self,'���������� �� �������� �� ��������� ������.',
//////                                        '��� �������� �� ���� ��������� � ���������� ���� ������ �� �������� �� ���������!', 1);
//
////  if (frmDaNe.ShowModal = mrYes) then
////  begin
//      tblS.Close;
//      ss := TStringList.Create;
//      tblZastit.Close;
//      tblZastit.ParamByName('cenovnik').AsInteger := tblID.AsInteger;
//      tblZastit.Open;
//      tblZastit.First;
//      ss.clear;
//      bojko:=1;
//      while not tblZastit.Eof do
//      begin
//         //mt2_ceni( cena_id, next_id, cenovnik, artvid, artikl, cena, danok, akciza, marza, cenanab, ts_od, ts_do, iznosmarza, participacija, doplata, recver)
//         //values('1000000162',null,'1','1','18953','92.9985','5','0','0','92.9985','2011-08-01 00:00:00',null,'12.00','15.00','0.00','2012-01-30 13:18:38' );
//         csvred:='update or insert into mt2_ceni( cena_id, next_id, cenovnik, artvid, artikl, cena, danok, akciza, marza, cenanab, ts_od, ts_do, iznosmarza, participacija, doplata, recver) values(''';
//         zapko:= ''',''';
//         for i:=1 to tblZastit.FieldsCount do
//         begin
//             if(i= tblZastit.FieldsCount) then zapko:=''');';
//             cfko:= StringReplace(tblZastit.FieldByFieldNo(i).AsString,',','.',[rfReplaceAll]);
//             csvred:=csvred+cfko+zapko;
//         end;
//         csvred := StringReplace(csvred, '''''', 'null',[rfReplaceAll]);
//         ss.Add(csvred);
//         bojko:=bojko+1;
//         if(bojko=100)then
//         begin
//            bojko:=1;
//            ss.Add('commit work;');
//         end;
//
//         tblZastit.Next;
//      end;
//      ss.Add('commit work;');
////oooo      zpatekFile := GetEnvironmentVariable(PChar('TEMP'))+'\CodExComputers\zastita\';
//      zpatekFile := dmKon.StartDir+'cxCeniZastita\';
//      if(not system.SysUtils.DirectoryExists(zpatekFile)) then  System.SysUtils.CreateDir(zpatekFile);
//      if(tblID.AsInteger=1)then
//          zpatekFile := zpatekFile+'PozlistaZas-'+dmKon.fnTSstr()+'.sql'
//      else
//          zpatekFile := zpatekFile+'Cenovnik-0'+tblID.AsString+'-'+dmKon.fnTSstr()+'.sql';
//
//      ss.SaveToFile(zpatekFile);
//      if( System.SysUtils.FileExists( zpatekFile )) then
//      begin
//           pripremaZaCenovnik(tblID.AsString);
//
//           if(tblID.AsInteger=1)then
//           begin  // Zapiranje na pozitivnata lista
//              qZap.ParamByName('cenovnik').AsInteger:=tblID.AsInteger;
//              qZap.ExecQuery;
//              qZap.Transaction.CommitRetaining;
//
//              fts:=ftmpdir+'mt2_ceni_poz_lista.sql';
//              url:=dmKon.url('updServer.00')+'/mt2_ceni_poz_lista.update';
//           end
//           else
//           begin
//              fts:=ftmpdir+'mt2_ceni_0'+tblID.AsString+'.sql';
//              url:=dmKon.url('cenovnik0'+tblID.AsString)+'mt2_ceni_02.update';   //oleeleeee
//           end;
//
//           GetInetFile(url,fts);
//           fSkriptor := TfSkriptor.Create(Self);
//           try
//              fSkriptor.prvpat := 1;
//              fSkriptor.ExecuteFile(fts);
//           finally
//              fSkriptor.Free;
//           end;
//      end;
//
//      tblPrezCen.Insert;
//      tblPrezCen.Post;
//
//  finally
//      	cxRemoveStatusWindow(status);
//  end;
//

end;

procedure TfrmRepNurSinhro.pripremaZaCenovnik(cenovn:String);
var
  cml:string;
  konstring:String;
  fts:String;
  url:String;
  vari:String;
begin
//
//   konstring:= dmKon.fibBaza.DatabaseName;
//   url:=dmKon.url('cenovnik0'+cenovn);
//{
//   if(cenovn='2')then
//   begin   // zapatirawe na vtoriot cenovnik za Naris aptekite
//       vari:='mt2_ceni_02_zap';
//       fts:=ftmpdir+vari+'.sql';
//       GetInetFile(url+'/'+vari+'.update',fts);
//       dmKon.SkripTor(fts);
//       //cml:='cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251';
//       //ExecCmdLineAndWait('cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251', SW_SHOWNORMAL);
//   end;
//}
//
//   vari:='mat_opijati';
//   fts:=ftmpdir+vari+'.sql';
//   GetInetFile(url+'/'+vari+'.update',fts);
//
//   fSkriptor:=TfSkriptor.Create(Self);
//   try
//      fSkriptor.prvpat := 1;
//      fSkriptor.ExecuteFile(fts);
//   finally
//     fSkriptor.Free;
//   end;
//
//   vari:='pzz_rezim_prepis';
//   fts:=ftmpdir+vari+'.sql';
//   GetInetFile(url+'/'+vari+'.update',fts);
//   dmKon.SkripTor(fts);
////   cml:='cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251';
////   ExecCmdLineAndWait('cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251', SW_SHOWNORMAL);
//
//   vari:='mat_proizvoditel';
//   fts:=ftmpdir+vari+'.sql';
//   GetInetFile(url+'/'+vari+'.update',fts);
//   dmKon.SkripTor(fts);
////   cml:='cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251';
////   ExecCmdLineAndWait('cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251', SW_SHOWNORMAL);
//
//   vari:='pzz_generika';
//   fts:=ftmpdir+vari+'.sql';
//   GetInetFile(url+'/'+vari+'.update',fts);
//   dmKon.SkripTor(fts);
////   cml:='cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251';
////   ExecCmdLineAndWait('cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251', SW_SHOWNORMAL);
//
//   vari:='pzz_genericki_nazivi';
//   fts:=ftmpdir+vari+'.sql';
//   GetInetFile(url+'/'+vari+'.update',fts);
//   dmKon.SkripTor(fts);
////   cml:='cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251';
////   ExecCmdLineAndWait('cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251', SW_SHOWNORMAL);
//
//   vari:='ap7_pozitivni_artikli';
//   fts:=ftmpdir+vari+'.sql';
//   GetInetFile(url+'/'+vari+'.update?recver='+cenovn,fts);
//   dmKon.SkripTor(fts);
////   cml:='cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251';
////   ExecCmdLineAndWait('cxScript.exe '+fts+' -D"'+konstring+'" -USYSDBA -PRoot -V'+fts+'.errlog -N -CWIN_1251', SW_SHOWNORMAL);

end;

procedure TfrmRepNurSinhro.ListToJson();
var
  i: Integer;
  jsonPair: TJSONPair;
  jsonObject: TJSONObject;
  jsonArray: TJSONArray;
  url,GetPrescriptionRealized,resp:String;
  ss:TStringStream;
  code:integer;
  fsParamsRR: TStringList;
  json_st:string;
begin
//  jsonArray := TJSONArray.Create();
//  jsonObject := TJSONObject.Create();
//  jsonPair := TJSONPair.Create('',jsonArray);
//
//  tblIspratnici.Close;
//  tblIspratnici.Open;
//  tblIspratnici.First;
//
//  while not tblIspratnici.Eof do
//
//  begin
//    jsonObject := TJSONObject.Create();
//    jsonObject.AddPair(TJSONPair.Create('IZLEZID',    tblIspratniciIZLEZID.AsString));
//    jsonObject.AddPair(TJSONPair.Create('APTID_OD',   dmKon.AptShifra));
//    jsonObject.AddPair(TJSONPair.Create('DOKGOD',     tblIspratniciDOKGOD.AsString));
//    jsonObject.AddPair(TJSONPair.Create('DOKRE',      tblIspratniciDOKRE.AsString));
//    jsonObject.AddPair(TJSONPair.Create('DOKTIP',     tblIspratniciDOKTIP.AsString));
//    jsonObject.AddPair(TJSONPair.Create('DOKBROJ',    tblIspratniciDOKBROJ.AsString));
//    jsonObject.AddPair(TJSONPair.Create('PARTIP',     tblIspratniciPARTIP.AsString));
//    jsonObject.AddPair(TJSONPair.Create('PARSIF',     tblIspratniciPARSIF.AsString));
//    jsonObject.AddPair(TJSONPair.Create('DATUMIZLEZ', tblIspratniciDATUMIZLEZ.AsString));
//  //  jsonObject.AddPair(TJSONPair.Create('KORISNIKID', tblIspratniciKORISNIKID.AsString));
//    jsonObject.AddPair(TJSONPair.Create('TS_INS',     tblIspratniciTS_INS.AsString));
//    jsonObject.AddPair(TJSONPair.Create('TS_UPD',     tblIspratniciTS_UPD.AsString));
//    jsonObject.AddPair(TJSONPair.Create('APTID_DO',   tblIspratniciAPTID_DO.AsString));
//    jsonObject.AddPair(TJSONPair.Create('ARTVID',     tblIspratniciARTVID.AsString));
//    jsonObject.AddPair(TJSONPair.Create('ARTSIF',     tblIspratniciARTSIF.AsString));
//    jsonObject.AddPair(TJSONPair.Create('BARKOD',     tblIspratniciBARKOD.AsString));
//    jsonObject.AddPair(TJSONPair.Create('KOL',        tblIspratniciKOL.AsString));
//    jsonObject.AddPair(TJSONPair.Create('BRPAK',      tblIspratniciBRPAK.AsString));
//   // jsonObject.AddPair(TJSONPair.Create('CENA',       tblIspratniciCENA.AsString));
//    jsonObject.AddPair(TJSONPair.Create('POPUST',     tblIspratniciPOPUST.AsString));
//    jsonObject.AddPair(TJSONPair.Create('DDV',        tblIspratniciDDV.AsString));
//    jsonObject.AddPair(TJSONPair.Create('VCENA',      StringReplace(tblIspratniciVCENA.AsString,',','.',[])));
//    jsonObject.AddPair(TJSONPair.Create('ROK',        tblIspratniciROK.AsString));
//
//    jsonArray.AddElement(jsonObject);
//
//      qInsert.Close;
//      qInsert.ParamByName('ts_ins_isp').AsString := tblIspratniciTS_INS.AsString;
//      qInsert.ParamByName('IZLEZID').Value := tblIspratniciIZLEZID.Value;
//      qInsert.ParamByName('KORISNIKID').AsString := dmKon.user;
//      qInsert.ExecQuery;
//
//      qUpdateIsp.Close;
//      qUpdateIsp.ParamByName('IZLEZID').AsString :=tblIspratniciIZLEZID.AsString;
//      qUpdateIsp.ExecQuery;
//
//    tblIspratnici.Next;
////    jsonObject.FREE;
//  end;
//              json_st := ReplaceStr(jsonPair.ToString,'"":','');
//            // ShowMessage(json_st);
//
//              url:=url_ml+'aptcensys/cs_wservices.php';
//
//              ss := TStringStream.Create('', TEncoding.UTF8);
//              fsParamsRR := TStringList.Create;
//              fsParamsRR.Add('action=GetIspratnica');
//              fsParamsRR.Add('json_string='+''+json_st+'');
//           //   fsParamsRR.Add('token='+DBModule.Setup7('token'));
//
//              dmKon.trajSi(true);
//              try
//                   idHTTP1.Request.Accept := 'application/json';
//                   IdHTTP1.Request.ContentType := 'application/x-www-form-urlencoded';
//                   idHTTP1.Request.ContentEncoding := 'utf-8';
//
//                   IdHTTP1.Post(url,fsParamsRR,ss);
//                   resp:=ss.DataString;
//                   code:=IdHTTP1.ResponseCode;
//                   if code=200 then
//                   begin
//                      //zapis vo log fajlot
////                      qInsert.Close;
////                      qInsert.ParamByName('ts_ins_isp').AsString := tblIspratniciTS_INS.AsString;
//////                      qInsert.ParamByName('ID_ISP').AsString := pMaxVlezBroj.FldByName['MAKS'].AsString;
////                      qInsert.ParamByName('KORISNIKID').AsString := dmKon.user;
////                      qInsert.ExecQuery;
//
//
//                   end
//              except
//                   on E: eIdHTTPProtocolexception do
//                   begin
//                 //   ShowMessage(E.ErrorMessage);
//                   end;
//              end;
//              dmKon.trajSi(false);
//              fsParamsRR.Free;

 end;

procedure TfrmRepNurSinhro.PubLagerAll;
begin
  //  pPubLagerAll.Close;
  //  pPubLagerAll.ExecProc;
end;

end.
