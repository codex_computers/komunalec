object fReporter: TfReporter
  Left = 179
  Top = 108
  Width = 750
  Height = 500
  Caption = #1056#1077#1087#1086#1088#1090' '#1076#1080#1079#1072#1112#1085#1077#1088
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 458
    Height = 473
    Align = alClient
    BevelOuter = bvLowered
    Caption = 'Panel1'
    TabOrder = 0
    object dxDBTreeView1: TdxDBTreeView
      Left = 1
      Top = 1
      Width = 456
      Height = 471
      ShowNodeHint = True
      AutoExpand = True
      RowSelect = True
      DisplayField = 'ID;NASLOV'
      KeyField = 'ID'
      ListField = 'NASLOV'
      ParentField = 'KOREN'
      RootValue = '1'
      SeparatedSt = ' - '
      RaiseOnError = True
      ShowRoot = False
      ReadOnly = True
      Indent = 19
      Align = alClient
      ParentColor = False
      Options = [trDBCanDelete, trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
      SortType = stText
      SelectedIndex = -1
      TabOrder = 0
      OnDblClick = aPregledExecute
      OnKeyPress = dxDBTreeView1KeyPress
    end
  end
  object pDizajn: TPanel
    Left = 458
    Top = 0
    Width = 284
    Height = 473
    Align = alRight
    BevelOuter = bvLowered
    Caption = 'pDizajn'
    TabOrder = 1
    object Panel2: TPanel
      Left = 1
      Top = 385
      Width = 282
      Height = 87
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'Panel2'
      TabOrder = 0
      object mUslovi: TcxDBMemo
        Left = 1
        Top = 1
        Width = 280
        Height = 85
        Align = alClient
        DataBinding.DataField = 'USLOVI'
        DataBinding.DataSource = dsReport
        ParentFont = False
        PopupMenu = ppMeni
        Style.StyleController = StyleController
        TabOrder = 0
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 282
      Height = 384
      Align = alClient
      BevelOuter = bvLowered
      Caption = 'Panel4'
      TabOrder = 1
      object cxDBMemo1: TcxDBMemo
        Left = 1
        Top = 270
        Width = 280
        Height = 113
        Align = alBottom
        DataBinding.DataField = 'SQL'
        DataBinding.DataSource = dsReport
        ParentFont = False
        PopupMenu = ppMeni
        Style.StyleController = StyleController
        TabOrder = 0
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 280
        Height = 269
        Align = alClient
        BevelOuter = bvLowered
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        PopupMenu = ppMeni
        TabOrder = 1
        object btnDizajn: TcxButton
          Left = 199
          Top = 234
          Width = 75
          Height = 25
          Action = aDizajn
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 13
          Colors.Normal = clBtnFace
          Colors.Hot = clYellow
          Colors.Pressed = clBlue
          LookAndFeel.Kind = lfUltraFlat
          LookAndFeel.NativeStyle = False
        end
        object btnPregled: TcxButton
          Left = 119
          Top = 234
          Width = 75
          Height = 25
          Action = aPregled
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 12
          Colors.Normal = clBtnFace
          Colors.Hot = clYellow
          Colors.Pressed = clBlue
          LookAndFeel.Kind = lfUltraFlat
          LookAndFeel.NativeStyle = False
        end
        object btnVidlivost: TcxButton
          Left = 7
          Top = 234
          Width = 75
          Height = 25
          Action = aVidlivost
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 11
          Colors.Normal = clBtnFace
          Colors.Hot = clYellow
          Colors.Pressed = clBlue
          LookAndFeel.Kind = lfUltraFlat
        end
        object eBroj: TcxDBTextEdit
          Left = 86
          Top = 40
          Width = 100
          Height = 21
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dsReport
          ParentFont = False
          PopupMenu = ppMeni
          TabOrder = 0
          OnKeyDown = eBrojKeyDown
        end
        object eNaslov: TcxDBTextEdit
          Left = 5
          Top = 157
          Width = 272
          Height = 21
          DataBinding.DataField = 'NASLOV'
          DataBinding.DataSource = dsReport
          ParentFont = False
          PopupMenu = ppMeni
          TabOrder = 4
          OnKeyDown = eBrojKeyDown
        end
        object eKoren: TcxDBTextEdit
          Left = 86
          Top = 64
          Width = 100
          Height = 21
          DataBinding.DataField = 'KOREN'
          DataBinding.DataSource = dsReport
          ParentFont = False
          PopupMenu = ppMeni
          TabOrder = 1
          OnKeyDown = eBrojKeyDown
        end
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 278
          Height = 35
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 7
          object DBNavigator1: TDBNavigator
            Left = 1
            Top = 1
            Width = 276
            Height = 33
            DataSource = dsReport
            Align = alClient
            TabOrder = 0
          end
        end
        object eUslov: TcxDBTextEdit
          Left = 5
          Top = 206
          Width = 272
          Height = 21
          DataBinding.DataField = 'VIDLIVOST'
          DataBinding.DataSource = dsReport
          ParentFont = False
          PopupMenu = ppMeni
          TabOrder = 6
          OnKeyDown = eBrojKeyDown
        end
        object eUslovi: TcxDBCheckBox
          Left = 5
          Top = 182
          Width = 272
          Height = 21
          Caption = #1055#1086#1112#1072#1074#1080' '#1119#1072#1084#1095#1077' '#1089#1086' '#1091#1089#1083#1086#1074#1080
          DataBinding.DataField = 'POJAVIUSLOVI'
          DataBinding.DataSource = dsReport
          ParentColor = False
          Properties.ValueChecked = 1
          Properties.ValueGrayed = 10
          Properties.ValueUnchecked = 0
          TabOrder = 5
          OnKeyDown = eBrojKeyDown
        end
        object StaticText1: TStaticText
          Left = 8
          Top = 42
          Width = 44
          Height = 17
          Caption = #1064#1080#1092#1088#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
        end
        object StaticText2: TStaticText
          Left = 8
          Top = 66
          Width = 39
          Height = 17
          Caption = #1050#1086#1088#1077#1085
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
        end
        object StaticText3: TStaticText
          Left = 8
          Top = 140
          Width = 46
          Height = 17
          Caption = #1053#1072#1089#1083#1086#1074
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
        end
        object eGrupa: TcxDBLookupComboBox
          Left = 86
          Top = 88
          Width = 187
          Height = 21
          DataBinding.DataField = 'TABELA_GRUPA'
          DataBinding.DataSource = dsReport
          Properties.DropDownAutoSize = True
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'TABELA_VID'
          Properties.ListColumns = <
            item
              FieldName = 'TABELA_VID'
            end
            item
              FieldName = 'OPIS'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dmMat.dsTabeliGrupa
          TabOrder = 2
          OnExit = eGrupaExit
        end
        object eBR: TcxDBTextEdit
          Left = 86
          Top = 112
          Width = 100
          Height = 21
          DataBinding.DataField = 'BR'
          DataBinding.DataSource = dsReport
          ParentFont = False
          PopupMenu = ppMeni
          TabOrder = 3
          OnKeyDown = eBrojKeyDown
        end
        object StaticText4: TStaticText
          Left = 8
          Top = 90
          Width = 38
          Height = 17
          Caption = #1043#1088#1091#1087#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 14
        end
        object StaticText5: TStaticText
          Left = 8
          Top = 114
          Width = 69
          Height = 17
          Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 15
        end
      end
    end
  end
  object dsReport: TDataSource
    DataSet = tReport
    Left = 184
    Top = 16
  end
  object ppMeni: TPopupMenu
    Left = 448
    Top = 256
    object mDizajn: TMenuItem
      Action = aDizajn
    end
    object mPregled: TMenuItem
      Action = aPregled
    end
    object N1: TMenuItem
      Action = aVidlivost
    end
  end
  object frRep: TfrReport
    Dataset = frdsRep
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 177
    Top = 173
    ReportForm = {19000000}
  end
  object frdsRep: TfrDBDataSet
    DataSource = dsp
    Left = 177
    Top = 141
  end
  object dsp: TDataSource
    DataSet = qp
    Left = 176
    Top = 240
  end
  object tReport: TSQLClientDataSet
    CommandText = 'select * from SYS_REPORT'#13#10'where tabela_grupa like :tg'
    Aggregates = <>
    Options = [poAllowCommandText]
    ObjectView = True
    Params = <
      item
        DataType = ftFixedChar
        Name = 'tg'
        ParamType = ptInput
      end>
    DBConnection = dmKon.Connection
    Left = 152
    Top = 16
    object tReportID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object tReportKOREN: TIntegerField
      FieldName = 'KOREN'
    end
    object tReportNASLOV: TStringField
      FieldName = 'NASLOV'
      Required = True
      Size = 50
    end
    object tReportSQL: TBlobField
      FieldName = 'SQL'
      Size = 1
    end
    object tReportDATA: TBlobField
      FieldName = 'DATA'
      Size = 1
    end
    object tReportXML: TBlobField
      FieldName = 'XML'
      Size = 1
    end
    object tReportUSLOVI: TBlobField
      FieldName = 'USLOVI'
      Size = 1
    end
    object tReportNASLOVI: TBlobField
      FieldName = 'NASLOVI'
      Size = 1
    end
    object tReportVIDLIVOST: TStringField
      FieldName = 'VIDLIVOST'
      Size = 100
    end
    object tReportPOJAVIUSLOVI: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'POJAVIUSLOVI'
    end
    object tReportUSLOVITEXT: TBlobField
      FieldName = 'USLOVITEXT'
      Size = 1
    end
    object tReportDINAMICKIUSLOVI: TBlobField
      FieldName = 'DINAMICKIUSLOVI'
      Size = 1
    end
    object tReportTABELA_GRUPA: TStringField
      FieldName = 'TABELA_GRUPA'
      FixedChar = True
      Size = 3
    end
    object tReportBR: TIntegerField
      FieldName = 'BR'
    end
  end
  object qp: TSQLClientDataSet
    Aggregates = <>
    Options = [poAllowCommandText]
    ObjectView = True
    Params = <>
    DBConnection = dmKon.Connection
    Left = 208
    Top = 240
  end
  object frDesigner1: TfrDesigner
    Left = 216
    Top = 144
  end
  object frDialogControls1: TfrDialogControls
    Left = 144
    Top = 344
  end
  object frUserDataset1: TfrUserDataset
    Left = 192
    Top = 344
  end
  object frRichObject1: TfrRichObject
    Left = 88
    Top = 352
  end
  object frCheckBoxObject1: TfrCheckBoxObject
    Left = 40
    Top = 368
  end
  object frShapeObject1: TfrShapeObject
    Left = 256
    Top = 352
  end
  object StyleControllerLookUp: TcxEditStyleController
    Style.BorderColor = clNavy
    Style.BorderStyle = ebsSingle
    Style.Color = 13948116
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clBlack
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = [fsBold]
    Style.HotTrack = True
    Style.LookAndFeel.Kind = lfUltraFlat
    Style.Shadow = True
    Left = 22
    Top = 57
  end
  object StyleController: TcxEditStyleController
    Style.BorderColor = clNavy
    Style.BorderStyle = ebsSingle
    Style.Color = clWhite
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clBlack
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.HotTrack = True
    Style.LookAndFeel.Kind = lfUltraFlat
    Style.Shadow = True
    Left = 62
    Top = 57
  end
  object ActionList1: TActionList
    Left = 488
    Top = 256
    object aVidlivost: TAction
      Caption = #1042#1080#1076#1083#1080#1074#1086#1089#1090
      OnExecute = aVidlivostExecute
    end
    object aPregled: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076
      OnExecute = aPregledExecute
    end
    object aDizajn: TAction
      Caption = #1044#1080#1079#1072#1112#1085
      OnExecute = aDizajnExecute
    end
  end
  object qryMaxBroj: TSQLQuery
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Name = 'tg'
        ParamType = ptInput
      end>
    SQL.Strings = (
      'select max(br) as maks from sys_report'
      'where tabela_grupa = :tg')
    SQLConnection = dmKon.Connection
    Left = 264
    Top = 16
    object qryMaxBrojMAKS: TIntegerField
      FieldName = 'MAKS'
    end
  end
  object qryNajdiSifra: TSQLQuery
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Name = 'tg'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'br'
        ParamType = ptInput
      end>
    SQL.Strings = (
      'select id from sys_report'
      'where tabela_grupa = :tg'
      'and br = :br')
    SQLConnection = dmKon.Connection
    Left = 264
    Top = 56
    object qryNajdiSifraID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
  end
  object frJPEGExport1: TfrJPEGExport
    Left = 312
    Top = 360
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    Left = 312
    Top = 328
  end
  object frRtfAdvExport1: TfrRtfAdvExport
    Left = 312
    Top = 296
  end
  object frBarCodeObject1: TfrBarCodeObject
    Left = 280
    Top = 136
  end
  object frChartObject1: TfrChartObject
    Left = 320
    Top = 136
  end
  object frRoundRectObject1: TfrRoundRectObject
    Left = 360
    Top = 128
  end
  object frCrossObject1: TfrCrossObject
    Left = 400
    Top = 136
  end
end
