object frmIzborTP: TfrmIzborTP
  Left = 0
  Top = 0
  Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
  ClientHeight = 566
  ClientWidth = 738
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 738
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 40
      Top = 30
      Width = 78
      Height = 13
      Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cbTipPartner: TcxLookupComboBox
      Left = 128
      Top = 27
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsTipPartner
      TabOrder = 0
      OnKeyDown = cbTipPartnerKeyDown
      Width = 257
    end
    object btPecati: TcxButton
      Left = 568
      Top = 30
      Width = 82
      Height = 25
      Action = aGrid
      LookAndFeel.NativeStyle = True
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 73
    Width = 738
    Height = 474
    Align = alClient
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 736
      Height = 472
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsSaldo
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1064#1092#1088#1072' '#1058#1055
          DataBinding.FieldName = 'TIP_PARTNER'
          Width = 22
        end
        object cxGrid1DBTableView1NAZIV_TIP_PARTNER: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1058#1055
          DataBinding.FieldName = 'NAZIV_TIP_PARTNER'
          Width = 98
        end
        object cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER'
          Width = 48
        end
        object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'NAZIV_PARTNER'
          Width = 249
        end
        object cxGrid1DBTableView1DOLZI: TcxGridDBColumn
          Caption = #1044#1086#1083#1078#1080
          DataBinding.FieldName = 'DOLZI'
          Width = 86
        end
        object cxGrid1DBTableView1POBARUVA: TcxGridDBColumn
          Caption = #1055#1086#1073#1072#1088#1091#1074#1072
          DataBinding.FieldName = 'POBARUVA'
          Width = 85
        end
        object cxGrid1DBTableView1SALDO: TcxGridDBColumn
          Caption = #1057#1072#1083#1076#1086
          DataBinding.FieldName = 'SALDO'
          Width = 86
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 547
    Width = 738
    Height = 19
    Panels = <
      item
        Text = 'F10 - '#1055#1077#1095#1072#1090#1080',Ctrl+E - '#1057#1085#1080#1084#1080' '#1074#1086' Excel, Esc - '#1048#1079#1083#1077#1079
        Width = 50
      end>
  end
  object ActionManager1: TActionManager
    Left = 520
    StyleName = 'Platform Default'
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aDizajner: TAction
      Caption = 'aDizajner'
      ShortCut = 24697
      OnExecute = aDizajnerExecute
    end
    object aSnimiExcel: TAction
      Caption = 'aSnimiExcel'
      ShortCut = 16453
      OnExecute = aSnimiExcelExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aGrid: TAction
      Caption = #1055#1086#1085#1072#1090#1072#1084#1091
      OnExecute = aGridExecute
    end
  end
  object tblSaldo: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '       kla.tip_partner,'
      '       mtp.naziv naziv_tip_partner,'
      '       kla.partner,'
      '       mp.naziv naziv_partner,'
      '       sum(kpg.iznos_vkupno) dolzi,'
      '       coalesce(sum((select sum(ku.iznos) from kom_uplati ku'
      
        '                                          where ku.presmetka_g_i' +
        'd=kpg.id and ku.flag=1)),0) pobaruva,'
      '  --  coalesce(sum(ku.iznos),0) pobaruva,'
      
        '       sum(kpg.iznos_vkupno)-coalesce(sum((select sum(ku.iznos) ' +
        'from kom_uplati ku'
      
        '                                                                ' +
        '     where ku.presmetka_g_id=kpg.id and ku.flag = 1 )),0) saldo'
      'from kom_presmetka_g kpg'
      'inner join kom_lokacii kl on kl.id=kpg.lokaciin_id'
      'inner join kom_lokacija_arhiva kla on kla.id_lokacija=kl.id'
      'inner join mat_tip_partner mtp on mtp.id=kla.tip_partner'
      
        'inner join mat_partner mp on mp.tip_partner=kla.tip_partner and ' +
        'mp.id=kla.partner'
      'where kpg.tip=1 and kla.tip_partner like :tp'
      
        '  and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1' +
        ',kla.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,k' +
        'la.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '    or ((encodedate(1,kpg.mesec,kpg.godina) >= encodedate(1,kla.' +
        'poc_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla' +
        '.poc_mesec is not null) ))'
      'group by 1,2,3,4'
      
        'having (sum(kpg.iznos_vkupno)-coalesce(sum((select sum(ku.iznos)' +
        ' from kom_uplati ku'
      '     where ku.presmetka_g_id=kpg.id and ku.flag=1 )),0)<>0)'
      '--or (sum(kpg.iznos_vkupno)-sum(ku.iznos)) is null')
    SelectSQL.Strings = (
      'select'
      '       kla.tip_partner,'
      '       mtp.naziv naziv_tip_partner,'
      '       kla.partner,'
      '       mp.naziv naziv_partner,'
      '       sum(kpg.iznos_vkupno) dolzi,'
      '       coalesce(sum((select sum(ku.iznos) from kom_uplati ku'
      
        '                                          where ku.presmetka_g_i' +
        'd=kpg.id and ku.flag=1)),0) pobaruva,'
      '  --  coalesce(sum(ku.iznos),0) pobaruva,'
      
        '       sum(kpg.iznos_vkupno)-coalesce(sum((select sum(ku.iznos) ' +
        'from kom_uplati ku'
      
        '                                                                ' +
        '     where ku.presmetka_g_id=kpg.id and ku.flag = 1 )),0) saldo'
      'from kom_presmetka_g kpg'
      'inner join kom_lokacii kl on kl.id=kpg.lokaciin_id'
      'inner join kom_lokacija_arhiva kla on kla.id_lokacija=kl.id'
      'inner join mat_tip_partner mtp on mtp.id=kla.tip_partner'
      
        'inner join mat_partner mp on mp.tip_partner=kla.tip_partner and ' +
        'mp.id=kla.partner'
      'where kpg.tip=1 and kla.tip_partner like :tp'
      
        '  and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1' +
        ',kla.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,k' +
        'la.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '    or ((encodedate(1,kpg.mesec,kpg.godina) >= encodedate(1,kla.' +
        'poc_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla' +
        '.poc_mesec is not null) ))'
      'group by 1,2,3,4'
      
        'having (sum(kpg.iznos_vkupno)-coalesce(sum((select sum(ku.iznos)' +
        ' from kom_uplati ku'
      '     where ku.presmetka_g_id=kpg.id and ku.flag=1 )),0)<>0)'
      '--or (sum(kpg.iznos_vkupno)-sum(ku.iznos)) is null')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 440
    oRefreshDeletedRecord = True
    object tblSaldoTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblSaldoNAZIV_TIP_PARTNER: TFIBStringField
      FieldName = 'NAZIV_TIP_PARTNER'
      Size = 101
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSaldoNAZIV_PARTNER: TFIBStringField
      FieldName = 'NAZIV_PARTNER'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSaldoDOLZI: TFIBBCDField
      FieldName = 'DOLZI'
      Size = 2
    end
    object tblSaldoPOBARUVA: TFIBBCDField
      FieldName = 'POBARUVA'
      Size = 2
    end
    object tblSaldoSALDO: TFIBBCDField
      FieldName = 'SALDO'
      Size = 2
    end
    object tblSaldoPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
  end
  object dsSaldo: TDataSource
    DataSet = tblSaldo
    Left = 480
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 360
    Top = 224
  end
end
