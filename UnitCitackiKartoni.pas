unit UnitCitackiKartoni;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, cxGraphics, Menus, StdCtrls,
  cxButtons, ComCtrls, cxGroupBox, cxRadioGroup, cxDropDownEdit, cxTextEdit,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxControls,
  cxContainer, cxEdit, ActnList, cxDBEdit, frxClass, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, cxGridCustomPopupMenu, cxGridPopupMenu, cxLookAndFeels, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, cxNavigator, System.Actions, frxDBSet, frxDesgn;

type
  TfrmCitackiKartoni = class(TForm)
    Panel1: TPanel;
    gbGrupno: TcxGroupBox;
    lblReon: TLabel;
    cxLookupCBReoni: TcxLookupComboBox;
    lblGodina: TLabel;
    cxComboBoxGodini: TcxComboBox;
    cxComboBoxMesec: TcxComboBox;
    lblMesec: TLabel;
    StatusBar1: TStatusBar;
    btPecati: TcxButton;
    cxButton2: TcxButton;
    ActionList1: TActionList;
    aGrupno: TAction;
    aPoedinecno: TAction;
    aPecati: TAction;
    aIzlez: TAction;
    aDizajner: TAction;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1LOKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZ: TcxGridDBColumn;
    cxGrid1DBTableView1ULICA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1REON: TcxGridDBColumn;
    cxGrid1DBTableView1CLENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1KNIGA: TcxGridDBColumn;
    cxGrid1DBTableView1REDEN_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1STAN: TcxGridDBColumn;
    cxGrid1DBTableView1DVOR: TcxGridDBColumn;
    cxGrid1DBTableView1PAUSAL: TcxGridDBColumn;
    cxGrid1DBTableView1BR_VODOMERI: TcxGridDBColumn;
    cxGrid1DBTableView1SOSTOJBA: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aOtvoriDatasetG: TAction;
    aOtvoriDatasetP: TAction;
    aExportExcel: TAction;
    frxDesigner1: TfrxDesigner;
    frMatriksReport: TfrxReport;
    qp: TfrxDBDataset;
    procedure aGrupnoExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPoedinecnoExecute(Sender: TObject);
    procedure OnKeyDownLookupAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure setGridForLokacii(tip_partner:String;partner:String);
    procedure Proveri(Sender:tobject);
    procedure aDizajnerExecute(Sender: TObject);
    procedure cxComboBoxMesecExit(Sender: TObject);
    procedure aOtvoriDatasetGExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gbGrupnoEnter(Sender: TObject);
    procedure aExportExcelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCitackiKartoni: TfrmCitackiKartoni;

implementation

uses dmKonekcija, dmMaticni, UnitDM1, Partner, Utils;

{$R *.dfm}

procedure TfrmCitackiKartoni.aDizajnerExecute(Sender: TObject);
begin
         frMatriksReport.LoadFromFile('CitackiKartoni.fr3');
         frMatriksReport.PrepareReport(true);
         frMatriksReport.DesignReport();
end;

procedure TfrmCitackiKartoni.aExportExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmCitackiKartoni.aGrupnoExecute(Sender: TObject);
begin
        frMatriksReport.LoadFromFile('CitackiKartoni.fr3');
        frMatriksReport.PrepareReport(true);
        frMatriksReport.ShowPreparedReport();
end;

procedure TfrmCitackiKartoni.aIzlezExecute(Sender: TObject);
begin
    Close;
end;

procedure TfrmCitackiKartoni.aOtvoriDatasetGExecute(Sender: TObject);
begin
        dm1.tblCitackiKartoni.Close;
        dm1.tblCitackiKartoni.ParamByName('reon').AsString:=cxLookupCBReoni.Text;
        dm1.tblCitackiKartoni.ParamByName('tip_partner').AsString:='%';
        dm1.tblCitackiKartoni.ParamByName('partner_id').AsString:='%';
        dm1.tblCitackiKartoni.ParamByName('god').AsString:=cxComboBoxGodini.Text;
        dm1.tblCitackiKartoni.ParamByName('mes').AsString:=cxComboBoxMesec.Text;
        dm1.tblCitackiKartoni.Open;
        dm1.tblVodSost.Close;
        dm1.tblVodSost.ParamByName('god').AsString:=cxComboBoxGodini.Text;
        dm1.tblVodSost.ParamByName('mes').AsString:=cxComboBoxMesec.Text;
        DM1.tblVodSost.Open;
end;

procedure TfrmCitackiKartoni.aPecatiExecute(Sender: TObject);
begin
        aGrupno.Execute
end;

procedure TfrmCitackiKartoni.aPoedinecnoExecute(Sender: TObject);
begin
        qp.RangeBegin := rbCurrent;
        qp.RangeEnd := reCurrent;

        frMatriksReport.LoadFromFile('CitackiKartoni.fr3');
        frMatriksReport.PrepareReport(true);
        frMatriksReport.ShowPreparedReport();

        qp.RangeBegin := rbFirst;
        qp.RangeEnd := reLast;
end;

procedure TfrmCitackiKartoni.cxComboBoxMesecExit(Sender: TObject);
begin
    Proveri(cxLookupCBReoni);
    Proveri(cxComboBoxGodini);
    Proveri(cxComboBoxMesec);
    aOtvoriDatasetG.Execute;
    ActiveControl:=cxGrid1;
end;

procedure TfrmCitackiKartoni.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm1.tblCitackiKartoni.Close;
end;

procedure TfrmCitackiKartoni.gbGrupnoEnter(Sender: TObject);
begin
  dm1.tblCitackiKartoni.Close;
end;

procedure TfrmCitackiKartoni.OnKeyDownLookupAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var kom : TWinControl;
begin    kom := Sender as TWinControl;
   case key of
        VK_RETURN:
        begin
            begin
//              if kom=cxComboBoxMesec then
//              begin
//                      Proveri(cxLookupCBReoni);
//                      Proveri(cxComboBoxGodini);
//                      Proveri(cxComboBoxMesec);
//                      aOtvoriDatasetG.Execute;
//                      ActiveControl:=cxGrid1;
//              end
              // else
                PostMessage(Handle, WM_NEXTDLGCTL,0,0);
            end;

        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;

end;

procedure TfrmCitackiKartoni.setGridForLokacii(tip_partner:String;partner:String);
begin
    DM1.TblKomLokaciiPoPartner.Close;
    DM1.TblKomLokaciiPoPartner.ParamByName('TIP_PARTNER').AsString := tip_partner;
    DM1.TblKomLokaciiPoPartner.ParamByName('PARTNER').AsString := partner;

    DM1.TblKomLokaciiPoPartner.Open;
end;

procedure TfrmCitackiKartoni.Proveri(Sender:tobject);
begin
    if (TEdit(Sender).Text='') or (TEdit(Sender).Text='0') then
    begin
      TEdit(Sender).SetFocus;
      Abort;
    end;
end;

end.
