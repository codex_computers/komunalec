unit dmReportUnit;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, frxClass, frxDesgn, frxDBSet,
  frxRich, frxExportXML;

type
  TdmReport = class(TDataModule)
    tblReportDizajn: TpFIBDataSet;
    tblReportDizajnSQL: TFIBBlobField;
    tblReportDizajnDATA: TFIBBlobField;
    tblSqlReport: TpFIBDataSet;
    qp: TfrxDBDataset;
    frxReport1: TfrxReport;
    tblDetail: TpFIBDataSet;
    frxDBDataset2: TfrxDBDataset;
    dsMaster: TDataSource;
    frxRichObject1: TfrxRichObject;
    tblDetailTuzeniSmetki: TpFIBDataSet;
    frxTblDetailTuzeniSmetki: TfrxDBDataset;
    frxXMLExport1: TfrxXMLExport;
    function frxDesigner1SaveReport(Report: TfrxReport;
      SaveAs: Boolean): Boolean;
    procedure qpClose(Sender: TObject);
    procedure qpFirst(Sender: TObject);
    procedure qpNext(Sender: TObject);
    procedure qpOpen(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Spremi(id: Integer; app:string);
    procedure BrisiStarReport(id: Integer);
  end;

var
  dmReport: TdmReport;

implementation

uses dmKonekcija;



{$R *.dfm}

procedure TdmReport.qpClose(Sender: TObject);
begin
//  frxDBDataset2.Close;
//  frxTblDetailTuzeniSmetki.Close;
end;

procedure TdmReport.qpFirst(Sender: TObject);
begin
//  frxDBDataset2.First;
//  frxTblDetailTuzeniSmetki.First;
end;

procedure TdmReport.qpNext(Sender: TObject);
begin
//  frxDBDataset2.Open;
//  frxTblDetailTuzeniSmetki.Open;
end;

procedure TdmReport.qpOpen(Sender: TObject);
begin
//  frxDBDataset2.Open;
//  frxTblDetailTuzeniSmetki.Open;
end;

function TdmReport.frxDesigner1SaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var template : TStream;
begin
  template := TMemoryStream.Create;
  template.Position := 0;
  dmReport.frxReport1.SaveToStream(template);
  dmReport.tblReportDizajn.Open;
  dmReport.tblReportDizajn.edit;
  (dmReport.tblReportDizajnDATA as TBlobField).LoadFromStream(template);
  dmReport.tblReportDizajn.Post;
end;

procedure TdmReport.Spremi(id :Integer ; app:string);
var
  RptStream: TStream;
  SqlStream: TStream;
begin
  frxReport1.Clear;
  frxReport1.Script.Clear;

  tblReportDizajn.Close;
  tblReportDizajn.Params.ParamByName('br').Value := id;
  tblReportDizajn.Params.ParamByName('app').Value:= app;
  tblReportDizajn.Open;

  tblSqlReport.Close;
  SqlStream := tblReportDizajn.CreateBlobStream(tblReportDizajnSQL , bmRead);
  tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);

  RptStream := tblReportDizajn.CreateBlobStream(tblReportDizajnDATA , bmRead);
  frxReport1.LoadFromStream(RptStream);
end;

procedure TdmReport.BrisiStarReport(id :Integer);
var
  SqlStream: TStream;
begin
  tblReportDizajn.Close;
  tblReportDizajn.Params.ParamByName('id').Value := id;
  tblReportDizajn.Open;

  tblSqlReport.Close;
  SqlStream := tblReportDizajn.CreateBlobStream(tblReportDizajnSQL , bmRead);
  tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);
end;

end.
