object frmProveriSostojba: TfrmProveriSostojba
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1074#1077#1088#1080' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088#1080
  ClientHeight = 412
  ClientWidth = 785
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 785
    Height = 412
    Align = alClient
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 783
      Height = 410
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.DSKomVodomeriSostojba
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnSorting = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1Column5: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'PARTNER_ID'
          HeaderAlignmentHorz = taCenter
          Width = 61
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'NAZIV'
          HeaderAlignmentHorz = taCenter
          Width = 108
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          Caption = #1056#1077#1076#1077#1085' '#1073#1088'. '#1074#1086' '#1095#1080#1090'. '#1082#1085#1080#1075#1072
          DataBinding.FieldName = 'CITACKA_KNIGA_RB'
          HeaderAlignmentHorz = taCenter
          Width = 61
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          Caption = #1051#1086#1082'. '#1072#1082#1090#1080#1074#1085#1072
          DataBinding.FieldName = 'LOKACIJA_AKTIVEN'
          HeaderAlignmentHorz = taCenter
          Width = 67
        end
        object cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn
          Caption = #1042#1086#1076#1086#1084#1077#1088
          DataBinding.FieldName = 'VODOMERI_ID'
          HeaderAlignmentHorz = taCenter
          Width = 62
        end
        object cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn
          Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          DataBinding.FieldName = 'STARA_SOSTOJBA'
          HeaderAlignmentHorz = taCenter
          Width = 61
        end
        object cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn
          Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          DataBinding.FieldName = 'NOVA_SOSTOJBA'
          HeaderAlignmentHorz = taCenter
          Width = 61
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1056#1072#1079#1083#1080#1082#1072
          DataBinding.FieldName = 'RAZLIKA'
          HeaderAlignmentHorz = taCenter
          Width = 62
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          HeaderAlignmentHorz = taCenter
          Width = 62
        end
        object cxGrid1DBTableView1MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
          HeaderAlignmentHorz = taCenter
          Width = 60
        end
        object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'REON_ID'
          Visible = False
        end
        object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
          DataBinding.FieldName = 'CITACKA_KNIGA'
          Visible = False
        end
        object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
          DataBinding.FieldName = 'LOKACIIN_ID'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Visible = False
        end
        object cxGrid1DBTableView1VODOMER_STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'VODOMER_STATUS'
          Visible = False
        end
        object cxGrid1DBTableView1PAUSAL: TcxGridDBColumn
          Caption = #1055#1072#1091#1096#1072#1083
          DataBinding.FieldName = 'PAUSAL'
          HeaderAlignmentHorz = taCenter
          Width = 62
        end
        object cxGrid1DBTableView1AVTOMATSKI_VNES: TcxGridDBColumn
          Caption = #1040#1074#1090#1086#1084#1072#1090#1089#1082#1080' '#1074#1085#1077#1089
          DataBinding.FieldName = 'AVTOMATSKI_VNES'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
end
