unit LokaciiPoPartner;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, cxDBExtLookupComboBox,
  cxLabel, cxDBLabel, FIBQuery, pFIBQuery, cxVGrid, cxDBVGrid,
  cxInplaceContainer, FIBDataSet, pFIBDataSet, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxRibbonCustomizationForm, dxBarBuiltInMenu,
  cxNavigator, System.Actions, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmLokaciiPoPartner = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    pclokacija: TcxPageControl;
    tsLokacija: TcxTabSheet;
    tsLokacijaPartner: TcxTabSheet;
    LPanel: TPanel;
    DPanel: TPanel;
    gbLokacija: TcxGroupBox;
    gbPartner: TcxGroupBox;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    txtTipP1: TcxDBTextEdit;
    txtIdP1: TcxDBTextEdit;
    txtLokacija: TcxTextEdit;
    lblBrojCK: TcxDBLabel;
    lblAdresa: TcxDBLabel;
    lblReon: TcxDBLabel;
    aPromeniPartner: TAction;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton10: TdxBarLargeButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxVeritikal1DBTableView1: TcxDBVerticalGrid;
    cxVeritikal1DBTableView1DBEditorRow2: TcxDBEditorRow;
    cxVeritikal1DBTableView1DBEditorRow3: TcxDBEditorRow;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    tblPartner: TpFIBDataSet;
    tblPartnerID: TFIBIntegerField;
    tblPartnerNAZIV: TFIBStringField;
    tblPartnerADRESA: TFIBStringField;
    tblPartnerIME: TFIBStringField;
    tblPartnerPREZIME: TFIBStringField;
    tblPartnerTipPartnerNaziv: TStringField;
    tblPartnerMestoNaziv: TStringField;
    tblPartnerTIP_PARTNER: TFIBIntegerField;
    dsPartner: TDataSource;
    tblPartnerMESTO: TFIBIntegerField;
    cxButton1: TcxButton;
    aZapisiLok: TAction;
    txtTipP: TcxTextEdit;
    txtIdP: TcxTextEdit;
    cxGridViewRepository1: TcxGridViewRepository;
    repPartner: TcxGridDBTableView;
    repPartnerID: TcxGridDBColumn;
    repPartnerNAZIV: TcxGridDBColumn;
    repPartnerTIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_LOKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1POC_MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1POC_GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1KRAJ_MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1KRAJ_GODINA: TcxGridDBColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxVeritikal1DBTableView1DBEditorRow1: TcxDBEditorRow;
    aAzurirajP: TAction;
    aBrisiP: TAction;
    qImaPresmetka: TpFIBQuery;
    dxBarLargeButton18: TdxBarLargeButton;
    cxButton2: TcxButton;
    cbPartner: TcxExtLookupComboBox;
    tblPartnerNAZIV_TP: TFIBStringField;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aPromeniPartnerExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aZapisiLokExecute(Sender: TObject);
    procedure cbPartnerPropertiesPopup(Sender: TObject);
    procedure aAzurirajPExecute(Sender: TObject);
    procedure aBrisiPExecute(Sender: TObject);
    function ImaPresmetka:Integer;
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;

   var godina, mesec :integer; old_tp,old_p,new_tp,new_p:string;
  end;

var
  frmLokaciiPoPartner: TfrmLokaciiPoPartner;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmMaticni,
  dmSystem, UnitDM1;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmLokaciiPoPartner.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmLokaciiPoPartner.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmLokaciiPoPartner.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmLokaciiPoPartner.aAzurirajPExecute(Sender: TObject);
begin
  qImaPresmetka.Close;
  qImaPresmetka.ParamByName('lokacija').AsString:=txtLokacija.text;
  qImaPresmetka.ParamByName('tp').AsString:=txtTipP.text;
  qImaPresmetka.ParamByName('p').AsString:=txtIdP.text;
  qImaPresmetka.ExecQuery;
  if qImaPresmetka.FldByName['da'].IsNull then
  begin
    ShowMessage('��� ��� ��������� �� ��������� �� ��������� �������!');
    Abort;
  end
  else
  begin
    gbPartner.Enabled:=True;
    dm1.tblLokacijaArhiva.Edit;
    txtTipP.SetFocus;
  end;
end;

procedure TfrmLokaciiPoPartner.aBrisiExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmLokaciiPoPartner.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmLokaciiPoPartner.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmLokaciiPoPartner.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmLokaciiPoPartner.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmLokaciiPoPartner.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmLokaciiPoPartner.cxDBTextEditAllEnter(Sender: TObject);
begin
 if Sender=TcxLookupComboBox(cbPartner) then
   begin
//     tblPartner.close;
//     tblPartner.ParamByName('tp').AsString:='%';
//     tblPartner.ParamByName('p').AsString:='%';
//     tblPartner.open;

     if (txtTipP.Text<>'') and (txtIdP.Text<>'') then
      begin
       //  if tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTipP.Text,txtIdP.Text]),[]) then
//             tblPartner.Filter:='TIP_PARTNER='+QuotedStr(txtTipP.text);
//             tblPartner.Filter:='ID='+QuotedStr(txtIdP.text);
//             tblPartner.Filtered:=True;
//             if  tblPartnerNAZIV.Value='' then
//             begin
//               ShowMessage('�� ������ ������� �� ��������� �����!');
//               txtTipP.SetFocus;
//             end
//             else
              if tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTipP.Text,txtIdP.Text]),[]) then
              begin
                 //repPartner.Focused:=True;
                 cbPartner.Text:=repPartnerNAZIV.EditValue; //tblPartnerNAZIV.Value;
              end;
     end;
//      else
//      begin
//        txtTipP.Text:=tblPartnerTIP_PARTNER.AsString;
//        txtIdP.Text:=tblPartnerID.AsString;
//      end;
   end;
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmLokaciiPoPartner.cxDBTextEditAllExit(Sender: TObject);
begin
   if Sender=TcxLookupComboBox(cbPartner) then
   begin
     if (ActiveControl=txtTipP) or (ActiveControl=txtIdP) then
         cbPartner.Clear
     else
     if  (cbPartner.Text<>'') then  //(dm1.tblSmetkaNAZIV_PARTNER.AsString<>'') or
         begin
            txtTipP.Text:=tblPartnerTIP_PARTNER.AsString;
            txtIdP.Text:=tblPartnerID.AsString;
          end
          else
            if  (cbPartner.Text='') and (txtIdP.Text<>'')  then
              begin
                 cbPartner.SetFocus;
                 Abort;
              end;
         // TEdit(Sender).Color:=clWhite;
          //txtZabeleska.SetFocus;
   end;
   if Sender=TcxTextEdit(txtLokacija) then
      if (txtLokacija.Text<>'') then
       begin
            DM1.tblLokacija.close;
            dm1.tblLokacija.ParamByName('lokacija').AsString:=txtLokacija.Text;
            dm1.tblLokacija.Open;
            if DM1.tblLokacija.IsEmpty then
            begin
                ShowMessage('��������� ������� �� ������!!');
                txtLokacija.Clear;
                txtLokacija.SetFocus;
                Abort;
            end;
            dm1.tblLokacijaArhiva.Close;
            dm1.tblLokacijaArhiva.ParamByName('lokacija').AsString:=txtLokacija.Text;
            DM1.tblLokacijaArhiva.Open;

            txtTipP.Text:=DM1.tblLokacijaTIP_PARTNER.AsString;
            txtIdP.Text:=DM1.tblLokacijaPARTNER_ID.AsString;

            tblPartner.close;

           // tblPartner.Filter:='TIP_PARTNER='+QuotedStr(txtTipP.text);
           // tblPartner.Filter:='ID='+QuotedStr(txtIdP.text);
           // tblPartner.Filtered:=True;
            //tblPartner.ParamByName('tp').AsString:=txtTipP.text;
           // tblPartner.ParamByName('p').AsString:=txtIdP.text;
            tblPartner.open;
            dsPartner.Enabled:=true;

            if tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTipP.Text,txtIdP.Text]),[]) then
            cbPartner.Text:=tblPartnerNAZIV.AsString;
            
//            else
//            begin
//                DM1.tblLokacijaArhiva.Close;
//                DM1.tblLokacijaArhiva.ParamByName('lokacija').AsString:=txtLokacija.Text;
//                dm1.tblLokacijaArhiva.Open;
//            end;
       end
       else
       begin
         txtLokacija.Clear;
         txtLokacija.SetFocus;
         Abort;
       end;


    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmLokaciiPoPartner.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
//   cbPartner.Text:=DM1.tblLokacijaArhivaNAZIV_PARTNER.Value;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmLokaciiPoPartner.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmLokaciiPoPartner.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmLokaciiPoPartner.prefrli;
begin
end;

procedure TfrmLokaciiPoPartner.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
          //  dm1.tblLokacija.close;
           // dm1.tblLokacijaArhiva.Close;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dm1.tblLokacija.close;
            dm1.tblLokacijaArhiva.Close;
            Action := caFree;
          end
          else Action := caNone;
    end
    else
    begin
      dm1.tblLokacija.close;
      dm1.tblLokacijaArhiva.Close;
    end;
end;
procedure TfrmLokaciiPoPartner.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
  pclokacija.ActivePage:=tsLokacija;
end;

//------------------------------------------------------------------------------

procedure TfrmLokaciiPoPartner.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
   // PrvPosledenTab(dPanel,posledna,prva);
   // dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    //dm1.tblPartner.close;
    pclokacija.ActivePage:=tsLokacija;
    txtLokacija.SetFocus;
end;
//------------------------------------------------------------------------------

procedure TfrmLokaciiPoPartner.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmLokaciiPoPartner.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmLokaciiPoPartner.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmLokaciiPoPartner.aZapisiLokExecute(Sender: TObject);
begin
  if  (cbPartner.Text<>'') then  //(dm1.tblSmetkaNAZIV_PARTNER.AsString<>'') or
         begin
           //������ ��������� PROC_KOM_LOK_ARHIVA
            dm1.pLokArhiva.close;
            dm1.pLokArhiva.ParamByName('new_tp').AsString:=txtTipP.text;
            dm1.pLokArhiva.ParamByName('new_p').AsString:=txtIdP.Text;
            dm1.pLokArhiva.ParamByName('old_tp').AsString:=old_tp;
            dm1.pLokArhiva.ParamByName('old_p').AsString:=old_p;
            dm1.pLokArhiva.ParamByName('lokacija').AsString:=txtLokacija.Text;
            dm1.pLokArhiva.ExecQuery;

            ShowMessage('������ ������� �� ��������� '+txtLokacija.text+' e '+cbPartner.text);

            //refresh
            dm1.tblLokacijaArhiva.Close;
            dm1.tblLokacijaArhiva.ParamByName('lokacija').AsString:=txtLokacija.Text;
            DM1.tblLokacijaArhiva.Open;

            gbPartner.Enabled:=false;
            ActiveControl:=cxVeritikal1DBTableView1;
          end
end;

procedure TfrmLokaciiPoPartner.cbPartnerPropertiesPopup(Sender: TObject);
begin
     tblPartner.Filtered:=false;
end;

//	����� �� ���������� �� �������
procedure TfrmLokaciiPoPartner.aOtkaziExecute(Sender: TObject);
begin
   if not txtLokacija.Focused then
   begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      //RestoreControls(dPanel);
      dm1.tblLokacija.Close;
      dm1.tblLokacijaArhiva.close;
      dsPartner.Enabled:=False;
      txtLokacija.Clear;
      txtTipP.Clear;
      txtIdP.Clear;
      cbPartner.Clear;
      gbPartner.Enabled:=False;
      gbLokacija.Enabled:=True;
      //cxVeritikal1DBTableView1DBEditorRow1.ws;
      txtlokacija.setfocus;
   end
  else
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end;
 // else

//  begin
    //  cxGrid1DBTableView1.DataController.DataSet.Cancel;
   //   RestoreControls(dPanel);
   //   dPanel.Enabled := false;
    //  lPanel.Enabled := true;
    //  cxGrid1.SetFocus;
 // end

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmLokaciiPoPartner.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmLokaciiPoPartner.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmLokaciiPoPartner.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmLokaciiPoPartner.aPromeniPartnerExecute(Sender: TObject);
begin
if ImaPresmetka=0 then
begin
if txtIdP.Text<>'' then
begin
    pclokacija.ActivePage:=tsLokacija;
    gbLokacija.Enabled:=False;
    tblPartner.Filtered:=False;
   // tblPartner.close;
   // tblPartner.ParamByName('tp').AsString:='%';
    //tblPartner.ParamByName('p').AsString:='%';
   // tblPartner.open;
     dsPartner.Enabled:=true;
    old_tp:=txtTipP.Text;
    old_p:=txtIdP.text;
    gbPartner.Enabled:=True;
    txtTipP.Clear;
    txtIdP.Clear;
    cbPartner.Clear;
    txtTipP.SetFocus;
end;
end
else
begin
  ShowMessage('�� ��������� �������, ���� ������� ���������. '+#10#13+' ������� �� � �����!');
  Abort;
end;
end;

procedure TfrmLokaciiPoPartner.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmLokaciiPoPartner.aBrisiPExecute(Sender: TObject);
begin
//  qImaPresmetka.Close;
//  qImaPresmetka.ParamByName('lokacija').AsString:=txtLokacija.text;
//  qImaPresmetka.ParamByName('tp').AsInteger:=dm1.tblLokacijaArhivaTIP_PARTNER.value;  //txtTipP.text;
//  qImaPresmetka.ParamByName('p').AsInteger:=dm1.tblLokacijaArhivaPARTNER.value; //txtIdP.text;
//  qImaPresmetka.ExecQuery;
  if ImaPresmetka=0 then
  begin
    ShowMessage('��� ��� ��������� �� ��������� �� ��������� �������!');
    Abort;
  end
  else
  begin
    frmDaNe := TfrmDaNe.Create(self,'������ �� ��������','���� ��� ������� ���� ������ �� �� ��������� ���������?',1);
    if (frmDaNe.ShowModal = mrYes) then
    begin
    dm1.tblLokacijaArhiva.Delete;
    dm1.tblLokacijaArhiva.FetchAll;
    DM1.tblLokacijaArhiva.FullRefresh;
    dm1.tblLokacijaArhiva.Edit;
    DM1.tblLokacijaArhivaKRAJ_MESEC.AsString:='';
    DM1.tblLokacijaArhivaKRAJ_GODINA.AsString:='';
    DM1.tblLokacijaArhiva.post;

    dm1.tblLokacija.Refresh;
    txtTipP.Text:=DM1.tblLokacijaTIP_PARTNER.AsString;
    txtIdP.Text:=DM1.tblLokacijaPARTNER_ID.AsString;

    tblPartner.close;
    tblPartner.open;

    dsPartner.Enabled:=true;

    if tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTipP.Text,txtIdP.Text]),[]) then
    cbPartner.Text:=tblPartnerNAZIV.AsString;
   end;
  end;
end;

procedure TfrmLokaciiPoPartner.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmLokaciiPoPartner.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmLokaciiPoPartner.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmLokaciiPoPartner.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

function TfrmLokaciiPoPartner.ImaPresmetka:Integer;
begin
  qImaPresmetka.Close;
  qImaPresmetka.ParamByName('lokacija').AsString:=txtLokacija.text;
  qImaPresmetka.ParamByName('tp').AsInteger:=dm1.tblLokacijaArhivaTIP_PARTNER.value;  //txtTipP.text;
  qImaPresmetka.ParamByName('p').AsInteger:=dm1.tblLokacijaArhivaPARTNER.value; //txtIdP.text;
  qImaPresmetka.ExecQuery;
  if qImaPresmetka.FldByName['da'].IsNull then
    Result:=0
  else Result:=1;
end;

end.
