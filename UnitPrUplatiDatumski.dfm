object FrmPrUplatiDatumski: TFrmPrUplatiDatumski
  Left = 0
  Top = 0
  Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1091#1087#1083#1072#1090#1080' '#1087#1086' '#1076#1072#1090#1091#1084
  ClientHeight = 641
  ClientWidth = 1069
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelGlaen: TPanel
    Left = 0
    Top = 126
    Width = 1069
    Height = 113
    Align = alTop
    BevelInner = bvLowered
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lblDatumDo: TLabel
      Left = 16
      Top = 35
      Width = 58
      Height = 13
      Caption = #1044#1072#1090#1091#1084' '#1076#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMenuHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblDatumOd: TLabel
      Left = 16
      Top = 12
      Width = 58
      Height = 13
      Caption = #1044#1072#1090#1091#1084' '#1086#1076
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMenuHighlight
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 16
      Top = 78
      Width = 59
      Height = 14
      Caption = #1050#1086#1088#1080#1089#1085#1080#1082
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMenuHighlight
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 56
      Width = 46
      Height = 14
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMenuHighlight
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxDateDatumOd: TcxDateEdit
      Left = 104
      Top = 7
      TabOrder = 0
      OnKeyDown = onKeyDownAll
      Width = 121
    end
    object cxDateDatumDo: TcxDateEdit
      Left = 104
      Top = 30
      TabOrder = 1
      OnKeyDown = onKeyDownAll
      Width = 121
    end
    object cbKorisnik: TcxLookupComboBox
      Left = 104
      Top = 74
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'NAZIV'
      Properties.ListColumns = <
        item
          FieldName = 'PARTNER'
        end
        item
          FieldName = 'USERNAME'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dsqryUser
      Properties.MaxLength = 30
      TabOrder = 3
      OnKeyDown = onKeyDownAll
      Width = 269
    end
    object btPrikazi: TcxButton
      Left = 400
      Top = 72
      Width = 136
      Height = 25
      Caption = #1055#1088#1080#1082#1072#1078#1080
      LookAndFeel.SkinName = 'Office2007Silver'
      TabOrder = 4
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btPrikaziClick
    end
    object cbGodina: TcxComboBox
      Left = 104
      Top = 52
      Properties.Items.Strings = (
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030')
      TabOrder = 2
      OnKeyDown = onKeyDownAll
      Width = 120
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 622
    Width = 1069
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'F11 - '#1055#1077#1095#1072#1090#1080', Esc - '#1048#1079#1083#1077#1079
  end
  object panelGrid: TPanel
    Left = 0
    Top = 239
    Width = 1069
    Height = 383
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 1065
      Height = 379
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 4
      ExplicitTop = -2
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.DSTblPrUplatiDatumski
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'IZNOS'
            Column = cxGrid1DBTableView1VRABOTEN
            DisplayText = #1057#1091#1084#1072
          end
          item
            Kind = skSum
            FieldName = 'IZNOS'
            Column = cxGrid1DBTableView1TIP_UPLATA
            DisplayText = #1057#1091#1084#1072
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,###.00'
            Kind = skSum
            FieldName = 'IZNOS'
            Column = cxGrid1DBTableView1IZNOS
            DisplayText = #1057#1091#1084#1072' '
          end
          item
            Column = cxGrid1DBTableView1DATUM_UPLATA
            DisplayText = #1057#1091#1084#1072
          end
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'IZNOS'
            Column = cxGrid1DBTableView1VRABOTEN
            DisplayText = #1075#1092#1075#1075
          end>
        DataController.Summary.SummaryGroups = <
          item
            Links = <
              item
                Column = cxGrid1DBTableView1VRABOTEN
              end
              item
                Column = cxGrid1DBTableView1TIP_UPLATA
              end>
            SummaryItems = <
              item
                Kind = skSum
                FieldName = 'IZNOS'
                Column = cxGrid1DBTableView1IZNOS
                DisplayText = #1057#1091#1084#1072
              end>
          end>
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupFooterMultiSummaries = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        OptionsView.GroupRowStyle = grsOffice11
        object cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085
          DataBinding.FieldName = 'VRABOTEN'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'USERNAME'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dsqryUser
          Visible = False
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 175
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          HeaderGlyphAlignmentHorz = taCenter
          Width = 189
        end
        object cxGrid1DBTableView1TIP_UPLATA: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
          DataBinding.FieldName = 'TIP_UPLATA'
          Visible = False
          GroupIndex = 1
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 87
        end
        object cxGrid1DBTableView1DATUM_UPLATA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1091#1087#1083#1072#1090#1072
          DataBinding.FieldName = 'DATUM_UPLATA'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 90
        end
        object cxGrid1DBTableView1IZNOS: TcxGridDBColumn
          Caption = #1048#1079#1085#1086#1089
          DataBinding.FieldName = 'IZNOS'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 139
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'ID'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 131
        end
        object cxGrid1DBTableView1ID_TU: TcxGridDBColumn
          DataBinding.FieldName = 'ID_TU'
          Visible = False
          HeaderGlyphAlignmentHorz = taCenter
          Width = 53
        end
        object cxGrid1DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Visible = False
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_NALOG: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072#1083#1086#1075
          DataBinding.FieldName = 'TIP_NALOG'
        end
        object cxGrid1DBTableView1NALOG: TcxGridDBColumn
          Caption = #1053#1072#1083#1086#1075
          DataBinding.FieldName = 'NALOG'
        end
        object cxGrid1DBTableView1TS_KNIZENJE: TcxGridDBColumn
          DataBinding.FieldName = 'TS_KNIZENJE'
          Visible = False
        end
        object cxGrid1DBTableView1DOKUMENT: TcxGridDBColumn
          Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          DataBinding.FieldName = 'DOKUMENT'
          Width = 104
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1069
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 3
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 1
    end
  end
  object ActionList1: TActionList
    Images = DM1.cxLargeImages
    Left = 520
    Top = 72
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 20
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      ShortCut = 122
      OnExecute = aPecatiExecute
    end
    object aDizajn: TAction
      Caption = 'aDizajn'
      ShortCut = 24697
      OnExecute = aDizajnExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 392
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44125.336542962960000000
      ReportTitle.Font.Charset = RUSSIAN_CHARSET
      ReportTitle.Font.Color = clBlack
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Tahoma'
      ReportTitle.Font.Style = [fsBold]
      ReportTitle.Text = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1091#1087#1083#1072#1090#1080' '#1087#1086' '#1076#1072#1090#1091#1084
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default'
      #1040#1082#1094#1080#1080)
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    ImageOptions.LargeImages = DM1.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 344
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 766
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 281
      DockedTop = 0
      FloatLeft = 766
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 766
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      LargeImageIndex = 39
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end>
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aPecatiTabela
      Caption = 'New Button'
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1080#1079#1075#1083#1077#1076' (F11)'
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aPecati
      Caption = #1055#1077#1095#1072#1090#1080' (F10)'
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object SubItemAkcii: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'BarButtonPecati'
        end>
    end
    object BarButtonIzlez: TdxBarButton
      Action = aIzlez
      Category = 1
    end
    object BarButtonPecati: TdxBarButton
      Action = aPecati
      Category = 1
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 464
    Top = 96
  end
  object qryUser: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    U.USERNAME,'
      '    U.TIP_PARTNER,'
      '    U.PARTNER,'
      '    U.NAZIV,'
      '    U.RE,'
      '    U.RABOTNA_EDINICA'
      'from view_user u'
      'inner join sys_user_menu m on m.username=u.username'
      'where m.aplikacija='#39'KOM'#39)
    SelectSQL.Strings = (
      'select'
      '    U.USERNAME,'
      '    U.TIP_PARTNER,'
      '    U.PARTNER,'
      '    U.NAZIV,'
      '    U.RE,'
      '    U.RABOTNA_EDINICA'
      'from view_user u'
      'inner join sys_user_menu m on m.username=u.username'
      'where m.aplikacija='#39'KOM'#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 512
    Top = 16
    poSQLINT64ToBCD = True
    object qryUserUSERNAME: TFIBStringField
      DisplayLabel = #1050#1086#1088'.'#1048#1084#1077
      DisplayWidth = 20
      FieldName = 'USERNAME'
      Size = 31
      Transliterate = False
      EmptyStrToNull = True
    end
    object qryUserTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object qryUserPARTNER: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object qryUserNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      DisplayWidth = 30
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object qryUserRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object qryUserRABOTNA_EDINICA: TFIBStringField
      FieldName = 'RABOTNA_EDINICA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsqryUser: TDataSource
    AutoEdit = False
    DataSet = qryUser
    Left = 480
    Top = 16
  end
  object ActionList2: TActionList
    Images = DM1.cxLargeImages
    Left = 368
    Top = 80
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 30
      ShortCut = 122
      OnExecute = aPecatiTabelaExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
  end
end
