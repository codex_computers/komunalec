object FrmKomPresmetka: TFrmKomPresmetka
  Left = 0
  Top = 0
  Caption = #1055#1088#1077#1089#1084#1077#1090#1082#1072' '#1085#1072' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
  ClientHeight = 459
  ClientWidth = 783
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelGoren: TPanel
    Left = 0
    Top = 0
    Width = 783
    Height = 184
    Align = alTop
    BevelInner = bvLowered
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lblMesec: TLabel
      Left = 43
      Top = 65
      Width = 36
      Height = 14
      Caption = #1052#1077#1089#1077#1094
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblGodina: TLabel
      Left = 43
      Top = 40
      Width = 40
      Height = 14
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblReon: TLabel
      Left = 43
      Top = 15
      Width = 28
      Height = 14
      Caption = #1056#1077#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxBtnPresmetaj: TcxButton
      Left = 314
      Top = 160
      Width = 123
      Height = 20
      Caption = #1055#1088#1077#1089#1084#1077#1090#1072#1112
      TabOrder = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = cxBtnPresmetajClick
    end
    object cxComboBoxGodini: TcxComboBox
      Left = 157
      Top = 36
      BeepOnEnter = False
      TabOrder = 1
      OnKeyDown = onKeyDownLookUpAll
      Width = 94
    end
    object cxComboBoxMesec: TcxComboBox
      Left = 157
      Top = 61
      BeepOnEnter = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
      TabOrder = 2
      OnKeyDown = onKeyDownLookUpAll
      Width = 94
    end
    object GroupBoxPoslednaPresmetka: TGroupBox
      Left = 598
      Top = 2
      Width = 183
      Height = 180
      Align = alRight
      Caption = #1055#1086#1089#1083#1077#1076#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      TabOrder = 5
      Visible = False
      object lblMesecLast: TLabel
        Left = 16
        Top = 40
        Width = 31
        Height = 13
        Caption = #1052#1077#1089#1077#1094
      end
      object lblMesecLastValue: TLabel
        Left = 80
        Top = 40
        Width = 53
        Height = 13
        Caption = '%'#1052#1077#1089#1077#1094'%'
      end
      object lblGodinaLast: TLabel
        Left = 16
        Top = 15
        Width = 37
        Height = 13
        Caption = #1043#1086#1076#1080#1085#1072
      end
      object lblGodinaLastValue: TLabel
        Left = 80
        Top = 15
        Width = 59
        Height = 13
        Caption = '%'#1043#1086#1076#1080#1085#1072'%'
      end
      object cxBtnIzbrisiPoslednaPresmetka: TcxButton
        Left = 8
        Top = 65
        Width = 172
        Height = 25
        Action = aIzbrisiLastPresmetka
        Caption = #1048#1079#1073#1088#1080#1096#1080' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
        Enabled = False
        TabOrder = 0
        Visible = False
      end
    end
    object cxLookupCBReoni: TcxLookupComboBox
      Left = 106
      Top = 11
      BeepOnEnter = False
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          Width = 100
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1080#1074
          FieldName = 'NAZIV'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = DM1.DSKomReoni
      TabOrder = 0
      OnKeyDown = onKeyDownLookUpAll
      Width = 145
    end
    object cxRadioGroup1: TcxRadioGroup
      Left = 43
      Top = 98
      Caption = ' '#1055#1077#1095#1072#1090#1077#1114#1077' '#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1080' '#1079#1072' ..'
      ParentFont = False
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077
          Value = -1
        end
        item
          Caption = #1060#1080#1079#1080#1095#1082#1080' '#1083#1080#1094#1072
          Value = 5
        end
        item
          Caption = #1044#1091#1116#1072#1085#1080
          Value = 6
          Tag = 2
        end
        item
          Caption = #1060#1080#1088#1084#1080
          Value = 1
        end>
      Style.BorderStyle = ebsFlat
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfFlat
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfFlat
      TabOrder = 3
      Height = 80
      Width = 208
    end
    object cxMemo1: TcxMemo
      Left = 388
      Top = 19
      Lines.Strings = (
        'cxMemo1')
      ParentFont = False
      Properties.ScrollBars = ssBoth
      Properties.WordWrap = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 6
      Visible = False
      Height = 73
      Width = 390
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 440
    Width = 783
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 
      'F9 - '#1055#1077#1095'. '#1073#1077#1083'. '#1079#1072' '#1086#1076#1073'., F11 - '#1055#1077#1095'. '#1073#1077#1083'. '#1079#1072' '#1089#1080#1090#1077', Ctrl+M - '#1041#1077#1083'.'#1087#1086 +
      ' '#1077'-'#1084#1072#1080#1083','#13#10' ESC - '#1048#1079#1083#1077#1079
  end
  object panelDolen: TPanel
    Left = 0
    Top = 184
    Width = 783
    Height = 256
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 779
      Height = 252
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.DSPresmetka
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'IZNOS_VKUPNO'
            Column = cxGrid1DBTableView1IZNOS_VKUPNO
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'LOKACIIN_ID'
          HeaderAlignmentHorz = taCenter
          Width = 62
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'PARTNER_ID'
          HeaderAlignmentHorz = taCenter
          Width = 46
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          HeaderAlignmentHorz = taCenter
          Width = 135
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089#1072
          DataBinding.FieldName = 'ADRESA'
          HeaderAlignmentHorz = taCenter
          Width = 99
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
          DataBinding.FieldName = 'CITACKA_KNIGA'
          HeaderAlignmentHorz = taCenter
          Width = 82
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
          DataBinding.FieldName = 'BROJ_FAKTURA'
          HeaderAlignmentHorz = taCenter
          Width = 94
        end
        object cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn
          Caption = #1048#1079#1085#1086#1089' '#1074#1082#1091#1087#1085#1086
          DataBinding.FieldName = 'IZNOS_VKUPNO'
          HeaderAlignmentHorz = taCenter
          Width = 77
        end
        object cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085
          DataBinding.FieldName = 'VRABOTEN'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 67
        end
        object cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
          DataBinding.FieldName = 'DATUM_PRESMETKA'
          HeaderAlignmentHorz = taCenter
          Width = 115
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dbimgIMG: TDBImage
    Left = 450
    Top = -6
    Width = 105
    Height = 105
    DataField = 'IMG'
    DataSource = DM1.DSPresmetka
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
  end
  object ActionList1: TActionList
    Left = 456
    Top = 24
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aIzbrisiLastPresmetka: TAction
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1087#1086#1089#1083#1077#1076#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      ShortCut = 119
      OnExecute = aIzbrisiLastPresmetkaExecute
    end
    object aCallFrfBeleskiPrint: TAction
      Caption = 'aCallFrfBeleskiPrint'
      ShortCut = 24697
      OnExecute = aCallFrfBeleskiPrintExecute
    end
    object aPrintAllBeleskiFizicki: TAction
      Caption = 'aPrintAllBeleskiFizicki'
      OnExecute = aPrintAllBeleskiFizickiExecute
    end
    object aPrintBeleskaOdberenFizicki: TAction
      Caption = 'aPrintBeleskaOdberenFizicki'
      OnExecute = aPrintBeleskaOdberenFizickiExecute
    end
    object aPrintAllFakturi: TAction
      Caption = 'aPrintAllFakturi'
      ShortCut = 115
      OnExecute = aPrintAllFakturiExecute
    end
    object aEdnaFaktura: TAction
      Caption = 'aEdnaFaktura'
      ShortCut = 121
      OnExecute = aEdnaFakturaExecute
    end
    object aMailFaktura: TAction
      Caption = 'aMailFaktura'
      ShortCut = 16461
      OnExecute = aMailFakturaExecute
    end
    object aMailFakturaDizajn: TAction
      Caption = 'aMailFakturaDizajn'
      ShortCut = 24653
      OnExecute = aMailFakturaDizajnExecute
    end
    object aNovIzgledB: TAction
      Caption = 'aNovIzgledB'
      ShortCut = 120
      OnExecute = aNovIzgledBExecute
    end
    object aNovIzgledBDizajn: TAction
      Caption = 'aNovIzgledBDizajn'
      ShortCut = 24642
      OnExecute = aNovIzgledBDizajnExecute
    end
    object aNovIzgledSiteB: TAction
      Caption = 'aNovIzgledSiteB'
      ShortCut = 122
      OnExecute = aNovIzgledSiteBExecute
    end
  end
  object frxReport1: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39440.559617523100000000
    ReportOptions.LastChange = 41855.390382245400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      
        'var suma, suma_razlika,suma_r,ne,cena,suma_kan,cena1:Real; danok' +
        ':array of extended;                                '
      'procedure Memo2OnAfterPrint(Sender: TfrxComponent);'
      
        'var uslugaBefore, uslugaNext, lokacijaBefore, lokacijaNext :Stri' +
        'ng;'
      
        '       procentDanokPreviouse, danokPreviouse, iznosNetoPreviouse' +
        ':extended;                        '
      'begin           '
      '       begin'
      '              uslugaBefore := <frxDSPresmetkaS."NAZIV">;'
      '              lokacijaBefore := <frxDSPresmetkaS."LOKACIIN_ID">;'
      
        '              iznosNetoPreviouse := <frxDSPresmetkaS."IZNOS_USLU' +
        'GA_NETO">;                      '
      
        '              procentDanokPreviouse := <frxDSPresmetkaS."PROCENT' +
        '_DANOK">;'
      
        '              danokPreviouse := <frxDSPresmetkaS."DANOK">;      ' +
        '            '
      '              uslugaNext := <frxDSPresmetkaS."NAZIV">;'
      
        '              lokacijaNext := <frxDSPresmetkaS."LOKACIIN_ID">;  ' +
        '                      '
      
        '              if ((uslugaBefore = uslugaNext) and (lokacijaBefor' +
        'e = lokacijaNext)) then           '
      '              begin'
      
        '                       setVisibleUslugiPolinja(false);          ' +
        '                                                                ' +
        '                                '
      '              end'
      
        '       end;                                                     ' +
        '                                  '
      'end;'
      ''
      'procedure GroupFooter1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '       setVisibleUslugiPolinja(true);'
      '       suma_razlika:=0;'
      '       suma_r:=0;                                 '
      'end;'
      ''
      'procedure Memo7OnAfterPrint(Sender: TfrxComponent);'
      
        'var lokacijaPreviouse, lokacijaNext, vodomerPreviouse, vodomerNe' +
        'xt,fak:String;'
      
        '    novaSostojbaPreviouse, staraSostojbaPreviouse , IznosBezDDV:' +
        ' extended;                                                      ' +
        '                                                 '
      'begin'
      
        '       lokacijaPreviouse := <frxDSPresmetkaVodomeriSostojba."LOK' +
        'ACIIN_ID">;'
      
        '       vodomerPreviouse := <frxDSPresmetkaVodomeriSostojba."VODO' +
        'MERI_ID">;'
      
        '       novaSostojbaPreviouse := <frxDSPresmetkaVodomeriSostojba.' +
        '"NOVA_SOSTOJBA">;'
      
        '       staraSostojbaPreviouse := <frxDSPresmetkaVodomeriSostojba' +
        '."STARA_SOSTOJBA">;'
      '       fak:=<frxDSPresmetkaG."BROJ_FAKTURA">;'
      '       cena:=<frxDSPresmetkaS."CENA">;'
      
        '       danok:=[<frxDSPresmetkaStavki1."CENA_VODA">*(<frxDSPresme' +
        'tkaVodomeriSostojba."NOVA_SOSTOJBA">-<frxDSPresmetkaVodomeriSost' +
        'ojba."STARA_SOSTOJBA">)*<frxDSPresmetkaStavki1."DDV">/100];     ' +
        '                                                '
      
        '       IznosBezDDV:=<frxDSPresmetkaS."IZNOS_USLUGA_NETO">;      ' +
        '                                 '
      '         '
      '       mmVodomerId.DataSet.Next;'
      
        '       lokacijaNext := <frxDSPresmetkaVodomeriSostojba."LOKACIIN' +
        '_ID">;'
      
        '       vodomerNext := <frxDSPresmetkaVodomeriSostojba."VODOMERI_' +
        'ID">;  '
      '       suma_r:=suma_razlika;  '
      '       if (<printNextVodomer> <> 2) then         '
      
        '       begin                                                    ' +
        '                                        '
      
        '               Set('#39'totalPotrosenaVoda'#39',<totalPotrosenaVoda>+nov' +
        'aSostojbaPreviouse-staraSostojbaPreviouse);'
      
        '               suma_r:=suma_razlika;                            ' +
        '                                               '
      '       end;'
      '                 '
      
        '       if ( (<frxDSPresmetkaVodomeriSostojba."LOKACIIN_ID"> = '#39'0' +
        #39') and (<frxDSPresmetkaVodomeriSostojba."VODOMERI_ID"> = '#39'0'#39') ) ' +
        'then                                              '
      '       begin'
      '              Set('#39'printNextVodomer'#39',3);'
      '              suma_r:=suma_razlika;'
      
        '              cena:=0;                                          ' +
        '          '
      '       end'
      '       else'
      '       begin'
      '           if (vodomerPreviouse=vodomerNext) then'
      '              begin                           '
      
        '                       setVisibleVodomeriPolinja(false);        ' +
        '                                                                ' +
        '                                   '
      '                       Set('#39'printNextVodomer'#39',2);'
      
        '                       suma_r:=suma_r-(<frxDSPresmetkaVodomeriSo' +
        'stojba."NOVA_SOSTOJBA">-<frxDSPresmetkaVodomeriSostojba."STARA_S' +
        'OSTOJBA">);'
      
        '                       cena:=0;                                 ' +
        '        '
      '              end'
      '              else'
      '              begin'
      
        '                       setVisibleVodomeriPolinja(true);         ' +
        '                  '
      '                       Set('#39'printNextVodomer'#39',1);'
      '                       suma_r:=suma_razlika;'
      
        '                       cena:=<frxDSPresmetkaS."CENA">;          ' +
        '                 '
      '              end;'
      '       end;'
      'end;'
      ''
      'procedure Memo7OnBeforePrint(Sender: TfrxComponent);'
      
        'var printMemo:boolean;                                          ' +
        '     '
      'begin'
      '       suma_r:=suma_razlika;    '
      '       if (<printNextVodomer> = 3) then'
      '       begin'
      
        '      // suma_r:=suma_razlika;                                  ' +
        '                 '
      
        '       if ( (<frxDSPresmetkaVodomeriSostojba."LOKACIIN_ID"> = '#39'0' +
        #39') and (<frxDSPresmetkaVodomeriSostojba."VODOMERI_ID"> = '#39'0'#39') ) ' +
        'then                                              '
      '       begin'
      
        '              setVisibleVodomeriPolinja(false);                 ' +
        ' '
      '              Set('#39'printNextVodomer'#39',2);'
      '              suma_r:=0;                                    '
      '              cena:=0;                                '
      '       end          '
      '       else'
      '       begin'
      
        '               setVisibleVodomeriPolinja(true);                 ' +
        '  '
      '               Set('#39'printNextVodomer'#39',1);'
      '               suma_r:=suma_razlika;'
      
        '               cena:=<frxDSPresmetkaS."CENA">;                  ' +
        '               '
      '       end;'
      '       end;           '
      'end;'
      ''
      'procedure Memo1OnAfterData(Sender: TfrxComponent);'
      'begin'
      
        '       if ( (<frxDSPresmetkaVodomeriSostojba."LOKACIIN_ID"> = '#39'0' +
        #39') and (<frxDSPresmetkaVodomeriSostojba."VODOMERI_ID"> = '#39'0'#39') ) ' +
        'then           '
      '       begin                   '
      '               Set('#39'printNextVodomer'#39',2);'
      '               setVisibleVodomeriPolinja(false);'
      '               suma_r:=suma_razlika;'
      '               cena:=0;                                 '
      '                          end'
      '       else'
      '       begin'
      '               Set('#39'printNextVodomer'#39',1);'
      '               setVisibleVodomeriPolinja(true);'
      '               suma_r:=suma_razlika;'
      
        '               cena:=<frxDSPresmetkaS."CENA">;                  ' +
        '               '
      '       end               '
      'end;'
      ''
      
        'procedure GroupFooterVodomeriSostojbaOnBeforePrint(Sender: TfrxC' +
        'omponent);'
      'begin'
      '     Set('#39'totalPotrosenaVoda'#39',0);'
      'end;'
      ''
      'procedure setVisibleUslugiPolinja(visible:boolean);'
      'begin'
      'end;'
      ''
      'procedure setVisibleVodomeriPolinja(visible:boolean);'
      'begin'
      '       mmStaraSostojba.Visible := visible;'
      '       mmNovaSostojba.Visible := visible;'
      '       mmRazlikaNovaStaraSostojba.Visible := visible;'
      
        '       frxDSPresmetkaSCENA.Visible:=visible;                    ' +
        '                  '
      '       frxDSPresmetkaSIznosBezDDV.Visible:=visible;'
      '       memo100.Visible:=visible;'
      '       if visible=true then suma_r:=suma_razlika;'
      'end;'
      ''
      
        'procedure GroupFooterPresmetkaGOnAfterPrint(Sender: TfrxComponen' +
        't);'
      'begin'
      '       Engine.NewPage;'
      '       setVisibleVodomeriPolinja(true);'
      '       suma_r:=suma_razlika;'
      '       cena:=<frxDSPresmetkaS."CENA">;                         '
      '  end;'
      ''
      
        'procedure mmRazlikaNovaStaraSostojbaOnBeforePrint(Sender: TfrxCo' +
        'mponent);'
      'begin'
      
        '     suma_razlika:=suma_r+(<frxDSPresmetkaVodomeriSostojba."NOVA' +
        '_SOSTOJBA">-<frxDSPresmetkaVodomeriSostojba."STARA_SOSTOJBA">)'
      'end;'
      ''
      'procedure Memo4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '   if ((<frxDSPresmetkaS."VID_USLUGA">='#39'3'#39') and (<frxDSPresmetka' +
        'S."USLUGA">='#39'3'#39')) '
      '       then cena:=<frxDSPresmetkaS."CENA">;    '
      
        '       //else suma_razlika:=0;                                  ' +
        '              '
      'end;'
      ''
      'procedure GroupFooter2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '     if ((<frxDSPresmetkaS."VID_USLUGA">='#39'3'#39') and (<frxDSPresmet' +
        'kaS."USLUGA">='#39'3'#39')) '
      '       then cena1:=<frxDSPresmetkaS."CENA">;             '
      'end;'
      ''
      'begin          '
      '      '
      'end.')
    Left = 312
    Top = 192
    Datasets = <
      item
        DataSet = frxDSPresmetkaG
        DataSetName = 'frxDSPresmetkaG'
      end
      item
        DataSet = frxDSPresmetkaS
        DataSetName = 'frxDSPresmetkaS'
      end
      item
        DataSet = frxDSPresmetkaStavki1
        DataSetName = 'frxDSPresmetkaStavki1'
      end
      item
        DataSet = frxDSPresmetkaStavki10
        DataSetName = 'frxDSPresmetkaStavki10'
      end
      item
        DataSet = frxDSPresmetkaStavki11
        DataSetName = 'frxDSPresmetkaStavki11'
      end
      item
        DataSet = frxDSPresmetkaStavki12
        DataSetName = 'frxDSPresmetkaStavki12'
      end
      item
        DataSet = frxDSPresmetkaStavki121
        DataSetName = 'frxDSPresmetkaStavki121'
      end
      item
        DataSet = frxDSPresmetkaStavki2
        DataSetName = 'frxDSPresmetkaStavki2'
      end
      item
        DataSet = frxDSPresmetkaStavki3
        DataSetName = 'frxDSPresmetkaStavki3'
      end
      item
        DataSet = frxDSPresmetkaStavki4
        DataSetName = 'frxDSPresmetkaStavki4'
      end
      item
        DataSet = frxDSPresmetkaStavki5
        DataSetName = 'frxDSPresmetkaStavki5'
      end
      item
        DataSet = frxDSPresmetkaStavki6
        DataSetName = 'frxDSPresmetkaStavki6'
      end
      item
        DataSet = frxDSPresmetkaStavki61
        DataSetName = 'frxDSPresmetkaStavki61'
      end
      item
        DataSet = frxDSPresmetkaStavki7
        DataSetName = 'frxDSPresmetkaStavki7'
      end
      item
        DataSet = frxDSPresmetkaStavki8
        DataSetName = 'frxDSPresmetkaStavki8'
      end
      item
        DataSet = frxDSPresmetkaVodomeriSostojba
        DataSetName = 'frxDSPresmetkaVodomeriSostojba'
      end
      item
        DataSet = frxDSSaldo
        DataSetName = 'frxDSSaldo'
      end>
    Variables = <
      item
        Name = ' Promenlivi'
        Value = Null
      end
      item
        Name = 'printNextVodomer'
        Value = Null
      end
      item
        Name = 'totalPotrosenaVoda'
        Value = '0'
      end
      item
        Name = 'totalOsnovicaDanok5'
        Value = '0'
      end
      item
        Name = 'totalDanok5'
        Value = '0'
      end
      item
        Name = 'totalOsnovicaDanok18'
        Value = '0'
      end
      item
        Name = 'totalDanok18'
        Value = '0'
      end
      item
        Name = 'PoslUplata'
        Value = #39#39
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'tahoma'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 152.400000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      OnBeforePrint = 'Page1OnBeforePrint'
      object MasterDataPresmetkaG: TfrxMasterData
        FillType = ftBrush
        Height = 181.417344800000000000
        Top = 41.574830000000000000
        Width = 755.906000000000000000
        DataSet = frxDSPresmetkaG
        DataSetName = 'frxDSPresmetkaG'
        RowCount = 0
        object mmPeriod: TfrxMemoView
          Left = 390.834880000000000000
          Top = 60.574803150000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."MESEC"]')
          ParentFont = False
        end
        object mmLokacija: TfrxMemoView
          Left = 89.606370000000000000
          Top = 111.118110240000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."PARTNER_ID"]/[frxDSPresmetkaG."LOKACIIN_ID"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 91.047310000000000000
          Top = 60.574830000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."BROJ_FAKTURA"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 498.897960000000000000
          Top = 60.574803150000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."GODINA"]')
          ParentFont = False
        end
        object frxDSPresmetkaGNAZIV: TfrxMemoView
          Left = 91.007874020000000000
          Top = 78.252010000000010000
          Width = 423.307360000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          DataField = 'NAZIV'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."NAZIV"]')
          ParentFont = False
        end
        object frxDSPresmetkaGADRESA: TfrxMemoView
          Left = 91.007874020000000000
          Top = 93.370130000000010000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          DataField = 'ADRESA'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."ADRESA"]')
          ParentFont = False
        end
        object frxDSPresmetkaGREON_ID: TfrxMemoView
          Left = 247.567100000000000000
          Top = 111.118110236220000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'REON_ID'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."REON_ID"]')
          ParentFont = False
        end
        object frxDSPresmetkaGCITACKA_KNIGA: TfrxMemoView
          Left = 349.834880000000000000
          Top = 111.118110236220000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'CITACKA_KNIGA'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."CITACKA_KNIGA"]')
          ParentFont = False
        end
        object frxDSPresmetkaGDATUM_PRESMETKA: TfrxMemoView
          Left = 147.929190000000000000
          Top = 128.125984251969000000
          Width = 154.960730000000000000
          Height = 15.118120000000000000
          DataField = 'DATUM_PRESMETKA'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd.mm.yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."DATUM_PRESMETKA"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 404.409710000000000000
          Top = 128.125984250000000000
          Width = 154.960730000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd.mm.yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."DDO"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 634.961040000000000000
          Top = 98.267780000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            'BILE')
          ParentFont = False
        end
      end
      object GroupHeaderPresmetkaG: TfrxGroupHeader
        FillType = ftBrush
        Top = 18.897650000000000000
        Width = 755.906000000000000000
        Condition = 'frxDSPresmetkaG."LOKACIIN_ID"'
      end
      object GroupFooterPresmetkaG: TfrxGroupFooter
        FillType = ftBrush
        Height = 48.755905510000000000
        Top = 604.724800000000000000
        Width = 755.906000000000000000
        OnAfterPrint = 'GroupFooterPresmetkaGOnAfterPrint'
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo23: TfrxMemoView
          Left = 569.440944880000000000
          Top = 1.000000000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki8."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 569.440944880000000000
          Top = 30.338589999999960000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."IZNOS_VKUPNO"]')
          ParentFont = False
        end
        object frxDSPresmetkaGIZNOS_VKUPNO: TfrxMemoView
          Left = 510.236550000000000000
          Top = 15.716318190000040000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Visible = False
          OnBeforePrint = 'frxDSPresmetkaGIZNOS_VKUPNOOnBeforePrint'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Fill.BackColor = 15987699
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            #1044#1086#1083#1075':')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 569.874460000000000000
          Top = 15.118119999999980000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          Visible = False
          OnBeforePrint = 'frxDSPresmetkaGIZNOS_VKUPNOOnBeforePrint'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Fill.BackColor = 15987699
          HAlign = haRight
          HideZeros = True
          Memo.UTF8W = (
            
              '[Round(<frxDSSaldo."SALDO">-<frxDSPresmetkaG."IZNOS_VKUPNO">+Str' +
              'ToFloat( <PoslUplata>))]')
          ParentFont = False
        end
      end
      object DetailDataVodomeriSostojba: TfrxDetailData
        FillType = ftBrush
        Height = 13.228346456692900000
        Top = 298.582870000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'DetailDataVodomeriSostojbaOnBeforePrint'
        RowCount = 6
        object frxDSPresmetkaSIznosBezDDV: TfrxMemoView
          Left = 569.370440000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDSPresmetkaStavki1."CENA_VODA">*(<frxDSPresmetkaVodomeriSos' +
              'tojba."NOVA_SOSTOJBA">-<frxDSPresmetkaVodomeriSostojba."STARA_SO' +
              'STOJBA">)]')
          ParentFont = False
        end
        object mmStaraSostojba: TfrxMemoView
          Left = 49.417440000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaVodomeriSostojba."NOVA_SOSTOJBA"]')
          ParentFont = False
        end
        object mmNovaSostojba: TfrxMemoView
          Left = 173.700990000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaVodomeriSostojba."STARA_SOSTOJBA"]')
          ParentFont = False
        end
        object mmRazlikaNovaStaraSostojba: TfrxMemoView
          Left = 302.070866140000000000
          Width = 71.811023620000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'mmRazlikaNovaStaraSostojbaOnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDSPresmetkaVodomeriSostojba."NOVA_SOSTOJBA">-<frxDSPresmetk' +
              'aVodomeriSostojba."STARA_SOSTOJBA">]')
          ParentFont = False
        end
        object frxDSPresmetkaSCENA: TfrxMemoView
          Left = 443.764070000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki1."CENA_VODA"]')
          ParentFont = False
        end
        object mmVodomerID: TfrxMemoView
          Left = 6.779530000000000000
          Width = 56.692950000000000000
          Height = 11.338590000000000000
          Visible = False
          OnAfterPrint = 'Memo7OnAfterPrint'
          OnBeforePrint = 'Memo7OnBeforePrint'
          DataField = 'VODOMERI_ID'
          DataSet = frxDSPresmetkaVodomeriSostojba
          DataSetName = 'frxDSPresmetkaVodomeriSostojba'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaVodomeriSostojba."VODOMERI_ID"]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 663.520100000000000000
          Width = 49.133858270000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki1."DDV"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 3.779527560000000000
        Top = 245.669450000000000000
        Width = 755.906000000000000000
        Condition = 'frxDSPresmetkaS."VID_USLUGA"'
        KeepTogether = True
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 135.685039370000000000
        Top = 445.984540000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo15: TfrxMemoView
          Left = 426.716760000000000000
          Top = 39.000000000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."OSNOVICA_DANOK_05"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 427.039580000000000000
          Top = 52.338590000000010000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."OSNOVICA_DANOK_18"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 569.440944880000000000
          Top = 40.559059999999990000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."DANOK_05"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 569.440944880000000000
          Top = 53.897650000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaG."DANOK_18"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 569.440944880000000000
          Top = 72.574829999999960000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[<frxDSPresmetkaG."DANOK_05">+<frxDSPresmetkaG."DANOK_18">]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 569.440944880000000000
          Top = 102.370130000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki6."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 569.440944880000000000
          Top = 115.708720000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki7."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 569.267716540000000000
          Top = 12.543290000000010000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki5."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 663.520100000000000000
          Top = 12.543290000000010000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki5."DDV"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 302.724490000000000000
          Top = 25.700819130000010000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 429.574871500000000000
          Top = 25.677180000000020000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Visible = False
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki61."CENA_NADOMESTOK"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 570.102442760000000000
          Top = 25.677180000000020000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki61."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 663.354826220000000000
          Top = 25.677180000000020000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki61."DDV"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 125.724490000000000000
          Top = 25.677180000000020000
          Width = 174.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1052#1077#1093#1072#1085#1080#1079#1072#1084)
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 301.362400000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDSPresmetkaStavki121."CENA_DVOR"><>0,<frxDSPresmetkaSta' +
              'vki121."IZNOS_USLUGA_NETO">/<frxDSPresmetkaStavki121."CENA_DVOR"' +
              '>,0)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 444.110431500000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki121."CENA_DVOR"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 569.740352760000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki121."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 663.992321260000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki121."DDV"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 124.724490000000000000
          Width = 174.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1047#1077#1083#1077#1085#1080#1083#1086' - '#1044#1042#1054#1056)
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 272.126160000000000000
        Width = 755.906000000000000000
        Condition = 'frxDSPresmetkaS."USLUGA"'
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 87.992191890000000000
        Top = 336.378170000000000000
        Width = 755.906000000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object frxDSPresmetkaGDVORNA_POVRSINA: TfrxMemoView
          Left = 301.889763780000000000
          Top = 58.401591890000030000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDSPresmetkaStavki4."CENA_DVOR"><>0,<frxDSPresmetkaStavk' +
              'i4."IZNOS_USLUGA_NETO">/<frxDSPresmetkaStavki4."CENA_DVOR">,0)]')
          ParentFont = False
        end
        object frxDSPresmetkaGSTANBENA_POVRSINA: TfrxMemoView
          Left = 301.889763780000000000
          Top = 48.606291890000030000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDSPresmetkaStavki3."CENA_STAN"><>0,<frxDSPresmetkaStavk' +
              'i3."IZNOS_USLUGA_NETO">/<frxDSPresmetkaStavki3."CENA_STAN">,0)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 301.889763780000000000
          Top = 10.181121889999990000
          Width = 71.811023620000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[suma_razlika]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 443.637795280000000000
          Top = 48.771653539999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki3."CENA_STAN"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 443.637795280000000000
          Top = 58.377952760000030000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki4."CENA_DVOR"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 443.637795280000000000
          Top = 10.188976379999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki2."CENA_KAN"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 569.267716540000000000
          Top = 10.188976379999990000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki2."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 663.519685040000000000
          Top = 10.188976379999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki2."DDV"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 569.267716540000000000
          Top = 48.629921260000010000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki3."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 569.267716540000000000
          Top = 58.377952760000030000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki4."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 663.519685040000000000
          Top = 48.629921260000010000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki3."DDV"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 663.520100000000000000
          Top = 58.377952760000030000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki4."DDV"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 300.944960000000000000
          Top = 27.496119130000010000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 443.692991500000000000
          Top = 27.472480000000020000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki10."CENA_NADOMESTOK"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 569.322912760000000000
          Top = 27.472480000000020000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki10."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 662.575296220000000000
          Top = 27.472480000000020000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki10."DDV"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 123.944960000000000000
          Top = 27.472480000000020000
          Width = 174.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1032#1072#1074#1085#1072' '#1095#1080#1089#1090#1086#1090#1072)
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 301.724490000000000000
          Top = 0.023639129999992290
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Visible = False
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 444.472521500000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki11."CENA_NADOMESTOK"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 570.102442760000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki11."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 663.354826220000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki11."DDV"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 124.724490000000000000
          Width = 174.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1050#1086#1088#1077#1082#1094#1080#1112#1072)
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 301.362400000000000000
          Top = 74.566929130000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDSPresmetkaStavki12."CENA_STAN"><>0,<frxDSPresmetkaStav' +
              'ki12."IZNOS_USLUGA_NETO">/<frxDSPresmetkaStavki12."CENA_STAN">,0' +
              ')]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 444.110431500000000000
          Top = 74.566929130000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki12."CENA_STAN"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 569.740352760000000000
          Top = 74.566929130000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki12."IZNOS_USLUGA_NETO"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 663.992321260000000000
          Top = 74.566929130000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'frxDSPresmetkaSCENAOnBeforePrint'
          DataSet = frxDSPresmetkaS
          DataSetName = 'frxDSPresmetkaS'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDSPresmetkaStavki12."DDV"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 124.724490000000000000
          Top = 74.566929130000000000
          Width = 174.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            #1047#1077#1083#1077#1085#1080#1083#1086' - '#1057#1058#1040#1053)
          ParentFont = False
        end
      end
    end
  end
  object frxDSPresmetkaG: TfrxDBDataset
    UserName = 'frxDSPresmetkaG'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER_ID=PARTNER_ID'
      'NAZIV=NAZIV'
      'IZNOS_VKUPNO=IZNOS_VKUPNO'
      'VRABOTEN=VRABOTEN'
      'LOKACIIN_ID=LOKACIIN_ID'
      'GODINA=GODINA'
      'MESEC=MESEC'
      'REON_ID=REON_ID'
      'CITACKA_KNIGA=CITACKA_KNIGA'
      'DANOK_05=DANOK_05'
      'DANOK_18=DANOK_18'
      'OSNOVICA_DANOK_05=OSNOVICA_DANOK_05'
      'OSNOVICA_DANOK_18=OSNOVICA_DANOK_18'
      'STANBENA_POVRSINA=STANBENA_POVRSINA'
      'DVORNA_POVRSINA=DVORNA_POVRSINA'
      'BROJ_FAKTURA=BROJ_FAKTURA'
      'DATUM_PRESMETKA=DATUM_PRESMETKA'
      'CITACKA_KNIGA_RB=CITACKA_KNIGA_RB'
      'ADRESA=ADRESA'
      'ID=ID'
      'NAZIV1=NAZIV1'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'DANOK=DANOK'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDO=DDO'
      'RB2=RB2'
      'EMAIL=EMAIL'
      'MAIL=MAIL'
      'VK_POTROSENO=VK_POTROSENO'
      'IMA_SLIKA=IMA_SLIKA'
      'LINK_SLIKA=LINK_SLIKA'
      'IMG=IMG'
      'LOK_ULICA=LOK_ULICA'
      'LOK_BROJ=LOK_BROJ')
    OpenDataSource = False
    DataSource = DM1.DSPresmetka
    BCDToCurrency = False
    Left = 344
    Top = 192
  end
  object frxDSPresmetkaS: TfrxDBDataset
    UserName = 'frxDSPresmetkaS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NAZIV=NAZIV'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'LOKACIIN_ID=LOKACIIN_ID'
      'CENA=CENA'
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'PROCENT_DANOK=PROCENT_DANOK'
      'DANOK=DANOK')
    OpenDataSource = False
    DataSource = DM1.DSPresmetka1
    BCDToCurrency = False
    Left = 344
    Top = 224
  end
  object frxDSPresmetkaVodomeriSostojba: TfrxDBDataset
    UserName = 'frxDSPresmetkaVodomeriSostojba'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LOKACIIN_ID=LOKACIIN_ID'
      'VODOMERI_ID=VODOMERI_ID'
      'NOVA_SOSTOJBA=NOVA_SOSTOJBA'
      'STARA_SOSTOJBA=STARA_SOSTOJBA'
      'SLIKA=SLIKA'
      'IMG=IMG'
      'BR_VODOMER=BR_VODOMER')
    OpenDataSource = False
    DataSource = DM1.DSPresmetka2
    BCDToCurrency = False
    Left = 344
    Top = 256
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Tahoma'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 280
    Top = 192
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 384
    Top = 192
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1040#1082#1094#1080#1080
      'Default')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    PopupMenuLinks = <>
    Style = bmsOffice11
    SunkenBorder = True
    UseSystemFont = True
    Left = 520
    Top = 24
    PixelsPerInch = 96
    object SubMenuAkcii: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'BarButtonPrintBeleskiAll'
        end
        item
          Visible = True
          ItemName = 'BarButtonPrintBeleskaSelected'
        end
        item
          Visible = True
          ItemName = 'BarButtonPrintFakturiAll'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzbrisiPresmetka'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzlez'
        end>
    end
    object BarButtonIzlez: TdxBarButton
      Action = aIzlez
      Category = 0
    end
    object BarButtonPrintBeleskiAll: TdxBarButton
      Action = aPrintAllBeleskiFizicki
      Caption = #1055#1077#1095#1072#1090#1080' '#1073#1077#1083#1077#1096#1082#1080' '#1079#1072' '#1089#1080#1090#1077
      Category = 0
    end
    object BarButtonPrintBeleskaSelected: TdxBarButton
      Action = aPrintBeleskaOdberenFizicki
      Caption = #1055#1077#1095#1072#1090#1080' '#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1086#1076#1073#1077#1088#1077#1085
      Category = 0
    end
    object BarButtonPrintFakturiAll: TdxBarButton
      Action = aPrintAllFakturi
      Caption = #1055#1077#1095#1072#1090#1080' '#1092#1072#1082#1090#1091#1088#1080' '#1079#1072' '#1089#1080#1090#1077
      Category = 0
    end
    object BarButtonIzbrisiPresmetka: TdxBarButton
      Action = aIzbrisiLastPresmetka
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      Category = 0
    end
  end
  object frxDSPresmetkaStavki1: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LOKACIIN_ID=LOKACIIN_ID'
      'VODOMERI_ID=VODOMERI_ID'
      'NOVA_SOSTOJBA=NOVA_SOSTOJBA'
      'STARA_SOSTOJBA=STARA_SOSTOJBA'
      'RAZLIKA=RAZLIKA'
      'CENA_VODA=CENA_VODA'
      'DDV=DDV'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUslugu1
    BCDToCurrency = False
    Left = 32
    Top = 184
  end
  object frxDSPresmetkaStavki2: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_KAN=CENA_KAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUslugi2
    BCDToCurrency = False
    Left = 32
    Top = 216
  end
  object frxDSPresmetkaStavki3: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_STAN=CENA_STAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva3
    BCDToCurrency = False
    Left = 32
    Top = 248
  end
  object frxDSPresmetkaStavki4: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki4'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_DVOR=CENA_DVOR'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva4
    BCDToCurrency = False
    Left = 32
    Top = 280
  end
  object frxDSPresmetkaStavki5: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki5'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_MANIPULATIVEN=CENA_MANIPULATIVEN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva5
    BCDToCurrency = False
    Left = 32
    Top = 312
  end
  object frxDSPresmetkaStavki6: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki6'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FVODA=CENA_FVODA'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva6
    BCDToCurrency = False
    Left = 32
    Top = 344
  end
  object frxDSPresmetkaStavki7: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki7'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FKAN=CENA_FKAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva7
    BCDToCurrency = False
    Left = 32
    Top = 376
  end
  object frxDSSaldo: TfrxDBDataset
    UserName = 'frxDSSaldo'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SALDO=SALDO')
    OpenDataSource = False
    DataSource = DM1.DSPresmetka3
    BCDToCurrency = False
    Left = 344
    Top = 296
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 472
    Top = 72
  end
  object frxDSPresmetkaStavki8: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki8'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FKAN=CENA_FKAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.dsPresmetkaPoUsluga8
    BCDToCurrency = False
    Left = 144
    Top = 376
  end
  object frxDSPresmetkaStavki10: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki10'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'DDV=DDV'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva10
    BCDToCurrency = False
    Left = 240
    Top = 376
  end
  object frxDSPresmetkaStavki61: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki61'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'DDV=DDV'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DM1.DSTblDPresmetkaPoUsluga61
    BCDToCurrency = False
    Left = 344
    Top = 376
  end
  object frxDSPresmetkaStavki11: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki11'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'DDV=DDV'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva11
    BCDToCurrency = False
    Left = 240
    Top = 336
  end
  object frxDSPresmetkaStavki12: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki12'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_STAN=CENA_STAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva12
    BCDToCurrency = False
    Left = 408
    Top = 336
  end
  object frxDSPresmetkaStavki121: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki121'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_DVOR=CENA_DVOR'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSTblPresmetkaPoUsluva121
    BCDToCurrency = False
    Left = 528
    Top = 336
  end
  object qUpdateImaSlika: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_presmetka_g g'
      'set g.ima_slika = :ima_slika'
      
        'where g.lokaciin_id = :lokacija and g.mesec = :mesec and g.godin' +
        'a = :godina and g.tip = 1')
    Left = 456
    Top = 121
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object IdHTTP2: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 312
    Top = 8
  end
end
