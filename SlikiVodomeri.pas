unit SlikiVodomeri;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, dxSkinsCore,
  dxSkinsDefaultPainters, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, dxSkinsdxBarPainter,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxBar, cxClasses, frxBarcode,
  frxDesgn, frxClass, frxDBSet, ActnList, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxGrid,
  ComCtrls, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, StdCtrls,
  cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxButtons, ExtCtrls, DateUtils,
  cxGroupBox, cxRadioGroup, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSpringTime, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,cxExport,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, cxNavigator, System.Actions, idcoderMIME,System.IOUtils, cxmemo,
  FIBQuery, pFIBQuery, cxCheckBox, dxpscore, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxRibbonPainter, dxPScxCommon,System.StrUtils,
  cxImageComboBox, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, Vcl.DBCtrls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP;

type
  TfrmSlikiVodomeri = class(TForm)
    panelGoren: TPanel;
    lblMesec: TLabel;
    lblGodina: TLabel;
    lblReon: TLabel;
    cxBtnPresmetaj: TcxButton;
    cxComboBoxGodini: TcxComboBox;
    cxComboBoxMesec: TcxComboBox;
    GroupBoxPoslednaPresmetka: TGroupBox;
    lblMesecLast: TLabel;
    lblMesecLastValue: TLabel;
    lblGodinaLast: TLabel;
    lblGodinaLastValue: TLabel;
    cxBtnIzbrisiPoslednaPresmetka: TcxButton;
    cxLookupCBReoni: TcxLookupComboBox;
    StatusBar1: TStatusBar;
    panelDolen: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ActionList1: TActionList;
    aIzlez: TAction;
    aCallFrfBeleskiPrint: TAction;
    aPrintAllBeleskiFizicki: TAction;
    aPrintBeleskaOdberenFizicki: TAction;
    aPrintAllFakturi: TAction;
    aEdnaFaktura: TAction;
    frxDSPresmetkaG: TfrxDBDataset;
    frxDSPresmetkaS: TfrxDBDataset;
    frxDSPresmetkaVodomeriSostojba: TfrxDBDataset;
    frxBarCodeObject1: TfrxBarCodeObject;
    dxBarManager1: TdxBarManager;
    SubMenuAkcii: TdxBarSubItem;
    BarButtonIzlez: TdxBarButton;
    BarButtonPrintBeleskiAll: TdxBarButton;
    BarButtonPrintBeleskaSelected: TdxBarButton;
    BarButtonPrintFakturiAll: TdxBarButton;
    BarButtonIzbrisiPresmetka: TdxBarButton;
    tblsaldo: TfrxDBDataset;
    cxGridPopupMenu1: TcxGridPopupMenu;
    frxDSPresmetkaStavki1: TfrxDBDataset;
    frxDSPresmetkaStavki2: TfrxDBDataset;
    frxDSPresmetkaStavki3: TfrxDBDataset;
    frxDSPresmetkaStavki4: TfrxDBDataset;
    frxDSPresmetkaStavki5: TfrxDBDataset;
    frxDSPresmetkaStavki6: TfrxDBDataset;
    frxDSPresmetkaStavki7: TfrxDBDataset;
    frxReport1: TfrxReport;
    frxDSPresmetkaStavki8: TfrxDBDataset;
    frxDSPresmetkaStavki10: TfrxDBDataset;
    frxDSPresmetkaStavki61: TfrxDBDataset;
    frxDSPresmetkaStavki11: TfrxDBDataset;
    frxDSPresmetkaStavki12: TfrxDBDataset;
    frxDSPresmetkaStavki121: TfrxDBDataset;
    aExporttoExcel: TAction;
    aMailFaktura: TAction;
    aMailFakturaDizajn: TAction;
    aNovIzbledB: TAction;
    aNovIzgledBDizajn: TAction;
    aNovIzgledSiteB: TAction;
    cxMemo1: TcxMemo;
    qUpdateImaSlika: TpFIBQuery;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn;
    cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1RAZLIKA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1SLIKA: TcxGridDBColumn;
    aPecati: TAction;
    aPecatiDizajner: TAction;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1ULICA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    aPrint: TAction;
    aSnimiIzgled: TAction;
    qProveriSostojba: TpFIBQuery;
    qInsertVS: TpFIBUpdateObject;
    qDeleteVS: TpFIBUpdateObject;
    IdHTTP2: TIdHTTP;
    dbimgIMG: TDBImage;
    procedure FormShow(Sender: TObject);
    procedure aPrintAllBeleskiFizickiExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function podatociOk():boolean;
    procedure cxBtnPresmetajClick(Sender: TObject);
    procedure closePresmetkaDatasets();
    procedure aCallFrfBeleskiPrintExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aEdnaFakturaExecute(Sender: TObject);
    procedure aPrintAllFakturiExecute(Sender: TObject);
    procedure aExporttoExcelExecute(Sender: TObject);
    procedure aMailFakturaExecute(Sender: TObject);
    procedure aMailFakturaDizajnExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aPecatiDizajnerExecute(Sender: TObject);
    procedure cxGrid1DBTableView1Column1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure aPrintExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
  //  procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSlikiVodomeri: TfrmSlikiVodomeri;

implementation

uses dmKonekcija, dmMaticni, UnitDM1,utils, dmResources;

{$R *.dfm}

procedure TfrmSlikiVodomeri.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   // �� �� ������� �������� KOM_VODOMERI_SOSTOBA_S


    if DM1.tblSlikiVovdomeri.Active then
    begin
        DM1.tblSlikiVovdomeri.Close();
    end;
    if DM1.TblPresmetkaG.Active then
    begin
        DM1.TblPresmetkaG.Close();
    end;

    Action := caFree;
end;

procedure TfrmSlikiVodomeri.FormCreate(Sender: TObject);
begin
 ProcitajFormaIzgled(self);
end;

procedure TfrmSlikiVodomeri.FormShow(Sender: TObject);
begin
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)));
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)-1));
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);

end;

procedure TfrmSlikiVodomeri.onKeyDownLookUpAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            begin
                PostMessage(Handle, WM_NEXTDLGCTL,0,0);
            end;

        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;

end;

procedure TfrmSlikiVodomeri.aCallFrfBeleskiPrintExecute(Sender: TObject);
begin
   frxReport1.DesignReport();
  //dmRes.frxReport1.DesignReport;
end;

procedure TfrmSlikiVodomeri.aEdnaFakturaExecute(Sender: TObject);
var uplateno:real;
begin
    uplateno := 0;
    if DM1.tblSlikiVovdomeri.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));

        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;

end;

procedure TfrmSlikiVodomeri.aExporttoExcelExecute(Sender: TObject);
begin
//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
  zacuvajVoExcel(cxGrid1, Caption);

end;

procedure TfrmSlikiVodomeri.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmSlikiVodomeri.aMailFakturaDizajnExecute(Sender: TObject);
begin
//   if dm1.tblSlikiVovdomeri.RecordCount > 0 then
//    begin
//        frxDSPresmetkaG.RangeBegin := rbCurrent;
//        frxDSPresmetkaG.RangeEnd := reCurrent;
//
//      dmRes.Spremi(dmKon.aplikacija,32);
//      //dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
//      dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblSlikiVovdomeriBROJ_FAKTURA.Value;;
//      dmKon.tblSqlReport.Open;
//      dmRes.frxReport1.DesignReport();
//    end;
end;

procedure TfrmSlikiVodomeri.aMailFakturaExecute(Sender: TObject);
var memStream: TMemoryStream;
status: TStatusWindowHandle;
begin
end;

procedure TfrmSlikiVodomeri.aPecatiDizajnerExecute(Sender: TObject);
begin
// dizajner
      dmRes.Spremi(dmKon.aplikacija,36);
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.tblSlikiVovdomeriGODINA.AsString;  // DM1.tblSlikiVovdomeriBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.tblSlikiVovdomeriMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.tblSlikiVovdomeriREON_ID.AsString;
      dmKon.tblSqlReport.Open;
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmSlikiVodomeri.aPecatiExecute(Sender: TObject);
begin
// da se ispecati
      dmRes.Spremi(dmKon.aplikacija,36);
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.tblSlikiVovdomeriGODINA.AsString;  // DM1.tblSlikiVovdomeriBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.tblSlikiVovdomeriMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.tblSlikiVovdomeriREON_ID.AsString;
      dmKon.tblSqlReport.Open;
      dmRes.frxReport1.ShowReport();
end;

procedure TfrmSlikiVodomeri.aPrintAllBeleskiFizickiExecute(Sender: TObject);
begin
    if DM1.tblSlikiVovdomeri.RecordCount > 0 then
    begin
        frxReport1.LoadFromFile('FRBeleskaFizickoLice.fr3');
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TfrmSlikiVodomeri.aPrintAllFakturiExecute(Sender: TObject);
var uplateno:real;
begin
    uplateno := 0;
    if DM1.tblSlikiVovdomeri.RecordCount > 0 then
    begin
        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();
    end
    else
    begin

        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TfrmSlikiVodomeri.aPrintExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('���� : ' + cxLookupCBReoni.Text + ', ������ � ����� : ' + cxComboBoxGodini.Text + ' / ' + cxComboBoxMesec.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);

end;

procedure TfrmSlikiVodomeri.aSnimiIzgledExecute(Sender: TObject);
begin
    zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
    ZacuvajFormaIzgled(self);
end;

procedure TfrmSlikiVodomeri.cxBtnPresmetajClick(Sender: TObject);
var mesec, line, ima_slika, smeni_red,lokacija,vodomer,sostojba, link_slika, zabeleska, tf_slika, stara_sostojba:string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i,j:integer;
var status: TStatusWindowHandle;
begin
//   status := cxCreateStatusWindow();
//   try
//     ima_slika = '0';
//    if podatociOk then
//    begin
//       // closePresmetkaDatasets;
//       //�� �� ������� ���� ������� KOM_VODOMERI_SOSTOJBA_S
//        qDeleteVS.Close;
//        qDeleteVS.ExecQuery;
//
//        if cxLookupCBReoni.Text<>'' then
//            DM1.tblSlikiVovdomeri.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue
//        else
//            DM1.tblSlikiVovdomeri.ParamByName('REON_ID').AsVariant :='%';
//        DM1.tblSlikiVovdomeri.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
//        DM1.tblSlikiVovdomeri.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
//
//        DM1.tblSlikiVovdomeri.Open;
//try
//      if not dm1.IsInternetConnected then
//    //  IdIcmpClient1.Ping ('www.google.com');
//
//    Except on E:Exception do
//      begin
//         ShowMessage('���� �������� ��������!');
//        Abort;
//      end
//    end;
//
//    if length(cxComboBoxMesec.Text) = 1 then
//      mesec := '0'+cxComboBoxMesec.Text
//    else mesec := cxComboBoxMesec.Text;
//
//         DM1.
//        if dm1.tblPecDukanilink_slika.AsString <> '' then
//            begin
//                  try
//                      MS := TMemoryStream.Create;
//                      GIf := TJPEGImage.Create;
//
//                      IdHTTP2.get(dm1.tblPecDukaniLINK_SLIKA.AsString,MS);
//                      Ms.Seek(0,soFromBeginning);
//                      Gif.LoadFromStream(MS);
//                      dbimgIMG.Picture.Graphic.Assign(GIf);
//                      CompressJpeg(gif);
//                 finally
//                        FreeAndNil(GIF);
//                        FreeAndNil(MS);;
//                end;
//                 ima_slika := '1';
//               //if (vodomer <> '') and (vodomer[1] <>'0') and  (lokacija <> '') and (lokacija <> '0') and (sostojba <> '') and (sostojba[1] <> '0') then
//                //begin
//                     //�� �� ������� ���� �� �������� �� VODOMERI SOSTOJBA
//                      qInsertVS.Close;
//                      qInsertVS.ParamByName('LOKACIJA').AsString := lokacija;
//                      qInsertVS.ParamByName('VODOMER').AsString := vodomer;
//                      qInsertVS.ParamByName('MESEC').AsString := cxComboBoxMesec.Text;
//                      qInsertVS.ParamByName('GODINA').AsString := cxComboBoxGodini.Text;
//                      if stara_sostojba <> '' then
//                         qInsertVS.ParamByName('STARA_SOSTOJBA').AsString := stara_sostojba
//                      else
//                         qInsertVS.ParamByName('STARA_SOSTOJBA').AsInteger := 0;
//                      qInsertVS.ParamByName('NOVA_SOSTOJBA').AsString := sostojba;
//                      qInsertVS.ParamByName('REON_ID').AsString := cxLookupCBReoni.Text;;
//                      qInsertVS.ExecQuery;
//                //end;
//              end;
//           end;
//
//      dm1.ZemiTextFile(link_zemi+cxComboBoxGodini.Text+'-'+mesec+'&where=REON%20%3D%27'+cxLookupCBReoni.Text //+'%27%20AND%20CITACKA_KNIGA%20%3D%27'+cxLookupCBCitackaKniga.Text
//          +'%27&select=LOKACISKI_BROJ,TOCNO_MANUALNO_CITANJE,SLIKA,MESEC_NA_CITANJE');
//
//             cel_zapis := '';
//             cxMemo1.Text := vraten_text;
//             i:=1;
//            // for i:=1 to cxMemo1.lines.Count do
//             while (i <> cxmemo1.Lines.Count) do
//
//             begin
//               line := cxMemo1.Lines[i];
//               lokacija := copy(line,1,pos(#9,line)-1);
//
//               tf_slika := trim(copy(line, length(lokacija)+1 , 5));
//               if tf_slika = 'true' then tf_slika := 'true' else tf_slika := 'false';
//              if tf_slika = 'true' then
//
//              begin
//               line := trim(copy(line,pos(#9,line)+6,length(line)-length(lokacija)-length(tf_slika)));
//                while (Pos('-',line) = 0) do
//                begin
//
//                  cel_zapis := cel_zapis+line ;
//                  i := i+1;
//                  line := cxMemo1.Lines[i];
//                end;
//                   cel_zapis := cel_zapis+line;
//
//             if (cel_zapis <> '')
//             then
//             begin
//                       qUpdateImaSlika.Close;
//                       qUpdateImaSlika.ParamByName('ima_slika').Value := '1';
//                       qUpdateImaSlika.ParamByName('lokacija').AsString := lokacija;
//                       qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                       qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                       qUpdateImaSlika.ExecQuery;
//             end;
//           end
//             else
//             begin
//                 qUpdateImaSlika.Close;
//                 qUpdateImaSlika.ParamByName('ima_slika').Value := '0';
//                 qUpdateImaSlika.ParamByName('lokacija').AsString := lokacija;
//                 qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                 qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                 qUpdateImaSlika.ExecQuery;
//             end;
//               i := i+1;
//               cel_zapis := '';
//             end;
//
//        dm1.tblSlikiVovdomeri.FullRefresh;
//
//        cxGrid1.SetFocus();
//   finally
//         cxRemoveStatusWindow(status);
//   end;

end;

procedure TfrmSlikiVodomeri.cxGrid1DBTableView1Column1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
var  AFocusedRecordIndex, ARecno: Integer;

begin

  ARecno := TcxGridDBTableView(Sender.GridView).DataController.DataSource.DataSet.RecNo;

  AFocusedRecordIndex :=  TcxGridDBTableView(Sender.GridView).Controller.FocusedRecordIndex;

  AText := IntToStr( ARecno - AFocusedRecordIndex + ARecord.Index);

end;

procedure TfrmSlikiVodomeri.cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
   AText := '������ : '+AValue;
end;

{procedure TFrmPectiDukani.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if DM1.tblSlikiVovdomeri.Active then
    begin
        DM1.tblSlikiVovdomeri.Close();
    end;

    Action := caFree;
end;
 }

function TfrmSlikiVodomeri.podatociOk():boolean;
var ret:boolean;
begin
    ret := false;
    if cxComboBoxGodini.Text = '' then
    begin
        cxComboBoxGodini.SetFocus();
        ShowMessage('������ �� �������� ������!');
    end
    else if cxComboBoxMesec.Text = '' then
    begin
        cxComboBoxMesec.SetFocus();
        ShowMessage('������ �� �������� �����!');
    end

    else
            begin
                ret := true;
            end;
         podatociOk := ret;
    end;

procedure TfrmSlikiVodomeri.closePresmetkaDatasets();
begin
    if DM1.tblSlikiVovdomeri.Active then
    begin
        DM1.tblSlikiVovdomeri.Close;
        DM1.TblDPresmetkaS.Close;
        DM1.tblDPrsmetkaVodomeriSostojba.Close;
        dm1.tblDSaldo.Close;
    end;
end;
end.
