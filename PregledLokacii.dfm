object frmPregledLokacii: TfrmPregledLokacii
  Left = 0
  Top = 0
  Action = aSerBrojVodomer
  BorderIcons = [biSystemMenu]
  Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1083#1086#1082#1072#1094#1080#1080
  ClientHeight = 695
  ClientWidth = 1080
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClick = aSerBrojVodomerExecute
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 0
    Top = 649
    Width = 1080
    Height = 23
    Align = alBottom
    AutoSize = False
    Caption = 
      '   '#1047#1072' '#1076#1072' '#1080#1079#1073#1077#1088#1077#1090#1077' '#1087#1086#1074#1077#1116#1077' '#1083#1086#1082#1072#1094#1080#1080' '#1079#1072' '#1074#1085#1077#1089' '#1085#1072' '#1052#1045#1056#1053#1040' '#1058#1054#1063#1050#1040', '#1076#1088#1078#1077#1090#1077' ' +
      #1075#1086' '#1087#1088#1080#1090#1080#1089#1085#1072#1090' '#1090#1072#1089#1090#1077#1088#1086#1090' CTRL'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -9
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Layout = tlCenter
    ExplicitLeft = 2
    ExplicitTop = 272
    ExplicitWidth = 738
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1080
    Height = 18
    Align = alTop
    BevelOuter = bvNone
    Color = 15913395
    ParentBackground = False
    TabOrder = 0
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 672
    Width = 1080
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Ctrl+M - '#1042#1085#1077#1089#1091#1074#1072#1114#1077'/'#1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1084#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112', Ctrl+S - '#1042#1085#1077#1089#1091#1074#1072#1114#1077'/'#1055 +
          #1088#1086#1084#1077#1085#1072' '#1085#1072' '#1057#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088', Esc - '#1048#1079#1083#1077#1079
      end>
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object pcPregledLokacii: TPageControl
    Left = 0
    Top = 144
    Width = 1080
    Height = 505
    ActivePage = tsLokacii
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnChange = pcPregledLokaciiChange
    object tsLokaciiVodomeri: TTabSheet
      Caption = #1051#1086#1082#1072#1094#1080#1080' '#1089#1086' '#1074#1086#1076#1086#1084#1077#1088#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1072
        Height = 477
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1072
          Height = 477
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object cxGridDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataModeController.SmartRefresh = True
            DataController.DataSource = DM1.dsPregledLokaciiVodomeri
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '###,##0.00'
                Kind = skSum
                FieldName = 'DOLZEN'
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            OptionsView.ColumnAutoWidth = True
            OptionsView.Footer = True
            object cxGridDBTableView1REON_ID: TcxGridDBColumn
              Caption = #1056#1077#1086#1085
              DataBinding.FieldName = 'REON_ID'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 25
            end
            object cxGridDBTableView1CITACKA_KNIGA: TcxGridDBColumn
              DataBinding.FieldName = 'CITACKA_KNIGA'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
            end
            object cxGridDBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn
              DataBinding.FieldName = 'CITACKA_KNIGA_RB'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
            end
            object cxGridDBTableView1RB2: TcxGridDBColumn
              DataBinding.FieldName = 'RB2'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
            end
            object cxGridDBTableView1LOKACIJA: TcxGridDBColumn
              Caption = #1051#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'LOKACIJA'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 37
            end
            object cxGridDBTableView1NAZIV_TIP_PARTNET: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV_TIP_PARTNER'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 45
            end
            object cxGridDBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
            end
            object cxGridDBTableView1PARTNER_ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'PARTNER_ID'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 32
            end
            object cxGridDBTableView1NAZIV: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074
              DataBinding.FieldName = 'NAZIV'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 160
            end
            object cxGridDBTableView1ADRESA: TcxGridDBColumn
              Caption = #1040#1076#1088#1077#1089#1072
              DataBinding.FieldName = 'ADRESA'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 69
            end
            object cxGridDBTableView1NAZIV_MESTO: TcxGridDBColumn
              Caption = #1052#1077#1089#1090#1086
              DataBinding.FieldName = 'NAZIV_MESTO'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 79
            end
            object cxGridDBTableView1MATICEN: TcxGridDBColumn
              Caption = #1052#1072#1090#1080#1095#1077#1085
              DataBinding.FieldName = 'MATICEN'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 62
            end
            object cxGridDBTableView1VODOMER: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088
              DataBinding.FieldName = 'VODOMER'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 118
            end
            object cxGridDBTableView1ID_MERNA_TOCKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1052#1077#1088#1085#1072' '#1058#1086#1095#1082#1072
              DataBinding.FieldName = 'ID_MERNA_TOCKA'
              Width = 129
            end
            object cxGridDBTableView1NAZIV_MERNA_TOCKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1085#1072' '#1058#1086#1095#1082#1072
              DataBinding.FieldName = 'NAZIV_MERNA_TOCKA'
              Width = 194
            end
            object cxGridDBTableView1AKTIVEN: TcxGridDBColumn
              Caption = #1040#1082#1090#1080#1074#1077#1085
              DataBinding.FieldName = 'AKTIVEN'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 108
            end
            object cxGridDBTableView1SERISKI_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'SERISKI_BROJ'
              Visible = False
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridDBTableView1
          end
        end
      end
    end
    object tsLokacii: TTabSheet
      Caption = #1051#1086#1082#1072#1094#1080#1080
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1072
        Height = 477
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataModeController.SmartRefresh = True
          DataController.DataSource = DM1.dsPregledLokacii
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '###,##0.00'
              Kind = skSum
              FieldName = 'DOLG'
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGridDBColumn18: TcxGridDBColumn
            Caption = #1056#1077#1086#1085
            DataBinding.FieldName = 'REON_ID'
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn19: TcxGridDBColumn
            DataBinding.FieldName = 'LOKACIJA'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn20: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn21: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072
            DataBinding.FieldName = 'PARTNER_ID'
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn22: TcxGridDBColumn
            Caption = #1055#1072#1088#1090#1085#1077#1088
            DataBinding.FieldName = 'NAZIV_PARTNER'
            HeaderAlignmentHorz = taCenter
            Width = 189
          end
          object cxGridDBColumn23: TcxGridDBColumn
            DataBinding.FieldName = 'ULICA_ID'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn24: TcxGridDBColumn
            Caption = #1040#1076#1088#1077#1089#1072
            DataBinding.FieldName = 'ADRESA'
            HeaderAlignmentHorz = taCenter
            Width = 133
          end
          object cxGridDBColumn25: TcxGridDBColumn
            Caption = #1052#1077#1089#1090#1086
            DataBinding.FieldName = 'NAZIV_MESTO'
            HeaderAlignmentHorz = taCenter
            Width = 221
          end
          object cxGridDBColumn26: TcxGridDBColumn
            Caption = #1057#1090#1072#1090#1091#1089
            DataBinding.FieldName = 'STATUS'
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn27: TcxGridDBColumn
            Caption = #1052#1072#1090#1080#1095#1077#1085
            DataBinding.FieldName = 'MATICEN'
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn28: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_NA_CLENOVI'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn29: TcxGridDBColumn
            DataBinding.FieldName = 'CITACKA_KNIGA'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn30: TcxGridDBColumn
            DataBinding.FieldName = 'CITACKA_KNIGA_RB'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn31: TcxGridDBColumn
            DataBinding.FieldName = 'STANBENA_POVRSINA'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBColumn32: TcxGridDBColumn
            DataBinding.FieldName = 'DVORNA_POVRSINA'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1080
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 3
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1045#1053#1048
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 0
    end
  end
  object ActionList1: TActionList
    Images = DM1.cxLargeImages
    Left = 737
    Top = 16
    object aNov: TAction
      Caption = 'aNov'
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = 'aAzuriraj'
      ShortCut = 117
    end
    object aBrisi: TAction
      Caption = 'aBrisi'
      ShortCut = 119
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
    end
    object aIzlez: TAction
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aPecati: TAction
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aSnimiVoExcel: TAction
      Caption = 'aSnimiVoExcel'
      ShortCut = 16453
      OnExecute = aSnimiVoExcelExecute
    end
    object aMaticenBroj: TAction
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1041#1088#1086#1112
      ShortCut = 16461
      OnExecute = aMaticenBrojExecute
    end
    object aSerBrojVodomer: TAction
      Caption = #1057#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088' '
      ShortCut = 16467
      OnExecute = aSerBrojVodomerExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      OnExecute = aSnimiIzgledExecute
    end
    object aMernaTocka: TAction
      Caption = #1052#1077#1088#1085#1072' '#1058#1086#1095#1082#1072
      ImageIndex = 16
      OnExecute = aMernaTockaExecute
    end
    object aBrisiMT: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 13
      OnExecute = aBrisiMTExecute
    end
    object aPratiPoMailGrupno: TAction
      Caption = 'aPratiPoMailGrupno'
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 807
    Top = 16
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 772
    Top = 16
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 947
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.Background.Mode = bmBrush
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.CenterTextAlignY = taTop
      PrinterPage.PageFooter.CenterTitle.Strings = (
        '')
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'. [Page #]')
      PrinterPage.PageHeader.CenterTitle.Strings = (
        #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1083#1086#1082#1072#1094#1080#1080' '#1089#1086' '#1074#1086#1076#1086#1084#1077#1088#1080)
      PrinterPage.PageHeader.Font.Charset = RUSSIAN_CHARSET
      PrinterPage.PageHeader.Font.Color = clBlack
      PrinterPage.PageHeader.Font.Height = -19
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = [fsBold]
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43845.451480810190000000
      ReportTitle.Font.Charset = RUSSIAN_CHARSET
      ReportTitle.Font.Color = clBlack
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Tahoma'
      ReportTitle.Font.Style = [fsBold]
      ShrinkToPageWidth = True
      TimeFormat = 0
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      OptionsCards.AutoWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.BandHeader = cxStyle5
      Styles.Caption = cxStyle7
      Styles.Content = cxStyle14
      Styles.Footer = cxStyle6
      Styles.Group = cxStyle13
      Styles.Header = cxStyle3
      Styles.Selection = cxStyle4
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.LeftTitle.Strings = (
        '')
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'. [Page #]')
      PrinterPage.PageHeader.CenterTitle.Strings = (
        #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1083#1086#1082#1072#1094#1080#1080)
      PrinterPage.PageHeader.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageHeader.Font.Color = clBlack
      PrinterPage.PageHeader.Font.Height = -19
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = [fsBold]
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43845.451480960650000000
      ReportTitle.Font.Charset = RUSSIAN_CHARSET
      ReportTitle.Font.Color = clBlack
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Tahoma'
      ReportTitle.Font.Style = [fsBold]
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository2
      Styles.BandHeader = cxStyle8
      Styles.Caption = cxStyle9
      Styles.Footer = cxStyle10
      Styles.Header = cxStyle11
      Styles.Selection = cxStyle12
      BuiltInReportLink = True
    end
  end
  object dxPrintDialog1: TdxPrintDialog
    StyleManager = dxPrintStyleManager1
    Left = 737
    Top = 51
  end
  object dxPrintStyleManager1: TdxPrintStyleManager
    CurrentStyle = dxPrintStyleManager1Style1
    Version = 0
    Left = 772
    Top = 51
    object dxPrintStyleManager1Style2: TdxPSPrintStyle
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ReverseTitlesOnEvenPages = True
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInStyle = True
    end
    object dxPrintStyleManager1Style1: TdxPSPrintStyle
      PrinterPage.CenterOnPageH = True
      PrinterPage.CenterOnPageV = True
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.CenterTitle.Strings = (
        '')
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page # '#1086#1076' Pages #]')
      PrinterPage.PageHeader.CenterTitle.Strings = (
        'Printed On [Date & Time Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ReverseTitlesOnEvenPages = True
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInStyle = True
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DM1.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 912
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      BorderStyle = bbsNone
      Caption = ' '#1055#1056#1054#1052#1045#1053#1040'/'#1042#1053#1045#1057#1059#1042#1040#1034#1045' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 887
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = ' '#1055#1088#1077#1075#1083#1077#1076#1080' '
      CaptionButtons = <>
      DockedLeft = 262
      DockedTop = 0
      FloatLeft = 887
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = ' '#1055#1077#1095#1072#1090#1077#1114#1077' '
      CaptionButtons = <>
      DockedLeft = 398
      DockedTop = 0
      FloatLeft = 887
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 468
      DockedTop = 0
      FloatLeft = 887
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1052#1077#1088#1085#1072' '#1090#1086#1095#1082#1072
      CaptionButtons = <>
      DockedLeft = 149
      DockedTop = 0
      FloatLeft = 1025
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aMaticenBroj
      Category = 0
      LargeImageIndex = 12
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aSerBrojVodomer
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088' '
      Category = 0
      LargeImageIndex = 35
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aSnimiVoExcel
      Caption = #1057#1085#1080#1084#1080' '#1074#1086' Excel'
      Category = 0
      LargeImageIndex = 9
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      LargeImageIndex = 9
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aPecati
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aIzlez
      Category = 0
      LargeImageIndex = 25
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aMernaTocka
      Caption = #1042#1085#1077#1089#1080
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aBrisiMT
      Category = 0
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 877
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clBlack
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 842
    Top = 16
    PixelsPerInch = 96
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
  end
  object qMernaTocka: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_lokacii m'
      'set m.id_merna_tocka=:id_merna_tocka'
      'where m.id = :lokacija')
    Left = 824
    Top = 56
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object dsFakturi: TDataSource
    DataSet = tblFakruri
    Left = 984
    Top = 80
  end
  object tblFakruri: TpFIBDataSet
    RefreshSQL.Strings = (
      
        ' SELECT  KL.REON_ID,KL.CITACKA_KNIGA, kl.citacka_kniga_rb,kl.rb2' +
        ','
      
        'Kla.id_lokacija LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KL.TIP_PARTNER,' +
        'KLa.partner PARTNER_ID,MP.NAZIV,MP.ADRESA,mp.mail, KPG.IZNOS_VKU' +
        'PNO'
      ',NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUGA'
      
        ',KPG.VRABOTEN,lastdaymonth(cast('#39'01.'#39'||kpg.mesec||'#39'.'#39'||kpg.godin' +
        'a as date)) datum_presmetka, KPG.BROJ_FAKTURA'
      ',KL.STANBENA_POVRSINA, KL.DVORNA_POVRSINA'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0) ' +
        'ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) OSNOVICA_DANOK_05'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0)' +
        ' ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) OSNOVICA_DANOK_18'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) DANOK_05'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) DANOK_18,'
      
        'lastdaymonth(cast('#39'01.'#39'||kpg.mesec||'#39'.'#39'||kpg.godina as date))+15' +
        ' DA_SE_PLATI,'
      'kpg.id,'
      'kpg.ddo,'
      '(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      'where KVS.LOKACIIN_ID = kl.id and'
      '      KVS.MESEC = kpg.mesec and'
      '      KVS.GODINA = kpg.godina   ) vk_potroseno'
      'FROM KOM_PRESMETKA_G KPG'
      ', KOM_LOKACII KL, MAT_PARTNER MP, kom_lokacija_arhiva kla'
      'WHERE(  KPG.GODINA=:GODINA'
      'AND KPG.MESEC=:MESEC'
      'and kla.id_lokacija=kl.id'
      'and KPG.LOKACIIN_ID=KLA.ID_LOKACIJA'
      'AND KLA.TIP_PARTNER=MP.TIP_PARTNER'
      'AND kla.tip_partner like :tp'
      'AND KLa.PARTNER=MP.ID'
      'AND KL.REON_ID like :REON_ID'
      'and KPG.tip=1'
      
        'and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null) '
      
        '                       or ((encodedate(1,kpg.mesec,kpg.godina) >' +
        '= encodedate(1,kla.poc_mesec,kla.poc_godina)) and kla.kraj_mesec' +
        ' is null))'
      '     ) and (     KPG.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      
        ' SELECT  KL.REON_ID,KL.CITACKA_KNIGA, kl.citacka_kniga_rb,kl.rb2' +
        ','
      
        'Kla.id_lokacija LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KL.TIP_PARTNER,' +
        'KLa.partner PARTNER_ID,MP.NAZIV,MP.ADRESA,mp.mail, KPG.IZNOS_VKU' +
        'PNO'
      ',NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUGA'
      
        ',KPG.VRABOTEN,lastdaymonth(cast('#39'01.'#39'||kpg.mesec||'#39'.'#39'||kpg.godin' +
        'a as date)) datum_presmetka, KPG.BROJ_FAKTURA'
      ',KL.STANBENA_POVRSINA, KL.DVORNA_POVRSINA'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0) ' +
        'ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) OSNOVICA_DANOK_05'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0)' +
        ' ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) OSNOVICA_DANOK_18'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) DANOK_05'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.presmetka_g_id=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPG.GODINA'
      'AND KC.MESEC=KPG.MESEC'
      'AND KPG.TIP=1) DANOK_18,'
      
        'lastdaymonth(cast('#39'01.'#39'||kpg.mesec||'#39'.'#39'||kpg.godina as date))+15' +
        ' DA_SE_PLATI,'
      'kpg.id,'
      'kpg.ddo,'
      '(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      'where KVS.LOKACIIN_ID = kl.id and'
      '      KVS.MESEC = kpg.mesec and'
      '      KVS.GODINA = kpg.godina   ) vk_potroseno'
      'FROM KOM_PRESMETKA_G KPG'
      ', KOM_LOKACII KL, MAT_PARTNER MP, kom_lokacija_arhiva kla'
      'WHERE KPG.GODINA=:GODINA'
      'AND KPG.MESEC=:MESEC'
      'and kla.id_lokacija=kl.id'
      'and KPG.LOKACIIN_ID=KLA.ID_LOKACIJA'
      'AND KLA.TIP_PARTNER=MP.TIP_PARTNER'
      'AND kla.tip_partner like :tp'
      'AND KLa.PARTNER=MP.ID'
      'AND KL.REON_ID like :REON_ID'
      'and KPG.tip=1'
      
        'and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null) '
      
        '                       or ((encodedate(1,kpg.mesec,kpg.godina) >' +
        '= encodedate(1,kla.poc_mesec,kla.poc_godina)) and kla.kraj_mesec' +
        ' is null))'
      ''
      'order by kl.reon_id,kl.citacka_kniga,kl.citacka_kniga_rb,kl.rb2')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 928
    Top = 80
    object tblFakruriREON_ID: TFIBIntegerField
      FieldName = 'REON_ID'
    end
    object tblFakruriCITACKA_KNIGA: TFIBIntegerField
      FieldName = 'CITACKA_KNIGA'
    end
    object tblFakruriCITACKA_KNIGA_RB: TFIBIntegerField
      FieldName = 'CITACKA_KNIGA_RB'
    end
    object tblFakruriRB2: TFIBFloatField
      FieldName = 'RB2'
    end
    object tblFakruriLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object tblFakruriGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblFakruriMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblFakruriTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblFakruriPARTNER_ID: TFIBIntegerField
      FieldName = 'PARTNER_ID'
    end
    object tblFakruriNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakruriADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakruriMAIL: TFIBStringField
      FieldName = 'MAIL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakruriIZNOS_VKUPNO: TFIBBCDField
      FieldName = 'IZNOS_VKUPNO'
      Size = 2
    end
    object tblFakruriNAZIV1: TFIBStringField
      FieldName = 'NAZIV1'
      Size = 1
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakruriIZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 1
    end
    object tblFakruriDANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 1
    end
    object tblFakruriIZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 1
    end
    object tblFakruriVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakruriDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblFakruriBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFakruriSTANBENA_POVRSINA: TFIBIntegerField
      FieldName = 'STANBENA_POVRSINA'
    end
    object tblFakruriDVORNA_POVRSINA: TFIBIntegerField
      FieldName = 'DVORNA_POVRSINA'
    end
    object tblFakruriOSNOVICA_DANOK_05: TFIBBCDField
      FieldName = 'OSNOVICA_DANOK_05'
    end
    object tblFakruriOSNOVICA_DANOK_18: TFIBBCDField
      FieldName = 'OSNOVICA_DANOK_18'
    end
    object tblFakruriDANOK_05: TFIBBCDField
      FieldName = 'DANOK_05'
    end
    object tblFakruriDANOK_18: TFIBBCDField
      FieldName = 'DANOK_18'
    end
    object tblFakruriDA_SE_PLATI: TFIBDateTimeField
      FieldName = 'DA_SE_PLATI'
    end
    object tblFakruriID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblFakruriDDO: TFIBDateField
      FieldName = 'DDO'
    end
    object tblFakruriVK_POTROSENO: TFIBBCDField
      FieldName = 'VK_POTROSENO'
      Size = 0
    end
  end
end
