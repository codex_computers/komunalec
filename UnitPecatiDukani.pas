unit UnitPecatiDukani;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxGraphics, dxSkinsCore,
  dxSkinsDefaultPainters, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, dxSkinsdxBarPainter,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxBar, cxClasses, frxBarcode,
  frxDesgn, frxClass, frxDBSet, ActnList, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxGrid,
  ComCtrls, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, StdCtrls,
  cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxButtons, ExtCtrls, DateUtils,
  cxGroupBox, cxRadioGroup, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSpringTime, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,cxExport,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, cxNavigator, System.Actions, idcoderMIME,System.IOUtils, cxmemo,
  FIBQuery, pFIBQuery, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, GIFImg, IWVCLBaseControl, IWBaseControl,
  IWBaseHTMLControl, IWControl, IWCompExtCtrls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, jpeg, Vcl.DBCtrls,Vcl.Imaging.pngimage,
  frxExportMail, IdMailBox, frxOLE, IdAttachment, cxVariants,OleCtrls,ComObj,IdAttachmentFile, IdAttachmentMemory,
  frxExportPDF ;

type
  TfrmPecatiDukani = class(TForm)
    panelGoren: TPanel;
    lblMesec: TLabel;
    lblGodina: TLabel;
    lblReon: TLabel;
    cxBtnPresmetaj: TcxButton;
    cxComboBoxGodini: TcxComboBox;
    cxComboBoxMesec: TcxComboBox;
    GroupBoxPoslednaPresmetka: TGroupBox;
    lblMesecLast: TLabel;
    lblMesecLastValue: TLabel;
    lblGodinaLast: TLabel;
    lblGodinaLastValue: TLabel;
    cxBtnIzbrisiPoslednaPresmetka: TcxButton;
    cxLookupCBReoni: TcxLookupComboBox;
    StatusBar1: TStatusBar;
    panelDolen: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ActionList1: TActionList;
    aIzlez: TAction;
    aIzbrisiLastPresmetka: TAction;
    aCallFrfBeleskiPrint: TAction;
    aPrintAllBeleskiFizicki: TAction;
    aPrintBeleskaOdberenFizicki: TAction;
    aPrintAllFakturi: TAction;
    aEdnaFaktura: TAction;
    frxDSPresmetkaG: TfrxDBDataset;
    frxDSPresmetkaS: TfrxDBDataset;
    frxDSPresmetkaVodomeriSostojba: TfrxDBDataset;
    frxBarCodeObject1: TfrxBarCodeObject;
    dxBarManager1: TdxBarManager;
    SubMenuAkcii: TdxBarSubItem;
    BarButtonIzlez: TdxBarButton;
    BarButtonPrintBeleskiAll: TdxBarButton;
    BarButtonPrintBeleskaSelected: TdxBarButton;
    BarButtonPrintFakturiAll: TdxBarButton;
    BarButtonIzbrisiPresmetka: TdxBarButton;
    tblsaldo: TfrxDBDataset;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxRadioGroup1: TcxRadioGroup;
    frxDSPresmetkaStavki1: TfrxDBDataset;
    frxDSPresmetkaStavki2: TfrxDBDataset;
    frxDSPresmetkaStavki3: TfrxDBDataset;
    frxDSPresmetkaStavki4: TfrxDBDataset;
    frxDSPresmetkaStavki5: TfrxDBDataset;
    frxDSPresmetkaStavki6: TfrxDBDataset;
    frxDSPresmetkaStavki7: TfrxDBDataset;
    frxReport1: TfrxReport;
    frxDSPresmetkaStavki8: TfrxDBDataset;
    frxDSPresmetkaStavki10: TfrxDBDataset;
    frxDSPresmetkaStavki61: TfrxDBDataset;
    frxDSPresmetkaStavki11: TfrxDBDataset;
    frxDSPresmetkaStavki12: TfrxDBDataset;
    frxDSPresmetkaStavki121: TfrxDBDataset;
    aExporttoExcel: TAction;
    aMailFaktura: TAction;
    aMailFakturaDizajn: TAction;
    aNovIzbledB: TAction;
    aNovIzgledBDizajn: TAction;
    aNovIzgledSiteB: TAction;
    cxMemo1: TcxMemo;
    qUpdateImaSlika: TpFIBQuery;
    IWImage1: TIWImage;
    IdHTTP1: TIdHTTP;
   // dbmimg:TDBImage;
    IdHTTP2: TIdHTTP;
    dbimgIMG: TDBImage;
    aPratiPoEmailFakturiGrupno: TAction;
    frxOLEObject1: TfrxOLEObject;
    IdMailBox1: TIdMailBox;
    frxMailExport1: TfrxMailExport;
    aPratiPoEmailPoedinecno: TAction;
    procedure FormShow(Sender: TObject);
    procedure aPrintAllBeleskiFizickiExecute(Sender: TObject);
    procedure aPrintBeleskaOdberenFizickiExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function podatociOk():boolean;
    procedure cxBtnPresmetajClick(Sender: TObject);
    procedure closePresmetkaDatasets();
    procedure aCallFrfBeleskiPrintExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aEdnaFakturaExecute(Sender: TObject);
    procedure aPrintAllFakturiExecute(Sender: TObject);
    procedure aExporttoExcelExecute(Sender: TObject);
    procedure aMailFakturaExecute(Sender: TObject);
    procedure aMailFakturaDizajnExecute(Sender: TObject);
    procedure aNovIzbledBExecute(Sender: TObject);
    procedure aNovIzgledBDizajnExecute(Sender: TObject);
    procedure aNovIzgledSiteBExecute(Sender: TObject);
    function CompressJpeg(OutJPG: TJPEGImage): Integer;
    procedure aPratiPoEmailFakturiGrupnoExecute(Sender: TObject);
    procedure PecatiPotvrda(preview : boolean; po_mail : boolean);
    procedure domail(recname: string; recmail: string; br_fak: string; mesec: string; godina: string; ddo: string; iznos: Real);
    procedure aPratiPoEmailPoedinecnoExecute(Sender: TObject);
  //  procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPecatiDukani: TfrmPecatiDukani;

implementation

uses dmKonekcija, dmMaticni, UnitDM1,utils, dmResources, DaNe;

{$R *.dfm}

procedure TfrmPecatiDukani.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if DM1.tblPecDukani.Active then
    begin
        DM1.tblPecDukani.Close();
    end;
    if DM1.TblPresmetkaG.Active then
    begin
        DM1.TblPresmetkaG.Close();
    end;

    Action := caFree;
end;

procedure TfrmPecatiDukani.FormShow(Sender: TObject);
begin
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)));
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)-1));

end;

procedure TfrmPecatiDukani.onKeyDownLookUpAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            begin
                PostMessage(Handle, WM_NEXTDLGCTL,0,0);
            end;

        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;

end;

procedure TfrmPecatiDukani.aCallFrfBeleskiPrintExecute(Sender: TObject);
begin
   frxReport1.DesignReport();
  //dmRes.frxReport1.DesignReport;
end;

procedure TfrmPecatiDukani.aEdnaFakturaExecute(Sender: TObject);
var uplateno:real;
begin
    uplateno := 0;
    if DM1.tblPecDukani.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));

        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;

end;

procedure TfrmPecatiDukani.aExporttoExcelExecute(Sender: TObject);
begin
//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
  zacuvajVoExcel(cxGrid1, Caption);

end;

procedure TfrmPecatiDukani.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmPecatiDukani.aMailFakturaDizajnExecute(Sender: TObject);
begin
   if dm1.tblPecDukani.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,32);
      //dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
      dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Open;
      dmRes.frxReport1.DesignReport();
    end;
end;

procedure TfrmPecatiDukani.aMailFakturaExecute(Sender: TObject);
var memStream: TMemoryStream;
status: TStatusWindowHandle;
begin
//    dm1.tblPecDukani.close;
//    TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
//                                                      ' and kla.id_lokacija='+cxGrid1DBTableView1LOKACIJA.EditValue+
//                                                      ' and kpg.broj_faktura ='''+cxGrid1DBTableView1BROJ_FAKTURA.EditValue+
//                                                      ''' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;
//
//    TblPresmetkaG.open;
//    TblPresmetkaS.Open;
//    TblPresmetkaVodomeriSostojba.Open;
//
//   TblSaldo.Close;
  // frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);

   if dm1.tblPecDukani.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,32);
      //dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
      dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Open;
      status := cxCreateStatusWindow();
     try
          memStream := TMemoryStream.Create;
          dmRes.frxPDFExport1.Stream := memStream;
          memStream.Position := 0;
          dmRes.frxReport1.PrepareReport(True);
          dmRes.frxReport1.Export(dmRes.frxPDFExport1);

      //     if dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([DM1.tblKarParTIP_PARTNER.AsString, DM1.tblKarParID.AsString]) , []) then
              dmRes.SendIndyEmail(dm1.tblPecDukaniMAIL.Value, '������� �� ������� (' + dm1.tblPecDukaniNAZIV.Value + ')', '������� �� �������', '', memStream, 'faktura');
            frxDSPresmetkaG.RangeBegin := rbFirst;
            frxDSPresmetkaG.RangeEnd := reLast;
        finally
          FreeAndNil(MemStream);
          dmRes.frxPDFExport1.Stream := nil;
          cxRemoveStatusWindow(status);
        end;

//   frmEFakturi := TfrmEFakturi.Create(Self);
//   // frmEFakturi.aEFakturi.Execute;
//   frmEFakturi.ShowModal;
//    frmEFakturi.Free;

    end;
end;

procedure TfrmPecatiDukani.aNovIzbledBExecute(Sender: TObject);
var mesec, line, ima_slika:string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i:integer;
    MS : TMemoryStream;
    GIf: TJPEGImage;
begin
   ima_slika := '0';
   if dm1.tblPecDukani.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,35);
      //dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.tblPecDukaniGODINA.AsString;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.tblPecDukaniMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := dm1.tblPecDukaniTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.tblPecDukaniREON_ID.AsString;
      dmKon.tblSqlReport.Open;
    //  frxReportKP.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(dm1.tblKarParPOBARUVA.asstring));
    //  frxReportKP.DesignReport();

       if servis = '1' then
       begin
        if dm1.tblPecDukanilink_slika.AsString <> '' then
            begin
                  try
                      MS := TMemoryStream.Create;
                      GIf := TJPEGImage.Create;

                      IdHTTP2.get(dm1.tblPecDukaniLINK_SLIKA.AsString,MS);
                      Ms.Seek(0,soFromBeginning);
                      Gif.LoadFromStream(MS);
                      dbimgIMG.Picture.Graphic.Assign(GIf);
                      CompressJpeg(gif);
                   //   dbimgIMG.Picture.Bitmap.SaveToFile('c:\cxCache\MyImage'+dm1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');
                 finally
                        FreeAndNil(GIF);
                        FreeAndNil(MS);;
                end;
                 ima_slika := '1';
               end
               else
                ima_slika := '0';


       end;
      dmres.frxReport1.Variables.AddVariable('Promenlivi','erk', servis);
      dmres.frxReport1.Variables.AddVariable('Promenlivi','ima_slika',ima_slika);
      dmRes.frxReport1.ShowReport;

      if FileExists('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg') then
          begin
             DeleteFile('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');
             //DeleteFile('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.txt')
          end;

      end;

end;


procedure TfrmPecatiDukani.aNovIzgledBDizajnExecute(Sender: TObject);
var mesec, line, ima_slika:string;
    sImageStr: String;
    img1 : TDBImage;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i:integer;
    MS : TMemoryStream;
    GIf: TJPEGImage;
begin
   if dm1.tblPecDukani.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,35);
     // dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.tblPecDukaniGODINA.AsString;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.tblPecDukaniMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := dm1.tblPecDukaniTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.tblPecDukaniREON_ID.AsString;
      dmKon.tblSqlReport.Open;
    //  frxReportKP.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(dm1.tblKarParPOBARUVA.asstring));
    //  frxReportKP.DesignReport();
           if servis = '1' then
       begin
       try
        if not dm1.IsInternetConnected then
        Except on E:Exception do
        begin
           ShowMessage('���� �������� ��������!');
          Abort;
        end
       end;
        if dm1.tblPecDukanilink_slika.AsString <> '' then
            begin
                  try
                      MS := TMemoryStream.Create;
                      GIf := TJPEGImage.Create;

                      IdHTTP2.get(dm1.tblPecDukaniLINK_SLIKA.AsString,MS);
                      Ms.Seek(0,soFromBeginning);
                      Gif.LoadFromStream(MS);
                      dbimgIMG.Picture.Graphic.Assign(GIf);
                      CompressJpeg(gif);
                   //   dbimgIMG.Picture.Bitmap.SaveToFile('c:\cxCache\MyImage'+dm1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');
                 finally
                        FreeAndNil(GIF);
                        FreeAndNil(MS);;
                end;
                 ima_slika := '1';
               end
               else
                ima_slika := '0';
       end;
      dmres.frxReport1.Variables.AddVariable('Promenlivi','erk', servis);
      dmres.frxReport1.Variables.AddVariable('Promenlivi','ima_slika',ima_slika);
      dmRes.frxReport1.DesignReport;

      if FileExists('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg') then
          begin
             DeleteFile('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');
             //DeleteFile('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.txt')
          end;

      end;
end;

procedure TfrmPecatiDukani.aNovIzgledSiteBExecute(Sender: TObject);
var mesec, line, ima_slika, smeni_red,lokacija, tf_slika:string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i,j:integer;
    MS : TMemoryStream;
    GIf: TJPEGImage;
begin
  ima_slika := '0';
//�� �� ����� ������� �� ����
 if servis = '1' then
  begin
    try
      if not dm1.IsInternetConnected then

      Except on E:Exception do
        begin
           ShowMessage('���� �������� ��������!');
          Abort;
        end
      end;
      dm1.tblPecDukani.First;
    while not dm1.tblPecDukani.Eof do
    begin
    if length(DM1.tblPecDukaniMESEC.AsString) = 1 then
      mesec := '0'+DM1.tblPecDukaniMESEC.AsString
    else mesec := DM1.tblPecDukaniMESEC.AsString;

        if dm1.tblPecDukanilink_slika.AsString <> '' then
            begin
                  try
                      MS := TMemoryStream.Create;
                      GIf := TJPEGImage.Create;

                      IdHTTP2.get(dm1.tblPecDukaniLINK_SLIKA.AsString,MS);
                      Ms.Seek(0,soFromBeginning);
                      Gif.LoadFromStream(MS);
                      dbimgIMG.Picture.Graphic.Assign(GIf);
                      CompressJpeg(gif);
                 finally
                        FreeAndNil(GIF);
                        FreeAndNil(MS);;
                end;
                 ima_slika := '1';
                  qUpdateImaSlika.Close;
                  qUpdateImaSlika.ParamByName('ima_slika').Value := 1;
                  qUpdateImaSlika.ParamByName('lokacija').AsString := dm1.tblPecDukaniLOKACIIN_ID.AsString;
                  qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
                  qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
                  qUpdateImaSlika.ExecQuery;
               end
               else
               begin
                ima_slika := '0';
                 qUpdateImaSlika.Close;
                 qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
                 qUpdateImaSlika.ParamByName('lokacija').AsString := dm1.tblPecDukaniLOKACIIN_ID.AsString;
                 qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
                 qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                 qUpdateImaSlika.ParamByName('id').Value := DM1.TblPresmetkaGID.Value;
                 qUpdateImaSlika.ExecQuery;
                end;
           //     end;
         //    end;
          //    end ;

//             else
//             begin
//                 qUpdateImaSlika.Close;
//                 qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
//                 qUpdateImaSlika.ParamByName('lokacija').AsString := lokacija;
//                 qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                 qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
////                 qUpdateImaSlika.ParamByName('id').Value := DM1.TblPresmetkaGID.Value;
//                 qUpdateImaSlika.ExecQuery;
//
////              ima_slika := '0';
           //  end;
               i := i+1;
               DM1.tblPecDukani.Next;
           end;
             //  cel_zapis := '';
             //  DM1.TblPresmetkaG.Next;
             dm1.tblPecDukani.FullRefresh;
             end;
 //      end;

//
// end;
//
// end;
   if dm1.tblPecDukani.RecordCount > 0 then
    begin
      dmRes.Spremi(dmKon.aplikacija,35);
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.tblPecDukaniGODINA.AsString;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.tblPecDukaniMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := dm1.tblPecDukaniTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.tblPecDukaniREON_ID.AsString;
      dmKon.tblSqlReport.Open;
      dmres.frxReport1.Variables.AddVariable('Promenlivi','ima_slika','0');
      dmres.frxReport1.Variables.AddVariable('Promenlivi','erk',servis);

      dmRes.frxReport1.PrepareReport(true);
      dmRes.frxReport1.ShowPreparedReport;
  //   if servis = '0' then
  //    begin
      dm1.tblPecDukani.First;
      while not dm1.tblPecDukani.eof do
      begin
        qUpdateImaSlika.Close;
        qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
        qUpdateImaSlika.ParamByName('lokacija').Value := dm1.tblPecDukaniLOKACIIN_ID.Value;
        qUpdateImaSlika.ParamByName('mesec').Value := dm1.tblPecDukaniMESEC.Value;
        qUpdateImaSlika.ParamByName('godina').Value := dm1.tblPecDukaniGODINA.Value;
        qUpdateImaSlika.ExecQuery;
        if FileExists('c:\cxCache\MyImage'+dm1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg') then
      begin
         DeleteFile('c:\cxCache\MyImage'+dm1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');
      end;
        dm1.tblPecDukani.Next;
      end;

    //  end;
    end;


end;

procedure TfrmPecatiDukani.aPratiPoEmailFakturiGrupnoExecute(Sender: TObject);
var
   status: TStatusWindowHandle;
begin
  if cxLookupCBReoni.Text = '30' then
  begin
      if (cxGrid1DBTableView1.DataController.FilteredRecordCount <> 0) then
      begin
        frmDaNe := TfrmDaNe.Create(self, '�������!', '�������� ������ ������� �� ������� �� ����� �� ���� ��������. ���� ��� �������?', 1);
        if (frmDaNe.ShowModal = mrYes) then
        begin
          status := cxCreateStatusWindow();
          DM1.tblPecDukani.First;
          while not DM1.tblPecDukani.eof do
           begin
            if not ((DM1.tblPecDukaniMAIL.IsNull) or (DM1.tblPecDukaniMAIL.AsString = '')) then
            begin
               PecatiPotvrda(false, true);
               domail(DM1.tblPecDukaniNAZIV.Value,DM1.tblPecDukaniMAIL.Value, DM1.tblPecDukaniBROJ_FAKTURA.Value, DM1.tblPecDukaniMESEC.AsString, dm1.tblPecDukaniGODINA.AsString, DM1.tblPecDukaniDDO.AsString,DM1.tblPecDukaniIZNOS_VKUPNO.Value);
            end;
             DM1.tblPecDukani.Next;
          end;
          cxRemoveStatusWindow(status);
        end;

      end; // kraj za ako ne e prazen gridot za selekcija
  end;
end;

procedure TfrmPecatiDukani.aPratiPoEmailPoedinecnoExecute(Sender: TObject);
var
  status: TStatusWindowHandle;
begin
  if cxLookupCBReoni.Text = '30' then
  begin
      if (cxGrid1DBTableView1.DataController.FilteredRecordCount <> 0) then
      begin
        frmDaNe := TfrmDaNe.Create(self, '�������!', '�������� �� ������� ������� �� �����. ���� ��� �������?', 1);
        if (frmDaNe.ShowModal = mrYes) then
        begin
           status := cxCreateStatusWindow();
        //  if not DM1.tblPecDukaniMAIL.IsNull then
          if not ((DM1.tblPecDukaniMAIL.IsNull) or (DM1.tblPecDukaniMAIL.Value = '')) then
            begin
              PecatiPotvrda(false, true);
              domail(DM1.tblPecDukaniNAZIV.Value,DM1.tblPecDukaniMAIL.Value, DM1.tblPecDukaniBROJ_FAKTURA.Value, DM1.tblPecDukaniMESEC.AsString, dm1.tblPecDukaniGODINA.AsString, DM1.tblPecDukaniDDO.AsString,DM1.tblPecDukaniIZNOS_VKUPNO.Value);
            end
            else
            begin
              ShowMessage('���� ������� ���� ������ �� ���������!');
//              Abort;
            end;
            cxRemoveStatusWindow(status);
        end;

      end; // kraj za ako ne e prazen gridot za selekcija
  end;
end;

procedure TFrmPecatiDukani.aPrintAllBeleskiFizickiExecute(Sender: TObject);
begin
    if DM1.tblPecDukani.RecordCount > 0 then
    begin
        frxReport1.LoadFromFile('FRBeleskaFizickoLice.fr3');
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
   // DM1.TblDPresmetkaS.Close;
   // DM1.tblDPrsmetkaVodomeriSostojba.Close;
   // dm1.tblDSaldo.Close;
end;

procedure TfrmPecatiDukani.aPrintAllFakturiExecute(Sender: TObject);
var uplateno:real;
begin
    uplateno := 0;
    if DM1.tblPecDukani.RecordCount > 0 then
    begin
        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();
    end
    else
    begin

        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TFrmPecatiDukani.aPrintBeleskaOdberenFizickiExecute(Sender: TObject);
begin
//    DM1.tblPecDukani.Close;
//    if (cxRadioGroup1.ItemIndex = 0) or (cxRadioGroup1.ItemIndex = -1) then
//        begin
//               DM1.tblPecDukani.ParamByName('TP').AsVariant := '%';
//
//        end;
//        if cxRadioGroup1.ItemIndex=1 then
//               DM1.tblPecDukani.ParamByName('TP').AsVariant := '5';
//        if cxRadioGroup1.ItemIndex=2 then
//               DM1.tblPecDukani.ParamByName('TP').AsVariant := '6';
//        if cxRadioGroup1.ItemIndex=3 then
//               DM1.tblPecDukani.ParamByName('TP').AsVariant := '1';
//
//        if cxLookupCBReoni.Text<>'' then
//            DM1.tblPecDukani.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue
//        else
//            DM1.tblPecDukani.ParamByName('REON_ID').AsVariant :='%';
//        DM1.tblPecDukani.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
//        DM1.tblPecDukani.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
//
//        DM1.tblPecDukani.Open;
    if DM1.tblPecDukani.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

        frxReport1.LoadFromFile('FRBeleskaFizickoLice.fr3');
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
  //  DM1.TblDPresmetkaS.Close;
//    DM1.tblDPrsmetkaVodomeriSostojba.Close;
  //  dm1.tblDSaldo.Close;
end;

procedure TfrmPecatiDukani.cxBtnPresmetajClick(Sender: TObject);
begin
    if podatociOk then
    begin
        closePresmetkaDatasets;

        if (cxRadioGroup1.ItemIndex = 0) or (cxRadioGroup1.ItemIndex = -1) then
        begin
               DM1.tblPecDukani.ParamByName('TP').AsVariant := '%';

        end;
        if cxRadioGroup1.ItemIndex=1 then
               DM1.tblPecDukani.ParamByName('TP').AsVariant := '5';
        if cxRadioGroup1.ItemIndex=2 then
               DM1.tblPecDukani.ParamByName('TP').AsVariant := '6';
        if cxRadioGroup1.ItemIndex=3 then
               DM1.tblPecDukani.ParamByName('TP').AsVariant := '1';

        if cxLookupCBReoni.Text<>'' then
            DM1.tblPecDukani.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue
        else
            DM1.tblPecDukani.ParamByName('REON_ID').AsVariant :='%';
        DM1.tblPecDukani.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        DM1.tblPecDukani.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;

        DM1.tblPecDukani.Open;
        DM1.tblDPresmetkaS.Open;
        DM1.tblDPrsmetkaVodomeriSostojba.Open;
        DM1.tblDSaldo.Open;

        cxGrid1.SetFocus();

    end;
end;

{procedure TFrmPectiDukani.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if DM1.tblPecDukani.Active then
    begin
        DM1.tblPecDukani.Close();
    end;

    Action := caFree;
end;
 }

function TFrmPecatiDukani.podatociOk():boolean;
var ret:boolean;
begin
    ret := false;
    if cxComboBoxGodini.Text = '' then
    begin
        cxComboBoxGodini.SetFocus();
        ShowMessage('������ �� �������� ������!');
    end
    else if cxComboBoxMesec.Text = '' then
    begin
        cxComboBoxMesec.SetFocus();
        ShowMessage('������ �� �������� �����!');
    end
    //else if cxLookupCBReoni.Text = '' then
    //begin
      //  cxLookupCBReoni.SetFocus;
       // ShowMessage('������ �� �������� ����!');
    //end

    else
            begin
                ret := true;
            end;
         podatociOk := ret;
    end;

procedure TFrmPecatiDukani.closePresmetkaDatasets();
begin
    if DM1.TblPecDukani.Active then
    begin
        DM1.TblPecDukani.Close;
        DM1.TblDPresmetkaS.Close;
        DM1.tblDPrsmetkaVodomeriSostojba.Close;
        dm1.tblDSaldo.Close;
    end;
end;

function TFrmPecatiDukani.CompressJpeg(OutJPG: TJPEGImage): Integer;
VAR tmpQStream: TMemoryStream;
begin
 tmpQStream:= TMemoryStream.Create;
 TRY
   OutJPG.Compress;
   OutJPG.SaveToStream(tmpQStream);
   OutJPG.SaveToFile('c:\cxCache\MyImage'+dm1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');    // You can remove this line.
   tmpQStream.Position := 0;                //
   OutJPG.LoadFromStream(tmpQStream);       // Reload the jpeg stream to OutJPG
   Result:= tmpQStream.Size;
 FINALLY
   FreeAndNil(tmpQStream);
 END;
end;

procedure TFrmPecatiDukani.PecatiPotvrda(preview : boolean; po_mail : boolean);
var
//  status: TStatusWindowHandle;
  pdfname, pdfurl:string;
  pdfStream : TFileStream;
  status1 : Boolean;
begin
  try
      frxDSPresmetkaG.RangeBegin := rbCurrent;
      frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi('KOM',40);
      dmKon.tblSqlReport.Close;
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.tblPecDukaniGODINA.AsString;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.tblPecDukaniMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := dm1.tblPecDukaniTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.tblPecDukaniREON_ID.AsString;
      dmKon.tblSqlReport.Params.ParamByName('lokacija').AsString := dm1.tblPecDukaniLOKACIIN_ID.AsString;
      dmKon.tblSqlReport.Prepare;
      dmKon.tblSqlReport.Open;

      dmRes.frxReport1.PrepareReport(true);

    //  status := cxCreateStatusWindow();

          dmres.frxPDFExport1.FileName:='�������_'+dm1.tblPecDukaniBROJ_FAKTURA.Value+'.pdf';
         try
           pdfStream := TFileStream.Create(dmKon.RabotenDir+'�������_'+dm1.tblPecDukaniBROJ_FAKTURA.Value+'.pdf', fmCreate or fmShareDenyNone);
           try
             dmres.frxPDFExport1.Stream := pdfStream;
             try
                dmres.frxReport1.Export(dmres.frxPDFExport1);
             except on Exception do  status1:=false;
             end;

          finally
            FreeAndNil(pdfStream);
            dmres.frxPDFExport1.Stream := NIL;
          end;
          except on Exception do
      end;
        //  cxRemoveStatusWindow(status);

  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;

end;


procedure TFrmPecatiDukani.domail(recname: string; recmail: string; br_fak: string; mesec: string; godina: string; ddo: string; iznos: Real);
const
  olMailItem = 0;
var
  Outlook: OLEVariant;
  MailItem: Variant;
  MailInspector : Variant;
  stringlist : TStringList;
  memStream: TMemoryStream;
 // status: TStatusWindowHandle;
begin

 try
   Outlook:=GetActiveOleObject('Outlook.Application') ;
  except
   Outlook:=CreateOleObject('Outlook.Application') ;
  end;
  try
    Stringlist := TStringList.Create;
    MailItem := Outlook.CreateItem(olMailItem) ;
    MailItem.Subject := '�� ��������� �-������ ��.'+br_fak+' �� '+mesec+'/'+godina;
    MailItem.Recipients.Add(recmail);
   // MailItem.CC:='ABC@GMAIL.COM';

    MailItem.Attachments.Add(dmKon.RabotenDir+'�������_'+dm1.tblPecDukaniBROJ_FAKTURA.Value+'.pdf');

    Stringlist := TStringList.Create;
    StringList.Add('������ �� �����: '+mesec+'/'+godina+#10+'��� �� �������: '+br_fak+#10+'��� �� �������: '+ddo+#10+'�����: '+FormatFloat('#,##0.00', iznos)+' ���.');
    MailItem.Body := StringList.text;
    stringlist.Add(#10+'�� �������� ������� �� ���������� ������� �� ����������� ���.');
    MailItem.body := stringlist.Text;
    stringlist.Add(#10+'��������� ������ � �������� ������� ������� ��������� �/��� ������������� ���������� ��� �� ��������� �������� �� ������ �� ����'
                +' � ��������� ���� ������. A�o �� ��� ������ �� ��� �� �������� ���� ������ � �������� �������, ������� �������� �� �������� �'
                +' ��������� ��� �� � �� ���������� �� ��������, ���� � ����� �������  � ������� �������������, �������� � ������������� � ������ ���������.'
                +' ��� ��� �� ������ �������� �� ������, �� ������ ������ �� �� ������������'
                +' ���������� �� �������� �������༝� �� ��� � ������ ������� �� �� ��������� �������� � �������� ������� �� ������ ������.');
    MailItem.body := stringlist.Text;
    MailItem.save;   //SENDS A MAIL WITH OUT OUTLOOK WINDOW. USE "SAVE" FOR DRAFT
{
  // TO SHOW OUTLOOK DIALOG. BUT YOU HAVE SET MAILITEM.SEND AS COMMENT
    MailInspector := MailItem.GetInspector;
    MailInspector.display(FALSE); //true means modal
    MailInspector.Send;
}   DeleteFile(dmKon.RabotenDir+'�������_'+dm1.tblPecDukaniBROJ_FAKTURA.Value+'.pdf');
  finally
    Outlook := Unassigned;
    StringList.Free;
  end;
end;

end.
