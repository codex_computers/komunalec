unit UnitOpomeni;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  cxLookAndFeelPainters, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit,
  cxGroupBox, cxTextEdit, cxControls, cxContainer, cxEdit, cxLabel, Menus,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls,
  cxButtons, ActnList, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  dxStatusBar, dxRibbonStatusBar, cxGridCustomPopupMenu, cxGridPopupMenu,
  FIBQuery, pFIBQuery, dateutils, FIBDataSet, pFIBDataSet, cxGridExportLink,
  cxLookAndFeels;

type
  TfrmOpomena = class(TForm)
    ActionList1: TActionList;
    pcOpomeni: TPageControl;
    tsOpomeni: TTabSheet;
    tsPresmetkiOpomeni: TTabSheet;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    txtBroj: TcxDBTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aZapisi: TAction;
    aIzlez: TAction;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    txtDolg: TcxDBTextEdit;
    txtBeleski: TcxDBTextEdit;
    cbTP: TcxDBLookupComboBox;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1DOLG: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_NEPLATENI_BEL: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    qBrisiOPPresmetka: TpFIBQuery;
    StatusBar1: TStatusBar;
    qImaOpomena: TpFIBQuery;
    aPecati: TAction;
    cxLabel7: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxComboBoxMesec: TcxDBComboBox;
    cxComboBoxGodini: TcxDBComboBox;
    cxComboBox1: TcxDBComboBox;
    cxComboBox2: TcxDBComboBox;
    cxLabel2: TcxLabel;
    cxComboBoxReon: TcxDBLookupComboBox;
    tblNeplateniPresmetki: TpFIBDataSet;
    dsNeplateniBeleski: TDataSource;
    tblNeplateniPresmetkiID: TFIBIntegerField;
    qUpdatePresmetka: TpFIBQuery;
    cxGridDBTableView1BROJ: TcxGridDBColumn;
    cxGridDBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_TP: TcxGridDBColumn;
    cxGridDBTableView1DOLG: TcxGridDBColumn;
    cxGridDBTableView1BROJ_NEPLATENI_BEL: TcxGridDBColumn;
    cxGridDBTableView1REON_ID: TcxGridDBColumn;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1NAZIV: TcxGridDBColumn;
    cxGridDBTableView1ADRESA: TcxGridDBColumn;
    cxGridDBTableView1DATUM_OD: TcxGridDBColumn;
    cxGridDBTableView1DATUM_DO: TcxGridDBColumn;
    cxGridDBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGridDBTableView1DOLZEN: TcxGridDBColumn;
    cxGridDBTableView1STARO_SLADO: TcxGridDBColumn;
    aSnimiVoExcel: TAction;
    cxGrid1DBTableViewREON: TcxGridDBColumn;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure pcOpomeniChange(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aSnimiVoExcelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOpomena: TfrmOpomena;

implementation

uses dmKonekcija, dmMaticni, UnitDM1, DaNe;

{$R *.dfm}

procedure TfrmOpomena.aAzurirajExecute(Sender: TObject);
begin
      pcOpomeni.ActivePage:=tsOpomeni;
      Panel1.Enabled:=True;
      txtBroj.SetFocus;
      dm1.tblOpomeni.Edit;
end;

procedure TfrmOpomena.aBrisiExecute(Sender: TObject);
begin
   frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort
    else
    begin
      qBrisiOPPresmetka.close;
      qBrisiOPPresmetka.ParamByName('opomena').AsString:=DM1.tblOpomeniBROJ.Value;
      qBrisiOPPresmetka.ExecQuery;

      dm1.tblOpomeni.Delete;
    end;
end;

procedure TfrmOpomena.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmOpomena.aNovExecute(Sender: TObject);
begin
      pcOpomeni.ActivePage:=tsOpomeni;
   //   dm1.tblOpomeni.cancel;
      Panel1.Enabled:=True;
      txtBroj.SetFocus;
      dm1.tblOpomeni.Insert;
   //
end;

procedure TfrmOpomena.aPecatiExecute(Sender: TObject);
begin
    dm1.ShowReport2('KOM',14,'broj',dm1.tblOpomeniBROJ.AsString);
end;

procedure TfrmOpomena.aSnimiVoExcelExecute(Sender: TObject);
begin
    dmMat.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
    if(dmMat.SaveToExcelDialog.Execute)then
        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid2,true, true, false);
end;

procedure TfrmOpomena.aZapisiExecute(Sender: TObject);
var sostojba:TDataSetState; d1,d2:TDate; m1,m2,s1,s2:string;
begin
  //proverka na zadolzitelmite polinja
    sostojba:=DM1.tblOpomeni.State;
    if (txtBroj.Text<>'') and (cxComboBoxMesec.text<>'')
    and (cxComboBoxGodini.text<>'')
    and (cxComboBox1.text<>'')
    and (cxComboBox2.text<>'')
    then
    begin
     if Length(cxComboBoxMesec.text)=1 then m1:='0'else m1:='';
     if Length(cxComboBox1.text)=1 then m2:='0'else m2:='';
     s1:='01.'+m1+cxComboBoxMesec.text+'.'+cxComboBoxGodini.Text;
     s2:='01.'+m2+cxComboBox1.text+'.'+cxComboBox2.Text;

     //proverka dali e soodvetno izbran periodot t.e. dane ima vekje opomena za toj period
    if sostojba=dsInsert then
     begin
         qImaOpomena.close;
         qImaOpomena.ParamByName('d1').AsString:=s1;
         qImaOpomena.ParamByName('d2').AsString:=s2;
         if cxComboBoxReon.text<>'' then
              qImaOpomena.ParamByName('reon').AsString:=cxComboBoxReon.text
         else
              qImaOpomena.ParamByName('reon').AsString:='%';

         if cbTP.text<>'' then
              qImaOpomena.ParamByName('tp').AsString:=cbTP.text
         else
              qImaOpomena.ParamByName('tp').AsString:='%';
         qImaOpomena.ExecQuery;
         if qImaOpomena.FldByName['ima'].AsInteger>0 then
         begin
           ShowMessage('�� ��������� ������, ��� ��� ��������� �������! �������� ���� ������.');
           cxComboBoxReon.SetFocus;
           Abort;
         end;
     end;

     //nova stavka vo KOM_OPOMENA
    //  sostojba:=DM1.tblOpomeni.State;
      dm1.tblOpomeniDATUM_OD.AsString:=s1;
      DM1.tblOpomeniDATUM_DO.AsString:=s2;
      dm1.tblOpomeni.Post;
    // update na poleto opomena_id vo Presmetka_g
    if sostojba=dsEdit then
    begin
        //treba da se update poleto OPOMENA_BROJ vo PRESMETKA_G
        qBrisiOPPresmetka.close;
        qBrisiOPPresmetka.ParamByName('opomena').AsString:=DM1.tblOpomeniBROJ.Value;
        qBrisiOPPresmetka.ExecQuery;
    end;
      dm1.pOpomena.Close;
      dm1.pOpomena.ParamByName('d1').AsString:=s1;
      dm1.pOpomena.ParamByName('d2').AsString:=s2;
      if cbTP.Text='' then
         dm1.pOpomena.ParamByName('tp').AsString:='%'
      else
         dm1.pOpomena.ParamByName('tp').AsString:=cbTP.Text;
      if cxComboBoxReon.Text='' then
         dm1.pOpomena.ParamByName('reon').AsString:='%'
      else
         dm1.pOpomena.ParamByName('reon').AsString:=cxComboBoxReon.Text;
      if txtDolg.text='' then
         dm1.pOpomena.ParamByName('suma').AsString:='0'
      else
         dm1.pOpomena.ParamByName('suma').AsString:=txtDolg.Text;
     if txtBroj.Text='' then
         dm1.pOpomena.ParamByName('broj').AsString:='0'
      else
         dm1.pOpomena.ParamByName('broj').AsString:=txtBeleski.Text;
     dm1.pOpomena.ParamByName('opomena').AsString:=dm1.tblOpomeniBROJ.Value;
     dm1.pOpomena.ExecProc;


//      tblNeplateniPresmetki.Close;
//      tblNeplateniPresmetki.ParamByName('d1').AsString:=s1;
//      tblNeplateniPresmetki.ParamByName('d2').AsString:=s2;
//      if cbTP.Text='' then
//         tblNeplateniPresmetki.ParamByName('tp').AsString:='%'
//      else
//         tblNeplateniPresmetki.ParamByName('tp').AsString:=cbTP.Text;
//      if cxComboBoxReon.Text='' then
//         tblNeplateniPresmetki.ParamByName('reon').AsString:='%'
//      else
//         tblNeplateniPresmetki.ParamByName('reon').AsString:=cxComboBoxReon.Text;
//      if txtDolg.text='' then
//         tblNeplateniPresmetki.ParamByName('suma').AsString:='0'
//      else
//         tblNeplateniPresmetki.ParamByName('suma').AsString:=txtDolg.Text;
//      if txtBroj.Text='' then
//         tblNeplateniPresmetki.ParamByName('broj').AsString:='0'
//      else
//         tblNeplateniPresmetki.ParamByName('broj').AsString:=txtBeleski.text;
//       tblNeplateniPresmetki.Open;
//
//       while not tblNeplateniPresmetki.eof do
//       begin
//         qUpdatePresmetka.Close;
//         qUpdatePresmetka.ParamByName('opomena').Value:=txtBroj.text;
//         qUpdatePresmetka.ParamByName('id').AsInteger:=tblNeplateniPresmetkiID.Value;
//         qUpdatePresmetka.ParamByName('d1').AsString:=s1;
//         qUpdatePresmetka.ParamByName('d2').AsString:=s2;
//         qUpdatePresmetka.ExecQuery;
//         tblNeplateniPresmetki.Next;
//       end;


      Panel1.Enabled:=false;
      cxGrid1.SetFocus;
   end
   else
   begin
     ShowMessage('�������������� ����� �� �� ���������!');
     Abort;
   end;
   ShowMessage('��������� �� ��������� ������ �� ���������!');
end;

procedure TfrmOpomena.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
        VK_RETURN:
        begin
         // if TEdit(sender).Name='txtBeleski' then
           //aZapisi.Execute

         //else
       // if TEdit(sender).Name='txtBroj' then
          //  txtDatumOd.SetFocus
        // else
       //  if TEdit(sender).Name='txtdatumdo' then
         //   cbTP.SetFocus
        //  else
        //begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
       // end;
        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;

end;

procedure TfrmOpomena.FormCreate(Sender: TObject);
begin
   pcOpomeni.TabIndex := 0;

   Panel1.Enabled:=False;

   DM1.tblOpomeni.Close;
   DM1.tblOpomeni.Open;
   ActiveControl:=cxGrid1;
end;

procedure TfrmOpomena.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case key of
       VK_ESCAPE:
        begin
            if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
                ModalResult := mrCancel;
                Close();
            end
            else
            begin
                cxGrid1DBTableView1.DataController.DataSet.Cancel;
                pcOpomeni.ActivePage := tsOpomeni;
                cxGrid1.SetFocus;
            end;
        end;
     end;


end;

procedure TfrmOpomena.pcOpomeniChange(Sender: TObject);
begin
//if DM1.tblOpomeni.State in [dsInsert,dsEdit] then dm1.tblOpomeni.Cancel;

     if pcOpomeni.TabIndex = 1 then
     begin
      //    DM1.tblPresmetkiVoOpomena.close;
        //  DM1.tblPresmetkiVoOpomena.open;
        dm1.tblPrOpomeni.open;
     end
     else if pcOpomeni.TabIndex = 0 then
      begin
      //   DM1.tblPresmetkiVoOpomena.close;
         dm1.tblPrOpomeni.Close;
         ActiveControl:=cxGrid1;
     end;
end;

end.
