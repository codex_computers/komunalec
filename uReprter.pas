unit uReprter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Provider, SqlExpr, cxContainer, cxEdit, FR_Shape, FR_ChBox,
  FR_Rich, FR_DSet, FR_DCtrl, FR_Desgn, DB, DBClient, DBLocal, DBLocalS,
  FR_DBSet, FR_Class, Menus, StdCtrls, cxCheckBox, cxDBEdit, DBCtrls,
  cxControls, cxTextEdit, cxMemo, ComCtrls, dxtree, dxdbtree, ExtCtrls;

type
  TfReporter = class(TForm)
    Panel1: TPanel;
    dxDBTreeView1: TdxDBTreeView;
    pDizajn: TPanel;
    Panel2: TPanel;
    mUslovi: TcxDBMemo;
    Panel4: TPanel;
    cxDBMemo1: TcxDBMemo;
    Panel5: TPanel;
    eBroj: TcxDBTextEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    Panel6: TPanel;
    DBNavigator1: TDBNavigator;
    Button1: TButton;
    Button2: TButton;
    cxDBTextEdit3: TcxDBTextEdit;
    cxDBCheckBox1: TcxDBCheckBox;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    Button3: TButton;
    dsReport: TDataSource;
    ppMeni: TPopupMenu;
    mDizajn: TMenuItem;
    mPregled: TMenuItem;
    frRep: TfrReport;
    frdsRep: TfrDBDataSet;
    dsp: TDataSource;
    tReport: TSQLClientDataSet;
    tReportID: TIntegerField;
    tReportKOREN: TIntegerField;
    tReportNASLOV: TStringField;
    tReportSQL: TBlobField;
    tReportDATA: TBlobField;
    tReportXML: TBlobField;
    tReportUSLOVI: TBlobField;
    tReportNASLOVI: TBlobField;
    tReportVIDLIVOST: TStringField;
    tReportPOJAVIUSLOVI: TIntegerField;
    tReportUSLOVITEXT: TBlobField;
    tReportDINAMICKIUSLOVI: TBlobField;
    qp: TSQLClientDataSet;
    frDesigner1: TfrDesigner;
    frDialogControls1: TfrDialogControls;
    frUserDataset1: TfrUserDataset;
    frRichObject1: TfrRichObject;
    frCheckBoxObject1: TfrCheckBoxObject;
    frShapeObject1: TfrShapeObject;
    StyleControllerLookUp: TcxEditStyleController;
    StyleController: TcxEditStyleController;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fReporter: TfReporter;

implementation

{$R *.dfm}

end.
