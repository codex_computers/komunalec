inherited FrmLokacijaUslugi: TFrmLokacijaUslugi
  Caption = #1059#1089#1083#1091#1075#1080' '#1079#1072' '#1083#1086#1082#1072#1094#1080#1112#1072#1090#1072
  ClientHeight = 593
  ClientWidth = 735
  ExplicitWidth = 751
  ExplicitHeight = 632
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 735
    Height = 299
    ExplicitWidth = 735
    ExplicitHeight = 299
    inherited cxGrid1: TcxGrid
      Width = 731
      Height = 295
      ExplicitWidth = 731
      ExplicitHeight = 295
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.DSKomLokacijaUslugi
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1083#1086#1082'.'
          DataBinding.FieldName = 'LOKACIIN_ID'
          HeaderAlignmentHorz = taCenter
          Width = 85
        end
        object cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1091#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'VID_USLUGA'
          HeaderAlignmentHorz = taCenter
          Width = 130
        end
        object cxGrid1DBTableView1USLUGA: TcxGridDBColumn
          Caption = #1059#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'USLUGA'
          HeaderAlignmentHorz = taCenter
          Width = 160
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          HeaderAlignmentHorz = taCenter
          Width = 354
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 425
    Width = 735
    Height = 145
    ExplicitTop = 425
    ExplicitWidth = 735
    ExplicitHeight = 145
    inherited Label1: TLabel
      Left = 256
      Top = 11
      Width = 43
      BiDiMode = bdRightToLeft
      Caption = #1064#1080#1092#1088#1072' '
      ParentBiDiMode = False
      Visible = False
      ExplicitLeft = 256
      ExplicitTop = 11
      ExplicitWidth = 43
    end
    object lblSifraLokacija1: TLabel [1]
      Left = 22
      Top = 86
      Width = 84
      Height = 13
      Caption = #1064#1080#1092#1088#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
    end
    object lblSifraLokacija: TLabel [2]
      Left = 6
      Top = 67
      Width = 84
      Height = 13
      Caption = #1064#1080#1092#1088#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
    end
    object lblNazivPartner: TLabel [3]
      Left = 22
      Top = 51
      Width = 43
      Height = 13
      Caption = #1055#1072#1088#1090#1085#1077#1088
    end
    object lblTipPartner: TLabel [4]
      Left = 22
      Top = 51
      Width = 43
      Height = 13
      Caption = #1055#1072#1088#1090#1085#1077#1088
      Visible = False
    end
    object lblPartnerId: TLabel [5]
      Left = 22
      Top = 35
      Width = 43
      Height = 13
      Caption = #1055#1072#1088#1090#1085#1077#1088
    end
    object lblPartner: TLabel [6]
      Left = 6
      Top = 16
      Width = 43
      Height = 13
      Caption = #1055#1072#1088#1090#1085#1077#1088
    end
    object Label2: TLabel [7]
      Left = 240
      Top = 38
      Width = 70
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1042#1080#1076' '#1091#1089#1083#1091#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object Label3: TLabel [8]
      Left = 261
      Top = 65
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1059#1089#1083#1091#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 316
      Top = 8
      DataBinding.DataField = 'LOKACIIN_ID'
      DataBinding.DataSource = DM1.DSKomLokacijaUslugi
      TabOrder = 1
      Visible = False
      ExplicitLeft = 316
      ExplicitTop = 8
      ExplicitWidth = 52
      Width = 52
    end
    inherited OtkaziButton: TcxButton
      Left = 604
      Top = 97
      TabOrder = 6
      ExplicitLeft = 604
      ExplicitTop = 97
    end
    inherited ZapisiButton: TcxButton
      Left = 523
      Top = 97
      TabOrder = 5
      ExplicitLeft = 523
      ExplicitTop = 97
    end
    object cxLookUpVID_USLUGA: TcxDBLookupComboBox
      Left = 367
      Top = 35
      DataBinding.DataField = 'VID_USLUGA'
      DataBinding.DataSource = DM1.DSKomLokacijaUslugi
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1080#1074
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = DM1.DSKomVidoviUslugi
      TabOrder = 2
      OnExit = cxLookUpVID_USLUGAExit
      OnKeyDown = EnterKakoTab
      Width = 310
    end
    object cxControlVID_USLUGA: TcxDBTextEdit
      Left = 316
      Top = 35
      BeepOnEnter = False
      DataBinding.DataField = 'VID_USLUGA'
      DataBinding.DataSource = DM1.DSKomLokacijaUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object cxDBTextEdit1: TcxDBTextEdit
      Left = 316
      Top = 62
      BeepOnEnter = False
      DataBinding.DataField = 'USLUGA'
      DataBinding.DataSource = DM1.DSKomLokacijaUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object cxLookUpUSLUGA: TcxDBLookupComboBox
      Left = 367
      Top = 62
      DataBinding.DataField = 'USLUGA'
      DataBinding.DataSource = DM1.DSKomLokacijaUslugi
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = DSKomUslugi
      TabOrder = 4
      OnExit = cxLookUpUSLUGAExit
      OnKeyDown = EnterKakoTab
      Width = 310
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 735
    ExplicitWidth = 735
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 570
    Width = 735
    ExplicitTop = 570
    ExplicitWidth = 735
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.440583831020000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object TblKomUslugi: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      'ID, VID_USLUGA, NAZIV'
      'FROM KOM_USLUGI'
      'where vid_usluga=:MAS_ID'
      '--where vid_usluga like :vu')
    AutoUpdateOptions.UpdateTableName = 'KOM_USLUGI'
    AutoUpdateOptions.KeyFields = 'ID;VID_USLUGA'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DM1.DSKomVidoviUslugi
    Left = 560
    Top = 73
    object TblKomUslugiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object TblKomUslugiVID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object TblKomUslugiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      DisplayWidth = 50
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object DSKomUslugi: TDataSource
    DataSet = TblKomUslugi
    Left = 600
    Top = 73
  end
end
