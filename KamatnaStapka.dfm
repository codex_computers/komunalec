inherited frmKamatnaStapka: TfrmKamatnaStapka
  Caption = #1050#1072#1084#1072#1090#1085#1072' '#1089#1090#1072#1087#1082#1072
  ClientHeight = 529
  ClientWidth = 609
  ExplicitWidth = 625
  ExplicitHeight = 567
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 609
    Height = 248
    ExplicitWidth = 609
    ExplicitHeight = 248
    inherited cxGrid1: TcxGrid
      Width = 605
      Height = 244
      ExplicitWidth = 605
      ExplicitHeight = 244
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.dsKamatnaStapka
        object cxGrid1DBTableView1OD_DATA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1086#1076
          DataBinding.FieldName = 'OD_DATA'
          Width = 166
        end
        object cxGrid1DBTableView1DO_DATA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1076#1086
          DataBinding.FieldName = 'DO_DATA'
          Width = 167
        end
        object cxGrid1DBTableView1STAPKA: TcxGridDBColumn
          Caption = #1057#1090#1072#1087#1082#1072
          DataBinding.FieldName = 'STAPKA'
          Width = 128
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 374
    Width = 609
    Height = 132
    ExplicitTop = 368
    ExplicitWidth = 594
    ExplicitHeight = 132
    inherited Label1: TLabel
      Left = 48
      Top = 58
      Width = 48
      Caption = #1057#1090#1072#1087#1082#1072' :'
      ExplicitLeft = 48
      ExplicitTop = 58
      ExplicitWidth = 48
    end
    object Label2: TLabel [1]
      Left = 29
      Top = 30
      Width = 82
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084'  '#1086#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 286
      Top = 30
      Width = 67
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 117
      Top = 55
      DataBinding.DataField = 'STAPKA'
      DataBinding.DataSource = DM1.dsKamatnaStapka
      TabOrder = 2
      ExplicitLeft = 117
      ExplicitTop = 55
      ExplicitWidth = 116
      Width = 116
    end
    inherited OtkaziButton: TcxButton
      Left = 486
      Top = 92
      TabOrder = 4
      ExplicitLeft = 471
      ExplicitTop = 92
    end
    inherited ZapisiButton: TcxButton
      Left = 405
      Top = 92
      TabOrder = 3
      ExplicitLeft = 390
      ExplicitTop = 92
    end
    object txtDatumOd: TcxDBDateEdit
      Left = 117
      Top = 27
      DataBinding.DataField = 'OD_DATA'
      DataBinding.DataSource = DM1.dsKamatnaStapka
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 116
    end
    object txtDatumDo: TcxDBDateEdit
      Left = 360
      Top = 27
      DataBinding.DataField = 'DO_DATA'
      DataBinding.DataSource = DM1.dsKamatnaStapka
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 609
    ExplicitWidth = 609
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 506
    Width = 609
    ExplicitTop = 506
    ExplicitWidth = 609
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 168
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 222
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 114
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40865.538499710640000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
