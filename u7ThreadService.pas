unit u7ThreadService;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FIBDataSet, pFIBDataSet,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  System.Actions, Vcl.ActnList, Vcl.ExtCtrls, FIBQuery, pFIBQuery,System.Diagnostics,
  pFIBStoredProc, REST.Json, data.dbxjson,
   System.JSON.Types, System.JSON.Readers,system.JSON,
  System.ImageList, Vcl.ImgList, IdMultipartFormData, System.StrUtils,
  IdAuthentication, Winapi.ActiveX, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom,
  Xml.XMLDoc, DateUtils;

type
  TfrmThreadService = class(TForm)
    IdHTTP11: TIdHTTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    tblPubLager: TpFIBDataSet;
    tblPubLagerRE: TFIBIntegerField;
    tblPubLagerARTVID: TFIBIntegerField;
    tblPubLagerARTSIF: TFIBIntegerField;
    tblPubLagerBRPAK: TFIBIntegerField;
    tblPubLagerLAGER: TFIBIntegerField;
    tblPubLagerLAGPAK: TFIBBCDField;
    tblPubLagerPRODCENA: TFIBBCDField;
    tblPubLagerNABCENA: TFIBBCDField;
    tblPubLagerDDV: TFIBBCDField;
    tblPubLagerTS_UPD: TFIBDateTimeField;
    tblPubLagerPUB: TFIBSmallIntField;
    tblPubLagerERRORCODE: TFIBStringField;
    tblPubLagerARTNAZ: TFIBStringField;
    tblPubLagerATC10: TFIBStringField;
    tblPubLagerPRIZVODITEL: TFIBStringField;
    tblPubLagerBARKOD: TFIBStringField;
    ActionList1: TActionList;
    aThreadPromeni: TAction;
    aPocni: TAction;
    aRezervacii: TAction;
    PocniTimer: TTimer;
    pMllogIns: TpFIBStoredProc;
    tblVremeE: TpFIBDataSet;
    tblVremeETSID: TFIBDateTimeField;
    tblVremeEVREME: TFIBBCDField;
    tblVremeESKOROE: TFIBFloatField;
    IdHTTP2: TIdHTTP;
    IdSSLIOHandlerSocketOpenSSL2: TIdSSLIOHandlerSocketOpenSSL;
    aBrisiRezervacii: TAction;
    pOtkazRezervacija: TpFIBStoredProc;
    aOtkRezervacii: TAction;
    qBrisiMLLOG: TpFIBQuery;
    tblRezervacii: TpFIBDataSet;
    dsRezervacii: TDataSource;
    tblRezervaciiID: TFIBBCDField;
    tblRezervaciiIZLOTID: TFIBBCDField;
    tblRezervaciiAPTID: TFIBBCDField;
    tblRezervaciiPRESCRIPTION_CODE: TFIBStringField;
    tblRezervaciiSTATUS: TFIBSmallIntField;
    tblRezervaciiARTVID: TFIBIntegerField;
    tblRezervaciiARTIKAL: TFIBIntegerField;
    tblRezervaciiTS_INS: TFIBDateTimeField;
    tblRezervaciiTS_UPD: TFIBDateTimeField;
    tblRezervaciiTS_DO: TFIBDateTimeField;
    tblRezervaciiPOTVRDIL: TFIBStringField;
    tblRezervaciiID_RG: TFIBBCDField;
    tblRezervaciiEMBG: TFIBStringField;
    tblRezervaciiEZBO: TFIBStringField;
    tblRezervaciiNAZIV: TFIBStringField;
    TrayIconRez: TTrayIcon;
    TrayIcons: TImageList;
    TrayIconRezOtk: TTrayIcon;
    tblPubLagerMIN_KOL_PAK: TFIBIntegerField;
    IdHTTP1: TIdHTTP;
    tblPubLagerPART: TFIBBCDField;
    tblPubLagerDOPLATA: TFIBBCDField;
    XMLDocPDDGI: TXMLDocument;
    SaveDialog1: TSaveDialog;
    procedure aThreadPromeniExecute(Sender: TObject);
    procedure aPocniExecute(Sender: TObject);
    procedure PocniTimerTimer(Sender: TObject);
    procedure ThreadDone(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aOtkRezervaciiExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function StreamToString(const Stream: TStream; const Encoding: TEncoding): string;
  private
    { Private declarations }
  public
   resp:string;
   procedure Pocetok;
   var prvpat : Integer;
    { Public declarations }
  end;


  type
  Tsinc = class(TThread)
  protected
    procedure Execute; override;
  public
   // fe: Integer;

  end;


var
  frmThreadService: TfrmThreadService;
  ThreadsRunning: Integer;
   rez_pred, rez_potoa : Integer;
implementation

uses unitDM1, dmKonekcija, Utils, PDDGIOZP; //uSkriptor;
var status, insupd : string;
jID, jartvid, jartIKAL, JAPTID,JSTATUS, JTS_INS, JTS_DO, JPRESCRIPTION_CODE, JID_RG, JEMBG, JEZBO, JNAZIV, jinformacija : string;

{$R *.dfm}


procedure Tsinc.Execute;
   var
  tm:TStopwatch;
  ibo:String;
begin
   try
   // if (StrToInt(DBModule.Setup7('rezervacija_lek')) > 0) then
     begin
       frmThreadService.aThreadPromeni.Execute();
     //  ibo:=ibo+'.mlLag';
     end;
   Except
     //
   end;

   try
//    if (StrToInt(DBModule.Setup7('rezervacija_lek')) > 0) then
//     begin
//       insupd := 'ins';
//       frmThreadService.aRezervacii.Execute();
//       ibo:=ibo+'.mlRez';
//     end;
//   DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;

    try
//    if (StrToInt(DBModule.Setup7('rezervacija_lek')) > 0) then
//     begin
//       insupd := 'upd';
//       frmThreadService.aOtkRezervacii.Execute();
//       ibo:=ibo+'.mlOtkRez';
//     end;
//   DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;

   try
//    if (StrToInt(DBModule.Setup7('rezervacija_lek')) > 0) then
//     begin
//       frmThreadService.aBrisiRezervacii.Execute();
//       ibo:=ibo+'.otkRez';
//     end;
////     DBModule.Setup7set('rrFazLog',ibo);
   Except
     //
   end;

  // tm.Stop;
   try
    //  frmThreadService.logRing(tm.ElapsedMilliseconds);
    //  ibo:=ibo+'.logRing';
//      DBModule.Setup7set('rrFazLog',ibo);
   Except
   end;

   //   ShowMessage(tm.Elapsed);
   //ibo:=ibo+'.'+tm.Elapsed;
//   DBModule.Setup7set('rrFazLog',ibo);

   try
 //   frmThreadService.pMllogIns.ParamByName('vreme').AsInt64 := tm.ElapsedMilliseconds;
  //  frmThreadService.pMllogIns.ExecProc;
   Except
   end;

end;

procedure TfrmThreadService.aOtkRezervaciiExecute(Sender: TObject);
var   url,GetPrescriptionRealized:String;
 i:integer;
 j: Integer;
 sega:TDateTime;
 prvpat:Boolean;
 ssR:TStringStream;
 codeR:integer;
 fsParamsR: TStringList;
 EDB:String;
 SifraFzo:String;
 mdb5:String;
// status: TStatusWindowHandle;
 prva_bukva, jsonStr:AnsiString;
begin
// //  status := cxCreateStatusWindow();
//   status := '2';
//   //------------------------------------------------------------------------------
//// Otkaz na Rezervacija na lek 11.01.2018
////   tblLekApteka.Close;
//  // tblLekApteka.open;
//   //if not tblLekApteka.IsEmpty then tblLekApteka.Delete;
//
// //   pOtkazRezervacija.Transaction := DBModule.ProcTransaction;
//
//  if( not dmKon.viewFirmi.Active)then dmKon.viewFirmi.Open;
//   SifraFzo:= dmKon.AptShifra;
//  // ShowMessage(SifraFzo);
//
//  url:=url_ml+'mServices/wservices.php';
//
//  fsParamsR := TStringList.Create;
//  fsParamsR.Add('action=GetAptOtkReservation');
//  fsParamsR.Add('in_aptid='+SifraFzo);
//  fsParamsR.Add('token='+DBModule.Setup7('token'));
//
// dmKon.trajSi(true);
//  try
//       ssR := TStringStream.Create('', TEncoding.UTF8);
//       IdHTTP2.Request.Accept := 'text/javascript';
//       IdHTTP2.Request.ContentType := 'text/html';
//       IdHTTP2.Response.CharSet := 'UTF-8';
//       IdHTTP2.Response.ContentEncoding := 'gzip';
//
//       IdHTTP2.Post(url,fsParamsR,ssR);    //  komentirano na 02.12.2017
//
//       resp:=ssR.DataString;
//       codeR:=IdHTTP2.ResponseCode;
//       if codeR=200 then
//       begin
//      //      ShowMessage(ssR.DataString);
//       end
//  except
//       on E: eIdHTTPProtocolexception do
//       begin
//       // ShowMessage(E.ErrorMessage);
//       end;
//  end;
//
////------------------------------------------------------------------------------
//
//  try
//     ParseJson;
//  except
// //   on E: Exception do
// //     ShowMessage(e.Message);
//  end;
//     if (rez_potoa > rez_pred) then
//    begin
//        TrayIconRezOtk.Visible := true;
//        TrayIconRezOtk.IconIndex := 3;
//
//        TrayIconRezOtk.BalloonTitle := '�������� ����������';
//        TrayIconRezOtk.BalloonHint := '��� �������� ����������!';
//        TrayIconRezOtk.ShowBalloonHint;
//
//        rez_pred := rez_potoa;
//    end;
//  dmKon.trajSi(false);
//  fsParamsR.Free;
end;

procedure TfrmThreadService.aPocniExecute(Sender: TObject);
begin
                try
                  with Tsinc.Create(True) do
                  begin
                    Start;
                  end;
                except

               end;
end;

procedure TfrmThreadService.aThreadPromeniExecute(Sender: TObject);
var
atribut, doc_elem, doc_data, atr_obvr, doc_header:IXMLNode;
    god:string;
    rbr, pred, rbr8, pred8, rbr32, pred32:integer;
    rbr1a, pred1a, rbr1b, pred1b, rbr1v, pred1v, rbr1g, pred1g:Integer;
    rbr33, pred33, rbr34, pred34, rbr35, pred35, rbr36, pred36, rbr37, pred37:integer;
    rbr39, pred39, rbr310, pred310, rbr311, pred311, rbr312, pred312, rbr313, pred313, rbr314, pred314:Integer;
    url, v2:String;
    ref_rep,rep,response: IXMLNode;
    fsParams,responset: TFileStream;
    sResponse, RefCode, responseMessage, xml: String;
    code:integer;
    enc : TEncoding;
     XMLDocPDDGI: IXMLDocument;
     smec,sgod:integer;
begin

          begin
           saveDialog1 := TSaveDialog.Create(self);
           saveDialog1.Title := '������� �� ��� ';
           saveDialog1.DefaultExt := 'xml';
           SaveDialog1.FileName:='inksanti.xml';
           saveDialog1.Filter := 'XML file|*.xml';
           saveDialog1.InitialDir := ExtractFilePath(ParamStr(0))+'\';

          //  if saveDialog1.Execute
           //then
           begin
             try

             CoInitialize(nil);
             XMLDocPDDGI:=TXMLDocument.Create(nil);

             XMLDocPDDGI.Active:=true;
             XMLDocPDDGI.Version:='1.0';
             XMLDocPDDGI.Encoding:='utf-8';
             XMLDocPDDGI.Options := [donodeautoindent];

             doc_header:=XMLDocPDDGI.DocumentElement;
             doc_header:=XMLDocPDDGI.AddChild('sentXMLeInkasator');
             doc_header.Attributes['xmlns'] := 'http://tempuri.org/';


             doc_elem:=doc_header.AddChild('INKASATORI');

             dm1.tblInkasatori.Close;
             dm1.tblInkasatori.Open;

                  //ZA POEDINECNI
             {*************************************************************************}

             dm1.tblInkasatori.First;
             while(not dm1.tblInkasatori.Eof)
             do
             begin
                 // POCETOK
                     atribut:=doc_elem.AddChild('INKASATOR');

                     atr_obvr:=atribut.AddChild('ID_INKASATOR');
                     atr_obvr.Text:=dm1.TBLInkasatoriID_INKASATOR.AsString;

                     atr_obvr:=atribut.AddChild('ID_FIRMA');
                     atr_obvr.Text:=dm1.tblInkasatoriID_FIRMA.AsString;

                     atr_obvr:=atribut.AddChild('IME_PREZIME');
                     atr_obvr.Text:=dm1.tblInkasatoriIME.Value+' '+dm1.tblInkasatoriPREZIME.Value;

                     atr_obvr:=atribut.AddChild('USERNAME');
                     atr_obvr.Text:=dm1.tblInkasatoriUSERNAME.AsString;

                     atr_obvr:=atribut.AddChild('PASS');
                     atr_obvr.Text:=dm1.tblInkasatoriPASS.AsString;

                 // KRAJ
             {*************************************************************************}

                dm1.tblInkasatori.Next;

             end;

             {*************************************************************************}
             //KRAJ ZA POEDINECNI

             dm1.tblData.Close;
             dm1.tblData.Open;

                  //ZA POEDINECNI
             {*************************************************************************}

             doc_data:=doc_header.AddChild('MASTERDATA');
             dm1.tblData.First;
             while(not dm1.tblData.Eof)
             do
             begin
                 // POCETOK
                     atribut:=doc_data.AddChild('DATA');

                     atr_obvr:=atribut.AddChild('ID_INKASATOR');
                     atr_obvr.Text:=dm1.tblDataID_INKASATOR.AsString;


                    atr_obvr:=atribut.AddChild('BROJ_VODOMER');
                     atr_obvr.Text:=dm1.tblDataBR_VODOMER.AsString;


                     atr_obvr:=atribut.AddChild('ID_PARTNER');
                     atr_obvr.Text:=dm1.tblDataID_PARTNER.AsString;

                     atr_obvr:=atribut.AddChild('IME_PREZIME');
                     atr_obvr.Text:=dm1.tblDataIME_PREZIME.Value;

                     atr_obvr:=atribut.AddChild('ULICA');
                     atr_obvr.Text:=dm1.tblDataULICA.AsString;

                     atr_obvr:=atribut.AddChild('BROJ_ULICA');
                     atr_obvr.Text:=QuotedStr(dm1.tblDataBROJ_ULICA.AsString);

                     atr_obvr:=atribut.AddChild('PAUSALNO');
                     atr_obvr.Text:=dm1.tblDataPAUSALNO.AsString;

                     atr_obvr:=atribut.AddChild('ID_REON');
                     atr_obvr.Text:=dm1.tblDataID_REON.AsString;

                     atr_obvr:=atribut.AddChild('NAZIV_REON');
                     atr_obvr.Text:=dm1.tblDataNAZIV_REON.AsString;

                     atr_obvr:=atribut.AddChild('AKTIVEN');
                     atr_obvr.Text:=dm1.tblDataAKTIVEN.AsString;

                     atr_obvr:=atribut.AddChild('AKTIVNA_LOKACIJA');
                     atr_obvr.Text:=dm1.tblDataAKTIVNA_LOKACIJA.AsString;

                     atr_obvr:=atribut.AddChild('ID_LOKACIJA');
                     atr_obvr.Text:=dm1.tblDataID_LOKACIJA.AsString;

                     atr_obvr:=atribut.AddChild('CITACKA_KNIGA');
                     atr_obvr.Text:=dm1.tblDataCITACKA_KNIGA.AsString;

                     atr_obvr:=atribut.AddChild('ID_FIRMA');
                     atr_obvr.Text:=dm1.tblDataID_FIRMA.AsString;

                     atr_obvr:=atribut.AddChild('ID_VODOMER');
                     atr_obvr.Text:=dm1.tblDataID_VODOMER.AsString;

                     atr_obvr:=atribut.AddChild('CITACKA_KNIGA_RB');
                     atr_obvr.Text:=dm1.tblDataCITACKA_KNIGA_RB.AsString;

                     atr_obvr:=atribut.AddChild('RB2');
                     atr_obvr.Text:=dm1.tblDataRB2.AsString;

                 // KRAJ
             {*************************************************************************}

                dm1.tblData.Next;

             end;

             {*************************************************************************}
                     //KRAJ ZA POEDINECNI
            if (MonthOf(Now)-1) = 0 then
            begin
              sgod := YearOf(now)-1;
              smec := 12;
            end
            else
            begin
              sgod := YearOf(now);
              smec := MonthOf(Now)-1;
            end;

             dm1.tblVodomer.Close;
             dm1.tblVodomer.ParamByName('godina').Value := sgod;
             dm1.tblVodomer.ParamByName('mesec').Value := smec;
             dm1.tblVodomer.Open;


                  //ZA POEDINECNI
             {*************************************************************************}
             doc_data:=doc_header.AddChild('VODOMERI');
             dm1.tblVodomer.First;
             while(not dm1.tblVodomer.Eof)
             do
             begin
                 // POCETOK
                     atribut:=doc_data.AddChild('VODOMER');

                     atr_obvr:=atribut.AddChild('ID_VODOMER');
                     atr_obvr.Text:=dm1.tblVodomerID_VODOMER.AsString;


                    atr_obvr:=atribut.AddChild('STARA_SOSTOJBA');
                     atr_obvr.Text:=dm1.tblVodomerSTARA_SOSTOJBA.AsString;


                     atr_obvr:=atribut.AddChild('NOVA_SOSTOJBA');
                     atr_obvr.Text:=dm1.tblVodomerNOVA_SOSTOJBA.AsString;

                     atr_obvr:=atribut.AddChild('GODINA');
                     atr_obvr.Text:=dm1.tblVodomerGODINA.AsString;

                     atr_obvr:=atribut.AddChild('MESEC');
                     atr_obvr.Text:=dm1.tblVodomerMESEC.AsString;

                 // KRAJ
             {*************************************************************************}

                dm1.tblVodomer.Next;

             end;

             {*************************************************************************}
             //KRAJ ZA POEDINECNI


              XMLDocPDDGI.SaveToFile(saveDialog1.FileName);
              fsParams := TFileStream.Create(saveDialog1.FileName, fmOpenRead or fmShareDenyWrite);
              xml := StreamToString(fsParams,enc.UTF8) ;

         try
                IdHTTP1.Request.ContentType := 'application/text';    //08.04.2019 komentirano
                // sResponse :=IdHTTP1.Post('http://10.20.1.45:491/eInkasator.svc/sentXMLeInkasator', fsParams);  //08.04.2019 komentirano
                  sResponse :=IdHTTP1.Post(link_zemi, fsParams);  //08.04.2019 komentirano

               //  sResponse :=IdHTTP1.Post('http://ws.codex.mk:490/eInkasator.svc/sentXMLeInkasator', fsParams);  //08.04.2019 komentirano
                 code:=IdHTTP1.ResponseCode;  //08.04.2019 komentirano

                if code=200 then
                begin
//                    qrySpecIzvestaj.Edit;
//                    qrySpecIzvestajTAG_WEB.Value:=1;
//                    qrySpecIzvestaj.Post;
//                    ShowMessage('Caieooaa?aoi a oniaoii caa?oaii.');
                end

          except
          //responseMessage:='Eioa?ia a?aoea ai nenoaiio ia IC.';
          end;

  finally
    CoUninitialize;

    fsParams.Free;
end;

end;

//           CelLink:= TIdURI.URLEncode(link_edit+'&LOKACISKI_BROJ='+dm1.TblKomLokaciiID.AsString+'&ULICA='+cxLookUpULICA.Text+'&BROJ='+cxControlBROJ_ULICA.Text+'&IME_I_PREZIME='+cxTxtPartnerNaziv.Text+'&REON='+dm1.TblKomLokaciiREON_ID.AsString+'&CITACKA_KNIGA='+dm1.TblKomLokaciiCITACKA_KNIGA.AsString+'&AKTIVEN='+dm1.TblKomLokaciiAKTIVEN.AsString);

           dm1.tblPromena.Close;
           DM1.tblPromena.Open;
           dm1.tblPromena.Insert;
           DM1.tblPromenaLINK.Value := link_zemi;//CelLink;
           DM1.tblPromena.Post;
          end;
end;

function TfrmThreadService.StreamToString(const Stream: TStream; const Encoding: TEncoding): string;
var
  StringBytes: TBytes;
begin
  Stream.Position := 0;
  SetLength(StringBytes, Stream.Size);
  Stream.ReadBuffer(StringBytes, Stream.Size);
  Result := Encoding.GetString(StringBytes);
end;

procedure TfrmThreadService.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//   if (StrToInt(DBModule.Setup7('rezervacija_lek')) > 0) then
// // da se izbrisat starite logovi
//  qBrisiMLLOG.ExecQuery;
end;

procedure TfrmThreadService.FormCreate(Sender: TObject);
begin
  PocniTimer.Interval := 60*30*1000; //na 1/2 cas da proveruva dali ima promena na podatocite
 //  if ima_promena = '1' then Pocetok;
end;

procedure TfrmThreadService.Pocetok;
begin
  // dmKon.trajSi(true);
                try

                  with Tsinc.Create(True) do
                  begin
                //  ShowMessage('daaaaaa');
                    Start;
                    OnTerminate := ThreadDone;
                  end;
                //end;
                // dmKon.trajSi(false);
                except

               end;
end;


procedure TfrmThreadService.PocniTimerTimer(Sender: TObject);
var
  vremee:Boolean;

begin
  if ima_promena = '1' then
  begin
      with Tsinc.Create(True) do
      begin
        Start;
      end;
      ima_promena := '0';
  end;
end;

procedure TfrmThreadService.ThreadDone(Sender: TObject);
begin

//  ShowMessage('GOTO >'+IntToStr(ThreadsRunning ));
  //Dec(ThreadsRunning);
end;

end.
