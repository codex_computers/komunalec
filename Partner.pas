unit Partner;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, ActnList, Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ComCtrls, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, ExtCtrls,
  cxGridExportLink, cxExport, Provider, SqlExpr, DBClient,// DBLocal, DBLocalS,
  cxDataStorage, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, cxLookAndFeels, cxLookAndFeelPainters, DBCtrls,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxCheckBox, IdTCPConnection, IdTCPClient, IdHTTP,
  IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,iduri,
  //dxSkinOffice2013White,
  cxNavigator, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, System.Actions,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;//,
   //System.Actions;

type
  TfrmPartner = class(TForm)
    StatusBar1: TStatusBar;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    ActionList1: TActionList;
    SaveToIniFile: TAction;
    ExportToExcel: TAction;
    pcPartner: TPageControl;
    pcTabTabelaren: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    //cxGrid1DBTableView1TipPartnerNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MestoNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1TEL: TcxGridDBColumn;
    cxGrid1DBTableView1FAX: TcxGridDBColumn;
    cxGrid1DBTableView1DANOCEN: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKO: TcxGridDBColumn;
    cxGrid1DBTableView1LOGO: TcxGridDBColumn;
    viewSmetki: TcxGridDBTableView;
    viewSmetkiID: TcxGridDBColumn;
    viewSmetkiTIP_PARTNER: TcxGridDBColumn;
    viewSmetkiPARTNER: TcxGridDBColumn;
    viewSmetkiTIP_BANKA: TcxGridDBColumn;
    viewSmetkiBANKA: TcxGridDBColumn;
    viewSmetkiBankaNaziv: TcxGridDBColumn;
    viewSmetkiZS_BROJ: TcxGridDBColumn;
    viewSmetkiDEVIZNA: TcxGridDBColumn;
    viewSmetkiAKTIVNA: TcxGridDBColumn;
    viewKontakti: TcxGridDBTableView;
    viewKontaktiSIFRA: TcxGridDBColumn;
    viewKontaktiTIP_PARTNER: TcxGridDBColumn;
    viewKontaktiID: TcxGridDBColumn;
    viewKontaktiNAZIV: TcxGridDBColumn;
    viewKontaktiMAIL: TcxGridDBColumn;
    viewKontaktiTEL: TcxGridDBColumn;
    viewKontaktiMOBILEN: TcxGridDBColumn;
    viewKontaktiOPIS: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1Level3: TcxGridLevel;
    pcTabEdinecen: TTabSheet;
    lblTip: TLabel;
    lblSifra: TLabel;
    lblNaziv: TLabel;
    lblAdresa: TLabel;
    lblMesto: TLabel;
    lblTelefon: TLabel;
    lblFaks: TLabel;
    lblDanocen: TLabel;
    lblPrezime: TLabel;
    lblTatko: TLabel;
    Label1: TLabel;
    TipPartner: TcxDBTextEdit;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    Adresa: TcxDBTextEdit;
    Mesto: TcxDBTextEdit;
    Telefon: TcxDBTextEdit;
    Faks: TcxDBTextEdit;
    Danocen: TcxDBTextEdit;
    //TipPartnerNaziv: TcxDBLookupComboBox;
   // MestoNaziv: TcxDBLookupComboBox;
    Prezime: TcxDBTextEdit;
    Tatko: TcxDBTextEdit;
    RE: TcxDBTextEdit;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    cbVraboten: TDBCheckBox;
    cbSubvencioniranje: TcxDBCheckBox;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    IdIcmpClient1: TIdIcmpClient;
    IdHTTP1: TIdHTTP;
    cbInkasator: TcxDBCheckBox;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    procedure cxDBTextAllEnter(Sender: TObject);
    procedure cxDBTextAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PrvPosledenTab(panel:TTabSheet;var posledna,prva:TWinControl);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure ExportToExcelExecute(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure viewSmetkiKeyPress(Sender: TObject; var Key: Char);
    procedure viewKontaktiKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    f1, f2, f3 : TField;
    e1, e2, e3 : TEdit;
    ec1:TcxLookupComboBox;
    val1, val2, val3 : Pointer;
    k : integer;
    procedure setVrednost(v1,v2,v3 : Variant);
    procedure prefrli;
  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;
    procedure getField(field1, field2, field3 :TField);
    procedure getEdit(edit1, edit2, edit3 :TEdit);
    procedure getEditC(edit1: TcxLookupComboBox; edit2, edit3 :TEdit);
    procedure getValue(var1, var2, var3 :Pointer);
  end;

var
  frmPartner: TfrmPartner;

implementation

uses dmMaticni, DaNe, TipPartner, Mesto, dmKonekcija, Utils, UnitLokacii,
  UnitDM1;

{$R *.dfm}
//------------------------------------------------------------------------------
constructor TfrmPartner.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
    inserting := insert;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.cxDBTextAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
    if (Sender = Sifra) then
    begin
        if ((dmMat.dsPartner.State = dsInsert) and (TcxdbTextEdit(Sender)=Sifra)) then
         //not dmMat.tblPartnerTIP_PARTNER.IsNull)) then
        //begin
            dmMat.tblPartnerID.Value := dmMat.zemiMax(dmMat.MaxPartner1,'tip_partner',Null,Null,TipPartner.EditValue,Null,Null,'MAKS');
       // end;
    end;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.cxDBTextAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    if (Key = VK_RETURN) then
    begin
        Key := 0;
        if (kom = posledna) then
        begin
            st := cxGrid1DBTableView1.DataController.DataSource.State;
            if (st = dsInsert) or (st = dsEdit) then
            begin
              //  if cbVraboten.Checked then dmMat.tblPartnerSTATUS.Value:=3
              //    else dmMat.tblPartnerSTATUS.Value:=0;
               cxGrid1DBTableView1.DataController.DataSet.Post;
               dmMat.tblPartner.FullRefresh;
               //cxGrid1DBTableView1.DataController.DataSet.Open;
              // ��� �� ����� ������ �� �������, �� ������ �� �� ������� ���� �� ��������� ���� � �� �� ������� �����
              // �������� � �� �� ����� ���� ������� �� ��������, �������� �������� � �� �� ������� ������� �� �������
              if sos_int = '1' then // od 1 vo 11,  duri da sredam , 01.08.2019
                begin
                if st in [dsEdit, dsInsert] then
                begin
                   ima_promena := '1';
//                if servis = '0' then
//                begin
//                  dm1.tblPromeniPartner.Close;
//                  DM1.tblPromeniPartner.ParamByName('tip_partner').Value := dmMat.tblPartnerTIP_PARTNER.Value;
//                  DM1.tblPromeniPartner.ParamByName('partner').Value := dmMat.tblPartnerID.Value;
//                  dm1.tblPromeniPartner.Open;
//
//                  CelLink:= TIdURI.URLEncode(link_edit+'&LOKACISKI_BROJ='+DM1.tblPromeniPartnerLOKACIJA.AsString+'&SIFRA_NA_KORISNIK='+dm1.tblPromeniPartnerPARTNER_ID.AsString+'&IME_I_PREZIME='+DM1.tblPromeniPartnerNAZIV.AsString);
//
//                   dm1.tblPromena.Close;
//                   DM1.tblPromena.Open;
//                   dm1.tblPromena.Insert;
//                   DM1.tblPromenaLINK.Value := CelLink;
//                   DM1.tblPromena.Post;
//
//                   //�� �� ������� ��� ���� �������� �����
//               //    if dm1.IsInternetConnected then
//                   try
//                if not dm1.IsInternetConnected then
//                 //  IdIcmpClient1.Ping('www.google.com');
//
//                   Except on E:Exception do
//             //      if not ima then
//                    begin
//                   //  ShowMessage('���� �������� ��������!');
//                     // DM1.TblKomLokacii.CloseOpen(true);
//                     // panelDole.Enabled := false;
//                     // enableGrid(true);
//                      cxGrid1.SetFocus();
//                      Abort;
//                    end
//
//
//                   end;
//                   begin
//                     // da se prati linkot
//                         dm1.ZemiTextFile(cellink);
//                     //    if (copy(Trim(vraten_text),2,1) = '1') then
//                         if pos('refreshed',vraten_text) > 0 then
//                         begin
//                           //update vo tabelata KOM_PROMENA
//                           dm1.tblPromena.Edit;
//                           dm1.tblPromenaPROMENA.Value := 1;
//                           dm1.tblPromena.Post;
//                         end;
//                   end;
//                end;
                end;
                end;

            pcPartner.ActivePage := pcTabTabelaren;
            end;
            if (st = dsInsert) and (inserting) then
            begin
                Key := VK_F5;
                FormKeyDown(Sender, Key, Shift);
            end
            else if (st = dsInsert) and (not inserting) then
            begin
//                retParams;
            end;
            if (st = dsEdit) then
                cxGrid1.SetFocus;
        end
        else PostMessage(Handle, WM_NEXTDLGCTL,0,0);
    end

end;
//------------------------------------------------------------------------------
procedure TfrmPartner.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
            if (not(pcPartner.ActivePage = pcTabTabelaren)) then
            begin
                PostMessage(Handle,WM_NextDlgCtl,0,0);
                Key := 0;
            end;
        end;
        VK_UP:
        begin
            if (not(pcPartner.ActivePage = pcTabTabelaren)) then
            begin
                PostMessage(Handle,WM_NextDlgCtl,1,0);
                Key := 0;
            end;
        end;
//        VK_RETURN:
//            if (ActiveControl = cxGrid1) and (not inserting) then
//                retParams;
        VK_F5:
        begin
            pcPartner.ActivePage := pcTabEdinecen;
            prva.SetFocus;
            cxGrid1DBTableView1.DataController.DataSet.Insert;
        end;
        VK_F6:
        begin
            pcPartner.ActivePage := pcTabEdinecen;
            prva.SetFocus;
            cxGrid1DBTableView1.DataController.DataSet.Edit;
        end;
        VK_F7:
        begin
            cxGrid1DBTableView1.DataController.DataSet.Refresh;
        end;
        VK_F8:
            cxGrid1DBTableView1.DataController.DataSet.Delete();
        VK_ESCAPE:
        begin
            if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
                ModalResult := mrCancel;
                Close();
            end
            else
            begin
                cxGrid1DBTableView1.DataController.DataSet.Cancel;
                pcPartner.ActivePage := pcTabTabelaren;
                cxGrid1.SetFocus;
            end;
        end;
    end;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.getField(field1,field2,field3 : TField);
begin
    k := 0;
    f1 := field1;
    f2 := field2;
    f3 := field3;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.getEdit(edit1,edit2,edit3 : TEdit);
begin
    k := 1;
    e1 := edit1;
    e2 := edit2;
    e3 := edit3;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.getEditC(edit1:TcxLookupComboBox; edit2,edit3 : TEdit);
begin
    k := 3;
    //e1 := edit1;
    ec1 := edit1;
    e2 := edit2;
    e3 := edit3;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.getValue(var1, var2, var3 :Pointer);
begin
    k := 2;
    val1 := var1;
    val2 := var2;
    val3 := var3;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.setVrednost(v1,v2,v3 : Variant);
begin
  case k of
    0:
    begin
        if(v1 <> Null) then
            f1.Value := v1;
        if(v2 <> Null) then
            f2.Value := v2;
        if(v3 <> Null) then
            f3.Value := v3;
    end;
    1:
    begin
        if(v1 <> Null) then
            e1.Text := v1;
        if(v2 <> Null) then
            e2.Text := v2;
        if(v3 <> Null) then
            e3.Text := v3;
    end;
    2:
    begin
        if(v1 <> Null) then
            Variant(val1^) := v1;
        if(v2 <> Null) then
            Variant(val2^) := v2;
        if(v3 <> Null) then
            Variant(val3^) := v3;
    end;
    3:
    begin
        if(v1 <> Null) then
            ec1.EditValue := v1;
        if(v2 <> Null) then
            e2.Text := v2;
        if(v3 <> Null) then
            e3.Text := v3;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.prefrli;
begin
    if(not inserting) then
    begin
        //setVrednost(dmMat.tblPartnerTIP_PARTNER.Value,dmMat.tblPartnerID.Value,Null);
        setVrednost(dmMat.tblPartnerTIP_PARTNER.Value,dmMat.tblPartnerID.Value,dmMat.tblPartnerNAZIV.Value);
        ModalResult := mrOk;
    end
end;
//------------------------------------------------------------------------------
procedure TfrmPartner.PrvPosledenTab(panel : TTabSheet; var posledna , prva : TWinControl);
begin
    list := TList.Create();
    panel.GetTabOrderList(list);
    prva := TWinControl(list.First);
    posledna := TWinControl(list.Last);
end;
//------------------------------------------------------------------------------

procedure TfrmPartner.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
            cxGrid1DBTableView1.DataController.DataSet.Cancel
        else
            cxGrid1DBTableView1.DataController.DataSet.Post;
    end;
    Action := caFree;
end;

procedure TfrmPartner.FormShow(Sender: TObject);
begin
    dmmat.tblPartner.Close;
    dmmat.tblPartner.Open;
  //  dmMat.MaxPartner1
    PrvPosledenTab(pcTabEdinecen,posledna,prva);
    cxGrid1DBTableView1.RestoreFromIniFile(Name,false,false,[]);
    pcPartner.ActivePage := pcTabTabelaren;
    cxGrid1DBTableView1.Controller.FocusedColumn := cxGrid1DBTableView1NAZIV;

end;

procedure TfrmPartner.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;

procedure TfrmPartner.ExportToExcelExecute(Sender: TObject);
begin
//    zaSaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
//    if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;

procedure TfrmPartner.cxGrid1DBTableView1DblClick(Sender: TObject);
var frmL:TFrmLokacija;
begin
    //prefrli;
        if Tag = 1 then
        begin
            frmL := TFrmLokacija.Create(nil,dmMat.tblPartnerID.AsString,dmMat.tblPartnerTIP_PARTNER.AsString,dmMat.tblPartnerNAZIV.AsString);
            frmL.ShowModal();
            frmL.Free();
        end
        else
        begin
            prefrli;
        end;
end;

procedure TfrmPartner.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
var frmL:TFrmLokacija;
begin
    if(Ord(Key) = VK_RETURN) then
    begin
        if Tag = 1 then
        begin
            frmL := TFrmLokacija.Create(nil,dmMat.tblPartnerID.AsString,dmMat.tblPartnerTIP_PARTNER.AsString,dmMat.tblPartnerNAZIV.AsString);
            frmL.ShowModal();
            frmL.Free();
        end
        else
        begin
            //cxSortiraj(cxGrid1DBTableView1);
            prefrli;
        end;
    end;
end;

procedure TfrmPartner.viewSmetkiKeyPress(Sender: TObject; var Key: Char);
begin
    cxSortiraj(viewSmetki);
end;

procedure TfrmPartner.viewKontaktiKeyPress(Sender: TObject; var Key: Char);
begin
    cxSortiraj(viewKontakti);
end;

end.
