unit UnitLokacijaUslugi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, FIBDataSet, pFIBDataSet, cxLookAndFeels, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBar, dxPSCore, dxPScxCommon,
  cxBarEditItem, dxStatusBar,
  dxRibbonStatusBar, dxRibbon, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TFrmLokacijaUslugi = class(TfrmMaster)
    lblSifraLokacija1: TLabel;
    lblSifraLokacija: TLabel;
    lblNazivPartner: TLabel;
    lblTipPartner: TLabel;
    lblPartnerId: TLabel;
    lblPartner: TLabel;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1USLUGA: TcxGridDBColumn;
    Label2: TLabel;
    Label3: TLabel;
    cxLookUpVID_USLUGA: TcxDBLookupComboBox;
    cxControlVID_USLUGA: TcxDBTextEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    cxLookUpUSLUGA: TcxDBLookupComboBox;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    TblKomUslugi: TpFIBDataSet;
    TblKomUslugiID: TFIBIntegerField;
    TblKomUslugiVID_USLUGA: TFIBIntegerField;
    TblKomUslugiNAZIV: TFIBStringField;
    DSKomUslugi: TDataSource;
  constructor Create(Owner : TComponent; tipPartner : string; partner : string; nazivPartner : string; sifraLokacija : string; citackaKniga : string; citackaKnigaRb : string);reintroduce; overload;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxLookUpVID_USLUGAExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxLookUpUSLUGAExit(Sender: TObject);
  private
    { Private declarations }
    var tipPartner,partner,nazivPartner,sifraLokacija,citackaKniga,citackaKnigaRb:string;
  public
 // Uslugi:string;
    { Public declarations }
  end;

var
  FrmLokacijaUslugi: TFrmLokacijaUslugi;

implementation

uses UnitDM1;

{$R *.dfm}
constructor TFrmLokacijaUslugi.Create(Owner : TComponent; tipPartner : string; partner : string; nazivPartner : string; sifraLokacija : string; citackaKniga : string; citackaKnigaRb : string);
begin
inherited Create(Owner);
    Self.tipPartner := tipPartner;
    Self.partner := partner;
    Self.nazivPartner := nazivPartner;
    Self.sifraLokacija := sifraLokacija;
    Self.citackaKniga := citackaKniga;
    Self.citackaKnigaRb := citackaKnigaRb;
end;

procedure TFrmLokacijaUslugi.cxLookUpUSLUGAExit(Sender: TObject);
begin
  inherited;
  if cxLookUpUSLUGA.Text='' then
  begin
    cxLookUpUSLUGA.SetFocus;
    Abort;
  end
  else  if dPanel.Enabled then
    ZapisiButton.SetFocus;


end;

procedure TFrmLokacijaUslugi.cxLookUpVID_USLUGAExit(Sender: TObject);
begin
  inherited;
  if cxLookUpVID_USLUGA.Text='' then
  begin
    cxLookUpVID_USLUGA.SetFocus;
    Abort;
  end
  else
   begin
//    TblKomUslugi.Close;
//    TblKomUslugi.ParamByName('vu').AsString:=cxControlVID_USLUGA.Text;
//    TblKomUslugi.open;
   end;

end;

procedure TFrmLokacijaUslugi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case key of
        VK_RETURN:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
        VK_UP:
        begin
            if (Sender.ClassName = 'TcxComboBox') then
            begin

                if TcxComboBox(Sender).DroppedDown = false then
                begin
                    PostMessage(Handle, WM_NEXTDLGCTL,1,0);
                    Key := 0;
                end;
            end
            else
            begin
                PostMessage(Handle, WM_NEXTDLGCTL,1,0);
                Key := 0;
            end;
        end;
   end;


 // inherited;

    if (DM1.TblKomLokacijaUslugi.State=dsInsert) and ((TEdit(sender).Name='cxControlVID_USLUGA')) then
         DM1.TblKomLokacijaUslugiLOKACIIN_ID.AsString := lblSifraLokacija1.Caption;


   

end;

procedure TFrmLokacijaUslugi.FormCreate(Sender: TObject);
begin
  inherited;
//    TblKomUslugi.Close;
//    TblKomUslugi.ParamByName('vu').AsString:='%';
    TblKomUslugi.open;
end;

procedure TFrmLokacijaUslugi.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    case Key of
            VK_F5:
        begin
            //Sifra. := lblSifraLokacija1.Caption;
            DM1.TblKomLokacijaUslugiLOKACIIN_ID.AsString := lblSifraLokacija1.Caption;
        end;
    end;
end;

procedure TFrmLokacijaUslugi.FormShow(Sender: TObject);
begin
  inherited;
    lblTipPartner.Caption := tipPartner;
    lblPartnerId.Caption := partner;
    lblNazivPartner.Caption := nazivPartner;

    lblSifraLokacija1.Caption := sifraLokacija;

    //lblCitackaKniga.Caption := citackaKniga;
    //lblCitackaKnigaRb.Caption := citackaKnigaRb;

    //DM1.TblKomUslugi.Open();
    //DM1.openTblUslugiPoVidUsluga(DM1.);
end;

end.
