inherited FrmVidoviUslugi: TFrmVidoviUslugi
  Caption = #1042#1080#1076#1086#1074#1080' '#1085#1072' '#1091#1089#1083#1091#1075#1080
  ClientHeight = 534
  ClientWidth = 602
  ExplicitWidth = 610
  ExplicitHeight = 568
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 269
    ExplicitWidth = 617
    ExplicitHeight = 279
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 275
      ExplicitWidth = 613
      ExplicitHeight = 275
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = DM1.DSKomVidoviUslugi
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          HeaderAlignmentHorz = taCenter
          Width = 386
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 395
    Width = 602
    Height = 116
    ExplicitTop = 395
    ExplicitWidth = 617
    ExplicitHeight = 116
    inherited Label1: TLabel
      Top = 23
      BiDiMode = bdRightToLeft
      ParentBiDiMode = False
      ExplicitTop = 23
    end
    object lblNaziv: TLabel [1]
      Left = 13
      Top = 54
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1053#1072#1079#1080#1074
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 61
      Top = 20
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.DSKomVidoviUslugi
      ExplicitLeft = 61
      ExplicitTop = 20
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 76
      TabOrder = 3
      ExplicitLeft = 526
      ExplicitTop = 76
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 76
      TabOrder = 2
      ExplicitLeft = 445
      ExplicitTop = 76
    end
    object cxControlNAZIV: TcxDBTextEdit
      Left = 61
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = DM1.DSKomVidoviUslugi
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 260
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 617
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 511
    Width = 602
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080',F9 - '#1047#1072#1087#1080#1096#1080', EN' +
          'TER/DbClick - '#1059#1089#1083#1091#1075#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
    ExplicitTop = 511
    ExplicitWidth = 617
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.442342881950000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
