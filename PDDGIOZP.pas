﻿unit PDDGIOZP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxLabel, System.DateUtils,xmldom, XMLIntf, msxmldom, XMLDoc,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, Xml.adomxmldom, Winapi.ActiveX;

type
  TfrmPDDGIOZP = class(TForm)
    AppBar1: TPanel;
    SaveDialog1: TSaveDialog;
    XMLDocPDDGI: TXMLDocument;
    IdHTTP1: TIdHTTP;
    btn2: TcxButton;
    procedure btn2Click(Sender: TObject);
    function StreamToString(const Stream: TStream; const Encoding: TEncoding): string;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPDDGIOZP: TfrmPDDGIOZP;

implementation

{$R *.dfm}

uses UnitDM1;

procedure TfrmPDDGIOZP.btn2Click(Sender: TObject);
var atribut, doc_elem, doc_data, atr_obvr, doc_header:IXMLNode;
    god:string;
    rbr, pred, rbr8, pred8, rbr32, pred32:integer;
    rbr1a, pred1a, rbr1b, pred1b, rbr1v, pred1v, rbr1g, pred1g:Integer;
    rbr33, pred33, rbr34, pred34, rbr35, pred35, rbr36, pred36, rbr37, pred37:integer;
    rbr39, pred39, rbr310, pred310, rbr311, pred311, rbr312, pred312, rbr313, pred313, rbr314, pred314:Integer;
    url, v2:String;
    ref_rep,rep,response: IXMLNode;
    fsParams,responset: TFileStream;
    sResponse, RefCode, responseMessage, xml: String;
    code:integer;
    enc : TEncoding;
     XMLDocPDDGI: IXMLDocument;
begin
   saveDialog1 := TSaveDialog.Create(self);
   saveDialog1.Title := 'Снимање на фајл ';
   saveDialog1.DefaultExt := 'xml';
   SaveDialog1.FileName:='inksanti.xml';
   saveDialog1.Filter := 'XML file|*.xml';
   saveDialog1.InitialDir := ExtractFilePath(ParamStr(0))+'\';

  //  if saveDialog1.Execute
   //then
   begin
     try

     CoInitialize(nil);
     XMLDocPDDGI:=TXMLDocument.Create(nil);

     XMLDocPDDGI.Active:=true;
     XMLDocPDDGI.Version:='1.0';
     XMLDocPDDGI.Encoding:='utf-8';
     XMLDocPDDGI.Options := [donodeautoindent];

     doc_header:=XMLDocPDDGI.DocumentElement;
     doc_header:=XMLDocPDDGI.AddChild('sentXMLeInkasator');
     doc_header.Attributes['xmlns'] := 'http://tempuri.org/';


     doc_elem:=doc_header.AddChild('INKASATORI');

     dm1.tblInkasatori.Close;
     dm1.tblInkasatori.Open;

          //ZA POEDINECNI
     {*************************************************************************}

     dm1.tblInkasatori.First;
     while(not dm1.tblInkasatori.Eof)
     do
     begin
         // POCETOK
             atribut:=doc_elem.AddChild('INKASATOR');

             atr_obvr:=atribut.AddChild('ID_INKASATOR');
             atr_obvr.Text:=dm1.TBLInkasatoriID_INKASATOR.AsString;

             atr_obvr:=atribut.AddChild('ID_FIRMA');
             atr_obvr.Text:=dm1.tblInkasatoriID_FIRMA.AsString;

             atr_obvr:=atribut.AddChild('IME_PREZIME');
             atr_obvr.Text:=dm1.tblInkasatoriIME.Value+' '+dm1.tblInkasatoriPREZIME.Value;

             atr_obvr:=atribut.AddChild('USERNAME');
             atr_obvr.Text:=dm1.tblInkasatoriUSERNAME.AsString;

             atr_obvr:=atribut.AddChild('PASS');
             atr_obvr.Text:=dm1.tblInkasatoriPASS.AsString;

         // KRAJ
     {*************************************************************************}

        dm1.tblInkasatori.Next;

     end;

     {*************************************************************************}
     //KRAJ ZA POEDINECNI

     dm1.tblData.Close;
     dm1.tblData.Open;

          //ZA POEDINECNI
     {*************************************************************************}

     doc_data:=doc_header.AddChild('MASTERDATA');
     dm1.tblData.First;
     while(not dm1.tblData.Eof)
     do
     begin
         // POCETOK
             atribut:=doc_data.AddChild('DATA');

             atr_obvr:=atribut.AddChild('ID_INKASATOR');
             atr_obvr.Text:=dm1.tblDataID_INKASATOR.AsString;


            atr_obvr:=atribut.AddChild('BROJ_VODOMER');
             atr_obvr.Text:=dm1.tblDataBR_VODOMER.AsString;


             atr_obvr:=atribut.AddChild('ID_PARTNER');
             atr_obvr.Text:=dm1.tblDataID_PARTNER.AsString;

             atr_obvr:=atribut.AddChild('IME_PREZIME');
             atr_obvr.Text:=dm1.tblDataIME_PREZIME.Value;

             atr_obvr:=atribut.AddChild('ULICA');
             atr_obvr.Text:=dm1.tblDataULICA.AsString;

             atr_obvr:=atribut.AddChild('BROJ_ULICA');
             atr_obvr.Text:=QuotedStr(dm1.tblDataBROJ_ULICA.AsString);

             atr_obvr:=atribut.AddChild('PAUSALNO');
             atr_obvr.Text:=dm1.tblDataPAUSALNO.AsString;

             atr_obvr:=atribut.AddChild('ID_REON');
             atr_obvr.Text:=dm1.tblDataID_REON.AsString;

             atr_obvr:=atribut.AddChild('NAZIV_REON');
             atr_obvr.Text:=dm1.tblDataNAZIV_REON.AsString;

             atr_obvr:=atribut.AddChild('AKTIVEN');
             atr_obvr.Text:=dm1.tblDataAKTIVEN.AsString;

             atr_obvr:=atribut.AddChild('AKTIVNA_LOKACIJA');
             atr_obvr.Text:=dm1.tblDataAKTIVNA_LOKACIJA.AsString;

             atr_obvr:=atribut.AddChild('ID_LOKACIJA');
             atr_obvr.Text:=dm1.tblDataID_LOKACIJA.AsString;

             atr_obvr:=atribut.AddChild('CITACKA_KNIGA');
             atr_obvr.Text:=dm1.tblDataCITACKA_KNIGA.AsString;

             atr_obvr:=atribut.AddChild('ID_FIRMA');
             atr_obvr.Text:=dm1.tblDataID_FIRMA.AsString;

             atr_obvr:=atribut.AddChild('ID_VODOMER');
             atr_obvr.Text:=dm1.tblDataID_VODOMER.AsString;

             atr_obvr:=atribut.AddChild('CITACKA_KNIGA_RB');
             atr_obvr.Text:=dm1.tblDataCITACKA_KNIGA_RB.AsString;

             atr_obvr:=atribut.AddChild('RB2');
             atr_obvr.Text:=dm1.tblDataRB2.AsString;

         // KRAJ
     {*************************************************************************}

        dm1.tblData.Next;

     end;

     {*************************************************************************}
     //KRAJ ZA POEDINECNI


     dm1.tblVodomer.Close;
     dm1.tblVodomer.ParamByName('godina').Value := YearOf(Now);
     dm1.tblVodomer.ParamByName('mesec').Value := MonthOf(Now)-1;
     dm1.tblVodomer.Open;

          //ZA POEDINECNI
     {*************************************************************************}
     doc_data:=doc_header.AddChild('VODOMERI');
     dm1.tblVodomer.First;
     while(not dm1.tblVodomer.Eof)
     do
     begin
         // POCETOK
             atribut:=doc_data.AddChild('VODOMER');

             atr_obvr:=atribut.AddChild('ID_VODOMER');
             atr_obvr.Text:=dm1.tblVodomerID_VODOMER.AsString;


            atr_obvr:=atribut.AddChild('STARA_SOSTOJBA');
             atr_obvr.Text:=dm1.tblVodomerSTARA_SOSTOJBA.AsString;


             atr_obvr:=atribut.AddChild('NOVA_SOSTOJBA');
             atr_obvr.Text:=dm1.tblVodomerNOVA_SOSTOJBA.AsString;

             atr_obvr:=atribut.AddChild('GODINA');
             atr_obvr.Text:=dm1.tblVodomerGODINA.AsString;

             atr_obvr:=atribut.AddChild('MESEC');
             atr_obvr.Text:=dm1.tblVodomerMESEC.AsString;

         // KRAJ
     {*************************************************************************}

        dm1.tblVodomer.Next;

     end;

     {*************************************************************************}
     //KRAJ ZA POEDINECNI


      XMLDocPDDGI.SaveToFile(saveDialog1.FileName);
      fsParams := TFileStream.Create(saveDialog1.FileName, fmOpenRead or fmShareDenyWrite);
      xml := StreamToString(fsParams,enc.UTF8) ;

         try
                IdHTTP1.Request.ContentType := 'application/text';    //08.04.2019 komentirano
                // sResponse :=IdHTTP1.Post('http://10.20.1.45:491/eInkasator.svc/sentXMLeInkasator', fsParams);  //08.04.2019 komentirano
               //  sResponse :=IdHTTP1.Post('http://95.86.15.252:490/eInkasator.svc/sentXMLeInkasator', fsParams);  //08.04.2019 komentirano

               //  sResponse :=IdHTTP1.Post('http://ws.codex.mk:490/eInkasator.svc/sentXMLeInkasator', fsParams);  //08.04.2019 komentirano
                 code:=IdHTTP1.ResponseCode;  //08.04.2019 komentirano

                if code=200 then
                begin
//                    qrySpecIzvestaj.Edit;
//                    qrySpecIzvestajTAG_WEB.Value:=1;
//                    qrySpecIzvestaj.Post;
//                    ShowMessage('Caieooaa?aoi a oniaoii caa?oaii.');
                end

          except
          //responseMessage:='Eioa?ia a?aoea ai nenoaiio ia IC.';
          end;

  finally
    CoUninitialize;

    XMLDocPDDGI.Active := False;
end;

end;

end;

function TfrmPDDGIOZP.StreamToString(const Stream: TStream; const Encoding: TEncoding): string;
var
  StringBytes: TBytes;
begin
  Stream.Position := 0;
  SetLength(StringBytes, Stream.Size);
  Stream.ReadBuffer(StringBytes, Stream.Size);
  Result := Encoding.GetString(StringBytes);
end;

end.
