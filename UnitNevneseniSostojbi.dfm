object frmNevneseniSostojbi: TfrmNevneseniSostojbi
  Left = 0
  Top = 0
  Caption = #1042#1086#1076#1086#1084#1077#1088#1080' '#1079#1072' '#1082#1086#1080' '#1085#1077#1084#1072' '#1074#1085#1077#1089#1077#1085#1086' '#1089#1086#1089#1090#1086#1112#1073#1080
  ClientHeight = 265
  ClientWidth = 692
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 692
    Height = 265
    Align = alClient
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DM1.dsGetNotVneseniSostojbi
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
        Caption = #1056#1077#1086#1085
        DataBinding.FieldName = 'REON_ID'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 48
      end
      object cxGrid1DBTableView1LOKACIJA: TcxGridDBColumn
        Caption = #1051#1086#1082#1072#1094#1080#1112#1072
        DataBinding.FieldName = 'LOKACIJA'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 54
      end
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        Caption = #1064#1080#1092#1088#1072
        DataBinding.FieldName = 'ID'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
      end
      object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
        Caption = #1053#1072#1079#1080#1074
        DataBinding.FieldName = 'NAZIV'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 127
      end
      object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
        Caption = #1040#1076#1088#1077#1089#1072
        DataBinding.FieldName = 'ADRESA'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 113
      end
      object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
        Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
        DataBinding.FieldName = 'CITACKA_KNIGA'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 83
      end
      object cxGrid1DBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn
        Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112' '#1074#1086' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
        DataBinding.FieldName = 'CITACKA_KNIGA_RB'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 94
      end
      object cxGrid1DBTableView1RB1: TcxGridDBColumn
        DataBinding.FieldName = 'RB1'
        Visible = False
        HeaderGlyphAlignmentHorz = taCenter
      end
      object cxGrid1DBTableView1VODOMER: TcxGridDBColumn
        Caption = #1042#1086#1076#1086#1084#1077#1088
        DataBinding.FieldName = 'VODOMER'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
        Width = 93
      end
      object cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn
        Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        DataBinding.FieldName = 'NOVA_SOSTOJBA'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
      end
      object cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn
        Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        DataBinding.FieldName = 'STARA_SOSTOJBA'
        HeaderAlignmentHorz = taCenter
        HeaderGlyphAlignmentHorz = taCenter
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ActionList1: TActionList
    Left = 416
    Top = 56
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
  end
end
