object frmNovaVrednost: TfrmNovaVrednost
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'frmNovaVrednost'
  ClientHeight = 103
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 250
    Height = 103
    Align = alClient
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = clWindow
    ParentBackground = False
    TabOrder = 0
    object cxLabel21: TcxLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Align = alTop
      AutoSize = False
      Caption = #1042#1085#1077#1089#1080' '#1074#1088#1077#1076#1085#1086#1089#1090':'
      ParentColor = False
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Color = clNavy
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindow
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.LookAndFeel.NativeStyle = False
      Style.Shadow = False
      Style.TextColor = clWindow
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.NativeStyle = False
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      Height = 23
      Width = 240
      AnchorX = 123
      AnchorY = 15
    end
    object txtNovaVrednost: TcxTextEdit
      Left = 64
      Top = 48
      Hint = #1042#1085#1077#1089#1080' '#1085#1086#1074' '#1080#1079#1085#1086#1089' '#1079#1072' '#1091#1089#1083#1091#1075#1072#1090#1072
      TabOrder = 1
      OnKeyDown = txtNovaVrednostKeyDown
      Width = 121
    end
  end
end
