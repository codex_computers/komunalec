inherited frmKarticaKorisnikFin: TfrmKarticaKorisnikFin
  Left = 0
  Top = 0
  Caption = #1050#1072#1088#1090#1080#1094#1072' '#1085#1072' '#1082#1086#1088#1080#1089#1085#1080#1082
  ClientHeight = 743
  ClientWidth = 944
  ExplicitWidth = 960
  ExplicitHeight = 782
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Top = 229
    Width = 944
    Height = 491
    ExplicitTop = 229
    ExplicitWidth = 944
    ExplicitHeight = 491
    inherited cxGrid1: TcxGrid
      Width = 940
      Height = 487
      TabStop = False
      ExplicitWidth = 940
      ExplicitHeight = 487
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.dsKarParFin
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'DOLZI'
            Column = cxGrid1DBTableView1DOLZI
            DisplayText = #1044#1086#1083#1078#1080
          end
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'POBARUVA'
            Column = cxGrid1DBTableView1POBARUVA
            DisplayText = #1055#1086#1073#1072#1088#1091#1074#1072
          end
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'SALDO'
            Column = cxGrid1DBTableView1SALDO
            DisplayText = #1056#1072#1079#1083#1080#1082#1072
          end>
        DataController.Summary.SummaryGroups = <
          item
            Links = <>
            SummaryItems = <>
          end>
        DataController.OnDetailCollapsed = cxGrid1DBTableView1DataControllerDetailCollapsed
        DataController.OnDetailExpanding = cxGrid1DBTableView1DataControllerDetailExpanding
        DataController.OnDetailExpanded = cxGrid1DBTableView1DataControllerDetailExpanded
        OptionsBehavior.PullFocusing = True
        OptionsData.Appending = True
        OptionsData.Editing = True
        OptionsSelection.MultiSelect = True
        OptionsView.NoDataToDisplayInfoText = ''
        OptionsView.ColumnAutoWidth = True
        OptionsView.Indicator = True
        object cxGrid1DBTableView1BROJ_FAKTURA: TcxGridDBColumn
          Caption = #1041#1088'.'#1060#1072#1082#1090#1091#1088#1072
          DataBinding.FieldName = 'BROJ_FAKTURA'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 118
        end
        object cxGrid1DBTableView1OPIS_LOKACIJA: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'OPIS_LOKACIJA'
          Visible = False
          GroupIndex = 0
          Styles.Header = dmRes.cxStyle121
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.FieldName = 'BROJ_FAKTURA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 63
        end
        object cxGrid1DBTableView1LOKACIJA: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'LOKACIJA'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087
          DataBinding.FieldName = 'TIP_PARTNER'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmMat.dsTipPartner
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 57
        end
        object cxGrid1DBTableView1MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 70
        end
        object cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'DATUM_PRESMETKA'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 94
        end
        object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
          DataBinding.FieldName = 'CITACKA_KNIGA'
          Visible = False
        end
        object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'REON_ID'
          Visible = False
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Visible = False
        end
        object cxGrid1DBTableView1DOLZI: TcxGridDBColumn
          Caption = #1044#1086#1083#1078#1080
          DataBinding.FieldName = 'DOLZI'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 139
        end
        object cxGrid1DBTableView1POBARUVA: TcxGridDBColumn
          Caption = #1055#1086#1073#1072#1088#1091#1074#1072
          DataBinding.FieldName = 'POBARUVA'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 134
        end
        object cxGrid1DBTableView1UPLATENO: TcxGridDBColumn
          Caption = #1059#1087#1083#1072#1090#1077#1085#1086
          DataBinding.FieldName = 'UPLATENO'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 82
        end
        object cxGrid1DBTableView1SALDO: TcxGridDBColumn
          Caption = #1057#1072#1083#1076#1086
          DataBinding.FieldName = 'SALDO'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 166
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Visible = False
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1PID: TcxGridDBColumn
          DataBinding.FieldName = 'PID'
          Visible = False
          Width = 20
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_REON: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_REON'
          Visible = False
        end
      end
      object cxGrid1DBTableView3: TcxGridDBTableView [1]
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsSiteUplati
        DataController.DetailKeyFieldNames = 'ID'
        DataController.KeyFieldNames = 'ID'
        DataController.MasterKeyFieldNames = 'PID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView3GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Visible = False
        end
        object cxGrid1DBTableView3MESEC: TcxGridDBColumn
          DataBinding.FieldName = 'MESEC'
          Visible = False
        end
        object cxGrid1DBTableView3DOLZI: TcxGridDBColumn
          Caption = #1044#1086#1083#1078#1080
          DataBinding.FieldName = 'DOLZI'
          Width = 100
        end
        object cxGrid1DBTableView3UPLATI: TcxGridDBColumn
          Caption = #1059#1087#1083#1072#1090#1080
          DataBinding.FieldName = 'UPLATI'
          Width = 96
        end
        object cxGrid1DBTableView3UPLATENO: TcxGridDBColumn
          Caption = #1059#1087#1083#1072#1090#1077#1085#1086
          DataBinding.FieldName = 'UPLATENO'
        end
        object cxGrid1DBTableView3TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Visible = False
        end
        object cxGrid1DBTableView3NAZIV_UPLATA: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1091#1087#1083#1072#1090#1072
          DataBinding.FieldName = 'NAZIV_UPLATA'
          Width = 115
        end
        object cxGrid1DBTableView3RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView3TIP_NALOG: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072#1083#1086#1075
          DataBinding.FieldName = 'TIP_NALOG'
        end
        object cxGrid1DBTableView3NALOG: TcxGridDBColumn
          Caption = #1053#1072#1083#1086#1075
          DataBinding.FieldName = 'NALOG'
        end
        object cxGrid1DBTableView3TS_KNIZENJE: TcxGridDBColumn
          DataBinding.FieldName = 'TS_KNIZENJE'
          Visible = False
        end
        object cxGrid1DBTableView3DOKUMENT: TcxGridDBColumn
          Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          DataBinding.FieldName = 'DOKUMENT'
          Width = 82
        end
        object cxGrid1DBTableView3NACIN_PLAKJANJE: TcxGridDBColumn
          Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
          DataBinding.FieldName = 'NACIN_PLAKJANJE'
          Width = 118
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView [2]
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsSiteStavki
        DataController.DetailKeyFieldNames = 'PRESMETKA_G_ID'
        DataController.KeyFieldNames = 'PRESMETKA_G_ID'
        DataController.MasterKeyFieldNames = 'PID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2VID_USLUGA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1091#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'VID_USLUGA'
          Width = 39
        end
        object cxGrid1DBTableView2USLUGA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1091#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'USLUGA'
        end
        object cxGrid1DBTableView2NAZIV_USLUGA: TcxGridDBColumn
          Caption = #1059#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'NAZIV_USLUGA'
          Width = 326
        end
        object cxGrid1DBTableView2IZNOS_USLUGA_NETO: TcxGridDBColumn
          Caption = #1048#1079#1085#1086#1089' '#1091#1089#1083#1091#1075#1072' '#1085#1077#1090#1086
          DataBinding.FieldName = 'IZNOS_USLUGA_NETO'
          Width = 106
        end
        object cxGrid1DBTableView2IZNOS_USLUGA: TcxGridDBColumn
          Caption = #1048#1079#1085#1086#1089' '#1091#1089#1083#1091#1075#1072
          DataBinding.FieldName = 'IZNOS_USLUGA'
          Width = 93
        end
        object cxGrid1DBTableView2DANOK: TcxGridDBColumn
          Caption = #1044#1072#1085#1086#1082
          DataBinding.FieldName = 'DANOK'
          Width = 96
        end
        object cxGrid1DBTableView2CENA: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'CENA'
        end
      end
      inherited cxGrid1Level1: TcxGridLevel
        Caption = 'truututy'
        Options.DetailTabsPosition = dtpTop
        object cxGrid1Level2: TcxGridLevel
          Caption = #1059#1089#1083#1091#1075#1080
          GridView = cxGrid1DBTableView2
          Options.DetailTabsPosition = dtpTop
          Visible = False
        end
        object cxGrid1Level3: TcxGridLevel
          Caption = #1059#1087#1083#1072#1090#1080
          GridView = cxGrid1DBTableView3
          Options.DetailTabsPosition = dtpTop
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 126
    Width = 944
    Height = 103
    Align = alTop
    ExplicitTop = 126
    ExplicitWidth = 944
    ExplicitHeight = 103
    inherited Label1: TLabel
      Left = 32
      Top = 18
      Width = 48
      Height = 14
      Caption = #1055#1072#1088#1090#1085#1077#1088
      Font.Height = -12
      Font.Style = []
      ExplicitLeft = 32
      ExplicitTop = 18
      ExplicitWidth = 48
      ExplicitHeight = 14
    end
    object lblDatumOd: TLabel [1]
      Left = 255
      Top = 36
      Width = 52
      Height = 14
      Caption = #1044#1072#1090#1091#1084' '#1086#1076
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object lblDatumDo: TLabel [2]
      Left = 255
      Top = 59
      Width = 52
      Height = 14
      Caption = #1044#1072#1090#1091#1084' '#1076#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [3]
      Left = 685
      Top = 9
      Width = 58
      Height = 18
      AutoSize = False
      Caption = #1056#1077#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel [4]
      Left = 480
      Top = 9
      Width = 58
      Height = 18
      AutoSize = False
      Caption = #1040#1076#1088#1077#1089#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel [5]
      Left = 600
      Top = 33
      Width = 58
      Height = 18
      AutoSize = False
      Caption = #1063#1080#1090'.'#1082#1085#1080#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [6]
      Left = 32
      Top = 50
      Width = 40
      Height = 14
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 502
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmMat.dsPartner
      TabOrder = 8
      Visible = False
      ExplicitLeft = 502
      ExplicitTop = 6
      ExplicitWidth = 53
      Width = 53
    end
    inherited OtkaziButton: TcxButton
      Left = 835
      Top = 172
      TabOrder = 10
      Visible = False
      ExplicitLeft = 835
      ExplicitTop = 172
    end
    inherited ZapisiButton: TcxButton
      Left = 749
      Top = 172
      TabOrder = 9
      Visible = False
      ExplicitLeft = 749
      ExplicitTop = 172
    end
    object cxTxtPartnerID2: TcxTextEdit
      Left = 149
      Top = 15
      BeepOnEnter = False
      TabOrder = 12
      Visible = False
      OnExit = cxTxtPartnerID2Exit
      OnKeyDown = EnterKakoTab
      Width = 71
    end
    object cxTxtPartnerNaziv: TcxTextEdit
      Left = 220
      Top = 15
      Properties.ReadOnly = True
      TabOrder = 2
      OnEnter = cxTxtPartnerNazivEnter
      OnExit = cxTxtPartnerNazivExit
      OnKeyDown = EnterKakoTab
      Width = 662
    end
    object cxDateDatumOd: TcxDateEdit
      Tag = 1
      Left = 323
      Top = 33
      Properties.ImmediatePost = True
      TabOrder = 6
      Visible = False
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object cxDateDatumDo: TcxDateEdit
      Tag = 1
      Left = 323
      Top = 57
      Properties.ImmediateDropDownWhenActivated = True
      Properties.ImmediatePost = True
      TabOrder = 7
      Visible = False
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object cxLookUpComboBoxTipPartner2: TcxTextEdit
      Left = 100
      Top = 15
      TabOrder = 13
      OnExit = cxLookUpComboBoxTipPartner2Exit
      OnKeyDown = EnterKakoTab
      Width = 49
    end
    object txtAdresa: TcxTextEdit
      Left = 544
      Top = 6
      Enabled = False
      ParentFont = False
      TabOrder = 3
      Visible = False
      Width = 135
    end
    object txtReon: TcxTextEdit
      Left = 722
      Top = 6
      Enabled = False
      TabOrder = 4
      Visible = False
      Width = 224
    end
    object txtCitKniga: TcxTextEdit
      Left = 824
      Top = 6
      Enabled = False
      TabOrder = 5
      Visible = False
      Width = 58
    end
    object cxGrid2: TcxGrid
      Left = 242
      Top = 49
      Width = 584
      Height = 80
      BevelInner = bvNone
      BevelOuter = bvNone
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      Visible = False
      LookAndFeel.NativeStyle = False
      object cxGrid2DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsLokacija
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.GridLines = glNone
        OptionsView.GroupByBox = False
        OptionsView.GroupRowStyle = grsOffice11
        object cxGrid2DBTableView1LOKACIJA: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'LOKACIJA'
          Width = 40
        end
        object cxGrid2DBTableView1NAZIV_REON: TcxGridDBColumn
          Caption = #1056#1077#1086#1085
          DataBinding.FieldName = 'NAZIV_REON'
          Width = 150
        end
        object cxGrid2DBTableView1REON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'REON_ID'
          Visible = False
        end
        object cxGrid2DBTableView1ADRESA: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089#1072
          DataBinding.FieldName = 'ADRESA'
          Width = 150
        end
        object cxGrid2DBTableView1CITACKA_KNIGA: TcxGridDBColumn
          Caption = #1063'.'#1050'.'
          DataBinding.FieldName = 'CITACKA_KNIGA'
          Width = 40
        end
        object cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
        end
        object cxGrid2DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Visible = False
        end
        object cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1085#1072
          DataBinding.FieldName = 'AKTIVEN'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1044#1072
              Value = 1
            end
            item
              Description = #1053#1077
              ImageIndex = 0
              Value = 0
            end>
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object cxLookUpComboBoxTipPartner: TcxMaskEdit
      Tag = 1
      Left = 100
      Top = 15
      Properties.IgnoreMaskBlank = True
      Properties.MaskKind = emkRegExpr
      Properties.EditMask = '\d\d\d\d\d'
      Properties.MaxLength = 0
      TabOrder = 0
      OnExit = cxLookUpComboBoxTipPartner2Exit
      OnKeyDown = EnterKakoTab
      Width = 49
    end
    object cxTxtPartnerID: TcxMaskEdit
      Tag = 1
      Left = 149
      Top = 15
      Properties.IgnoreMaskBlank = True
      Properties.MaskKind = emkRegExpr
      Properties.EditMask = '\d\d\d\d\d\d\d\d\d\d'
      Properties.MaxLength = 0
      TabOrder = 1
      OnExit = cxTxtPartnerID2Exit
      OnKeyDown = EnterKakoTab
      Width = 71
    end
    object dbimgIMG: TDBImage
      Left = 450
      Top = -6
      Width = 105
      Height = 105
      DataField = 'IMG'
      DataSource = dsVodSost
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      Visible = False
    end
    object cbGodina: TcxComboBox
      Left = 100
      Top = 46
      Properties.Items.Strings = (
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030')
      TabOrder = 15
      OnKeyDown = EnterKakoTab
      Width = 120
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 944
    ExplicitWidth = 944
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 720
    Width = 944
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F11-'#1048#1079#1073#1088#1072#1085#1080' '#1073#1077#1083'.,  F9-'#1045#1076'. '#1073#1077#1083#1077#1096#1082#1072', F10-'#1050#1072#1088#1090#1080#1094#1072', F5-'#1045#1076'. '#1087#1088#1077#1089#1084#1077#1090#1082#1072 +
          ', F6-'#1040#1078#1091#1088#1080#1088#1072#1112', F8-'#1041#1088#1080#1096#1080', Esc-'#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
    ExplicitTop = 720
    ExplicitWidth = 944
  end
  object cxMemo1: TcxMemo [4]
    Left = 787
    Top = 120
    Lines.Strings = (
      'cxMemo1')
    ParentFont = False
    Properties.ScrollBars = ssBoth
    Properties.WordWrap = False
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 4
    Visible = False
    Height = 73
    Width = 390
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 64
  end
  inherited PopupMenu1: TPopupMenu
    Left = 184
    Top = 72
  end
  inherited dxBarManager1: TdxBarManager
    Left = 672
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1077#1089#1084#1077#1090#1082#1072
      DockedDockControl = nil
      FloatClientWidth = 64
      FloatClientHeight = 156
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      Visible = False
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 624
      FloatClientWidth = 111
      FloatClientHeight = 126
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 876
      FloatClientHeight = 104
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1055#1077#1095#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 759
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dr'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      CaptionButtons = <>
      DockedLeft = 538
      DockedTop = 0
      FloatLeft = 886
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    inherited dxBarLargeButtonAzuriraj: TdxBarLargeButton
      Caption = #1057#1090#1086#1088#1085#1080#1088#1072#1112
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPrintOdredeniBeleskiFizicki
      Category = 0
      Visible = ivNever
    end
    object dr: TdxBarLargeButton
      Action = aNovIzgledB
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1077#1076#1080#1085#1077#1095#1085#1072' '#1073#1077#1083#1077#1096#1082#1072
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPoedinecnaFaktura
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aIzbraniFakturi
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aEdinecna
      Caption = #1055#1086#1077#1076#1080#1085#1077#1095#1085#1072
      Category = 0
      Visible = ivNever
      LargeImageIndex = 4
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aPecati
      Category = 0
      SyncImageIndex = False
      ImageIndex = -1
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aPratiEmail
      Category = 0
      Visible = ivNever
    end
  end
  inherited ActionList1: TActionList
    Left = 416
    Top = 8
    inherited aZapisi: TAction
      ShortCut = 0
    end
    object aPecati: TAction
      Caption = #1050#1072#1088#1090#1080#1094#1072' '#1085#1072' '#1082#1086#1088#1080#1089#1085#1080#1082
      ImageIndex = 40
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aCallFrfBeleskiPrint: TAction
      Caption = 'aCallFrfBeleskiPrint'
      ShortCut = 24697
      OnExecute = aCallFrfBeleskiPrintExecute
    end
    object aPrintOdredeniBeleskiFizicki: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1080#1079#1073#1088#1072#1085#1080' '#1073#1077#1083#1077#1096#1082#1080
      Enabled = False
      ImageIndex = 8
      ShortCut = 122
      OnExecute = aPrintOdredeniBeleskiFizickiExecute
    end
    object aEdnaBeleskaFizicki: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1077#1076#1080#1085#1077#1095#1085#1072' '#1073#1077#1083#1077#1096#1082#1072' '
      Enabled = False
      ImageIndex = 41
      OnExecute = aEdnaBeleskaFizickiExecute
    end
    object aPoedinecnaFaktura: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1077#1076#1080#1085#1077#1095#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
      Enabled = False
      ImageIndex = 35
      ShortCut = 123
      OnExecute = aPoedinecnaFakturaExecute
    end
    object aIzbraniFakturi: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1080#1079#1073#1088#1072#1085#1080' '#1092#1072#1082#1090#1091#1088#1080
      Enabled = False
      ImageIndex = 8
      ShortCut = 115
      OnExecute = aIzbraniFakturiExecute
    end
    object aEdinecna: TAction
      Caption = #1055#1086#1077#1076#1080#1085#1077#1095#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      ShortCut = 116
      OnExecute = aEdinecnaExecute
    end
    object aStorno: TAction
      Caption = #1057#1090#1086#1088#1085#1080#1088#1072#1112
      OnExecute = aStornoExecute
    end
    object aKorekcija: TAction
      Caption = #1050#1086#1088#1077#1082#1094#1080#1112#1072' '#1085#1072' '#1089#1090#1072#1088#1072' '#1073#1077#1083#1077#1096#1082#1072
      OnExecute = aKorekcijaExecute
    end
    object aPratiEmail: TAction
      Caption = #1055#1088#1072#1090#1080' '#1087#1086' '#1077'-'#1084#1072#1080#1083
      ImageIndex = 45
      OnExecute = aPratiEmailExecute
    end
    object aEFakturaDukjanDizajn: TAction
      Caption = 'aEFakturaDukjanDizajn'
      ShortCut = 24641
      OnExecute = aEFakturaDukjanDizajnExecute
    end
    object aEFakturaDukjan: TAction
      Caption = 'aEFakturaDukjan'
      ShortCut = 16449
      OnExecute = aEFakturaDukjanExecute
    end
    object aEFakturaDizajn: TAction
      Caption = 'aEFakturaDizajn'
      ShortCut = 24646
      OnExecute = aEFakturaDizajnExecute
    end
    object aEFaktura: TAction
      Caption = 'aEFaktura'
      ShortCut = 16454
      OnExecute = aEFakturaExecute
    end
    object aMailFaktura: TAction
      Caption = 'aMailFaktura'
      ShortCut = 16461
      OnExecute = aMailFakturaExecute
    end
    object aMailFakturaDizajn: TAction
      Caption = 'aMailFakturaDizajn'
      ShortCut = 24653
      OnExecute = aMailFakturaDizajnExecute
    end
    object aNovIzgledBDizajn: TAction
      Caption = 'aNovIzgledBDizajn'
      ShortCut = 24642
      OnExecute = aNovIzgledBDizajnExecute
    end
    object aNovIzgledB: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1077#1076#1080#1085#1077#1095#1085#1072' '#1073#1077#1083#1077#1096#1082#1072' '
      Enabled = False
      ImageIndex = 41
      ShortCut = 120
      OnExecute = aNovIzgledBExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 752
    Top = 200
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.437528125000000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 448
    Top = 72
    PixelsPerInch = 96
  end
  object tblKarPar: TpFIBDataSet
    RefreshSQL.Strings = (
      'select g.broj_faktura,p.tip_partner, p.id, p.naziv, p.adresa,'
      'g.datum_presmetka,g.iznos_vkupno dolzi,'
      'u.iznos pobaruva,u.datum_uplata uplateno,'
      
        'g.iznos_vkupno-(case when u.iznos is null then 0 else u.iznos en' +
        'd) saldo,'
      'tu.id||'#39' '#39'|| tu.naziv poteklo'
      'from  kom_lokacii l'
      
        'inner join mat_partner p  on l.tip_partner=p.tip_partner and l.p' +
        'artner_id=p.id'
      'inner join kom_presmetka_g g on g.lokaciin_id=l.id'
      'inner  join kom_reoni r on r.id=l.reon_id'
      
        'left outer join kom_uplati u on u.lokaciin_id=g.lokaciin_id and ' +
        'u.mesec=g.mesec and u.godina=g.godina'
      'left outer join kom_tip_uplata tu on tu.id=u.tip'
      'where l.tip_partner=5 and l.partner_id=:p')
    SelectSQL.Strings = (
      'select g.broj_faktura,p.tip_partner, p.id, p.naziv, p.adresa,'
      'g.datum_presmetka,g.iznos_vkupno dolzi,'
      'u.iznos pobaruva,u.datum_uplata uplateno,'
      
        'g.iznos_vkupno-(case when u.iznos is null then 0 else u.iznos en' +
        'd) saldo,'
      'tu.id||'#39' '#39'|| tu.naziv poteklo'
      'from  kom_lokacii l'
      
        'inner join mat_partner p  on l.tip_partner=p.tip_partner and l.p' +
        'artner_id=p.id'
      'inner join kom_presmetka_g g on g.lokaciin_id=l.id'
      'inner  join kom_reoni r on r.id=l.reon_id'
      
        'left outer join kom_uplati u on u.lokaciin_id=g.lokaciin_id and ' +
        'u.mesec=g.mesec and u.godina=g.godina and u.flag=1'
      'left outer join kom_tip_uplata tu on tu.id=u.tip'
      'where l.tip_partner=5 and l.partner_id=:p'
      'order by g.datum_presmetka')
    AutoUpdateOptions.UpdateTableName = 'MAT_PARTNER'
    AutoUpdateOptions.AutoReWriteSqls = True
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 707
    oFetchAll = True
    object tblKarParBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKarParTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblKarParID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblKarParNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKarParADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKarParDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblKarParDOLZI: TFIBBCDField
      FieldName = 'DOLZI'
      Size = 2
    end
    object tblKarParPOBARUVA: TFIBBCDField
      FieldName = 'POBARUVA'
      Size = 2
    end
    object tblKarParUPLATENO: TFIBDateField
      FieldName = 'UPLATENO'
    end
    object tblKarParSALDO: TFIBBCDField
      FieldName = 'SALDO'
      Size = 2
    end
    object tblKarParPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 62
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKarPar: TDataSource
    DataSet = tblKarPar
    Left = 120
    Top = 707
  end
  object tblKorisnik: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.tip_partner, p.id, p.naziv, p.adresa'
      'from mat_partner p'
      'where p.tip_partner in (1,5,6)')
    AutoUpdateOptions.UpdateTableName = 'MAT_PARTNER'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 544
    Top = 224
    object tblKorisnikTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblKorisnikID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblKorisnikNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKorisnikADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKorisnik: TDataSource
    DataSet = tblKorisnik
    Left = 624
    Top = 200
  end
  object TblPresmetkaG: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT '
      
        '    LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DATE)' +
        ') DATUM_PRESMETKA,'
      '    kpg.id,'
      '    KPG.LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KL.TIP_PARTNER,'
      '    KL.PARTNER_ID,MP.NAZIV,MP.ADRESA,KPG.IZNOS_VKUPNO'
      
        '    ,NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUG' +
        'A'
      '    ,KPG.VRABOTEN,'
      '    KPG.BROJ_FAKTURA'
      
        '    ,KL.REON_ID,KL.CITACKA_KNIGA, KL.RB1, KL.STANBENA_POVRSINA, ' +
        'KL.DVORNA_POVRSINA'
      '    ,(SELECT'
      
        '    SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO' +
        ',0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_05'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0)' +
        ' ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_18'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_05'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_18,'
      
        '--LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DATE))+' +
        '15 DA_SE_PLATI,'
      'kpg.ddo,'
      '(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      'where KVS.LOKACIIN_ID = kl.id and'
      '      KVS.MESEC = kpg.mesec and'
      '      KVS.GODINA = kpg.godina   ) vk_potroseno,'
      'kpg.ima_slika '
      ' --, (select  first 1 g.saldo saldo'
      
        '--from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,' +
        ':MAS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--order by g.godina||g.mesec desc)   saldo,'
      '--(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      '--from KOM_VODOMERI_SOSTOJBA KVS'
      '--where KVS.LOKACIIN_ID = :MAS_LOKACIIN_ID and'
      '--      KVS.MESEC = :MAS_MESEC and'
      '--      KVS.GODINA = :MAS_GODINA   ) vk_potroseno'
      'FROM KOM_PRESMETKA_G KPG'
      ', KOM_LOKACII KL, MAT_PARTNER MP,'
      '  kom_lokacija_arhiva kla'
      'WHERE( '
      'kla.id_lokacija = kl.id'
      'and KPG.LOKACIIN_ID=KLa.id_lokacija'
      'AND KLa.TIP_PARTNER=MP.TIP_PARTNER'
      'AND KLa.PARTNER=MP.ID'
      'and kpg.tip<>2'
      
        'and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '     or ((encodedate(1,kpg.mesec,kpg.godina) >= encodedate(1,kla' +
        '.poc_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kl' +
        'a.poc_mesec is not null) ))'
      '--AND O.ID=KL.ID'
      '--AND KPG.DATUM_PRESMETKA BETWEEN :D1 AND :D2'
      '--AND KL.PARTNER_ID=:P'
      '--AND KL.TIP_PARTNER = :TP'
      '--AND KPG.BROJ_FAKTURA LIKE :BROJ'
      '     ) and (     KPG.ID = :OLD_ID'
      '     )'
      '    '
      'union '
      'SELECT '
      
        '    LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DATE)' +
        ') DATUM_PRESMETKA,'
      '    kpg.id,'
      '    KPG.LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KL.TIP_PARTNER,'
      '    KL.PARTNER_ID,MP.NAZIV,MP.ADRESA,KPG.IZNOS_VKUPNO'
      
        '    ,NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUG' +
        'A'
      '    ,KPG.VRABOTEN,'
      '    KPG.BROJ_FAKTURA'
      
        '    ,KL.REON_ID,KL.CITACKA_KNIGA, KL.RB1, KL.STANBENA_POVRSINA, ' +
        'KL.DVORNA_POVRSINA'
      '    ,(SELECT'
      
        '    SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO' +
        ',0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_05'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0)' +
        ' ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_18'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_05'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_18,'
      
        '--LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DATE))+' +
        '15 DA_SE_PLATI,'
      'kpg.ddo,'
      '(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      'where KVS.LOKACIIN_ID = kl.id and'
      '      KVS.MESEC = kpg.mesec and'
      '      KVS.GODINA = kpg.godina   ) vk_potroseno,'
      'kpg.ima_slika '
      ' --, (select  first 1 g.saldo saldo'
      
        '--from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,' +
        ':MAS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--order by g.godina||g.mesec desc)   saldo,'
      '--(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      '--from KOM_VODOMERI_SOSTOJBA KVS'
      '--where KVS.LOKACIIN_ID = :MAS_LOKACIIN_ID and'
      '--      KVS.MESEC = :MAS_MESEC and'
      '--      KVS.GODINA = :MAS_GODINA   ) vk_potroseno'
      'FROM KOM_PRESMETKA_G KPG'
      ', KOM_LOKACII KL, MAT_PARTNER MP,'
      '  kom_lokacija_arhiva kla'
      'WHERE'
      'kla.id_lokacija = kl.id'
      'and KPG.LOKACIIN_ID=KLa.id_lokacija'
      'AND KLa.TIP_PARTNER=MP.TIP_PARTNER'
      'AND KLa.PARTNER=MP.ID'
      'and kpg.tip in (6,7)'
      '       and kla.tip_partner = kpg.tp and kla.partner = kpg.p'
      '       and (     KPG.ID = :OLD_ID'
      '     )')
    SelectSQL.Strings = (
      
        'SELECT LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DA' +
        'TE)) DATUM_PRESMETKA,'
      '    kpg.id,'
      '    KPG.LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KL.TIP_PARTNER,'
      '    KL.PARTNER_ID,MP.NAZIV,MP.ADRESA,KPG.IZNOS_VKUPNO'
      
        '    ,NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUG' +
        'A'
      '    ,KPG.VRABOTEN,'
      '    '
      '    KPG.BROJ_FAKTURA'
      
        '    ,KL.REON_ID,KL.CITACKA_KNIGA, KL.RB1, KL.STANBENA_POVRSINA, ' +
        'KL.DVORNA_POVRSINA'
      '    ,(SELECT'
      
        '    SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO' +
        ',0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_05'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0)' +
        ' ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_18'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_05'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_18,'
      
        '--LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DATE))+' +
        '15 DA_SE_PLATI,'
      'kpg.ddo,'
      '(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      'where KVS.LOKACIIN_ID = kl.id and'
      '      KVS.MESEC = kpg.mesec and'
      '      KVS.GODINA = kpg.godina   ) vk_potroseno,'
      'kpg.ima_slika '
      ' --, (select  first 1 g.saldo saldo'
      
        '--from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,' +
        ':MAS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--order by g.godina||g.mesec desc)   saldo,'
      '--(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      '--from KOM_VODOMERI_SOSTOJBA KVS'
      '--where KVS.LOKACIIN_ID = :MAS_LOKACIIN_ID and'
      '--      KVS.MESEC = :MAS_MESEC and'
      '--      KVS.GODINA = :MAS_GODINA   ) vk_potroseno'
      'FROM KOM_PRESMETKA_G KPG'
      ', KOM_LOKACII KL, MAT_PARTNER MP,'
      '  kom_lokacija_arhiva kla'
      'WHERE'
      'kla.id_lokacija = kl.id'
      'and KPG.LOKACIIN_ID=KLa.id_lokacija'
      'AND KLa.TIP_PARTNER=MP.TIP_PARTNER'
      'AND KLa.PARTNER=MP.ID'
      'and kpg.tip<>2'
      
        'and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '     or ((encodedate(1,kpg.mesec,kpg.godina) >= encodedate(1,kla' +
        '.poc_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kl' +
        'a.poc_mesec is not null) ))'
      '--AND O.ID=KL.ID'
      '--AND KPG.DATUM_PRESMETKA BETWEEN :D1 AND :D2'
      '--AND KL.PARTNER_ID=:P'
      '--AND KL.TIP_PARTNER = :TP'
      '--AND KPG.BROJ_FAKTURA LIKE :BROJ'
      'union '
      
        'SELECT  LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS D' +
        'ATE)) DATUM_PRESMETKA,'
      '    kpg.id,'
      '    KPG.LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KL.TIP_PARTNER,'
      '    KL.PARTNER_ID,MP.NAZIV,MP.ADRESA,KPG.IZNOS_VKUPNO'
      
        '    ,NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUG' +
        'A'
      '    ,KPG.VRABOTEN,'
      '    '
      '    KPG.BROJ_FAKTURA'
      
        '    ,KL.REON_ID,KL.CITACKA_KNIGA, KL.RB1, KL.STANBENA_POVRSINA, ' +
        'KL.DVORNA_POVRSINA'
      '    ,(SELECT'
      
        '    SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO' +
        ',0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_05'
      ',(SELECT'
      
        'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0)' +
        ' ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) OSNOVICA_DANOK_18'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_05'
      ',(SELECT'
      'SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'
      'FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=KPG.ID'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'AND KPG.TIP<>2) DANOK_18,'
      
        '--LASTDAYMONTH(CAST('#39'01.'#39'||KPG.MESEC||'#39'.'#39'||KPG.GODINA AS DATE))+' +
        '15 DA_SE_PLATI,'
      'kpg.ddo,'
      '(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      'where KVS.LOKACIIN_ID = kl.id and'
      '      KVS.MESEC = kpg.mesec and'
      '      KVS.GODINA = kpg.godina   ) vk_potroseno,'
      'kpg.ima_slika '
      ' --, (select  first 1 g.saldo saldo'
      
        '--from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,' +
        ':MAS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--order by g.godina||g.mesec desc)   saldo,'
      '--(select sum(kvs.nova_sostojba - kvs.stara_sostojba)'
      '--from KOM_VODOMERI_SOSTOJBA KVS'
      '--where KVS.LOKACIIN_ID = :MAS_LOKACIIN_ID and'
      '--      KVS.MESEC = :MAS_MESEC and'
      '--      KVS.GODINA = :MAS_GODINA   ) vk_potroseno'
      'FROM KOM_PRESMETKA_G KPG'
      ', KOM_LOKACII KL, MAT_PARTNER MP,'
      '  kom_lokacija_arhiva kla'
      'WHERE'
      'kla.id_lokacija = kl.id'
      'and KPG.LOKACIIN_ID=KLa.id_lokacija'
      'AND KLa.TIP_PARTNER=MP.TIP_PARTNER'
      'AND KLa.PARTNER=MP.ID'
      'and kpg.tip in (6,7)'
      '       and kla.tip_partner = kpg.tp and kla.partner = kpg.p')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 656
    Top = 201
    object TblPresmetkaGTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object TblPresmetkaGPARTNER_ID: TFIBIntegerField
      FieldName = 'PARTNER_ID'
    end
    object TblPresmetkaGNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGIZNOS_VKUPNO: TFIBBCDField
      FieldName = 'IZNOS_VKUPNO'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
    end
    object TblPresmetkaGNAZIV1: TFIBStringField
      FieldName = 'NAZIV1'
      Size = 1
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGIZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 1
    end
    object TblPresmetkaGDANOK: TFIBBCDField
      FieldName = 'DANOK'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 1
    end
    object TblPresmetkaGIZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 1
    end
    object TblPresmetkaGVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblPresmetkaGGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object TblPresmetkaGMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object TblPresmetkaGADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGREON_ID: TFIBIntegerField
      FieldName = 'REON_ID'
    end
    object TblPresmetkaGCITACKA_KNIGA: TFIBIntegerField
      FieldName = 'CITACKA_KNIGA'
    end
    object TblPresmetkaGDANOK_05: TFIBBCDField
      FieldName = 'DANOK_05'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 8
    end
    object TblPresmetkaGDANOK_18: TFIBBCDField
      FieldName = 'DANOK_18'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 8
    end
    object TblPresmetkaGOSNOVICA_DANOK_05: TFIBBCDField
      FieldName = 'OSNOVICA_DANOK_05'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 8
    end
    object TblPresmetkaGOSNOVICA_DANOK_18: TFIBBCDField
      FieldName = 'OSNOVICA_DANOK_18'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 8
    end
    object TblPresmetkaGSTANBENA_POVRSINA: TFIBIntegerField
      FieldName = 'STANBENA_POVRSINA'
    end
    object TblPresmetkaGDVORNA_POVRSINA: TFIBIntegerField
      FieldName = 'DVORNA_POVRSINA'
    end
    object TblPresmetkaGBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object TblPresmetkaGRB1: TFIBIntegerField
      FieldName = 'RB1'
    end
    object TblPresmetkaGID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TblPresmetkaGDDO: TFIBDateField
      FieldName = 'DDO'
    end
    object fbcdfldTblPresmetkaGVK_POTROSENO: TFIBBCDField
      FieldName = 'VK_POTROSENO'
      Size = 0
    end
    object TblPresmetkaGIMA_SLIKA: TFIBSmallIntField
      FieldName = 'IMA_SLIKA'
    end
  end
  object DSPresmetka: TDataSource
    DataSet = TblPresmetkaG
    Left = 696
    Top = 201
  end
  object TblPresmetkaS: TpFIBDataSet
    RefreshSQL.Strings = (
      '/*select distinct KPS.VID_USLUGA,'
      '                KPS.USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                KU.NAZIV,'
      '                KPS.IZNOS_USLUGA_NETO,'
      '                KPS.DANOK,'
      '                KPS.IZNOS_USLUGA,'
      '                KC.CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      (KPS.VID_USLUGA || KPS.USLUGA not in (16, 26)) and'
      '      KPS.VID_USLUGA <> 12'
      'union'
      'select distinct cast(1 as integer) VID_USLUGA,'
      '                cast(6 as integer) USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                cast('#39#1060#1054#1053#1044' '#1047#1040' '#1042#1054#1044#1048#39' as varchar(100)) NAZIV,'
      
        '                cast(sum(KPS.IZNOS_USLUGA_NETO) as decimal(15,3)' +
        ') IZNOS_USLUGA_NETO,'
      '                cast(sum(KPS.DANOK) as decimal(15,3)) DANOK,'
      
        '                cast(sum(KPS.IZNOS_USLUGA) as decimal(15,3)) IZN' +
        'OS_USLUGA,'
      '                cast(avg(KC.CENA) as decimal(15,2)) CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and/*'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA in (1, 2) and'
      '      KPS.USLUGA = 6 '
      'group by KPS.LOKACIIN_ID, KC.DANOK'
      'union'
      'select distinct cast(12 as integer) VID_USLUGA,'
      '                cast(1 as integer) USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                cast('#39#1047#1045#1051#1045#1053#1048#1051#1054#39' as varchar(100)) NAZIV,'
      
        '                cast(sum(KPS.IZNOS_USLUGA_NETO) as decimal(15,3)' +
        ') IZNOS_USLUGA_NETO,'
      '                cast(sum(KPS.DANOK) as decimal(15,3)) DANOK,'
      
        '                cast(sum(KPS.IZNOS_USLUGA) as decimal(15,3)) IZN' +
        'OS_USLUGA,'
      '                cast(avg(KC.CENA) as decimal(15,4)) CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA = 12'
      '--and kps.usluga=6'
      '--and kps.vid_usluga <> 12'
      'group by KPS.LOKACIIN_ID, KC.DANOK    '
      '*/'
      ''
      'SELECT DISTINCT'
      'KPS.VID_USLUGA,'
      'KPS.USLUGA,'
      'KPS.LOKACIIN_ID,'
      'KU.NAZIV,'
      'KPS.IZNOS_USLUGA_NETO,'
      'KPS.DANOK,'
      'KPS.IZNOS_USLUGA,'
      'KC.CENA,'
      'KC.DANOK PROCENT_DANOK'
      'FROM KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=:MAS_ID'
      'AND KPS.USLUGA=KU.ID'
      'AND KPS.VID_USLUGA=KU.VID_USLUGA'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'and (kps.vid_usluga||kps.usluga not in (16,26))'
      'union'
      'select distinct KPS.VID_USLUGA,'
      '                KPS.USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                KU.NAZIV,'
      '                KPS.IZNOS_USLUGA_NETO,'
      '                KPS.DANOK,'
      '                KPS.IZNOS_USLUGA,'
      '                KC.CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC /*$$IBEC$$ and'
      '      (KPS.VID_USLUGA || KPS.USLUGA not in (16, 26))'
      'union'
      'select distinct kps.VID_USLUGA,'
      '                kps.USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                ku.naziv,'
      
        '                cast(KPS.IZNOS_USLUGA_NETO as decimal(15,3)) IZN' +
        'OS_USLUGA_NETO,'
      '                cast(KPS.DANOK as decimal(15,3)) DANOK,'
      
        '                cast(kPS.IZNOS_USLUGA as decimal(15,3)) IZNOS_US' +
        'LUGA,'
      '                cast(KC.CENA as decimal(15,2)) CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA in (1, 2) and'
      '      KPS.USLUGA = 6'
      ' $$IBEC$$*/')
    SelectSQL.Strings = (
      '/*select distinct KPS.VID_USLUGA,'
      '                KPS.USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                KU.NAZIV,'
      '                KPS.IZNOS_USLUGA_NETO,'
      '                KPS.DANOK,'
      '                KPS.IZNOS_USLUGA,'
      '                KC.CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      (KPS.VID_USLUGA || KPS.USLUGA not in (16, 26)) and'
      '      KPS.VID_USLUGA <> 12'
      'union'
      'select distinct cast(1 as integer) VID_USLUGA,'
      '                cast(6 as integer) USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                cast('#39#1060#1054#1053#1044' '#1047#1040' '#1042#1054#1044#1048#39' as varchar(100)) NAZIV,'
      
        '                cast(sum(KPS.IZNOS_USLUGA_NETO) as decimal(15,3)' +
        ') IZNOS_USLUGA_NETO,'
      '                cast(sum(KPS.DANOK) as decimal(15,3)) DANOK,'
      
        '                cast(sum(KPS.IZNOS_USLUGA) as decimal(15,3)) IZN' +
        'OS_USLUGA,'
      '                cast(avg(KC.CENA) as decimal(15,2)) CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and/*'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA in (1, 2) and'
      '      KPS.USLUGA = 6 '
      'group by KPS.LOKACIIN_ID, KC.DANOK'
      'union'
      'select distinct cast(12 as integer) VID_USLUGA,'
      '                cast(1 as integer) USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                cast('#39#1047#1045#1051#1045#1053#1048#1051#1054#39' as varchar(100)) NAZIV,'
      
        '                cast(sum(KPS.IZNOS_USLUGA_NETO) as decimal(15,3)' +
        ') IZNOS_USLUGA_NETO,'
      '                cast(sum(KPS.DANOK) as decimal(15,3)) DANOK,'
      
        '                cast(sum(KPS.IZNOS_USLUGA) as decimal(15,3)) IZN' +
        'OS_USLUGA,'
      '                cast(avg(KC.CENA) as decimal(15,4)) CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA = 12'
      '--and kps.usluga=6'
      '--and kps.vid_usluga <> 12'
      'group by KPS.LOKACIIN_ID, KC.DANOK    '
      '*/'
      ''
      'SELECT DISTINCT'
      'KPS.VID_USLUGA,'
      'KPS.USLUGA,'
      'KPS.LOKACIIN_ID,'
      'KU.NAZIV,'
      'KPS.IZNOS_USLUGA_NETO,'
      'KPS.DANOK,'
      'KPS.IZNOS_USLUGA,'
      'KC.CENA,'
      'KC.DANOK PROCENT_DANOK'
      'FROM KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'WHERE KPS.PRESMETKA_G_ID=:MAS_ID'
      'AND KPS.USLUGA=KU.ID'
      'AND KPS.VID_USLUGA=KU.VID_USLUGA'
      'AND KC.USLUGA=KPS.USLUGA'
      'AND KC.VID_USLUGA=KPS.VID_USLUGA'
      'AND KC.GODINA=KPS.GODINA'
      'AND KC.MESEC=KPS.MESEC'
      'and (kps.vid_usluga||kps.usluga not in (16,26))'
      'union'
      'select distinct KPS.VID_USLUGA,'
      '                KPS.USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                KU.NAZIV,'
      '                KPS.IZNOS_USLUGA_NETO,'
      '                KPS.DANOK,'
      '                KPS.IZNOS_USLUGA,'
      '                KC.CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC /*$$IBEC$$ and'
      '      (KPS.VID_USLUGA || KPS.USLUGA not in (16, 26))'
      'union'
      'select distinct kps.VID_USLUGA,'
      '                kps.USLUGA,'
      '                KPS.LOKACIIN_ID,'
      '                ku.naziv,'
      
        '                cast(KPS.IZNOS_USLUGA_NETO as decimal(15,3)) IZN' +
        'OS_USLUGA_NETO,'
      '                cast(KPS.DANOK as decimal(15,3)) DANOK,'
      
        '                cast(kPS.IZNOS_USLUGA as decimal(15,3)) IZNOS_US' +
        'LUGA,'
      '                cast(KC.CENA as decimal(15,2)) CENA,'
      '                KC.DANOK PROCENT_DANOK'
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA in (1, 2) and'
      '      KPS.USLUGA = 6'
      ' $$IBEC$$*/')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 656
    Top = 233
    object TblPresmetkaSVID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object TblPresmetkaSUSLUGA: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object TblPresmetkaSLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblPresmetkaSNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaSIZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 8
    end
    object TblPresmetkaSDANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 8
    end
    object TblPresmetkaSIZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 8
    end
    object TblPresmetkaSCENA: TFIBBCDField
      FieldName = 'CENA'
      Size = 8
    end
    object TblPresmetkaSPROCENT_DANOK: TFIBIntegerField
      FieldName = 'PROCENT_DANOK'
    end
  end
  object DSPresmetka1: TDataSource
    DataSet = TblPresmetkaS
    Left = 696
    Top = 233
  end
  object frxReportKP: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39440.559617523100000000
    ReportOptions.LastChange = 39441.662998530100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 432
    Top = 224
    Datasets = <
      item
        DataSet = frxDSPresmetkaG
        DataSetName = 'frxDSPresmetkaG'
      end
      item
        DataSet = frxDSPresmetkaS
        DataSetName = 'frxDSPresmetkaS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'tahoma'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Subreport1: TfrxSubreport
        Left = 83.149660000000000000
        Top = 41.574830000000000000
        Width = 94.488250000000000000
        Height = 283.464750000000000000
        Page = frxReportKP.Page2
      end
      object Subreport2: TfrxSubreport
        ShiftMode = smWhenOverlapped
        Left = 79.370130000000000000
        Top = 328.819110000000000000
        Width = 94.488250000000000000
        Height = 328.819110000000000000
        Page = frxReportKP.Page3
      end
      object Subreport3: TfrxSubreport
        ShiftMode = smWhenOverlapped
        Left = 79.370130000000000000
        Top = 665.197280000000000000
        Width = 94.488250000000000000
        Height = 317.480520000000000000
        Page = frxReportKP.Page4
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 216.000000000000000000
      PaperHeight = 90.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'tahoma'
        Font.Style = []
        Height = 22.677180000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 740.787880000000000000
        Condition = 
          '<frxDBDataset1."LOKACIIN_ID">+<frxDBDataset1."GODINA">+<frxDBDat' +
          'aset1."MESEC">'
        KeepTogether = True
        StartNewPage = True
        object Memo3: TfrxMemoView
          Left = 506.457020000000000000
          Width = 219.212740000000000000
          Height = 22.677180000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[<frxDBDataset1."LOKACIIN_ID">')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 64.252010000000000000
        Width = 740.787880000000000000
        DataSet = frxDSPresmetkaG
        DataSetName = 'frxDSPresmetkaG'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779529999999994000
          Width = 219.212740000000000000
          Height = 22.677180000000000000
          DataField = 'IZNOS_VKUPNO'
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDSPresmetkaG."IZNOS_VKUPNO"]')
          ParentFont = False
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 90.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'tahoma'
        Font.Style = []
        Height = 22.677180000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        Condition = 
          '<frxDBDataset2."LOKACIIN_ID">+<frxDBDataset2."GODINA">+<frxDBDat' +
          'aset2."MESEC">'
        KeepTogether = True
        StartNewPage = True
        object Memo2: TfrxMemoView
          Left = 506.457020000000000000
          Width = 219.212740000000000000
          Height = 22.677180000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[<frxDBDataset2."LOKACIIN_ID">')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 64.252010000000000000
        Width = 740.409927000000000000
        DataSet = frxDSPresmetkaS
        DataSetName = 'frxDSPresmetkaS'
        RowCount = 0
        object Memo4: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779529999999994000
          Width = 219.212740000000000000
          Height = 22.677180000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDataset2."IZNOS_VKUPNO"]')
          ParentFont = False
        end
      end
    end
    object Page4: TfrxReportPage
      PaperWidth = 216.000000000000000000
      PaperHeight = 90.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'tahoma'
        Font.Style = []
        Height = 22.677180000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 740.787880000000000000
        Condition = 
          '<frxDBDataset3."LOKACIIN_ID">+<frxDBDataset3."GODINA">+<frxDBDat' +
          'aset3."MESEC">'
        KeepTogether = True
        StartNewPage = True
        object Memo5: TfrxMemoView
          Left = 506.457020000000000000
          Width = 219.212740000000000000
          Height = 22.677180000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[<frxDBDataset3."LOKACIIN_ID">')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 64.252010000000000000
        Width = 740.787880000000000000
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 22.677180000000000000
          Top = 3.779529999999994000
          Width = 219.212740000000000000
          Height = 22.677180000000000000
          DataSet = frxDSPresmetkaG
          DataSetName = 'frxDSPresmetkaG'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'tahoma'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDataset3."IZNOS_VKUPNO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDSPresmetkaG: TfrxDBDataset
    UserName = 'frxDSPresmetkaG'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER_ID=PARTNER_ID'
      'NAZIV=NAZIV'
      'IZNOS_VKUPNO=IZNOS_VKUPNO'
      'NAZIV1=NAZIV1'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'DANOK=DANOK'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'VRABOTEN=VRABOTEN'
      'LOKACIIN_ID=LOKACIIN_ID'
      'GODINA=GODINA'
      'MESEC=MESEC'
      'ADRESA=ADRESA'
      'REON_ID=REON_ID'
      'CITACKA_KNIGA=CITACKA_KNIGA'
      'DANOK_05=DANOK_05'
      'DANOK_18=DANOK_18'
      'OSNOVICA_DANOK_05=OSNOVICA_DANOK_05'
      'OSNOVICA_DANOK_18=OSNOVICA_DANOK_18'
      'STANBENA_POVRSINA=STANBENA_POVRSINA'
      'DVORNA_POVRSINA=DVORNA_POVRSINA'
      'BROJ_FAKTURA=BROJ_FAKTURA'
      'DATUM_PRESMETKA=DATUM_PRESMETKA'
      'RB1=RB1'
      'ID=ID'
      'DDO=DDO'
      'VK_POTROSENO=VK_POTROSENO'
      'IMA_SLIKA=IMA_SLIKA')
    OpenDataSource = False
    DataSource = DSPresmetka
    BCDToCurrency = False
    Left = 464
    Top = 224
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 504
    Top = 224
  end
  object frxDSPresmetkaS: TfrxDBDataset
    UserName = 'frxDSPresmetkaS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'LOKACIIN_ID=LOKACIIN_ID'
      'NAZIV=NAZIV'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'DANOK=DANOK'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'CENA=CENA'
      'PROCENT_DANOK=PROCENT_DANOK')
    OpenDataSource = False
    DataSource = DSPresmetka1
    BCDToCurrency = False
    Left = 464
    Top = 256
  end
  object TblPresmetkaVodomeriSostojba: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_VODOMERI_SOSTOJBA'
      'SET '
      '   -- NOVA_SOSTOJBA = :NOVA_SOSTOJBA,'
      '   -- STARA_SOSTOJBA = :STARA_SOSTOJBA,'
      '   -- SLIKA = :SLIKA,'
      '    IMG = :IMG'
      'WHERE'
      '    LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and VODOMERI_ID = :OLD_VODOMERI_ID'
      '    ')
    RefreshSQL.Strings = (
      'select KVS.LOKACIIN_ID,'
      '       KVS.VODOMERI_ID,'
      '       KVS.NOVA_SOSTOJBA,'
      '       KVS.STARA_SOSTOJBA,'
      '       kvs.slika,'
      '       v.VODOMERID br_vodomer'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      
        'inner join kom_vodomeri v on v.ID=kvs.VODOMERI_ID and v.LOKACIIN' +
        '_ID = kvs.LOKACIIN_ID'
      'where(  KVS.LOKACIIN_ID = :MAS_LOKACIIN_ID and'
      '      KVS.MESEC = :MAS_MESEC and'
      '      KVS.GODINA = :MAS_GODINA'
      '     ) and (     KVS.LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and KVS.VODOMERI_ID = :OLD_VODOMERI_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select KVS.LOKACIIN_ID,'
      '       KVS.VODOMERI_ID,'
      '       KVS.NOVA_SOSTOJBA,'
      '       KVS.STARA_SOSTOJBA,'
      '       kvs.slika,'
      '       kvs.img,'
      '       v.VODOMERID br_vodomer'
      'from KOM_VODOMERI_SOSTOJBA KVS'
      
        'inner join kom_vodomeri v on v.ID=kvs.VODOMERI_ID and v.LOKACIIN' +
        '_ID = kvs.LOKACIIN_ID'
      'where KVS.LOKACIIN_ID = :MAS_LOKACIIN_ID and'
      '      KVS.MESEC = :MAS_MESEC and'
      '      KVS.GODINA = :MAS_GODINA   ')
    AutoUpdateOptions.AutoParamsToFields = True
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 656
    Top = 265
    object TblPresmetkaVodomeriSostojbaLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblPresmetkaVodomeriSostojbaVODOMERI_ID: TFIBIntegerField
      FieldName = 'VODOMERI_ID'
    end
    object TblPresmetkaVodomeriSostojbaNOVA_SOSTOJBA: TFIBIntegerField
      FieldName = 'NOVA_SOSTOJBA'
    end
    object TblPresmetkaVodomeriSostojbaSTARA_SOSTOJBA: TFIBIntegerField
      FieldName = 'STARA_SOSTOJBA'
    end
    object TblPresmetkaVodomeriSostojbaSLIKA: TFIBStringField
      FieldName = 'SLIKA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaVodomeriSostojbaIMG: TFIBBlobField
      FieldName = 'IMG'
      Size = 8
    end
    object TblPresmetkaVodomeriSostojbaBR_VODOMER: TFIBStringField
      FieldName = 'BR_VODOMER'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object DSPresmetka2: TDataSource
    DataSet = TblPresmetkaVodomeriSostojba
    Left = 696
    Top = 265
  end
  object TblSaldo: TpFIBDataSet
    RefreshSQL.Strings = (
      'select sum(g.saldo) saldo'
      
        'from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,:M' +
        'AS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--select  first 1 g.saldo saldo'
      
        '--from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,' +
        ':MAS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--order by g.godina||g.mesec desc')
    SelectSQL.Strings = (
      'select sum(g.saldo) saldo'
      
        'from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,:M' +
        'AS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--select  first 1 g.saldo saldo'
      
        '--from proc_kom_saldo_korisnik(:datum_od,:MAS_MESEC,:MAS_GODINA,' +
        ':MAS_LOKACIIN_ID, :MAS_TIP_PARTNER, :MAS_PARTNER_ID) g'
      '--order by g.godina||g.mesec desc')
    AutoUpdateOptions.AutoParamsToFields = True
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 656
    Top = 297
    object fbcdfldTblSaldoSALDO: TFIBBCDField
      FieldName = 'SALDO'
      Size = 2
    end
  end
  object DSPresmetka3: TDataSource
    DataSet = TblSaldo
    Left = 696
    Top = 297
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 592
    Top = 232
  end
  object frxDSPresmetkaStavki5: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki5'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_MANIPULATIVEN=CENA_MANIPULATIVEN')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva5
    BCDToCurrency = False
    Left = 288
    Top = 424
  end
  object TblPresmetkaPoUslugi1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT DISTINCT'
      'KVS.LOKACIIN_ID,KVS.VODOMERI_ID'
      ',KVS.NOVA_SOSTOJBA,KVS.STARA_SOSTOJBA,'
      
        'KVS.RAZLIKA,  S.CENA CENA_VODA,KC.DANOK DDV, S.IZNOS_USLUGA,S.IZ' +
        'NOS_USLUGA_NETO'
      'FROM KOM_VODOMERI_SOSTOJBA KVS'
      
        'INNER JOIN KOM_PRESMETKA_S S ON S.LOKACIIN_ID=KVS.LOKACIIN_ID AN' +
        'D'
      'S.MESEC=KVS.MESEC AND S.GODINA=KVS.GODINA'
      
        'INNER JOIN KOM_CENOVNIK KC ON S.VID_USLUGA=KC.VID_USLUGA AND S.U' +
        'SLUGA=KC.USLUGA'
      'AND S.GODINA=KC.GODINA AND S.MESEC=KC.MESEC'
      'WHERE s.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 1'
      'AND S.USLUGA IN (2,3,5)'
      '')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 297
    oFetchAll = True
    object TblPresmetkaPoUslugi1LOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblPresmetkaPoUslugi1VODOMERI_ID: TFIBIntegerField
      FieldName = 'VODOMERI_ID'
    end
    object TblPresmetkaPoUslugi1NOVA_SOSTOJBA: TFIBIntegerField
      FieldName = 'NOVA_SOSTOJBA'
    end
    object TblPresmetkaPoUslugi1STARA_SOSTOJBA: TFIBIntegerField
      FieldName = 'STARA_SOSTOJBA'
    end
    object TblPresmetkaPoUslugi1RAZLIKA: TFIBBCDField
      FieldName = 'RAZLIKA'
      Size = 0
    end
    object TblPresmetkaPoUslugi1CENA_VODA: TFIBBCDField
      FieldName = 'CENA_VODA'
      Size = 2
    end
    object TblPresmetkaPoUslugi1DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
    object TblPresmetkaPoUslugi1IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUslugi1IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
  end
  object DSTblPresmetkaPoUslugu1: TDataSource
    DataSet = TblPresmetkaPoUslugi1
    Left = 403
    Top = 295
  end
  object TblPresmetkaPoUslugi2: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      'S.IZNOS_USLUGA_NETO'
      ', S.IZNOS_USLUGA, c.danok ddv,'
      'S.DANOK, S.CENA CENA_KAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 2'
      'AND S.USLUGA = 2'
      '--and :MAS_TIP=1')
    SelectSQL.Strings = (
      'SELECT'
      'S.IZNOS_USLUGA_NETO'
      ', S.IZNOS_USLUGA, c.danok ddv,'
      'S.DANOK, S.CENA CENA_KAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 2'
      'AND S.USLUGA = 2'
      '--and :MAS_TIP=1'
      '')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 329
    oFetchAll = True
    object TblPresmetkaPoUslugi2IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUslugi2IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUslugi2DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
    object TblPresmetkaPoUslugi2DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUslugi2CENA_KAN: TFIBBCDField
      FieldName = 'CENA_KAN'
      Size = 2
    end
  end
  object DSTblPresmetkaPoUslugi2: TDataSource
    DataSet = TblPresmetkaPoUslugi2
    Left = 403
    Top = 329
  end
  object TblPresmetkaPoUsluga3: TpFIBDataSet
    RefreshSQL.Strings = (
      '--SELECT S.VID_USLUGA, S.USLUGA,'
      '--S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok DDV,'
      '--S.DANOK, S.CENA CENA_STAN'
      '--FROM KOM_PRESMETKA_S s'
      
        '--inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_uslug' +
        'a=s.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      '---WHERE S.presmetka_g_id=:MAS_ID'
      '--AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (3,5,7)'
      '--AND S.USLUGA not IN (1,2,4,6)'
      '--and :MAS_TIP=1'
      '--ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok DDV,'
      'S.DANOK, S.CENA CENA_STAN'
      'FROM KOM_PRESMETKA_S s'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (3,5,7,10)'
      'AND S.USLUGA not IN (1,2,4,6)')
    SelectSQL.Strings = (
      '--SELECT S.VID_USLUGA, S.USLUGA,'
      '--S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok DDV,'
      '--S.DANOK, S.CENA CENA_STAN'
      '--FROM KOM_PRESMETKA_S s'
      
        '--inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_uslug' +
        'a=s.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      '---WHERE S.presmetka_g_id=:MAS_ID'
      '--AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (3,5,7)'
      '--AND S.USLUGA not IN (1,2,4,6)'
      '--and :MAS_TIP=1'
      '--ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok DDV,'
      'S.DANOK, S.CENA CENA_STAN'
      'FROM KOM_PRESMETKA_S s'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (3,5,7,10)'
      'AND S.USLUGA not IN (1,2,4,6)'
      'ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      '')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 361
    oFetchAll = True
    object TblPresmetkaPoUsluga3VID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object TblPresmetkaPoUsluga3USLUGA: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object TblPresmetkaPoUsluga3IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUsluga3IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUsluga3DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
    object TblPresmetkaPoUsluga3DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga3CENA_STAN: TFIBBCDField
      FieldName = 'CENA_STAN'
      Size = 8
    end
  end
  object DSTblPresmetkaPoUsluva3: TDataSource
    DataSet = TblPresmetkaPoUsluga3
    Left = 403
    Top = 361
  end
  object TblPresmetkaPoUsluga4: TpFIBDataSet
    RefreshSQL.Strings = (
      '--SELECT S.VID_USLUGA, S.USLUGA,'
      '--S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      '--S.DANOK, S.CENA CENA_DVOR'
      '--FROM KOM_PRESMETKA_S S'
      
        '--inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_uslug' +
        'a=s.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      '--WHERE S.presmetka_g_id=:MAS_ID'
      '--AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (4,6)'
      '--and :MAS_TIP=1'
      '--ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_DVOR'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 3'
      'AND S.USLUGA IN (4,6)')
    SelectSQL.Strings = (
      '--SELECT S.VID_USLUGA, S.USLUGA,'
      '--S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      '--S.DANOK, S.CENA CENA_DVOR'
      '--FROM KOM_PRESMETKA_S S'
      
        '--inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_uslug' +
        'a=s.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      '--WHERE S.presmetka_g_id=:MAS_ID'
      '--AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (4,6)'
      '--and :MAS_TIP=1'
      '--ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_DVOR'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 3'
      'AND S.USLUGA IN (4,6)'
      'ORDER BY S.VID_USLUGA,S.USLUGA')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 393
    oFetchAll = True
    object TblPresmetkaPoUsluga4VID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object TblPresmetkaPoUsluga4USLUGA: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object TblPresmetkaPoUsluga4IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUsluga4IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUsluga4DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga4CENA_DVOR: TFIBBCDField
      FieldName = 'CENA_DVOR'
      Size = 8
    end
    object TblPresmetkaPoUsluga4DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
  end
  object DSTblPresmetkaPoUsluva4: TDataSource
    DataSet = TblPresmetkaPoUsluga4
    Left = 403
    Top = 393
  end
  object TblPresmetkaPoUsluga5: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_MANIPULATIVEN'
      'from KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'where S.presmetka_g_id=:MAS_ID'
      'and S.VID_USLUGA = 5'
      'and S.USLUGA = 2'
      '--and :MAS_TIP=1')
    SelectSQL.Strings = (
      'select'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_MANIPULATIVEN'
      'from KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'where S.presmetka_g_id=:MAS_ID'
      'and S.VID_USLUGA = 5'
      'and S.USLUGA = 2'
      '--and :MAS_TIP=1')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 425
    oFetchAll = True
    object TblPresmetkaPoUsluga5IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUsluga5IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUsluga5DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
    object TblPresmetkaPoUsluga5DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga5CENA_MANIPULATIVEN: TFIBBCDField
      FieldName = 'CENA_MANIPULATIVEN'
      Size = 2
    end
  end
  object DSTblPresmetkaPoUsluva5: TDataSource
    DataSet = TblPresmetkaPoUsluga5
    Left = 403
    Top = 425
  end
  object TblPresmetkaPoUsluga6: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok ddv,'
      'S.DANOK, S.CENA CENA_FVODA'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 1'
      'AND S.USLUGA = 6'
      '--and :MAS_TIP=1')
    SelectSQL.Strings = (
      'SELECT S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok ddv,'
      'S.DANOK, S.CENA CENA_FVODA'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 1'
      'AND S.USLUGA = 6'
      '--and :MAS_TIP=1')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 457
    oFetchAll = True
    object TblPresmetkaPoUsluga6IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUsluga6IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUsluga6DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga6CENA_FVODA: TFIBBCDField
      FieldName = 'CENA_FVODA'
      Size = 2
    end
    object TblPresmetkaPoUsluga6DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
  end
  object DSTblPresmetkaPoUsluva6: TDataSource
    DataSet = TblPresmetkaPoUsluga6
    Left = 403
    Top = 457
  end
  object TblPresmetkaPoUsluga7: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_FKAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 2'
      'AND S.USLUGA = 6'
      '')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 360
    Top = 489
    oFetchAll = True
    object TblPresmetkaPoUsluga7IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUsluga7IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUsluga7DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
    object TblPresmetkaPoUsluga7DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga7CENA_FKAN: TFIBBCDField
      FieldName = 'CENA_FKAN'
      Size = 2
    end
  end
  object DSTblPresmetkaPoUsluva7: TDataSource
    DataSet = TblPresmetkaPoUsluga7
    Left = 403
    Top = 489
  end
  object tblStornoG: TpFIBDataSet
    RefreshSQL.Strings = (
      'select g.*'
      'from kom_presmetka_g g'
      ''
      ' WHERE '
      '        G.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select g.*'
      'from kom_presmetka_g g')
    AutoUpdateOptions.UpdateTableName = 'KOM_PRESMETKA_G'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 536
    Top = 393
    object tblStornoGID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblStornoGLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object tblStornoGMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblStornoGGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblStornoGIZNOS_VKUPNO: TFIBBCDField
      FieldName = 'IZNOS_VKUPNO'
      Size = 2
    end
    object tblStornoGDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblStornoGVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStornoGBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStornoGIZNOS_PLATENO: TFIBBCDField
      FieldName = 'IZNOS_PLATENO'
      Size = 2
    end
    object tblStornoGRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblStornoGTIP_NALOG: TFIBStringField
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStornoGNALOG: TFIBIntegerField
      FieldName = 'NALOG'
    end
    object tblStornoGTS_KNIZENJE: TFIBDateTimeField
      FieldName = 'TS_KNIZENJE'
    end
    object tblStornoGOPOMENA_BROJ: TFIBStringField
      FieldName = 'OPOMENA_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStornoGTIP: TFIBIntegerField
      FieldName = 'TIP'
    end
    object tblStornoGDDO: TFIBDateField
      FieldName = 'DDO'
    end
    object tblStornoGID_KOREKCIJA: TFIBIntegerField
      FieldName = 'ID_KOREKCIJA'
    end
  end
  object dsStornoG: TDataSource
    DataSet = tblStornoG
    Left = 576
    Top = 393
  end
  object dsStornoS: TDataSource
    DataSet = tblStornoS
    Left = 576
    Top = 433
  end
  object tblStornoS: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT *'
      'FROM KOM_PRESMETKA_S S'
      '')
    AutoUpdateOptions.UpdateTableName = 'KOM_PRESMETKA_S'
    AutoUpdateOptions.KeyFields = 'id;presmetka_g_id'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 536
    Top = 433
    object tblStornoSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblStornoSPRESMETKA_G_ID: TFIBIntegerField
      FieldName = 'PRESMETKA_G_ID'
    end
    object tblStornoSLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object tblStornoSMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblStornoSGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblStornoSVID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object tblStornoSUSLUGA: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object tblStornoSIZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object tblStornoSIZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object tblStornoSDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblStornoSVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStornoSDANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object tblStornoSCENA: TFIBBCDField
      FieldName = 'CENA'
      Size = 2
    end
  end
  object qMaks: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select gen_id(gen_kom_presmetka_g_id,1) id, cast(coalesce(max(su' +
        'bstr(g.broj_faktura, strlen(g.broj_faktura),strlen(g.broj_faktur' +
        'a))),0) as integer) maks'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l'
      'and g.mesec=:m'
      'and g.godina=:g'
      'and g.tip=1')
    Left = 622
    Top = 336
  end
  object qPovrsina: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select g.usluga, cast(g.iznos_usluga_neto/g.cena as integer) pov' +
        'rsina, kc.nacin_presmetka'
      'from kom_presmetka_s g'
      
        'inner join kom_cenovnik kc on kc.usluga=g.usluga and kc.vid_uslu' +
        'ga=g.vid_usluga and kc.godina=g.godina and kc.mesec=g.mesec'
      'where g.lokaciin_id=:l and g.mesec=:m and g.godina=:g'
      'and g.vid_usluga=3 and (g.iznos_usluga_neto/g.cena)>0')
    Left = 622
    Top = 392
  end
  object qLokaciiTemp: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_lokacii u'
      'set u.stanbena_povrsina=:sp, u.dvorna_povrsina=:dp'
      'where u.id=:l')
    Left = 654
    Top = 424
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qLokacii: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select l.stanbena_povrsina,l.dvorna_povrsina'
      'from kom_lokacii l'
      'where l.id=:l')
    Left = 654
    Top = 392
  end
  object qUpdateUplati: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_uplati u'
      'set u.presmetka_g_id=:p'
      'where u.lokaciin_id=:l and u.mesec=:m and u.godina=:g')
    Left = 686
    Top = 424
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qKluc: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select g.id'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l'
      'and g.mesec=:m'
      'and g.godina=:g'
      'and g.tip=1')
    Left = 686
    Top = 392
  end
  object TblPresmetkaPoUsluga8: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_FKAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 9'
      'AND S.USLUGA = 1'
      '')
    SelectSQL.Strings = (
      'SELECT S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_FKAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 9'
      'AND S.USLUGA = 1'
      '')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 363
    Top = 529
    oFetchAll = True
    object FIBBCDField1: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object FIBBCDField2: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object FIBIntegerField1: TFIBIntegerField
      FieldName = 'DDV'
    end
    object FIBBCDField3: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object FIBBCDField4: TFIBBCDField
      FieldName = 'CENA_FKAN'
      Size = 2
    end
  end
  object DSTblPresmetkaPoUsluva8: TDataSource
    DataSet = TblPresmetkaPoUsluga8
    Left = 403
    Top = 529
  end
  object frxDSPresmetkaStavki3: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_STAN=CENA_STAN')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva3
    BCDToCurrency = False
    Left = 288
    Top = 361
  end
  object frxDSPresmetkaStavki1: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LOKACIIN_ID=LOKACIIN_ID'
      'VODOMERI_ID=VODOMERI_ID'
      'NOVA_SOSTOJBA=NOVA_SOSTOJBA'
      'STARA_SOSTOJBA=STARA_SOSTOJBA'
      'RAZLIKA=RAZLIKA'
      'CENA_VODA=CENA_VODA'
      'DDV=DDV'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUslugu1
    BCDToCurrency = False
    Left = 288
    Top = 296
  end
  object frxDSPresmetkaStavki2: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_KAN=CENA_KAN')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUslugi2
    BCDToCurrency = False
    Left = 288
    Top = 324
  end
  object tblLokacija: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct'
      '    l.id lokacija,'
      '    p.tip_partner,'
      '    p.id,'
      '    p.naziv,'
      '    p.adresa,'
      '    l.reon_id,'
      '    kr.naziv naziv_reon,'
      '    l.citacka_kniga,'
      '    l.aktiven'
      'from  kom_lokacii l'
      'inner join kom_lokacija_arhiva kla on kla.id_lokacija=l.id'
      
        'inner join mat_partner p  on kla.tip_partner=p.tip_partner and k' +
        'la.partner=p.id'
      'inner join kom_presmetka_g g on g.lokaciin_id=kla.id_lokacija'
      'inner join kom_reoni kr on kr.id=l.reon_id'
      'where kla.tip_partner=:tp and kla.partner=:p --and g.tip=1'
      
        '    and lastdaymonth((cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as date' +
        '))) between :d1 and :d2'
      
        '    and (((encodedate(1,g.mesec,g.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '      or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.po' +
        'c_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.p' +
        'oc_mesec is not null) and (kla.poc_mesec is not null) ))')
    SelectSQL.Strings = (
      'select distinct'
      '    l.id lokacija,'
      '    p.tip_partner,'
      '    p.id,'
      '    p.naziv,'
      '    p.adresa,'
      '    l.reon_id,'
      '    kr.naziv naziv_reon,'
      '    l.citacka_kniga,'
      '    l.aktiven'
      'from  kom_lokacii l'
      'inner join kom_lokacija_arhiva kla on kla.id_lokacija=l.id'
      
        'inner join mat_partner p  on kla.tip_partner=p.tip_partner and k' +
        'la.partner=p.id'
      'inner join kom_presmetka_g g on g.lokaciin_id=kla.id_lokacija'
      'inner join kom_reoni kr on kr.id=l.reon_id'
      'where kla.tip_partner=:tp and kla.partner=:p --and g.tip=1'
      
        '    and lastdaymonth((cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as date' +
        '))) between :d1 and :d2'
      
        '    and (((encodedate(1,g.mesec,g.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '      or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.po' +
        'c_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.p' +
        'oc_mesec is not null) and (kla.poc_mesec is not null) ))')
    AutoUpdateOptions.UpdateTableName = 'MAT_PARTNER'
    AutoUpdateOptions.AutoReWriteSqls = True
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 722
    oFetchAll = True
    object tblLokacijaLOKACIJA: TFIBIntegerField
      FieldName = 'LOKACIJA'
    end
    object tblLokacijaTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblLokacijaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblLokacijaNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLokacijaADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLokacijaREON_ID: TFIBIntegerField
      FieldName = 'REON_ID'
    end
    object tblLokacijaNAZIV_REON: TFIBStringField
      FieldName = 'NAZIV_REON'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLokacijaCITACKA_KNIGA: TFIBIntegerField
      FieldName = 'CITACKA_KNIGA'
    end
    object tblLokacijaAKTIVEN: TFIBSmallIntField
      FieldName = 'AKTIVEN'
    end
  end
  object dsLokacija: TDataSource
    DataSet = tblLokacija
    Left = 119
    Top = 723
  end
  object DataSource2: TDataSource
    Left = 352
    Top = 65288
  end
  object frxDSPresmetkaStavki4: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki4'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_DVOR=CENA_DVOR'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva4
    BCDToCurrency = False
    Left = 288
    Top = 384
  end
  object frxDSPresmetkaStavki6: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki6'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FVODA=CENA_FVODA'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva6
    BCDToCurrency = False
    Left = 288
    Top = 456
  end
  object frxDSPresmetkaStavki7: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki7'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_FKAN=CENA_FKAN')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva7
    BCDToCurrency = False
    Left = 288
    Top = 488
  end
  object frxDSPresmetkaStavki8: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki8'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_FKAN=CENA_FKAN')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva8
    BCDToCurrency = False
    Left = 288
    Top = 528
  end
  object frxDSPresmetkaVodomeriSostojba: TfrxDBDataset
    UserName = 'frxDSPresmetkaVodomeriSostojba'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LOKACIIN_ID=LOKACIIN_ID'
      'VODOMERI_ID=VODOMERI_ID'
      'NOVA_SOSTOJBA=NOVA_SOSTOJBA'
      'STARA_SOSTOJBA=STARA_SOSTOJBA'
      'SLIKA=SLIKA'
      'IMG=IMG'
      'BR_VODOMER=BR_VODOMER')
    OpenDataSource = False
    DataSource = DSPresmetka2
    BCDToCurrency = False
    Left = 536
    Top = 288
  end
  object frxDSSaldo: TfrxDBDataset
    UserName = 'frxDSSaldo'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SALDO=SALDO')
    OpenDataSource = False
    DataSource = DSPresmetka3
    BCDToCurrency = False
    Left = 536
    Top = 336
  end
  object frxDSPresmetkaStavki10: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki10'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva10
    BCDToCurrency = False
    Left = 184
    Top = 528
  end
  object TblPresmetkaPoUsluga10: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    S.IZNOS_USLUGA_NETO,'
      '    S.IZNOS_USLUGA,'
      '    c.danok ddv,'
      '    S.DANOK,'
      '    S.CENA CENA_NADOMESTOK,'
      '    kvu.naziv naziv_vid_usluga,'
      '    ku.naziv naziv_usluga'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'inner join kom_vidovi_uslugi kvu on kvu.id=s.vid_usluga'
      
        'inner join kom_uslugi ku on ku.vid_usluga=kvu.id and ku.id=s.usl' +
        'uga'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 10'
      '--AND S.USLUGA = 6')
    SelectSQL.Strings = (
      'SELECT'
      '    S.IZNOS_USLUGA_NETO,'
      '    S.IZNOS_USLUGA,'
      '    c.danok ddv,'
      '    S.DANOK,'
      '    S.CENA CENA_NADOMESTOK,'
      '    kvu.naziv naziv_vid_usluga,'
      '    ku.naziv naziv_usluga'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'inner join kom_vidovi_uslugi kvu on kvu.id=s.vid_usluga'
      
        'inner join kom_uslugi ku on ku.vid_usluga=kvu.id and ku.id=s.usl' +
        'uga'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 10'
      '--AND S.USLUGA = 6')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 507
    Top = 529
    oFetchAll = True
    object TblPresmetkaPoUsluga10IZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaPoUsluga10IZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaPoUsluga10DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
    object TblPresmetkaPoUsluga10DANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga10CENA_NADOMESTOK: TFIBBCDField
      FieldName = 'CENA_NADOMESTOK'
      Size = 2
    end
    object TblPresmetkaPoUsluga10NAZIV_VID_USLUGA: TFIBStringField
      FieldName = 'NAZIV_VID_USLUGA'
      Size = 60
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaPoUsluga10NAZIV_USLUGA: TFIBStringField
      FieldName = 'NAZIV_USLUGA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object DSTblPresmetkaPoUsluva10: TDataSource
    DataSet = TblPresmetkaPoUsluga10
    Left = 547
    Top = 529
  end
  object TblDPresmetkaPoUsluga2: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      'S.IZNOS_USLUGA_NETO'
      ', S.IZNOS_USLUGA, c.danok ddv,'
      'S.DANOK, S.CENA CENA_KAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 2'
      'AND S.USLUGA = 2')
    SelectSQL.Strings = (
      'SELECT'
      'S.IZNOS_USLUGA_NETO'
      ', S.IZNOS_USLUGA, c.danok ddv,'
      'S.DANOK, S.CENA CENA_KAN'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 2'
      'AND S.USLUGA = 2')
    AutoUpdateOptions.AutoReWriteSqls = True
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 648
    Top = 782
    oFetchAll = True
    object FIBBCDField18: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object FIBBCDField19: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object FIBBCDField20: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object FIBBCDField21: TFIBBCDField
      FieldName = 'CENA_KAN'
      Size = 2
    end
    object FIBIntegerField24: TFIBIntegerField
      FieldName = 'DDV'
    end
  end
  object DSDPresmetkaPoUsluva2: TDataSource
    DataSet = TblDPresmetkaPoUsluga2
    Left = 709
    Top = 783
  end
  object TblPresmetkaPoUsluga61: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    S.IZNOS_USLUGA_NETO,'
      '    S.IZNOS_USLUGA,'
      '    c.danok ddv,'
      '    S.DANOK,'
      '    S.CENA CENA_NADOMESTOK,'
      '    kvu.naziv naziv_vid_usluga,'
      '    ku.naziv naziv_usluga'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'inner join kom_vidovi_uslugi kvu on kvu.id=s.vid_usluga'
      
        'inner join kom_uslugi ku on ku.vid_usluga=kvu.id and ku.id=s.usl' +
        'uga'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 10'
      '--AND S.USLUGA = 6')
    SelectSQL.Strings = (
      'SELECT'
      '    S.IZNOS_USLUGA_NETO,'
      '    S.IZNOS_USLUGA,'
      '    c.danok ddv,'
      '    S.DANOK,'
      '    S.CENA CENA_NADOMESTOK,'
      '    kvu.naziv naziv_vid_usluga,'
      '    ku.naziv naziv_usluga'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'inner join kom_vidovi_uslugi kvu on kvu.id=s.vid_usluga'
      
        'inner join kom_uslugi ku on ku.vid_usluga=kvu.id and ku.id=s.usl' +
        'uga'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 6'
      'AND S.USLUGA = 1')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 507
    Top = 489
    oFetchAll = True
    object FIBBCDField5: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object FIBBCDField6: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object FIBIntegerField2: TFIBIntegerField
      FieldName = 'DDV'
    end
    object FIBBCDField7: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object FIBBCDField8: TFIBBCDField
      FieldName = 'CENA_NADOMESTOK'
      Size = 2
    end
    object FIBStringField1: TFIBStringField
      FieldName = 'NAZIV_VID_USLUGA'
      Size = 60
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'NAZIV_USLUGA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTblPresmetkaPoUsluva61: TDataSource
    DataSet = TblPresmetkaPoUsluga61
    Left = 547
    Top = 489
  end
  object frxDSPresmetkaStavki61: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki61'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = dsTblPresmetkaPoUsluva61
    BCDToCurrency = False
    Left = 184
    Top = 488
  end
  object qTuzbaSmetka: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'SELECT'
      ' distinct 1 ima'
      'FROM'
      '    kom_tuzba_smetki t'
      '    inner join kom_presmetka_g g on g.id = t.smetka_id'
      'where t.smetka_id = :smetka_id')
    Left = 808
    Top = 304
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object DSTblPresmetkaPoUsluva11: TDataSource
    DataSet = TblPresmetkaPoUsluga11
    Left = 835
    Top = 361
  end
  object TblPresmetkaPoUsluga11: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    S.IZNOS_USLUGA_NETO,'
      '    S.IZNOS_USLUGA,'
      '    c.danok ddv,'
      '    S.DANOK,'
      '    S.CENA CENA_NADOMESTOK,'
      '    kvu.naziv naziv_vid_usluga,'
      '    ku.naziv naziv_usluga'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'inner join kom_vidovi_uslugi kvu on kvu.id=s.vid_usluga'
      
        'inner join kom_uslugi ku on ku.vid_usluga=kvu.id and ku.id=s.usl' +
        'uga'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 11'
      '--AND S.USLUGA = 6')
    SelectSQL.Strings = (
      'SELECT'
      '    S.IZNOS_USLUGA_NETO,'
      '    S.IZNOS_USLUGA,'
      '    c.danok ddv,'
      '    S.DANOK,'
      '    S.CENA CENA_NADOMESTOK,'
      '    kvu.naziv naziv_vid_usluga,'
      '    ku.naziv naziv_usluga'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'inner join kom_vidovi_uslugi kvu on kvu.id=s.vid_usluga'
      
        'inner join kom_uslugi ku on ku.vid_usluga=kvu.id and ku.id=s.usl' +
        'uga'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 11'
      '--AND S.USLUGA = 6')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 795
    Top = 361
    oFetchAll = True
    object FIBBCDField9: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object FIBBCDField10: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object FIBIntegerField3: TFIBIntegerField
      FieldName = 'DDV'
    end
    object FIBBCDField11: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object FIBBCDField12: TFIBBCDField
      FieldName = 'CENA_NADOMESTOK'
      Size = 2
    end
    object FIBStringField3: TFIBStringField
      FieldName = 'NAZIV_VID_USLUGA'
      Size = 60
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      FieldName = 'NAZIV_USLUGA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDSPresmetkaStavki11: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki11'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUsluva11
    BCDToCurrency = False
    Left = 888
    Top = 360
  end
  object DSTblPresmetkaPoUslugi12: TDataSource
    DataSet = TblPresmetkaPoUsluga12
    Left = 179
    Top = 321
  end
  object TblPresmetkaPoUsluga12: TpFIBDataSet
    RefreshSQL.Strings = (
      '/*select cast(12 as integer) VID_USLUGA,'
      '       cast(1 as integer) USLUGA,'
      
        '       cast(sum(KPS.IZNOS_USLUGA_NETO) as decimal(15,3)) IZNOS_U' +
        'SLUGA_NETO,'
      
        '       cast(sum(KPS.IZNOS_USLUGA) as decimal(15,3)) IZNOS_USLUGA' +
        ','
      '       KC.DANOK DDV,'
      '       cast(sum(KPS.DANOK) as decimal(15,3)) DANOK,'
      '       cast(avg(KC.CENA) as decimal(15,4)) CENA_STAN'
      ''
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA = 12 and'
      '      KPS.USLUGA not in (1, 2)'
      'group by KPS.LOKACIIN_ID, KC.DANOK  '
      ''
      '*/'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok DDV,'
      'S.DANOK, S.CENA CENA_STAN'
      'FROM KOM_PRESMETKA_S s'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 12'
      '--AND S.USLUGA IN (3,5,7,10)'
      'AND S.USLUGA not IN (1,2,4,6)')
    SelectSQL.Strings = (
      '/*select cast(12 as integer) VID_USLUGA,'
      '       cast(1 as integer) USLUGA,'
      
        '       cast(sum(KPS.IZNOS_USLUGA_NETO) as decimal(15,3)) IZNOS_U' +
        'SLUGA_NETO,'
      
        '       cast(sum(KPS.IZNOS_USLUGA) as decimal(15,3)) IZNOS_USLUGA' +
        ','
      '       KC.DANOK DDV,'
      '       cast(sum(KPS.DANOK) as decimal(15,3)) DANOK,'
      '       cast(avg(KC.CENA) as decimal(15,4)) CENA_STAN'
      ''
      'from KOM_PRESMETKA_S KPS, KOM_USLUGI KU, KOM_CENOVNIK KC'
      'where KPS.PRESMETKA_G_ID = :MAS_ID and'
      '      KPS.USLUGA = KU.ID and'
      '      KPS.VID_USLUGA = KU.VID_USLUGA and'
      '      KC.USLUGA = KPS.USLUGA and'
      '      KC.VID_USLUGA = KPS.VID_USLUGA and'
      '      KC.GODINA = KPS.GODINA and'
      '      KC.MESEC = KPS.MESEC and'
      '      KPS.VID_USLUGA = 12 and'
      '      KPS.USLUGA not in (1, 2)'
      'group by KPS.LOKACIIN_ID, KC.DANOK  '
      ''
      '*/'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA, c.danok DDV,'
      'S.DANOK, S.CENA CENA_STAN'
      'FROM KOM_PRESMETKA_S s'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 12'
      '--AND S.USLUGA IN (3,5,7,10)'
      'AND S.USLUGA not IN (1,2,4,6)'
      'ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      ''
      '')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 120
    Top = 321
    oFetchAll = True
    object FIBIntegerField4: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object FIBIntegerField5: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object FIBBCDField13: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object FIBBCDField14: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object FIBBCDField15: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaPoUsluga12CENA_STAN: TFIBBCDField
      FieldName = 'CENA_STAN'
      Size = 8
    end
    object TblPresmetkaPoUsluga12DDV: TFIBIntegerField
      FieldName = 'DDV'
    end
  end
  object frxDSPresmetkaStavki12: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki12'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DDV=DDV'
      'DANOK=DANOK'
      'CENA_STAN=CENA_STAN')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUslugi12
    BCDToCurrency = False
    Left = 56
    Top = 321
  end
  object DSTblPresmetkaPoUslugi121: TDataSource
    DataSet = TblPresmetkaPoUsluga121
    Left = 179
    Top = 369
  end
  object TblPresmetkaPoUsluga121: TpFIBDataSet
    RefreshSQL.Strings = (
      '--SELECT S.VID_USLUGA, S.USLUGA,'
      '--S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      '--S.DANOK, S.CENA CENA_DVOR'
      '--FROM KOM_PRESMETKA_S S'
      
        '--inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_uslug' +
        'a=s.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      '--WHERE S.presmetka_g_id=:MAS_ID'
      '--AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (4,6)'
      '--and :MAS_TIP=1'
      '--ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_DVOR'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 12'
      'AND S.USLUGA IN (4,6)')
    SelectSQL.Strings = (
      '--SELECT S.VID_USLUGA, S.USLUGA,'
      '--S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      '--S.DANOK, S.CENA CENA_DVOR'
      '--FROM KOM_PRESMETKA_S S'
      
        '--inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_uslug' +
        'a=s.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      '--WHERE S.presmetka_g_id=:MAS_ID'
      '--AND S.VID_USLUGA = 3'
      '--AND S.USLUGA IN (4,6)'
      '--and :MAS_TIP=1'
      '--ORDER BY S.VID_USLUGA,S.USLUGA'
      ''
      'SELECT S.VID_USLUGA, S.USLUGA,'
      'S.IZNOS_USLUGA_NETO, S.IZNOS_USLUGA,c.danok ddv,'
      'S.DANOK, S.CENA CENA_DVOR'
      'FROM KOM_PRESMETKA_S S'
      
        'inner join kom_cenovnik c on c.usluga=s.usluga and c.vid_usluga=' +
        's.vid_usluga and c.godina=s.godina and c.mesec=s.mesec'
      'WHERE S.presmetka_g_id=:MAS_ID'
      'AND S.VID_USLUGA = 12'
      'AND S.USLUGA IN (4,6)'
      'ORDER BY S.VID_USLUGA,S.USLUGA')
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 120
    Top = 369
    oFetchAll = True
    object FIBIntegerField7: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object FIBIntegerField8: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object FIBBCDField17: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object FIBBCDField22: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object FIBBCDField23: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object FIBBCDField24: TFIBBCDField
      FieldName = 'CENA_DVOR'
      Size = 2
    end
    object FIBIntegerField9: TFIBIntegerField
      FieldName = 'DDV'
    end
  end
  object frxDSPresmetkaStavki121: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki121'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_DVOR=CENA_DVOR'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DSTblPresmetkaPoUslugi121
    BCDToCurrency = False
    Left = 56
    Top = 368
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 368
    Top = 8
  end
  object tblSlika: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 688
    Top = 349
    object tblSlikaSlika: TBlobField
      FieldName = 'Slika'
    end
  end
  object dsSlika: TDataSource
    DataSet = tblSlika
    Left = 728
    Top = 349
  end
  object frxSlika: TfrxDBDataset
    UserName = 'frxSlika'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RecId=RecId'
      'Slika=Slika')
    DataSource = dsSlika
    BCDToCurrency = False
    Left = 768
    Top = 349
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'png'
    Left = 248
    Top = 8
  end
  object tblVodSost: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_VODOMERI_SOSTOJBA'
      'SET '
      '    IMG = :IMG'
      'WHERE'
      '    LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and VODOMERI_ID = :OLD_VODOMERI_ID'
      '    ')
    RefreshSQL.Strings = (
      'select v.img, v.lokaciin_id, v.vodomeri_id,slika'
      'from kom_vodomeri_sostojba v'
      'where(  v.lokaciin_id=:lokacija '
      ' and v.mesec=:MESEC '
      'and v.godina=:GODINA '
      '--and v.vodomeri_id = :vodomer'
      '     ) and (     V.LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and V.VODOMERI_ID = :OLD_VODOMERI_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select v.img, v.lokaciin_id, v.vodomeri_id,slika'
      'from kom_vodomeri_sostojba v'
      'where v.lokaciin_id=:lokacija '
      ' and v.mesec=:MESEC '
      'and v.godina=:GODINA '
      '--and v.vodomeri_id = :vodomer')
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 846
    Top = 176
    oRefreshDeletedRecord = True
    object tblVodSostIMG: TFIBBlobField
      FieldName = 'IMG'
      Size = 8
    end
    object tblVodSostLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object tblVodSostVODOMERI_ID: TFIBIntegerField
      FieldName = 'VODOMERI_ID'
    end
    object tblVodSostSLIKA: TFIBStringField
      FieldName = 'SLIKA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsVodSost: TDataSource
    DataSet = tblVodSost
    Left = 878
    Top = 176
  end
  object IdHTTP2: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 312
    Top = 8
  end
  object IdDecoderMIME1: TIdDecoderMIME
    FillChar = '='
    Left = 844
    Top = 61
  end
  object tblSiteStavki: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    s.presmetka_g_id,'
      '    s.vid_usluga,'
      '    s.usluga,'
      '    u.naziv naziv_usluga,'
      '    s.iznos_usluga_neto,'
      '    s.iznos_usluga,'
      '    s.datum_presmetka,'
      '    s.vraboten,'
      '    s.danok,'
      '    s.cena'
      'from kom_presmetka_g g'
      'inner join kom_presmetka_s s on s.presmetka_g_id = g.id'
      
        'inner join kom_uslugi u on u.id = s.usluga and u.vid_usluga = s.' +
        'vid_usluga'
      'where s.presmetka_g_id = :MAS_PID')
    SelectSQL.Strings = (
      'select'
      '    s.presmetka_g_id,'
      '    s.vid_usluga,'
      '    s.usluga,'
      '    u.naziv naziv_usluga,'
      '    s.iznos_usluga_neto,'
      '    s.iznos_usluga,'
      '    s.datum_presmetka,'
      '    s.vraboten,'
      '    s.danok,'
      '    s.cena'
      'from kom_presmetka_g g'
      'inner join kom_presmetka_s s on s.presmetka_g_id = g.id'
      
        'inner join kom_uslugi u on u.id = s.usluga and u.vid_usluga = s.' +
        'vid_usluga'
      'where s.presmetka_g_id = :MAS_PID'
      'order by s.vid_usluga')
    FilterOptions = [foCaseInsensitive]
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Filtered = True
    Left = 488
    Top = 627
    oFetchAll = True
    object tblSiteStavkiVID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object tblSiteStavkiUSLUGA: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object tblSiteStavkiNAZIV_USLUGA: TFIBStringField
      FieldName = 'NAZIV_USLUGA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSiteStavkiIZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object tblSiteStavkiIZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object tblSiteStavkiDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblSiteStavkiVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSiteStavkiDANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object tblSiteStavkiCENA: TFIBBCDField
      FieldName = 'CENA'
      Size = 2
    end
    object tblSiteStavkiPRESMETKA_G_ID: TFIBIntegerField
      FieldName = 'PRESMETKA_G_ID'
    end
  end
  object dsSiteStavki: TDataSource
    DataSet = tblSiteStavki
    Left = 562
    Top = 627
  end
  object tblSiteUplati: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct'
      '    g.id,'
      '    g.godina,'
      '    g.mesec,'
      '    (select b1 from round5(g.iznos_vkupno)) dolzi,'
      '    u.iznos uplati,'
      '    u.datum_uplata uplateno,'
      '    u.tip,'
      '    ktu.naziv naziv_uplata,'
      '    u.re,'
      '    u.tip_nalog,'
      '    u.nalog,'
      '    u.ts_knizenje,'
      '    u.dokument,'
      
        '    (case when u.id_saldo is not null then '#39#1040#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077#39' els' +
        'e '#39#1059#1087#1083#1072#1090#1072#39' end) nacin_plakjanje'
      'from'
      '    kom_presmetka_g g'
      
        '    inner join kom_lokacija_arhiva kla on kla.id_lokacija=g.loka' +
        'ciin_id'
      
        '    left outer join kom_uplati u on u.presmetka_g_id=g.id and u.' +
        'flag = 1'
      '    left outer join kom_tip_uplata ktu on ktu.id=u.tip'
      'where(  --kla.tip_partner=:tp and kla.partner=:p and g.tip=1'
      
        '    --and lastdaymonth((cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as da' +
        'te))) between :d1 and :d2'
      
        '     (((encodedate(1,g.mesec,g.godina) between encodedate(1,kla.' +
        'poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.kr' +
        'aj_godina)) and kla.kraj_mesec is not null)'
      
        '      or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.po' +
        'c_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.p' +
        'oc_mesec is not null) ))'
      '    and u.iznos>0'
      '    and g.id=:MAS_PID'
      '     ) and (     G.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select distinct'
      '    g.id,'
      '    g.godina,'
      '    g.mesec,'
      '    (select b1 from round5(g.iznos_vkupno)) dolzi,'
      '    u.iznos uplati,'
      '    u.datum_uplata uplateno,'
      '    u.tip,'
      '    ktu.naziv naziv_uplata,'
      '    u.re,'
      '    u.tip_nalog,'
      '    u.nalog,'
      '    u.ts_knizenje,'
      '    u.dokument,'
      
        '    (case when u.id_saldo is not null then '#39#1040#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077#39' els' +
        'e '#39#1059#1087#1083#1072#1090#1072#39' end) nacin_plakjanje'
      'from'
      '    kom_presmetka_g g'
      
        '    inner join kom_lokacija_arhiva kla on kla.id_lokacija=g.loka' +
        'ciin_id'
      
        '    left outer join kom_uplati u on u.presmetka_g_id=g.id and u.' +
        'flag = 1'
      '    left outer join kom_tip_uplata ktu on ktu.id=u.tip'
      'where --kla.tip_partner=:tp and kla.partner=:p and g.tip=1'
      
        '    --and lastdaymonth((cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as da' +
        'te))) between :d1 and :d2'
      
        '     (((encodedate(1,g.mesec,g.godina) between encodedate(1,kla.' +
        'poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.kr' +
        'aj_godina)) and kla.kraj_mesec is not null)'
      
        '      or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.po' +
        'c_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.p' +
        'oc_mesec is not null) ))'
      '    and u.iznos>0'
      '    and g.id=:MAS_PID')
    FilterOptions = [foCaseInsensitive]
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Filtered = True
    Left = 336
    Top = 627
    oFetchAll = True
    object tblSiteUplatiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblSiteUplatiGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblSiteUplatiMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblSiteUplatiDOLZI: TFIBBCDField
      FieldName = 'DOLZI'
      Size = 2
    end
    object tblSiteUplatiUPLATI: TFIBBCDField
      FieldName = 'UPLATI'
      Size = 2
    end
    object tblSiteUplatiUPLATENO: TFIBDateField
      FieldName = 'UPLATENO'
    end
    object tblSiteUplatiTIP: TFIBSmallIntField
      FieldName = 'TIP'
    end
    object tblSiteUplatiNAZIV_UPLATA: TFIBStringField
      FieldName = 'NAZIV_UPLATA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSiteUplatiRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblSiteUplatiTIP_NALOG: TFIBStringField
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSiteUplatiNALOG: TFIBIntegerField
      FieldName = 'NALOG'
    end
    object tblSiteUplatiTS_KNIZENJE: TFIBDateTimeField
      FieldName = 'TS_KNIZENJE'
    end
    object tblSiteUplatiDOKUMENT: TFIBStringField
      FieldName = 'DOKUMENT'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSiteUplatiNACIN_PLAKJANJE: TFIBStringField
      FieldName = 'NACIN_PLAKJANJE'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSiteUplati: TDataSource
    DataSet = tblSiteUplati
    Left = 410
    Top = 627
  end
end
