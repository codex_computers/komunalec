unit DULista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ComCtrls,
  ActnList, cxLookAndFeels, cxLookAndFeelPainters, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxBar, dxSkinBlueprint, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinHighContrast, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSpringTime, dxSkinTheAsphaltWorld,
  dxSkinVS2010, dxSkinWhiteprint, dxSkinsdxBarPainter, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, System.Actions;

type
  TfrmDULista = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    StatusBar1: TStatusBar;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BR_RATA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1VK_RATI: TcxGridDBColumn;
    cxGrid1DBTableView1VO_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_USLUGA_NETO: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DANOK: TcxGridDBColumn;
    ActionList1: TActionList;
    aDopUsl: TAction;
    aIzlez: TAction;
    aNov: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxBarManager1: TdxBarManager;
    procedure FormCreate(Sender: TObject);
    procedure aDopUslExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDULista: TfrmDULista;

implementation

uses dmKonekcija, dmMaticni, UnitDM1, UnitDopUsluga;

{$R *.dfm}

procedure TfrmDULista.aNovExecute(Sender: TObject);
begin
   frmDopUsluga:=TfrmDopUsluga.Create(Application);
   dm1.TblDopUslugi.Close;
   DM1.TblDopUslugi.ParamByName('id').Value:=DM1.tblDUListaID.Value;
   dm1.TblDopUslugi.Open;
   DM1.TblRati.Open;
   frmDopUsluga.aNov.Execute;
   frmDopUsluga.ShowModal;
   frmDopUsluga.free;
//   cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmDULista.aDopUslExecute(Sender: TObject);
begin
   frmDopUsluga:=TfrmDopUsluga.Create(Application);
   dm1.TblDopUslugi.Close;
   DM1.TblDopUslugi.ParamByName('id').Value:=DM1.tblDUListaID.Value;
   dm1.TblDopUslugi.Open;
   DM1.TblRati.Open;
   frmDopUsluga.ShowModal;
   frmDopUsluga.free;
//   cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmDULista.aIzlezExecute(Sender: TObject);
begin
 Close;
end;

procedure TfrmDULista.FormCreate(Sender: TObject);
begin
   DM1.tblDULista.Close;
   DM1.tblDULista.Open;
end;

end.
