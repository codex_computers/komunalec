unit UnitIzborTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, ActnList,
  PlatformDefaultStyleActnCtrls, ActnMan, StdCtrls, cxButtons, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  ComCtrls, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  FIBDataSet, pFIBDataSet, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxGridCustomPopupMenu, cxGridPopupMenu,cxGridExportLink, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, cxNavigator, System.Actions;

type
  TfrmIzborTP = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    cbTipPartner: TcxLookupComboBox;
    btPecati: TcxButton;
    ActionManager1: TActionManager;
    aPecati: TAction;
    aDizajner: TAction;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tblSaldo: TpFIBDataSet;
    dsSaldo: TDataSource;
    tblSaldoTIP_PARTNER: TFIBIntegerField;
    tblSaldoNAZIV_TIP_PARTNER: TFIBStringField;
    tblSaldoNAZIV_PARTNER: TFIBStringField;
    tblSaldoDOLZI: TFIBBCDField;
    tblSaldoPOBARUVA: TFIBBCDField;
    tblSaldoSALDO: TFIBBCDField;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1DOLZI: TcxGridDBColumn;
    cxGrid1DBTableView1POBARUVA: TcxGridDBColumn;
    cxGrid1DBTableView1SALDO: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    cxGridPopupMenu1: TcxGridPopupMenu;
    aSnimiExcel: TAction;
    aIzlez: TAction;
    aGrid: TAction;
    tblSaldoPARTNER: TFIBIntegerField;
    procedure aPecatiExecute(Sender: TObject);
    procedure aDizajnerExecute(Sender: TObject);
    procedure cbTipPartnerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aSnimiExcelExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aGridExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzborTP: TfrmIzborTP;

implementation

uses dmMaticni, dmKonekcija, dmReportUnit, Utils;

{$R *.dfm}

procedure TfrmIzborTP.aGridExecute(Sender: TObject);
begin
         tblSaldo.Close;
         if cbTipPartner.text<>'' then
           tblSaldo.ParamByName('tp').AsString:=cbTipPartner.EditValue
         else
           tblSaldo.ParamByName('tp').AsString:='%';
         tblSaldo.Open;
end;

procedure TfrmIzborTP.aIzlezExecute(Sender: TObject);
begin
   Close;
end;

procedure TfrmIzborTP.aDizajnerExecute(Sender: TObject);
begin
   dmReport.Spremi(19,dmKon.aplikacija);
   dmReport.tblSqlReport.Close;
   if cbTipPartner.Text<>'' then
     dmReport.tblSqlReport.ParamByName('tp').AsString:=cbTipPartner.EditValue
   else
     dmReport.tblSqlReport.ParamByName('tp').AsString:='%';
   dmReport.tblSqlReport.Open;
   dmReport.frxReport1.DesignReport();
end;

procedure TfrmIzborTP.aPecatiExecute(Sender: TObject);
begin
   dmReport.Spremi(19,dmKon.aplikacija);
   dmReport.tblSqlReport.Close;
   if cbTipPartner.Text<>'' then
     dmReport.tblSqlReport.ParamByName('tp').AsString:=cbTipPartner.EditValue
   else
     dmReport.tblSqlReport.ParamByName('tp').AsString:='%';
   dmReport.tblSqlReport.Open;
   dmReport.frxReport1.ShowReport();
end;

procedure TfrmIzborTP.aSnimiExcelExecute(Sender: TObject);
begin
    zacuvajVoExcel(cxGrid1, Caption);
//     dmMat.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
//    if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, true);
end;

procedure TfrmIzborTP.cbTipPartnerKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
      VK_RETURN :
      begin
//         tblSaldo.Close;
//         if cbTipPartner.text<>'' then
//           tblSaldo.ParamByName('tp').AsString:=cbTipPartner.EditValue
//         else
//           tblSaldo.ParamByName('tp').AsString:='%';
//         tblSaldo.Open;
        // btPecati.SetFocus;
          aGrid.Execute;
      end;
      VK_ESCAPE : aIzlez.Execute;
    end;
end;

end.
