unit UnitVodomeri;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxMaskEdit, cxDropDownEdit, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBar, dxPSCore, dxPScxCommon, //dxPScxGrid6Lnk,
  cxBarEditItem, dxStatusBar,
  dxRibbonStatusBar, dxRibbon, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSpringTime, dxRibbonSkins,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, idUri,StrUtils, FIBQuery, pFIBQuery, IdBaseComponent,
  IdComponent, IdRawBase, IdRawClient, IdIcmpClient, //dxSkinOffice2013White,
  cxNavigator, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxRibbonCustomizationForm,
  System.Actions, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxCalendar;//, System.Actions;

type
  TFrmVodomeri = class(TfrmMaster)
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    lblStatus: TLabel;
    lblPartnerId: TLabel;
    lblNazivPartner: TLabel;
    lblPartner: TLabel;
    lblSifraLokacija: TLabel;
    lblPodatociKniga: TLabel;
    lblSifraLokacija1: TLabel;
    lblCitackaKniga: TLabel;
    lblCitackaKnigaRb: TLabel;
    lblTipStatus: TLabel;
    cxComboBoxPAUSAL: TcxDBComboBox;
    lblPausal: TLabel;
    cxComboBoxSTATUS: TcxDBComboBox;
    lblTipPausal: TLabel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    Action1: TAction;
    qZemiID: TpFIBQuery;
    IdIcmpClient1: TIdIcmpClient;
    Label2: TLabel;
    Label3: TLabel;
    txtDatum_od: TcxDBDateEdit;
    txtDatum_do: TcxDBDateEdit;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    Label4: TLabel;
    txtBrVod: TcxDBTextEdit;
    cxGrid1DBTableView1VODOMERID: TcxGridDBColumn;
    constructor Create(Owner : TComponent; tipPartner : string; partner : string; nazivPartner : string; sifraLokacija : string; citackaKniga : string; citackaKnigaRb : string; Reon : string);reintroduce; overload;
    procedure FormShow(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure ZemiID;
  private
    { Private declarations }
    var tipPartner,partner,nazivPartner,sifraLokacija,citackaKniga,citackaKnigaRb,reon:string;
  public
    { Public declarations }
  end;

var
  FrmVodomeri: TFrmVodomeri;

implementation

uses UnitDM1, utils, dmKonekcija;

{$R *.dfm}

//------------------------------------------------------------------------------
procedure TFrmVodomeri.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  id_vodomer,akt:string;
  i:integer;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    ZapisiButton.SetFocus;

    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
      begin
        if ((st = dsInsert) and inserting) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          aNov.Execute;
        end;

        if ((st = dsInsert) and (not inserting)) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
        end;

        if (st = dsEdit) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
        end;
      end;
    end;
  end;
  aOtkazi.Execute;


 ///////////////////////////////////////
 //insert vo tabelata KOM_PROMENA
 ///////////////////////////////////////
    if sos_int = '1' then// od 1 vo 11,  duri da sredam , 01.08.2019
    begin
       if st in [dsInsert, dsEdit] then
        begin
//         if servis = '0' then
//          begin
//           ZemiID;
//           id_vodomer := '';
////           for I := 0 to (length(qZemiID.FldByName['ID'].AsString) div 3) do
////            begin
////               if length(Copy( ReverseString(qZemiID.FldByName['ID'].AsString), i*3+1, 3)) = 3 then
////                          id_vodomer := id_vodomer+ Copy( ReverseString(qZemiID.FldByName['ID'].AsString), i*3+1, 3)+ ','
////               else
////                          id_vodomer := id_vodomer+ Copy( ReverseString(qZemiID.FldByName['ID'].AsString), i*3+1, 3)
////            end;
////           id_vodomer := Trim(ReverseString(id_vodomer));
//
//           id_vodomer := qZemiID.FldByName['ID'].AsString;
//         //  if dm1.TblKomLokaciiAKTIVEN.Value = 1 then akt := '��' else akt := '��';
//        //   CelLink:= TIdURI.URLEncode(link+'&SIFRA_NA_KORISNIK%7C0='+sifraLokacija+'&ID%7C0='+sifraLokacija+'|'+id_vodomer);
//           CelLink:= TIdURI.URLEncode(link_insert+'&SIFRA_NA_KORISNIK='+dm1.TblKomLokaciiPARTNER_ID.AsString+'&Vodomer='+id_vodomer+'&ULICA='+dm1.TblKomLokaciiNAZIV_ULICA.Value+'&BROJ='+dm1.TblKomLokaciiBROJ.AsString+'&IME_I_PREZIME='+dm1.TblKomLokaciiNAZIV_PARTNER.Value+'&REON='+dm1.TblKomLokaciiREON_ID.AsString+'&LOKACISKI_BROJ='+dm1.TblKomLokaciiID.AsString+'&CITACKA_KNIGA='+dm1.TblKomLokaciiCITACKA_KNIGA.AsString+'&AKTIVEN='+DM1.TblKomLokaciiAKTIVEN.AsString);
//           dm1.tblPromena.Close;
//           DM1.tblPromena.Open;
//           dm1.tblPromena.Insert;
//           DM1.tblPromenaLINK.Value := CelLink;
//           DM1.tblPromena.Post;
//
//           //�� �� ������� ��� ���� �������� �����
//          // if dm1.IsInternetConnected then
//          try
//      if not dm1.IsInternetConnected then
//         //  IdIcmpClient1.Ping ('www.google.com');
//
//           Except on E:Exception do
//     //      if not ima then
//            begin
//                Abort;
//            end;
//          end;
//           begin
//           //try
//             // da se prati linkot
//                 dm1.ZemiTextFile(cellink);
//                // cxMemo1.Text := vraten_text;
//                 if (copy(vraten_text,1,1) = '1') then
//                // if pos('added',vraten_text) > 0 then
//                 begin
//                   //update vo tabelata KOM_PROMENA
//                   dm1.tblPromena.Edit;
//                   dm1.tblPromenaPROMENA.Value := 1;
//                   dm1.tblPromena.Post;
//                 end;
//           end;
//        end;

           ima_promena := '1';
        end;
    end;
//
// ///////////////////////////////////////
// //update vo tabelata KOM_PROMENA       DA SE PROVERI!!!!!!!!!!!!!!!!1
// ///////////////////////////////////////
//       if st = dsEdit then
//        begin
//           id_vodomer := '';
//           for I := 0 to (length(dm1.TblKomVodomeriID.AsString) div 3) do
//            begin
//               if length(Copy( ReverseString(dm1.TblKomVodomeriID.AsString), i*3+1, 3)) = 3 then
//                          id_vodomer := id_vodomer+ Copy( ReverseString(dm1.TblKomVodomeriID.AsString), i*3+1, 3)+ ','
//               else
//                          id_vodomer := id_vodomer+ Copy( ReverseString(dm1.TblKomVodomeriID.AsString), i*3+1, 3)
//            end;
//           id_vodomer := Trim(ReverseString(id_vodomer));
//           CelLink:= TIdURI.URLEncode(link_edit+'&LOKACISKI_BROJ%7C0='+dm1.TblKomLokaciiID.AsString+'&Vodomer%7C0='+id_vodomer+'&PAUSAL_%7C0=TRUE' //+dm1.TblKomVodomeriPAUSAL.AsString
//           );
//
//           dm1.tblPromena.Close;
//           DM1.tblPromena.Open;
//           dm1.tblPromena.Insert;
//           DM1.tblPromenaLINK.Value := CelLink;
//           DM1.tblPromena.Post;
//
//           //�� �� ������� ��� ���� �������� �����
//          // if dm1.IsInternetConnected then
//          try
//          if not dm1.IsInternetConnected then
//      //     IdIcmpClient1.Ping ('www.google.com');
//           Except on E:Exception do
//     //      if not ima then
//            begin
//                Abort;
//            end;
//          end;
//           begin
//           //try
//             // da se prati linkot
//                 dm1.ZemiTextFile(cellink);
//             //    if (copy(Trim(vraten_text),2,1) = '1') then
//                 if pos('refreshed',vraten_text) > 0 then
//                // if pos('added',vraten_text) > 0 then
//                 begin
//                   //update vo tabelata KOM_PROMENA
//                   dm1.tblPromena.Edit;
//                   dm1.tblPromenaPROMENA.Value := 1;
//                   dm1.tblPromena.Post;
//                 end;
//           end;
//        end;
//    end;


end;

constructor TFrmVodomeri.Create(Owner : TComponent; tipPartner : string; partner : string; nazivPartner : string; sifraLokacija : string; citackaKniga : string; citackaKnigaRb : string; Reon : string);
begin
inherited Create(Owner);
    Self.tipPartner := tipPartner;
    Self.partner := partner;
    Self.nazivPartner := nazivPartner;
    Self.sifraLokacija := sifraLokacija;
    Self.citackaKniga := citackaKniga;
    Self.citackaKnigaRb := citackaKnigaRb;
    self.reon := reon;
end;
procedure TFrmVodomeri.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
    if  (TEdit(sender).Name='SIFRA') and
    (DM1.TblKomVodomeri.State = dsInsert) then
    begin
    //treba da se doprave
     // dm1.TblKomVodomeriID.AsString:=lblSifraLokacija1.Caption
    end;
end;

procedure TFrmVodomeri.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
    if  (TEdit(sender).Name='cxComboBoxPAUSAL') and
    (DM1.TblKomVodomeri.State = dsInsert) then
    begin
        DM1.TblKomVodomeriLOKACIIN_ID.AsString := lblSifraLokacija1.Caption;
        dm1.TblKomVodomeriTIP_PARTNER.AsString:=tipPartner;
        DM1.TblKomVodomeriPARTNER.AsString:=partner;
        dm1.TblKomVodomeriREON_ID.AsString:=reon;
      //  ZapisiButton.SetFocus;
    //    dm1.TblKomVodomeriREON_ID.AsString:=frm
    end;
end;

procedure TFrmVodomeri.FormShow(Sender: TObject);
begin
  inherited;
   // lblTipPartner.Caption := tipPartner;
    lblPartnerId.Caption := partner;
    lblNazivPartner.Caption := nazivPartner;

    lblSifraLokacija1.Caption := sifraLokacija;

    lblCitackaKniga.Caption := citackaKniga;
    lblCitackaKnigaRb.Caption := citackaKnigaRb;
end;

procedure TFrmVodomeri.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
//    case key of
//        VK_RETURN:
//        begin
//            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
//        end;
//        VK_UP:
//        begin
//            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
//            Key := 0;
//        end;
//    end;
end;

procedure TFrmVodomeri.ZemiID;
begin
    qZemiID.close;
    qZemiID.ParamByName('lokacija').Value := sifraLokacija;
    qZemiID.ExecQuery;
end;

end.
