object FrmLokacija: TFrmLokacija
  Left = 0
  Top = 0
  Caption = #1051#1086#1082#1072#1094#1080#1112#1072' '#1087#1086' '#1087#1072#1088#1090#1085#1077#1088
  ClientHeight = 639
  ClientWidth = 1073
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object StatusBar1: TStatusBar
    Left = 0
    Top = 620
    Width = 1073
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 
      'F3 - '#1042#1086#1076#1086#1084#1077#1088#1080', F4 - '#1059#1089#1083#1091#1075#1080', F5 - '#1053#1086#1074#1072' '#1083#1086#1082#1072#1094#1080#1112#1072', F6 -'#1055#1088#1086#1084#1077#1085#1080', Ctr' +
      'l+F6 - '#1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072',  ESC - '#1048#1079#1083#1077#1079
  end
  object panelGore: TPanel
    Left = 0
    Top = 0
    Width = 1073
    Height = 258
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 1
    object panelGorePartner: TPanel
      Left = 2
      Top = 2
      Width = 1069
      Height = 53
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object lblPartner: TLabel
        Left = 16
        Top = 15
        Width = 65
        Height = 18
        Caption = #1055#1072#1088#1090#1085#1077#1088
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxTxtPartnerID: TcxTextEdit
        Left = 315
        Top = 11
        BeepOnEnter = False
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -16
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 97
      end
      object cxTxtPartnerNaziv: TcxTextEdit
        Left = 443
        Top = 11
        Enabled = False
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -16
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 602
      end
      object cxLookUpComboBoxTipPartner: TcxLookupComboBox
        Left = 123
        Top = 11
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmMat.dsTipPartner
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -16
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        OnKeyDown = onKeyDownLookUpAll
        Width = 191
      end
    end
    object panelGoreReon: TPanel
      Left = 2
      Top = 55
      Width = 1069
      Height = 1
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 1
    end
    object panelDole: TPanel
      Left = 2
      Top = 56
      Width = 1069
      Height = 200
      Align = alClient
      BevelInner = bvLowered
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object lblUlica: TLabel
        Left = 432
        Top = 11
        Width = 48
        Height = 18
        Caption = #1059#1083#1080#1094#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblBroj: TLabel
        Left = 772
        Top = 12
        Width = 29
        Height = 18
        Caption = #1041#1088#1086#1112
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblReon: TLabel
        Left = 7
        Top = 11
        Width = 37
        Height = 18
        Caption = #1056#1077#1086#1085
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblBrojNaClenovi: TLabel
        Left = 7
        Top = 75
        Width = 111
        Height = 18
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1083#1077#1085#1086#1074#1080
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblStanbenaPovr: TLabel
        Left = 314
        Top = 70
        Width = 107
        Height = 18
        Caption = #1057#1090#1072#1085#1073#1077#1085#1072' '#1087#1086#1074#1088'.'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblDvornaPovr: TLabel
        Left = 673
        Top = 71
        Width = 92
        Height = 18
        Caption = #1044#1074#1086#1088#1085#1072' '#1087#1086#1074#1088'.'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblCitackaKniga: TLabel
        Left = 7
        Top = 43
        Width = 112
        Height = 18
        Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblCitackaKnigaRb: TLabel
        Left = 263
        Top = 43
        Width = 147
        Height = 18
        Caption = #1056#1077#1076'. '#1073#1088'. '#1074#1086' '#1095#1080#1090'. '#1082#1085'.'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblRedenBroj: TLabel
        Left = 33
        Top = 144
        Width = 84
        Height = 18
        Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblAktiven: TLabel
        Left = 612
        Top = 43
        Width = 56
        Height = 18
        Caption = #1040#1082#1090#1080#1074#1077#1085
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 780
        Top = 44
        Width = 76
        Height = 16
        Caption = '1- '#1044#1072'/ 0 - '#1053#1077
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 7
        Top = 110
        Width = 98
        Height = 18
        Caption = #1052#1077#1088#1085#1072' '#1090#1086#1095#1082#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 432
        Top = 110
        Width = 83
        Height = 18
        Caption = #1048#1085#1082#1072#1089#1072#1090#1086#1088
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxControlBROJ_NA_CLENOVI: TcxDBTextEdit
        Left = 123
        Top = 75
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_NA_CLENOVI'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 95
      end
      object cxControlSTANBENA_POVR: TcxDBTextEdit
        Left = 439
        Top = 67
        BeepOnEnter = False
        DataBinding.DataField = 'STANBENA_POVRSINA'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 9
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 103
      end
      object cxControlREON: TcxDBTextEdit
        Left = 123
        Top = 7
        BeepOnEnter = False
        DataBinding.DataField = 'REON_ID'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 55
      end
      object cxLookUpREON: TcxDBLookupComboBox
        Left = 179
        Top = 7
        BeepOnEnter = False
        DataBinding.DataField = 'REON_ID'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            FieldName = 'ID'
          end
          item
            Caption = #1053#1072#1079#1080#1074
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = DM1.DSKomReoni
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 247
      end
      object cxControlULICA: TcxDBTextEdit
        Left = 487
        Top = 7
        BeepOnEnter = False
        DataBinding.DataField = 'ULICA_ID'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 55
      end
      object cxLookUpULICA: TcxDBLookupComboBox
        Left = 543
        Top = 7
        BeepOnEnter = False
        DataBinding.DataField = 'ULICA_ID'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'naziv'
          end
          item
            FieldName = 'reon_id'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = DM1.DSKomUlici
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 223
      end
      object cxControlBROJ_ULICA: TcxDBTextEdit
        Left = 807
        Top = 8
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Properties.CharCase = ecUpperCase
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 4
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 60
      end
      object cxControlDVORNA_POVR: TcxDBTextEdit
        Left = 780
        Top = 67
        BeepOnEnter = False
        DataBinding.DataField = 'DVORNA_POVRSINA'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 10
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 87
      end
      object cxControlCITACKA_KNIGA: TcxDBTextEdit
        Left = 123
        Top = 38
        BeepOnEnter = False
        DataBinding.DataField = 'CITACKA_KNIGA'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 94
      end
      object cxControlCITACKA_KNIGA_RB: TcxDBTextEdit
        Left = 439
        Top = 38
        BeepOnEnter = False
        DataBinding.DataField = 'CITACKA_KNIGA_RB'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        OnEnter = onEnterAll
        OnExit = cxControlCITACKA_KNIGA_RBExit
        OnKeyDown = onKeyDownAll
        Width = 103
      end
      object cxControlREDEN_BROJ: TcxDBTextEdit
        Left = 123
        Top = 140
        BeepOnEnter = False
        DataBinding.DataField = 'RB2'
        DataBinding.DataSource = DM1.DSKomLokacii
        Enabled = False
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        StyleDisabled.TextColor = clNone
        TabOrder = 15
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 303
      end
      object cxComboBoxAKTIVEN: TcxDBComboBox
        Left = 681
        Top = 39
        BeepOnEnter = False
        DataBinding.DataField = 'AKTIVEN'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          '0'
          '1')
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 7
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 84
      end
      object btnZapisi: TcxButton
        Left = 873
        Top = 164
        Width = 172
        Height = 25
        Caption = #1047#1072#1087#1080#1096#1080
        LookAndFeel.SkinName = 'MoneyTwins'
        TabOrder = 16
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btnZapisiClick
      end
      object cxMemo1: TcxMemo
        Left = 984
        Top = 31
        Lines.Strings = (
          'cxMemo1')
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 17
        Visible = False
        Height = 61
        Width = 240
      end
      object txtMernaTocka: TcxDBTextEdit
        Left = 123
        Top = 107
        BeepOnEnter = False
        DataBinding.DataField = 'ID_MERNA_TOCKA'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 11
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 55
      end
      object cbMernaTocka: TcxDBLookupComboBox
        Left = 179
        Top = 107
        BeepOnEnter = False
        DataBinding.DataField = 'ID_MERNA_TOCKA'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            FieldName = 'ID'
          end
          item
            Caption = #1053#1072#1079#1080#1074
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = DM1.dsMernaTocka
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 12
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 247
      end
      object cbInkasator: TcxDBLookupComboBox
        Left = 681
        Top = 107
        BeepOnEnter = False
        DataBinding.DataField = 'ID_INKASATOR'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Properties.DropDownAutoSize = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID_INKASATOR'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            FieldName = 'ID_INKASATOR'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = DSKomReoni
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 14
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 186
      end
      object txtInkasator: TcxDBTextEdit
        Left = 520
        Top = 107
        BeepOnEnter = False
        DataBinding.DataField = 'ID_INKASATOR'
        DataBinding.DataSource = DM1.DSKomLokacii
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 13
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 161
      end
    end
  end
  object panelGrid: TPanel
    Left = 0
    Top = 289
    Width = 1073
    Height = 331
    Align = alClient
    BevelInner = bvLowered
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 1069
      Height = 327
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.DSKomLokacii
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'ID'
          FooterAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARTNER'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn
          Caption = #1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER_ID'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
          Caption = #1056#1077#1086#1085
          DataBinding.FieldName = 'REON_ID'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1ULICA_ID: TcxGridDBColumn
          Caption = #1059#1083#1080#1094#1072
          DataBinding.FieldName = 'ULICA_ID'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          Caption = #1041#1088#1086#1112
          DataBinding.FieldName = 'BROJ'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1BROJ_NA_CLENOVI: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1083#1077#1085#1086#1074#1080
          DataBinding.FieldName = 'BROJ_NA_CLENOVI'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1STANBENA_POVRSINA: TcxGridDBColumn
          Caption = #1057#1090#1072#1085#1073#1077#1085#1072' '#1087#1086#1074#1088'.'
          DataBinding.FieldName = 'STANBENA_POVRSINA'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 97
        end
        object cxGrid1DBTableView1DVORNA_POVRSINA: TcxGridDBColumn
          Caption = #1044#1074#1086#1088#1085#1072' '#1087#1086#1074#1088'.'
          DataBinding.FieldName = 'DVORNA_POVRSINA'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 95
        end
        object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
          Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075'.'
          DataBinding.FieldName = 'CITACKA_KNIGA'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn
          Caption = #1056#1077#1073'. '#1073#1088' '#1074#1086' '#1095#1080#1090'. '#1082#1085'.'
          DataBinding.FieldName = 'CITACKA_KNIGA_RB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 105
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1077#1085
          DataBinding.FieldName = 'AKTIVEN'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
          DataBinding.FieldName = 'REDEN_BROJ'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxBtnPartneri: TcxButton
    Left = 417
    Top = 8
    Width = 26
    Height = 34
    OptionsImage.Glyph.SourceDPI = 96
    OptionsImage.Glyph.Data = {
      424DCE1200000000000036000000280000002300000022000000010020000000
      000000000000C40E0000C40E00000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7FFF5F5
      F5FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFEFFF4F4
      F4FFDDDEDEFFC3C5C5FFD2D3D3FFFEFDFDFFFFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFB
      FCFFF1F1F1FFE2E2E2FFD6D8D8FFD9DEDDFFE4EAE9FFCCCFCFFFF6F6F6FFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FEFFF8F8F8FFEEF0F0FFE7EAEAFFE7EAEAFFEFF2F2FFF9FAFBFFFFFFFF00FEFF
      FFFFD9DFDEFFEEEEEEFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFE
      FEFFF7F9F9FFF0F3F3FFEBEFEFFFEAEFEEFFEFF3F2FFF7F8F7FFFBFBFBFFFCFC
      FCFFFCFCFCFFFCFCFCFFFDFDFDFFE8ECECFFE9EAEAFFFFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FBFC
      FBFFF4F6F6FFEDF0EFFFE8ECEBFFE8ECECFFEEF0EFFFF2F3F3FFF4F4F4FFF2F3
      F3FFF1F2F3FFEFF1F1FFEFEFF0FFF3F3F4FFF9FAFBFFFCFDFDFFF2F5F5FFE6E9
      E8FFFFFEFFFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFDFDFFF9F9F9FFF4F4
      F4FFECEEEEFFE6E9E8FFE2E6E5FFE5E7E7FFE9EAEAFFEFEEEEFFF1F0F1FFECEC
      EDFFE2E4E5FFD9DADAFFD1CDCBFFC9BFB8FFC6B1A4FFC2AB9BFFC4AC9CFFCFBC
      B0FFECE8E5FFF6F9F9FFE6EBEAFFFCFBFBFFFFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFEFFF9F9F9FFF2F2F2FFEBEBEBFFE3E3E3FFDBDD
      DDFFD8DBDBFFD7DBDAFFD9DDDCFFDEE0E0FFE3E4E4FFE8E8E8FFEBEAEAFFECED
      EDFFE7E9EAFFD8D7D6FFC7BFBAFFBBABA1FFB69E8EFFB69A86FFBB9E89FFC0A5
      91FFC2A994FFC2A994FFB69C89FFC0A592FFE9E6E4FFEBEFEFFFF8F8F8FFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F5F5F5FFC7C7C7FFAEB0
      AFFFB7BABAFFC4C7C6FFCFD2D1FFD8D9D9FFDEDEDEFFE1E1E1FFE3E2E2FFE4E4
      E4FFE5E5E5FFE7E7E8FFDDDDDCFFBFB0A4FFAF9482FFB1937EFFB79B85FFBCA2
      8CFFBFA48EFFBEA38DFFBC9F88FFBB9D85FFBB9D86FFB6A18FFFBDA18DFFD1BF
      B2FFEFF3F3FFF3F5F5FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FAF9F9FFC6C8C7FFC0C2C2FFD3D4D4FFD7D7D7FFD9D9D9FFDAD9D9FFDBDB
      DBFFDDDDDDFFDFDFDFFFE1E1E1FFE3E3E3FFDBDBDAFFB29B8CFFAB8D76FFB69B
      85FFB79C86FFB79982FFB7987FFFBBA08AFFB7987DFFB8997FFFB99C81FFBB9C
      81FFB89B83FFC2A894FFC3A998FFEFF0EFFFEEF1F1FFFFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E4E4E4FFC0C3C2FFD0D1D1FFD2D2
      D2FFD4D4D4FFD6D6D6FFD8D8D8FFD9D9D9FFDBDBDBFFDDDDDDFFDFE0E1FFC1B7
      B0FFA2846EFFB19680FFAE8D74FFB08E74FFB49176FFB69376FFC2B1A2FFBC9F
      88FFBA987DFFBB9A7FFFBB9B80FFBA9C81FFC2A691FFBCA18DFFF0ECEAFFECF0
      EFFFFDFDFDFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F9F8
      F8FFC7C9C9FFC9CACAFFCECECEFFD0D0D0FFD2D2D2FFD4D4D4FFD6D6D6FFD8D8
      D8FFDADADAFFD7D8D9FFAF9B8DFFA68972FFA98B73FFAB8A6FFFB18E72FFB590
      75FFB69073FFC8BBAEFFC9B8ABFFBA9477FFBB997CFFBB9A7DFFBB9C81FFC0A5
      8EFFBEA391FFF0F0EFFFEAEEEDFFF7F9F9FFFFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00DCDDDCFFC2C3C3FFCBCACAFFCCCCCCFFCECE
      CEFFD0D0D0FFD3D3D3FFD3D1D1FFC8C6C6FFB1B4B5FF978678FFAA8A74FFAA8A
      71FFAA876CFFB18C70FFB58E72FFB68F70FFCBBAAEFFDCD9D9FFC0A087FFBB97
      79FFBB987BFFBD9E84FFBA9B85FFC5B0A3FFF2F3F4FFECEDEDFFF0F2F2FFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F1F2F1FFC0C4
      C3FFC5C5C5FFC8C8C8FFCACACAFFCECDCDFFBDBEBEFF8F9B9AFF6D8482FF5C7E
      7BFF6F7B71FFA4816CFFAF8F77FFAB8568FFB0896DFFB48D70FFB58D6EFFC8B5
      A6FFD4D5D7FFBEB4ACFFB2967FFFC0A086FFC0A187FFB09079FFD9D0C9FFF0F1
      F2FFEDEEEDFFE9EDECFFFEFEFFFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FDFEFEFFCCCFCFFFBEBFBFFFC4C4C4FFC7C7C7FFA7ADADFF5C78
      76FF517773FF547F7AFF52837DFF537F79FF87725EFFAB8A72FFAE8B70FFAE85
      69FFB48C70FFB39275FF9C9F9FFF7FA7BFFF73A3C1FF739EB8FF809FB1FF9992
      8CFFBEA594FFEBEBEBFFEBECECFFEDEDEDFFE5E9E8FFF9FAFAFFFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E2E3E3FFB9BABAFFC1C0
      C0FFB6B7B7FF577270FF4C7672FF487874FF41746FFF40756EFF417972FF5676
      6CFF8E725CFFAA8970FFB18D74FFAE8A6EFF839195FF5F98BBFF6EA6CAFF75AC
      CFFF77AED2FF74ACD1FF689FC2FF9FBACCFFEAE9E9FFE9E9E8FFEAEAEAFFE3E7
      E7FFEFF2F2FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F5F6F6FFBABDBCFFBEBCBCFF959C9BFF446763FF42716BFF396C67FF3C70
      6BFF3E746FFF417771FF407B76FF53776DFF80715BFFA27B61FF8A8681FF5994
      B9FF6FA6C9FF68A3C9FF64A3CAFF65A4CCFF6CA7CEFF79AFD1FF68A2C7FFB4C9
      D7FFEBE9E6FFE7E7E7FFE6E7E6FFE6EAE9FFFFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FEFEFEFFC8CACAFFB8B7B7FF86908FFF4067
      62FF396A64FF356964FF396F6AFF3B726DFF3C7571FF3E7974FF3E7C77FF447C
      75FF5E7365FF578096FF689EC3FF66A0C5FF6AA3C6FF71A6C9FF6EA7CAFF67A4
      CBFF68A5CCFF71AACDFF7CABC7FFE1E1E2FFE5E4E4FFE6E5E5FFDFE2E1FFFAFA
      FAFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DEDF
      DFFFB2B2B2FF8F9797FF3E615DFF3A6C67FF316662FF346C67FF37706BFF3874
      6EFF3A7772FF3D7871FF3D7871FF41827DFF4B88A8FF6B9FC0FF6DA1C2FF7BA8
      C6FF83ADC9FF7FACCAFF74A8C9FF65A2C9FF6FA7CBFF6BA1C3FFCFD6DBFFE3E2
      E1FFE3E3E3FFDCDFDFFFEEEFEFFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F2F4F3FFB1B2B2FFA9A9A9FF476460FF3D6A66FF2F67
      62FF2F6A65FF336E68FF35716BFF3B776FFF396867FF396368FF407179FF4B84
      A7FF6E9EBDFF75A3BFFF8AADC4FF93B3C7FF8AB0C8FF7AAAC6FF68A1C6FF6DA4
      C8FF639BBDFFC6D0D6FFE1DFDEFFDFDFDFFFDDDEDEFFDFE2E2FFFEFEFEFFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFEFFBDC0BFFFADAD
      ADFF848E8DFF365955FF3B6C67FF2F6863FF2E6963FF2F6661FF356169FF3A5E
      72FF43667FFF466A84FF457B9DFF6B9AB8FF77A1BDFF8BABC1FF99B2C5FF8DAF
      C5FF79A7C3FF69A0C3FF69A0C2FF689BBAFFD2D5D7FFDCDBDAFFDBDBDBFFDDDD
      DDFFD4D8D7FFF4F4F4FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00D5D6D6FFA2A4A4FFAEACADFF758381FF365754FF396762FF3568
      62FF2D5560FF3B5E77FF416A83FF3F6984FF406985FF3D6F8FFF5288A9FF779F
      BAFF7DA4BCFF83A7BEFF7DA5BFFF71A1BFFF6EA1C1FF5691B6FF92AFC1FFD9D7
      D6FFD6D6D6FFD8D8D8FFDBDBDBFFD2D5D4FFDEDFDFFFFFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00ECEDEDFFA3A5A5FFA5A5A5FFACAB
      ABFF8C9392FF506764FF345459FF32556CFF3C657FFF37627FFF3A6582FF3C66
      83FF3B6683FF3A7091FF5287A8FF709BB5FF759FB9FF729EB9FF699AB8FF518C
      AFFF6E99B4FFCCCDCEFFD2D1D1FFD2D2D2FFD5D5D5FFD7D7D7FFD6D7D6FFC7CA
      C9FFF4F4F5FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFC
      FCFFB1B2B2FF9E9E9EFFA3A3A3FFAAA9A9FFABA9A8FF4F6472FF315870FF3661
      7DFF3C6785FF426E8BFF426E8BFF3F6A87FF396581FF396B8BFF417798FF4B80
      A3FF5286A8FF5A8BA9FF88A3B6FFC4C6C7FFCDCCCBFFCCCCCCFFCFCFCFFFD0D0
      D0FFD3D2D2FFD6D6D6FFBEC2C1FFD7D7D7FFFFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C8CACAFF979898FF9F9F9FFFA2A1A1FFA6A5
      A4FF496070FF315871FF386380FF416D8AFF487491FF487491FF436F8CFF3A67
      83FF3D657FFF3A5F78FF788C9AFFAFB7BCFFBBBDBEFFC7C5C3FFC5C5C5FFC6C6
      C6FFC9C9C9FFCBCBCBFFCCCCCCFFCFCFCFFFD2D2D2FFC5C8C7FFB1B2B2FFF2F2
      F2FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E3E5E5FF9698
      98FF9A9A9AFF9D9D9DFFA2A1A1FF495F6DFF2F556DFF396481FF44708DFF4E7A
      97FF4B7894FF436F8CFF3A6683FF3A637EFF355970FF90999FFFC1BFBEFFBDBC
      BCFFBEBEBEFFC0C0C0FFC2C2C2FFC4C4C4FFC7C7C7FFC9C9C9FFCBCBCBFFCECE
      CEFFCCCECDFFA5A6A6FFD7D7D7FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F8F8F8FFA3A3A3FF959494FF999999FF9F9E9EFF6A767EFF2347
      5EFF39637DFF3D6987FF467390FF45728FFF3D6987FF38637EFF385F78FF3B5A
      6EFFA8AAACFFB7B7B7FFB7B7B7FFB9B9B9FFBCBCBCFFBEBEBEFFBFBFBFFFC2C2
      C2FFC4C4C4FFC5C5C5FFC4C4C5FFC6C8C7FFCBCDCDFFEDEDEDFFFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BCBCBCFF8E8E8EFF9595
      95FF979797FF969697FF3D5463FF294E65FF3B637EFF3C6783FF3B6682FF3863
      7FFF365E78FF294D63FF76828CFFB3B1B1FFB0B0B0FFB3B3B3FFB5B5B5FFB7B7
      B7FFB8B8B8FFB8B9B9FFB9B9B9FFBEC0BFFFCACDCDFFDDDFDEFFEFF0F0FFFCFC
      FCFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D8D8D8FF8C8C8CFF919191FF939393FF989696FF8C8E8FFF3E5563FF2346
      5CFF2A5068FF2D536CFF2A4D65FF294A5FFF66757FFFABA9A9FFABAAAAFFACAB
      ABFFACADADFFACACACFFAEB0B0FFB9BABAFFCACDCCFFE0E2E1FFF2F3F3FFFDFE
      FEFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F1F1F1FF959595FF8B8B8BFF8F8F8FFF9191
      91FF969594FF949594FF747C81FF51636EFF4A5E6BFF63717BFF8A8F92FFA4A2
      A2FFA0A0A0FF9FA0A0FFA5A7A7FFB6B7B7FFCCCDCDFFE5E5E5FFF7F7F7FFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFEFFABAB
      ABFF868686FF8B8B8BFF8D8D8DFF8F8F8FFF929292FF979695FF999997FF9998
      97FF9A9998FF989999FFA0A1A1FFB6B6B6FFD0D0D0FFEAEBEBFFFAFBFBFFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C7C7C7FF828282FF888888FF8A8A8AFF8A8A8AFF8A8A
      8AFF8A8A8AFF8F8F8FFF9F9F9FFFB9B9B9FFD6D7D7FFF0F0F0FFFEFEFEFFFFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7FF888888FF7E7E
      7EFF818181FF8B8B8BFFA0A0A0FFBFBFBFFFE0E0E0FFF6F6F6FFFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FEFEFEFFB1B1B1FFA4A4A4FFC9C9C9FFE9E9E9FFFBFBFBFFFFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00}
    TabOrder = 2
    OnClick = cxBtnPartneriClick
  end
  object ActionList1: TActionList
    Left = 16
    Top = 595
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aNovaLokacija: TAction
      Caption = #1053#1086#1074#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
      ShortCut = 116
      OnExecute = aNovaLokacijaExecute
    end
    object aIzbrisiLokacija: TAction
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1083#1086#1082#1072#1094#1080#1112#1072
      Visible = False
      OnExecute = aIzbrisiLokacijaExecute
    end
    object aPromeni: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080
      ShortCut = 117
      OnExecute = aPromeniExecute
    end
    object aVodomeri: TAction
      Caption = #1055#1088#1080#1082#1072#1078#1080' '#1074#1086#1076#1086#1084#1077#1088#1080
      ShortCut = 114
      OnExecute = aVodomeriExecute
    end
    object aPromeniRedBrojVoKniga: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1088#1077#1076'. '#1073#1088#1086#1112' '#1074#1086' '#1082#1085#1080#1075#1072
      ShortCut = 8309
      OnExecute = aPromeniRedBrojVoKnigaExecute
    end
    object aPromeniCitackaKnigaRb: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072'/'#1088#1077#1076#1077#1085' '#1073#1088#1086#1112' '#1074#1086' '#1095#1080#1090'. '#1082#1085#1080#1075#1072
      ShortCut = 118
      OnExecute = aPromeniCitackaKnigaRbExecute
    end
    object aLokacijaUslugi: TAction
      Caption = 'aLokacijaUslugi'
      ShortCut = 115
      OnExecute = aLokacijaUslugiExecute
    end
    object aPromenaCitKn: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
      ShortCut = 16501
      OnExecute = aPromenaCitKnExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default'
      #1040#1082#1094#1080#1080)
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 48
    Top = 595
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      31
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1052#1077#1085#1080
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 289
      FloatTop = 140
      FloatClientWidth = 51
      FloatClientHeight = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = []
      IsMainMenu = True
      ItemLinks = <
        item
          Visible = True
          ItemName = 'subListAkcii'
        end>
      MultiLine = True
      OneOnRow = True
      Row = 0
      UseOwnFont = True
      Visible = True
      WholeRow = True
    end
    object subListAkcii: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'BarButtonNovaLokacija'
        end
        item
          Visible = True
          ItemName = 'BarButtonPromeni'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzbrisi'
        end
        item
          Visible = True
          ItemName = 'BarButton'
        end
        item
          Visible = True
          ItemName = 'Uslugi'
        end
        item
          Visible = True
          ItemName = 'Promena'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzlez'
        end>
    end
    object BarButtonIzlez: TdxBarButton
      Action = aIzlez
      Category = 1
    end
    object BarButtonNovaLokacija: TdxBarButton
      Action = aNovaLokacija
      Category = 1
    end
    object BarButtonPromeni: TdxBarButton
      Action = aPromeni
      Category = 1
    end
    object BarButtonIzbrisi: TdxBarButton
      Action = aIzbrisiLokacija
      Category = 1
    end
    object BarButton: TdxBarButton
      Action = aVodomeri
      Category = 1
    end
    object BarButtonPromeniRedenBrojVoKniga: TdxBarButton
      Action = aPromeniRedBrojVoKniga
      Category = 1
    end
    object Uslugi: TdxBarButton
      Action = aLokacijaUslugi
      Caption = #1059#1089#1083#1091#1075#1080' '#1087#1086' '#1083#1086#1082#1072#1094#1080#1112#1072
      Category = 1
    end
    object Promena: TdxBarButton
      Action = aPromenaCitKn
      Category = 1
    end
  end
  object qCK: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select max(l.citacka_kniga_rb)+1 citacka_kniga_rb'
      'from kom_lokacii l'
      'where l.reon_id=:r and l.citacka_kniga=:ck')
    Left = 112
    Top = 595
  end
  object IdIcmpClient1: TIdIcmpClient
    Host = 'www.google.com'
    Protocol = 1
    ProtocolIPv6 = 0
    IPVersion = Id_IPv4
    PacketSize = 1024
    Left = 80
    Top = 595
  end
  object tblDopUsl: TpFIBDataSet
    RefreshSQL.Strings = (
      'select count(*) ima'
      'from KOM_DOPOLNITELNA_USLUGA D'
      'where D.TIP_PARTNER = :TIP_PARTNER and'
      '      D.PARTNER = :PARTNER and'
      '      D.VO_PRESMETKA = 0   ')
    SelectSQL.Strings = (
      'select count(*) ima'
      'from KOM_DOPOLNITELNA_USLUGA D'
      'where D.TIP_PARTNER = :TIP_PARTNER and'
      '      D.PARTNER = :PARTNER and'
      '      D.VO_PRESMETKA = 0   ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 866
    Top = 18
    object tblDopUslIMA: TFIBIntegerField
      FieldName = 'IMA'
    end
  end
  object dsDopUsl: TDataSource
    DataSet = tblDopUsl
    Left = 936
    Top = 19
  end
  object TblKomReoni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_REONI'
      'SET '
      '    NAZIV = :NAZIV,'
      '    ID_INKASATOR = :ID_INKASATOR,'
      '    TIP_INKASATOR = :TIP_INKASATOR,'
      '    PASS = :PASS,'
      '    USERNAME = :USERNAME'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KOM_REONI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KOM_REONI('
      '    ID,'
      '    NAZIV,'
      '    ID_INKASATOR,'
      '    TIP_INKASATOR,'
      '    PASS,'
      '    USERNAME'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :ID_INKASATOR,'
      '    :TIP_INKASATOR,'
      '    :PASS,'
      '    :USERNAME'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      
        'r.ID, r.NAZIV, r.ID_INKASATOR, r.TIP_INKASATOR, r.PASS, r.USERNA' +
        'ME'
      'FROM KOM_REONI r'
      ''
      ' WHERE '
      '        R.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      
        'r.ID, r.NAZIV, r.ID_INKASATOR, r.TIP_INKASATOR, r.PASS, r.USERNA' +
        'ME'
      'FROM KOM_REONI r'
      'order by id')
    AutoUpdateOptions.UpdateTableName = 'KOM_REONI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 66
    object TblKomReoniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object TblKomReoniNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblKomReoniTIP_INKASATOR: TFIBIntegerField
      FieldName = 'TIP_INKASATOR'
    end
    object TblKomReoniPASS: TFIBStringField
      FieldName = 'PASS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblKomReoniUSERNAME: TFIBStringField
      FieldName = 'USERNAME'
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblKomReoniID_INKASATOR: TFIBBCDField
      FieldName = 'ID_INKASATOR'
      Size = 0
    end
  end
  object DSKomReoni: TDataSource
    DataSet = TblKomReoni
    Left = 119
    Top = 66
  end
end
