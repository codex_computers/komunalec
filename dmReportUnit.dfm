object dmReport: TdmReport
  OldCreateOrder = False
  Height = 230
  Width = 470
  object tblReportDizajn: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_REPORT'
      'SET '
      '    ID = :ID,'
      '    "SQL" = :"SQL",'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_REPORT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_REPORT('
      '    ID,'
      '    "SQL",'
      '    DATA'
      ')'
      'VALUES('
      '    :ID,'
      '    :"SQL",'
      '    :DATA'
      ')')
    RefreshSQL.Strings = (
      'select id,sql,data '
      'from sys_report '
      'where(  br = :br and tabela_grupa = :app'
      '     ) and (     SYS_REPORT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select id,sql,data '
      'from sys_report '
      'where br = :br and tabela_grupa = :app')
    Transaction = dmKon.tPateka
    Database = dmKon.fibPateka
    AutoCommit = True
    Left = 32
    Top = 144
    object tblReportDizajnSQL: TFIBBlobField
      FieldName = 'SQL'
      Size = 8
    end
    object tblReportDizajnDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object tblSqlReport: TpFIBDataSet
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 136
    Top = 144
  end
  object qp: TfrxDBDataset
    UserName = 'qp'
    OnFirst = qpFirst
    OnNext = qpNext
    CloseDataSource = False
    OnClose = qpClose
    OnOpen = qpOpen
    DataSet = tblSqlReport
    BCDToCurrency = False
    Left = 224
    Top = 144
  end
  object frxReport1: TfrxReport
    Version = '4.15.4'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.CreateDate = 40112.552251597200000000
    ReportOptions.LastChange = 40112.554411469900000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    Left = 384
    Top = 144
  end
  object tblDetail: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    g.lokaciin_id,  '
      '    g.id smetka,'
      '    g.broj_faktura,'
      '    g.mesec,'
      '    g.godina,'
      '    round(g.iznos_vkupno,0) iznos,'
      '    coalesce(sum(ku.iznos),0) plateno ,'
      '    round(g.iznos_vkupno,0)-coalesce(sum(ku.iznos),0) dolzi'
      ''
      'from  kom_presmetka_g g'
      
        '    left join kom_uplati ku on g.id =  ku.presmetka_g_id and ku.' +
        'flag = 1'
      'where g.lokaciin_id = :mas_id'
      '    and g.tip = 1'
      
        '    and  encodedate(01,g.mesec,g.godina) between :mas_od_datum a' +
        'nd :mas_do_datum'
      
        'group BY  g.lokaciin_id, g.id, g.mesec, g.godina, G.iznos_vkupno' +
        ', g.broj_faktura'
      'order by g.godina, g.mesec'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsMaster
    Left = 136
    Top = 88
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    DataSet = tblDetail
    BCDToCurrency = False
    Left = 224
    Top = 88
  end
  object dsMaster: TDataSource
    DataSet = tblSqlReport
    Left = 32
    Top = 96
  end
  object frxRichObject1: TfrxRichObject
    Left = 376
    Top = 80
  end
  object tblDetailTuzeniSmetki: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    t.id tuzbaID,'
      '    g.broj_faktura,'
      '    g.mesec,'
      '    g.godina,'
      '    kts.iznos,'
      
        '    lastdaymonth(cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as date))+15' +
        ' DaSePlatiDo'
      '    '
      'from kom_tuzba t'
      '    inner join kom_tuzba_smetki kts on t.id = kts.tuzba_id'
      '    inner join kom_presmetka_g g on kts.smetka_id = g.id'
      ''
      'where'
      '    t.id = :mas_tuzba'
      'order by g.godina, g.mesec'
      '    ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsMaster
    Left = 136
    Top = 32
  end
  object frxTblDetailTuzeniSmetki: TfrxDBDataset
    UserName = 'frxTblDetailTuzeniSmetki'
    CloseDataSource = False
    DataSet = tblDetailTuzeniSmetki
    BCDToCurrency = False
    Left = 224
    Top = 32
  end
  object frxXMLExport1: TfrxXMLExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    Background = True
    Creator = 'FastReport'
    EmptyLines = True
    SuppressPageHeadersFooters = False
    RowsCount = 0
    Split = ssNotSplit
    Left = 376
    Top = 24
  end
end
