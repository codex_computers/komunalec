object FrmUplati: TFrmUplati
  Left = 0
  Top = 0
  Caption = #1059#1087#1083#1072#1090#1080
  ClientHeight = 664
  ClientWidth = 1113
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 24
    Width = 1113
    Height = 621
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1113
      Height = 621
      ActivePage = TabSheet2
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = #1059#1087#1083#1072#1090#1080' '#1087#1086' '#1073#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072'      '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        DesignSize = (
          1105
          588)
        object lblBrojFaktura: TLabel
          Left = 25
          Top = 18
          Width = 105
          Height = 14
          Caption = #1041#1088'oj '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cxTxtBrojFaktura: TcxTextEdit
          Tag = 1
          Left = 136
          Top = 16
          TabOrder = 0
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          OnKeyPress = cxTxtBrojFakturaKeyPress
          Width = 121
        end
        object Panel2: TPanel
          Left = 0
          Top = 61
          Width = 1103
          Height = 239
          Anchors = [akLeft, akTop, akRight]
          BevelInner = bvLowered
          Color = cl3DLight
          ParentBackground = False
          TabOrder = 1
          object lblMesec: TLabel
            Left = 30
            Top = 91
            Width = 37
            Height = 16
            Caption = #1052#1077#1089#1077#1094
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblGodina: TLabel
            Left = 30
            Top = 114
            Width = 42
            Height = 16
            Caption = #1043#1086#1076#1080#1085#1072
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblPartner: TLabel
            Left = 30
            Top = 67
            Width = 87
            Height = 16
            Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblLokacija: TLabel
            Left = 30
            Top = 20
            Width = 54
            Height = 16
            Caption = #1051#1086#1082#1072#1094#1080#1112#1072
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblIznosPresmetka: TLabel
            Left = 30
            Top = 138
            Width = 100
            Height = 16
            Caption = #1048#1079#1085#1086#1089' '#1055#1088#1077#1089#1084#1077#1090#1082#1072
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblPlateno: TLabel
            Left = 30
            Top = 162
            Width = 49
            Height = 16
            Caption = #1055#1083#1072#1090#1077#1085#1086
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object lblRazlika: TLabel
            Left = 30
            Top = 184
            Width = 47
            Height = 16
            Caption = #1056#1072#1079#1083#1080#1082#1072
            Color = cl3DLight
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object Label1: TLabel
            Left = 30
            Top = 43
            Width = 49
            Height = 16
            Caption = #1055#1072#1088#1090#1085#1077#1088
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cxTxtMesec: TcxTextEdit
            Tag = 1
            Left = 135
            Top = 86
            Properties.ReadOnly = False
            TabOrder = 2
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 122
          end
          object cxTxtGodina: TcxTextEdit
            Tag = 1
            Left = 135
            Top = 111
            Properties.ReadOnly = False
            TabOrder = 3
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 122
          end
          object cxTxtNazivPartner: TcxTextEdit
            Left = 136
            Top = 62
            Properties.ReadOnly = True
            TabOrder = 1
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 281
          end
          object cxTxtLokacija: TcxTextEdit
            Tag = 1
            Left = 136
            Top = 16
            Properties.ReadOnly = False
            TabOrder = 0
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 122
          end
          object cxTxtIznosPresmetka: TcxTextEdit
            Left = 136
            Top = 134
            Enabled = False
            Properties.ReadOnly = True
            StyleDisabled.BorderColor = clWindowText
            StyleDisabled.BorderStyle = ebsUltraFlat
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 4
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 121
          end
          object panelPredhodnoVnesenaUplata: TPanel
            Left = 674
            Top = 2
            Width = 427
            Height = 235
            Align = alRight
            BevelInner = bvLowered
            Enabled = False
            TabOrder = 7
            object lblPoslednoVnesenaUplata: TLabel
              Left = 10
              Top = 0
              Width = 399
              Height = 26
              AutoSize = False
              Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1074#1085#1077#1089#1077#1085#1072' '#1091#1087#1083#1072#1090#1072
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold, fsUnderline]
              ParentFont = False
              Transparent = True
              Layout = tlCenter
            end
            object lblMesecPos: TLabel
              Left = 24
              Top = 89
              Width = 37
              Height = 16
              Caption = #1052#1077#1089#1077#1094
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lblGodinaPos: TLabel
              Left = 24
              Top = 115
              Width = 42
              Height = 16
              Caption = #1043#1086#1076#1080#1085#1072
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lblIznosPos: TLabel
              Left = 24
              Top = 140
              Width = 100
              Height = 16
              Caption = #1048#1079#1085#1086#1089' '#1055#1088#1077#1089#1084#1077#1090#1082#1072
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lblPartnerPos: TLabel
              Left = 23
              Top = 63
              Width = 87
              Height = 16
              Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lblLokacijaPos: TLabel
              Left = 22
              Top = 37
              Width = 54
              Height = 16
              Caption = #1051#1086#1082#1072#1094#1080#1112#1072
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lblIznosPlatenoPos: TLabel
              Left = 24
              Top = 166
              Width = 86
              Height = 16
              Caption = #1048#1079#1085#1086#1089' '#1087#1083#1072#1090#1077#1085#1086
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cxTxtMesecPos: TcxTextEdit
              Left = 136
              Top = 85
              TabOrder = 0
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 63
            end
            object cxTxtGodinaPos: TcxTextEdit
              Left = 136
              Top = 111
              TabOrder = 1
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 63
            end
            object cxTxtIznosPresmetkaPos: TcxTextEdit
              Left = 136
              Top = 136
              TabOrder = 2
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 121
            end
            object cxTxtNazivPartnerPos: TcxTextEdit
              Left = 135
              Top = 58
              TabOrder = 3
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 217
            end
            object cxTxtLokacijaPos: TcxTextEdit
              Left = 135
              Top = 34
              TabOrder = 4
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 63
            end
            object cxTxtIznosPlatenoPos: TcxTextEdit
              Left = 136
              Top = 162
              TabOrder = 5
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 121
            end
          end
          object cxTxtPlateno: TcxTextEdit
            Left = 136
            Top = 158
            Enabled = False
            Properties.ReadOnly = True
            StyleDisabled.BorderColor = clWindowText
            StyleDisabled.BorderStyle = ebsUltraFlat
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 5
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 121
          end
          object cxTxtRazlika: TcxTextEdit
            Left = 136
            Top = 181
            Enabled = False
            Properties.ReadOnly = True
            StyleDisabled.BorderColor = clWindowText
            StyleDisabled.BorderStyle = ebsUltraFlat
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 6
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 121
          end
          object cxLookUpComboBoxTipPartnerLok: TcxLookupComboBox
            Left = 136
            Top = 39
            Properties.DropDownListStyle = lsFixedList
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dmMat.dsTipPartner
            Properties.MaxLength = 0
            TabOrder = 8
            OnEnter = cxLookUpComboBoxTipPartnerLokEnter
            OnKeyDown = onKeyDownLookUpAll
            Width = 169
          end
          object cxTxtPartnerIDLok: TcxTextEdit
            Left = 304
            Top = 39
            BeepOnEnter = False
            TabOrder = 9
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 113
          end
        end
        object panelControls: TPanel
          Left = 0
          Top = 441
          Width = 1105
          Height = 147
          Align = alBottom
          BevelInner = bvLowered
          TabOrder = 2
          object lblIznosPlateno: TLabel
            Left = 12
            Top = 30
            Width = 114
            Height = 18
            Caption = #1048#1079#1085#1086#1089' '#1087#1083#1072#1090#1077#1085#1086
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cxControlIZNOS: TcxDBTextEdit
            Tag = 1
            Left = 132
            Top = 30
            BeepOnEnter = False
            DataBinding.DataField = 'IZNOS'
            DataBinding.DataSource = DM1.DSKomUplati
            Properties.MaxLength = 0
            Properties.OnValidate = cxControlIZNOSPropertiesValidate
            TabOrder = 0
            OnEnter = onEnterAll
            OnExit = onExitAll
            OnKeyDown = onKeyDownAll
            Width = 121
          end
        end
        object rgTipPartner: TcxRadioGroup
          Left = 297
          Top = 0
          Caption = '  '#1048#1079#1073#1077#1088#1077#1090#1077' ...  '
          ParentBackground = False
          ParentColor = False
          Properties.Columns = 2
          Properties.Items = <
            item
              Caption = #1060#1080#1079#1080#1095#1082#1080' '#1083#1080#1094#1072' '#1080' '#1076#1091#1116#1072#1085#1080
            end
            item
              Caption = #1055#1088#1072#1074#1085#1080' '#1083#1080#1094#1072
            end>
          Style.BorderStyle = ebsUltraFlat
          Style.Color = clWindow
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          TabOrder = 3
          Visible = False
          Height = 44
          Width = 400
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1059#1087#1083#1072#1090#1080' '#1087#1086' '#1087#1072#1088#1090#1085#1077#1088'    '
        ImageIndex = 1
        object Label4: TLabel
          Left = 120
          Top = 142
          Width = 34
          Height = 14
          Alignment = taRightJustify
          Caption = #1057#1072#1083#1076#1086
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          Visible = False
        end
        object panelGore: TPanel
          Left = 0
          Top = 0
          Width = 1105
          Height = 49
          Align = alTop
          BevelInner = bvLowered
          BevelOuter = bvNone
          TabOrder = 0
          object panelPartner: TPanel
            Left = 1
            Top = 1
            Width = 1103
            Height = 47
            Align = alClient
            TabOrder = 0
            object Label2: TLabel
              Left = 4
              Top = 11
              Width = 62
              Height = 19
              Caption = #1055#1072#1088#1090#1085#1077#1088
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -16
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cxLookUpComboBoxTipPartner: TcxLookupComboBox
              Left = 79
              Top = 7
              Properties.DropDownListStyle = lsFixedList
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'ID'
                end
                item
                  FieldName = 'NAZIV'
                end>
              Properties.ListSource = dmMat.dsTipPartner
              Properties.MaxLength = 0
              TabOrder = 0
              OnKeyDown = onKeyDownLookUpAll
              Width = 121
            end
            object cxTxtPartnerID: TcxTextEdit
              Left = 206
              Top = 7
              BeepOnEnter = False
              TabOrder = 1
              OnEnter = onEnterAll
              OnExit = onExitAll
              OnKeyDown = onKeyDownAll
              Width = 97
            end
            object cxBtnPartneri: TcxButton
              Left = 303
              Top = 4
              Width = 26
              Height = 27
              OptionsImage.Glyph.SourceDPI = 96
              OptionsImage.Glyph.Data = {
                424DCE1200000000000036000000280000002300000022000000010020000000
                000000000000C40E0000C40E00000000000000000000FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F7FFF5F5
                F5FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFEFFF4F4
                F4FFDDDEDEFFC3C5C5FFD2D3D3FFFEFDFDFFFFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFB
                FCFFF1F1F1FFE2E2E2FFD6D8D8FFD9DEDDFFE4EAE9FFCCCFCFFFF6F6F6FFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
                FEFFF8F8F8FFEEF0F0FFE7EAEAFFE7EAEAFFEFF2F2FFF9FAFBFFFFFFFF00FEFF
                FFFFD9DFDEFFEEEEEEFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFE
                FEFFF7F9F9FFF0F3F3FFEBEFEFFFEAEFEEFFEFF3F2FFF7F8F7FFFBFBFBFFFCFC
                FCFFFCFCFCFFFCFCFCFFFDFDFDFFE8ECECFFE9EAEAFFFFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FBFC
                FBFFF4F6F6FFEDF0EFFFE8ECEBFFE8ECECFFEEF0EFFFF2F3F3FFF4F4F4FFF2F3
                F3FFF1F2F3FFEFF1F1FFEFEFF0FFF3F3F4FFF9FAFBFFFCFDFDFFF2F5F5FFE6E9
                E8FFFFFEFFFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFDFDFFF9F9F9FFF4F4
                F4FFECEEEEFFE6E9E8FFE2E6E5FFE5E7E7FFE9EAEAFFEFEEEEFFF1F0F1FFECEC
                EDFFE2E4E5FFD9DADAFFD1CDCBFFC9BFB8FFC6B1A4FFC2AB9BFFC4AC9CFFCFBC
                B0FFECE8E5FFF6F9F9FFE6EBEAFFFCFBFBFFFFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FEFEFEFFF9F9F9FFF2F2F2FFEBEBEBFFE3E3E3FFDBDD
                DDFFD8DBDBFFD7DBDAFFD9DDDCFFDEE0E0FFE3E4E4FFE8E8E8FFEBEAEAFFECED
                EDFFE7E9EAFFD8D7D6FFC7BFBAFFBBABA1FFB69E8EFFB69A86FFBB9E89FFC0A5
                91FFC2A994FFC2A994FFB69C89FFC0A592FFE9E6E4FFEBEFEFFFF8F8F8FFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F5F5F5FFC7C7C7FFAEB0
                AFFFB7BABAFFC4C7C6FFCFD2D1FFD8D9D9FFDEDEDEFFE1E1E1FFE3E2E2FFE4E4
                E4FFE5E5E5FFE7E7E8FFDDDDDCFFBFB0A4FFAF9482FFB1937EFFB79B85FFBCA2
                8CFFBFA48EFFBEA38DFFBC9F88FFBB9D85FFBB9D86FFB6A18FFFBDA18DFFD1BF
                B2FFEFF3F3FFF3F5F5FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FAF9F9FFC6C8C7FFC0C2C2FFD3D4D4FFD7D7D7FFD9D9D9FFDAD9D9FFDBDB
                DBFFDDDDDDFFDFDFDFFFE1E1E1FFE3E3E3FFDBDBDAFFB29B8CFFAB8D76FFB69B
                85FFB79C86FFB79982FFB7987FFFBBA08AFFB7987DFFB8997FFFB99C81FFBB9C
                81FFB89B83FFC2A894FFC3A998FFEFF0EFFFEEF1F1FFFFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E4E4E4FFC0C3C2FFD0D1D1FFD2D2
                D2FFD4D4D4FFD6D6D6FFD8D8D8FFD9D9D9FFDBDBDBFFDDDDDDFFDFE0E1FFC1B7
                B0FFA2846EFFB19680FFAE8D74FFB08E74FFB49176FFB69376FFC2B1A2FFBC9F
                88FFBA987DFFBB9A7FFFBB9B80FFBA9C81FFC2A691FFBCA18DFFF0ECEAFFECF0
                EFFFFDFDFDFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F9F8
                F8FFC7C9C9FFC9CACAFFCECECEFFD0D0D0FFD2D2D2FFD4D4D4FFD6D6D6FFD8D8
                D8FFDADADAFFD7D8D9FFAF9B8DFFA68972FFA98B73FFAB8A6FFFB18E72FFB590
                75FFB69073FFC8BBAEFFC9B8ABFFBA9477FFBB997CFFBB9A7DFFBB9C81FFC0A5
                8EFFBEA391FFF0F0EFFFEAEEEDFFF7F9F9FFFFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00DCDDDCFFC2C3C3FFCBCACAFFCCCCCCFFCECE
                CEFFD0D0D0FFD3D3D3FFD3D1D1FFC8C6C6FFB1B4B5FF978678FFAA8A74FFAA8A
                71FFAA876CFFB18C70FFB58E72FFB68F70FFCBBAAEFFDCD9D9FFC0A087FFBB97
                79FFBB987BFFBD9E84FFBA9B85FFC5B0A3FFF2F3F4FFECEDEDFFF0F2F2FFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F1F2F1FFC0C4
                C3FFC5C5C5FFC8C8C8FFCACACAFFCECDCDFFBDBEBEFF8F9B9AFF6D8482FF5C7E
                7BFF6F7B71FFA4816CFFAF8F77FFAB8568FFB0896DFFB48D70FFB58D6EFFC8B5
                A6FFD4D5D7FFBEB4ACFFB2967FFFC0A086FFC0A187FFB09079FFD9D0C9FFF0F1
                F2FFEDEEEDFFE9EDECFFFEFEFFFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FDFEFEFFCCCFCFFFBEBFBFFFC4C4C4FFC7C7C7FFA7ADADFF5C78
                76FF517773FF547F7AFF52837DFF537F79FF87725EFFAB8A72FFAE8B70FFAE85
                69FFB48C70FFB39275FF9C9F9FFF7FA7BFFF73A3C1FF739EB8FF809FB1FF9992
                8CFFBEA594FFEBEBEBFFEBECECFFEDEDEDFFE5E9E8FFF9FAFAFFFFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E2E3E3FFB9BABAFFC1C0
                C0FFB6B7B7FF577270FF4C7672FF487874FF41746FFF40756EFF417972FF5676
                6CFF8E725CFFAA8970FFB18D74FFAE8A6EFF839195FF5F98BBFF6EA6CAFF75AC
                CFFF77AED2FF74ACD1FF689FC2FF9FBACCFFEAE9E9FFE9E9E8FFEAEAEAFFE3E7
                E7FFEFF2F2FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00F5F6F6FFBABDBCFFBEBCBCFF959C9BFF446763FF42716BFF396C67FF3C70
                6BFF3E746FFF417771FF407B76FF53776DFF80715BFFA27B61FF8A8681FF5994
                B9FF6FA6C9FF68A3C9FF64A3CAFF65A4CCFF6CA7CEFF79AFD1FF68A2C7FFB4C9
                D7FFEBE9E6FFE7E7E7FFE6E7E6FFE6EAE9FFFFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FEFEFEFFC8CACAFFB8B7B7FF86908FFF4067
                62FF396A64FF356964FF396F6AFF3B726DFF3C7571FF3E7974FF3E7C77FF447C
                75FF5E7365FF578096FF689EC3FF66A0C5FF6AA3C6FF71A6C9FF6EA7CAFF67A4
                CBFF68A5CCFF71AACDFF7CABC7FFE1E1E2FFE5E4E4FFE6E5E5FFDFE2E1FFFAFA
                FAFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DEDF
                DFFFB2B2B2FF8F9797FF3E615DFF3A6C67FF316662FF346C67FF37706BFF3874
                6EFF3A7772FF3D7871FF3D7871FF41827DFF4B88A8FF6B9FC0FF6DA1C2FF7BA8
                C6FF83ADC9FF7FACCAFF74A8C9FF65A2C9FF6FA7CBFF6BA1C3FFCFD6DBFFE3E2
                E1FFE3E3E3FFDCDFDFFFEEEFEFFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00F2F4F3FFB1B2B2FFA9A9A9FF476460FF3D6A66FF2F67
                62FF2F6A65FF336E68FF35716BFF3B776FFF396867FF396368FF407179FF4B84
                A7FF6E9EBDFF75A3BFFF8AADC4FF93B3C7FF8AB0C8FF7AAAC6FF68A1C6FF6DA4
                C8FF639BBDFFC6D0D6FFE1DFDEFFDFDFDFFFDDDEDEFFDFE2E2FFFEFEFEFFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFEFFBDC0BFFFADAD
                ADFF848E8DFF365955FF3B6C67FF2F6863FF2E6963FF2F6661FF356169FF3A5E
                72FF43667FFF466A84FF457B9DFF6B9AB8FF77A1BDFF8BABC1FF99B2C5FF8DAF
                C5FF79A7C3FF69A0C3FF69A0C2FF689BBAFFD2D5D7FFDCDBDAFFDBDBDBFFDDDD
                DDFFD4D8D7FFF4F4F4FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00D5D6D6FFA2A4A4FFAEACADFF758381FF365754FF396762FF3568
                62FF2D5560FF3B5E77FF416A83FF3F6984FF406985FF3D6F8FFF5288A9FF779F
                BAFF7DA4BCFF83A7BEFF7DA5BFFF71A1BFFF6EA1C1FF5691B6FF92AFC1FFD9D7
                D6FFD6D6D6FFD8D8D8FFDBDBDBFFD2D5D4FFDEDFDFFFFFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00ECEDEDFFA3A5A5FFA5A5A5FFACAB
                ABFF8C9392FF506764FF345459FF32556CFF3C657FFF37627FFF3A6582FF3C66
                83FF3B6683FF3A7091FF5287A8FF709BB5FF759FB9FF729EB9FF699AB8FF518C
                AFFF6E99B4FFCCCDCEFFD2D1D1FFD2D2D2FFD5D5D5FFD7D7D7FFD6D7D6FFC7CA
                C9FFF4F4F5FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFC
                FCFFB1B2B2FF9E9E9EFFA3A3A3FFAAA9A9FFABA9A8FF4F6472FF315870FF3661
                7DFF3C6785FF426E8BFF426E8BFF3F6A87FF396581FF396B8BFF417798FF4B80
                A3FF5286A8FF5A8BA9FF88A3B6FFC4C6C7FFCDCCCBFFCCCCCCFFCFCFCFFFD0D0
                D0FFD3D2D2FFD6D6D6FFBEC2C1FFD7D7D7FFFFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00C8CACAFF979898FF9F9F9FFFA2A1A1FFA6A5
                A4FF496070FF315871FF386380FF416D8AFF487491FF487491FF436F8CFF3A67
                83FF3D657FFF3A5F78FF788C9AFFAFB7BCFFBBBDBEFFC7C5C3FFC5C5C5FFC6C6
                C6FFC9C9C9FFCBCBCBFFCCCCCCFFCFCFCFFFD2D2D2FFC5C8C7FFB1B2B2FFF2F2
                F2FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E3E5E5FF9698
                98FF9A9A9AFF9D9D9DFFA2A1A1FF495F6DFF2F556DFF396481FF44708DFF4E7A
                97FF4B7894FF436F8CFF3A6683FF3A637EFF355970FF90999FFFC1BFBEFFBDBC
                BCFFBEBEBEFFC0C0C0FFC2C2C2FFC4C4C4FFC7C7C7FFC9C9C9FFCBCBCBFFCECE
                CEFFCCCECDFFA5A6A6FFD7D7D7FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00F8F8F8FFA3A3A3FF959494FF999999FF9F9E9EFF6A767EFF2347
                5EFF39637DFF3D6987FF467390FF45728FFF3D6987FF38637EFF385F78FF3B5A
                6EFFA8AAACFFB7B7B7FFB7B7B7FFB9B9B9FFBCBCBCFFBEBEBEFFBFBFBFFFC2C2
                C2FFC4C4C4FFC5C5C5FFC4C4C5FFC6C8C7FFCBCDCDFFEDEDEDFFFFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BCBCBCFF8E8E8EFF9595
                95FF979797FF969697FF3D5463FF294E65FF3B637EFF3C6783FF3B6682FF3863
                7FFF365E78FF294D63FF76828CFFB3B1B1FFB0B0B0FFB3B3B3FFB5B5B5FFB7B7
                B7FFB8B8B8FFB8B9B9FFB9B9B9FFBEC0BFFFCACDCDFFDDDFDEFFEFF0F0FFFCFC
                FCFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00D8D8D8FF8C8C8CFF919191FF939393FF989696FF8C8E8FFF3E5563FF2346
                5CFF2A5068FF2D536CFF2A4D65FF294A5FFF66757FFFABA9A9FFABAAAAFFACAB
                ABFFACADADFFACACACFFAEB0B0FFB9BABAFFCACDCCFFE0E2E1FFF2F3F3FFFDFE
                FEFFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00F1F1F1FF959595FF8B8B8BFF8F8F8FFF9191
                91FF969594FF949594FF747C81FF51636EFF4A5E6BFF63717BFF8A8F92FFA4A2
                A2FFA0A0A0FF9FA0A0FFA5A7A7FFB6B7B7FFCCCDCDFFE5E5E5FFF7F7F7FFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFEFFABAB
                ABFF868686FF8B8B8BFF8D8D8DFF8F8F8FFF929292FF979695FF999997FF9998
                97FF9A9998FF989999FFA0A1A1FFB6B6B6FFD0D0D0FFEAEBEBFFFAFBFBFFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00C7C7C7FF828282FF888888FF8A8A8AFF8A8A8AFF8A8A
                8AFF8A8A8AFF8F8F8FFF9F9F9FFFB9B9B9FFD6D7D7FFF0F0F0FFFEFEFEFFFFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7FF888888FF7E7E
                7EFF818181FF8B8B8BFFA0A0A0FFBFBFBFFFE0E0E0FFF6F6F6FFFFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FEFEFEFFB1B1B1FFA4A4A4FFC9C9C9FFE9E9E9FFFBFBFBFFFFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
                FF00FFFFFF00FFFFFF00FFFFFF00}
              TabOrder = 2
              OnClick = cxBtnPartneriClick
            end
            object cxTxtPartnerNaziv: TcxTextEdit
              Left = 329
              Top = 7
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 520
            end
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 208
          Width = 1105
          Height = 380
          Align = alBottom
          BevelInner = bvLowered
          TabOrder = 8
          object cxGrid2: TcxGrid
            Left = 2
            Top = 2
            Width = 1101
            Height = 376
            Align = alClient
            TabOrder = 0
            object cxGrid2DBTableView1: TcxGridDBTableView
              OnKeyDown = cxGrid2DBTableView1KeyDown
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = DM1.DSNeplateniZadolzuvanjePoLokacija
              DataController.Filter.Active = True
              DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'RAZLIKA'
                  Column = cxGrid2DBTableView1RAZLIKA
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'PLATENO'
                  Column = cxGrid2DBTableView1PLATENO
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'IZNOS_VKUPNO'
                  Column = cxGrid2DBTableView1IZNOS_VKUPNO
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.MultiSelect = True
              OptionsView.ColumnAutoWidth = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              object cxGrid2DBTableView1BROJ_FAKTURA: TcxGridDBColumn
                Caption = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
                DataBinding.FieldName = 'BROJ_FAKTURA'
                Options.Editing = False
                Width = 151
              end
              object cxGrid2DBTableView1DATUM_PRESMETKA: TcxGridDBColumn
                Caption = #1044#1072#1090#1091#1084' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
                DataBinding.FieldName = 'DATUM_PRESMETKA'
                Options.Editing = False
                Width = 131
              end
              object cxGrid2DBTableView1GODINA: TcxGridDBColumn
                Caption = #1043#1086#1076#1080#1085#1072
                DataBinding.FieldName = 'GODINA'
                Options.Editing = False
                Width = 62
              end
              object cxGrid2DBTableView1MESEC: TcxGridDBColumn
                Caption = #1052#1077#1089#1077#1094
                DataBinding.FieldName = 'MESEC'
                Options.Editing = False
                Width = 53
              end
              object cxGrid2DBTableView1IZNOS_VKUPNO: TcxGridDBColumn
                Caption = #1048#1079#1085#1086#1089' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
                DataBinding.FieldName = 'IZNOS_VKUPNO'
                Options.Editing = False
                Width = 115
              end
              object cxGrid2DBTableView1PLATENO: TcxGridDBColumn
                Caption = #1055#1083#1072#1090#1077#1085#1086
                DataBinding.FieldName = 'PLATENO'
                Options.Editing = False
                Width = 95
              end
              object cxGrid2DBTableView1RAZLIKA: TcxGridDBColumn
                Caption = #1056#1072#1079#1083#1080#1082#1072
                DataBinding.FieldName = 'RAZLIKA'
                Options.Editing = False
                Width = 92
              end
              object cxGrid2DBTableViewIzberi: TcxGridDBColumn
                Caption = #1048#1079#1073#1077#1088#1080
                DataBinding.ValueType = 'Boolean'
                PropertiesClassName = 'TcxCheckBoxProperties'
                Properties.ImmediatePost = True
                Properties.NullStyle = nssUnchecked
                Properties.OnChange = cxGrid2DBTableViewIzberiPropertiesChange
                Width = 68
              end
              object cxGrid2DBTableView1LOKACIJA: TcxGridDBColumn
                DataBinding.FieldName = 'LOKACIJA'
                Visible = False
              end
              object cxGrid2DBTableView1ID: TcxGridDBColumn
                DataBinding.FieldName = 'ID'
                Visible = False
              end
              object cxGrid2DBTableView1KONTO: TcxGridDBColumn
                DataBinding.FieldName = 'KONTO'
                Visible = False
              end
              object cxGrid2DBTableView1O_REF_ID: TcxGridDBColumn
                DataBinding.FieldName = 'O_REF_ID'
                Visible = False
              end
              object cxGrid2DBTableView1O_REF_ID2: TcxGridDBColumn
                DataBinding.FieldName = 'O_REF_ID2'
                Visible = False
              end
              object cxGrid2DBTableView1TUZBA: TcxGridDBColumn
                Caption = #1041#1088'.'#1090#1091#1078#1073#1072
                DataBinding.FieldName = 'TUZBA'
                Options.Editing = False
                Styles.Content = DM1.cxStyle1
                Width = 129
              end
              object cxGrid2DBTableView1DOGOVOR: TcxGridDBColumn
                Caption = #1041#1088'.'#1076#1086#1075#1086#1074#1086#1088
                DataBinding.FieldName = 'DOGOVOR'
                Options.Editing = False
                Styles.Content = DM1.cxStyle1
                Width = 83
              end
            end
            object cxGrid2Level1: TcxGridLevel
              GridView = cxGrid2DBTableView1
            end
          end
        end
        object cxTxtVkOdbereni: TcxTextEdit
          Left = 163
          Top = 112
          TabStop = False
          Enabled = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          StyleDisabled.BorderColor = clWindowText
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 132
        end
        object cxTxtVkZadolzuvanje: TcxTextEdit
          Left = 527
          Top = 56
          TabStop = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          TabOrder = 4
          Width = 132
        end
        object cxDateDatumUplata: TcxDateEdit
          Left = 163
          Top = 56
          Enabled = False
          Properties.ReadOnly = True
          Properties.ShowTime = False
          TabOrder = 1
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 132
        end
        object cxTxtZaVracanje: TcxTextEdit
          Left = 527
          Top = 112
          TabStop = False
          Enabled = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          TabOrder = 6
          Width = 132
        end
        object cxMaskPlateno: TcxTextEdit
          Left = 527
          Top = 84
          TabStop = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = False
          TabOrder = 3
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 132
        end
        object btZapisi: TcxButton
          Left = 691
          Top = 133
          Width = 124
          Height = 33
          Action = aZapisi
          LookAndFeel.NativeStyle = True
          TabOrder = 5
        end
        object cbSite: TcxCheckBox
          Left = 821
          Top = 178
          Caption = #1054#1079#1085#1072#1095#1080' '#1089#1080#1090#1077
          ParentFont = False
          Properties.ImmediatePost = True
          Properties.OnChange = cxCheckBox1Properties
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 9
          Transparent = True
          Width = 97
        end
        object txtDok: TcxTextEdit
          Left = 163
          Top = 84
          TabOrder = 2
          OnExit = cxDateDatumUplataExit
          OnKeyDown = onKeyDownAll
          Width = 132
        end
        object txtSaldo: TcxTextEdit
          Left = 163
          Top = 140
          TabStop = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          TabOrder = 10
          Visible = False
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 121
        end
        object lblDatumUplata: TcxLabel
          Left = 11
          Top = 57
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1091#1087#1083#1072#1090#1072
          Style.TextColor = clRed
          Transparent = True
          Height = 25
          Width = 132
        end
        object lblVkupnoPlateno: TcxLabel
          Left = 377
          Top = 86
          AutoSize = False
          Caption = #1042#1082#1091#1087#1085#1086' '#1087#1083#1072#1090#1077#1085#1086
          Style.TextColor = clRed
          Transparent = True
          Height = 25
          Width = 130
        end
        object cxLabel2: TcxLabel
          Left = 50
          Top = 85
          AutoSize = False
          Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          Style.TextColor = clRed
          Transparent = True
          Height = 25
          Width = 93
        end
        object lblVkupno: TcxLabel
          Left = 361
          Top = 58
          AutoSize = False
          Caption = #1042#1082#1091#1087#1085#1086' '#1079#1072' '#1087#1083#1072#1116#1072#1114#1077
          Style.TextColor = clRed
          Transparent = True
          Height = 25
          Width = 145
        end
        object lblZaVracanjePoslednaUplata: TcxLabel
          Left = 378
          Top = 113
          AutoSize = False
          Caption = #1056#1072#1079#1083#1080#1082#1072
          Style.TextColor = clRed
          Transparent = True
          Height = 25
          Width = 129
        end
        object cxLabel1: TcxLabel
          Left = 10
          Top = 113
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1073#1077#1083#1077#1096#1082#1080
          Transparent = True
          Height = 25
          Width = 132
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1041#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1091#1087#1083#1072#1090#1080
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGrid3: TcxGrid
          Left = 0
          Top = 0
          Width = 1105
          Height = 569
          Align = alClient
          TabOrder = 0
          object cxGrid3DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DM1.dsBrisiUplati
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.MultiSelect = True
            object cxGrid3DBTableView1SIFRA_PART: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              Visible = False
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid3DBTableView1NAZIV: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074
              Visible = False
              HeaderAlignmentHorz = taCenter
              Width = 209
            end
            object cxGrid3DBTableView1BROJ_FAKTURA: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
              DataBinding.FieldName = 'BROJ_FAKTURA'
              HeaderAlignmentHorz = taCenter
              Width = 111
            end
            object cxGrid3DBTableView1IZNOS: TcxGridDBColumn
              Caption = #1048#1079#1085#1086#1089
              DataBinding.FieldName = 'IZNOS'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid3DBTableView1LOKACIJA: TcxGridDBColumn
              Caption = #1051#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'LOKACIJA'
              HeaderAlignmentHorz = taCenter
              Width = 71
            end
            object cxGrid3DBTableView1ID_TU: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1090#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
              DataBinding.FieldName = 'ID_TU'
              Visible = False
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid3DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1091#1087#1083#1072#1090#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid3DBTableView1TIP_PARTNER: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'TIP_PARTNER'
              Width = 38
            end
            object cxGrid3DBTableView1PARTNER_ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'PARTNER_ID'
              Width = 60
            end
            object cxGrid3DBTableView1NAZIV_PARTNER: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV_PARTNER'
              Width = 212
            end
            object cxGrid3DBTableView1DATUM_UPLATA: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1091#1087#1083#1072#1090#1072
              DataBinding.FieldName = 'DATUM_UPLATA'
              HeaderAlignmentHorz = taCenter
              Width = 129
            end
            object cxGrid3DBTableView1TIP_UPLATA: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
              DataBinding.FieldName = 'TIP_UPLATA'
              HeaderAlignmentHorz = taCenter
              Width = 129
            end
            object cxGrid3DBTableView1DOKUMENT: TcxGridDBColumn
              Caption = #1044#1086#1082#1091#1084#1077#1085#1090
              DataBinding.FieldName = 'DOKUMENT'
              Visible = False
              Width = 71
            end
            object cxGrid3DBTableView1TIP_NALOG: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1083#1086#1075
              DataBinding.FieldName = 'TIP_NALOG'
              Width = 75
            end
            object cxGrid3DBTableView1NALOG: TcxGridDBColumn
              Caption = #1053#1072#1083#1086#1075
              DataBinding.FieldName = 'NALOG'
              Width = 65
            end
            object cxGrid3DBTableView1ID_FNG: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1072#1083#1086#1075
              DataBinding.FieldName = 'ID_FNG'
              Visible = False
              Width = 165
            end
            object cxGrid3DBTableView1O_REF_ID: TcxGridDBColumn
              DataBinding.FieldName = 'O_REF_ID'
              Visible = False
            end
            object cxGrid3DBTableView1O_REF_ID2: TcxGridDBColumn
              DataBinding.FieldName = 'O_REF_ID2'
              Visible = False
            end
            object cxGrid3DBTableView1ID_TEMP: TcxGridDBColumn
              DataBinding.FieldName = 'ID_TEMP'
              Visible = False
            end
          end
          object cxGrid3Level1: TcxGridLevel
            GridView = cxGrid3DBTableView1
          end
        end
        object StatusBar2: TStatusBar
          Left = 0
          Top = 569
          Width = 1105
          Height = 19
          Panels = <>
          SimplePanel = True
          SimpleText = 'F8 - '#1041#1088#1080#1096#1080' '#1091#1087#1083#1072#1090#1072', F11 - '#1056#1072#1089#1082#1085#1080#1078#1080' '#1085#1072#1083#1086#1075
        end
      end
      object ptBrisiAvansi: TTabSheet
        Caption = #1041#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1072#1074#1072#1085#1089#1080
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGrid4: TcxGrid
          Left = 0
          Top = 0
          Width = 1105
          Height = 569
          Align = alClient
          TabOrder = 0
          object cxGrid4DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DM1.dsBrisiSaldo
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGrid4DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid4DBTableView1TIP_PARTNER: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'TIP_PARTNER'
              Width = 29
            end
            object cxGrid4DBTableView1PARTNER: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'PARTNER'
              Width = 68
            end
            object cxGrid4DBTableView1NAZIV_PARTNER: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV_PARTNER'
              Width = 323
            end
            object cxGrid4DBTableView1IZNOS: TcxGridDBColumn
              Caption = #1048#1079#1085#1086#1089
              DataBinding.FieldName = 'IZNOS'
              Width = 73
            end
            object cxGrid4DBTableView1TIP: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1087#1083#1072#1090#1072
              DataBinding.FieldName = 'TIP'
              Visible = False
              Width = 81
            end
            object cxGrid4DBTableView1NAZIV_TIP_UPLATA: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
              DataBinding.FieldName = 'NAZIV_TIP_UPLATA'
              Width = 134
            end
            object cxGrid4DBTableView1DOKUMENT: TcxGridDBColumn
              Caption = #1044#1086#1082#1091#1084#1077#1085#1090
              DataBinding.FieldName = 'DOKUMENT'
              Width = 73
            end
            object cxGrid4DBTableView1DATUM_UPLATA: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_UPLATA'
              Width = 72
            end
            object cxGrid4DBTableView1TIP_NALOG: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1083#1086#1075
              DataBinding.FieldName = 'TIP_NALOG'
              Width = 52
            end
            object cxGrid4DBTableView1NALOG: TcxGridDBColumn
              Caption = #1053#1072#1083#1086#1075
              DataBinding.FieldName = 'NALOG'
            end
            object cxGrid4DBTableView1ID_SALDO: TcxGridDBColumn
              DataBinding.FieldName = 'ID_SALDO'
              Visible = False
            end
            object cxGrid4DBTableView1RE_FNG: TcxGridDBColumn
              Caption = #1056#1045' '#1085#1086#1074#1086
              DataBinding.FieldName = 'RE_FNG'
              Width = 70
            end
            object cxGrid4DBTableView1GODINA_FNG: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA_FNG'
              Width = 70
            end
            object cxGrid4DBTableView1TIP_NALOG_FNG: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1083#1086#1075' '#1085#1086#1074#1086
              DataBinding.FieldName = 'TIP_NALOG_FNG'
              Width = 70
            end
            object cxGrid4DBTableView1NALOG_FNG: TcxGridDBColumn
              Caption = #1053#1072#1083#1086#1075' '#1085#1086#1074#1086
              DataBinding.FieldName = 'NALOG_FNG'
              Width = 70
            end
          end
          object cxGrid4Level1: TcxGridLevel
            GridView = cxGrid4DBTableView1
          end
        end
        object StatusBar3: TStatusBar
          Left = 0
          Top = 569
          Width = 1105
          Height = 19
          Panels = <
            item
              Text = 'Ctrl+F8 - '#1041#1088#1080#1096#1080' '#1072#1074#1072#1085#1089
              Width = 50
            end>
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 645
    Width = 1113
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'Enter - '#1055#1083#1072#1116#1072#1114#1077',  ESC - '#1048#1079#1083#1077#1079
  end
  object dxBarManager1: TdxBarManager
    AutoDockColor = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      #1040#1082#1094#1080#1080)
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    DockColor = clActiveBorder
    LookAndFeel.NativeStyle = True
    MenuAnimations = maRandom
    PopupMenuLinks = <>
    Style = bmsStandard
    UseSystemFont = False
    Left = 16
    Top = 556
    PixelsPerInch = 96
    DockControlHeights = (
      0
      0
      24
      0)
    object dxBarManager1Bar1: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = #1052#1077#1085#1080
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 365
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      IsMainMenu = True
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 272
          Visible = True
          ItemName = 'dxBarLookupCombo1'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 117
          Visible = True
          ItemName = 'cxControlDATUM_UPLATA'
        end
        item
          BeginGroup = True
          UserDefine = [udWidth]
          UserWidth = 172
          Visible = True
          ItemName = 'dxBarLookupComboNalog'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 103
          Visible = True
          ItemName = 'txtGodina'
        end>
      MultiLine = True
      OneOnRow = True
      Row = 0
      UseOwnFont = True
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarLookupComboNalog: TdxBarLookupCombo
      Caption = '    '#1053#1072#1083#1086#1075'  '
      Category = 0
      Hint = '    '#1053#1072#1083#1086#1075'  '
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FF00FF00FF00FF00FF00FF00FF00}
      ShowCaption = True
      KeyField = 'ID'
      ListField = 'ID;opis_nalog;datum'
      ListFieldIndex = 1
      ListSource = dsNalog
      RowCount = 7
    end
    object dxBarLookupCombo2: TdxBarLookupCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FF00FF00FF00FF00FF00FF00FF00}
      RowCount = 7
    end
    object txtGodina: TcxBarEditItem
      Caption = '    '#1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = '    '#1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      ShowCaption = True
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030')
      InternalEditValue = 2020
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 1
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarCombo1: TdxBarCombo
      Caption = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
      Category = 1
      Hint = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072
      MergeKind = mkNone
      Visible = ivAlways
      ShowCaption = True
      Text = '0 - '#1054#1076' '#1080#1085#1082#1072#1089#1072#1090#1086#1088
      ShowEditor = False
      DropDownCount = 3
      Items.Strings = (
        '0 - '#1054#1076' '#1080#1085#1082#1072#1089#1072#1090#1086#1088
        '1 - '#1054#1076' '#1073#1072#1085#1082#1086#1074' '#1080#1079#1074#1086#1076
        '2 - '#1042#1086' '#1075#1086#1090#1086#1074#1086)
      ItemIndex = 0
    end
    object dxBarImageCombo1: TdxBarImageCombo
      Category = 1
      MergeKind = mkNone
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000C0C0C000C0C0C000C0C0
        C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
        C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFC0C0C000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFFFF00
        FFFFFFFF00FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFF00FF
        FFFF808000FFFF00FFFFFFFFFFFF000000FF000000FFFFFFFFFF000000FF0000
        00FFFFFFFFFF000000FFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFF0000
        FFFF00FF00FFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFF0000FF0000
        FFFFFF00FFFF808000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFC0C0C000000000FFFF0000FF8000
        00FF808000FF00FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        00FFFFFFFFFFFFFFFFFFFF0000FF000000FFC0C0C000000000FFFF0000FF00FF
        FFFF00FF00FFFFFF00FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFC0C0C000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFF8080
        00FF800000FF00FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFF8000
        00FF0000FFFF808000FFFFFFFFFF000000FF000000FFFFFFFFFF000000FF0000
        00FF000000FF000000FFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFF0000
        FFFF800000FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFC0C0C000000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFC0C0C000}
      Items.Strings = (
        '0 - '
        '1 -')
      ItemIndex = -1
      ImageIndexes = (
        0
        1)
    end
    object dxBarLookupCombo1: TdxBarLookupCombo
      Caption = '   '#1058#1080#1087' '#1085#1072' '#1059#1087#1083#1072#1090#1072'  '
      Category = 1
      Hint = '   '#1058#1080#1087' '#1085#1072' '#1059#1087#1083#1072#1090#1072'  '
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FF00FF00FF00FF00FF00FF00FF00}
      ShowCaption = True
      KeyField = 'ID'
      ListField = 'NAZIV'
      ListSource = DM1.DSKomTipUplata
      RowCount = 7
    end
    object cxControlDATUM_UPLATA: TdxBarDateCombo
      Align = iaRight
      Caption = '   '#1044#1072#1090#1091#1084' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      Category = 1
      Hint = '                    '#1044#1072#1090#1091#1084' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      Visible = ivAlways
      OnChange = cxControlDATUM_UPLATAChange
      OnExit = cxControlDATUM_UPLATAExit
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FFFFFFFFFF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF0000
        00FF000000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000080FF000080FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000080FF000080FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000080FF0000
        80FFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        80FF000080FFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFF000080FF000080FF000080FF0000
        80FF000080FFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF800000FF800000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFF000000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF800000FF800000FF000000FFFF00FF00000000FF800000FF8000
        00FF800000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FF800000FF8000
        00FF800000FF800000FF800000FF800000FF800000FF800000FF800000FF0000
        00FFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ShowCaption = True
      ShowDayText = False
    end
    object dxBarLookupCombo3: TdxBarLookupCombo
      Align = iaRight
      Caption = '     '#1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      Category = 1
      Hint = '     '#1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FF00FF00FF00FF00FF00FF00FF00}
      ShowCaption = True
      ImmediateDropDown = True
      KeyField = 'ID'
      ListField = 'id;NAZIV'
      ListSource = dmMat.dsTipPartner
      RowCount = 7
    end
  end
  object ActionList1: TActionList
    Left = 16
    Top = 492
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aBrisi: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aNeplateni: TAction
      Caption = 'aNeplateni'
      OnExecute = aNeplateniExecute
    end
    object aZapisi: TAction
      Caption = #1055#1083#1072#1116#1072#1114#1077
      ImageIndex = 29
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOznaciSite: TAction
      Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077
      OnExecute = aOznaciSiteExecute
    end
    object aBrisiSaldo: TAction
      Caption = 'aBrisiSaldo'
      ShortCut = 16503
      OnExecute = aBrisiSaldoExecute
    end
    object aRaskniziUplata: TAction
      Caption = 'aRaskniziUplata'
      ShortCut = 122
      OnExecute = aRaskniziUplataExecute
    end
  end
  object qPresmetka: TpFIBDataSet
    RefreshSQL.Strings = (
      'select g.id, g.lokaciin_id, g.mesec, g.godina, g.broj_faktura'
      'from kom_presmetka_g g'
      
        '--inner join kom_lokacija_arhiva kla on kla.id_lokacija=g.lokaci' +
        'in_id'
      '--inner join kom_lokacii l on l.id=kla.id_lokacija'
      
        'where(  g.broj_faktura=:bf --and ((l.tip_partner in (5,6) and :i' +
        '=0) or (l.tip_partner = 1 and :i=1))'
      'and g.tip=1 and g.lokaciin_id like :l'
      
        '--and (((encodedate(1,g.mesec,g.godina) between encodedate(1,kla' +
        '.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.k' +
        'raj_godina)) and kla.kraj_mesec is not null)'
      
        '-- or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.poc_m' +
        'esec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc_' +
        'mesec is not null) ))'
      '     ) and (     G.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select g.id, g.lokaciin_id, g.mesec, g.godina, g.broj_faktura'
      'from kom_presmetka_g g'
      
        '--inner join kom_lokacija_arhiva kla on kla.id_lokacija=g.lokaci' +
        'in_id'
      '--inner join kom_lokacii l on l.id=kla.id_lokacija'
      
        'where g.broj_faktura=:bf --and ((l.tip_partner in (5,6) and :i=0' +
        ') or (l.tip_partner = 1 and :i=1))'
      'and g.tip=1 and g.lokaciin_id like :l'
      
        '--and (((encodedate(1,g.mesec,g.godina) between encodedate(1,kla' +
        '.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.k' +
        'raj_godina)) and kla.kraj_mesec is not null)'
      
        '-- or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.poc_m' +
        'esec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc_' +
        'mesec is not null) ))')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 556
    object qPresmetkaLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object qPresmetkaMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object qPresmetkaGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object qPresmetkaBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object qPresmetkaID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object dsPresmetka: TDataSource
    Left = 144
    Top = 524
  end
  object qBrojFaktura: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select broj_faktura'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l and g.mesec=:m and g.godina=:g')
    Left = 144
    Top = 588
  end
  object tblPoslednoZadolzuvanje: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT first(1)'
      '    G.BROJ_FAKTURA,'
      '    G.DATUM_PRESMETKA,'
      '    G.GODINA,'
      '    G.MESEC,'
      '    cast(g.IZNOS_VKUPNO as integer) iznos_vkupno,'
      '    p.TIP_PARTNER,'
      '    p.ID partner,'
      '    p.naziv,'
      '    SUM(COALESCE(KU.IZNOS,0)) PLATENO,'
      
        '    SUM(COALESCE(KU.IZNOS,0))-cast(g.IZNOS_VKUPNO as integer) RA' +
        'ZLIKA'
      'FROM KOM_PRESMETKA_G G'
      
        'LEFT JOIN KOM_UPLATI KU ON G.id=ku.presmetka_g_id and ku.flag = ' +
        '1'
      ' inner join KOM_LOKACII KL on G.LOKACIIN_ID=KL.ID'
      ' inner join kom_lokacija_arhiva kla on kla.id_lokacija=kl.id'
      
        ' inner join mat_partner p on p.id=kla.partner and p.tip_partner=' +
        'kla.tip_partner'
      'WHERE'
      '    KLa.id_lokacija=:LOKACIJA'
      '    and g.tip=1'
      
        '   and (((encodedate(1,g.mesec,g.godina) between encodedate(1,kl' +
        'a.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.' +
        'kraj_godina)) and kla.kraj_mesec is not null)'
      
        '    or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.poc_' +
        'mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc' +
        '_mesec is not null) ))'
      
        'GROUP BY G.BROJ_FAKTURA,G.DATUM_PRESMETKA,G.GODINA,G.MESEC,G.IZN' +
        'OS_VKUPNO,p.TIP_PARTNER, p.ID, p.naziv'
      'ORDER BY G.GODINA desc, G.MESEC desc')
    SelectSQL.Strings = (
      'SELECT first(1)'
      '    G.BROJ_FAKTURA,'
      '    G.DATUM_PRESMETKA,'
      '    G.GODINA,'
      '    G.MESEC,'
      '    cast(g.IZNOS_VKUPNO as integer) iznos_vkupno,'
      '    p.TIP_PARTNER,'
      '    p.ID partner,'
      '    p.naziv,'
      '    SUM(COALESCE(KU.IZNOS,0)) PLATENO,'
      
        '    SUM(COALESCE(KU.IZNOS,0))-cast(g.IZNOS_VKUPNO as integer) RA' +
        'ZLIKA'
      'FROM KOM_PRESMETKA_G G'
      
        'LEFT JOIN KOM_UPLATI KU ON G.id=ku.presmetka_g_id and ku.flag = ' +
        '1'
      ' inner join KOM_LOKACII KL on G.LOKACIIN_ID=KL.ID'
      ' inner join kom_lokacija_arhiva kla on kla.id_lokacija=kl.id'
      
        ' inner join mat_partner p on p.id=kla.partner and p.tip_partner=' +
        'kla.tip_partner'
      'WHERE'
      '    KLa.id_lokacija=:LOKACIJA'
      '    and g.tip=1'
      
        '   and (((encodedate(1,g.mesec,g.godina) between encodedate(1,kl' +
        'a.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.' +
        'kraj_godina)) and kla.kraj_mesec is not null)'
      
        '    or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.poc_' +
        'mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc' +
        '_mesec is not null) ))'
      
        'GROUP BY G.BROJ_FAKTURA,G.DATUM_PRESMETKA,G.GODINA,G.MESEC,G.IZN' +
        'OS_VKUPNO,p.TIP_PARTNER, p.ID, p.naziv'
      'ORDER BY G.GODINA desc, G.MESEC desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 16
    Top = 588
    object tblPoslednoZadolzuvanjeBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoslednoZadolzuvanjeDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblPoslednoZadolzuvanjeGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblPoslednoZadolzuvanjeMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblPoslednoZadolzuvanjeIZNOS_VKUPNO: TFIBIntegerField
      FieldName = 'IZNOS_VKUPNO'
    end
    object tblPoslednoZadolzuvanjeNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPoslednoZadolzuvanjePLATENO: TFIBBCDField
      FieldName = 'PLATENO'
      Size = 2
    end
    object tblPoslednoZadolzuvanjeRAZLIKA: TFIBBCDField
      FieldName = 'RAZLIKA'
      Size = 2
    end
    object tblPoslednoZadolzuvanjeTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblPoslednoZadolzuvanjePARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
  end
  object dsPoslednoZadolzuvanje: TDataSource
    DataSet = tblPoslednoZadolzuvanje
    Left = 112
    Top = 524
  end
  object dsUplata: TDataSource
    DataSet = tblUplata
    Left = 80
    Top = 524
  end
  object tblUplata: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    g.id, '
      '    G.BROJ_FAKTURA,'
      '    G.DATUM_PRESMETKA,'
      '    G.GODINA,'
      '    G.MESEC,'
      '    cast(g.IZNOS_VKUPNO as integer) iznos_vkupno,'
      '    p.naziv,'
      '    SUM(COALESCE(KU.IZNOS,0)) PLATENO,'
      
        '    SUM(COALESCE(KU.IZNOS,0))-cast(g.IZNOS_VKUPNO as integer) RA' +
        'ZLIKA'
      'FROM KOM_PRESMETKA_G G'
      
        '    LEFT JOIN KOM_UPLATI KU ON G.id=ku.presmetka_g_id and ku.fla' +
        'g = 1'
      '    inner join KOM_LOKACII KL on G.LOKACIIN_ID=KL.ID'
      '   inner join kom_lokacija_arhiva kla on kla.id_lokacija=kl.id'
      
        '   inner join mat_partner p on p.id=kla.partner and p.tip_partne' +
        'r=kla.tip_partner'
      'WHERE( '
      '    g.mesec=:mesec'
      '    AND g.godina=:godina'
      '    AND KLa.id_lokacija=:LOKACIJA'
      '    and g.tip=1'
      
        '    and (((encodedate(1,g.mesec,g.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '    or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.poc_' +
        'mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc' +
        '_mesec is not null) ))'
      '     ) and (     G.ID = :OLD_ID'
      '     )'
      '    '
      
        'GROUP BY g.id,G.BROJ_FAKTURA,G.DATUM_PRESMETKA,G.GODINA,G.MESEC,' +
        'G.IZNOS_VKUPNO,p.naziv')
    SelectSQL.Strings = (
      'SELECT'
      '    g.id, '
      '    G.BROJ_FAKTURA,'
      '    G.DATUM_PRESMETKA,'
      '    G.GODINA,'
      '    G.MESEC,'
      '    cast(g.IZNOS_VKUPNO as integer) iznos_vkupno,'
      '    p.naziv,'
      '    SUM(COALESCE(KU.IZNOS,0)) PLATENO,'
      
        '    SUM(COALESCE(KU.IZNOS,0))-cast(g.IZNOS_VKUPNO as integer) RA' +
        'ZLIKA'
      'FROM KOM_PRESMETKA_G G'
      
        '    LEFT JOIN KOM_UPLATI KU ON G.id=ku.presmetka_g_id and ku.fla' +
        'g = 1'
      '    inner join KOM_LOKACII KL on G.LOKACIIN_ID=KL.ID'
      '   inner join kom_lokacija_arhiva kla on kla.id_lokacija=kl.id'
      
        '   inner join mat_partner p on p.id=kla.partner and p.tip_partne' +
        'r=kla.tip_partner'
      'WHERE'
      '    g.mesec=:mesec'
      '    AND g.godina=:godina'
      '    AND KLa.id_lokacija=:LOKACIJA'
      '    and g.tip=1'
      
        '    and (((encodedate(1,g.mesec,g.godina) between encodedate(1,k' +
        'la.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla' +
        '.kraj_godina)) and kla.kraj_mesec is not null)'
      
        '    or ((encodedate(1,g.mesec,g.godina) >= encodedate(1,kla.poc_' +
        'mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc' +
        '_mesec is not null) ))'
      
        'GROUP BY g.id,G.BROJ_FAKTURA,G.DATUM_PRESMETKA,G.GODINA,G.MESEC,' +
        'G.IZNOS_VKUPNO,p.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 144
    Top = 556
    object tblUplataBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblUplataGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblUplataMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblUplataIZNOS_VKUPNO: TFIBIntegerField
      FieldName = 'IZNOS_VKUPNO'
    end
    object tblUplataNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataPLATENO: TFIBBCDField
      FieldName = 'PLATENO'
      Size = 2
    end
    object tblUplataRAZLIKA: TFIBBCDField
      FieldName = 'RAZLIKA'
      Size = 2
    end
    object tblUplataID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object tblUplataSaldo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_UPLATA_SALDO'
      'SET '
      '    ID = :ID,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    IZNOS = :IZNOS,'
      '    TIP = :TIP,'
      '    DOKUMENT = :DOKUMENT,'
      '    DATUM_UPLATA = :DATUM_UPLATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KOM_UPLATA_SALDO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KOM_UPLATA_SALDO('
      '    ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    IZNOS,'
      '    TIP,'
      '    DOKUMENT,'
      '    DATUM_UPLATA'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :IZNOS,'
      '    :TIP,'
      '    :DOKUMENT,'
      '    :DATUM_UPLATA'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    kus.id,'
      '    kus.tip_partner,'
      '    kus.partner,'
      '    kus.iznos,'
      '    kus.tip,'
      '    kus.dokument,'
      '    kus.datum_uplata'
      'from kom_uplata_saldo kus'
      'where(  kus.tip_partner=:tip_partner and kus.partner=:partner'
      
        'and kus.tip=:tip and kus.dokument=:dokument and kus.datum_uplata' +
        '=:datum_uplata'
      '     ) and (     KUS.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select'
      '    kus.id,'
      '    kus.tip_partner,'
      '    kus.partner,'
      '    kus.iznos,'
      '    kus.tip,'
      '    kus.dokument,'
      '    kus.datum_uplata'
      'from kom_uplata_saldo kus'
      'where kus.tip_partner=:tip_partner and kus.partner=:partner'
      
        'and kus.tip=:tip and kus.dokument=:dokument and kus.datum_uplata' +
        '=:datum_uplata'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 588
    object tblUplataSaldoID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblUplataSaldoIZNOS: TFIBBCDField
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblUplataSaldoTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblUplataSaldoPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblUplataSaldoTIP: TFIBSmallIntField
      FieldName = 'TIP'
    end
    object tblUplataSaldoDOKUMENT: TFIBStringField
      FieldName = 'DOKUMENT'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataSaldoDATUM_UPLATA: TFIBDateField
      FieldName = 'DATUM_UPLATA'
    end
  end
  object dsUplataSaldo: TDataSource
    DataSet = tblUplataSaldo
    Left = 144
    Top = 492
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 48
    Top = 492
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 80
    Top = 492
  end
  object qDaliImaSaldo: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(kus.iznos,0) saldo'
      'from kom_uplata_saldo kus'
      'where kus.tip_partner=:tip_partner and kus.partner=:partner'
      'and kus.tip=:tip and kus.dokument=:dokument')
    Left = 112
    Top = 588
    qoStartTransaction = True
  end
  object dsBrisiUplataSaldo: TDataSource
    DataSet = tblBrisiUplataSaldo
    Left = 48
    Top = 524
  end
  object tblBrisiUplataSaldo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_UPLATA_SALDO'
      'SET '
      '    ID = :ID,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    IZNOS = :IZNOS,'
      '    TIP = :TIP,'
      '    DOKUMENT = :DOKUMENT,'
      '    DATUM_UPLATA = :DATUM_UPLATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    KOM_UPLATA_SALDO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO KOM_UPLATA_SALDO('
      '    ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    IZNOS,'
      '    TIP,'
      '    DOKUMENT,'
      '    DATUM_UPLATA'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :IZNOS,'
      '    :TIP,'
      '    :DOKUMENT,'
      '    :DATUM_UPLATA'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    kus.id,'
      '    kus.tip_nalog,'
      '    kus.iznos'
      'from kom_uplata_saldo kus'
      
        'left outer join kom_uplati ku on ku.id_saldo=kus.id and ku.flag ' +
        '= 1'
      'where(  kus.tip_partner=:tip_partner and kus.partner=:partner'
      
        '    and kus.tip=:tip and kus.datum_uplata=:datum_uplata and kus.' +
        'dokument=:dokument'
      '    and ku.id is null and kus.tip_nalog is null'
      '--and kus.tip_nalog is null and kus.iznos=kus.ostanato'
      '     ) and (     KUS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    kus.id,'
      '    kus.tip_nalog,'
      '    kus.iznos'
      'from kom_uplata_saldo kus'
      
        'left outer join kom_uplati ku on ku.id_saldo=kus.id and ku.flag ' +
        '= 1'
      'where kus.tip_partner=:tip_partner and kus.partner=:partner'
      
        '    and kus.tip=:tip and kus.datum_uplata=:datum_uplata and kus.' +
        'dokument=:dokument'
      '    and ku.id is null and kus.tip_nalog is null'
      '--and kus.tip_nalog is null and kus.iznos=kus.ostanato')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 556
    object tblBrisiUplataSaldoID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblBrisiUplataSaldoTIP_NALOG: TFIBStringField
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBrisiUplataSaldoIZNOS: TFIBBCDField
      FieldName = 'IZNOS'
      Size = 2
    end
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 112
    Top = 492
  end
  object qBrisiUpaltaFirmi: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete'
      'from  kom_uplati ku'
      'where'
      '    ku.tip=:tip'
      '    and ku.datum_uplata=:datum_uplata'
      '    and ku.dokument=:dokument'
      
        '    and ku.lokaciin_id in (select l.id from kom_lokacii l where ' +
        'l.tip_partner=:tip_partner and l.partner_id=:partner )'
      '    and ku.tip_nalog is null'
      '    and ku.flag=1')
    Description = 
      #1044#1072' '#1089#1077' '#1080#1079#1073#1088#1080#1096#1072#1090' '#1089#1080#1090#1077' '#1091#1087#1083#1072#1090#1080', '#1087#1086' '#1090#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1072', '#1076#1072#1090#1091#1084' '#1085#1072' '#1091#1087#1083#1072#1090#1072' '#1080' ' +
      #1076#1086#1082#1091#1084#1077#1085#1090' '
    Left = 16
    Top = 620
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblNalog: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select FG.ID, FG.RE, FG.GODINA, FG.TIP_NALOG||'#39'/'#39'|| FG.NALOG opi' +
        's_nalog, fg.TIP_NALOG, fg.nalog,  FG.DATUM, FG.OPIS, FG.PROKNIZE' +
        'N, FG.OTVOREN, FG.VID_NALOG,'
      '       FG.TS_INS, FG.TS_UPD, FG.USR_INS, FG.USR_UPD'
      'from FIN_NG FG                       '
      'where(  FG.PROKNIZEN = 0 and '
      'fg.DATUM>'#39'16.06.2019'#39'  and '
      'fg.RE = :re'
      '     ) and (     FG.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'select FG.ID, FG.RE, FG.GODINA, FG.TIP_NALOG||'#39'/'#39'|| FG.NALOG opi' +
        's_nalog, fg.TIP_NALOG, fg.nalog,  FG.DATUM, FG.OPIS, FG.PROKNIZE' +
        'N, FG.OTVOREN, FG.VID_NALOG,'
      '       FG.TS_INS, FG.TS_UPD, FG.USR_INS, FG.USR_UPD'
      'from FIN_NG FG                       '
      'where FG.PROKNIZEN = 0 and '
      'fg.DATUM>'#39'16.06.2019'#39'  and '
      'fg.RE = :re')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 112
    Top = 556
    object tblNalogRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblNalogID: TFIBBCDField
      FieldName = 'ID'
      Size = 0
    end
    object tblNalogGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblNalogDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblNalogOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogPROKNIZEN: TFIBSmallIntField
      FieldName = 'PROKNIZEN'
    end
    object tblNalogOTVOREN: TFIBSmallIntField
      FieldName = 'OTVOREN'
    end
    object tblNalogVID_NALOG: TFIBSmallIntField
      FieldName = 'VID_NALOG'
    end
    object tblNalogTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblNalogTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblNalogUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogOPIS_NALOG: TFIBStringField
      FieldName = 'OPIS_NALOG'
      Size = 17
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogTIP_NALOG: TFIBStringField
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNalogNALOG: TFIBIntegerField
      FieldName = 'NALOG'
    end
  end
  object dsNalog: TDataSource
    DataSet = tblNalog
    Left = 16
    Top = 524
  end
  object qUpdateUplata: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_uplati ku'
      'set ku.id_fng = null,'
      '    ku.tip_nalog = null,'
      '    ku.nalog = null,'
      '    ku.re = null,'
      '    ku.ts_knizenje = null'
      ' '
      'where'
      '    ku.id=:id')
    Description = #1044#1072' '#1089#1077' '#1088#1072#1089#1082#1085#1080#1078#1080' '#1091#1087#1083#1072#1090#1072#1090#1072
    Left = 80
    Top = 588
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qKlucUplata: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select max(ku.id) id'
      'from kom_uplati ku')
    Left = 764
    Top = 365
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pBelFin: TpFIBStoredProc
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE "PROC_FIN_IOS_SELECT_KOM " (?IN_RE, ?IN_GOD, ?' +
        'IN_NALOG, ?IN_TP, ?IN_P, ?IN_DOKUMENT, ?IN_D)')
    StoredProcName = 'PROC_FIN_IOS_SELECT_KOM '
    Left = 644
    Top = 389
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qProveriNalog: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'SELECT'
      '   1 da'
      'FROM'
      '    FIN_NG f'
      'where f.PROKNIZEN = 1'
      'and id = :id')
    Left = 148
    Top = 173
    qoStartTransaction = True
  end
end
