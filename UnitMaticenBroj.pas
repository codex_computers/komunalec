unit UnitMaticenBroj;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, FIBQuery, pFIBQuery, cxControls,
  cxContainer, cxEdit, cxGroupBox, cxLabel, ComCtrls, cxTextEdit,StdCtrls,
  Menus, cxButtons, cxGraphics, cxLookAndFeels, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;//, dxSkinOffice2013White;

type
  TfrmMaticenBroj = class(TForm)
    Panel1: TPanel;
    gbMatSer: TcxGroupBox;
    qMaticenBroj: TpFIBQuery;
    txtMaticen: TcxTextEdit;
    StatusBar1: TStatusBar;
    cxGroupBox2: TcxGroupBox;
    lblKorisnik: TcxLabel;
    lblTip: TcxLabel;
    lblId: TcxLabel;
    lblLok: TcxLabel;
    qSeriskiBroj: TpFIBQuery;
    cxButton1: TcxButton;
    procedure txtMaticenKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtMaticenEnter(Sender: TObject);
    procedure txtMaticenExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMaticenBroj: TfrmMaticenBroj;

implementation

uses UnitDM1, dmKonekcija, dmMaticni, DaNe;

{$R *.dfm}

procedure TfrmMaticenBroj.cxButton1Click(Sender: TObject);
begin
    frmDaNe := TfrmDaNe.Create(self, '�������', '���� � ����� ������ �����?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort
       else
       begin
       if tag=0 then
       begin
         qMaticenBroj.Close;
         qMaticenBroj.ParamByName('maticen').AsString:=txtMaticen.Text;
         qMaticenBroj.ParamByName('tp').AsInteger:=DM1.tblPregledLokaciiVodomeriTIP_PARTNER.Value;
         qMaticenBroj.ParamByName('p').AsInteger:=DM1.tblPregledLokaciiVodomeriPARTNER_ID.Value;
         qMaticenBroj.ExecQuery;
       end
       else
       begin
         qSeriskiBroj.Close;
         qSeriskiBroj.ParamByName('seriski_broj').AsString:=txtMaticen.Text;
         qSeriskiBroj.ParamByName('l').AsInteger:=dm1.tblPregledLokaciiVodomeriLOKACIJA.Value;
         qSeriskiBroj.ParamByName('id').AsAnsiString:=DM1.tblPregledLokaciiVodomeriID.AsString;
         qSeriskiBroj.ExecQuery;
       end;
       tag:=3;
       Close;
      end
end;

procedure TfrmMaticenBroj.FormShow(Sender: TObject);
begin
   if tag=0 then
   begin
     gbMatSer.Caption:=' ������� ��� ';
   end
   else
   begin
     gbMatSer.Caption:=' ������� ��� �� ������� ';

   end;
end;

procedure TfrmMaticenBroj.txtMaticenEnter(Sender: TObject);
begin
  TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmMaticenBroj.txtMaticenExit(Sender: TObject);
begin
  TEdit(Sender).Color:=clWhite;
end;

procedure TfrmMaticenBroj.txtMaticenKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if key=VK_RETURN then
      begin
        frmDaNe := TfrmDaNe.Create(self, '�������', '���� � ����� ������ �����?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort
       else
       begin
       if tag=0 then
       begin
         qMaticenBroj.Close;
         qMaticenBroj.ParamByName('maticen').AsString:=txtMaticen.Text;
         qMaticenBroj.ParamByName('tp').AsInteger:=DM1.tblPregledLokaciiVodomeriTIP_PARTNER.Value;
         qMaticenBroj.ParamByName('p').AsInteger:=DM1.tblPregledLokaciiVodomeriPARTNER_ID.Value;
         qMaticenBroj.ExecQuery;
       end
       else
       begin
         qSeriskiBroj.Close;
         qSeriskiBroj.ParamByName('seriski_broj').AsString:=txtMaticen.Text;
         qSeriskiBroj.ParamByName('l').AsInteger:=dm1.tblPregledLokaciiVodomeriLOKACIJA.Value;
         qSeriskiBroj.ParamByName('id').AsAnsiString:=DM1.tblPregledLokaciiVodomeriVODOMER.Value;
         qSeriskiBroj.ExecQuery;
       end;
       tag:=3;
       Close;
      end
      end
      else
      if key=VK_ESCAPE then
      begin
         tag:=2;
         Close;
      end;

end;

end.
