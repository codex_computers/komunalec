object FrmPromeniCitackaKnigaRb: TFrmPromeniCitackaKnigaRb
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1084#1077#1085#1080' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072'/'#1088#1077#1076#1077#1085' '#1073#1088#1086#1112' '#1074#1086' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
  ClientHeight = 150
  ClientWidth = 340
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 131
    Width = 340
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'ESC - '#1048#1079#1083#1077#1079
  end
  object panelGlaven: TPanel
    Left = 0
    Top = 0
    Width = 340
    Height = 131
    Align = alClient
    BevelInner = bvLowered
    Color = 6988454
    ParentBackground = False
    TabOrder = 1
    object lblRedenBrojOld: TLabel
      Left = 8
      Top = 50
      Width = 175
      Height = 13
      Caption = #1057#1090#1072#1088' '#1088#1077#1076#1077#1085' '#1073#1088#1086'j '#1074#1086' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
    end
    object lblCitackaKniga: TLabel
      Left = 8
      Top = 15
      Width = 75
      Height = 13
      Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
    end
    object lblCitackaKnigaRbNew: TLabel
      Left = 8
      Top = 85
      Width = 77
      Height = 13
      Caption = #1053#1086#1074' '#1088#1077#1076#1077#1085' '#1073#1088#1086#1112
    end
    object cxTxtCitackaKniga: TcxTextEdit
      Left = 192
      Top = 11
      Enabled = False
      TabOrder = 0
      Width = 105
    end
    object cxTxtRedenBrojOld: TcxTextEdit
      Left = 192
      Top = 46
      Enabled = False
      TabOrder = 1
      Width = 105
    end
    object cxTxtRedenBrojNew: TcxTextEdit
      Left = 192
      Top = 81
      TabOrder = 2
      OnEnter = onTxtEnter
      OnExit = onTxtExit
      OnKeyDown = onTxtKeyDown
      Width = 105
    end
  end
  object ActionList1: TActionList
    Left = 304
    Top = 8
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
  end
end
