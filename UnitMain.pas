unit UnitMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FIBDataSet, dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxBarPainter,
  dxBar, cxClasses, dxSkinscxPCPainter, ComCtrls, StdCtrls, ExtCtrls, DB,
  pFIBDataSet, ActnList, cxControls, cxContainer, cxEdit, cxTextEdit, cxDBEdit,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinSilver, dxSkinStardust, dxSkinSummer2008, dxSkinValentine,
  dxSkinXmas2008Blue, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxRibbon, jpeg, cxLabel, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSpringTime,
  dxSkinsdxRibbonPainter, dxRibbonSkins, MK, KamatnaStapka, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxRibbonCustomizationForm, System.Actions, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxGDIPlusClasses;//, //dxSkinOffice2013White,
 // System.Actions;

type
  TFrmMain = class(TForm)
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    SubListSifrarnici: TdxBarSubItem;
    BarButtonLokacii: TdxBarButton;
    BarButton: TdxBarButton;
    Statusbar: TStatusBar;
    BarButtonReoni: TdxBarButton;
    BarButtonUlici: TdxBarButton;
    SubListPresmetki: TdxBarSubItem;
    BarButtonVodomeriSostojba: TdxBarButton;
    BarButtonVidoviUslugi: TdxBarButton;
    BarButtonUslugi: TdxBarButton;
    BarButtonCenovnik: TdxBarButton;
    BarButtonPresmetka: TdxBarButton;
    BarButtonUplati: TdxBarButton;
    BarButtonPrUplatiDatumski: TdxBarButton;
    SubItemPregled: TdxBarSubItem;
    Panel1: TPanel;
    tblSubMeni: TpFIBDataSet;
    tblSubMeniID: TFIBIntegerField;
    tblSubMeniKOREN: TFIBIntegerField;
    tblSubMeniNASLOV: TFIBStringField;
    dsSubMeni: TDataSource;
    tblMeni: TpFIBDataSet;
    tblMeniKOREN: TFIBIntegerField;
    tblMeniID: TFIBIntegerField;
    tblMeniNASLOV: TFIBStringField;
    tblMeniPOJAVIUSLOVI: TFIBIntegerField;
    tblMeniTABELA_GRUPA: TFIBStringField;
    tblMeniBR: TFIBIntegerField;
    dsMeni: TDataSource;
    mButton: TdxBarButton;
    ActionList1: TActionList;
    aPrikazi: TAction;
    dxBarButton1: TdxBarButton;
    aDopUslugi: TAction;
    DopUslugi: TdxBarButton;
    aKartica: TAction;
    KartKorisnik: TdxBarButton;
    aTipUplata: TAction;
    TipUplati: TdxBarButton;
    aPecatiDukani: TAction;
    PecatiDukani: TdxBarButton;
    aDULista: TAction;
    DULista: TdxBarButton;
    aOpomeni: TAction;
    Opomena: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    aSnimiMeni: TAction;
    aKorisnik: TAction;
    SnimiMeni: TdxBarButton;
    aAplikacii: TdxBarButton;
    Korisnici: TdxBarButton;
    aAplikacija: TAction;
    aKorisnikUlogi: TAction;
    Ulogi: TdxBarButton;
    aUloga: TAction;
    Uloga: TdxBarButton;
    aPregledLokacii: TAction;
    PregledLokacii: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    Lokacii: TdxBarSubItem;
    aFizicki: TAction;
    aSite: TAction;
    SiteLokacii: TdxBarButton;
    SamoFizickiLica: TdxBarButton;
    aCitackiKartoni: TAction;
    CitackiKartoni: TdxBarButton;
    aPeriod: TAction;
    Period: TdxBarButton;
    aSpecifikacijaReoni: TAction;
    aDizajn: TAction;
    dxBarButton2: TdxBarButton;
    Panel2: TPanel;
    aLokacijaPartner: TAction;
    LokacijaArhiva: TdxBarButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarManager1Bar5: TdxBar;
    dxBarManager1Bar6: TdxBar;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    Image1: TImage;
    Image2: TImage;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    dxRibbon1Tab3: TdxRibbonTab;
    Image3: TImage;
    lblDatumVreme: TcxLabel;
    cxLabel2: TcxLabel;
    aFormConfig: TAction;
    aSnimi: TAction;
    aUlogaK: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarButton3: TdxBarButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aIzlez: TAction;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton27: TdxBarLargeButton;
    aAbout: TAction;
    aKamatnaStapka: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    aKorekcija: TAction;
    aPromenaPodatoci: TAction;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarLargeButton30: TdxBarLargeButton;
    aIskluciKamata: TAction;
    dxBarLargeButton31: TdxBarLargeButton;
    aMernaTocka: TAction;
    dxBarLargeButton32: TdxBarLargeButton;
    aKN: TAction;
    btTipPakuvanje1: TdxBarButton;
    btTipPakuvanje2: TdxBarButton;
    aEFakturi: TAction;
    aPregledSlikiDizajn: TAction;
    dxbrlrgbtn1: TdxBarLargeButton;
    aSlikiVodomeri: TAction;
    aFirmi: TAction;
    dxBarButton6: TdxBarButton;
    dxBarLargeButton33: TdxBarLargeButton;
    aKarticaFin: TAction;
    procedure otvoriTabeli();
    procedure showAndFreeForm(form: Tform);
    procedure BarButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BarButtonLokaciiClick(Sender: TObject);
    procedure BarButtonReoniClick(Sender: TObject);
    procedure BarButtonUliciClick(Sender: TObject);
    procedure BarButtonVodomeriSostojbaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BarButtonVidoviUslugiClick(Sender: TObject);
    procedure BarButtonUslugiClick(Sender: TObject);
    procedure BarButtonCenovnikClick(Sender: TObject);
    procedure BarButtonPresmetkaClick(Sender: TObject);
    procedure BarButtonUplatiClick(Sender: TObject);
    procedure BarButtonPrUplatiDatumskiClick(Sender: TObject);
    procedure CreateReportsMenu;
    procedure aPrikaziExecute(Sender: TObject);
    procedure dxBarMgrMainClickItem(Sender:TdxBarManager;
      ClickedItem:TdxBarItem);
    procedure FormShow(Sender: TObject);
    procedure aDopUslugiExecute(Sender: TObject);
    procedure aKarticaExecute(Sender: TObject);
    procedure aTipUplataExecute(Sender: TObject);
    procedure aPecatiDukaniExecute(Sender: TObject);
    procedure aDUListaExecute(Sender: TObject);
    procedure aOpomeniExecute(Sender: TObject);
    procedure aSnimiMeniExecute(Sender: TObject);
    procedure aKorisnikExecute(Sender: TObject);
    procedure aAplikacijaExecute(Sender: TObject);
    procedure aKorisnikUlogiExecute(Sender: TObject);
    procedure aUlogaExecute(Sender: TObject);
    procedure aPregledLokaciiExecute(Sender: TObject);
    constructor Create(Owner : TComponent; tip_partner : string);reintroduce; overload;
    procedure SiteLokaciiClick(Sender: TObject);
    procedure SamoFizickiLicaClick(Sender: TObject);
    procedure aCitackiKartoniExecute(Sender: TObject);
    procedure aPeriodExecute(Sender: TObject);
    procedure aSpecifikacijaReoniExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
    procedure aLokacijaPartnerExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure aSnimiExecute(Sender: TObject);
    procedure aUlogaKExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aKamatnaStapkaExecute(Sender: TObject);
    procedure aKorekcijaExecute(Sender: TObject);
    procedure aPromenaPodatociExecute(Sender: TObject);
    procedure aIskluciKamataExecute(Sender: TObject);
    procedure aMernaTockaExecute(Sender: TObject);
    procedure aKNExecute(Sender: TObject);
    procedure aEFakturiExecute(Sender: TObject);
    procedure aSlikiVodomeriExecute(Sender: TObject);
    procedure PocniTimerTimer(Sender: TObject);
    procedure aKarticaFinExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMain: TFrmMain;
implementation

uses UnitDM1,  Partner, dmMaticni, UnitLokacii, UnitReoni,
  UnitUlici, UnitVodomeriSostojbaVnes, UnitVidoviUslugi, UnitUslugi,
  UnitCenovnik, UnitPromeniCitackaKnigaRb, UnitPresmetka, UnitUplati,
  UnitPrUplatiDatumski, dmKonekcija,cxConstantsMak, UnitDopUsluga,
  KarticaKorisnik, UnitTipUplati, UnitPecatiDukani, DULista,
  KorisnikUloga,
  KorisniciPateka, Aplikacii,Ulogi, PregledLokacii, UnitCitackiKartoni,
  UnitPeriod, dmReportUnit,  UnitIzborTP, LokaciiPoPartner, dmResources,
  FormConfig, AboutBox, Utils, UnitKorekcija, UnitPromena, PromenaPodatoci,
  IskluciKamata, MernaTocka, KarticaKorisnikNova, EFakturi, SlikiVodomeri, u7ThreadService, KarticaKorisnikFin;

{$R *.dfm}

procedure TFrmMain.BarButtonCenovnikClick(Sender: TObject);
var frm:TFrmCenovnik;
begin
    frm := TFrmCenovnik.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonClick(Sender: TObject);
var frm:TfrmPartner;
begin
    dm1.tblPartner.close;
    dm1.tblPartner.ParamByName('tp').AsString:='%';
    dm1.tblPartner.open;
    frm := TfrmPartner.Create(nil);
    frm.Tag := 1;
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonLokaciiClick(Sender: TObject);
var frm:TFrmLokacija;
begin
    frm := TFrmLokacija.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonPresmetkaClick(Sender: TObject);
var frm:TFrmKomPresmetka;
begin
    frm := TFrmKomPresmetka.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonPrUplatiDatumskiClick(Sender: TObject);
var frm:TFrmPrUplatiDatumski;
begin
    frm := TFrmPrUplatiDatumski.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonReoniClick(Sender: TObject);
var frm:TFrmReoni;
begin
    frm := TFrmReoni.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonUliciClick(Sender: TObject);
var frm:TFrmUlici;
begin
    frm := TFrmUlici.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonUplatiClick(Sender: TObject);
var frm:TFrmUplati;
begin
    frm := TFrmUplati.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonUslugiClick(Sender: TObject);
var frm:TFrmUslugi;
begin
    frm := TFrmUslugi.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonVidoviUslugiClick(Sender: TObject);
var frm:TFrmVidoviUslugi;
begin
    frm := TFrmVidoviUslugi.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.BarButtonVodomeriSostojbaClick(Sender: TObject);
var frm:TFrmVodomeriSostojba;
begin
    frm := TFrmVodomeriSostojba.Create(nil);
    showAndFreeForm(frm);
end;



procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    dmKon.fibBaza.Close();
    dmKon.fibPateka.Close();
    dmKon.tBaza.Active := false;
    dmKon.tPateka.Active := false;
    Action := caFree;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
   dmKon.fibPateka.Connected := true;
   dmkon.aplikacija:='KOM';
   if dmKon.fibBaza.Connected=false then
    begin
      Application.Terminate;
    end
    else
    begin
         otvoriTabeli();
    end;
    if sos_int = '1' then
    begin
      ima_promena := '0';
     // PocniTimer.Enabled := True;
    end;

    lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
    // za ponatamu ....
//    if dmKon.user='DSLAVICA'then
//    begin
//    dmRes.dxSkinController1.SkinName := 'Office2007Green';
//    dmRes.cxLookAndFeelController1.SkinName := 'Office2007Green';
//    dmRes.dxDockingManager1.LookAndFeel.SkinName := 'Office2007Green';
//    end;
if sos_int = '1' then
 begin
   frmThreadService := TfrmThreadService.Create(self);
   try
     frmThreadService.PocniTimer.Enabled := True;
   except
//
   end;
 end;
end;

procedure TFrmMain.FormShow(Sender: TObject);
   var filename:String;
begin

   WindowState:=wsMaximized;
    if dmKon.prava=0 then
        begin
           dxBarManager1.Bars[0].AllowCustomizing:=true;
           dxBarManager1.Bars[1].Visible:=true;
//
           dmKOn.tblAplikacii.Open;
           dmKon.tblUseri.Open;
           dmkon.tblUlogi.Open;
           dmKon.tblKorisnici.Open;
           dmKon.tblKorisnikUloga.Open;
//
//
        end
        else
        begin
           filename:='MeniOdBaza_'+dmKon.user+'.ini';
        //   ShowMessage('das '+dmKon.aplikacija);
        //   ShowMessage('UsER '+dmKon.user);
         //  ShowMessage('re '+IntToStr(dmKon.qryUserUloga.RecordCount));


           dmKon.qryUserUlogaMENI.SaveToFile(dmkon.RabotenDir+filename);
           dxBarManager1.LoadFromIniFile(dmkon.RabotenDir+filename);
           dxBarManager1.CanCustomize:=false;
           dxBarManager1.Bars[1].Visible:=false;
        end;
//  lblDatum.Left:=Width - 250;
 // FIBqryDatum.Open;
 // lblDatum.Caption:=FIBqryDatumDATUM.DisplayText;
  CreateReportsMenu;

    //���� �����, �� ��� �� ������ ����������� �� ��
  frmMK:=TfrmMK.Create(Application);
  frmMK.ShowModal;
  frmMK.free;
  // CreateReportsMenu;
 // cxSetEureka('biljana.trpkoska@codex.mk');
end;

procedure TFrmMain.otvoriTabeli();
begin
     dmKOn.tblAplikacii.Open;
     dmKon.tblUseri.Open;
     dmkon.tblUlogi.Open;
     dmKon.tblKorisnici.Open;
     dmKon.tblKorisnikUloga.Open;

    DM1.TblKomReoni.Open;
    DM1.TblKomVidoviUslugi.Open;
    dm1.TblKomVidoviUslugi.Open;
    dm1.TblKomUslugi.open;
  //  dm1.TblDopUslugi.Open;
    dm1.TblRati.Open;
    dm1.tblTipUplata.Open;

    //DM1.TblKomUslugi.Open;
   // dmLookUp.tblPartner.open;
    dm1.tblPartner.ParamByName('tp').AsString:='%';
    dm1.tblPartner.Open();
    dmMat.tblPartner.Open();
    dmMat.tblTipPartner.Open();
    dmMat.tblRE.Open();
    dmMat.tblMesto.Open;
    DM1.qSetupFizicki.ExecQuery;
    fizicki_od := dm1.qSetupFizicki.FldByName['v1'].AsDate;
    DM1.qSetupDukani.ExecQuery;
    pravni_od := dm1.qSetupDukani.FldByName['v1'].AsDate;
    dm1.tblKontenPlan.Open;
    dm1.tblKamatnaStapka.Open;
    dm1.qSetupLink.ExecQuery;
    link_insert := dm1.qSetupLink.FldByName['V1'].AsString;
    dm1.qSetupLinkedit.ExecQuery;
    link_edit := dm1.qSetupLinkedit.FldByName['V1'].AsString;
    dm1.qSetupLinkZemi.ExecQuery;
    dm1.tblMernaTocka.Open;
    link_zemi := dm1.qSetupLinkZemi.FldByName['V1'].AsString;

    dm1.qSetupLinkPrezemi.ExecQuery;
    link_prezemi := dm1.qSetupLinkPrezemi.FldByName['V1'].AsString;

    dm1.qSetupLinkSlika.ExecQuery;
    link_sliki := dm1.qSetupLinkSlika.FldByName['V1'].AsString;

    dm1.qSosInt.ExecQuery;
    sos_int := dm1.qSosInt.FldByName['v1'].AsString;

    dm1.qSetupServis.ExecQuery;
    servis := dm1.qSetupServis.FldByName['v1'].AsString;

    dm1.qSetupFirma.ExecQuery;
    firma := dm1.qSetupFirma.FldByName['v1'].AsString;

    dm1.qSetupGG100.ExecQuery;
    gorna_granica1 := dm1.qSetupGG100.FldByName['v1'].AsString;

    dm1.qSetupGG500.ExecQuery;
    gorna_granica5 := dm1.qSetupGG500.FldByName['v2'].AsString;
end;

procedure TFrmMain.PocniTimerTimer(Sender: TObject);
begin
  if ima_promena = '1' then
  begin
  // frmThreadService := TfrmThreadService.Create(self);
   try
     //frmThreadService.PocniTimer.Enabled := True;
     //frmThreadService.aPocni.execute;
   except

   end;
   try

   except

   end;
  end;
  ima_promena := '0';
end;

procedure TFrmMain.SamoFizickiLicaClick(Sender: TObject);
begin
   frmPregledLokacii:=TfrmPregledLokacii.Create(Self,'5');
   frmPregledLokacii.Caption:='������� �� ������� �� ������� ����';
   frmPregledLokacii.ShowModal;
   frmPregledLokacii.Free;
end;

procedure TFrmMain.showAndFreeForm(form:TForm);
begin
    form.ShowModal;
    form.Free;
end;

procedure TFrmMain.SiteLokaciiClick(Sender: TObject);
begin
   frmPregledLokacii:=TfrmPregledLokacii.Create(Self,'%');
   frmPregledLokacii.ShowModal;
   frmPregledLokacii.Free;
end;

procedure TfrmMain.CreateReportsMenu;
var ime:AnsiString; AItemLink:TdxBarItemLink; i,j,k:integer; mSub:TdxBarSubItem;
begin
   { if dmKon.RepciOdPateka then
    begin
       tblSubMeni.Database:=dmKon.fibPateka;
       tblSubMeni.Transaction:=dmKon.tPateka;
       tblSubMeni.UpdateTransaction:=dmKon.tPateka;
       tblMeni.Database:=dmKon.fibPateka;
       tblMeni.Transaction:=dmKon.tPateka;
       tblMeni.UpdateTransaction:=dmKon.tPateka;
    end
    else
    begin
       tblSubMeni.Database:=dm.Baza;
       tblSubMeni.Transaction:=dm.Transakcija;
       tblSubMeni.UpdateTransaction:=dm.Transakcija;
       tblMeni.Database:=dm.Baza;
       tblMeni.Transaction:=dm.Transakcija;
       tblMeni.UpdateTransaction:=dm.Transakcija;
    end;}
    tblSubMeni.Open;
    tblSubMeni.First;
    tblMeni.DataSource:=dsSubMeni;
    tblMeni.Open;

    while (not tblSubMeni.Eof) do
      begin
        ime := 's' + IntToStr(i);
        mSub :=  TdxBarSubItem.Create(Forms.Application);
        mSub.Name := ime;
        mSub.Enabled := true;
        mSub.Visible := ivAlways;
        mSub.Category := 0;
        mSub.Caption := tblSubMeniNASLOV.AsString;
        AItemLink:= SubItemPregled.ItemLinks.Add;
        AItemLink.Index := i;
        AItemLink.Item := mSub;
        tblMeni.First;

        while (not tblMeni.Eof) do
         begin
            ime := 'ss' + IntToStr(j);
            mButton := TdxBarButton.Create(Forms.Application);
            mButton.Name := ime;
            mButton.Enabled := True;
            mButton.Visible := ivAlways;
            mButton.Description := 'reports';
            mButton.Tag := tblMeniBR.Value;
            mButton.HelpContext := tblMeniPOJAVIUSLOVI.Value;
            mButton.Caption := tblMeniNASLOV.AsString;
            AItemLink := mSub.ItemLinks.Add;
            AItemLink.Index := k;
            AItemLink.Item := mButton;
            tblMeni.Next;
            j:=j+1;
            k:=k+1;
          end;
        k := 0;
        tblSubMeni.Next;
        i:=i+1;
    end;
end;

procedure TFrmMain.aTipUplataExecute(Sender: TObject);
var frm:TfrmTipUplati;
begin
   frmTipUplati:=TfrmTipUplati.Create(self,true);
   frmTipUplati.ShowModal;
   frmTipUplati.free;
end;

procedure TFrmMain.aUlogaExecute(Sender: TObject);
begin
   frmUlogi:=TfrmUlogi.Create(Application);
   frmUlogi.ShowModal;
   frmUlogi.Free;
end;

procedure TFrmMain.aUlogaKExecute(Sender: TObject);
begin
  aUloga.Execute;
end;

procedure TFrmMain.aKorisnikUlogiExecute(Sender: TObject);
begin
  frmKorisnikUloga:=TfrmKorisnikUloga.Create(Application);
  frmKorisnikUloga.ShowModal;
  frmKorisnikUloga.Free;

end;

procedure TFrmMain.aLokacijaPartnerExecute(Sender: TObject);
begin
  frmLokaciiPoPartner:=TfrmLokaciiPoPartner.Create(Application);
  frmLokaciiPoPartner.ShowModal;
  frmLokaciiPoPartner. free;
end;

procedure TFrmMain.aMernaTockaExecute(Sender: TObject);
begin
   frmMernaTocka := TfrmMernaTocka.Create(Self);
   frmMernaTocka.ShowModal;
   frmMernaTocka.Free;
end;

procedure TFrmMain.aAboutExecute(Sender: TObject);
begin
   frmAboutBox:=TfrmAboutBox.Create(Application);
   frmAboutBox.ShowModal;
   frmAboutBox.free;
end;

procedure TFrmMain.aAplikacijaExecute(Sender: TObject);
begin
   frmAplikacii:=TfrmAplikacii.Create(Application);
   frmAplikacii.ShowModal;
   frmAplikacii.Free;
end;

procedure TFrmMain.aCitackiKartoniExecute(Sender: TObject);
begin
      frmCitackiKartoni:=TfrmCitackiKartoni.Create(Application);
      frmCitackiKartoni.ShowModal;
      frmCitackiKartoni.Free;

end;

procedure TFrmMain.aIskluciKamataExecute(Sender: TObject);
begin
   frmIskluciKamata := TfrmIskluciKamata.Create(self);
   frmIskluciKamata.ShowModal;
   frmIskluciKamata.Free;
end;

procedure TFrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TFrmMain.aDizajnExecute(Sender: TObject);
begin
   dmReport.Spremi(18,dmKon.aplikacija);
   dmReport.tblSqlReport.Open;
   dmReport.frxReport1.DesignReport();
end;

procedure TFrmMain.aDopUslugiExecute(Sender: TObject);
var frm:TfrmDopUsluga;
begin
    frm := TfrmDopUsluga.Create(nil);
    showAndFreeForm(frm);
end;

procedure TFrmMain.aDUListaExecute(Sender: TObject);
begin
  frmDULista:=TfrmDULista.Create(Application);
  frmDULista.ShowModal;
  frmDULista.free;
end;

procedure TFrmMain.aEFakturiExecute(Sender: TObject);
begin
  frmEFakturi := TfrmEFakturi.Create(Self);
  frmEFakturi.ShowModal;
  frmEFakturi.Free;
end;

procedure TFrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TFrmMain.aKamatnaStapkaExecute(Sender: TObject);
begin
  frmKamatnaStapka:=TfrmKamatnaStapka.Create(Application);
  frmKamatnaStapka.ShowModal;
  frmKamatnaStapka.Free;
end;

procedure TFrmMain.aKarticaExecute(Sender: TObject);
begin
  frmKarticaKorisnik:=TfrmKarticaKorisnik.Create(self,true);
  frmKarticaKorisnik.ShowModal;
  frmKarticaKorisnik.Free;
end;

procedure TFrmMain.aKarticaFinExecute(Sender: TObject);
begin
  frmKarticaKorisnikFin:=TfrmKarticaKorisnikFin.Create(self,true);
  frmKarticaKorisnikFin.ShowModal;
  frmKarticaKorisnikFin.Free;
end;

procedure TFrmMain.aKNExecute(Sender: TObject);
begin
 frmKarticaKorisnikNova := TfrmKarticaKorisnikNova.Create(Self,False);
 frmKarticaKorisnikNova.ShowModal;
 frmKarticaKorisnikNova.Free;
end;

procedure TFrmMain.aKorekcijaExecute(Sender: TObject);
begin
   frmkorekcija := tfrmkorekcija.Create(self);
   frmKorekcija.ShowModal;
   frmKorekcija.free;
end;

procedure TFrmMain.aKorisnikExecute(Sender: TObject);
begin
  frmKorisniciPateka:=TfrmKorisniciPateka.Create(Application);
  frmKorisniciPateka.ShowModal;
  frmKorisniciPateka.Free;
end;

procedure TFrmMain.aOpomeniExecute(Sender: TObject);
begin
//    frmOpomena:=TfrmOpomena.Create(Application);
//    frmOpomena.ShowModal;
//    frmOpomena.Free;
end;

procedure TFrmMain.aPecatiDukaniExecute(Sender: TObject);
begin
   frmPecatiDukani:=TfrmPecatiDukani.Create(Application);
   frmPecatiDukani.ShowModal;
   frmPecatiDukani.Free;
end;

procedure TFrmMain.aPeriodExecute(Sender: TObject);
begin
   frmPeriod:=TfrmPeriod.Create(Application);
   frmPeriod.ShowModal;
   frmPeriod.Free;
end;

procedure TFrmMain.aPregledLokaciiExecute(Sender: TObject);
begin
   frmPregledLokacii:=TfrmPregledLokacii.Create(Self,'%');
   frmPregledLokacii.ShowModal;
   frmPregledLokacii.Free;
end;

procedure TfrmMain.aPrikaziExecute(Sender: TObject);
begin
  dm1.ShowReporter;
end;

procedure TFrmMain.aPromenaPodatociExecute(Sender: TObject);
begin
   frmPromenaPodatoci := TfrmPromenaPodatoci.Create(Self);
   frmPromenaPodatoci.ShowModal;
   frmPromenaPodatoci.Free;
end;

procedure TFrmMain.aSlikiVodomeriExecute(Sender: TObject);
begin
  frmSlikiVodomeri := TfrmSlikiVodomeri.Create(self);
  frmSlikiVodomeri.ShowModal;
  frmSlikiVodomeri.Free;
end;

procedure TFrmMain.aSnimiExecute(Sender: TObject);
begin
  aSnimiMeni.Execute;
end;

procedure TFrmMain.aSnimiMeniExecute(Sender: TObject);
begin
   dxBarManager1.SaveToIniFile('Komunalec.ini');
end;

procedure TFrmMain.aSpecifikacijaReoniExecute(Sender: TObject);
begin
//   dmReport.Spremi(18,dmKon.aplikacija);
//   dmReport.tblSqlReport.Open;
//   dmReport.frxReport1.ShowReport();
     frmIzborTP:=TfrmIzborTP.Create(Application);
     frmIzborTP.ShowModal;
     frmIzborTP.Free;
end;

procedure TfrmMain.dxBarMgrMainClickItem(Sender:TdxBarManager;
      ClickedItem:TdxBarItem);
begin
    if (ClickedItem.Description = 'reports') then
    begin
            dm1.ShowParam('KOM', ClickedItem.Tag);
    end;
end;

constructor TfrmMain.Create(Owner : TComponent; tip_partner: string);
begin
inherited Create(Owner);
    tip_partner := '%';
end;

end.
