object frmDopUsluga: TfrmDopUsluga
  Left = 183
  Top = 0
  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1091#1089#1083#1091#1075#1080
  ClientHeight = 453
  ClientWidth = 569
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 569
    Height = 233
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 23
      Width = 105
      Height = 18
      AutoSize = False
      Caption = #1050#1086#1088#1080#1089#1085#1080#1082
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 47
      Width = 105
      Height = 17
      AutoSize = False
      Caption = #1051#1086#1082#1072#1094#1080#1112#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 73
      Width = 105
      Height = 21
      AutoSize = False
      Caption = #1059#1089#1083#1091#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 99
      Width = 105
      Height = 21
      AutoSize = False
      Caption = #1055#1086#1095#1085#1091#1074#1072' '#1086#1076' '#1084#1077#1089#1077#1094
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 16
      Top = 124
      Width = 105
      Height = 21
      AutoSize = False
      Caption = #1041#1088'.'#1056#1072#1090#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 16
      Top = 151
      Width = 105
      Height = 21
      AutoSize = False
      Caption = #1058#1072#1088#1080#1092#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 16
      Top = 178
      Width = 105
      Height = 21
      AutoSize = False
      Caption = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 208
      Top = 97
      Width = 57
      Height = 21
      AutoSize = False
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object txtSifra: TcxDBTextEdit
      Tag = 1
      Left = 170
      Top = 19
      DataBinding.DataField = 'PARTNER'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 1
      OnExit = txtSifraExit
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object txtTipU: TcxDBTextEdit
      Tag = 1
      Left = 137
      Top = 71
      DataBinding.DataField = 'VID_USLUGA'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 4
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 41
    end
    object txtUsluga: TcxDBTextEdit
      Tag = 1
      Left = 177
      Top = 71
      DataBinding.DataField = 'USLUGA'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 5
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 49
    end
    object txtLokacija: TcxDBTextEdit
      Left = 137
      Top = 46
      DataBinding.DataField = 'LOKACIIN_ID'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 3
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 89
    end
    object txtVkRati: TcxDBTextEdit
      Tag = 1
      Left = 137
      Top = 124
      DataBinding.DataField = 'VK_RATI'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 10
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 89
    end
    object txtDanok: TcxDBTextEdit
      Tag = 1
      Left = 137
      Top = 151
      DataBinding.DataField = 'DANOK'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 11
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 89
    end
    object txtIznos: TcxDBTextEdit
      Tag = 1
      Left = 137
      Top = 178
      DataBinding.DataField = 'IZNOS_USLUGA_NETO'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 12
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 152
    end
    object cbUsluga: TcxDBLookupComboBox
      Left = 226
      Top = 71
      DataBinding.DataField = 'UslugaLookup'
      DataBinding.DataSource = DM1.dsDopUslugi
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      TabOrder = 6
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 214
    end
    object txtMesec: TcxDBTextEdit
      Tag = 1
      Left = 137
      Top = 97
      DataBinding.DataField = 'MESEC'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 7
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 41
    end
    object txtGodina: TcxDBTextEdit
      Tag = 1
      Left = 256
      Top = 96
      DataBinding.DataField = 'GODINA'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 8
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 89
    end
    object cbPartner: TcxDBLookupComboBox
      Left = 225
      Top = 19
      DataBinding.DataField = 'PARTNER'
      DataBinding.DataSource = DM1.dsDopUslugi
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'tip_partner'
        end
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end
        item
          FieldName = 'lokacija'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dsFizDukani
      TabOrder = 2
      OnExit = cbPartnerExit
      OnKeyDown = EnterKakoTab
      Width = 220
    end
    object txtID: TcxDBTextEdit
      Tag = 1
      Left = 256
      Top = 123
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 9
      Visible = False
      OnExit = AllKeyExit
      OnKeyDown = EnterKakoTab
      Width = 58
    end
    object txtTip: TcxDBTextEdit
      Tag = 1
      Left = 137
      Top = 19
      DataBinding.DataField = 'TIP_PARTNER'
      DataBinding.DataSource = DM1.dsDopUslugi
      TabOrder = 0
      OnExit = txtTipExit
      OnKeyDown = EnterKakoTab
      Width = 33
    end
    object cxButton1: TcxButton
      Left = 353
      Top = 192
      Width = 80
      Height = 25
      Action = aZapisi
      TabOrder = 13
    end
    object cxButton2: TcxButton
      Left = 439
      Top = 192
      Width = 75
      Height = 25
      Action = aIzlez
      TabOrder = 14
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 233
    Width = 569
    Height = 201
    Align = alClient
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 567
      Height = 199
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView3: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.dsRati
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1088#1072#1090#1080'>'
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView3BR_RATA: TcxGridDBColumn
          Caption = #1041#1088'.'#1056#1072#1090#1072
          DataBinding.FieldName = 'BR_RATA'
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
          Width = 109
        end
        object cxGrid1DBTableView3MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
          HeaderAlignmentHorz = taCenter
          Width = 128
        end
        object cxGrid1DBTableView3IZNOS_USLUGA: TcxGridDBColumn
          Caption = #1048#1079#1085#1086#1089
          DataBinding.FieldName = 'IZNOS_USLUGA'
          HeaderAlignmentHorz = taCenter
          Width = 180
        end
        object cxGrid1DBTableView3ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView3Column1: TcxGridDBColumn
          Caption = #1055#1083#1072#1090#1077#1085#1072' '#1088#1072#1090#1072
          DataBinding.FieldName = 'VO_PRESMETKA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1044#1040
              ImageIndex = 0
              Value = 1
            end
            item
              Description = #1053#1045
              Value = 0
            end>
          HeaderAlignmentHorz = taCenter
          Width = 111
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView3
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 434
    Width = 569
    Height = 19
    Panels = <
      item
        Text = 'F5 - '#1053#1086#1074',  F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F8 - '#1041#1088#1080#1096#1080', Esc - '#1054#1090#1082#1072#1079#1080'/'#1048#1079#1083#1077#1079
        Width = 50
      end>
  end
  object ActionList1: TActionList
    Left = 456
    Top = 40
    object aNov: TAction
      Caption = #1053#1086#1074#1072
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      OnExecute = aZapisiExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default'
      'Menus')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    PopupMenuLinks = <>
    Style = bmsFlat
    UseSystemFont = True
    Left = 416
    Top = 40
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Main Menu'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 393
      FloatTop = 299
      FloatClientWidth = 49
      FloatClientHeight = 22
      ItemLinks = <>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object N1: TdxBarButton
      Caption = #1040#1082#1094#1080#1080
      Category = 1
      Visible = ivAlways
    end
  end
  object tblFizDukani: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    p.tip_partner,'
      '    p.id,'
      '    p.naziv,'
      '    l.id lokacija'
      'FROM mat_partner p'
      
        '    left join kom_lokacija_arhiva kla on p.id=kla.partner and p.' +
        'tip_partner=kla.tip_partner and kla.kraj_mesec is null'
      
        '    inner join kom_lokacii l on p.tip_partner = l.tip_partner an' +
        'd p.id = l.partner_id --kla.id_lokacija=l.id'
      'WHERE p.TIP_PARTNER in (6,1,5) and p.tip_partner like :tip')
    SelectSQL.Strings = (
      'select'
      '    p.tip_partner,'
      '    p.id,'
      '    p.naziv,'
      '    l.id lokacija'
      'FROM mat_partner p'
      
        '    left join kom_lokacija_arhiva kla on p.id=kla.partner and p.' +
        'tip_partner=kla.tip_partner and kla.kraj_mesec is null'
      
        '    inner join kom_lokacii l on p.tip_partner = l.tip_partner an' +
        'd p.id = l.partner_id --kla.id_lokacija=l.id'
      'WHERE p.TIP_PARTNER in (6,1,5) and p.tip_partner like :tip')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 376
    Top = 120
    object tblFizDukaniTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      DisplayWidth = 5
      FieldName = 'TIP_PARTNER'
    end
    object tblFizDukaniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblFizDukaniNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      DisplayWidth = 40
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblFizDukaniLOKACIJA: TFIBIntegerField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072
      DisplayWidth = 10
      FieldName = 'LOKACIJA'
    end
  end
  object dsFizDukani: TDataSource
    DataSet = tblFizDukani
    Left = 464
    Top = 120
  end
  object qBrisiRati: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete'
      'from kom_dopolnitelna_usluga du1'
      'where du1.id=:MAS_ID and du1.br_rata<>0')
    BeforeExecute = qBrisiRatiBeforeExecute
    Left = 416
    Top = 120
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qProveriDU: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'Select (Case'
      '          When Sum(du1.vo_presmetka) >= 1 Then 1'
      '          Else 0'
      '        End) ima'
      'From kom_dopolnitelna_usluga du1'
      'Where du1.id = :mas_id And'
      '      du1.br_rata <> 0   ')
    Left = 416
    Top = 72
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
