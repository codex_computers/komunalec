object frmEFakturi: TfrmEFakturi
  Left = 0
  Top = 0
  Caption = 'frmEFakturi'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 299
    Align = alClient
    TabOrder = 0
  end
  object actnlst1: TActionList
    Left = 448
    Top = 80
    object aEFakturi: TAction
      Caption = 'aEFakturi'
      ShortCut = 16449
      OnExecute = aEFakturiExecute
    end
    object aEFakturiDizajn: TAction
      Caption = 'aEFakturiDizajn'
      ShortCut = 24641
      OnExecute = aEFakturiDizajnExecute
    end
  end
end
