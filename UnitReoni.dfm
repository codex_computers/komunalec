inherited FrmReoni: TFrmReoni
  Caption = #1056#1077#1086#1085#1080
  ClientHeight = 577
  ClientWidth = 618
  ExplicitWidth = 634
  ExplicitHeight = 616
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 618
    Height = 244
    ExplicitWidth = 618
    ExplicitHeight = 244
    inherited cxGrid1: TcxGrid
      Width = 614
      Height = 240
      ExplicitWidth = 614
      ExplicitHeight = 240
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        OnKeyDown = cxGrid1DBTableView1KeyDown
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = DM1.DSKomReoni
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 431
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 370
    Width = 618
    Height = 184
    ExplicitTop = 370
    ExplicitWidth = 618
    ExplicitHeight = 184
    inherited Label1: TLabel
      Left = 7
      Top = 27
      Width = 43
      BiDiMode = bdRightToLeft
      Caption = #1064#1080#1092#1088#1072' '
      ParentBiDiMode = False
      ExplicitLeft = 7
      ExplicitTop = 27
      ExplicitWidth = 43
    end
    object lblNAZIV: TLabel [1]
      Left = 10
      Top = 54
      Width = 48
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1053#1072#1079#1080#1074
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 74
      Top = 24
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.DSKomReoni
      ExplicitLeft = 74
      ExplicitTop = 24
    end
    inherited OtkaziButton: TcxButton
      Left = 516
      Top = 143
      TabOrder = 6
      ExplicitLeft = 516
      ExplicitTop = 143
    end
    inherited ZapisiButton: TcxButton
      Left = 435
      Top = 143
      TabOrder = 5
      ExplicitLeft = 435
      ExplicitTop = 143
    end
    object cxControlNAZIV: TcxDBTextEdit
      Left = 74
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = DM1.DSKomReoni
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 277
    end
    object cxLabel8: TcxLabel
      Left = 5
      Top = 79
      Caption = #1048#1085#1082#1072#1089#1072#1090#1086#1088
      Style.TextColor = clNavy
      Style.TextStyle = [fsBold]
      Properties.Alignment.Horz = taRightJustify
      Properties.WordWrap = True
      Transparent = True
      Width = 67
      AnchorX = 72
    end
    object txtTipInkasant: TcxDBTextEdit
      Tag = 1
      Left = 74
      Top = 78
      DataBinding.DataField = 'TIP_INKASATOR'
      DataBinding.DataSource = DM1.DSKomReoni
      ParentFont = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 47
    end
    object txtInkasant: TcxDBTextEdit
      Tag = 1
      Left = 120
      Top = 78
      DataBinding.DataField = 'ID_INKASATOR'
      DataBinding.DataSource = DM1.DSKomReoni
      ParentFont = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 78
    end
    object cbInkasant: TcxLookupComboBox
      Left = 198
      Top = 78
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1048#1085#1082#1072#1089#1072#1090#1086#1088
      Properties.ClearKey = 46
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'id'
      Properties.ListColumns = <
        item
          Width = 40
          FieldName = 'TIP_PARTNER'
        end
        item
          Width = 60
          FieldName = 'id'
        end
        item
          Width = 170
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dsInkasant
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 293
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 618
    ExplicitWidth = 618
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 554
    Width = 618
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080',F9 - '#1047#1072#1087#1080#1096#1080', F1' +
          '0 - '#1055#1077#1095#1072#1090#1080', ENTER/DbClick - '#1059#1083#1080#1094#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
    ExplicitTop = 554
    ExplicitWidth = 618
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 552
    Top = 400
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.442027129630000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object tblInkasant: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    TIP_PARTNER,'
      '    tip_partner||'#39'000'#39'||ID id,'
      '    NAZIV'
      'FROM'
      '    MAT_PARTNER '
      'where(  status = 5'
      '     ) and (     MAT_PARTNER.TIP_PARTNER = :OLD_TIP_PARTNER'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    TIP_PARTNER,'
      '    tip_partner||'#39'000'#39'||ID id,'
      '    NAZIV'
      'FROM'
      '    MAT_PARTNER '
      'where status = 5')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 512
    Top = 432
    object tblInkasantTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblInkasantNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblInkasantID: TFIBStringField
      FieldName = 'ID'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsInkasant: TDataSource
    DataSet = tblInkasant
    Left = 560
    Top = 432
  end
end
