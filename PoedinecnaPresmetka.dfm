object frmPoedinecnaPresmetka: TfrmPoedinecnaPresmetka
  Left = 0
  Top = 0
  Caption = #1055#1086#1077#1076#1080#1085#1077#1095#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
  ClientHeight = 298
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 645
    Height = 144
    Align = alTop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 18
      Width = 107
      Height = 18
      AutoSize = False
      Caption = #1047#1072' '#1050#1086#1088#1080#1089#1085#1080#1082#1086#1090
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMesec: TLabel
      Left = 16
      Top = 93
      Width = 36
      Height = 14
      Caption = #1052#1077#1089#1077#1094
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblGodina: TLabel
      Left = 16
      Top = 69
      Width = 40
      Height = 14
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxTxtPartnerID: TcxTextEdit
      Left = 129
      Top = 15
      BeepOnEnter = False
      Enabled = False
      Style.BorderStyle = ebsNone
      Style.Color = clBtnFace
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      TabOrder = 0
      Width = 60
    end
    object cxTxtPartnerNaziv: TcxTextEdit
      Left = 211
      Top = 15
      Enabled = False
      Properties.ReadOnly = True
      Style.BorderStyle = ebsNone
      Style.Color = clBtnFace
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      TabOrder = 1
      Width = 253
    end
    object cxComboBoxGodini: TcxComboBox
      Tag = 1
      Left = 80
      Top = 65
      BeepOnEnter = False
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 94
    end
    object cxComboBoxMesec: TcxComboBox
      Tag = 1
      Left = 80
      Top = 89
      BeepOnEnter = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
      TabOrder = 3
      OnKeyDown = EnterKakoTab
      Width = 94
    end
    object cxBtnPresmetaj: TcxButton
      Left = 504
      Top = 89
      Width = 121
      Height = 20
      Caption = #1055#1088#1077#1089#1084#1077#1090#1072#1112
      TabOrder = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = cxBtnPresmetajClick
    end
    object cxLookUpComboBoxTipPartner: TcxTextEdit
      Left = 96
      Top = 15
      Enabled = False
      Style.BorderStyle = ebsNone
      Style.Color = clBtnFace
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      TabOrder = 5
      Visible = False
      Width = 49
    end
    object cxtxtLokacija: TcxTextEdit
      Left = 14
      Top = 42
      BeepOnEnter = False
      Enabled = False
      Style.BorderStyle = ebsNone
      Style.Color = clBtnFace
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      TabOrder = 6
      Visible = False
      Width = 60
    end
    object cxtxtReon: TcxTextEdit
      Left = 114
      Top = 42
      BeepOnEnter = False
      Enabled = False
      Style.BorderStyle = ebsNone
      Style.Color = clBtnFace
      Style.Shadow = False
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      TabOrder = 7
      Visible = False
      Width = 60
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 144
    Width = 645
    Height = 135
    Align = alClient
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 643
      Height = 133
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView3: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DSPresmetka
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView3ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3LOKACIIN_ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'LOKACIIN_ID'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3IZNOS_VKUPNO: TcxGridDBColumn
          Caption = #1048#1079#1085#1086#1089' '#1074#1082#1091#1087#1085#1086
          DataBinding.FieldName = 'IZNOS_VKUPNO'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3DATUM_PRESMETKA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
          DataBinding.FieldName = 'DATUM_PRESMETKA'
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'VRABOTEN'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3BROJ_FAKTURA: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
          DataBinding.FieldName = 'BROJ_FAKTURA'
          HeaderAlignmentHorz = taCenter
          Width = 131
        end
        object cxGrid1DBTableView3IZNOS_PLATENO: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_PLATENO'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3TIP_NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_NALOG'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3TS_KNIZENJE: TcxGridDBColumn
          DataBinding.FieldName = 'TS_KNIZENJE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView3TIP: TcxGridDBColumn
          Caption = #1058#1080#1087
          DataBinding.FieldName = 'TIP'
          HeaderAlignmentHorz = taCenter
          Width = 71
        end
      end
      object cxGrid1DBTableView4: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DSPresmetka1
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView4LOKACIIN_ID: TcxGridDBColumn
          DataBinding.FieldName = 'LOKACIIN_ID'
        end
        object cxGrid1DBTableView4MESEC: TcxGridDBColumn
          DataBinding.FieldName = 'MESEC'
        end
        object cxGrid1DBTableView4GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
        end
        object cxGrid1DBTableView4VID_USLUGA: TcxGridDBColumn
          DataBinding.FieldName = 'VID_USLUGA'
        end
        object cxGrid1DBTableView4USLUGA: TcxGridDBColumn
          DataBinding.FieldName = 'USLUGA'
        end
        object cxGrid1DBTableView4IZNOS_USLUGA_NETO: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_USLUGA_NETO'
        end
        object cxGrid1DBTableView4IZNOS_USLUGA: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_USLUGA'
        end
        object cxGrid1DBTableView4DATUM_PRESMETKA: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_PRESMETKA'
        end
        object cxGrid1DBTableView4VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'VRABOTEN'
        end
        object cxGrid1DBTableView4DANOK: TcxGridDBColumn
          DataBinding.FieldName = 'DANOK'
        end
        object cxGrid1DBTableView4CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
        end
        object cxGrid1DBTableView4ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView4PRESMETKA_G_ID: TcxGridDBColumn
          DataBinding.FieldName = 'PRESMETKA_G_ID'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView3
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 279
    Width = 645
    Height = 19
    Panels = <>
  end
  object TblPresmetkaS: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT *'
      'FROM KOM_PRESMETKA_S S'
      'where s.presmetka_g_id=:MAS_ID')
    AutoUpdateOptions.UpdateTableName = 'KOM_PRESMETKA_S'
    AutoUpdateOptions.KeyFields = 'id;presmetka_g_id'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = DSPresmetka
    Left = 320
    Top = 41
    object TblPresmetkaSLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblPresmetkaSMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object TblPresmetkaSGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object TblPresmetkaSVID_USLUGA: TFIBIntegerField
      FieldName = 'VID_USLUGA'
    end
    object TblPresmetkaSUSLUGA: TFIBIntegerField
      FieldName = 'USLUGA'
    end
    object TblPresmetkaSIZNOS_USLUGA_NETO: TFIBBCDField
      FieldName = 'IZNOS_USLUGA_NETO'
      Size = 3
    end
    object TblPresmetkaSIZNOS_USLUGA: TFIBBCDField
      FieldName = 'IZNOS_USLUGA'
      Size = 3
    end
    object TblPresmetkaSDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object TblPresmetkaSVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaSDANOK: TFIBBCDField
      FieldName = 'DANOK'
      Size = 3
    end
    object TblPresmetkaSCENA: TFIBBCDField
      FieldName = 'CENA'
      Size = 2
    end
    object TblPresmetkaSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TblPresmetkaSPRESMETKA_G_ID: TFIBIntegerField
      FieldName = 'PRESMETKA_G_ID'
    end
  end
  object DSPresmetka1: TDataSource
    DataSet = TblPresmetkaS
    Left = 360
    Top = 41
  end
  object DSPresmetka: TDataSource
    DataSet = TblPresmetkaG
    Left = 360
    Top = 9
  end
  object TblPresmetkaG: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    g.id,'
      '    g.lokaciin_id,'
      '    g.mesec,'
      '    g.godina,'
      '    cast(round(g.iznos_vkupno) as numeric(15,2)) iznos_vkupno,'
      '    g.datum_presmetka,'
      '    g.vraboten,'
      '    g.broj_faktura,'
      '    g.iznos_plateno,'
      '    g.re,'
      '    g.tip_nalog,'
      '    g.nalog,'
      '    g.ts_knizenje,'
      '    g.tip'
      'from kom_presmetka_g g'
      'where(  g.lokaciin_id=:l and g.mesec=:m and g.godina=:g'
      'and g.tip=1'
      '     ) and (     G.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select'
      '    g.id,'
      '    g.lokaciin_id,'
      '    g.mesec,'
      '    g.godina,'
      '    cast(round(g.iznos_vkupno) as numeric(15,2)) iznos_vkupno,'
      '    g.datum_presmetka,'
      '    g.vraboten,'
      '    g.broj_faktura,'
      '    g.iznos_plateno,'
      '    g.re,'
      '    g.tip_nalog,'
      '    g.nalog,'
      '    g.ts_knizenje,'
      '    g.tip'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l and g.mesec=:m and g.godina=:g'
      'and g.tip=1'
      '')
    AutoUpdateOptions.UpdateTableName = 'KOM_PRESMETKA_G'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 320
    Top = 9
    object TblPresmetkaGID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TblPresmetkaGLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblPresmetkaGMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object TblPresmetkaGGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object TblPresmetkaGIZNOS_VKUPNO: TFIBBCDField
      FieldName = 'IZNOS_VKUPNO'
      Size = 2
    end
    object TblPresmetkaGDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object TblPresmetkaGVRABOTEN: TFIBStringField
      FieldName = 'VRABOTEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGIZNOS_PLATENO: TFIBBCDField
      FieldName = 'IZNOS_PLATENO'
      Size = 2
    end
    object TblPresmetkaGRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object TblPresmetkaGTIP_NALOG: TFIBStringField
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblPresmetkaGNALOG: TFIBIntegerField
      FieldName = 'NALOG'
    end
    object TblPresmetkaGTS_KNIZENJE: TFIBDateTimeField
      FieldName = 'TS_KNIZENJE'
    end
    object TblPresmetkaGTIP: TFIBIntegerField
      FieldName = 'TIP'
    end
  end
  object qMaks: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select gen_id(gen_kom_presmetka_g_id,1) id, cast(coalesce(max(su' +
        'bstr(g.broj_faktura, strlen(g.broj_faktura),strlen(g.broj_faktur' +
        'a))),0) as integer) maks'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l'
      'and g.mesec=:m'
      'and g.godina=:g'
      'and g.tip=1')
    Left = 398
    Top = 8
  end
  object pFIBQuery1: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select gen_id(gen_kom_presmetka_g_id,1) id, cast(coalesce(max(su' +
        'bstr(g.broj_faktura, strlen(g.broj_faktura),strlen(g.broj_faktur' +
        'a))),0) as integer) maks'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l'
      'and g.mesec=:m'
      'and g.godina=:g'
      'and g.tip=1')
    Left = 398
    Top = 40
  end
  object qGen: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select gen_id(gen_kom_presmetka_g_id,1) id, cast(coalesce(max(su' +
        'bstr(g.broj_faktura, strlen(g.broj_faktura),strlen(g.broj_faktur' +
        'a))),0) as integer) maks'
      'from kom_presmetka_g g'
      'where g.lokaciin_id=:l'
      'and g.mesec=:m'
      'and g.godina=:g'
      'and g.tip=1')
    Left = 398
    Top = 72
  end
  object qIdKorekcija: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1'
      '    g.datum_presmetka,'
      '    g.id_korekcija'
      'from kom_presmetka_g g'
      'where g.lokaciin_id = :lokacija'
      '  and g.mesec = :mesec'
      '  and g.godina = :godina'
      '  and g.tip = 2'
      'order by g.datum_presmetka desc')
    Left = 446
    Top = 8
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
