object frmPromena: TfrmPromena
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072' '
  ClientHeight = 299
  ClientWidth = 407
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 407
    Height = 280
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 108
      Width = 241
      Height = 18
      AutoSize = False
      Caption = #1057#1090#1072#1074#1080' '#1112#1072' '#1087#1088#1077#1076' '#1083#1086#1082#1072#1094#1080#1112#1072#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 16
      Top = 167
      Width = 58
      Height = 18
      AutoSize = False
      Caption = #1063#1080#1090'.'#1082#1085#1080#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object lblCitackaKniga: TLabel
      Left = 13
      Top = 40
      Width = 75
      Height = 13
      Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
    end
    object lblRedenBrojOld: TLabel
      Left = 13
      Top = 59
      Width = 83
      Height = 13
      Caption = #1057#1090#1072#1088' '#1088#1077#1076#1077#1085' '#1073#1088#1086'j'
    end
    object Label2: TLabel
      Left = 13
      Top = 19
      Width = 46
      Height = 13
      Caption = #1051#1086#1082#1072#1094#1080#1112#1072
    end
    object Label3: TLabel
      Left = 14
      Top = 89
      Width = 379
      Height = 1
      AutoSize = False
      Caption = 'Label3'
      Color = clMedGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label5: TLabel
      Left = 122
      Top = 167
      Width = 79
      Height = 18
      AutoSize = False
      Caption = #1063#1080#1090'.'#1082#1085#1080#1075#1072' '#1056#1041
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object cxLookUpComboBoxTipPartner: TcxTextEdit
      Left = 13
      Top = 129
      TabOrder = 0
      OnExit = cxLookUpComboBoxTipPartnerExit
      OnKeyDown = EnterKakoTab
      Width = 49
    end
    object cxTxtPartnerID: TcxTextEdit
      Left = 62
      Top = 129
      BeepOnEnter = False
      TabOrder = 1
      OnExit = cxTxtPartnerIDExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cxTxtPartnerNaziv: TcxTextEdit
      Left = 122
      Top = 129
      Properties.ReadOnly = True
      TabOrder = 2
      OnEnter = cxTxtPartnerNazivEnter
      OnExit = cxTxtPartnerNazivExit
      OnKeyDown = EnterKakoTab
      Width = 239
    end
    object txtCitKniga: TcxTextEdit
      Left = 13
      Top = 184
      Enabled = False
      ParentFont = False
      Style.BorderColor = clWindowText
      Style.BorderStyle = ebsNone
      Style.Color = clInactiveBorder
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNone
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.Shadow = False
      Style.TextStyle = []
      Style.IsFontAssigned = True
      StyleDisabled.TextColor = clWindowText
      StyleDisabled.TextStyle = [fsBold]
      TabOrder = 3
      Width = 58
    end
    object cxTxtCitackaKniga: TcxTextEdit
      Left = 102
      Top = 33
      Enabled = False
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 4
      Width = 49
    end
    object cxTxtRedenBrojOld: TcxTextEdit
      Left = 102
      Top = 57
      Enabled = False
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 5
      Width = 49
    end
    object cxtxtNaziv: TcxTextEdit
      Left = 102
      Top = 16
      Enabled = False
      ParentFont = False
      Style.BorderStyle = ebsNone
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 6
      Width = 219
    end
    object txtCitKnRb: TcxTextEdit
      Left = 122
      Top = 184
      Enabled = False
      ParentFont = False
      Style.BorderColor = clWindowText
      Style.BorderStyle = ebsNone
      Style.Color = clInactiveBorder
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNone
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.Shadow = False
      Style.TextStyle = []
      Style.IsFontAssigned = True
      StyleDisabled.TextColor = clWindowText
      StyleDisabled.TextStyle = [fsBold]
      TabOrder = 7
      Width = 63
    end
    object cxButton1: TcxButton
      Left = 224
      Top = 232
      Width = 75
      Height = 25
      Action = aPromeni
      TabOrder = 8
    end
    object cxButton2: TcxButton
      Left = 318
      Top = 232
      Width = 75
      Height = 25
      Action = aOtkazi
      TabOrder = 9
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 280
    Width = 407
    Height = 19
    Panels = <>
  end
  object ActionList1: TActionList
    Left = 288
    Top = 16
    object aPromeni: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080
      OnExecute = aPromeniExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      OnExecute = aOtkaziExecute
    end
  end
  object IdIcmpClient1: TIdIcmpClient
    Host = 'www.google.com'
    Protocol = 1
    ProtocolIPv6 = 0
    IPVersion = Id_IPv4
    PacketSize = 1024
    Left = 448
    Top = 216
  end
  object qCitackKniga: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'SELECT  l.citacka_kniga'
      'FROM kom_lokacii l'
      'WHERE'
      '   l.id = :lokacija')
    Left = 656
    Top = 120
    qoStartTransaction = True
  end
end
