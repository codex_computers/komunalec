unit UnitIzberiFaktura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, FIBDataSet, pFIBDataSet, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels,
  cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxBar, dxPSCore, dxPScxCommon, //dxPScxGrid6Lnk,
  cxBarEditItem, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSpringTime, dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint;

type
  TfrmIzberiFaktura = class(TfrmMaster)
    dsIzberiFaktura: TDataSource;
    tblIzbriFaktura: TpFIBDataSet;
    tblIzbriFakturaBROJ_FAKTURA: TFIBStringField;
    tblIzbriFakturaLOKACIIN_ID: TFIBIntegerField;
    tblIzbriFakturaDATUM_PRESMETKA: TFIBDateTimeField;
    tblIzbriFakturaGODINA: TFIBIntegerField;
    tblIzbriFakturaMESEC: TFIBIntegerField;
    tblIzbriFakturaIZNOS_VKUPNO: TFIBIntegerField;
    tblIzbriFakturaPLATENO: TFIBBCDField;
    tblIzbriFakturaRAZLIKA: TFIBBCDField;
    cxGrid1DBTableView1BROJ_FAKTURA: TcxGridDBColumn;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn;
    cxGrid1DBTableView1PLATENO: TcxGridDBColumn;
    cxGrid1DBTableView1RAZLIKA: TcxGridDBColumn;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure prefrli;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzberiFaktura: TfrmIzberiFaktura;

implementation

uses dmKonekcija, UnitDM1, Utils;

{$R *.dfm}

procedure TfrmIzberiFaktura.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  //inherited;
      if(Ord(Key) = VK_RETURN) then
     //   prefrli
        ModalResult := mrOk
  else if (Ord(Key) <> VK_RETURN) then
       cxSortiraj(cxGrid1DBTableView1);

end;

procedure TfrmIzberiFaktura.FormCreate(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.Controller.GoToFirst;
  cxGrid1DBTableView1.Controller.FocusedColumnIndex:=1;
end;

procedure TfrmIzberiFaktura.prefrli;
begin
  inherited;
//  if(not inserting) then
//    begin
//        setVrednost(tblIzbriFakturaLOKACIIN_ID.Value,tblIzbriFakturaMESEC.Value,tblIzbriFakturaGODINA.Value);
//        ModalResult := mrOk;
//    end;
end;

procedure TfrmIzberiFaktura.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  prefrli;
end;

end.
