unit uReporter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Provider, SqlExpr, cxContainer, cxEdit, FR_Shape, FR_ChBox,
  FR_Rich, FR_DSet, FR_DCtrl, FR_Desgn, DB, DBClient, DBLocal, DBLocalS,
  FR_DBSet, FR_Class, Menus, StdCtrls, cxCheckBox, cxDBEdit, DBCtrls,
  cxControls, cxTextEdit, cxMemo, ComCtrls, dxtree, dxdbtree, ExtCtrls,
  cxLookAndFeelPainters, cxButtons, ActnList, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, FMTBcd, frOLEExl,
  frexpimg, frRtfExp, FR_Cross, FR_RRect, FR_Chart, FR_BarC;

type

  TfReporter = class(TForm)
    Panel1: TPanel;
    dxDBTreeView1: TdxDBTreeView;
    pDizajn: TPanel;
    Panel2: TPanel;
    mUslovi: TcxDBMemo;
    Panel4: TPanel;
    cxDBMemo1: TcxDBMemo;
    Panel5: TPanel;
    eBroj: TcxDBTextEdit;
    eNaslov: TcxDBTextEdit;
    eKoren: TcxDBTextEdit;
    Panel6: TPanel;
    DBNavigator1: TDBNavigator;
    eUslov: TcxDBTextEdit;
    eUslovi: TcxDBCheckBox;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    dsReport: TDataSource;
    ppMeni: TPopupMenu;
    mDizajn: TMenuItem;
    mPregled: TMenuItem;
    frRep: TfrReport;
    frdsRep: TfrDBDataSet;
    dsp: TDataSource;
    tReport: TSQLClientDataSet;
    tReportID: TIntegerField;
    tReportKOREN: TIntegerField;
    tReportNASLOV: TStringField;
    tReportSQL: TBlobField;
    tReportDATA: TBlobField;
    tReportXML: TBlobField;
    tReportUSLOVI: TBlobField;
    tReportNASLOVI: TBlobField;
    tReportVIDLIVOST: TStringField;
    tReportPOJAVIUSLOVI: TIntegerField;
    tReportUSLOVITEXT: TBlobField;
    tReportDINAMICKIUSLOVI: TBlobField;
    qp: TSQLClientDataSet;
    frDesigner1: TfrDesigner;
    frDialogControls1: TfrDialogControls;
    frUserDataset1: TfrUserDataset;
    frRichObject1: TfrRichObject;
    frCheckBoxObject1: TfrCheckBoxObject;
    frShapeObject1: TfrShapeObject;
    StyleControllerLookUp: TcxEditStyleController;
    StyleController: TcxEditStyleController;
    btnVidlivost: TcxButton;
    btnPregled: TcxButton;
    btnDizajn: TcxButton;
    ActionList1: TActionList;
    aVidlivost: TAction;
    aPregled: TAction;
    aDizajn: TAction;
    N1: TMenuItem;
    tReportTABELA_GRUPA: TStringField;
    tReportBR: TIntegerField;
    eGrupa: TcxDBLookupComboBox;
    eBR: TcxDBTextEdit;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    qryMaxBroj: TSQLQuery;
    qryMaxBrojMAKS: TIntegerField;
    qryNajdiSifra: TSQLQuery;
    qryNajdiSifraID: TIntegerField;
    frJPEGExport1: TfrJPEGExport;
    frOLEExcelExport1: TfrOLEExcelExport;
    frRtfAdvExport1: TfrRtfAdvExport;
    frBarCodeObject1: TfrBarCodeObject;
    frChartObject1: TfrChartObject;
    frRoundRectObject1: TfrRoundRectObject;
    frCrossObject1: TfrCrossObject;
//    procedure FormCreate(Sender: TObject);overload;
    constructor Create(Owner : TComponent); overload;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure dxDBTreeView1KeyPress(Sender: TObject; var Key: Char);
    procedure aVidlivostExecute(Sender: TObject);
    procedure aPregledExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eBrojKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eGrupaExit(Sender: TObject);
  private
    parametri : AnsiString;
    tblgrp : AnsiString;
    procedure sqlPrai;
    procedure rep( gol:integer; diz:integer; broj:integer; usl:AnsiString);
    function  NajdiBroj(grupa:AnsiString;broj:integer):Integer;
    { Private declarations }
  public
    constructor Create(Owner : TComponent; tblgrupa :AnsiString); overload;
//    procedure FormCreate(Sender: TObject; tblgrupa : AnsiString);overload;
    procedure Dizajner;overload;
    procedure Dizajner(grupa:AnsiString);overload;
    procedure Reporter;overload;
    procedure Reporter(flt:AnsiString);overload;
    procedure setParametri(param:AnsiString);
    procedure Pregled(broj:integer;usl:AnsiString);overload;
    procedure Dizajn(broj:integer;usl:AnsiString);overload;
    procedure Pecati(broj:integer;usl:AnsiString);overload;

    procedure Pregled(grupa:AnsiString;broj:integer;usl:AnsiString);overload;
    procedure Dizajn(grupa:AnsiString;broj:integer;usl:AnsiString);overload;
    procedure Pecati(grupa:AnsiString;broj:integer;usl:AnsiString);overload;

    procedure Spremaj(broj:integer);overload;
    procedure Spremaj(grupa:AnsiString;broj:integer);overload;
    procedure Param(par_ime:AnsiString;par:Variant);
    procedure Show;overload;
    procedure Design;

    { Public declarations }
  end;

var
  fReporter: TfReporter;

implementation

uses uUslovi, dmKonekcija, dmMaticni;

{$R *.dfm}
//-*----------------------------------------------------------------------------
constructor TfReporter.Create(Owner : TComponent; tblgrupa :AnsiString);
begin
inherited Create(Owner);
    if(dmKon.RepciOdPateka) then
    begin
         tReport.DBConnection := dmKon.conPateka;
         qryMaxBroj.SQLConnection := dmKon.conPateka;
         qryNajdiSifra.SQLConnection := dmKon.conPateka;
    end
    else
    begin
         tReport.DBConnection := dmKon.Connection;
         qryMaxBroj.SQLConnection := dmKon.Connection;
         qryNajdiSifra.SQLConnection := dmKon.Connection;
    end;
    parametri := '';
    if(tblgrupa = '') then
        tblgrp := '%'
    else
        tblgrp := tblgrupa;
    fuslovi:=TfUslovi.create(self);

end;
//-*----------------------------------------------------------------------------
constructor TfReporter.Create(Owner : TComponent);
begin
inherited Create(Owner);
    parametri := '';
    tblgrp := '%';
    fuslovi:=TfUslovi.create(self);

end;
//-*----------------------------------------------------------------------------
procedure TfReporter.sqlPrai;
var
    pos:integer;
    tt:AnsiString;
begin

    if(tReportPOJAVIUSLOVI.AsInteger<>0) then
    begin
        fUslovi.putVidlivost(tReportVIDLIVOST.AsString);
        fUslovi.ShowModal;
        tReport.Edit;
        tReportDINAMICKIUSLOVI.AsString:=fUslovi.Uslovi;
        tReportUSLOVITEXT.AsString:=fUslovi.NaslovText;
    end
    else
        fUslovi.Reset;
    qp.Close;
    tt:=tReportSQL.AsString;
    qp.CommandText:='';
    pos:=AnsiPos('/***/',tt);
    if(pos<>0) then
        Insert(mUslovi.Lines.Text,tt,pos);
    pos:=AnsiPos('/***/',tt);
    if(pos<>0) then
        Insert(tReportDINAMICKIUSLOVI.AsString,tt,pos);

    qp.CommandText:=tt;
    qp.Open;
end;
//-*----------------------------------------------------------------------------
  procedure TfReporter.rep( gol:integer; diz:integer; broj:integer; usl:AnsiString);
  begin
   tReport.Params.ParamByName('tg').Value := tblgrp;
   tReport.Open;         // ������
   tReport.Filter:='ID='+inttostr(broj) ;
   tReport.Filtered:=true;

   tReport.Edit;
   if not(gol=0) then
   begin
      tReportDINAMICKIUSLOVI.AsString:=usl;
      sqlPrai;
      qp.Open;
   end;
   frRep.LoadFromBlobField(tReportDATA);

   case diz of

      0:
        begin
            if not(parametri = '') then
                frRep.Dictionary.Variables.Value[1] := parametri;
            frRep.ShowReport;
        end;
      1:
         frRep.DesignReport;
      2: begin
            if not(parametri = '') then
                frRep.Dictionary.Variables.Value[1] := parametri;      
              frRep.PrepareReport;
              frRep.PrintPreparedReport('',1,true,TfrPrintPages(frAll));
         end;

     end;
   if not(gol=0) then
   begin
      tReport.Edit;
      frRep.SaveToBlobField(tReport.FieldByName('DATA'));
      qp.Close;
   end;
   tReport.ApplyUpdates(0);
   tReport.Close;

  end;
//-*----------------------------------------------------------------------------
  procedure TfReporter.setParametri(param:AnsiString);
  begin
    parametri := param;
  end;
//-*----------------------------------------------------------------------------
  procedure TfReporter.Dizajner;
  begin
       pDizajn.Visible:=true;
       tReport.Filtered:=false;
       Width:=600;
       ShowModal;
  end;
//-*----------------------------------------------------------------------------
  procedure TfReporter.Dizajner(grupa:AnsiString);
  begin
       pDizajn.Visible:=true;
       tReport.Filtered:=false;
       Width:=600;
       ShowModal;
  end;
//-*----------------------------------------------------------------------------
  procedure TfReporter.Reporter;
  begin
       pDizajn.Visible:=false;
       tReport.Filtered:=false;
       Width:=300;
       ShowModal;
  end;
//-*----------------------------------------------------------------------------
  procedure TfReporter.Reporter(flt:AnsiString);
  begin
       pDizajn.Visible:=false;
       tReport.Filter:=flt;
       tReport.Filtered:=true;
       Width:=300;
       ShowModal;
  end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Pregled(broj:integer;usl:AnsiString);
begin
     if usl<>'' then  rep(0,0,broj,usl)
     else rep(1,0,broj,'');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Dizajn(broj:integer;usl:AnsiString); //{rep(0,1,broj,usl);};
begin
     if usl<>'' then  rep(0,1,broj,usl)
     else rep(1,1,broj,'');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Pecati(broj:integer;usl:AnsiString);//{rep(0,2,broj,usl);};
begin
     if usl<>'' then  rep(0,2,broj,usl)
     else rep(1,2,broj,'');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Pregled(grupa:AnsiString;broj:integer;usl:AnsiString);
var
    br : Integer;
begin
     br := NajdiBroj(grupa,broj);
     if usl<>'' then  rep(0,0,br,usl)
     else rep(1,0,br,'');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Dizajn(grupa:AnsiString;broj:integer;usl:AnsiString); //{rep(0,1,broj,usl);};
var
    br : Integer;
begin
     br := NajdiBroj(grupa,broj);
     if usl<>'' then  rep(0,1,br,usl)
     else rep(1,1,br,'');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Pecati(grupa:AnsiString;broj:integer;usl:AnsiString);//{rep(0,2,broj,usl);};
var
    br : Integer;
begin
     br := NajdiBroj(grupa,broj);
     if usl<>'' then  rep(0,2,br,usl)
     else rep(1,2,br,'');
end;
//-*----------------------------------------------------------------------------
{
procedure TfReporter.FormCreate(Sender: TObject);
begin
     parametri := '';
     tblgrp := '%';
     fuslovi:=TfUslovi.create(self);
end;
//-*----------------------------------------------------------------------------
{
procedure TfReporter.FormCreate(Sender: TObject; tblgrupa : AnsiString);
begin
     parametri := '';
     tblgrp := tblgrupa;
     fuslovi:=TfUslovi.create(self);
end;
//-*----------------------------------------------------------------------------
}
procedure TfReporter.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     tReport.ApplyUpdates(0);
     tReport.Close;
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.FormShow(Sender: TObject);
begin
     tReport.Params.ParamByName('tg').Value := tblgrp;
     tReport.Open;
     dxDBTreeView1.DataSource := dsReport;     
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.dxDBTreeView1KeyPress(Sender: TObject; var Key: Char);
begin
     if(ord(Key)=VK_RETURN) then
     begin
         Key:=#0;
         aPregled.Execute;
     end;
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.aVidlivostExecute(Sender: TObject);
begin
     tReport.Edit;
     tReportVIDLIVOST.AsString:=fUslovi.getVidlivost;
     tReport.Post;
     if(tReport.ChangeCount > 0) then
      tReport.ApplyUpdates(0);
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.aPregledExecute(Sender: TObject);
begin
     if(tReportDATA.IsNull=true)  then
     ShowMessage('�������� � ������ ���� .frf"')
     else
     begin
          sqlPrai;
          frRep.LoadFromBlobField(tReportDATA);
          frRep.ShowReport;
     end;
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.aDizajnExecute(Sender: TObject);
begin
     sqlPrai;
     if (tReportDATA.IsNull=false) then
          frRep.LoadFromBlobField(tReportDATA);
     frRep.DesignReport;
     tReport.Edit;
     frRep.SaveToBlobField(tReport.FieldByName('DATA'));
     tReport.Post;
     if(tReport.ChangeCount > 0) then
          tReport.ApplyUpdates(0);
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_ESCAPE:
            Close();
    end;
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.eBrojKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
            Key := 0;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
        VK_RETURN:
        begin
            Key := 0;
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end
    end;
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Spremaj(broj:integer);
var
   Opts:TLocateOptions;
begin
   tReport.Params.ParamByName('tg').Value := tblgrp;
   tReport.Open();
   if(tReport.Locate('ID',broj,Opts)) then
   begin
      frRep.LoadFromBlobField(tReportDATA);
      qp.Close();
      qp.CommandText:=tReportSQL.AsString;
      qp.Params.ParseSQL(tReportSQL.AsString,true);
   end
   else
      ShowMessage('�� ������ ������� �� �������� ���!');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Spremaj(grupa:AnsiString;broj:integer);
var
   Opts:TLocateOptions;
   lv : Array[1..2] of Variant;
begin
   lv[1] := grupa;
   lv[2] := broj;
   tReport.Params.ParamByName('tg').Value := tblgrp;
   tReport.Open();
   if(tReport.Locate('TABELA_GRUPA;BR',VarArrayOf(lv),Opts)) then
   begin
      frRep.LoadFromBlobField(tReportDATA);
      qp.Close();
      qp.CommandText:=tReportSQL.AsString;
      qp.Params.ParseSQL(tReportSQL.AsString,true);
   end
   else
      ShowMessage('�� ������ ������� �� �������� ���!');
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Param(par_ime:AnsiString;par:Variant);
begin
   qp.Params.ParamByName(par_ime).Value:=par;
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Show;
begin
   qp.Open();
   frRep.ShowReport();
   qp.Close();
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.Design;
begin
   qp.Open();
   frRep.DesignReport();
   tReport.Edit();
   frRep.SaveToBlobField(tReport.FieldByName('DATA'));
   tReport.ApplyUpdates(0);
   tReport.Close();
   qp.Close();
end;
//-*----------------------------------------------------------------------------
procedure TfReporter.eGrupaExit(Sender: TObject);
begin
    if (tReport.State = dsInsert) then
    begin
        qryMaxBroj.ParamByName('tg').Value := tReportTABELA_GRUPA.Value;
        qryMaxBroj.Open;
        tReportBR.Value := qryMaxBrojMAKS.Value;
        qryMaxBroj.Close;
    end
end;
//-*----------------------------------------------------------------------------
function  TfReporter.NajdiBroj(grupa:AnsiString;broj:integer):Integer;
var
    ret : Integer;
begin
    ret := 0;
    qryNajdiSifra.ParamByName('tg').Value := grupa;
    qryNajdiSifra.ParamByName('br').Value := broj;
    qryNajdiSifra.Open;
    if(qryNajdiSifra.IsEmpty) then
    begin
        ShowMessage('�� ������ ������� �� �� ���!');
        Abort;
    end
    else
        ret := qryNajdiSifraID.Value;
    result := ret;
end;

end.




