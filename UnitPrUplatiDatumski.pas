unit UnitPrUplatiDatumski;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ComCtrls, ExtCtrls, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, cxContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSCore, dxPScxCommon,  dxPrnDlg, dxSkinsdxBarPainter, dxBar,
  Menus, cxLookAndFeelPainters, cxButtons, cxGridCustomPopupMenu,
  cxGridPopupMenu, FIBDataSet, pFIBDataSet, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxLookAndFeels, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxRibbon, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dxCore, cxDateUtils,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, dxRibbonCustomizationForm,
  System.Actions, System.DateUtils;

type
  TFrmPrUplatiDatumski = class(TForm)
    panelGlaen: TPanel;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    panelGrid: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxDateDatumOd: TcxDateEdit;
    cxDateDatumDo: TcxDateEdit;
    lblDatumDo: TLabel;
    cxGrid1DBTableView1DATUM_UPLATA: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    aPecati: TAction;
    dxBarManager1: TdxBarManager;
    SubItemAkcii: TdxBarSubItem;
    BarButtonIzlez: TdxBarButton;
    BarButtonPecati: TdxBarButton;
    cxGrid1DBTableView1TIP_UPLATA: TcxGridDBColumn;
    lblDatumOd: TLabel;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ID_TU: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Label1: TLabel;
    cbKorisnik: TcxLookupComboBox;
    qryUser: TpFIBDataSet;
    dsqryUser: TDataSource;
    qryUserUSERNAME: TFIBStringField;
    qryUserTIP_PARTNER: TFIBIntegerField;
    qryUserPARTNER: TFIBIntegerField;
    qryUserNAZIV: TFIBStringField;
    qryUserRE: TFIBIntegerField;
    qryUserRABOTNA_EDINICA: TFIBStringField;
    btPrikazi: TcxButton;
    dxBarManager1Bar2: TdxBar;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    ActionList2: TActionList;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aOtkazi: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1TS_KNIZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1DOKUMENT: TcxGridDBColumn;
    Label6: TLabel;
    cbGodina: TcxComboBox;
    aDizajn: TAction;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure onKeyDownAll(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure showPregled(datum_od:String;datum_do:String);
    procedure aPecatiExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    function podatociOk():boolean;
    procedure FormCreate(Sender: TObject);
    procedure btPrikaziClick(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPrUplatiDatumski: TFrmPrUplatiDatumski;

implementation

uses UnitDM1, dmKonekcija, dmMaticni, Utils, dmResources;

{$R *.dfm}

procedure TFrmPrUplatiDatumski.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
   brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TFrmPrUplatiDatumski.aDizajnExecute(Sender: TObject);
var k:string;
begin
    k:='%';
    if DM1.TblPrUplatiDatumski.RecordCount > 0 then
    begin
         if qryUser.Locate('NAZIV',VarArrayOf([cbKorisnik.Text]),[]) then
        // DM1.TblPrUplatiDatumski.ParamByName('user').AsString:=qryUser
             k:=qryUserUSERNAME.Value;
      //  dm1.ShowReport10('KOM',7,'od_datum',cxDateDatumOd.Text,'do_datum', cxDateDatumDo.Text,'user',k,'in_re',inttostr(dmKon.re),'in_god',cbgodina.Text);
    //    dxComponentPrinter1Link1.Preview(true);

      dmRes.Spremi(dmKon.aplikacija,7);
   //  dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblKarParBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('od_datum').AsString := cxDateDatumOd.Text;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('do_datum').AsString := cxDateDatumDo.Text;
      dmKon.tblSqlReport.Params.ParamByName('user').AsString := k;
      dmKon.tblSqlReport.Params.ParamByName('in_re').AsString := inttostr(dmKon.re);
      dmKon.tblSqlReport.Params.ParamByName('in_god').AsString := cbgodina.Text;
      dmKon.tblSqlReport.Open;

      dmres.frxReport1.Variables.AddVariable('Promenlivi','od_datum',''''+cxDateDatumOd.Text+'''');
      dmres.frxReport1.Variables.AddVariable('Promenlivi','do_datum',''''+cxDateDatumDo.Text+'''');
      dmRes.frxReport1.DesignReport();


    end;

end;

procedure TFrmPrUplatiDatumski.aIzlezExecute(Sender: TObject);
begin
    DM1.closeDataSetIfActive(DM1.TblPrUplatiDatumski);
    Close();
end;

procedure TFrmPrUplatiDatumski.aPageSetupExecute(Sender: TObject);
begin
    dxComponentPrinter1Link1.PageSetup;
end;

procedure TFrmPrUplatiDatumski.aPecatiExecute(Sender: TObject);
var k:string;
begin
    k:='%';
    if DM1.TblPrUplatiDatumski.RecordCount > 0 then
    begin
         if qryUser.Locate('NAZIV',VarArrayOf([cbKorisnik.Text]),[]) then
        // DM1.TblPrUplatiDatumski.ParamByName('user').AsString:=qryUser
             k:=qryUserUSERNAME.Value;
      //  dm1.ShowReport10('KOM',7,'od_datum',cxDateDatumOd.Text,'do_datum', cxDateDatumDo.Text,'user',k,'in_re',inttostr(dmKon.re),'in_god',cbgodina.Text);
    //    dxComponentPrinter1Link1.Preview(true);

      dmRes.Spremi(dmKon.aplikacija,7);
   //  dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.tblKarParBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('od_datum').AsString := cxDateDatumOd.Text;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('do_datum').AsString := cxDateDatumDo.Text;
      dmKon.tblSqlReport.Params.ParamByName('user').AsString := k;
      dmKon.tblSqlReport.Params.ParamByName('in_re').AsString := inttostr(dmKon.re);
      dmKon.tblSqlReport.Params.ParamByName('in_god').AsString := cbgodina.Text;
      dmKon.tblSqlReport.Open;

      dmres.frxReport1.Variables.AddVariable('Promenlivi','od_datum',''''+cxDateDatumOd.Text+'''');
      dmres.frxReport1.Variables.AddVariable('Promenlivi','do_datum', ''''+cxDateDatumDo.Text+'''');
      dmRes.frxReport1.ShowReport();


    end;
end;

procedure TFrmPrUplatiDatumski.aPecatiTabelaExecute(Sender: TObject);
begin
   dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TFrmPrUplatiDatumski.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
   dxComponentPrinter1Link1.DesignReport();
end;

procedure TFrmPrUplatiDatumski.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TFrmPrUplatiDatumski.aSnimiPecatenjeExecute(Sender: TObject);
begin
    zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TFrmPrUplatiDatumski.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TFrmPrUplatiDatumski.btPrikaziClick(Sender: TObject);
begin
      cxButton1Click(Sender);
end;

procedure TFrmPrUplatiDatumski.cxButton1Click(Sender: TObject);
begin
    if podatociOk then
    begin
        showPregled(cxDateDatumOd.Text,cxDateDatumDo.Text);
    end;
end;

procedure TFrmPrUplatiDatumski.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    DM1.closeDataSetIfActive(DM1.TblPrUplatiDatumski);
    dm1.TblPrUplatiDatumski.Close;
    Action := CaFree;
end;

procedure TFrmPrUplatiDatumski.FormCreate(Sender: TObject);
begin
    qryUser.Open;
end;

procedure TFrmPrUplatiDatumski.FormShow(Sender: TObject);
begin
   cbGodina.Text := IntToStr(yearof(now));
end;

procedure TFrmPrUplatiDatumski.onKeyDownAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin

            if TEdit(Sender).Name='btPikazi' then
            begin
               // showPregled(cxDateDatumOd.Text,cxDateDatumDo.Text);
               cxButton1Click(Sender);
            end
            else

            begin
                PostMessage(Handle,WM_NextDlgCtl,0,0);
            end;

        end;
    end;
end;


procedure TFrmPrUplatiDatumski.showPregled(datum_od:String;datum_do:String);
begin
    if DM1.TblPrUplatiDatumski.Active then
      DM1.TblPrUplatiDatumski.Close;

    DM1.TblPrUplatiDatumski.ParamByName('in_god').AsString:=cbGodina.Text;
    DM1.TblPrUplatiDatumski.ParamByName('in_re').AsInteger:=dmKon.re;
    DM1.TblPrUplatiDatumski.ParamByName('DATUM_OD').AsString:=datum_od;
    DM1.TblPrUplatiDatumski.ParamByName('DATUM_DO').AsString:=datum_do;
    if cbKorisnik.text='' then
    begin
         DM1.TblPrUplatiDatumski.ParamByName('user').AsString:='%';
    end
    else
    begin
      //if qryUser.Locate('NAZIV',VarArrayOf([cbKorisnik.Text]),[]) then
         DM1.TblPrUplatiDatumski.ParamByName('user').AsString:=qryUserUSERNAME.Value;
    end;
    DM1.TblPrUplatiDatumski.Open;
    cxGrid1.SetFocus;
end;

function TFrmPrUplatiDatumski.podatociOk():boolean;
var ret:boolean;
begin
    ret := false;
    if cxDateDatumOd.Text = '' then
    begin
        ShowMessage('������ �� �������� ����� ��!');
        cxDateDatumOd.SetFocus;
    end
    else if cxDateDatumDo.Text = '' then
    begin
        ShowMessage('������ �� �������� ����� ��!');
        cxDateDatumDo.SetFocus;
    end
    else
    begin
        ret := true;
    end;

    podatociOk := ret;
end;

end.
