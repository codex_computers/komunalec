unit KarticaKorisnikNova;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxCheckBox, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBExtLookupComboBox, FIBDataSet, pFIBDataSet,
  cxDBLookupComboBox, frxBarcode, frxClass, frxDBSet, frxDesgn, cxCalendar, DateUtils,
  cxImageComboBox, dxSkinsdxBarPainter, dxBar, FIBQuery, pFIBQuery, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinSilver, dxSkinStardust, dxSkinSummer2008, dxSkinValentine,
  dxSkinXmas2008Blue, cxLookAndFeels, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
   cxBarEditItem, dxStatusBar, dxRibbonStatusBar, dxRibbon,
  cxVGrid, cxDBVGrid, cxInplaceContainer, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSpringTime, dxSkinsdxRibbonPainter, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, Utils, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, //dxSkinOffice2013White,
  cxNavigator, dxCore, cxDateUtils, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxRibbonCustomizationForm, System.Actions;//,
//  System.Actions;

type
  TfrmKarticaKorisnikNova = class(TfrmMaster)
    cxGrid1DBTableView1BROJ_FAKTURA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1DOLZI: TcxGridDBColumn;
    cxGrid1DBTableView1POBARUVA: TcxGridDBColumn;
    cxGrid1DBTableView1UPLATENO: TcxGridDBColumn;
    cxGrid1DBTableView1SALDO: TcxGridDBColumn;
    tblKarPar: TpFIBDataSet;
    tblKarParBROJ_FAKTURA: TFIBStringField;
    tblKarParTIP_PARTNER: TFIBIntegerField;
    tblKarParID: TFIBIntegerField;
    tblKarParNAZIV: TFIBStringField;
    tblKarParADRESA: TFIBStringField;
    tblKarParDATUM_PRESMETKA: TFIBDateTimeField;
    tblKarParDOLZI: TFIBBCDField;
    tblKarParPOBARUVA: TFIBBCDField;
    tblKarParUPLATENO: TFIBDateField;
    tblKarParSALDO: TFIBBCDField;
    tblKarParPOTEKLO: TFIBStringField;
    dsKarPar: TDataSource;
    tblKorisnik: TpFIBDataSet;
    dsKorisnik: TDataSource;
    tblKorisnikTIP_PARTNER: TFIBIntegerField;
    tblKorisnikID: TFIBIntegerField;
    tblKorisnikNAZIV: TFIBStringField;
    tblKorisnikADRESA: TFIBStringField;
    aPecati: TAction;
    TblPresmetkaG: TpFIBDataSet;
    TblPresmetkaGTIP_PARTNER: TFIBIntegerField;
    TblPresmetkaGPARTNER_ID: TFIBIntegerField;
    TblPresmetkaGNAZIV: TFIBStringField;
    TblPresmetkaGIZNOS_VKUPNO: TFIBBCDField;
    TblPresmetkaGNAZIV1: TFIBStringField;
    TblPresmetkaGIZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaGDANOK: TFIBBCDField;
    TblPresmetkaGIZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaGVRABOTEN: TFIBStringField;
    TblPresmetkaGLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaGGODINA: TFIBIntegerField;
    TblPresmetkaGMESEC: TFIBIntegerField;
    TblPresmetkaGADRESA: TFIBStringField;
    TblPresmetkaGREON_ID: TFIBIntegerField;
    TblPresmetkaGCITACKA_KNIGA: TFIBIntegerField;
    TblPresmetkaGDANOK_05: TFIBBCDField;
    TblPresmetkaGDANOK_18: TFIBBCDField;
    TblPresmetkaGOSNOVICA_DANOK_05: TFIBBCDField;
    TblPresmetkaGOSNOVICA_DANOK_18: TFIBBCDField;
    TblPresmetkaGSTANBENA_POVRSINA: TFIBIntegerField;
    TblPresmetkaGDVORNA_POVRSINA: TFIBIntegerField;
    TblPresmetkaGBROJ_FAKTURA: TFIBStringField;
    DSPresmetka: TDataSource;
    TblPresmetkaS: TpFIBDataSet;
    DSPresmetka1: TDataSource;
    frxDesigner1: TfrxDesigner;
    frxReport1: TfrxReport;
    frxDSPresmetkaG: TfrxDBDataset;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxDSPresmetkaS: TfrxDBDataset;
    TblPresmetkaVodomeriSostojba: TpFIBDataSet;
    DSPresmetka2: TDataSource;
    TblSaldo: TpFIBDataSet;
    DSPresmetka3: TDataSource;
    aCallFrfBeleskiPrint: TAction;
    aPrintOdredeniBeleskiFizicki: TAction;
    aEdnaBeleskaFizicki: TAction;
    cxTxtPartnerID2: TcxTextEdit;
    cxTxtPartnerNaziv: TcxTextEdit;
    lblDatumOd: TLabel;
    lblDatumDo: TLabel;
    cxDateDatumOd: TcxDateEdit;
    cxDateDatumDo: TcxDateEdit;
    cxLookUpComboBoxTipPartner2: TcxTextEdit;
    Label2: TLabel;
    txtAdresa: TcxTextEdit;
    Label3: TLabel;
    txtReon: TcxTextEdit;
    txtCitKniga: TcxTextEdit;
    Label4: TLabel;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    frxDSPresmetkaStavki5: TfrxDBDataset;
    aPoedinecnaFaktura: TAction;
    TblPresmetkaGDATUM_PRESMETKA: TFIBDateTimeField;
    TblPresmetkaGRB1: TFIBIntegerField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    aIzbraniFakturi: TAction;
    Label5: TLabel;
    TblPresmetkaPoUslugi1: TpFIBDataSet;
    DSTblPresmetkaPoUslugu1: TDataSource;
    TblPresmetkaPoUslugi2: TpFIBDataSet;
    DSTblPresmetkaPoUslugi2: TDataSource;
    TblPresmetkaPoUsluga3: TpFIBDataSet;
    DSTblPresmetkaPoUsluva3: TDataSource;
    TblPresmetkaPoUsluga4: TpFIBDataSet;
    TblPresmetkaPoUsluga4VID_USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga4USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga4IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga4IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga4DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga4CENA_DVOR: TFIBBCDField;
    TblPresmetkaPoUsluga4DDV: TFIBIntegerField;
    DSTblPresmetkaPoUsluva4: TDataSource;
    TblPresmetkaPoUsluga5: TpFIBDataSet;
    DSTblPresmetkaPoUsluva5: TDataSource;
    TblPresmetkaPoUsluga6: TpFIBDataSet;
    TblPresmetkaPoUsluga6IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga6IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga6DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga6CENA_FVODA: TFIBBCDField;
    TblPresmetkaPoUsluga6DDV: TFIBIntegerField;
    DSTblPresmetkaPoUsluva6: TDataSource;
    TblPresmetkaPoUsluga7: TpFIBDataSet;
    DSTblPresmetkaPoUsluva7: TDataSource;
    aEdinecna: TAction;
    tblStornoG: TpFIBDataSet;
    dsStornoG: TDataSource;
    dsStornoS: TDataSource;
    tblStornoS: TpFIBDataSet;
    qMaks: TpFIBQuery;
    aStorno: TAction;
    tblStornoGID: TFIBIntegerField;
    tblStornoGLOKACIIN_ID: TFIBIntegerField;
    tblStornoGMESEC: TFIBIntegerField;
    tblStornoGGODINA: TFIBIntegerField;
    tblStornoGIZNOS_VKUPNO: TFIBBCDField;
    tblStornoGDATUM_PRESMETKA: TFIBDateTimeField;
    tblStornoGVRABOTEN: TFIBStringField;
    tblStornoGBROJ_FAKTURA: TFIBStringField;
    tblStornoGIZNOS_PLATENO: TFIBBCDField;
    tblStornoGRE: TFIBIntegerField;
    tblStornoGTIP_NALOG: TFIBStringField;
    tblStornoGNALOG: TFIBIntegerField;
    tblStornoGTS_KNIZENJE: TFIBDateTimeField;
    tblStornoGTIP: TFIBIntegerField;
    tblStornoSID: TFIBIntegerField;
    tblStornoSPRESMETKA_G_ID: TFIBIntegerField;
    tblStornoSLOKACIIN_ID: TFIBIntegerField;
    tblStornoSMESEC: TFIBIntegerField;
    tblStornoSGODINA: TFIBIntegerField;
    tblStornoSVID_USLUGA: TFIBIntegerField;
    tblStornoSUSLUGA: TFIBIntegerField;
    tblStornoSIZNOS_USLUGA_NETO: TFIBBCDField;
    tblStornoSIZNOS_USLUGA: TFIBBCDField;
    tblStornoSDATUM_PRESMETKA: TFIBDateTimeField;
    tblStornoSVRABOTEN: TFIBStringField;
    tblStornoSDANOK: TFIBBCDField;
    tblStornoSCENA: TFIBBCDField;
    TblPresmetkaSVID_USLUGA: TFIBIntegerField;
    TblPresmetkaSUSLUGA: TFIBIntegerField;
    TblPresmetkaSLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaSNAZIV: TFIBStringField;
    TblPresmetkaSIZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaSDANOK: TFIBBCDField;
    TblPresmetkaSIZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaSCENA: TFIBBCDField;
    TblPresmetkaSPROCENT_DANOK: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaVODOMERI_ID: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaNOVA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaSTARA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaPoUslugi1LOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaPoUslugi1VODOMERI_ID: TFIBIntegerField;
    TblPresmetkaPoUslugi1NOVA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaPoUslugi1STARA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaPoUslugi1RAZLIKA: TFIBBCDField;
    TblPresmetkaPoUslugi1CENA_VODA: TFIBBCDField;
    TblPresmetkaPoUslugi1DDV: TFIBIntegerField;
    TblPresmetkaPoUslugi1IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUslugi1IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaGID: TFIBIntegerField;
    qPovrsina: TpFIBQuery;
    qLokaciiTemp: TpFIBQuery;
    qLokacii: TpFIBQuery;
    qUpdateUplati: TpFIBQuery;
    qKluc: TpFIBQuery;
    TblPresmetkaGDDO: TFIBDateField;
    TblPresmetkaPoUsluga8: TpFIBDataSet;
    FIBBCDField1: TFIBBCDField;
    FIBBCDField2: TFIBBCDField;
    FIBIntegerField1: TFIBIntegerField;
    FIBBCDField3: TFIBBCDField;
    FIBBCDField4: TFIBBCDField;
    DSTblPresmetkaPoUsluva8: TDataSource;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dr: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    cxGrid1DBTableView1LOKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PID: TcxGridDBColumn;
    cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_REON: TcxGridDBColumn;
    TblPresmetkaPoUsluga3VID_USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga3USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga3IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga3IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga3DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga3DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga3CENA_STAN: TFIBBCDField;
    frxDSPresmetkaStavki3: TfrxDBDataset;
    frxDSPresmetkaStavki1: TfrxDBDataset;
    frxDSPresmetkaStavki2: TfrxDBDataset;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1LOKACIJA: TcxGridDBColumn;
    cxGrid2DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid2DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid2DBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV_REON: TcxGridDBColumn;
    tblLokacija: TpFIBDataSet;
    dsLokacija: TDataSource;
    DataSource2: TDataSource;
    tblLokacijaLOKACIJA: TFIBIntegerField;
    tblLokacijaTIP_PARTNER: TFIBIntegerField;
    tblLokacijaID: TFIBIntegerField;
    tblLokacijaNAZIV: TFIBStringField;
    tblLokacijaADRESA: TFIBStringField;
    tblLokacijaREON_ID: TFIBIntegerField;
    tblLokacijaNAZIV_REON: TFIBStringField;
    tblLokacijaCITACKA_KNIGA: TFIBIntegerField;
    tblLokacijaAKTIVEN: TFIBSmallIntField;
    cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_LOKACIJA: TcxGridDBColumn;
    frxDSPresmetkaStavki4: TfrxDBDataset;
    frxDSPresmetkaStavki6: TfrxDBDataset;
    frxDSPresmetkaStavki7: TfrxDBDataset;
    frxDSPresmetkaStavki8: TfrxDBDataset;
    frxDSPresmetkaVodomeriSostojba: TfrxDBDataset;
    frxDSSaldo: TfrxDBDataset;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    frxDSPresmetkaStavki10: TfrxDBDataset;
    TblPresmetkaPoUsluga10: TpFIBDataSet;
    DSTblPresmetkaPoUsluva10: TDataSource;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView3GODINA: TcxGridDBColumn;
    cxGrid1DBTableView3MESEC: TcxGridDBColumn;
    cxGrid1DBTableView3DOLZI: TcxGridDBColumn;
    cxGrid1DBTableView3UPLATI: TcxGridDBColumn;
    cxGrid1DBTableView3UPLATENO: TcxGridDBColumn;
    cxGrid1DBTableView3TIP: TcxGridDBColumn;
    cxGrid1DBTableView3NAZIV_UPLATA: TcxGridDBColumn;
    cxGrid1DBTableView3RE: TcxGridDBColumn;
    cxGrid1DBTableView3TIP_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView3NALOG: TcxGridDBColumn;
    cxGrid1DBTableView3TS_KNIZENJE: TcxGridDBColumn;
    cxGrid1DBTableView3DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView3NACIN_PLAKJANJE: TcxGridDBColumn;
    TblPresmetkaPoUsluga5IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga5IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga5DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga5DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga5CENA_MANIPULATIVEN: TFIBBCDField;
    TblPresmetkaPoUsluga10IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga10IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga10DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga10DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga10CENA_NADOMESTOK: TFIBBCDField;
    TblPresmetkaPoUsluga10NAZIV_VID_USLUGA: TFIBStringField;
    TblPresmetkaPoUsluga10NAZIV_USLUGA: TFIBStringField;
    TblPresmetkaPoUsluga7IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga7IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga7DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga7DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga7CENA_FKAN: TFIBBCDField;
    TblDPresmetkaPoUsluga2: TpFIBDataSet;
    FIBBCDField18: TFIBBCDField;
    FIBBCDField19: TFIBBCDField;
    FIBBCDField20: TFIBBCDField;
    FIBBCDField21: TFIBBCDField;
    FIBIntegerField24: TFIBIntegerField;
    DSDPresmetkaPoUsluva2: TDataSource;
    TblPresmetkaPoUslugi2IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUslugi2IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUslugi2DDV: TFIBIntegerField;
    TblPresmetkaPoUslugi2DANOK: TFIBBCDField;
    TblPresmetkaPoUslugi2CENA_KAN: TFIBBCDField;
    tblStornoGOPOMENA_BROJ: TFIBStringField;
    tblStornoGDDO: TFIBDateField;
    tblStornoGID_KOREKCIJA: TFIBIntegerField;
    cxLookUpComboBoxTipPartner: TcxMaskEdit;
    cxTxtPartnerID: TcxMaskEdit;
    aKorekcija: TAction;
    TblPresmetkaPoUsluga61: TpFIBDataSet;
    FIBBCDField5: TFIBBCDField;
    FIBBCDField6: TFIBBCDField;
    FIBIntegerField2: TFIBIntegerField;
    FIBBCDField7: TFIBBCDField;
    FIBBCDField8: TFIBBCDField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    dsTblPresmetkaPoUsluva61: TDataSource;
    frxDSPresmetkaStavki61: TfrxDBDataset;
    qTuzbaSmetka: TpFIBQuery;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2VID_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView2USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView2IZNOS_USLUGA_NETO: TcxGridDBColumn;
    cxGrid1DBTableView2IZNOS_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView2DANOK: TcxGridDBColumn;
    cxGrid1DBTableView2CENA: TcxGridDBColumn;
    cxGrid1Level3: TcxGridLevel;
    DSTblPresmetkaPoUsluva11: TDataSource;
    TblPresmetkaPoUsluga11: TpFIBDataSet;
    FIBBCDField9: TFIBBCDField;
    FIBBCDField10: TFIBBCDField;
    FIBIntegerField3: TFIBIntegerField;
    FIBBCDField11: TFIBBCDField;
    FIBBCDField12: TFIBBCDField;
    FIBStringField3: TFIBStringField;
    FIBStringField4: TFIBStringField;
    frxDSPresmetkaStavki11: TfrxDBDataset;
    TblSaldoSALDO: TFIBBCDField;
    DSTblPresmetkaPoUslugi12: TDataSource;
    TblPresmetkaPoUsluga12: TpFIBDataSet;
    FIBIntegerField4: TFIBIntegerField;
    FIBIntegerField5: TFIBIntegerField;
    FIBBCDField13: TFIBBCDField;
    FIBBCDField14: TFIBBCDField;
    FIBBCDField15: TFIBBCDField;
    frxDSPresmetkaStavki12: TfrxDBDataset;
    DSTblPresmetkaPoUslugi121: TDataSource;
    TblPresmetkaPoUsluga121: TpFIBDataSet;
    FIBIntegerField7: TFIBIntegerField;
    FIBIntegerField8: TFIBIntegerField;
    FIBBCDField17: TFIBBCDField;
    FIBBCDField22: TFIBBCDField;
    FIBBCDField23: TFIBBCDField;
    FIBBCDField24: TFIBBCDField;
    FIBIntegerField9: TFIBIntegerField;
    frxDSPresmetkaStavki121: TfrxDBDataset;
    TblPresmetkaPoUsluga12CENA_STAN: TFIBBCDField;
    TblPresmetkaPoUsluga12DDV: TFIBIntegerField;
    aPratiEmail: TAction;
    dxbrlrgbtn1: TdxBarLargeButton;
    procedure FormShow(Sender: TObject);
    procedure cbKorisnikExit(Sender: TObject);
    procedure aCallFrfBeleskiPrintExecute(Sender: TObject);
    procedure aPrintOdredeniBeleskiFizickiExecute(Sender: TObject);
    procedure aEdnaBeleskaFizickiExecute(Sender: TObject);
    procedure cxBtnPartneriClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure callFrmPartneri;
    procedure setGridForLokacii(tip_partner:String;partner:String);
    procedure setUpLokationData;
    procedure setUpNeplateniZadolzuvanjaData(tip_partner:String;partner:String;lokacija:String);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit1Exit(Sender: TObject);
    procedure cxDBTextEdit2Click(Sender: TObject);
    procedure cxTxtPartnerNazivExit(Sender: TObject);
    procedure cxDateDatumDoExit(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure cxLookUpComboBoxTipPartner2Exit(Sender: TObject);
    procedure cxTxtPartnerID2Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxTxtPartnerNazivEnter(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aPoedinecnaFakturaExecute(Sender: TObject);
    procedure aIzbraniFakturiExecute(Sender: TObject);
    procedure aStornirajExecute(Sender: TObject);
    procedure aEdinecnaExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aStornoExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aKorekcijaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1DataControllerDetailCollapsed(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanded(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
    procedure aPratiEmailExecute(Sender: TObject);
  private

    { Private declarations }
  public
  var PresmetkaPoFaktura:String;
    { Public declarations }
  end;

var
  frmKarticaKorisnikNova: TfrmKarticaKorisnikNova;

implementation

uses UnitDM1, dmKonekcija, dmMaticni, Partner, DaNe,
  PoedinecnaPresmetka, UnitVodSostPoedinecno, dmResources;

{$R *.dfm}

procedure TfrmKarticaKorisnikNova.aAzurirajExecute(Sender: TObject);
var frm:TfrmVodSost; stan,dvor,stan_temp,dvor_temp,i,lok,proknizena :integer;
begin
  //inherited;
   if (dm1.tblKarParAKTIVEN.Value=0) and (dm1.tblKarPar.RecordCount>0) then
     begin
       ShowMessage('��������� �� � �������! �� �� ���� �� �� ������� ���� ����� �������, ����� �� � �������.');
       Abort;
     end;

//if (dm1.tblKarParUPLATENO.AsString='') then
if (dm1.tblKarParPOBARUVA.AsFloat=0) //and (dm1.tblKarParSALDO.AsInteger=0)
then
begin
 frmDaNe := TfrmDaNe.Create(self,'����������',' ���� �� ������� ������� �� ���������?',1);
  if (frmDaNe.ShowModal = mrYes) then
  begin

     proknizena:=0;

//     if (dm1.tblKarParPOBARUVA.AsFloat<>0) and (dm1.tblKarParSALDO.AsInteger=0) then
//     begin
//         ShowMessage('�� ��������� ��� ��� �������! �� ���� �� �� ����� ������!');
//         Abort;
//     end
//     else
//   begin
     if (not dm1.tblKarParNALOG.IsNull or not dm1.tblKarParID_FNG.IsNull) then
     begin
       proknizena:=1;
      // ShowMessage('������� � ���������, �� ���� �� �� ����� ������!');


     end;
        aStorno.Execute;
      //da se napravi nova presmetka, prevo treba da se promenat vleznite parametri
        //vodomeri sostijba, nova sostojba
      frmDaNe := TfrmDaNe.Create(self,'����������',' ���� �� �� �������� ��������� �� ���������?',1);
        if (frmDaNe.ShowModal = mrYes) then
         begin
            frmVodSost:=TfrmVodSost.Create(nil,dm1.tblKarParTIP_PARTNER.AsString,dm1.tblKarParID.AsString,dm1.tblKarParMESEC.AsString,dm1.tblKarParGODINA.AsString,dm1.tblKarParCITACKA_KNIGA.AsString,dm1.tblKarParREON_ID.AsString);
            frmVodSost.ShowModal;
            frmVodSost.Free;
        end;
       // i gjubrarina (povrsina) da se izvlece od stavite

       // ako se update - uva presmetkata - da se napravi???

       try
            stan_temp:=0;
            lok:=dm1.tblKarParLOKACIJA.Value;
            qPovrsina.Close;
            qPovrsina.ParamByName('l').AsString:=dm1.tblKarParLOKACIJA.AsString;
            qPovrsina.ParamByName('m').AsString:=dm1.tblKarParMESEC.AsString;
            qPovrsina.ParamByName('g').AsString:=dm1.tblKarParGODINA.AsString;
            qPovrsina.ExecQuery;
            while not qPovrsina.Eof do
            begin
              if qPovrsina.FldByName['nacin_presmetka'].AsInteger = 3 then
                stan_temp:=qPovrsina.FldByName['povrsina'].AsInteger
              else if qPovrsina.FldByName['nacin_presmetka'].AsInteger = 4 then
              dvor_temp:=qPovrsina.FldByName['povrsina'].AsInteger;
              qPovrsina.Next;
            end;

          // kolku se vo tabela lokacii stan i dvor
             qLokacii.Close;
             qLokacii.ParamByName('l').AsInteger:=lok;
             qLokacii.ExecQuery;
             stan:=qLokacii.FldByName['stanbena_povrsina'].AsInteger;
             dvor:=qLokacii.FldByName['dvorna_povrsina'].AsInteger;

          //update vo lokacii
             qLokaciiTemp.Close;
             qLokaciiTemp.ParamByName('l').AsInteger:=lok;
             qLokaciiTemp.ParamByName('sp').AsInteger:=stan_temp;
             qLokaciiTemp.ParamByName('dp').AsInteger:=dvor_temp;
             qLokaciiTemp.ExecQuery;

            if proknizena=1 then
              begin
                  //nova presmetka
                  DM1.ProcKomPresmetkaEdinecna.close;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('REON_ID').AsString := dm1.tblKarParREON_ID.AsString;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('GODINA').AsString := dm1.tblKarParGODINA.AsString;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('MESEC').AsString := dm1.tblKarParMESEC.AsString;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('AKTIVEN').AsInteger := 1;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('VRABOTEN').AsString := dmKon.user;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('LOKACIJA').AsInteger :=lok;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('PID').AsInteger :=dm1.tblKarParPID.Value;
                  DM1.ProcKomPresmetkaEdinecna.Prepare;
                  DM1.ProcKomPresmetkaEdinecna.ExecProc;
              end
            else
            begin
                  DM1.QDeleteLastPresmetka.Close;
                  DM1.QDeleteLastPresmetka.ParamByName('GODINA').AsString := dm1.tblKarParGODINA.AsString;
                  DM1.QDeleteLastPresmetka.ParamByName('MESEC').AsString := dm1.tblKarParMESEC.AsString;
                  DM1.QDeleteLastPresmetka.ParamByName('LOKACIJA').AsString := DM1.tblKarParLOKACIJA.AsString;
                  DM1.QDeleteLastPresmetka.ExecQuery();

                  DM1.ProcKomPresmetkaEdinecna.close;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('REON_ID').AsString := dm1.tblKarParREON_ID.AsString;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('GODINA').AsString := dm1.tblKarParGODINA.AsString;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('MESEC').AsString := dm1.tblKarParMESEC.AsString;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('AKTIVEN').AsInteger := 1;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('VRABOTEN').AsString := dmKon.user;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('LOKACIJA').AsInteger :=lok;
                  DM1.ProcKomPresmetkaEdinecna.ParamByName('PID').AsString := '';
                  DM1.ProcKomPresmetkaEdinecna.Prepare;
                  DM1.ProcKomPresmetkaEdinecna.ExecProc;


                 //update presmetka
//                  DM1.ProcKomPresmetkaUpdate.close;
//                  DM1.ProcKomPresmetkaUpdate.ParamByName('REON_ID').AsString := dm1.tblKarParREON_ID.AsString;
//                  DM1.ProcKomPresmetkaUpdate.ParamByName('GODINA').AsString := dm1.tblKarParGODINA.AsString;
//                  DM1.ProcKomPresmetkaUpdate.ParamByName('MESEC').AsString := dm1.tblKarParMESEC.AsString;
//                  DM1.ProcKomPresmetkaUpdate.ParamByName('AKTIVEN').AsInteger := 1;
//      //            DM1.procKomPresmetka.ParamByName('VRABOTEN').AsString := dmKon.user;
//                  DM1.ProcKomPresmetkaUpdate.ParamByName('LOKACIJA').AsInteger :=lok;
//
//
//            DM1.ProcKomPresmetkaUpdate.Prepare;
//            DM1.ProcKomPresmetkaUpdate.ExecProc;
             end;

            qKluc.close;
            qKluc.ParamByName('m').AsString:=dm1.tblKarParMESEC.AsString;
            qkluc.ParamByName('g').AsString:=dm1.tblKarParGODINA.AsString;
            qkluc.ParamByName('l').AsInteger:=lok;
            qKluc.ExecQuery;

            //da se proveria dali ima uplata
         if (dm1.tblKarParPOBARUVA.AsFloat<>0) and (dm1.tblKarParSALDO.AsInteger<>0) then
          begin
            qUpdateUplati.Close;
            qUpdateUplati.ParamByName('p').AsString:=qkluc.FldByName['id'].AsString;
            qUpdateUplati.ParamByName('l').AsInteger:=lok;
            qUpdateUplati.ParamByName('m').AsString:=dm1.tblKarParMESEC.AsString;
            qUpdateUplati.ParamByName('g').AsString:=dm1.tblKarParGODINA.AsString;
            qUpdateUplati. ExecQuery;
          end;

           //da se vratat stan i dvor vo lokacii
            qLokaciiTemp.Close;
            qLokaciiTemp.ParamByName('l').AsInteger:=lok;
            qLokaciiTemp.ParamByName('sp').AsInteger:=stan;
            qLokaciiTemp.ParamByName('dp').AsInteger:=dvor;
            qLokaciiTemp.ExecQuery;

           except
             On E: Exception do
                ShowMessage(E.Message);
          end;
         ShowMessage('��������� � ������� �� ���������!');
         DM1.tblKarPar.Close;
         dm1.tblKarPar.ParamByName('tp').AsString:=cxLookUpComboBoxTipPartner.Text;
         dm1.tblKarPar.ParamByName('p').AsString:=cxTxtPartnerID.Text;
         dm1.tblKarPar.ParamByName('d1').AsString:=cxDateDatumOd.Text;
         dm1.tblKarPar.ParamByName('d2').Asdate:=EndOfTheMonth(StrToDate(cxDateDatumDo.Text));
         DM1.tblKarPar.open;
         dm1.tblSiteUplati.open;
         dm1.tblSiteStavki.Open;
   end;
//end;

end
else
begin
    ShowMessage('�� ��������� ��� ��� �������! �� ���� �� �� ����� ������!');
    Abort;
end;


end;

procedure TfrmKarticaKorisnikNova.aBrisiExecute(Sender: TObject);
var i,lok,proknizena :integer;
begin
   qTuzbaSmetka.Close;
   qTuzbaSmetka.ParamByName('SMETKA_ID').Value := cxGrid1DBTableView1PID.EditValue;
   qTuzbaSmetka.ExecQuery;

if qTuzbaSmetka.FldByName['ima'].AsInteger = 1 then
begin
    ShowMessage('�� ��������� ��� ��� �����! �� ���� �� �� �����!');
    Abort;
end;


 if (not dm1.tblKarParPOBARUVA.IsNull) and (dm1.tblKarParSALDO.AsInteger=0) then
   begin
         ShowMessage('�� ��������� ��� ��� �������! �� ���� �� �� �����!');
         Abort;
   end

  else
  begin
  frmDaNe := TfrmDaNe.Create(self,'����������',' ���� �� �� ������� ���������?',1);
  if (frmDaNe.ShowModal = mrYes) then
  begin
 //    proknizena:=0;
//   if (not dm1.tblKarParPOBARUVA.IsNull) and (dm1.tblKarParSALDO.AsInteger=0) then
//     begin
//         ShowMessage('�� ��������� ��� ��� �������! �� ���� �� �� �����!');
//         Abort;
//     end
//     else
//     begin
//         if not dm1.tblKarParNALOG.IsNull then
//         begin
//            proknizena:=1;
//
//         end
//         else
        // begin
//            DM1.QDeleteLastPresmetka.Close;
//            DM1.QDeleteLastPresmetka.ParamByName('GODINA').AsString := dm1.tblKarParGODINA.AsString;
//            DM1.QDeleteLastPresmetka.ParamByName('MESEC').AsString := dm1.tblKarParMESEC.AsString;
//            DM1.QDeleteLastPresmetka.ParamByName('LOKACIJA').AsString := DM1.tblKarParLOKACIJA.AsString;
//
//            DM1.QDeleteLastPresmetka.ExecQuery();
        //  end;
          aStorno.Execute;
         DM1.tblKarPar.Close;
         dm1.tblKarPar.ParamByName('tp').AsString:=cxLookUpComboBoxTipPartner.Text;
         dm1.tblKarPar.ParamByName('p').AsString:=cxTxtPartnerID.Text;
         dm1.tblKarPar.ParamByName('d1').AsString:=cxDateDatumOd.Text;
         dm1.tblKarPar.ParamByName('d2').Asdate:=EndOfTheMonth(StrToDate(cxDateDatumDo.Text));
         DM1.tblKarPar.open;
         dm1.tblSiteUplati.open;
         dm1.tblSiteStavki.Open;
  end;
 //
 end;

end;

procedure TfrmKarticaKorisnikNova.aCallFrfBeleskiPrintExecute(Sender: TObject);
begin
  inherited;
   frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(dm1.tblKarParPOBARUVA.asstring));
   frxReport1.DesignReport();
end;

procedure TfrmKarticaKorisnikNova.aOtkaziExecute(Sender: TObject);
begin
 // inherited;
   if not dPanel.Enabled then
   begin
      dPanel.Enabled:=True;
       TblPresmetkaG.Close;
       dm1.tblKarPar.Close;
       tblLokacija.Close;

       dmMat.tblPartner.Filtered:=false;
       cxLookUpComboBoxTipPartner.Text:='';
       cxTxtPartnerID.Text:='';
       cxTxtPartnerNaziv.Text:='';
       txtAdresa.Text:='';
       txtReon.Text:='';
       txtCitKniga.Text:='';
       cxDateDatumOd.Text:='';
       cxDateDatumDo.Text:='';
       aEdnaBeleskaFizicki.Enabled:=False;
       aPrintOdredeniBeleskiFizicki.Enabled:=False;
       aPoedinecnaFaktura.Enabled:=false;
       aIzbraniFakturi.Enabled:=false;
       //ActionList1.State:=asSuspended;
       cxLookUpComboBoxTipPartner.SetFocus;
     //  Abort;
   end
   else
      close;
end;

procedure TfrmKarticaKorisnikNova.aPecatiExecute(Sender: TObject);
begin
  inherited;
  if (Validacija(dPanel)) = False then
          dm1.ShowReport8('KOM',5,'od_datum',cxDateDatumOd.Text,'do_datum',datetostr(EndOfTheMonth(StrToDate(cxDateDatumDo.Text))),'_partip',cxLookUpComboBoxTipPartner.Text ,'_parsif',cxTxtPartnerID.text);
end;

procedure TfrmKarticaKorisnikNova.aPoedinecnaFakturaExecute(Sender: TObject);
var uplateno:real;
begin
    TblPresmetkaG.close;
    TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
                                                      ' and kla.id_lokacija='+cxGrid1DBTableView1LOKACIJA.EditValue+
                                                      ' and kpg.broj_faktura ='''+cxGrid1DBTableView1BROJ_FAKTURA.EditValue+
                                                      ''' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;

    TblPresmetkaG.open;
    TblPresmetkaS.Open;
    TblPresmetkaVodomeriSostojba.Open;
   // TblSaldo.Open;

 //   TblPresmetkaVodomeriSostojba.open;
   // TblPresmetkaPoUsluga3.Open;

//    TblSaldo.Close;
//    TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
//    TblSaldo.Open;}
   TblSaldo.Close;
   frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
//    if (frmDaNe.ShowModal <> mrYes) then
//    begin
//        TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(TblPresmetkaGGODINA.Value,TblPresmetkaGMESEC.Value)+15);
//        TblSaldo.Open;
//    end
//    else
//    begin
//      TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
//      TblSaldo.Open;
//    end;
    //ShowMessage(TblSaldoSALDO.AsString);
   if TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

       if dm1.tblKarParPOBARUVA.AsString='' then
           uplateno:=0
        else
          uplateno:=dm1.tblKarParPOBARUVA.AsFloat;
        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
        //TblSaldo.Open;

        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TfrmKarticaKorisnikNova.aPratiEmailExecute(Sender: TObject);
var uplateno : Double;
  i: Integer;
  memStream: TMemoryStream;
  status: TStatusWindowHandle;

begin
  inherited;
begin
  inherited;
    //if (Validacija(dPanel)) = False then
    //      dm1.ShowReport8('KOM',5,'od_datum',cxDateDatumOd.Text,'do_datum',datetostr(EndOfTheMonth(StrToDate(cxDateDatumDo.Text))),'_partip',cxLookUpComboBoxTipPartner.Text ,'_parsif',cxTxtPartnerID.text);

   //if not (dm.tblKreditNota.State in [dsEdit,dsInsert]) then
 //begin
    TblPresmetkaG.close;
    TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
                                                         ' and kla.id_lokacija='+dm1.tblKarParLOKACIJA.AsString+
                                                         ' and kpg.broj_faktura ='''+cxGrid1DBTableView1BROJ_FAKTURA.EditValue+
                                                         ''' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;
    TblPresmetkaG.open;
    TblPresmetkaS.Open;
    TblPresmetkaVodomeriSostojba.open;
    TblSaldo.Close;
    frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
    if (frmDaNe.ShowModal <> mrYes) then
    begin
        TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(TblPresmetkaGGODINA.Value,TblPresmetkaGMESEC.Value)+15);
        TblSaldo.Open;
    end
    else
    begin
      TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
      TblSaldo.Open;
    end;

    if TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;
        if dm1.tblKarParPOBARUVA.AsString='' then
           uplateno:=0
        else
          uplateno:=dm1.tblKarParPOBARUVA.AsFloat;
        frxReport1.LoadFromFile('FRBeleskaFizickoLice1.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
       // frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
 // dmRes.frxReport1.ShowReport();

  //prati ja po mail
   	status := cxCreateStatusWindow();
        try
          memStream := TMemoryStream.Create;
          dmRes.frxPDFExport1.Stream := memStream;
          memStream.Position := 0;
          dmRes.frxReport1.PrepareReport(True);
          dmRes.frxReport1.Export(dmRes.frxPDFExport1);

        //  dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([TblPresmetkaGTIP_PARTNER.AsString, TblPresmetkaGPARTNER_ID.AsString]) , []);
          dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([cxLookUpComboBoxTipPartner.Text, cxTxtPartnerID.text]) , []);

//          if (devizno = false) then
  //          dmRes.SendIndyEmail(dmMat.tblPartnerMAIL.Value, '���������� ������� �� ������� (' + dmMat.tblPartnerNAZIV.Value + ')', '���������� ������� �� �������', '', memStream, 'kartica_partner')
  //        else
            dmRes.SendIndyEmail(dmMat.tblPartnerMAIL.Value, '������� �� ������� (' + dmMat.tblPartnerNAZIV.Value + ')', '������� �� �������', '', memStream, 'faktura');

        finally
          FreeAndNil(MemStream);
          dmRes.frxPDFExport1.Stream := nil;
          cxRemoveStatusWindow(status);
        end;
    end;
end;
end;

procedure TfrmKarticaKorisnikNova.aPrintOdredeniBeleskiFizickiExecute(Sender: TObject);
var vk_selectirani,i :Integer; br_beleski:String;
begin
  br_beleski:='';
   vk_selectirani := cxGrid1DBTableView1.Controller.SelectedRecordCount;
    for i:=0 to vk_selectirani-1 do
    begin
       if br_beleski<>'' then
          br_beleski:=br_beleski+','+QuotedStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0])
       else
       begin
          br_beleski:=QuotedStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0]);
       end;
    end;
        TblPresmetkaG.close;
        TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
                                                         ' and kpg.broj_faktura in ('+br_beleski+')'+
                                                         ' and kla.id_lokacija='+dm1.tblKarParLOKACIJA.AsString+
                                                         ' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;

        TblPresmetkaG.open;
        TblPresmetkaS.Open;
        TblPresmetkaVodomeriSostojba.open;
        TblSaldo.Close;
        frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(TblPresmetkaGGODINA.Value,TblPresmetkaGMESEC.Value)+30);
            TblSaldo.Open;
        end
        else
        begin
            TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
            TblSaldo.Open;
        end;
            if TblPresmetkaG.RecordCount > 0 then
            begin
                frxReport1.LoadFromFile('FRBeleskaFizickoLice1.fr3');
                frxReport1.PrepareReport(true);
                frxReport1.ShowPreparedReport();
            end
            else
            begin
                ShowMessage('���� �������� �� ��������!');
            end;
  end;

procedure TfrmKarticaKorisnikNova.aStornirajExecute(Sender: TObject);
begin
 // inherited;

end;

procedure TfrmKarticaKorisnikNova.aStornoExecute(Sender: TObject);
var mesec:string; broj,id:integer;
begin
   //   frmDaNe := TfrmDaNe.Create(self,'����������','��� ��� ��������� ��������� �� ����������� �������! ���� �� �� ����������?',1);
    //    if (frmDaNe.ShowModal = mrYes) then
      //    begin
             //treba da se stornira presmetkata i da se napravi nova
             qMaks.Close;
             qMaks.ParamByName('l').AsString:=dm1.tblKarParLOKACIJA.AsString;
             qMaks.ParamByName('m').AsString:=dm1.tblKarParMESEC.AsString;
             qMaks.ParamByName('g').AsString:=dm1.tblKarParGODINA.AsString;
             qMaks.ExecQuery;

             broj:= StrToInt(qMaks.FldByName['maks'].Value)+1;
             id:=StrToInt(qMaks.FldByName['id'].Value);
             tblStornoG.Close;
             tblStornoG.Open;
             tblStornoG.Insert;
             tblStornoGID.Value:=qMaks.FldByName['id'].AsInteger;
             tblStornoGLOKACIIN_ID.AsString:=dm1.tblKarParLOKACIJA.AsString;
             tblStornoGMESEC.AsString:=dm1.tblKarParMESEC.AsString;
             tblStornoGGODINA.AsString:=dm1.tblKarParGODINA.AsString;
             tblStornoGIZNOS_VKUPNO.AsFloat:=dm1.tblKarParDOLZI.AsFloat*(-1);
             tblStornoGVRABOTEN.AsString:=dmKon.user;
             if dm1.tblKarParMESEC.Value<10 then
                mesec:='0'+dm1.tblKarParMESEC.AsString
             else mesec:=dm1.tblKarParMESEC.AsString;
             tblStornoGBROJ_FAKTURA.AsString:=copy(dm1.tblKarParGODINA.asstring,3,2)+mesec+dm1.tblKarParLOKACIJA.AsString+'0'+IntToStr(broj);
             tblStornoGDATUM_PRESMETKA.AsDateTime:=Now;
             tblStornoGTIP.Value:=2;
             tblStornoGID_KOREKCIJA.AsString:=dm1.tblKarParPID.AsString;
             tblStornoG.Post;

             dm1.qDaliImaStavki.Close;
             dm1.qDaliImaStavki.ParamByName('id').AsString:=dm1.tblKarParPID.AsString;
             dm1.qDaliImaStavki.ExecQuery;

             while not dm1.qDaliImaStavki.Eof do
             begin
                   tblStornoS.Close;
                   tblStornoS.Open;
                   tblStornoS.Insert;
                   tblStornoSPRESMETKA_G_ID.Value:=id;
                   tblStornoSLOKACIIN_ID.Value:=DM1.qDaliImaStavki.FldByName['lokaciin_id'].Value;
                   tblStornoSMESEC.Value:=DM1.qDaliImaStavki.FldByName['mesec'].Value;
                   tblStornoSGODINA.Value:=DM1.qDaliImaStavki.FldByName['godina'].Value;
                   tblStornoSVID_USLUGA.Value:=DM1.qDaliImaStavki.FldByName['vid_usluga'].Value;
                   tblStornoSUSLUGA.Value:=DM1.qDaliImaStavki.FldByName['usluga'].Value;
                   tblStornoSIZNOS_USLUGA_NETO.Value:=DM1.qDaliImaStavki.FldByName['iznos_usluga_neto'].AsFloat*(-1);
                   tblStornoSIZNOS_USLUGA.Value:=DM1.qDaliImaStavki.FldByName['iznos_usluga'].AsFloat*(-1);
                   tblStornoSDATUM_PRESMETKA.AsDateTime:=Now;
                   tblStornoSVRABOTEN.Value:=dmKon.user;
                   tblStornoSDANOK.AsFloat:=DM1.qDaliImaStavki.FldByName['danok'].AsFloat*(-1);
                   tblStornoSCENA.Value:=DM1.qDaliImaStavki.FldByName['cena'].Value;
                   tblStornoS.Post;

                   dm1.qDaliImaStavki.Next;
             end;
             dm1.qUpdateTip.Close;
             dm1.qUpdateTip.ParamByName('l').AsString:=dm1.tblKarParLOKACIJA.AsString;
             dm1.qUpdateTip.ParamByName('m').AsString:=dm1.tblKarParMESEC.AsString;
             dm1.qUpdateTip.ParamByName('g').AsString:=dm1.tblKarParGODINA.AsString;
             dm1.qUpdateTip.ExecQuery;
             //ShowMessage('');
         // end
         // else
        //  begin
               //da ne se pravi storno
        //  end;
          //da se napravi nova presmetka, prevo treba da se promenat vleznite parametri
          //vodomeri sostijba, nova sostojba i gjubrarina (povrsina) da se izvlece od stavite


end;

procedure TfrmKarticaKorisnikNova.aEdinecnaExecute(Sender: TObject);
var frm:TfrmPoedinecnaPresmetka;
begin
    frm := TfrmPoedinecnaPresmetka.Create(nil,cxTxtPartnerID.Text,cxLookUpComboBoxTipPartner.Text, DM1.TblKomLokaciiPoPartnerID.AsString,cxTxtPartnerNaziv.Text,dm1.TblKomLokaciiPoPartnerREON_ID.asstring);
    frm.ShowModal();
  //  frmKarticaKorisnik.Hide;
    frm.Free();
   if (cxTxtPartnerID.Text<>'') and (cxDateDatumOd.Text<>'') then
    begin
      DM1.tblKarPar.Close;
      dm1.tblKarPar.ParamByName('tp').AsString:=cxLookUpComboBoxTipPartner.Text;
      dm1.tblKarPar.ParamByName('p').AsString:=cxTxtPartnerID.Text;
      dm1.tblKarPar.ParamByName('d1').AsString:=cxDateDatumOd.Text;
      dm1.tblKarPar.ParamByName('d2').Asdate:=EndOfTheMonth(StrToDate(cxDateDatumDo.Text));
      DM1.tblKarPar.open;
      dm1.tblSiteUplati.open;
      dm1.tblSiteStavki.Open;
    end;
end;

procedure TfrmKarticaKorisnikNova.aEdnaBeleskaFizickiExecute(
  Sender: TObject);
  var x:TextFile; uplateno:real;
begin
    TblPresmetkaG.close;
    TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
                                                         ' and kla.id_lokacija='+dm1.tblKarParLOKACIJA.AsString+
                                                         ' and kpg.broj_faktura ='''+cxGrid1DBTableView1BROJ_FAKTURA.EditValue+
                                                         ''' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;
    TblPresmetkaG.open;
    TblPresmetkaS.Open;
    TblPresmetkaVodomeriSostojba.open;
    TblSaldo.Close;
    frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
    if (frmDaNe.ShowModal <> mrYes) then
    begin
        TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(TblPresmetkaGGODINA.Value,TblPresmetkaGMESEC.Value)+15);
        TblSaldo.Open;
    end
    else
    begin
      TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
      TblSaldo.Open;
    end;

    if TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;
        if dm1.tblKarParPOBARUVA.AsString='' then
           uplateno:=0
        else
          uplateno:=dm1.tblKarParPOBARUVA.AsFloat;
        frxReport1.LoadFromFile('FRBeleskaFizickoLice1.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TfrmKarticaKorisnikNova.aIzbraniFakturiExecute(Sender: TObject);
var vk_selectirani,i :Integer; br_beleski:String;
begin
  br_beleski:='';
   vk_selectirani := cxGrid1DBTableView1.Controller.SelectedRecordCount;
    for i:=0 to vk_selectirani-1 do
    begin
       if br_beleski<>'' then
          br_beleski:=br_beleski+','+QuotedStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0])
       else
       begin
          br_beleski:=QuotedStr(cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0]);
       end;
    end;
        TblPresmetkaG.close;
        TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
                                                         ' and kla.id_lokacija='+dm1.tblKarParLOKACIJA.AsString+
                                                         ' and kpg.broj_faktura in ('+br_beleski+')'+
                                                         ' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;

        TblPresmetkaG.open;
        TblSaldo.Close;
        frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(TblPresmetkaGGODINA.Value,TblPresmetkaGMESEC.Value)+30);
            TblSaldo.Open;
        end
        else
        begin
            TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
            TblSaldo.Open;
        end;
       // TblPresmetkaS.Open;
        //TblPresmetkaVodomeriSostojba.open;
      //  frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
      //  if (frmDaNe.ShowModal <> mrYes) then
     //   begin
      //      TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(TblPresmetkaGGODINA.Value,TblPresmetkaGMESEC.Value)+30);
      //      TblSaldo.Open;
       // end
      //  else
      //  begin
          //  TblSaldo.ParamByName('datum_od').AsDate:=strtodate(cxDateDatumOd.Text);
           // TblSaldo.Open;
      //  end;
            if TblPresmetkaG.RecordCount > 0 then
            begin
                frxReport1.LoadFromFile('FRFaktura.fr3');
                frxReport1.PrepareReport(true);
                frxReport1.ShowPreparedReport();
            end
            else
            begin
                ShowMessage('���� �������� �� ��������!');
            end;

end;

procedure TfrmKarticaKorisnikNova.aKorekcijaExecute(Sender: TObject);
begin
  inherited;
   // ��������, ���� � ������� ���������
       if (dm1.tblKarParAKTIVEN.Value=0) and (dm1.tblKarPar.RecordCount>0) then
     begin
       ShowMessage('��������� �� � �������! �� �� ���� �� �� ������� ���� ����� �������, ����� �� � �������.');
       Abort;
     end;
       if (dm1.tblKarParPOBARUVA.AsFloat=0) //and (dm1.tblKarParSALDO.AsInteger=0)
        then
        begin
         frmDaNe := TfrmDaNe.Create(self,'����������',' ���� �� ������� �������� �� ���������?',1);
          if (frmDaNe.ShowModal = mrYes) then
          begin
             // ���� �� ��������� �����

          end;
        end;

end;

procedure TfrmKarticaKorisnikNova.cbKorisnikExit(Sender: TObject);
begin
 // inherited;
//  tblLokacija.Close;
//  tblLokacija.ParamByName('tp').AsString:=cxLookUpComboBoxTipPartner.Text;
//  tblLokacija.ParamByName('p').AsString:=cxTxtPartnerID.Text;
//  tblLokacija.ParamByName('d1').AsString:=cxDateDatumOd.Text;
//  tblLokacija.ParamByName('d2').Asdate:=EndOfTheMonth(StrToDate(cxDateDatumDo.Text));
//  tblLokacija.Open;

  DM1.tblKarPar.Close;
  dm1.tblKarPar.ParamByName('tp').AsString:=cxLookUpComboBoxTipPartner.Text;
  dm1.tblKarPar.ParamByName('p').AsString:=cxTxtPartnerID.Text;
  dm1.tblKarPar.ParamByName('d1').AsString:=cxDateDatumOd.Text;
  dm1.tblKarPar.ParamByName('d2').Asdate:=EndOfTheMonth(StrToDate(cxDateDatumDo.Text));
//  dm1.tblKarPar.ParamByName('d2').AsString:=DateToStr(EndOfTheMonth(cxDateDatumDo.Date));
  DM1.tblKarPar.open;
  dm1.tblSiteUplati.open;
  dm1.tblSiteStavki.Open;
  dPanel.Enabled:=false;

  if (cxLookUpComboBoxTipPartner.Text='1') then
  begin
    aEdnaBeleskaFizicki.Enabled:=False;
    aPrintOdredeniBeleskiFizicki.Enabled:=False;
    aPoedinecnaFaktura.Enabled:=true;
    aIzbraniFakturi.Enabled:=true;
  end
  else
  begin
     aEdnaBeleskaFizicki.Enabled:=true;
    aPrintOdredeniBeleskiFizicki.Enabled:=true;
    aPoedinecnaFaktura.Enabled:=false;
    aIzbraniFakturi.Enabled:=false;
  end;
{  TblPresmetkaG.close;
  TblPresmetkaG.ParamByName('tp').AsString:=cxLookUpComboBoxTipPartner.Text;
  TblPresmetkaG.ParamByName('p').AsString:=cxTxtPartnerID.Text;
  TblPresmetkaG.open;

  TblPresmetkaS.Open;
  TblPresmetkaVodomeriSostojba.open;
  TblSaldo.Open;
 }
end;

procedure TfrmKarticaKorisnikNova.cxBtnPartneriClick(Sender: TObject);
begin
  inherited;
       callFrmPartneri();
end;

procedure TfrmKarticaKorisnikNova.cxDateDatumDoExit(Sender: TObject);
begin
  //inherited;
    cbKorisnikExit(Sender);
end;

procedure TfrmKarticaKorisnikNova.cxDBTextEdit2Click(Sender: TObject);
begin
  inherited;

  cxBtnPartneriClick(Sender);
end;

procedure TfrmKarticaKorisnikNova.cxGrid1DBTableView1DataControllerDetailCollapsed(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
  inherited;
 ADataController.ClearDetailLinkObject(ARecordIndex, 0);
end;

procedure TfrmKarticaKorisnikNova.cxGrid1DBTableView1DataControllerDetailExpanded(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
  inherited;
  ADataController.FocusedRecordIndex := ARecordIndex;
end;

procedure TfrmKarticaKorisnikNova.cxGrid1DBTableView1DataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
  inherited;
  ADataController.CollapseDetails;
end;

procedure TfrmKarticaKorisnikNova.cxLookUpComboBoxTipPartner2Exit(Sender: TObject);
begin
  inherited;
  if cxLookUpComboBoxTipPartner.Text<>'' then
       dmmat.tblPartner.Filter:='TIP_PARTNER='+QuotedStr(cxLookUpComboBoxTipPartner.Text);
  //dm1.TblKomLokaciiPoPartner.Filtered:=True;
 // dm.tblCenaTutun.Open;
end;

procedure TfrmKarticaKorisnikNova.cxTextEdit1Exit(Sender: TObject);
begin
  inherited;
  cxBtnPartneriClick(Sender);
end;

procedure TfrmKarticaKorisnikNova.cxTxtPartnerID2Exit(Sender: TObject);
begin
  inherited;
  if (cxTxtPartnerID.Text<>'') and (cxLookUpComboBoxTipPartner.Text<>'') then
  begin
     dmmat.tblPartner.Filter:='ID='+QuotedStr(cxTxtPartnerID.Text);
     dmmat.tblPartner.Filtered:=True;
  end
  else if cxLookUpComboBoxTipPartner.Text<>'' then
        dmmat.tblPartner.Filtered:=True;
end;

procedure TfrmKarticaKorisnikNova.cxTxtPartnerNazivEnter(Sender: TObject);
begin
  inherited;
//   dmmat.tblPartner.close;
  // dmmat.tblPartner.ParamByName('tp').AsString:='%';
  dmmat.tblPartner.open;
  if (cxLookUpComboBoxTipPartner.Text<>'') and (cxTxtPartnerID.Text<>'') then
  begin
   if dmmat.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([cxLookUpComboBoxTipPartner.Text, cxTxtPartnerID.text]),[]) then
    begin
      setGridForLokacii(dmmat.tblPartnerTIP_PARTNER.AsString,dmmat.tblPartnerID.AsString);
      cxDateDatumOd.SetFocus;
    end;
   end
    else
    begin
        cxBtnPartneriClick(Sender);
    end;

end;

procedure TfrmKarticaKorisnikNova.cxTxtPartnerNazivExit(Sender: TObject);
begin
 // inherited;
if (cxLookUpComboBoxTipPartner.Text<>'') and (cxTxtPartnerID.Text<>'') then
begin
  if not dmmat.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([cxLookUpComboBoxTipPartner.Text,cxTxtPartnerID.text]),[]) then
      cxBtnPartneriClick(Sender)
  else cxTxtPartnerNaziv.Text:=dmmat.tblPartnerNAZIV.Value;
end
else cxLookUpComboBoxTipPartner.SetFocus;
end;

procedure TfrmKarticaKorisnikNova.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  TblPresmetkaG.Close;
  dm1.tblKarPar.Close;
  dmmat.tblPartner.Close;
  dmmat.tblPartner.Filtered:=false;
end;

procedure TfrmKarticaKorisnikNova.FormCreate(Sender: TObject);
begin
  //inherited;
  dmmat.tblPartner.close;
  PresmetkaPoFaktura:='SELECT KPG.ID,KPG.LOKACIIN_ID,KPG.GODINA,KPG.MESEC,KLA.TIP_PARTNER,KLA.PARTNER PARTNER_ID,MP.NAZIV,MP.ADRESA,KPG.IZNOS_VKUPNO'+
                      ',NULL NAZIV1,0.0 IZNOS_USLUGA_NETO,0.0 DANOK,0.0 IZNOS_USLUGA'+
                      ',KPG.VRABOTEN,lastdaymonth(ENCODEDATE(1, KPG.MESEC, KPG.GODINA)) datum_presmetka, KPG.BROJ_FAKTURA'+
                      ',KL.REON_ID,KL.CITACKA_KNIGA, KL.RB1, KL.STANBENA_POVRSINA, KL.DVORNA_POVRSINA'+
                      ',(SELECT'+
                      ' SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0) ELSE 0 END)'+
                      ' FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'+
                      ' WHERE KPS.PRESMETKA_G_ID=KPG.ID'+
                      ' AND KC.USLUGA=KPS.USLUGA'+
                      ' AND KC.VID_USLUGA=KPS.VID_USLUGA'+
                      ' AND KC.GODINA=KPS.GODINA'+
                      ' AND KC.MESEC=KPS.MESEC'+
                      ' AND KPG.TIP=1) OSNOVICA_DANOK_05'+
                      ',(SELECT'+
                      ' SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.IZNOS_USLUGA_NETO,0) ELSE 0 END)'+
                      ' FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'+
                      ' WHERE KPS.PRESMETKA_G_ID=KPG.ID'+
                      ' AND KC.USLUGA=KPS.USLUGA'+
                      ' AND KC.VID_USLUGA=KPS.VID_USLUGA'+
                      ' AND KC.GODINA=KPS.GODINA'+
                      ' AND KC.MESEC=KPS.MESEC'+
                      ' AND KPG.TIP=1) OSNOVICA_DANOK_18'+
                      ',(SELECT'+
                      ' SUM(CASE WHEN KC.DANOK=5 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'+
                      ' FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'+
                      ' WHERE KPS.PRESMETKA_G_ID=KPG.ID'+
                      ' AND KC.USLUGA=KPS.USLUGA'+
                      ' AND KC.VID_USLUGA=KPS.VID_USLUGA'+
                      ' AND KC.GODINA=KPS.GODINA'+
                      ' AND KC.MESEC=KPS.MESEC'+
                      ' AND KPG.TIP=1) DANOK_05'+
                      ',(SELECT'+
                      ' SUM(CASE WHEN KC.DANOK=18 THEN COALESCE(KPS.DANOK,0) ELSE 0 END)'+
                      ' FROM KOM_PRESMETKA_S KPS,KOM_CENOVNIK KC'+
                      ' WHERE KPS.PRESMETKA_G_ID=KPG.ID'+
                      ' AND KC.USLUGA=KPS.USLUGA'+
                      ' AND KC.VID_USLUGA=KPS.VID_USLUGA'+
                      ' AND KC.GODINA=KPS.GODINA'+
                      ' AND KC.MESEC=KPS.MESEC'+
                      ' AND KPG.TIP=1) DANOK_18,'+
                     // ' lastdaymonth(ENCODEDATE(1, KPG.MESEC, KPG.GODINA))+15 da_se_plati, '+
                      ' KPG.DDO, '+
                      ' KPG.ID_KOREKCIJA '+
                      ' FROM KOM_PRESMETKA_G KPG'+
                      ', KOM_LOKACII KL, MAT_PARTNER MP,'+
                      ' KOM_lokacija_arhiva kla' +
                      ' WHERE'+
                      ' kla.id_lokacija=kl.id'+
                      ' AND KPG.LOKACIIN_ID=KLA.ID_LOKACIJA'+
                      ' AND KLA.TIP_PARTNER=MP.TIP_PARTNER'+
                      ' AND KLA.PARTNER=MP.ID'+
                      ' and kpg.tip=1 '+
                      ' and (((encodedate(1,kpg.mesec,kpg.godina) between encodedate(1,kla.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.kraj_godina)) and kla.kraj_mesec is not null) '+
                      ' or ((encodedate(1,kpg.mesec,kpg.godina) >= encodedate(1,kla.poc_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc_mesec is not null) ))'
end;

procedure TfrmKarticaKorisnikNova.FormShow(Sender: TObject);
begin
 // inherited;
  dPanel.Enabled:=True;
  dmmat.tblPartner.Cancel;
 // tblKorisnik.Close;
 // tblKorisnik.Open;
//  dmmat.tblPartner.Insert;
  PrvPosledenTab(dPanel,posledna,prva);
  ActiveControl:=cxLookUpComboBoxTipPartner;
//  tblKorisnik.insert;
end;

procedure TfrmKarticaKorisnikNova.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //inherited;
  case key of
        VK_RETURN:
          if TEdit(sender).Name='cxDateDatumDo' then
          begin
           if (Validacija(dPanel)) = False then
               cxDateDatumDoExit(Sender);
          end
          else
        begin

            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;

end;

procedure TfrmKarticaKorisnikNova.callFrmPartneri;
var frmP:TfrmPartner;
begin
    //partnerSet := false;

    frmP :=TfrmPartner.Create(nil,false);

    //frmP.getEdit(TEdit(cxTxtPartnerTIP_PARTNER),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.getEdit(tedit(cxLookUpComboBoxTipPartner),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.ShowModal();
    frmP.Free();

    if frmP.ModalResult = mrOk then
    begin
        setGridForLokacii(VarToStr(cxLookUpComboBoxTipPartner.EditValue),cxTxtPartnerID.Text);
        setUpLokationData();
    end;

end;

procedure TfrmKarticaKorisnikNova.setGridForLokacii(tip_partner:String;partner:String);
begin
    DM1.TblKomLokaciiPoPartner.Close;
    DM1.TblKomLokaciiPoPartner.ParamByName('TIP_PARTNER').AsString := tip_partner;
    DM1.TblKomLokaciiPoPartner.ParamByName('PARTNER').AsString := partner;

    DM1.TblKomLokaciiPoPartner.Open;
    txtAdresa.Text:=DM1.TblKomLokaciiPoPartnerNAZIV_ULICA.Text+' '+ dm1.TblKomLokaciiPoPartnerBROJ.AsString;;
    txtReon.Text:=DM1.TblKomLokaciiPoPartnerNAZIV_REON.Value;
    txtCitKniga.Text:=DM1.TblKomLokaciiPoPartnerCITACKA_KNIGA.AsString;
    if cxLookUpComboBoxTipPartner.Text='5' then
       cxDateDatumOd.Text:=dm1.qSetupFizicki.FldByName['v1'].AsString //'01.01.2005'
    else
       cxDateDatumOd.Text:=dm1.qSetupDukani.FldByName['v1'].AsString;  //'01.01.2003';
    cxDateDatumDo.Text:=DateToStr(Now);
end;

procedure TfrmKarticaKorisnikNova.setUpLokationData();
begin
   // dPanel.Enabled := false;
   // if (DM1.TblKomLokaciiPoPartner.RecordCount = 1) then
  //  begin
        //setUpNeplateniZadolzuvanjaData(VarToStr(cxLookUpComboBoxTipPartner.EditValue),cxTxtPartnerID.Text,DM1.TblKomLokaciiPoPartnerID.AsString);

       // cxGrid1.SetFocus();
   // end
  //  else
   // begin
        //ShowMessage('�������� ������� �� ������!');
       // cxGrid1.SetFocus();
  //  end;

end;

procedure TfrmKarticaKorisnikNova.setUpNeplateniZadolzuvanjaData(tip_partner:String;partner:String;lokacija:String);
begin
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('TIP_PARTNER').AsString := tip_partner;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('PARTNER').AsString := partner;
    DM1.TblNeplateniZadolzuvanjaPoLokacija.ParamByName('LOKACIJA').AsString := lokacija;

    DM1.TblNeplateniZadolzuvanjaPoLokacija.Open();

    if DM1.TblNeplateniZadolzuvanjaPoLokacijaRAZLIKA.Value=0 then
    begin
        ShowMessage('��������� � ��������!');
        Abort;
    end
    else
    begin
   //    cxDateDatumUplata.Enabled := true;
    //   cxMaskPlateno.Enabled := true;
    end;
end;



end.
