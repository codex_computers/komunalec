inherited frmTipUplati: TfrmTipUplati
  Caption = #1058#1080#1087' '#1085#1072' '#1091#1087#1083#1072#1090#1080
  ClientHeight = 610
  ClientWidth = 688
  ExplicitWidth = 704
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 688
    Height = 275
    ExplicitWidth = 688
    ExplicitHeight = 275
    inherited cxGrid1: TcxGrid
      Width = 684
      Height = 271
      ExplicitWidth = 684
      ExplicitHeight = 271
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.dsTipUplata
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 265
        end
        object cxGrid1DBTableView1KONTO: TcxGridDBColumn
          Caption = #1050#1086#1085#1090#1086
          DataBinding.FieldName = 'KONTO'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 127
        end
        object cxGrid1DBTableView1VID: TcxGridDBColumn
          Caption = #1042#1080#1076
          DataBinding.FieldName = 'VID'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Width = 83
        end
        object cxGrid1DBTableView1KAMATA: TcxGridDBColumn
          Caption = #1050#1072#1084#1072#1090#1072
          DataBinding.FieldName = 'KAMATA'
          HeaderAlignmentHorz = taCenter
          Width = 75
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 401
    Width = 688
    Height = 186
    ExplicitTop = 401
    ExplicitWidth = 688
    ExplicitHeight = 186
    inherited Label1: TLabel
      Left = 20
      Width = 43
      Caption = #1064#1080#1092#1088#1072' '
      ExplicitLeft = 20
      ExplicitWidth = 43
    end
    object Label2: TLabel [1]
      Left = 20
      Top = 57
      Width = 50
      Height = 13
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 20
      Top = 108
      Width = 50
      Height = 13
      AutoSize = False
      Caption = #1042#1080#1076
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 157
      Top = 108
      Width = 214
      Height = 21
      AutoSize = False
      Caption = '1 - '#1041#1083#1072#1075#1072#1112#1085#1072' / 2 - '#1041#1072#1085#1082#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Konro: TLabel [4]
      Left = 20
      Top = 82
      Width = 50
      Height = 15
      AutoSize = False
      Caption = #1050#1086#1085#1090#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [5]
      Left = 157
      Top = 137
      Width = 324
      Height = 21
      AutoSize = False
      Caption = #1054#1079#1085#1072#1095#1080', '#1072#1082#1086' '#1090#1088#1077#1073#1072' '#1082#1072#1084#1072#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 70
      Top = 16
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.dsTipUplata
      ExplicitLeft = 70
      ExplicitTop = 16
    end
    inherited OtkaziButton: TcxButton
      Left = 597
      Top = 146
      TabOrder = 6
      ExplicitLeft = 597
      ExplicitTop = 146
    end
    inherited ZapisiButton: TcxButton
      Left = 516
      Top = 146
      TabOrder = 5
      ExplicitLeft = 516
      ExplicitTop = 146
    end
    object cxDBTextEdit1: TcxDBTextEdit
      Left = 70
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = DM1.dsTipUplata
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 225
    end
    object cxDBComboBox1: TcxDBComboBox
      Left = 70
      Top = 104
      DataBinding.DataField = 'VID'
      DataBinding.DataSource = DM1.dsTipUplata
      Properties.Items.Strings = (
        '1'
        '2'
        '3')
      TabOrder = 4
      OnKeyDown = EnterKakoTab
      Width = 76
    end
    object cxDBKonto: TcxDBTextEdit
      Left = 70
      Top = 78
      DataBinding.DataField = 'KONTO'
      DataBinding.DataSource = DM1.dsTipUplata
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 56
    end
    object cxDBLookupKonto: TcxDBLookupComboBox
      Left = 126
      Top = 78
      DataBinding.DataField = 'KONTO'
      DataBinding.DataSource = DM1.dsTipUplata
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'SIFRA'
      Properties.ListColumns = <
        item
          FieldName = 'SIFRA'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.dsKontenPlan
      TabOrder = 3
      OnKeyDown = EnterKakoTab
      Width = 309
    end
    object cbKamata: TcxDBCheckBox
      Left = 20
      Top = 132
      AutoSize = False
      Caption = #1050#1072#1084#1072#1090#1072
      DataBinding.DataField = 'KAMATA'
      DataBinding.DataSource = DM1.dsTipUplata
      Properties.NullStyle = nssInactive
      Properties.ValueChecked = '1'
      Properties.ValueUnchecked = '0'
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
      TabOrder = 7
      Height = 21
      Width = 126
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 688
    ExplicitWidth = 688
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 587
    Width = 688
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080',F9 - '#1047#1072#1087#1080#1096#1080', Es' +
          'c - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
    ExplicitTop = 587
    ExplicitWidth = 688
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 216
  end
  inherited PopupMenu1: TPopupMenu
    Left = 256
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 296
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.442174699080000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
