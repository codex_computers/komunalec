object frmSlikiVodomeri: TfrmSlikiVodomeri
  Left = 0
  Top = 0
  Caption = #1057#1083#1080#1082#1080' '#1086#1076' '#1042#1086#1076#1086#1084#1077#1088#1080
  ClientHeight = 605
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelGoren: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 153
    Align = alTop
    BevelInner = bvLowered
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object lblMesec: TLabel
      Left = 43
      Top = 90
      Width = 44
      Height = 18
      Caption = #1052#1077#1089#1077#1094
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblGodina: TLabel
      Left = 43
      Top = 65
      Width = 48
      Height = 18
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblReon: TLabel
      Left = 43
      Top = 40
      Width = 32
      Height = 18
      Caption = #1056#1077#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxBtnPresmetaj: TcxButton
      Left = 363
      Top = 84
      Width = 121
      Height = 26
      Caption = #1055#1088#1080#1082#1072#1078#1080
      OptionsImage.Images = dmRes.cxIcoDBSmall
      TabOrder = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = cxBtnPresmetajClick
    end
    object cxComboBoxGodini: TcxComboBox
      Left = 157
      Top = 63
      BeepOnEnter = False
      TabOrder = 1
      OnKeyDown = onKeyDownLookUpAll
      Width = 131
    end
    object cxComboBoxMesec: TcxComboBox
      Left = 157
      Top = 89
      BeepOnEnter = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
      TabOrder = 2
      OnKeyDown = onKeyDownLookUpAll
      Width = 131
    end
    object GroupBoxPoslednaPresmetka: TGroupBox
      Left = 790
      Top = 2
      Width = 183
      Height = 149
      Align = alRight
      Caption = #1055#1086#1089#1083#1077#1076#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      TabOrder = 4
      Visible = False
      object lblMesecLast: TLabel
        Left = 16
        Top = 40
        Width = 31
        Height = 13
        Caption = #1052#1077#1089#1077#1094
      end
      object lblMesecLastValue: TLabel
        Left = 80
        Top = 40
        Width = 53
        Height = 13
        Caption = '%'#1052#1077#1089#1077#1094'%'
      end
      object lblGodinaLast: TLabel
        Left = 16
        Top = 15
        Width = 37
        Height = 13
        Caption = #1043#1086#1076#1080#1085#1072
      end
      object lblGodinaLastValue: TLabel
        Left = 80
        Top = 15
        Width = 59
        Height = 13
        Caption = '%'#1043#1086#1076#1080#1085#1072'%'
      end
      object cxBtnIzbrisiPoslednaPresmetka: TcxButton
        Left = 8
        Top = 65
        Width = 172
        Height = 25
        Caption = #1048#1079#1073#1088#1080#1096#1080' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
        Enabled = False
        TabOrder = 0
        Visible = False
      end
    end
    object cxLookupCBReoni: TcxLookupComboBox
      Left = 106
      Top = 36
      BeepOnEnter = False
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          Width = 100
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1080#1074
          FieldName = 'NAZIV'
        end>
      Properties.ListOptions.AnsiSort = True
      Properties.ListSource = DM1.DSKomReoni
      TabOrder = 0
      OnKeyDown = onKeyDownLookUpAll
      Width = 182
    end
    object cxMemo1: TcxMemo
      Left = 534
      Top = 14
      Lines.Strings = (
        'cxMemo1')
      ParentFont = False
      Properties.ScrollBars = ssBoth
      Properties.WordWrap = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 5
      Visible = False
      Height = 73
      Width = 390
    end
    object dbimgIMG: TDBImage
      Left = 450
      Top = -6
      Width = 105
      Height = 105
      DataField = 'IMG'
      DataSource = DM1.dsSlikiVodomeri
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      Visible = False
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 586
    Width = 975
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 
      'F10 - '#1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072', F11 - '#1055#1077#1095#1072#1090#1080' '#1080#1079#1074#1077#1096#1090#1072#1112',  CTRL+ E - Excel, Ct' +
      'rl + I - '#1057#1085#1080#1084#1080' '#1080#1079#1075#1083#1077#1076',  ESC - '#1048#1079#1083#1077#1079
  end
  object panelDolen: TPanel
    Left = 0
    Top = 153
    Width = 975
    Height = 433
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 971
      Height = 429
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.dsSlikiVodomeri
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            FieldName = 'SLIKA'
            Column = cxGrid1DBTableView1SLIKA
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.Indicator = True
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1056#1077#1076'.'#1073#1088'.'
          DataBinding.ValueType = 'String'
          OnGetDisplayText = cxGrid1DBTableView1Column1GetDisplayText
          Width = 57
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'ID'
          Width = 86
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARTNER'
          Width = 36
        end
        object cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER_ID'
          Width = 72
        end
        object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV_PARTNER'
          Width = 189
        end
        object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
          Caption = #1056#1077#1086#1085
          DataBinding.FieldName = 'REON_ID'
          Visible = False
          Width = 94
        end
        object cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn
          Caption = #1041#1088'.'#1042#1086#1076#1086#1084#1077#1088
          DataBinding.FieldName = 'VODOMERI_ID'
          Width = 92
        end
        object cxGrid1DBTableView1ULICA: TcxGridDBColumn
          Caption = #1059#1083#1080#1094#1072
          DataBinding.FieldName = 'ULICA'
          Width = 115
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          Caption = #1041#1088'.'
          DataBinding.FieldName = 'BROJ'
          Width = 57
        end
        object cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn
          Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          DataBinding.FieldName = 'STARA_SOSTOJBA'
          Width = 80
        end
        object cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn
          Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          DataBinding.FieldName = 'NOVA_SOSTOJBA'
          Width = 96
        end
        object cxGrid1DBTableView1RAZLIKA: TcxGridDBColumn
          Caption = #1056#1072#1079#1083#1080#1082#1072
          DataBinding.FieldName = 'RAZLIKA'
          Visible = False
          Width = 81
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          Visible = False
          Width = 81
        end
        object cxGrid1DBTableView1MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
          Visible = False
          Width = 81
        end
        object cxGrid1DBTableView1SLIKA: TcxGridDBColumn
          Caption = #1048#1084#1072' '#1089#1083#1080#1082#1072
          DataBinding.FieldName = 'SLIKA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1053#1045
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1044#1040
              Value = 1
            end>
          Width = 63
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object ActionList1: TActionList
    Left = 699
    Top = 16
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aCallFrfBeleskiPrint: TAction
      Caption = 'aCallFrfBeleskiPrint'
      OnExecute = aCallFrfBeleskiPrintExecute
    end
    object aPrintAllBeleskiFizicki: TAction
      Caption = 'aPrintAllBeleskiFizicki'
      OnExecute = aPrintAllBeleskiFizickiExecute
    end
    object aPrintBeleskaOdberenFizicki: TAction
      Caption = 'aPrintBeleskaOdberenFizicki'
    end
    object aPrintAllFakturi: TAction
      Caption = 'aPrintAllFakturi'
      ShortCut = 115
      OnExecute = aPrintAllFakturiExecute
    end
    object aEdnaFaktura: TAction
      Caption = 'aEdnaFaktura'
      OnExecute = aEdnaFakturaExecute
    end
    object aExporttoExcel: TAction
      Caption = 'aExporttoExcel'
      ShortCut = 16453
      OnExecute = aExporttoExcelExecute
    end
    object aMailFaktura: TAction
      Caption = 'aMailFaktura'
      ShortCut = 16461
      OnExecute = aMailFakturaExecute
    end
    object aMailFakturaDizajn: TAction
      Caption = 'aMailFakturaDizajn'
      ShortCut = 24653
      OnExecute = aMailFakturaDizajnExecute
    end
    object aNovIzbledB: TAction
      Caption = 'aNovIzbledB'
      ShortCut = 120
    end
    object aNovIzgledBDizajn: TAction
      Caption = 'aNovIzgledBDizajn'
      ShortCut = 24642
    end
    object aNovIzgledSiteB: TAction
      Caption = 'aNovIzgledSiteB'
    end
    object aPecati: TAction
      Caption = 'aPecati'
      ShortCut = 122
      OnExecute = aPecatiExecute
    end
    object aPecatiDizajner: TAction
      Caption = 'aPecatiDizajner'
      ShortCut = 24697
      OnExecute = aPecatiDizajnerExecute
    end
    object aPrint: TAction
      Caption = 'aPrint'
      ShortCut = 121
      OnExecute = aPrintExecute
    end
    object aSnimiIzgled: TAction
      Caption = 'aSnimiIzgled'
      ShortCut = 16457
      OnExecute = aSnimiIzgledExecute
    end
  end
  object frxDSPresmetkaG: TfrxDBDataset
    UserName = 'frxDSPresmetkaG'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_VKUPNO=IZNOS_VKUPNO'
      'NAZIV1=NAZIV1'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'DANOK=DANOK'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'VRABOTEN=VRABOTEN'
      'ADRESA=ADRESA'
      'CITACKA_KNIGA=CITACKA_KNIGA'
      'DANOK_05=DANOK_05'
      'DANOK_18=DANOK_18'
      'OSNOVICA_DANOK_05=OSNOVICA_DANOK_05'
      'OSNOVICA_DANOK_18=OSNOVICA_DANOK_18'
      'STANBENA_POVRSINA=STANBENA_POVRSINA'
      'DVORNA_POVRSINA=DVORNA_POVRSINA'
      'DA_SE_PLATI=DA_SE_PLATI'
      'DATUM_PRESMETKA=DATUM_PRESMETKA'
      'CITACKA_KNIGA_RB=CITACKA_KNIGA_RB'
      'ID=ID'
      'DDO=DDO'
      'RB2=RB2'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER_ID=PARTNER_ID'
      'NAZIV=NAZIV'
      'MAIL=MAIL'
      'BROJ_FAKTURA=BROJ_FAKTURA'
      'VK_POTROSENO=VK_POTROSENO'
      'GODINA=GODINA'
      'MESEC=MESEC'
      'REON_ID=REON_ID'
      'LOKACIIN_ID=LOKACIIN_ID'
      'IMA_SLIKA=IMA_SLIKA')
    OpenDataSource = False
    DataSource = DM1.dsPecDukani
    BCDToCurrency = False
    Left = 734
    Top = 86
  end
  object frxDSPresmetkaS: TfrxDBDataset
    UserName = 'frxDSPresmetkaS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NAZIV=NAZIV'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'LOKACIIN_ID=LOKACIIN_ID'
      'CENA=CENA'
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'DANOK=DANOK'
      'PROCENT_DANOK=PROCENT_DANOK')
    OpenDataSource = False
    DataSource = DM1.dsDPresmetkaS
    BCDToCurrency = False
    Left = 769
    Top = 86
  end
  object frxDSPresmetkaVodomeriSostojba: TfrxDBDataset
    UserName = 'frxDSPresmetkaVodomeriSostojba'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LOKACIIN_ID=LOKACIIN_ID'
      'VODOMERI_ID=VODOMERI_ID'
      'NOVA_SOSTOJBA=NOVA_SOSTOJBA'
      'STARA_SOSTOJBA=STARA_SOSTOJBA'
      'SLIKA=SLIKA'
      'IMG=IMG')
    OpenDataSource = False
    DataSource = DM1.dsDVodomeriSostojba
    BCDToCurrency = False
    Left = 909
    Top = 51
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 804
    Top = 16
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1040#1082#1094#1080#1080
      'Default')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    PopupMenuLinks = <>
    Style = bmsOffice11
    SunkenBorder = True
    UseSystemFont = True
    Left = 769
    Top = 16
    PixelsPerInch = 96
    object SubMenuAkcii: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'BarButtonPrintBeleskiAll'
        end
        item
          Visible = True
          ItemName = 'BarButtonPrintBeleskaSelected'
        end
        item
          Visible = True
          ItemName = 'BarButtonPrintFakturiAll'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzbrisiPresmetka'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzlez'
        end>
    end
    object BarButtonIzlez: TdxBarButton
      Action = aIzlez
      Category = 0
    end
    object BarButtonPrintBeleskiAll: TdxBarButton
      Action = aPrintAllBeleskiFizicki
      Caption = #1055#1077#1095#1072#1090#1080' '#1073#1077#1083#1077#1096#1082#1080' '#1079#1072' '#1089#1080#1090#1077
      Category = 0
    end
    object BarButtonPrintBeleskaSelected: TdxBarButton
      Action = aPrintBeleskaOdberenFizicki
      Caption = #1055#1077#1095#1072#1090#1080' '#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1086#1076#1073#1077#1088#1077#1085
      Category = 0
    end
    object BarButtonPrintFakturiAll: TdxBarButton
      Action = aPrintAllFakturi
      Caption = #1055#1077#1095#1072#1090#1080' '#1092#1072#1082#1090#1091#1088#1080' '#1079#1072' '#1089#1080#1090#1077
      Category = 0
    end
    object BarButtonIzbrisiPresmetka: TdxBarButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      Category = 0
      Visible = ivAlways
      ShortCut = 119
    end
  end
  object tblsaldo: TfrxDBDataset
    UserName = 'frxDSSaldo'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SALDO=SALDO')
    OpenDataSource = False
    DataSource = DM1.dsDSaldo
    BCDToCurrency = False
    Left = 699
    Top = 86
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 734
    Top = 16
  end
  object frxDSPresmetkaStavki1: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LOKACIIN_ID=LOKACIIN_ID'
      'VODOMERI_ID=VODOMERI_ID'
      'NOVA_SOSTOJBA=NOVA_SOSTOJBA'
      'STARA_SOSTOJBA=STARA_SOSTOJBA'
      'RAZLIKA=RAZLIKA'
      'CENA_VODA=CENA_VODA'
      'DDV=DDV'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva1
    BCDToCurrency = False
    Left = 804
    Top = 86
  end
  object frxDSPresmetkaStavki2: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_KAN=CENA_KAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva2
    BCDToCurrency = False
    Left = 909
    Top = 86
  end
  object frxDSPresmetkaStavki3: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_STAN=CENA_STAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva3
    BCDToCurrency = False
    Left = 839
    Top = 86
  end
  object frxDSPresmetkaStavki4: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki4'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_DVOR=CENA_DVOR'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva4
    BCDToCurrency = False
    Left = 874
    Top = 86
  end
  object frxDSPresmetkaStavki5: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki5'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_MANIPULATIVEN=CENA_MANIPULATIVEN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva5
    BCDToCurrency = False
    Left = 874
    Top = 51
  end
  object frxDSPresmetkaStavki6: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki6'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FVODA=CENA_FVODA'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva6
    BCDToCurrency = False
    Left = 909
    Top = 16
  end
  object frxDSPresmetkaStavki7: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki7'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FKAN=CENA_FKAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva7
    BCDToCurrency = False
    Left = 699
    Top = 51
  end
  object frxReport1: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39609.625425208330000000
    ReportOptions.LastChange = 39609.625425208330000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 699
    Top = 121
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
    end
  end
  object frxDSPresmetkaStavki8: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki8'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_FKAN=CENA_FKAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.dsDPresmetkaPoUsluga8
    BCDToCurrency = False
    Left = 839
    Top = 16
  end
  object frxDSPresmetkaStavki10: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki10'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'DDV=DDV'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DM1.dsDPresmetkaPoUsluga10
    BCDToCurrency = False
    Left = 874
    Top = 16
  end
  object frxDSPresmetkaStavki61: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki61'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'DDV=DDV'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DM1.DSTblDPresmetkaPoUsluga61
    BCDToCurrency = False
    Left = 839
    Top = 51
  end
  object frxDSPresmetkaStavki11: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki11'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'DDV=DDV'
      'CENA_NADOMESTOK=CENA_NADOMESTOK'
      'NAZIV_VID_USLUGA=NAZIV_VID_USLUGA'
      'NAZIV_USLUGA=NAZIV_USLUGA')
    OpenDataSource = False
    DataSource = DM1.dsDPresmetkaPoUsluga11
    BCDToCurrency = False
    Left = 769
    Top = 51
  end
  object frxDSPresmetkaStavki12: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki12'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_STAN=CENA_STAN'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva12
    BCDToCurrency = False
    Left = 734
    Top = 51
  end
  object frxDSPresmetkaStavki121: TfrxDBDataset
    UserName = 'frxDSPresmetkaStavki121'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VID_USLUGA=VID_USLUGA'
      'USLUGA=USLUGA'
      'IZNOS_USLUGA_NETO=IZNOS_USLUGA_NETO'
      'IZNOS_USLUGA=IZNOS_USLUGA'
      'DANOK=DANOK'
      'CENA_DVOR=CENA_DVOR'
      'DDV=DDV')
    OpenDataSource = False
    DataSource = DM1.DSDPresmetkaPoUsluva121
    BCDToCurrency = False
    Left = 804
    Top = 51
  end
  object qUpdateImaSlika: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_vodomeri_sostojba_s G'
      'set G.ima_SLIKA = :IMA_SLIKA'
      'where G.LOKACIIN_ID = :LOKACIJA and'
      '      G.MESEC = :MESEC and'
      '      G.GODINA = :GODINA')
    Left = 734
    Top = 121
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 925
    Top = 51
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43729.507579097220000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object qProveriSostojba: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'SELECT'
      '  v.stara_sostojba'
      'FROM'
      '    KOM_VODOMERI_SOSTOJBA v'
      'where v.mesec = :mesec'
      'and v.godina = :godina '
      'and v.lokaciin_id = :lokacija'
      'and v.vodomeri_id  = :vodomer')
    Left = 667
    Top = 16
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qInsertVS: TpFIBUpdateObject
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'insert into KOM_VODOMERI_SOSTOJBA_S (LOKACIIN_ID, VODOMERI_ID, S' +
        'TARA_SOSTOJBA, NOVA_SOSTOJBA, GODINA, MESEC, REON_ID, SLIKA, ZAB' +
        'ELESKA,'
      '                                     IMG, IMA_SLIKA)'
      
        'values (:LOKACIJA, :VODOMER, :STARA_SOSTOJBA, :NOVA_SOSTOJBA, :G' +
        'ODINA, :MESEC, :REON_ID, :SLIKA, :ZABELESKA, :IMG, :IMA_SLIKA)  ')
    OrderInList = 0
    Left = 492
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteVS: TpFIBUpdateObject
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete from kom_vodomeri_sostojba_s')
    OrderInList = 0
    Left = 492
    Top = 99
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object IdHTTP2: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 312
    Top = 8
  end
end
