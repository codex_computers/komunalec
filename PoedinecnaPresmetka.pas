unit PoedinecnaPresmetka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, StdCtrls, ComCtrls, ExtCtrls, cxGraphics, Menus,
  cxLookAndFeelPainters, cxButtons, cxMaskEdit, cxDropDownEdit, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet, FIBQuery,
  pFIBQuery, cxLookAndFeels, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, Utils,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator;

type
  TfrmPoedinecnaPresmetka = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    cxTxtPartnerID: TcxTextEdit;
    cxTxtPartnerNaziv: TcxTextEdit;
    cxComboBoxGodini: TcxComboBox;
    cxComboBoxMesec: TcxComboBox;
    lblMesec: TLabel;
    lblGodina: TLabel;
    cxBtnPresmetaj: TcxButton;
    cxLookUpComboBoxTipPartner: TcxTextEdit;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    TblPresmetkaS: TpFIBDataSet;
    DSPresmetka1: TDataSource;
    DSPresmetka: TDataSource;
    TblPresmetkaG: TpFIBDataSet;
    TblPresmetkaSLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaSMESEC: TFIBIntegerField;
    TblPresmetkaSGODINA: TFIBIntegerField;
    TblPresmetkaSVID_USLUGA: TFIBIntegerField;
    TblPresmetkaSUSLUGA: TFIBIntegerField;
    TblPresmetkaSIZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaSIZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaSDATUM_PRESMETKA: TFIBDateTimeField;
    TblPresmetkaSVRABOTEN: TFIBStringField;
    TblPresmetkaSDANOK: TFIBBCDField;
    TblPresmetkaSCENA: TFIBBCDField;
    cxtxtLokacija: TcxTextEdit;
    cxtxtReon: TcxTextEdit;
    qMaks: TpFIBQuery;
    pFIBQuery1: TpFIBQuery;
    TblPresmetkaSID: TFIBIntegerField;
    TblPresmetkaSPRESMETKA_G_ID: TFIBIntegerField;
    qGen: TpFIBQuery;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView4: TcxGridDBTableView;
    cxGrid1DBTableView4LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView4MESEC: TcxGridDBColumn;
    cxGrid1DBTableView4GODINA: TcxGridDBColumn;
    cxGrid1DBTableView4VID_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView4USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView4IZNOS_USLUGA_NETO: TcxGridDBColumn;
    cxGrid1DBTableView4IZNOS_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView4DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView4VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView4DANOK: TcxGridDBColumn;
    cxGrid1DBTableView4CENA: TcxGridDBColumn;
    cxGrid1DBTableView4ID: TcxGridDBColumn;
    cxGrid1DBTableView4PRESMETKA_G_ID: TcxGridDBColumn;
    cxGrid1DBTableView3LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView3MESEC: TcxGridDBColumn;
    cxGrid1DBTableView3GODINA: TcxGridDBColumn;
    cxGrid1DBTableView3IZNOS_VKUPNO: TcxGridDBColumn;
    cxGrid1DBTableView3DATUM_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView3VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView3BROJ_FAKTURA: TcxGridDBColumn;
    cxGrid1DBTableView3IZNOS_PLATENO: TcxGridDBColumn;
    cxGrid1DBTableView3RE: TcxGridDBColumn;
    cxGrid1DBTableView3TIP_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView3NALOG: TcxGridDBColumn;
    cxGrid1DBTableView3TS_KNIZENJE: TcxGridDBColumn;
    cxGrid1DBTableView3ID: TcxGridDBColumn;
    cxGrid1DBTableView3TIP: TcxGridDBColumn;
    qIdKorekcija: TpFIBQuery;
    TblPresmetkaGID: TFIBIntegerField;
    TblPresmetkaGLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaGMESEC: TFIBIntegerField;
    TblPresmetkaGGODINA: TFIBIntegerField;
    TblPresmetkaGIZNOS_VKUPNO: TFIBBCDField;
    TblPresmetkaGDATUM_PRESMETKA: TFIBDateTimeField;
    TblPresmetkaGVRABOTEN: TFIBStringField;
    TblPresmetkaGBROJ_FAKTURA: TFIBStringField;
    TblPresmetkaGIZNOS_PLATENO: TFIBBCDField;
    TblPresmetkaGRE: TFIBIntegerField;
    TblPresmetkaGTIP_NALOG: TFIBStringField;
    TblPresmetkaGNALOG: TFIBIntegerField;
    TblPresmetkaGTS_KNIZENJE: TFIBDateTimeField;
    TblPresmetkaGTIP: TFIBIntegerField;
    procedure cxBtnPresmetajClick(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function podatociOk():boolean;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    
  public
    { Public declarations }
    constructor Create(Owner : TComponent; id : string; tip_partner:string;  lokacija: string; naziv:string; reon:string);reintroduce; overload;
  end;

var
  frmPoedinecnaPresmetka: TfrmPoedinecnaPresmetka;
  izlez:Integer;
implementation

uses DaNe, dmKonekcija, dmMaticni, UnitDM1, IzberiLokacija;

{$R *.dfm}

constructor TfrmPoedinecnaPresmetka.Create(Owner: TComponent; id,tip_partner, lokacija,  naziv, reon: string);
begin
   inherited Create(Owner);
    cxTxtPartnerID.Text := id;
    cxLookUpComboBoxTipPartner.EditValue :=tip_partner;
    cxtxtLokacija.Text:=lokacija;
    cxTxtPartnerNaziv.Text := naziv;
    cxtxtReon.Text := reon;
end;

procedure TfrmPoedinecnaPresmetka.cxBtnPresmetajClick(Sender: TObject);
var mesec:string; broj,id:integer;
begin
//inherited;
if (Validacija(Panel1)) = False then
begin
  if DM1.TblKomLokacii.Active = true then
    begin
        DM1.TblKomLokacii.Close;
    end;
    DM1.TblKomLokacii.Close;
    DM1.TblKomLokacii.ParamByName('partner_id').AsString := cxTxtPartnerID.text;
    DM1.TblKomLokacii.ParamByName('tip_partner').AsString := cxLookUpComboBoxTipPartner.Text;
    DM1.TblKomLokacii.Open;

    //nova forma za izbor na lokacija za koja kje se pravi presmetka , ako ima povekje od edna lokacija
     dm1.TblKomLokacii.FetchAll;
     if dm1.TblKomLokacii.RecordCount>1 then
     begin
        frmIzberiLokacija:=TfrmIzberiLokacija.Create(Application);
        frmIzberiLokacija.ShowModal;
        izlez:=frmIzberiLokacija.Tag;
        frmIzberiLokacija.Free;
     end;
  if izlez=0 then
  begin
    dm1.qDaliImaPresmetka.Close;
    dm1.qDaliImaPresmetka.ParamByName('l').AsString:=DM1.TblKomLokaciiID.AsString;
    dm1.qDaliImaPresmetka.ParamByName('m').AsString:=cxComboBoxMesec.Text;
    dm1.qDaliImaPresmetka.ParamByName('g').AsString:=cxComboBoxGodini.text;
    dm1.qDaliImaPresmetka.ExecQuery;

    if not dm1.qDaliImaPresmetka.FldByName['BROJ_FAKTURA'].isnull then
    begin
       ShowMessage('��� ��� ��������� �� ���������� �������, ����� � ������');
    end
    else
    begin
      // vednash se pravi presmetka za soodvetnata lokacija
         if podatociOk then
                      begin
                          //�������� ���� �� ������ ��������� ����� �������� ���������
                          qIdKorekcija.Close;
                          qIdKorekcija.ParamByName('lokacija').AsString :=DM1.TblKomLokaciiID.AsString;
                          qIdKorekcija.ParamByName('mesec').AsString := cxComboBoxMesec.EditValue;
                          qIdKorekcija.ParamByName('GODINA').AsString := cxComboBoxGodini.EditValue;
                          qIdKorekcija.ExecQuery;

                          DM1.ProcKomPresmetkaEdinecna.close;
                          if cxtxtReon.Text='' then cxtxtReon.Text:='%';
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('REON_ID').AsString := dm1.TblKomLokaciiREON_ID.AsString;
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('GODINA').AsString := cxComboBoxGodini.EditValue;
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('MESEC').AsString := cxComboBoxMesec.EditValue;
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('AKTIVEN').AsInteger := 1;
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('VRABOTEN').AsString := dmKon.user;
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('LOKACIJA').AsString :=DM1.TblKomLokaciiID.AsString;
                       if qIdKorekcija.FldByName['id_korekcija'].AsString = '' then
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('PID').AsString :=''
                       else
                          DM1.ProcKomPresmetkaEdinecna.ParamByName('PID').AsString := qIdKorekcija.FldByName['id_korekcija'].AsString;
                          DM1.ProcKomPresmetkaEdinecna.Prepare;
                          DM1.ProcKomPresmetkaEdinecna.ExecProc;

                          ShowMessage('����������� � ��������!');
                          cxGrid1.SetFocus;
              end;
    end;
       TblPresmetkaG.Close;
       TblPresmetkaG.ParamByName('l').AsString:=DM1.TblKomLokaciiID.AsString;;
       TblPresmetkaG.ParamByName('m').AsString:=cxComboBoxMesec.Text;
       TblPresmetkaG.ParamByName('g').AsString:=cxComboBoxGodini.Text;
       TblPresmetkaG.Open;
       TblPresmetkaS.open;
       cxGrid1.SetFocus
  end;
end;
end;

procedure TfrmPoedinecnaPresmetka.EnterKakoTab(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      case key of
        VK_RETURN:
        begin
       {      Key := 0;
             if TEdit(sender).Name='cxTxtPartnerTIP_PARTNER' then
             begin
                PostMessage(Handle,WM_NextDlgCtl,0,0)
             end
             else if TEdit(sender).Name='cxTxtPartnerID' then
             begin
                  if ( (TEdit(sender).Text = '') or (cxLookUpComboBoxTipPartner.Text = '')) then
                  begin
                      callFrmPartneri();
                  end
                  else
                  begin
                      getPartner();
                  end;
             end
             else
             begin }
                  PostMessage(Handle,WM_NextDlgCtl,0,0);
            // end;
        end;
        VK_DOWN:
        begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
            Key := 0;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
    end;
end;

procedure TfrmPoedinecnaPresmetka.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case key of
     VK_ESCAPE: Close;
   end;

end;

function TfrmPoedinecnaPresmetka.podatociOk: boolean;
var ret:boolean;
begin
    ret := false;
    if cxComboBoxGodini.Text = '' then
        begin
            cxComboBoxGodini.SetFocus();
            ShowMessage('������ �� �������� ������!');
        end
        else if cxComboBoxMesec.Text = '' then
        begin
            cxComboBoxMesec.SetFocus();
            ShowMessage('������ �� �������� �����!');
        end
        else if not(DM1.isCenovnikGenerated(cxComboBoxGodini.Text,cxComboBoxMesec.Text)) then
        begin
            ShowMessage('���������� �� ��� ������ �� � ���������!������ �� �� ���������� ������ ���� �� ����������');
        end
    else
        begin
            ret := true;
        end;
     podatociOk := ret;
end;

end.
