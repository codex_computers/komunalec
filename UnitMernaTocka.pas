unit UnitMernaTocka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, FIBQuery, pFIBQuery, cxControls,
  cxContainer, cxEdit, cxGroupBox, cxLabel, ComCtrls, cxTextEdit,StdCtrls,
  Menus, cxButtons, cxGraphics, cxLookAndFeels, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, //dxSkinOffice2013White,
  cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TfrmVnesiMernaTocka = class(TForm)
    Panel1: TPanel;
    gbMatSer: TcxGroupBox;
    qMernaTocka: TpFIBQuery;
    StatusBar1: TStatusBar;
    cxButton1: TcxButton;
    Label2: TLabel;
    txtMernaTocka: TcxTextEdit;
    cbMernaTocka: TcxLookupComboBox;
    procedure txtMaticenKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtMaticenEnter(Sender: TObject);
    procedure txtMaticenExit(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVnesiMernaTocka: TfrmVnesiMernaTocka;

implementation

uses UnitDM1, dmKonekcija, dmMaticni, DaNe;

{$R *.dfm}

procedure TfrmVnesiMernaTocka.cxButton1Click(Sender: TObject);
begin
    frmDaNe := TfrmDaNe.Create(self, '�������', '���� ��������� � �� ���?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort
       else
       begin
        txtMernaTocka.Text := DM1.tblMernaTockaID.AsString;
       self.Close;
      end
end;

procedure TfrmVnesiMernaTocka.txtMaticenEnter(Sender: TObject);
begin
  TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmVnesiMernaTocka.txtMaticenExit(Sender: TObject);
begin
  TEdit(Sender).Color:=clWhite;
end;

procedure TfrmVnesiMernaTocka.txtMaticenKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if key=VK_RETURN then
      begin
        frmDaNe := TfrmDaNe.Create(self, '�������', '���� ��������� � �� ���?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort
       else
       begin
        txtMernaTocka.Text := DM1.tblMernaTockaID.AsString;
       //self.Close;
      end
          //form.Close;
    //  end
      end
      else
      if key=VK_ESCAPE then
      begin
        // tag:=2;
         Close;
      end;

end;

end.
