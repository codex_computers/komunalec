unit UnitDopUsluga;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxBarPainter, ComCtrls,
  ExtCtrls, dxBar, cxClasses, Menus, ActnList, cxGraphics, cxDBEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox,
  cxControls, cxContainer, cxEdit, cxTextEdit, StdCtrls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxGridCustomView, cxGrid, cxDBLookupComboBox, FIBDataSet,
  pFIBDataSet, FIBQuery, pFIBQuery, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxImageComboBox,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSpringTime, cxButtons, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;

type
  TfrmDopUsluga = class(TForm)
    ActionList1: TActionList;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    N1: TdxBarButton;
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    txtSifra: TcxDBTextEdit;
    Label2: TLabel;
    Label3: TLabel;
    txtTipU: TcxDBTextEdit;
    txtUsluga: TcxDBTextEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    txtLokacija: TcxDBTextEdit;
    txtVkRati: TcxDBTextEdit;
    txtDanok: TcxDBTextEdit;
    txtIznos: TcxDBTextEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView3BR_RATA: TcxGridDBColumn;
    cxGrid1DBTableView3MESEC: TcxGridDBColumn;
    cxGrid1DBTableView3IZNOS_USLUGA: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cbUsluga: TcxDBLookupComboBox;
    txtMesec: TcxDBTextEdit;
    Label8: TLabel;
    txtGodina: TcxDBTextEdit;
    aNov: TAction;
    tblFizDukani: TpFIBDataSet;
    dsFizDukani: TDataSource;
    cbPartner: TcxDBLookupComboBox;
    tblFizDukaniTIP_PARTNER: TFIBIntegerField;
    tblFizDukaniID: TFIBIntegerField;
    tblFizDukaniNAZIV: TFIBStringField;
    tblFizDukaniLOKACIJA: TFIBIntegerField;
    aIzlez: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    qBrisiRati: TpFIBQuery;
    cxGrid1DBTableView3ID: TcxGridDBColumn;
    txtID: TcxDBTextEdit;
    cxGrid1DBTableView3Column1: TcxGridDBColumn;
    txtTip: TcxDBTextEdit;
    cxButton1: TcxButton;
    aZapisi: TAction;
    cxButton2: TcxButton;
    qProveriDU: TpFIBQuery;
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbPartnerExit(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure qBrisiRatiBeforeExecute(Sender: TObject);
    procedure txtTipExit(Sender: TObject);
    procedure txtSifraExit(Sender: TObject);
    procedure AllKeyExit(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    function ImaBeleska : boolean;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
   sifra,br_rata,mesec,vk_rati,vkupno,
   prv_mesec,godina,vid_usluga,usluga,tp,p,lok,danok:Integer;
   vk_iznos_DDV,iznos_bez_DDV:Real;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDopUsluga: TfrmDopUsluga;

implementation

uses dmKonekcija, dmMaticni, UnitDM1, Partner, Master, DaNe, Utils;

{$R *.dfm}

procedure TfrmDopUsluga.aAzurirajExecute(Sender: TObject);
begin
 tag:=0;
 if dm1.TblDopUslugiVO_PRESMETKA.Value<>1 then
  begin
    qBrisiRati.Close;
    qBrisiRati.ParamByName('MAS_ID').AsString:=txtID.Text;
    qBrisiRati.ExecQuery;
   // DM1.TblDopUslugi.Refresh;
    cxGrid1DBTableView3.DataController.DataSet.Refresh;
    dm1.TblRati.Close;
    dm1.TblRati.open;
    Panel1.Enabled:=True;
    txtSifra.SetFocus;
    dm1.TblDopUslugi.Edit;
  end
  else
    begin
       ShowMessage('�� ���� �� �� ������� �������. ��� ������� ������ �� �����������!');
       Abort;
    end;
end;

procedure TfrmDopUsluga.aBrisiExecute(Sender: TObject);

begin
///  if dm1.TblDopUslugiVO_PRESMETKA.Value<>1 then

  qProveriDU.Close;
  qproveridu.ParamByName('MAS_ID').AsString:=txtID.Text;
  qProveriDU.ExecQuery;
  if qProveriDU.FldByName['ima'].AsString='0' then

  begin
     tag:=1;
     qBrisiRati.Close;
     qBrisiRati.ParamByName('MAS_ID').AsString:=txtID.Text;
     qBrisiRati.ExecQuery;
     dm1.TblDopUslugi.Delete;
  end
  else
    begin
       ShowMessage('�� ���� �� �� ����� �������. ��� ������� ������ �� �����������!');
       Abort;
    end;
  dm1.tblDULista.Close;
  DM1.tblDULista.Open;
  Close;
end;

procedure TfrmDopUsluga.aIzlezExecute(Sender: TObject);
begin
  if dm1.TblDopUslugi.State in [dsInsert,dsEdit] then
  begin
         dm1.TblDopUslugi.Cancel;
         Panel1.Enabled:=False;
         ActiveControl:=cxGrid1;
  end
  else
  begin
        Close;
  end;
  end;

procedure TfrmDopUsluga.aNovExecute(Sender: TObject);
begin
//if not ImaBeleska then
 // begin
   Panel1.Enabled:=True;
   sifra:=0;
   vk_iznos_DDV:=0;
   mesec:=0;
   ActiveControl:=txtTip;
   DM1.TblDopUslugi.Insert;
   dm1.PMaxDopUsluga.ExecProc;
   dm1.TblDopUslugiID.Value:=dm1.PMaxDopUsluga.FldByName['MAKS'].Value+1;
   sifra:=dm1.PMaxDopUsluga.FldByName['MAKS'].Value+1;
   dm1.PMaxDopUsluga.Close;
   dm1.TblDopUslugiBR_RATA.Value:=0;
//  end
//  else
//  begin
//       ShowMessage('�� ���������� '+dm1.tblDUListaNAZIV.AsString+', �� '+  DM1.tblDUListaMESEC.AsString+ ' �����, '+DM1.tblDUListaGODINA.AsString+' ������ ��� ��������� ���������!'+#10#13+
//               ' �� ����� ���� �������, �������� � ���� �� �� ������� �����������.');
//  end;

end;

procedure TfrmDopUsluga.aZapisiExecute(Sender: TObject);
begin
               if not(Validacija(Panel1)) then
               begin
               if (not ImaBeleska) then
                 begin
                  dm1.TblDopUslugiIZNOS_USLUGA.Value:=(dm1.TblDopUslugiIZNOS_USLUGA_NETO.Value*StrToInt(txtDanok.Text)/100)+dm1.TblDopUslugiIZNOS_USLUGA_NETO.Value;
                 // vk_iznos_DDV:=StrToFloat(txtIznos.Text);//*StrToInt(txtDanok.Text)/100)+StrToFloat(txtIznos.text);
                  vk_iznos_DDV:=dm1.TblDopUslugiIZNOS_USLUGA_NETO.Value;
                  dm1.TblDopUslugiVK_RATI.AsString:=txtVkRati.text;
                  vk_rati:=StrToInt(txtVkRati.text);
                  vkupno:=StrToInt(txtVkRati.text);
                  dm1.TblDopUslugiDATUM.Value:=Now;
                  prv_mesec:=strtoint(txtMesec.text);
                  godina:=StrToInt(txtGodina.text);
                  vid_usluga:=StrToInt(txtTipU.text);
                  usluga:=StrToInt(txtUsluga.Text);
                  //iznos_bez_DDV:=StrToFloat(txtIznos.Text);
                  iznos_bez_DDV:=dm1.TblDopUslugiIZNOS_USLUGA_NETO.Value/(1+strtoint(txtDanok.Text)/100);
                  danok:=strtoint(txtDanok.Text);
                  txtIznos.Text:=floattostr(vk_iznos_DDV);
                  sifra:=strtoint(txtID.Text);
                  DM1.TblDopUslugi.Post;
                  dm1.TblDopUslugi.Close;
                  dm1.TblDopUslugi.ParamByName('id').Value:=sifra;
                  DM1.TblDopUslugi.Open;
                  dm1.TblRati.open;
                  Panel1.Enabled:=False;
                  br_rata:=0;
                  while (vkupno>0) do
                   begin
                       dm1.TblRati.Insert;
                       br_rata:=br_rata+1;
                       dm1.TblRatiID.value:=sifra;
                       dm1.TblRatiBR_RATA.value:=br_rata;
                       dm1.TblRatiTIP_PARTNER.value:=tp;
                       dm1.TblRatiPARTNER.Value:=p;

                       dm1.TblRatiLOKACIIN_ID.Value:=lok;
                       if (prv_mesec+br_rata-1)<=12 then
                       begin
                          dm1.TblRatiMESEC1.Value:=prv_mesec+br_rata-1;
                          dm1.TblRatiGODINA.value:=godina;
                       end
                       else
                       begin
                          mesec:=mesec+1;
                          dm1.TblRatiMESEC1.Value:=mesec;
                          dm1.TblRatiGODINA.Value:=godina+1;
                       end;
                       dm1.TblRatiVID_USLUGA.Value:=vid_usluga;
                       dm1.TblRatiUSLUGA.Value:=usluga;
                       dm1.TblRatiVK_RATI.Value:=vk_rati;
                       dm1.TblRatiIZNOS_USLUGA_NETO.Value:=iznos_bez_DDV/vk_rati;
                       dm1.TblRatiVK_RATI.Value:=vk_rati;
                       dm1.TblRatiIZNOS_USLUGA.Value:=vk_iznos_DDV/vk_rati;
                       dm1.TblRatiDANOK.Value:=danok;
                       dm1.TblRati.Post;
                     vkupno:=vkupno-1;
                 end;
                  cxGrid1.SetFocus;
                 end
                 else
                 begin
                      ShowMessage('�� ���������� '+cbPartner.Text+', �� '+  txtMesec.Text+ ' �����, '+txtGodina.Text+' ������ ��� ��������� ���������!'+#10#13+
                        ' �� ����� ���� �������, �������� � ���� �� �� ������� �����������.');
                      dm1.TblRati.Cancel;
                      cxGrid1.SetFocus;
                 end;
             end ;
end;

procedure TfrmDopUsluga.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//   dm1.tblDULista.Refresh;
    DM1.tblDULista.CloseOpen(True);
end;

procedure TfrmDopUsluga.FormCreate(Sender: TObject);
begin
   tblFizDukani.Close;
   tblFizDukani.ParamByName('tip').Value:='%';
   tblFizDukani.Open;
   Panel1.Enabled:=False;
   ActiveControl:=cxGrid1;
  //  aNov.Execute;
end;

procedure TfrmDopUsluga.qBrisiRatiBeforeExecute(Sender: TObject);
begin
  if tag=0 then
  begin
      frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� ��������� ������� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort;
  end
  else
  begin
      frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort
  end;
end;

procedure TfrmDopUsluga.AllKeyExit(Sender: TObject);
begin
  TEdit(Sender).Color:=clWhite;
end;

procedure TfrmDopUsluga.txtSifraExit(Sender: TObject);
begin
   if txtSifra.Text<>'' then
   begin
     tblFizDukani.Close;
     tblFizDukani.ParamByName('tip').Value:=txtTip.Text;
     tblFizDukani.Open;
   end;
   TEdit(Sender).Color:=clWhite;
end;

procedure TfrmDopUsluga.txtTipExit(Sender: TObject);
begin
   if txttip.text='' then
   begin
      ShowMessage('������ ��� �� �������!');
      txtTip.SetFocus;
      Abort;
   end;
   TEdit(Sender).Color:=clWhite;
end;

procedure TfrmDopUsluga.cbPartnerExit(Sender: TObject);
begin
if (txtSifra.Text<>'') then
begin
  if (tblFizDukani.Locate('ID;TIP_PARTNER', VarArrayOf([strtoint(txtSifra.text),strtoint(txtTip.text)]),[]))
   and (dm1.TblDopUslugi.State <> dsBrowse) then
  begin
      dm1.TblDopUslugiLOKACIIN_ID.Value:=tblFizDukaniLOKACIJA.Value;
      dm1.TblDopUslugiTIP_PARTNER.Value:=tblFizDukaniTIP_PARTNER.Value;
      tp:=tblFizDukaniTIP_PARTNER.Value;
      p:=tblFizDukaniID.Value;
      lok:=tblFizDukaniLOKACIJA.Value;
  end;
end
else
 begin
  ShowMessage('������� �������!');
  Abort;
end;

end;

procedure TfrmDopUsluga.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
 begin
    case key of
     VK_RETURN:
       // begin
             PostMessage(Handle,WM_NextDlgCtl,0,0);
       // end;
     VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
     VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
end;
end;

function TfrmDopUsluga.ImaBeleska : boolean;
var rez : boolean;
begin
    rez := false;
    dm1.qImaBeleska.Close;
    dm1.qImaBeleska.ParamByName('lokacija').AsString := txtLokacija.Text;
    dm1.qImaBeleska.ParamByName('mesec').AsString := txtMesec.Text;
    dm1.qImaBeleska.ParamByName('godina').AsString := txtGodina.Text;
    dm1.qImaBeleska.ExecQuery;

    if dm1.qImaBeleska.FldByName['ImaBeleska'].IsNull then
      rez := false
    else
      rez := true;
    ImaBeleska := rez;
end;

end.
