object frmPartner: TfrmPartner
  Left = 166
  Top = 147
  Caption = #1055#1072#1088#1090#1085#1077#1088#1080
  ClientHeight = 570
  ClientWidth = 890
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 551
    Width = 890
    Height = 19
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F10 - '#1055#1077#1095#1072#1090#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
  end
  object pcPartner: TPageControl
    Left = 0
    Top = 0
    Width = 890
    Height = 551
    ActivePage = pcTabTabelaren
    Align = alClient
    TabOrder = 1
    TabStop = False
    object pcTabTabelaren: TTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 882
        Height = 523
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dmMat.dsPartner
          DataController.Filter.Active = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077'>'
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
          end
          object cxGrid1DBTableView1TipPartnerNaziv: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1087#1072#1088#1090#1085#1077#1088
            DataBinding.FieldName = 'TIP_NAZIV'
            Visible = False
          end
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
          end
          object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Width = 152
          end
          object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Width = 150
          end
          object cxGrid1DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Visible = False
          end
          object cxGrid1DBTableView1MestoNaziv: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_NAZIV'
            Width = 150
          end
          object cxGrid1DBTableView1TEL: TcxGridDBColumn
            DataBinding.FieldName = 'TEL'
            Width = 80
          end
          object cxGrid1DBTableView1FAX: TcxGridDBColumn
            DataBinding.FieldName = 'FAX'
            Width = 80
          end
          object cxGrid1DBTableView1DANOCEN: TcxGridDBColumn
            DataBinding.FieldName = 'DANOCEN'
            Width = 81
          end
          object cxGrid1DBTableView1IME: TcxGridDBColumn
            DataBinding.FieldName = 'IME'
            Visible = False
          end
          object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
            DataBinding.FieldName = 'PREZIME'
            Visible = False
          end
          object cxGrid1DBTableView1TATKO: TcxGridDBColumn
            DataBinding.FieldName = 'TATKO'
            Visible = False
          end
          object cxGrid1DBTableView1LOGO: TcxGridDBColumn
            DataBinding.FieldName = 'LOGO'
            Visible = False
          end
          object cxGrid1DBTableView1STATUS: TcxGridDBColumn
            Caption = #1048#1085#1082#1072#1089#1072#1090#1086#1088
            DataBinding.FieldName = 'STATUS'
          end
          object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
            Caption = #1042#1085#1077#1089#1077#1085#1086
            DataBinding.FieldName = 'TS_INS'
          end
          object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
            Caption = #1055#1088#1086#1084#1077#1085#1072
            DataBinding.FieldName = 'TS_UPD'
          end
        end
        object viewSmetki: TcxGridDBTableView
          OnKeyPress = viewSmetkiKeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dmMat.dsSmetkiP
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NewItemRow.Visible = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.Appending = True
          OptionsView.GroupByBox = False
          object viewSmetkiID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
          end
          object viewSmetkiTIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
          end
          object viewSmetkiPARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Visible = False
          end
          object viewSmetkiTIP_BANKA: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_BANKA'
            Visible = False
          end
          object viewSmetkiBANKA: TcxGridDBColumn
            DataBinding.FieldName = 'BANKA'
            Visible = False
          end
          object viewSmetkiBankaNaziv: TcxGridDBColumn
            Caption = #1041#1072#1085#1082#1072
            DataBinding.FieldName = 'BankaLookUp'
            Width = 250
          end
          object viewSmetkiZS_BROJ: TcxGridDBColumn
            Caption = #1046#1080#1088#1086' '#1089#1084#1077#1090#1082#1072
            DataBinding.FieldName = 'ZS_BROJ'
          end
          object viewSmetkiDEVIZNA: TcxGridDBColumn
            DataBinding.FieldName = 'DEVIZNA'
            Width = 80
          end
          object viewSmetkiAKTIVNA: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVNA'
            Width = 80
          end
        end
        object viewKontakti: TcxGridDBTableView
          OnKeyPress = viewKontaktiKeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dmMat.dsKontaktP
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NewItemRow.Visible = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsData.Appending = True
          OptionsView.GroupByBox = False
          object viewKontaktiSIFRA: TcxGridDBColumn
            DataBinding.FieldName = 'SIFRA'
          end
          object viewKontaktiTIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
          end
          object viewKontaktiID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object viewKontaktiNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Width = 300
          end
          object viewKontaktiMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'MAIL'
            Width = 150
          end
          object viewKontaktiTEL: TcxGridDBColumn
            DataBinding.FieldName = 'TEL'
            Width = 100
          end
          object viewKontaktiMOBILEN: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN'
            Width = 100
          end
          object viewKontaktiOPIS: TcxGridDBColumn
            DataBinding.FieldName = 'OPIS'
            Visible = False
          end
        end
        object cxGrid1Level1: TcxGridLevel
          Caption = #1055#1072#1088#1090#1085#1077#1088#1080
          GridView = cxGrid1DBTableView1
        end
        object cxGrid1Level2: TcxGridLevel
          Caption = #1046#1080#1088#1086' '#1089#1084#1077#1090#1082#1080
          GridView = viewSmetki
        end
        object cxGrid1Level3: TcxGridLevel
          Caption = #1051#1080#1094#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
          GridView = viewKontakti
        end
      end
    end
    object pcTabEdinecen: TTabSheet
      Caption = #1045#1076#1080#1085#1077#1095#1077#1085
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblTip: TLabel
        Left = 5
        Top = 34
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1080#1087' '#1087#1072#1088#1090#1085#1077#1088
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblSifra: TLabel
        Left = 5
        Top = 59
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1064#1080#1092#1088#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNaziv: TLabel
        Left = 5
        Top = 84
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' / '#1053#1072#1079#1080#1074
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblAdresa: TLabel
        Left = 5
        Top = 159
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblMesto: TLabel
        Left = 5
        Top = 184
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1090#1086
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTelefon: TLabel
        Left = 5
        Top = 209
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1077#1083#1077#1092#1086#1085
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblFaks: TLabel
        Left = 5
        Top = 234
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1060#1072#1082#1089
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDanocen: TLabel
        Left = 5
        Top = 259
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1085#1086#1095#1077#1085
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPrezime: TLabel
        Left = 5
        Top = 109
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblTatko: TLabel
        Left = 5
        Top = 134
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 5
        Top = 283
        Width = 91
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073'.'#1077#1076'.'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TipPartner: TcxDBTextEdit
        Left = 101
        Top = 30
        DataBinding.DataField = 'TIP_PARTNER'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 0
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object Sifra: TcxDBTextEdit
        Left = 101
        Top = 55
        BeepOnEnter = False
        DataBinding.DataField = 'ID'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 2
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object Naziv: TcxDBTextEdit
        Left = 101
        Top = 80
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 5
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 350
      end
      object Adresa: TcxDBTextEdit
        Left = 101
        Top = 155
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 8
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 350
      end
      object Mesto: TcxDBTextEdit
        Left = 101
        Top = 180
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 9
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object Telefon: TcxDBTextEdit
        Left = 101
        Top = 205
        BeepOnEnter = False
        DataBinding.DataField = 'TEL'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 11
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 150
      end
      object Faks: TcxDBTextEdit
        Left = 101
        Top = 230
        BeepOnEnter = False
        DataBinding.DataField = 'FAX'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 12
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 150
      end
      object Danocen: TcxDBTextEdit
        Left = 101
        Top = 255
        BeepOnEnter = False
        DataBinding.DataField = 'DANOCEN'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 13
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 150
      end
      object TipPartnerNaziv: TcxDBLookupComboBox
        Left = 153
        Top = 30
        DataBinding.DataField = 'TIP_PARTNER'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.AnsiSort = True
        Properties.ListOptions.RowSelect = False
        Properties.ListSource = dmMat.dsTipPartner
        Style.Color = 13948116
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 300
      end
      object MestoNaziv: TcxDBLookupComboBox
        Left = 153
        Top = 180
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.AnsiSort = True
        Properties.ListOptions.RowSelect = False
        Properties.ListSource = dmMat.dsMesto
        Style.Color = 13948116
        TabOrder = 10
        OnKeyDown = EnterKakoTab
        Width = 300
      end
      object Prezime: TcxDBTextEdit
        Left = 101
        Top = 105
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 6
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 350
      end
      object Tatko: TcxDBTextEdit
        Left = 101
        Top = 130
        BeepOnEnter = False
        DataBinding.DataField = 'TATKO'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 7
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 350
      end
      object RE: TcxDBTextEdit
        Left = 101
        Top = 279
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.CharCase = ecUpperCase
        TabOrder = 14
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 46
      end
      object cxDBLookupComboBox1: TcxDBLookupComboBox
        Left = 149
        Top = 279
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dmMat.dsPartner
        ParentFont = False
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.AnsiSort = True
        Properties.ListOptions.RowSelect = False
        Properties.ListSource = dmMat.dsRE
        Style.Color = 13948116
        TabOrder = 16
        OnKeyDown = EnterKakoTab
        Width = 300
      end
      object cbVraboten: TDBCheckBox
        Left = 440
        Top = 57
        Width = 75
        Height = 17
        Caption = '  '#1042#1088#1072#1073#1086#1090#1077#1085
        DataField = 'STATUS'
        DataSource = dmMat.dsPartner
        TabOrder = 15
        ValueChecked = '3'
        ValueUnchecked = '0'
        Visible = False
      end
      object cbSubvencioniranje: TcxDBCheckBox
        Left = 314
        Top = 53
        Caption = #1057#1091#1073#1074#1077#1085#1094#1080#1086#1085#1080#1088#1072#1114#1077
        DataBinding.DataField = 'STATUS'
        DataBinding.DataSource = dmMat.dsPartner
        Properties.Alignment = taLeftJustify
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 3
        Properties.ValueUnchecked = 0
        TabOrder = 4
        Transparent = True
        Visible = False
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 118
      end
      object cbInkasator: TcxDBCheckBox
        Left = 162
        Top = 55
        Caption = #1048#1085#1082#1072#1089#1072#1090#1086#1088
        DataBinding.DataField = 'STATUS'
        DataBinding.DataSource = dmMat.dsPartner
        Properties.Alignment = taLeftJustify
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 5
        Properties.ValueUnchecked = 0
        TabOrder = 3
        Transparent = True
        OnEnter = cxDBTextAllEnter
        OnExit = cxDBTextAllExit
        OnKeyDown = EnterKakoTab
        Width = 78
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 424
    Top = 8
  end
  object PopupMenu1: TPopupMenu
    Left = 456
    Top = 8
    object N1: TMenuItem
      Action = SaveToIniFile
    end
    object Excel1: TMenuItem
      Action = ExportToExcel
    end
  end
  object ActionList1: TActionList
    Left = 488
    Top = 8
    object SaveToIniFile: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnExecute = SaveToIniFileExecute
    end
    object ExportToExcel: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
      OnExecute = ExportToExcelExecute
    end
  end
  object IdIcmpClient1: TIdIcmpClient
    Host = 'www.google.com'
    Protocol = 1
    ProtocolIPv6 = 0
    IPVersion = Id_IPv4
    PacketSize = 1024
    Left = 744
    Top = 8
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.CharSet = 'utf-8'
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.AcceptCharSet = 'utf-8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 560
    Top = 56
  end
end
