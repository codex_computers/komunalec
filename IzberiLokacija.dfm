object frmIzberiLokacija: TfrmIzberiLokacija
  Left = 0
  Top = 0
  Caption = #1048#1079#1073#1077#1088#1080' '#1083#1086#1082#1072#1094#1080#1112#1072
  ClientHeight = 286
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 267
    Align = alClient
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 499
      Height = 265
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        OnKeyDown = cxGrid1DBTableView1KeyDown
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DM1.DSKomLokacii
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'ID'
          Width = 77
        end
        object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
          Caption = #1056#1077#1086#1085
          DataBinding.FieldName = 'REON_ID'
          Width = 97
        end
        object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
          Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
          DataBinding.FieldName = 'CITACKA_KNIGA'
        end
        object cxGrid1DBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn
          Caption = #1056#1077#1076'.'#1041#1088'.'#1063#1080#1090'.'#1050#1085#1080#1075#1072
          DataBinding.FieldName = 'CITACKA_KNIGA_RB'
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARTNER'
        end
        object cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER_ID'
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1085#1072' '#1083#1086#1082'.'
          DataBinding.FieldName = 'AKTIVEN'
        end
        object cxGrid1DBTableView1ULICA_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ULICA_ID'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ_NA_CLENOVI: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_NA_CLENOVI'
          Visible = False
        end
        object cxGrid1DBTableView1STANBENA_POVRSINA: TcxGridDBColumn
          DataBinding.FieldName = 'STANBENA_POVRSINA'
          Visible = False
        end
        object cxGrid1DBTableView1DVORNA_POVRSINA: TcxGridDBColumn
          DataBinding.FieldName = 'DVORNA_POVRSINA'
          Visible = False
        end
        object cxGrid1DBTableView1PRED: TcxGridDBColumn
          DataBinding.FieldName = 'PRED'
          Visible = False
        end
        object cxGrid1DBTableView1SLED: TcxGridDBColumn
          DataBinding.FieldName = 'SLED'
          Visible = False
        end
        object cxGrid1DBTableView1KOREN: TcxGridDBColumn
          DataBinding.FieldName = 'KOREN'
          Visible = False
        end
        object cxGrid1DBTableView1RB1: TcxGridDBColumn
          DataBinding.FieldName = 'RB1'
          Visible = False
        end
        object cxGrid1DBTableView1RB2: TcxGridDBColumn
          DataBinding.FieldName = 'RB2'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 267
    Width = 501
    Height = 19
    Panels = <
      item
        Text = 'Enter - '#1048#1079#1073#1077#1088#1080',  Escape - '#1048#1079#1083#1077#1079
        Width = 50
      end>
  end
  object ActionList1: TActionList
    Left = 400
    Top = 120
    object aIzberi: TAction
      Caption = 'aIzberi'
    end
  end
end
