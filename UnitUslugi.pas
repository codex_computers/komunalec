unit UnitUslugi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxLookAndFeels, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxBar, dxPSCore,
  dxPScxCommon,  cxBarEditItem, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint;

type
  TFrmUslugi = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxLookUpVID_USLUGA: TcxDBLookupComboBox;
    cxControlVID_USLUGA: TcxDBTextEdit;
    lblVidUsluga: TLabel;
    lblNaziv: TLabel;
    cxControlNAZIV: TcxDBTextEdit;
    Label4: TLabel;
    Label7: TLabel;
    txtKFL: TcxDBTextEdit;
    cbKFL: TcxDBLookupComboBox;
    Konro: TLabel;
    txtKD: TcxDBTextEdit;
    cbKD: TcxDBLookupComboBox;
    Label3: TLabel;
    txtKPL: TcxDBTextEdit;
    cbKPL: TcxDBLookupComboBox;
    Label5: TLabel;
    txtKU: TcxDBTextEdit;
    cbKU: TcxDBLookupComboBox;
    Label6: TLabel;
    txtRE: TcxDBTextEdit;
    cbRE: TcxDBLookupComboBox;
    cxGrid1DBTableView1KFL: TcxGridDBColumn;
    cxGrid1DBTableView1KD: TcxGridDBColumn;
    cxGrid1DBTableView1KPL: TcxGridDBColumn;
    cxGrid1DBTableView1KU: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    procedure OnKeyDownAllLookUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure setDefaultVidUsluga(defaultVidUsluga:String);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

  private
    { Private declarations }
    var defaultVidUsluga:String;
  public
    { Public declarations }
  end;

var
  FrmUslugi: TFrmUslugi;

implementation

uses UnitDM1, dmKonekcija, dmMaticni;

{$R *.dfm}

procedure TFrmUslugi.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    case Key of
        VK_F5:
        begin
          if defaultVidUsluga <> '' then
          begin
            DM1.TblKomUslugiVID_USLUGA.AsString := defaultVidUsluga;
          end;
        end;
    end;
end;

procedure TFrmUslugi.FormShow(Sender: TObject);
begin
  inherited;
    if defaultVidUsluga = '' then
    begin
        //DM1.openTblUslugiAll();
        DM1.TblKomUslugi.open;
    end;
end;

procedure TFrmUslugi.OnKeyDownAllLookUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    case key of
        VK_RETURN:
        begin
          PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;        
    end;
end;

procedure TFrmUslugi.setDefaultVidUsluga(defaultVidUsluga:String);
begin
    Self.defaultVidUsluga := defaultVidUsluga;
end;

end.
