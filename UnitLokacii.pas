
unit UnitLokacii;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, ActnList, StdCtrls, Menus, cxLookAndFeelPainters,
  cxButtons, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxControls, cxContainer, cxEdit, cxTextEdit, dxSkinsdxBarPainter, cxClasses,
  dxBar, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  DB, cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxDBEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, FIBQuery,
  pFIBQuery, FIBDataSet, pFIBDataSet, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSpringTime, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, iduri,
  cxMemo, IdBaseComponent, IdComponent, IdRawBase, IdRawClient, IdIcmpClient,
 // dxSkinOffice2013White,
   cxNavigator, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  System.Actions, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light; // System.Actions;

type
  TFrmLokacija = class(TForm)
    StatusBar1: TStatusBar;
    panelGore: TPanel;
    panelGrid: TPanel;
    ActionList1: TActionList;
    aIzlez: TAction;
    panelGorePartner: TPanel;
    lblPartner: TLabel;
    cxBtnPartneri: TcxButton;
    cxTxtPartnerID: TcxTextEdit;
    cxTxtPartnerNaziv: TcxTextEdit;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    subListAkcii: TdxBarSubItem;
    BarButtonIzlez: TdxBarButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGrid1DBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ULICA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_NA_CLENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1STANBENA_POVRSINA: TcxGridDBColumn;
    cxGrid1DBTableView1DVORNA_POVRSINA: TcxGridDBColumn;
    panelGoreReon: TPanel;
    panelDole: TPanel;
    cxControlBROJ_NA_CLENOVI: TcxDBTextEdit;
    cxControlSTANBENA_POVR: TcxDBTextEdit;
    lblReon: TLabel;
    cxControlREON: TcxDBTextEdit;
    cxLookUpREON: TcxDBLookupComboBox;
    lblUlica: TLabel;
    cxControlULICA: TcxDBTextEdit;
    cxLookUpULICA: TcxDBLookupComboBox;
    aNovaLokacija: TAction;
    lblBroj: TLabel;
    lblBrojNaClenovi: TLabel;
    lblStanbenaPovr: TLabel;
    lblDvornaPovr: TLabel;
    cxControlDVORNA_POVR: TcxDBTextEdit;
    lblCitackaKniga: TLabel;
    cxControlCITACKA_KNIGA: TcxDBTextEdit;
    lblCitackaKnigaRb: TLabel;
    cxControlCITACKA_KNIGA_RB: TcxDBTextEdit;
    aIzbrisiLokacija: TAction;
    aPromeni: TAction;
    cxControlBROJ_ULICA: TcxDBTextEdit;
    BarButtonNovaLokacija: TdxBarButton;
    BarButtonPromeni: TdxBarButton;
    BarButtonIzbrisi: TdxBarButton;
    aVodomeri: TAction;
    BarButton: TdxBarButton;
    aPromeniRedBrojVoKniga: TAction;
    BarButtonPromeniRedenBrojVoKniga: TdxBarButton;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxControlREDEN_BROJ: TcxDBTextEdit;
    lblRedenBroj: TLabel;
    cxComboBoxAKTIVEN: TcxDBComboBox;
    lblAktiven: TLabel;
    Label1: TLabel;
    btnZapisi: TcxButton;
    aPromeniCitackaKnigaRb: TAction;
    cxLookUpComboBoxTipPartner: TcxLookupComboBox;
    aLokacijaUslugi: TAction;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    aPromenaCitKn: TAction;
    Uslugi: TdxBarButton;
    Promena: TdxBarButton;
    qCK: TpFIBQuery;
    cxMemo1: TcxMemo;
    IdIcmpClient1: TIdIcmpClient;
    Label2: TLabel;
    txtMernaTocka: TcxDBTextEdit;
    cbMernaTocka: TcxDBLookupComboBox;
    tblDopUsl: TpFIBDataSet;
    dsDopUsl: TDataSource;
    tblDopUslIMA: TFIBIntegerField;
    cbInkasator: TcxDBLookupComboBox;
    txtInkasator: TcxDBTextEdit;
    Label3: TLabel;
    TblKomReoni: TpFIBDataSet;
    TblKomReoniID: TFIBIntegerField;
    TblKomReoniNAZIV: TFIBStringField;
    TblKomReoniTIP_INKASATOR: TFIBIntegerField;
    TblKomReoniPASS: TFIBStringField;
    TblKomReoniUSERNAME: TFIBStringField;
    TblKomReoniID_INKASATOR: TFIBBCDField;
    DSKomReoni: TDataSource;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EditAllKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure callFrmPartneri();
    procedure cxBtnPartneriClick(Sender: TObject);
    procedure getPartner();
    procedure prepareLocationsForPartner();
    procedure cistiPodatoci();
    procedure aNovaLokacijaExecute(Sender: TObject);
    //procedure cancelUpdates();
    function cancelUpdates():boolean;
    procedure onKeyDownAll(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure onEnterAll(Sender: TObject);
    procedure onExitAll(Sender: TObject);
    procedure aIzbrisiLokacijaExecute(Sender: TObject);
    procedure aPromeniExecute(Sender: TObject);
    function podatociOK():boolean;
    procedure aVodomeriExecute(Sender: TObject);
    procedure aPromeniRedBrojVoKnigaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnZapisiClick(Sender: TObject);
    procedure aPromeniCitackaKnigaRbExecute(Sender: TObject);
    //function threadProcedure(P : Pointer) : LongInt; stdcall;
    procedure enableGrid(enable:boolean);
    procedure aLokacijaUslugiExecute(Sender: TObject);
    procedure aPromenaCitKnExecute(Sender: TObject);
    procedure cxControlCITACKA_KNIGA_RBExit(Sender: TObject);
  private
    { Private declarations }
    partnerSet:Boolean;
  public
    { Public declarations }
    constructor Create(Owner : TComponent; id : string; tip_partner: string; naziv:string);reintroduce; overload;
  end;

var
  FrmLokacija: TFrmLokacija;

implementation

uses dmMaticni, UnitDM1,
Partner, DaNe, UnitVodomeri, UnitPromeniRedBr,
  UnitMain, UnitPromeniCitackaKnigaRb, UnitLokacijaUslugi, dmKonekcija,
  UnitPromena,Utils, PDDGIOZP, u7ThreadService;

{$R *.dfm}

//------------------------------------------------------------------------------
constructor TFrmLokacija.Create(Owner : TComponent; id : string; tip_partner: string; naziv:string);
begin
    inherited Create(Owner);
    cxTxtPartnerID.Text := id;
    //cxTxtPartnerTIP_PARTNER.Text := tip_partner;
    cxLookUpComboBoxTipPartner.EditValue := tip_partner;
    cxTxtPartnerNaziv.Text := naziv;
    Tag := 1;
    //prepareLocationsForPartner();
end;

//function TFrmLokacija.threadProcedure(P : Pointer) : LongInt; stdcall;
function threadProcedure(P : Pointer) : LongInt; stdcall;
var
  I: Integer;
  f:Double;
begin
//
    ShowMessage('pocna');
    for I := 0 to 10000000 do
    begin
         f := (i+1000)/3.423*123.343;
    end;
    threadProcedure := 1;
end;


procedure TFrmLokacija.EditAllKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (key = vk_return) then
    begin
         if TEdit(sender).Name='cxTxtPartnerTIP_PARTNER' then
         begin
            PostMessage(Handle,WM_NextDlgCtl,0,0)
         end
         else if TEdit(sender).Name='cxTxtPartnerID' then
         begin
              //if ( (TEdit(sender).Text = '') or (cxTxtPartnerTIP_PARTNER.Text = '')) then
              if ( (TEdit(sender).Text = '') or (cxLookUpComboBoxTipPartner.Text = '')) then
              begin
                 cxBtnPartneriClick(sender);
               //   callFrmPartneri();
              end
              else
              begin
                  getPartner();
              end;
         end;
    end;
end;

procedure TFrmLokacija.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    cancelUpdates();

    if DM1.TblKomLokacii.Active = true then
    begin
        DM1.TblKomLokacii.Close();
    end;
    Action := caFree;
end;

procedure TFrmLokacija.FormShow(Sender: TObject);
begin
    if Tag = 1 then
    begin
        prepareLocationsForPartner();
    end;
    TblKomReoni.Open;
end;

procedure TFrmLokacija.aIzbrisiLokacijaExecute(Sender: TObject);
var lokacija,koren:string;
begin
    frmDaNe := TfrmDaNe.Create(self,'������ �� ��������','���� ��� ������� ���� ������ �� �� ��������� ���������?',1);
    if (frmDaNe.ShowModal = mrYes) then
    begin
        koren:=dm1.TblKomLokaciiKOREN.AsString;
        lokacija:=dm1.TblKomLokaciiID.AsString;
        DM1.TblKomLokacii.Delete();
        DM1.TblKomLokacii.CloseOpen(true);
        if DM1.TblKomLokacii.Locate('KOREN',VarArrayOf([lokacija]),[]) then
          begin
            dm1.TblKomLokacii.edit;
            dm1.TblKomLokaciiKOREN.AsString:=koren;
            DM1.TblKomLokacii.Post;
            DM1.ProcSetOrder1.Prepare;
            DM1.ProcSetOrder1.ExecProc;
          end;
        cxGrid1.SetFocus();
    end
end;

procedure TFrmLokacija.aIzlezExecute(Sender: TObject);
begin
    if partnerSet then
    begin
    {
        if self.Tag = 1 then
        begin
            Close();
        end
        else
        begin
            cistiPodatoci();
        end;
        }
        {
        if DM1.TblKomLokacii.State in [dsEdit,dsInsert] then
        begin
            DM1.TblKomLokacii.Cancel();
            cxGrid1.SetFocus();
            panelDole.Enabled := false;
            Abort();
        end;
        }
        if cancelUpdates then
        begin
            enableGrid(true);
            if cxControlCITACKA_KNIGA.Enabled = false then
            begin
                cxControlCITACKA_KNIGA.Enabled := true;
                cxControlCITACKA_KNIGA_RB.Enabled := true;
            end;
            

            cxGrid1.SetFocus();
            panelDole.Enabled := false;
            Abort();
        end;
        if self.Tag <> 1 then
        begin
            cistiPodatoci();
            //Abort();
        end
        else
        begin
            Close();
        end;
    end
    else
    begin
        Close();
    end;
    //Close();
end;

procedure TFrmLokacija.aLokacijaUslugiExecute(Sender: TObject);
var frm:TFrmLokacijaUslugi;
begin
    if DM1.TblKomLokacii.State = dsBrowse then
    begin
        if DM1.TblKomLokacii.RecordCount > 0 then
        begin
            DM1.TblKomLokacijaUslugi.Close();
            DM1.TblKomLokacijaUslugi.SelectSQL.Text := DM1.selectUslugiPoLokacija+DM1.TblKomLokaciiID.AsString;
            DM1.TblKomLokacijaUslugi.Open();

            frm := TFrmLokacijaUslugi.Create(nil,cxLookUpComboBoxTipPartner.Text,cxTxtPartnerID.Text,cxTxtPartnerNaziv.Text,DM1.TblKomLokaciiID.AsString,DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString);
            frm.ShowModal();
            frm.Free();
        end
        else
        begin
            ShowMessage('������ �� ������ ������� �� ������');
        end;
    end
    else
    begin
        ShowMessage('�� ������ �� �������� �� �������� ������ ��������� ��� ����������� �������');
    end;
end;

procedure TFrmLokacija.aNovaLokacijaExecute(Sender: TObject);
begin
    if partnerSet then
    begin
        panelDole.Enabled := true;
        cxControlReon.SetFocus();
        DM1.TblKomLokacii.Insert;

        enableGrid(false);

        cxControlREDEN_BROJ.Enabled := false;
    end
    else
    begin
        ShowMessage('������ �� �������� ������� ���� �� ������� �������!');
    end;
end;

procedure TFrmLokacija.aPromenaCitKnExecute(Sender: TObject);
var frm:TfrmPromena;
begin
    if DM1.TblKomLokacii.RecordCount > 0 then
    begin
        if DM1.TblKomLokacii.State = dsBrowse then
        begin
            frm := TFrmPromena.Create(nil, DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString, DM1.TblKomLokaciiID.AsString,cxTxtPartnerNaziv.Text);
            frm.ShowModal();
            frm.Free();
        end
        else
        begin
            ShowMessage('�� ������ �� ����������� ����� ��� ������ ��������� ��� ����������� �������');
        end;
    end
    else
    begin
        ShowMessage('������ �� �������� ������� �� ������');
    end;

end;

procedure TFrmLokacija.aPromeniCitackaKnigaRbExecute(Sender: TObject);
var frm:TFrmPromeniCitackaKnigaRb;
begin
    if DM1.TblKomLokacii.RecordCount > 0 then
    begin
        if DM1.TblKomLokacii.State = dsBrowse then
        begin
            //frm := TFrmPromeniCitackaKnigaRb.Create(nil, DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString,DM1.TblKomLokaciiPRED.AsString,DM1.TblKomLokaciiSLED.AsString,DM1.TblKomLokaciiREDEN_BROJ.AsString);
            frm := TFrmPromeniCitackaKnigaRb.Create(nil,DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString,DM1.TblKomLokaciiPRED.AsString,DM1.TblKomLokaciiSLED.AsString,DM1.TblKomLokaciiRB1.AsString);
            frm.ShowModal();
            frm.Free();
        end
        else
        begin
            ShowMessage('�� ������ �� ����������� ������ ��������� ��� ����������� �������');
        end;
    end
    else
    begin
        ShowMessage('������ �� �������� ������� �� ������');
    end;
end;

procedure TFrmLokacija.aPromeniExecute(Sender: TObject);
begin
    if DM1.TblKomLokacii.RecordCount > 0 then
    begin
        panelDole.Enabled := true;
        DM1.TblKomLokacii.Edit();

        enableGrid(false);

        cxControlCITACKA_KNIGA.Enabled := false;
        cxControlCITACKA_KNIGA_RB.Enabled := false;

        cxControlREON.SetFocus();

        cxControlREDEN_BROJ.Enabled := false;
    end
    else
    begin
        ShowMessage('���� �������� �� �������');
    end;

end;

procedure TFrmLokacija.aPromeniRedBrojVoKnigaExecute(Sender: TObject);
var frm:TFrmPromeniRedenBroj;
//var frm:TFrmPromeniCitackaKnigaRb;
begin
    if DM1.TblKomLokacii.RecordCount > 0 then
    begin
        if DM1.TblKomLokacii.State = dsBrowse then
        begin
            //frm := TFrmPromeniRedenBroj.Create(nil, DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString);
            frm := TFrmPromeniRedenBroj.Create(nil, DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString, DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString);
            //frm := TFrmPromeniCitackaKnigaRb.Create(nil, DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString,DM1.TblKomLokaciiPRED.AsString,DM1.TblKomLokaciiSLED.AsString,DM1.TblKomLokaciiREDEN_BROJ.AsString);
            frm.ShowModal();
            frm.Free();
        end
        else
        begin
            ShowMessage('�� ������ �� ����������� ����� ��� ������ ��������� ��� ����������� �������');
        end;
    end
    else
    begin
        ShowMessage('������ �� �������� ������� �� ������');
    end;
end;

procedure TFrmLokacija.aVodomeriExecute(Sender: TObject);
var frm:TFrmVodomeri;
begin
    if DM1.TblKomLokacii.State = dsBrowse then
    begin
        if DM1.TblKomLokacii.RecordCount > 0 then
        begin
            DM1.TblKomVodomeri.Close();
            DM1.TblKomVodomeri.SelectSQL.Text := DM1.selectVodomeriPoLokacija+DM1.TblKomLokaciiID.AsString;
            DM1.TblKomVodomeri.Open();

            //frm := TFrmVodomeri.Create(nil,cxTxtPartnerTIP_PARTNER.Text,cxTxtPartnerID.Text,cxTxtPartnerNaziv.Text,DM1.TblKomLokaciiID.AsString,DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString);
            frm := TFrmVodomeri.Create(nil,cxLookUpComboBoxTipPartner.Text,cxTxtPartnerID.Text,cxTxtPartnerNaziv.Text,DM1.TblKomLokaciiID.AsString,DM1.TblKomLokaciiCITACKA_KNIGA.AsString,DM1.TblKomLokaciiCITACKA_KNIGA_RB.AsString,DM1.TblKomLokaciiREON_ID.AsString);
            frm.ShowModal();
            frm.Free();
        end
        else
        begin
            ShowMessage('������ �� ������ ������� �� ������');
        end;
    end
    else
    begin
        ShowMessage('�� ������ �� �������� �� �������� ������ ��������� ��� ����������� �������');
    end;
end;

procedure TFrmLokacija.btnZapisiClick(Sender: TObject);

var  thr : THandle;
  thrID : DWORD;
  st : TDataSetState;
  line : string;
  akt:string;
  status: TStatusWindowHandle;
begin
 {
  thr := CreateThread(nil, 0, @threadProcedure, nil, 0, thrID);
  if (thr = 0) then
    ShowMessage('Thread not created')
   else
   begin
      ShowMessage('zavrsi threadot');
      Abort();
   end;
}
status := cxCreateStatusWindow();
   try

    if podatociOK then
    begin
        //if True then

         if cxGrid1DBTableView1.DataController.DataSource.State=dsEdit then
         begin
            if (DM1.TblKomLokaciiAKTIVEN.Value = 0) then
              begin
                  //������� ���� ��� ���� ���� �� ������� (������������ ������)
                  tblDopUsl.Close;
                  tblDopUsl.ParamByName('tip_partner').Value := DM1.TblKomLokaciiTIP_PARTNER.Value;
                  tblDopUsl.ParamByName('partner').Value := DM1.TblKomLokaciiPARTNER_ID.Value;
                  tblDopUsl.Open;

                  if (tblDopUslIMA.Value>0) then
                  begin
                      ShowMessage('���������� ��� ��������� ���� �� ������������ ������. '#10#13' �� ���� �� �� ����������!');
                      aIzlez.Execute;
                      Abort;
                  end;
              end;
         end;

        //DM1.TblKomLokaciiTIP_PARTNER.AsString := cxTxtPartnerTIP_PARTNER.Text;
           DM1.TblKomLokaciiTIP_PARTNER.AsString := cxLookUpComboBoxTipPartner.EditValue;
           DM1.TblKomLokaciiPARTNER_ID.AsString := cxTxtPartnerID.Text;


     //   dm1.TblKomLokaciiRB1.AsString:=cxControlCITACKA_KNIGA_RB.Text;
//         if cxGrid1DBTableView1.DataController.DataSource.State=dsInsert then
  //       begin
            st := cxGrid1DBTableView1.DataController.DataSet.State;
    //     end;


    //    dm1.TblKomLokaciiRB2.AsFloat:=(dm1.TblKomLokaciiRB2.AsFloat+qRB2.FldByName['rb2'].AsFloat)/2;
        cxGrid1DBTableView1.DataController.DataSet.Post;

  ///////////////////////////////////////////////////////////
 //edit vo tabelata KOM_PROMENA
 ///////////////////////////////////////
    if sos_int = '1' then  // od 1 vo 11,  duri da sredam , 01.08.2019
    begin
       if st in [dsEdit,dsInsert] then
        begin
       //   if servis = '0' then
        //  begin

              //javna pormenliva za oznaka deka ima promena
              ima_promena := '1';

          //  begin
           //  ShowMessage('���� �������� ��������!');
              DM1.TblKomLokacii.CloseOpen(true);
              panelDole.Enabled := false;
              enableGrid(true);
              cxGrid1.SetFocus();
              Abort;
          //  end


          // end;
          // begin
             // da se prati linkot
                // dm1.ZemiTextFile(cellink);
             //    if (copy(Trim(vraten_text),2,1) = '1') then
                // if pos('refreshed',vraten_text) > 0 then
            //     begin
                   //update vo tabelata KOM_PROMENA
             //      dm1.tblPromena.Edit;
             //      dm1.tblPromenaPROMENA.Value := 1;
              //     dm1.tblPromena.Post;
              //   end;
           //end;
          end;
      //  end;
    end;


        DM1.TblKomLokacii.CloseOpen(true);
        panelDole.Enabled := false;
        enableGrid(true);
        cxGrid1.SetFocus();
    end;
   finally
      	cxRemoveStatusWindow(status);
   end;
end;

procedure TFrmLokacija.callFrmPartneri;
var frmP:TfrmPartner;
begin
    partnerSet := false;
    frmP :=TfrmPartner.Create(nil,false);
    //frmP.getEdit(TEdit(cxTxtPartnerTIP_PARTNER),TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.getEditC(cxLookUpComboBoxTipPartner,TEdit(cxTxtPartnerID),TEdit(cxTxtPartnerNaziv));
    frmP.ShowModal();
    frmP.Free();
    if frmP.ModalResult = mrOk then
    begin
        prepareLocationsForPartner();
    end;
end;

procedure TFrmLokacija.cxBtnPartneriClick(Sender: TObject);
begin
    dmmat.tblPartner.close;
  //  dm1.tblPartner.ParamByName('tp').AsString:='%';
    dmmat.tblPartner.open;
    callFrmPartneri();
end;

procedure TFrmLokacija.cxControlCITACKA_KNIGA_RBExit(Sender: TObject);
begin
   if cxControlCITACKA_KNIGA_RB.Text<>'' then
   begin
    qCK.Close;
    qCK.ParamByName('r').AsString:=cxControlREON.Text;
    qCK.ParamByName('ck').AsString:=cxControlCITACKA_KNIGA.Text;
    qCK.ExecQuery;
    if (qCK.FldByName['citacka_kniga_rb'].AsInteger<strtoint(cxControlCITACKA_KNIGA_RB.Text)) and (not qCK.FldByName['citacka_kniga_rb'].IsNull) then
    begin
      cxControlCITACKA_KNIGA_RB.Text:=qCK.FldByName['citacka_kniga_rb'].AsString;
    end;
   end
   else
     Abort;
   TEdit(Sender).Color:=clWhite;
end;

procedure TFrmLokacija.getPartner;
begin
    partnerSet := false;
    DM1.QGetPartner.Params[0].AsString := cxTxtPartnerID.Text;
    //DM1.QGetPartner.Params[1].AsString := cxTxtPartnerTIP_PARTNER.Text;
    DM1.QGetPartner.Params[1].AsString := VarToStr(cxLookUpComboBoxTipPartner.EditValue);
    DM1.QGetPartner.ExecQuery();
    if DM1.QGetPartner.RecordCount >= 1 then
    begin
          //partnerSet := true;
          cxTxtPartnerNaziv.Text := DM1.QGetPartner.FieldByName('naziv').AsString;
          prepareLocationsForPartner();
    end
    else
    begin
          //cxTxtPartnerTIP_PARTNER.Text := '';
          cxLookUpComboBoxTipPartner.ItemIndex := -1;
          cxTxtPartnerID.Text := '';
          callFrmPartneri();
    end;

end;

procedure TFrmLokacija.onEnterAll(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TFrmLokacija.onExitAll(Sender: TObject);
begin
      TEdit(Sender).Color:=clWhite;
end;

procedure TFrmLokacija.onKeyDownAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
             Key := 0;
             if TEdit(sender).Name='cxTxtPartnerTIP_PARTNER' then
             begin
                PostMessage(Handle,WM_NextDlgCtl,0,0)
             end
             else if TEdit(sender).Name='cxTxtPartnerID' then
             begin
                  if ( (TEdit(sender).Text = '') or (cxLookUpComboBoxTipPartner.Text = '')) then
                  begin
                      callFrmPartneri();
                  end
                  else
                  begin
                      getPartner();
                  end;
             end
             else
             begin
                  PostMessage(Handle,WM_NextDlgCtl,0,0);
             end;
        end;
        VK_DOWN:
        begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
            Key := 0;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
    end;
end;

procedure TFrmLokacija.onKeyDownLookUpAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;
end;

procedure TFrmLokacija.prepareLocationsForPartner;
begin

    PanelGorePartner.Enabled := false;
    cxBtnPartneri.Enabled := false;
    cancelUpdates();
    if DM1.TblKomLokacii.Active = true then
    begin
        DM1.TblKomLokacii.Close();
    end;

    DM1.TblKomLokacii.Params[0].AsString := cxTxtPartnerID.Text;
    //DM1.TblKomLokacii.Params[1].AsString := cxTxtPartnerTIP_PARTNER.Text;
    DM1.TblKomLokacii.Params[1].AsString := VarToStr(cxLookUpComboBoxTipPartner.EditValue);
    DM1.TblKomLokacii.Open();

    DM1.openTblUliciAll();

    partnerSet := true;

    cxGrid1.SetFocus();
end;

procedure TFrmLokacija.cistiPodatoci;
begin
    cancelUpdates();

    DM1.TblKomLokacii.Close();
    PanelGorePartner.Enabled := true;
    cxTxtPartnerNaziv.Text := '';
    cxTxtPartnerID.Text := '';
    //cxTxtPartnerTIP_PARTNER.Text := '';
    cxLookUpComboBoxTipPartner.ItemIndex := -1;
    partnerSet := false;
    cxBtnPartneri.Enabled := true;
    //cxTxtPartnerTIP_PARTNER.SetFocus;
    cxLookUpComboBoxTipPartner.SetFocus;

    panelDole.Enabled := false;
end;

//procedure TFrmLokacija.cancelUpdates();
function TFrmLokacija.cancelUpdates():boolean;
var ret:boolean;
begin
    ret := false;
    if DM1.TblKomLokacii.State in [dsInsert, dsEdit] then
    begin
        DM1.TblKomLokacii.Cancel();
        ret := true;
    end;
    cancelUpdates := ret;
end;

function TFrmLokacija.podatociOK():boolean;
var ret:boolean;
begin
    ret := false;
    if cxLookUpREON.Text = '' then
    begin
        ShowMessage('������ �� ������� ����!');
        cxControlREON.SetFocus();
    end
    else if cxLookUpULICA.Text = '' then
    begin
        ShowMessage('������ �� ������� �����!');
        cxControlULICA.SetFocus();
    end
    else if cxControlCITACKA_KNIGA.Text = '' then
    begin
        ShowMessage('������ �� ������� ��� �� ������� �����!');
        cxControlCITACKA_KNIGA.SetFocus();
    end

    else if cxControlCITACKA_KNIGA_RB.Text = '' then
    begin
        ShowMessage('������ �� ������� ����� ��� �� ��������� �����!');
    end

    else
    begin
        ret := true;
    end;

    podatociOK := ret;
end;


procedure TFrmLokacija.enableGrid(enable:boolean);
begin
    cxGrid1.Enabled := enable;
end;

end.
