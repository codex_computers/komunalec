inherited FrmUlici: TFrmUlici
  Caption = #1059#1083#1080#1094#1080
  ClientHeight = 626
  ClientWidth = 602
  ExplicitWidth = 610
  ExplicitHeight = 660
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 363
    ExplicitWidth = 602
    ExplicitHeight = 373
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 369
      ExplicitWidth = 598
      ExplicitHeight = 369
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = DM1.DSKomUlici
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Width = 107
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          Width = 391
        end
        object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
          Caption = #1056#1077#1086#1085
          DataBinding.FieldName = 'REON_ID'
          Width = 98
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 489
    Width = 602
    Height = 114
    ExplicitTop = 483
    ExplicitWidth = 602
    ExplicitHeight = 114
    inherited Label1: TLabel
      Left = 6
      BiDiMode = bdRightToLeft
      ParentBiDiMode = False
      ExplicitLeft = 6
    end
    object lblReon: TLabel [1]
      Left = 6
      Top = 55
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1056#1077#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    object lblNaziv: TLabel [2]
      Left = 6
      Top = 80
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      BiDiMode = bdRightToLeft
      Caption = #1053#1072#1079#1080#1074
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBiDiMode = False
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 62
      Top = 26
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = DM1.DSKomUlici
      ExplicitLeft = 62
      ExplicitTop = 26
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 74
      TabOrder = 5
      ExplicitLeft = 511
      ExplicitTop = 74
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 74
      TabOrder = 4
      ExplicitLeft = 430
      ExplicitTop = 74
    end
    object cxControlREON: TcxDBTextEdit
      Left = 62
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'REON_ID'
      DataBinding.DataSource = DM1.DSKomUlici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cxLookUpREON: TcxDBLookupComboBox
      Left = 141
      Top = 51
      DataBinding.DataField = 'REON_ID'
      DataBinding.DataSource = DM1.DSKomUlici
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          FieldName = 'ID'
        end
        item
          Caption = #1053#1072#1079#1080#1074
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = DM1.DSKomReoni
      TabOrder = 2
      OnKeyDown = OnKeyDownAllLookUp
      Width = 150
    end
    object cxControlNAZIV: TcxDBTextEdit
      Left = 62
      Top = 76
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = DM1.DSKomUlici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 229
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 602
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 603
    Width = 602
    ExplicitTop = 603
    ExplicitWidth = 602
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 168
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 222
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 114
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.442213761570000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
