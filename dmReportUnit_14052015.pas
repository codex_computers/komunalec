unit dmReportUnit;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, frxClass, frxDesgn, frxDBSet,
  frxRich, frxExportXML, Vcl.Dialogs, IdMessage, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient,
  IdSMTPBase, IdSMTP,  IdAttachmentFile, IdAttachmentMemory, frxExportPDF;

type
  TdmReport = class(TDataModule)
    tblReportDizajn: TpFIBDataSet;
    tblReportDizajnSQL: TFIBBlobField;
    tblReportDizajnDATA: TFIBBlobField;
    tblSqlReport: TpFIBDataSet;
    qp: TfrxDBDataset;
    frxReport1: TfrxReport;
    tblDetail: TpFIBDataSet;
    frxDBDataset2: TfrxDBDataset;
    dsMaster: TDataSource;
    frxRichObject1: TfrxRichObject;
    tblDetailTuzeniSmetki: TpFIBDataSet;
    frxTblDetailTuzeniSmetki: TfrxDBDataset;
    frxXMLExport1: TfrxXMLExport;
    dlgSaveSaveToExcelDialog: TSaveDialog;
    SaveDialogReport: TSaveDialog;
    SMTP: TIdSMTP;
    EmailMessage: TIdMessage;
    frxPDFExport1: TfrxPDFExport;
    function frxDesigner1SaveReport(Report: TfrxReport;
      SaveAs: Boolean): Boolean;
    procedure qpClose(Sender: TObject);
    procedure qpFirst(Sender: TObject);
    procedure qpNext(Sender: TObject);
    procedure qpOpen(Sender: TObject);
  private
    { Private declarations }
  public

    Attachment : TIdAttachmentFile;
    AttachmentMem : TIdAttachmentMemory;

    procedure Spremi(id :Integer; app : string);
    procedure BrisiStarReport(id: Integer);
    procedure SendIndyEmail(RecipientEmail : AnsiString; EmailSubject : AnsiString; BodyText : AnsiString; attachment_file : AnsiString = ''; attachment_stream : TMemoryStream = nil; naziv_fajl : AnsiString = '');

  end;

var
  dmReport: TdmReport;

implementation

uses dmKonekcija, dmMaticni;



{$R *.dfm}

procedure TdmReport.qpClose(Sender: TObject);
begin
//  frxDBDataset2.Close;
//  frxTblDetailTuzeniSmetki.Close;
end;

procedure TdmReport.qpFirst(Sender: TObject);
begin
//  frxDBDataset2.First;
//  frxTblDetailTuzeniSmetki.First;
end;

procedure TdmReport.qpNext(Sender: TObject);
begin
//  frxDBDataset2.Open;
//  frxTblDetailTuzeniSmetki.Open;
end;

procedure TdmReport.qpOpen(Sender: TObject);
begin
//  frxDBDataset2.Open;
//  frxTblDetailTuzeniSmetki.Open;
end;

function TdmReport.frxDesigner1SaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var template : TStream;
begin
  if (SaveAs = false) then
  begin
    template := TMemoryStream.Create;
    template.Position := 0;
    frxReport1.SaveToStream(template);
    dmKon.tblReportDizajn.Open;
    dmKon.tblReportDizajn.Edit;
    (dmKon.tblReportDizajnDATA as TBlobField).LoadFromStream(template);
    dmKon.tblReportDizajn.Post;

    FreeAndNil(template);
  end
  else
  begin
    // Give the dialog a title
    SaveDialogReport.Title := '����� �� �������� �� ��������';

    // Set up the starting directory to be the current one
    SaveDialogReport.InitialDir := GetCurrentDir;

    // Allow only .txt and .doc file types to be saved
    SaveDialogReport.Filter := 'FastReport file|*.fr3';

    // Set the default extension
    SaveDialogReport.DefaultExt := 'fr3';

    // Select text files as the starting filter type
    SaveDialogReport.FilterIndex := 1;

    // Display the open file dialog
    if SaveDialogReport.Execute then frxReport1.SaveToFile(SaveDialogReport.FileName);
  end;
end;

procedure TdmReport.Spremi(id :Integer ; app:string);
//procedure Spremi(app : string; br :Integer);
var
   i : integer;
var
  RptStream: TStream;
  SqlStream: TStream;
    RptStream2: TStream;
  SqlStream2: TStream;
    RptStream3: TStream;
  SqlStream3: TStream;
begin

  frxReport1.Clear;
  frxReport1.Script.Clear;

  // ������ �� cxRound ��������� �� FastReport-��
  frxReport1.AddFunction('function cxRound(broj: Double; decimali: Integer = 0): Double;', 'Codex functions', '������������ �� ��������� ��� �� ���������� ������� �� ����������. Default � �� 0 �.� �� ��� ���');

  tblReportDizajn.Close;
  tblReportDizajn.Params.ParamByName('APP').Value := app;
  tblReportDizajn.Params.ParamByName('BR').Value := id;
  tblReportDizajn.Open;

  tblSqlReport.Close;
  //dmKon.tblSqlReport2.Close;
  //dmKon.tblSqlReport3.Close;

  for i := 0 to tblSqlReport.Params.Count - 1 do
        tblSqlReport.Params[i].Clear;
//  for i := 0 to dmKon.tblSqlReport2.Params.Count - 1 do
//        dmKon.tblSqlReport2.Params[i].Clear;
//  for i := 0 to dmKon.tblSqlReport3.Params.Count - 1 do
//        dmKon.tblSqlReport3.Params[i].Clear;

  try
    SqlStream := tblReportDizajn.CreateBlobStream(tblReportDizajnSQL , bmRead);
    tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);
//    SqlStream2 := dmKon.tblReportDizajn.CreateBlobStream(dmKon.tblReportDizajnSQL2 , bmRead);
//    dmKon.tblSqlReport2.SQLs.SelectSQL.LoadFromStream(SqlStream2);
//    SqlStream3 := dmKon.tblReportDizajn.CreateBlobStream(dmKon.tblReportDizajnSQL3 , bmRead);
//    dmKon.tblSqlReport3.SQLs.SelectSQL.LoadFromStream(SqlStream3);

    RptStream := tblReportDizajn.CreateBlobStream(tblReportDizajnDATA , bmRead);
    frxReport1.LoadFromStream(RptStream);
  finally
   FreeAndNil(SqlStream);
   //FreeAndNil(SqlStream2);
   //FreeAndNil(SqlStream3);
   FreeAndNil(RptStream);
  end;
end;

procedure TdmReport.BrisiStarReport(id :Integer);
var
  SqlStream: TStream;
begin
  tblReportDizajn.Close;
  tblReportDizajn.Params.ParamByName('id').Value := id;
  tblReportDizajn.Open;

  tblSqlReport.Close;
  SqlStream := tblReportDizajn.CreateBlobStream(tblReportDizajnSQL , bmRead);
  tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);
end;

 // Prati email preku indy komponentata
procedure TdmReport.SendIndyEmail(RecipientEmail : AnsiString; EmailSubject : AnsiString; BodyText : AnsiString; attachment_file : AnsiString = ''; attachment_stream : TMemoryStream = nil; naziv_fajl : AnsiString = '');
begin
  dmMat.tblMailSetup.Close;
  dmMat.tblMailSetup.Open;
  if (dmMat.tblMailSetup.Locate('RE', dmKon.re, [])) then
  begin
    //setup SMTP
    SMTP.Host := dmMat.tblMailSetupSMTP_HOST.Value;
    SMTP.Port := dmMat.tblMailSetupSMTP_PORT.Value;
    SMTP.Username := dmMat.tblMailSetupLOGIN.Value;
    SMTP.Password := dmMat.tblMailSetupLOGIN_PASSWORD.Value;

    EmailMessage.Clear;

    EmailMessage.CharSet := 'utf-8';
    EmailMessage.AttachmentEncoding := 'UUE';
    EmailMessage.From.Address := dmMat.tblMailSetupFROM_MAIL.Value;

     //setup mail message
    EmailMessage.Recipients.EMailAddresses := RecipientEmail;
    EmailMessage.Subject := EmailSubject;
    EmailMessage.Body.Text :=  UTF8Encode(BodyText + sLineBreak  + sLineBreak + dmKon.firma_naziv + sLineBreak + dmKon.firma_adresa + sLineBreak + dmKon.firma_mesto);

    // ����� BCC ����� �� ����������
    EmailMessage.BccList.Clear;

    if dmMat.tblMailSetupCOPY_TO_SENDER.Value = 1 then EmailMessage.BccList.Add.Address := dmKon.UserEmail;

    if (attachment_file <> '') then
    begin
      if (FileExists(attachment_file)) then Attachment := TIdAttachmentFile.Create(EmailMessage.MessageParts,attachment_file);
  //    Attachment.FileName := attachment_file;
    end;

    if (attachment_stream <> nil) then
    begin
      AttachmentMem := TIdAttachmentMemory.Create(EmailMessage.MessageParts);
      AttachmentMem.LoadFromStream(attachment_stream);

      AttachmentMem.ContentType := 'application/pdf';
      AttachmentMem.FileName := naziv_fajl;
    end;

    //send mail
    try
      try
        SMTP.Connect;
        SMTP.Send(EmailMessage);
        ShowMessage('�������� � ������� ������� �� : ' + RecipientEmail);
      except on E:Exception do
        ShowMessage('������: ' + E.Message);
      end;
    finally
      SMTP.Disconnect;
    end;
  end
  else
  ShowMessage('������! ������ ���������� email ������ �� �������/���.�������');
end;


end.
