object frmLokaciiPoPartner: TfrmLokaciiPoPartner
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1051#1086#1082#1072#1094#1080#1112#1072' '#1087#1086' '#1050#1086#1088#1080#1089#1085#1080#1094#1080' ('#1055#1072#1088#1090#1085#1077#1088#1080')'
  ClientHeight = 482
  ClientWidth = 843
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    843
    482)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 843
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 459
    Width = 843
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object pclokacija: TcxPageControl
    Left = 0
    Top = 126
    Width = 843
    Height = 333
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = tsLokacija
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.NativeStyle = False
    ClientRectBottom = 333
    ClientRectRight = 843
    ClientRectTop = 24
    object tsLokacija: TcxTabSheet
      Caption = #1051#1086#1082#1072#1094#1080#1080' '
      ImageIndex = 0
      object LPanel: TPanel
        Left = 0
        Top = 0
        Width = 843
        Height = 201
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object gbPartner: TcxGroupBox
          Left = 24
          Top = 138
          Hint = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1051#1086#1082#1072#1094#1080#1112#1072
          Caption = '  '#1058#1077#1082#1086#1074#1077#1085' '#1055#1072#1088#1090#1085#1077#1088' '#1085#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1051#1054#1050#1040#1062#1048#1032#1040' '
          Enabled = False
          ParentFont = False
          Style.Edges = [bLeft, bTop, bRight, bBottom]
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          Style.TextStyle = []
          Style.TransparentBorder = True
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          TabOrder = 0
          DesignSize = (
            611
            57)
          Height = 57
          Width = 611
          object txtTipP1: TcxDBTextEdit
            Tag = 1
            Left = 18
            Top = 21
            DataBinding.DataField = 'TIP_PARTNER'
            DataBinding.DataSource = DM1.dsLokacijaArhiva
            TabOrder = 3
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 37
          end
          object txtIdP1: TcxDBTextEdit
            Tag = 1
            Left = 55
            Top = 21
            DataBinding.DataField = 'PARTNER'
            DataBinding.DataSource = DM1.dsLokacijaArhiva
            TabOrder = 4
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 39
          end
          object cxButton1: TcxButton
            Left = 513
            Top = 20
            Width = 75
            Height = 22
            Action = aZapisiLok
            Anchors = [akTop, akRight]
            TabOrder = 5
          end
          object txtTipP: TcxTextEdit
            Left = 18
            Top = 21
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 37
          end
          object txtIdP: TcxTextEdit
            Left = 55
            Top = 21
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 71
          end
          object cbPartner: TcxExtLookupComboBox
            Left = 126
            Top = 21
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.View = repPartner
            Properties.KeyFieldNames = 'tip_partner;ID'
            Properties.ListFieldItem = repPartnerNAZIV
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 345
          end
        end
      end
      object DPanel: TPanel
        Left = 0
        Top = 201
        Width = 843
        Height = 108
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          843
          108)
        object cxVeritikal1DBTableView1: TcxDBVerticalGrid
          Left = 24
          Top = 6
          Width = 788
          Height = 57
          Anchors = [akLeft, akTop, akRight]
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          LookAndFeel.NativeStyle = False
          OptionsView.ScrollBars = ssNone
          OptionsBehavior.IncSearch = True
          OptionsData.Editing = False
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          Navigator.Buttons.CustomButtons = <>
          ParentFont = False
          Styles.Selection = dmRes.Bright
          TabOrder = 0
          DataController.DataSource = dsPartner
          Version = 1
          object cxVeritikal1DBTableView1DBEditorRow2: TcxDBEditorRow
            Properties.DataBinding.FieldName = 'ADRESA'
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxVeritikal1DBTableView1DBEditorRow3: TcxDBEditorRow
            Properties.Caption = #1052#1077#1089#1090#1086
            Properties.DataBinding.FieldName = 'MestoNaziv'
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object cxVeritikal1DBTableView1DBEditorRow1: TcxDBEditorRow
            Properties.Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
            Properties.DataBinding.FieldName = 'NAZIV_TP'
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
        end
        object cxButton2: TcxButton
          Left = 721
          Top = 72
          Width = 75
          Height = 22
          Action = aOtkazi
          Anchors = [akTop, akRight]
          TabOrder = 1
        end
      end
    end
    object tsLokacijaPartner: TcxTabSheet
      Caption = #1051#1086#1082#1072#1094#1080#1112#1072' '#1087#1086' '#1055#1072#1088#1090#1085#1077#1088
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 105
        Width = 843
        Height = 204
        Align = alBottom
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
          DataController.DataSource = DM1.dsLokacijaArhiva
          DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
          end
          object cxGrid1DBTableView1ID_LOKACIJA: TcxGridDBColumn
            Caption = #1051#1054#1050#1040#1062#1048#1032#1040
            DataBinding.FieldName = 'ID_LOKACIJA'
            Visible = False
            GroupIndex = 0
            Options.Editing = False
            Width = 54
          end
          object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1055#1072#1088#1090#1085#1077#1088
            DataBinding.FieldName = 'TIP_PARTNER'
            Options.Editing = False
            Width = 27
          end
          object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
            DataBinding.FieldName = 'PARTNER'
            Options.Editing = False
            Width = 44
          end
          object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
            Caption = #1053#1072#1079#1080#1074
            DataBinding.FieldName = 'NAZIV_PARTNER'
            Options.Editing = False
            Width = 253
          end
          object cxGrid1DBTableView1DATUM: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM'
            Visible = False
            Options.Editing = False
          end
          object cxGrid1DBTableView1POC_MESEC: TcxGridDBColumn
            Caption = #1055#1086#1095#1077#1090#1077#1085' '#1084#1077#1089#1077#1094
            DataBinding.FieldName = 'POC_MESEC'
            Width = 74
          end
          object cxGrid1DBTableView1POC_GODINA: TcxGridDBColumn
            Caption = #1055#1086#1095#1077#1090#1085#1072' '#1075#1086#1076#1080#1085#1072
            DataBinding.FieldName = 'POC_GODINA'
            Width = 72
          end
          object cxGrid1DBTableView1KRAJ_MESEC: TcxGridDBColumn
            Caption = #1050#1088#1072#1077#1085' '#1084#1077#1089#1077#1094
            DataBinding.FieldName = 'KRAJ_MESEC'
            Width = 62
          end
          object cxGrid1DBTableView1KRAJ_GODINA: TcxGridDBColumn
            Caption = #1050#1088#1072#1112#1085#1072' '#1075#1086#1076#1080#1085#1072
            DataBinding.FieldName = 'KRAJ_GODINA'
            Width = 78
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object gbLokacija: TcxGroupBox
    Left = 24
    Top = 154
    Anchors = [akLeft, akTop, akRight]
    Caption = '  '#1047#1072' '#1051#1086#1082#1072#1094#1080#1112#1072'  '
    ParentFont = False
    Style.Font.Charset = RUSSIAN_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -12
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.Shadow = False
    Style.TextStyle = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 3
    Height = 76
    Width = 788
    object Label3: TLabel
      Left = 151
      Top = 16
      Width = 58
      Height = 18
      AutoSize = False
      Caption = #1040#1076#1088#1077#1089#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 151
      Top = 34
      Width = 33
      Height = 18
      AutoSize = False
      Caption = #1056#1077#1086#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 151
      Top = 53
      Width = 58
      Height = 18
      AutoSize = False
      Caption = #1063#1080#1090'.'#1082#1085#1080#1075#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object txtLokacija: TcxTextEdit
      Tag = 1
      Left = 16
      Top = 20
      Hint = #1042#1085#1077#1089#1077#1090#1077' '#1083#1086#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1082#1086#1112#1072' '#1089#1072#1082#1072#1090#1077' '#1076#1072' '#1075#1086' '#1089#1084#1077#1085#1080#1090#1077' '#1087#1072#1088#1090#1085#1077#1088#1086#1090
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 105
    end
    object lblBrojCK: TcxDBLabel
      Left = 213
      Top = 51
      AutoSize = True
      DataBinding.DataField = 'CITACKA_KNIGA'
      DataBinding.DataSource = DM1.dsLokacija
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextStyle = []
      Style.IsFontAssigned = True
      StyleHot.TextStyle = []
      Transparent = True
    end
    object lblAdresa: TcxDBLabel
      Left = 213
      Top = 14
      AutoSize = True
      DataBinding.DataField = 'ADRESA'
      DataBinding.DataSource = DM1.dsLokacija
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextStyle = []
      Style.IsFontAssigned = True
      StyleHot.TextStyle = []
      Transparent = True
    end
    object lblReon: TcxDBLabel
      Left = 213
      Top = 31
      DataBinding.DataField = 'NAZIV_REON'
      DataBinding.DataSource = DM1.dsLokacija
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextStyle = []
      Style.IsFontAssigned = True
      StyleHot.TextStyle = []
      Transparent = True
      Height = 21
      Width = 205
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 624
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 729
    Top = 51
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 799
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1040#1056#1058#1053#1045#1056' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 132
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 339
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPromeniPartner
      Caption = #1055#1088#1086#1084#1077#1085#1080
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiP
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 589
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aPromeniPartner: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1055#1072#1088#1090#1085#1077#1088
      Hint = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1051#1086#1082#1072#1094#1080#1112#1072
      ImageIndex = 14
      ShortCut = 116
      OnExecute = aPromeniPartnerExecute
    end
    object aZapisiLok: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 6
      OnExecute = aZapisiLokExecute
    end
    object aAzurirajP: TAction
      Caption = 'aAzurirajP'
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajPExecute
    end
    object aBrisiP: TAction
      Caption = #1041#1088#1080#1096#1080
      Hint = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1055#1072#1088#1090#1085#1077#1088#1086#1090', '#1076#1086#1082#1086#1083#1082#1091' '#1089#1077' '#1091#1096#1090#1077' '#1085#1077#1084#1072' '#1085#1072#1087#1088#1072#1074#1077#1085#1086'  '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      ImageIndex = 20
      ShortCut = 119
      OnExecute = aBrisiPExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 624
    Top = 51
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43731.358214907410000000
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsPreview.MaxLineCount = 3
      OptionsPreview.Visible = False
      OptionsSize.AutoWidth = True
      OptionsView.Footers = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      OptionsView.GroupFooters = False
      StyleRepository = cxStyleRepository2
      Styles.BandHeader = cxStyle2
      Styles.Caption = cxStyle3
      Styles.FilterBar = cxStyle4
      Styles.Footer = cxStyle5
      Styles.Header = cxStyle6
      Styles.Selection = cxStyle7
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 589
    Top = 51
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 694
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
  end
  object tblPartner: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    p.TIP_PARTNER,'
      '    p.ID,'
      '   p.NAZIV||'#39'  ('#39'||p.tip_partner||'#39'/'#39'||p.id||'#39')'#39' naziv,'
      '    p.ADRESA,'
      '    p.MESTO,'
      '    p.IME,'
      '    p.PREZIME,'
      '    mtp.naziv naziv_tp'
      'from mat_partner p'
      'inner join mat_tip_partner mtp on mtp.id=p.tip_partner'
      '--where p.tip_partner like :tp'
      '--and p.id like :p'
      '--order by p.TIP_PARTNER,'
      ' --   p.ID'
      ''
      ' WHERE '
      '        P.TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and P.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    p.TIP_PARTNER,'
      '    p.ID,'
      '   p.NAZIV||'#39'  ('#39'||p.tip_partner||'#39'/'#39'||p.id||'#39')'#39' naziv,'
      '    p.ADRESA,'
      '    p.MESTO,'
      '    p.IME,'
      '    p.PREZIME,'
      '    mtp.naziv naziv_tp'
      'from mat_partner p'
      'inner join mat_tip_partner mtp on mtp.id=p.tip_partner'
      '--where p.tip_partner like :tp'
      '--and p.id like :p'
      '--order by p.TIP_PARTNER,'
      ' --   p.ID')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 659
    Top = 51
    poSQLINT64ToBCD = True
    oTrimCharFields = False
    oRefreshAfterPost = False
    object tblPartnerID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPartnerNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 177
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerTipPartnerNaziv: TStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072#1079#1080#1074
      FieldKind = fkLookup
      FieldName = 'TipPartnerNaziv'
      LookupDataSet = dmMat.tblTipPartner
      LookupKeyFields = 'ID'
      LookupResultField = 'NAZIV'
      KeyFields = 'TIP_PARTNER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object tblPartnerMestoNaziv: TStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072#1079#1080#1074
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'MestoNaziv'
      LookupDataSet = dmMat.tblMesto
      LookupKeyFields = 'ID'
      LookupResultField = 'NAZIV'
      KeyFields = 'MESTO'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object tblPartnerTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblPartnerMESTO: TFIBIntegerField
      FieldName = 'MESTO'
    end
    object tblPartnerNAZIV_TP: TFIBStringField
      FieldName = 'NAZIV_TP'
      Size = 101
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPartner: TDataSource
    AutoEdit = False
    DataSet = tblPartner
    Left = 764
    Top = 16
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 659
    Top = 16
    object repPartner: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsPartner
      DataController.KeyFieldNames = 'tip_partner;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object repPartnerTIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Visible = False
        Width = 65
      end
      object repPartnerID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Width = 70
      end
      object repPartnerNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 422
      end
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 729
    Top = 16
    PixelsPerInch = 96
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWhite
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWindow
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clWindowText
    end
  end
  object qImaPresmetka: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select 1 da'
      'from kom_lokacija_arhiva kla'
      'where'
      '    kla.id_lokacija=:lokacija and'
      '    kla.tip_partner=:tp and'
      '    kla.partner=:p and'
      '    kla.poc_mesec is null and'
      '    kla.poc_godina is null')
    Left = 694
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
