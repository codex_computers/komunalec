object FrmPromeniRedenBroj: TFrmPromeniRedenBroj
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1084#1077#1085#1080' '#1088#1077#1076#1077#1085' '#1073#1088#1086#1112
  ClientHeight = 181
  ClientWidth = 369
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelGlaven: TPanel
    Left = 0
    Top = 0
    Width = 369
    Height = 162
    Align = alClient
    BevelInner = bvLowered
    Color = 13079700
    ParentBackground = False
    TabOrder = 0
    object lblCitackaKniga: TLabel
      Left = 16
      Top = 24
      Width = 75
      Height = 13
      Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
    end
    object lblRedenBrojOld: TLabel
      Left = 16
      Top = 56
      Width = 83
      Height = 13
      Caption = #1057#1090#1072#1088' '#1088#1077#1076#1077#1085' '#1073#1088#1086'j'
    end
    object lblCitackaKnigaRbNew: TLabel
      Left = 16
      Top = 112
      Width = 77
      Height = 13
      Caption = #1053#1086#1074' '#1088#1077#1076#1077#1085' '#1073#1088#1086#1112
    end
    object Label1: TLabel
      Left = 16
      Top = 85
      Width = 166
      Height = 13
      Caption = #1053#1086#1074#1072' '#1080#1083#1080' '#1089#1090#1072#1088#1072'  - '#1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
    end
    object cxTxtCitackaKniga: TcxTextEdit
      Left = 200
      Top = 21
      Enabled = False
      TabOrder = 0
      Width = 105
    end
    object cxTxtRedenBrojOld: TcxTextEdit
      Left = 200
      Top = 52
      Enabled = False
      TabOrder = 1
      Width = 105
    end
    object cxTxtRedenBrojNew: TcxTextEdit
      Left = 200
      Top = 109
      TabOrder = 2
      OnEnter = onTxtEnter
      OnExit = onTxtExit
      OnKeyDown = onTxtKeyDown
      Width = 105
    end
    object cxTxtCitackaKnigaNew: TcxTextEdit
      Left = 200
      Top = 82
      TabOrder = 3
      Width = 105
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 162
    Width = 369
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'Esc - '#1048#1079#1083#1077#1079
  end
  object ActionList1: TActionList
    Left = 320
    Top = 24
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
  end
end
