object frmRepNurSinhro: TfrmRepNurSinhro
  Left = 0
  Top = 0
  Caption = 'frmRepNurSinhro'
  ClientHeight = 525
  ClientWidth = 928
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object timer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = timerTimer
    Left = 472
    Top = 112
  end
  object tblRepUpd: TpFIBDataSet
    Tag = 2
    UpdateSQL.Strings = (
      'UPDATE SYS_REPORT'
      'SET '
      '    VREP_ID = :VREP_ID,'
      '    KOREN = :KOREN,'
      '    NASLOV = :NASLOV,'
      '    BR = :BR,'
      '    TABELA_GRUPA = :TABELA_GRUPA,'
      '    REP_PRETPLATA = :REP_PRETPLATA,'
      '    XML = :XML,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_REPORT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_REPORT('
      '    ID,'
      '    VREP_ID,'
      '    KOREN,'
      '    NASLOV,'
      '    BR,'
      '    TABELA_GRUPA,'
      '    REP_PRETPLATA,'
      '    XML,'
      '    DATA'
      ')'
      'VALUES('
      '    :ID,'
      '    :VREP_ID,'
      '    :KOREN,'
      '    :NASLOV,'
      '    :BR,'
      '    :TABELA_GRUPA,'
      '    :REP_PRETPLATA,'
      '    :XML,'
      '    :DATA'
      ')')
    RefreshSQL.Strings = (
      
        'select r.id,r.vrep_id,r.koren,r.naslov,r.br,r.tabela_grupa,r.rep' +
        '_pretplata,r.xml,r.data'
      'from sys_report r'
      'where(  r.id=:repid'
      '     ) and (     R.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'select r.id,r.vrep_id,r.koren,r.naslov,r.br,r.tabela_grupa,r.rep' +
        '_pretplata,r.xml,r.data'
      'from sys_report r'
      'where r.id=:repid')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AfterPost = tblNurUpdAfterPost
    BeforePost = tblNurUpdBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 192
    Top = 112
    object tblRepUpdVREP_ID: TFIBIntegerField
      FieldName = 'VREP_ID'
    end
    object tblRepUpdKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRepUpdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblRepUpdNASLOV: TFIBStringField
      FieldName = 'NASLOV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRepUpdBR: TFIBIntegerField
      FieldName = 'BR'
    end
    object tblRepUpdTABELA_GRUPA: TFIBStringField
      FieldName = 'TABELA_GRUPA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRepUpdREP_PRETPLATA: TFIBSmallIntField
      FieldName = 'REP_PRETPLATA'
    end
    object tblRepUpdXML: TFIBBlobField
      FieldName = 'XML'
      Size = 8
    end
    object tblRepUpdDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object dsRepUpd: TDataSource
    AutoEdit = False
    DataSet = tblRepUpd
    Left = 286
    Top = 112
  end
  object tblrr: TpFIBDataSet
    Tag = 2
    RefreshSQL.Strings = (
      'select * from SYS_REPLIKATOR rp'
      ''
      ' WHERE '
      '        RP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select max(coalesce(vrep_id,0)) mvrep_id from sys_report'
      '')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 384
    Top = 112
    object tblrrMVREP_ID: TFIBIntegerField
      FieldName = 'MVREP_ID'
    end
  end
  object tblnn: TpFIBDataSet
    Tag = 2
    RefreshSQL.Strings = (
      'select * from SYS_REPLIKATOR rp'
      ''
      ' WHERE '
      '        RP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select max(coalesce(rec_id,0)) rec_id from sys_nurko')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 384
    Top = 176
    object tblnnREC_ID: TFIBBCDField
      FieldName = 'REC_ID'
      Size = 0
    end
  end
  object tblNurUpd: TpFIBDataSet
    Tag = 2
    UpdateSQL.Strings = (
      'UPDATE SYS_NURKO'
      'SET '
      '    REC_ID = :REC_ID,'
      '    VID = :VID,'
      '    IME = :IME,'
      '    OPIS = :OPIS,'
      '    SEL = :SEL,'
      '    VISINA = :VISINA,'
      '    SIRINA = :SIRINA,'
      '    KEY_FIELD_NAMES = :KEY_FIELD_NAMES,'
      '    LIST_FIELD_ITEM = :LIST_FIELD_ITEM,'
      '    GRUPA = :GRUPA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_NURKO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_NURKO('
      '    ID,'
      '    REC_ID,'
      '    VID,'
      '    IME,'
      '    OPIS,'
      '    SEL,'
      '    VISINA,'
      '    SIRINA,'
      '    KEY_FIELD_NAMES,'
      '    LIST_FIELD_ITEM,'
      '    GRUPA'
      ')'
      'VALUES('
      '    :ID,'
      '    :REC_ID,'
      '    :VID,'
      '    :IME,'
      '    :OPIS,'
      '    :SEL,'
      '    :VISINA,'
      '    :SIRINA,'
      '    :KEY_FIELD_NAMES,'
      '    :LIST_FIELD_ITEM,'
      '    :GRUPA'
      ')')
    RefreshSQL.Strings = (
      'select'
      '      n.id,n.rec_id,n.vid, n.ime, n.opis, n.sel,'
      
        '      n.visina, n.sirina, n.key_field_names, n.list_field_item, ' +
        'n.grupa'
      'from sys_nurko n'
      'where(  n.id=:nurid'
      '     ) and (     N.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '      n.id,n.rec_id,n.vid, n.ime, n.opis, n.sel,'
      
        '      n.visina, n.sirina, n.key_field_names, n.list_field_item, ' +
        'n.grupa'
      'from sys_nurko n'
      'where n.id=:nurid')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AfterPost = tblNurUpdAfterPost
    BeforePost = tblNurUpdBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 192
    Top = 184
    object tblNurUpdID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblNurUpdREC_ID: TFIBBCDField
      FieldName = 'REC_ID'
      Size = 0
    end
    object tblNurUpdVID: TFIBIntegerField
      FieldName = 'VID'
    end
    object tblNurUpdIME: TFIBStringField
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNurUpdOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNurUpdSEL: TFIBBlobField
      FieldName = 'SEL'
      Size = 8
    end
    object tblNurUpdVISINA: TFIBIntegerField
      FieldName = 'VISINA'
    end
    object tblNurUpdSIRINA: TFIBIntegerField
      FieldName = 'SIRINA'
    end
    object tblNurUpdKEY_FIELD_NAMES: TFIBStringField
      FieldName = 'KEY_FIELD_NAMES'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNurUpdLIST_FIELD_ITEM: TFIBStringField
      FieldName = 'LIST_FIELD_ITEM'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNurUpdGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblKanal: TpFIBDataSet
    Tag = 2
    RefreshSQL.Strings = (
      'select * from SYS_REPLIKATOR rp'
      ''
      ' WHERE '
      '        RP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select ss.v1 kanal from sys_setup ss'
      'where ss.p1='#39'UpgradeKanal'#39' and ss.p2='#39'ap7'#39)
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 192
    Top = 256
    object tblKanalKANAL: TFIBStringField
      FieldName = 'KANAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tbloo: TpFIBDataSet
    Tag = 2
    UpdateSQL.Strings = (
      'UPDATE PZZ_OSIGURENICI'
      'SET '
      '    EZBO = :EZBO,'
      '    PREZIME = :PREZIME,'
      '    T_IME = :T_IME,'
      '    IME = :IME,'
      '    DATUM_RAGJANJE = :DATUM_RAGJANJE,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    LEKAR = :LEKAR,'
      '    FAKSIMIL_STOMATOLOG = :FAKSIMIL_STOMATOLOG,'
      '    FAKSIMIL_GINEKOLOG = :FAKSIMIL_GINEKOLOG,'
      '    OSNOV_NA_OSIGURUVANJE = :OSNOV_NA_OSIGURUVANJE,'
      '    PRAVO_NA_OSIGURUVANJE = :PRAVO_NA_OSIGURUVANJE,'
      '    VAZNOST_NA_OSIGURUVANJE = :VAZNOST_NA_OSIGURUVANJE,'
      '    Z_FOND = :Z_FOND,'
      '    Z_BROJ = :Z_BROJ,'
      '    Z_CLEN = :Z_CLEN,'
      '    BZL = :BZL,'
      '    NEMAGO = :NEMAGO'
      'WHERE'
      '    EMBG = :OLD_EMBG'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PZZ_OSIGURENICI'
      'WHERE'
      '        EMBG = :OLD_EMBG'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PZZ_OSIGURENICI('
      '    EMBG,'
      '    EZBO,'
      '    PREZIME,'
      '    T_IME,'
      '    IME,'
      '    DATUM_RAGJANJE,'
      '    ADRESA,'
      '    TELEFON,'
      '    LEKAR,'
      '    FAKSIMIL_STOMATOLOG,'
      '    FAKSIMIL_GINEKOLOG,'
      '    OSNOV_NA_OSIGURUVANJE,'
      '    PRAVO_NA_OSIGURUVANJE,'
      '    VAZNOST_NA_OSIGURUVANJE,'
      '    Z_FOND,'
      '    Z_BROJ,'
      '    Z_CLEN,'
      '    BZL,'
      '    NEMAGO'
      ')'
      'VALUES('
      '    :EMBG,'
      '    :EZBO,'
      '    :PREZIME,'
      '    :T_IME,'
      '    :IME,'
      '    :DATUM_RAGJANJE,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :LEKAR,'
      '    :FAKSIMIL_STOMATOLOG,'
      '    :FAKSIMIL_GINEKOLOG,'
      '    :OSNOV_NA_OSIGURUVANJE,'
      '    :PRAVO_NA_OSIGURUVANJE,'
      '    :VAZNOST_NA_OSIGURUVANJE,'
      '    :Z_FOND,'
      '    :Z_BROJ,'
      '    :Z_CLEN,'
      '    :BZL,'
      '    :NEMAGO'
      ')')
    RefreshSQL.Strings = (
      'select'
      '      distinct(os.embg) dist,'
      '      os.embg, os.ezbo,'
      '      os.prezime, os.t_ime, os.ime,'
      '      os.datum_ragjanje, os.adresa, os.telefon,'
      ''
      '      os.lekar,'
      '      os.faksimil_stomatolog,'
      '      os.faksimil_ginekolog,'
      ''
      '      os.osnov_na_osiguruvanje,'
      '      os.pravo_na_osiguruvanje,'
      '      os.vaznost_na_osiguruvanje,'
      ''
      '      os.z_fond, os.z_broj, os.z_clen,'
      '      os.bzl,'
      '      os.nemago'
      'from pzz_osigurenici os'
      'inner join ap7_recepti r on r.embg=os.embg'
      
        'where(  (os.vaznost_na_osiguruvanje<current_date or (os.vaznost_' +
        'na_osiguruvanje is null)) and coalesce(os.nemago,0)<5'
      '     ) and (     OS.EMBG = :OLD_EMBG'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '      distinct(os.embg) dist,'
      '      os.embg, os.ezbo,'
      '      os.prezime, os.t_ime, os.ime,'
      '      os.datum_ragjanje, os.adresa, os.telefon,'
      ''
      '      os.lekar,'
      '      os.faksimil_stomatolog,'
      '      os.faksimil_ginekolog,'
      ''
      '      os.osnov_na_osiguruvanje,'
      '      os.pravo_na_osiguruvanje,'
      '      os.vaznost_na_osiguruvanje,'
      ''
      '      os.z_fond, os.z_broj, os.z_clen,'
      '      os.bzl,'
      '      os.nemago'
      'from pzz_osigurenici os'
      'inner join ap7_recepti r on r.embg=os.embg'
      
        'where (os.vaznost_na_osiguruvanje<current_date or (os.vaznost_na' +
        '_osiguruvanje is null)) and coalesce(os.nemago,0)<5')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 384
    Top = 240
    object tblooEMBG: TFIBStringField
      FieldName = 'EMBG'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooDIST: TFIBStringField
      FieldName = 'DIST'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooEZBO: TFIBStringField
      FieldName = 'EZBO'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooPREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooT_IME: TFIBStringField
      FieldName = 'T_IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooIME: TFIBStringField
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooDATUM_RAGJANJE: TFIBDateField
      FieldName = 'DATUM_RAGJANJE'
    end
    object tblooADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooTELEFON: TFIBStringField
      FieldName = 'TELEFON'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooLEKAR: TFIBIntegerField
      FieldName = 'LEKAR'
    end
    object tblooFAKSIMIL_STOMATOLOG: TFIBIntegerField
      FieldName = 'FAKSIMIL_STOMATOLOG'
    end
    object tblooFAKSIMIL_GINEKOLOG: TFIBIntegerField
      FieldName = 'FAKSIMIL_GINEKOLOG'
    end
    object tblooOSNOV_NA_OSIGURUVANJE: TFIBStringField
      FieldName = 'OSNOV_NA_OSIGURUVANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooPRAVO_NA_OSIGURUVANJE: TFIBStringField
      FieldName = 'PRAVO_NA_OSIGURUVANJE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooVAZNOST_NA_OSIGURUVANJE: TFIBDateField
      FieldName = 'VAZNOST_NA_OSIGURUVANJE'
    end
    object tblooZ_FOND: TFIBSmallIntField
      FieldName = 'Z_FOND'
    end
    object tblooZ_BROJ: TFIBIntegerField
      FieldName = 'Z_BROJ'
    end
    object tblooZ_CLEN: TFIBSmallIntField
      FieldName = 'Z_CLEN'
    end
    object tblooBZL: TFIBStringField
      FieldName = 'BZL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblooNEMAGO: TFIBIntegerField
      FieldName = 'NEMAGO'
    end
  end
  object IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv23
    SSLOptions.SSLVersions = [sslvSSLv2, sslvSSLv3, sslvTLSv1]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 720
    Top = 104
  end
  object IdHTTP1: TIdHTTP
    IOHandler = IdSSLIOHandlerSocketOpenSSL1
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentEncoding = 'UTF-8'
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.ContentType = 'application/xml'
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 
      'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWe' +
      'bKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile' +
      ' Safari/<WebKit Rev>'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 720
    Top = 56
  end
  object tblKds: TpFIBDataSet
    Tag = 2
    RefreshSQL.Strings = (
      'select atrib,vred from AP7_KDS;')
    SelectSQL.Strings = (
      'select atrib,vred from AP7_KDS;')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 552
    Top = 112
    object tblKdsATRIB: TFIBStringField
      FieldName = 'ATRIB'
      Size = 202
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKdsVRED: TFIBStringField
      FieldName = 'VRED'
      Size = 8128
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblPubLager: TpFIBDataSet
    Tag = 2
    RefreshSQL.Strings = (
      'select'
      ' pl.re, pl.artvid, pl.artsif, pl.brpak, '
      ' a.naziv artnaz,a.gennazivid atc10,'
      
        ' pl.lager, pl.lagpak, pl.prodcena, pl.nabcena, pl.ddv, pl.ts_upd' +
        ', pl.pub,'
      ' pl.errorcode,pr.naziv prizvoditel,a.barkod'
      'from ws_publager pl'
      
        'inner join mtr_artikal a on a.artvid=pl.artvid and a.id=pl.artsi' +
        'f'
      'left join mat_proizvoditel pr on pr.id=a.prozvoditel'
      
        'where(  (pl.errorcode!='#39'00000'#39' or (pl.errorcode is null)) or :to' +
        'ptan=1'
      '     ) and (     PL.RE = :OLD_RE'
      '    and PL.ARTVID = :OLD_ARTVID'
      '    and PL.ARTSIF = :OLD_ARTSIF'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select'
      ' pl.re, pl.artvid, pl.artsif, pl.brpak, '
      ' a.naziv artnaz,a.gennazivid atc10,'
      
        ' pl.lager, pl.lagpak, pl.prodcena, pl.nabcena, pl.ddv, pl.ts_upd' +
        ', pl.pub,'
      ' pl.errorcode,pr.naziv prizvoditel,a.barkod'
      'from ws_publager pl'
      
        'inner join mtr_artikal a on a.artvid=pl.artvid and a.id=pl.artsi' +
        'f'
      'left join mat_proizvoditel pr on pr.id=a.prozvoditel'
      
        'where (pl.errorcode!='#39'00000'#39' or (pl.errorcode is null)) or :topt' +
        'an=1'
      '')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 192
    Top = 328
    object tblPubLagerRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblPubLagerARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblPubLagerARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object tblPubLagerBRPAK: TFIBIntegerField
      FieldName = 'BRPAK'
    end
    object tblPubLagerLAGER: TFIBIntegerField
      FieldName = 'LAGER'
    end
    object tblPubLagerLAGPAK: TFIBBCDField
      FieldName = 'LAGPAK'
      Size = 0
    end
    object tblPubLagerPRODCENA: TFIBBCDField
      FieldName = 'PRODCENA'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
    end
    object tblPubLagerNABCENA: TFIBBCDField
      FieldName = 'NABCENA'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
    end
    object tblPubLagerDDV: TFIBBCDField
      FieldName = 'DDV'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
    end
    object tblPubLagerTS_UPD: TFIBDateTimeField
      DefaultExpression = 'current_timestamp'
      FieldName = 'TS_UPD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblPubLagerPUB: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'PUB'
    end
    object tblPubLagerERRORCODE: TFIBStringField
      FieldName = 'ERRORCODE'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPubLagerARTNAZ: TFIBStringField
      FieldName = 'ARTNAZ'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPubLagerATC10: TFIBStringField
      FieldName = 'ATC10'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPubLagerPRIZVODITEL: TFIBStringField
      FieldName = 'PRIZVODITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPubLagerBARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object fbSkript: TpFIBScripter
    Database = dmKon.fibBaza
    Transaction = dmKon.tProc
    AutoDDL = False
    Left = 384
    Top = 331
  end
  object tblVremeE: TpFIBDataSet
    Tag = 2
    RefreshSQL.Strings = (
      'select first 1'
      '    rr.tsid,rr.vreme,rr.skoroe'
      'from ap7_rrlog rr'
      'where(  rr.skoroe>0'
      '     ) and (     RR.TSID = :OLD_TSID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select first 1'
      '    rr.tsid,rr.vreme,rr.skoroe'
      'from ap7_rrlog rr'
      'where rr.skoroe>0')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AutoCalcFields = False
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 552
    Top = 240
    object tblVremeETSID: TFIBDateTimeField
      FieldName = 'TSID'
    end
    object tblVremeEVREME: TFIBBCDField
      FieldName = 'VREME'
      Size = 0
    end
    object tblVremeESKOROE: TFIBFloatField
      FieldName = 'SKOROE'
    end
  end
  object pRrlogIns: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE AP7_RRLOG_INS (?VREME)')
    StoredProcName = 'AP7_RRLOG_INS'
    Left = 552
    Top = 328
    qoAutoCommit = True
  end
  object tbl: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MT2_CENOVNIK'
      'SET '
      '    OPIS = :OPIS,'
      '    POZITIV = :POZITIV,'
      '    VALUTA = :VALUTA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MT2_CENOVNIK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MT2_CENOVNIK('
      '    ID,'
      '    OPIS,'
      '    POZITIV,'
      '    VALUTA'
      ')'
      'VALUES('
      '    :ID,'
      '    :OPIS,'
      '    :POZITIV,'
      '    :VALUTA'
      ')')
    RefreshSQL.Strings = (
      'select  cn.id,cn.opis,cn.pozitiv,cn.valuta'
      'from mt2_cenovnik cn'
      ' WHERE '
      '        CN.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select  cn.id,cn.opis,cn.pozitiv,cn.valuta'
      'from mt2_cenovnik cn')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 424
    Top = 32
    object tblID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPOZITIV: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'POZITIV'
    end
    object tblVALUTA: TFIBStringField
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object ds: TDataSource
    DataSet = tbl
    Left = 454
    Top = 32
  end
  object tblS: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      
        '      ce.artvid,ce.artsif,a.naziv artnaz,p.naziv proizvod,a.brpa' +
        'k,ce.cena,ce.ddv,ce.cenanab,ce.marza,'
      
        '      ce.iznosmarza, ce.doplata , ce.PARTIC,a.generika,a.gennazi' +
        'vid,gn.polno_ime, g.naziv gen_naziv,'
      '      a.rezim_izdavanje,ce.vazeod'
      'from MT2_CENI_NA_DATUM(:id,:vreme) ce'
      'join mtr_artikal a on a.artvid=ce.artvid and a.id=ce.artsif'
      'left join pzz_genericki_nazivi gn on gn.gennazivid=a.gennazivid'
      'left join pzz_generika g on g.generid=gn.generid'
      'left join mat_proizvoditel p on p.id = a.prozvoditel')
    SelectSQL.Strings = (
      'select'
      
        '      ce.artvid,ce.artsif,a.naziv artnaz,p.naziv proizvod,a.brpa' +
        'k,ce.cena,ce.ddv,ce.cenanab,ce.marza,'
      
        '      ce.iznosmarza, ce.doplata , ce.PARTIC,a.generika,a.gennazi' +
        'vid,gn.polno_ime, g.naziv gen_naziv,'
      '      a.rezim_izdavanje,ce.vazeod'
      'from MT2_CENI_NA_DATUM(:id,:vreme) ce'
      'join mtr_artikal a on a.artvid=ce.artvid and a.id=ce.artsif'
      'left join pzz_genericki_nazivi gn on gn.gennazivid=a.gennazivid'
      'left join pzz_generika g on g.generid=gn.generid'
      'left join mat_proizvoditel p on p.id = a.prozvoditel')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = ds
    Left = 432
    Top = 168
    dcForceOpen = True
    object tblSARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblSARTSIF: TFIBIntegerField
      DisplayLabel = #1096#1080#1092#1088#1072
      FieldName = 'ARTSIF'
    end
    object tblSARTNAZ: TFIBStringField
      DisplayLabel = #1085#1072#1079#1080#1074
      FieldName = 'ARTNAZ'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSBRPAK: TFIBIntegerField
      DisplayLabel = #1073#1088'.'#1074#1086' '#1087#1072#1082
      FieldName = 'BRPAK'
    end
    object tblSCENA: TFIBBCDField
      DisplayLabel = #1087#1088#1086#1076#1072#1078#1085#1072' '#1094#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA'
      Size = 2
    end
    object tblSDDV: TFIBBCDField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DDV'
      Size = 2
    end
    object tblSCENANAB: TFIBBCDField
      DisplayLabel = #1085#1072#1073#1072#1074#1085#1072' '#1094#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENANAB'
      Size = 2
    end
    object tblSMARZA: TFIBBCDField
      DisplayLabel = #1084#1072#1088#1078#1072' %'
      FieldName = 'MARZA'
      Size = 2
    end
    object tblSIZNOSMARZA: TFIBBCDField
      DisplayLabel = #1080#1079#1085#1086#1089' '#1084#1072#1088#1078#1072
      FieldName = 'IZNOSMARZA'
      Size = 2
    end
    object tblSDOPLATA: TFIBBCDField
      DisplayLabel = #1076#1086#1087#1083#1072#1090#1072
      FieldName = 'DOPLATA'
      Size = 2
    end
    object tblSPROIZVOD: TFIBStringField
      DisplayLabel = #1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVOD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSPARTIC: TFIBBCDField
      FieldName = 'PARTIC'
      Size = 2
    end
    object tblSREZIM_IZDAVANJE: TFIBStringField
      DisplayLabel = #1048#1079#1076#1072#1074'.'
      FieldName = 'REZIM_IZDAVANJE'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSGENERIKA: TFIBStringField
      FieldName = 'GENERIKA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSGENNAZIVID: TFIBStringField
      DisplayLabel = 'ATC'
      FieldName = 'GENNAZIVID'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSPOLNO_IME: TFIBStringField
      FieldName = 'POLNO_IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSGEN_NAZIV: TFIBStringField
      DisplayLabel = #1043#1077#1085#1077#1088#1080#1082#1072
      FieldName = 'GEN_NAZIV'
      Size = 60
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSVAZEOD: TFIBDateTimeField
      DisplayLabel = #1042#1072#1078#1080' '#1086#1076
      FieldName = 'VAZEOD'
    end
  end
  object dsS: TDataSource
    DataSet = tblS
    Left = 478
    Top = 168
  end
  object tblZastit: TpFIBDataSet
    RefreshSQL.Strings = (
      'select '
      
        '   cena_id, next_id, cenovnik, artvid, artikl, cena, danok, akci' +
        'za, marza, cenanab, ts_od, ts_do, iznosmarza, participacija, dop' +
        'lata, recver'
      'from mt2_ceni'
      'where(  cenovnik=:cenovnik'
      
        '--values('#39'1000000162'#39',null,'#39'1'#39','#39'1'#39','#39'18953'#39','#39'92.9985'#39','#39'5'#39','#39'0'#39','#39'0'#39 +
        ','#39'92.9985'#39','#39'2011-08-01 00:00:00'#39',null,'#39'12.00'#39','#39'15.00'#39','#39'0.00'#39','#39'20' +
        '12-01-30 13:18:38'#39' )'
      '     ) and (     MT2_CENI.CENA_ID = :OLD_CENA_ID'
      '     )'
      '    ; ')
    SelectSQL.Strings = (
      'select '
      
        '   cena_id, next_id, cenovnik, artvid, artikl, cena, danok, akci' +
        'za, marza, cenanab, ts_od, ts_do, iznosmarza, participacija, dop' +
        'lata, recver'
      'from mt2_ceni'
      'where cenovnik=:cenovnik')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = ds
    Left = 656
    Top = 216
    dcForceOpen = True
  end
  object qZap: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete from mt2_ceni where cenovnik=:cenovnik and artvid=1;')
    Left = 656
    Top = 152
  end
  object tblPrezCen: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MT2_PREZ_CEN'
      'SET '
      '    TS_INS = :TS_INS'
      'WHERE'
      '    CENOVNIK = :OLD_CENOVNIK'
      '    and DATUM = :OLD_DATUM'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MT2_PREZ_CEN'
      'WHERE'
      '        CENOVNIK = :OLD_CENOVNIK'
      '    and DATUM = :OLD_DATUM'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MT2_PREZ_CEN('
      '    CENOVNIK,'
      '    DATUM,'
      '    TS_INS'
      ')'
      'VALUES('
      '    :CENOVNIK,'
      '    :DATUM,'
      '    :TS_INS'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    CENOVNIK,'
      '    DATUM,'
      '    TS_INS'
      'FROM'
      '    MT2_PREZ_CEN '
      ''
      ' WHERE '
      '        MT2_PREZ_CEN.CENOVNIK = :OLD_CENOVNIK'
      '    and MT2_PREZ_CEN.DATUM = :OLD_DATUM'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    CENOVNIK,'
      '    DATUM,'
      '    TS_INS'
      'FROM'
      '    MT2_PREZ_CEN '
      'order by datum desc')
    AutoUpdateOptions.UpdateTableName = 'MTR_IO'
    AutoUpdateOptions.KeyFields = 'RE;SYS_TIP_DOKUMENT;BROJ'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 553
    Top = 37
    object tblPrezCenDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblPrezCenCENOVNIK: TFIBIntegerField
      FieldName = 'CENOVNIK'
    end
    object tblPrezCenTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
  end
  object dsPrezCen: TDataSource
    DataSet = tblPrezCen
    Left = 591
    Top = 37
  end
  object tblIspratnici: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select IZLEZID, DOKGOD, DOKRE, DOKTIP, DOKBROJ, PARTIP, PARSIF, ' +
        'DATUMIZLEZ, TS_UPD, TS_INS, APTID_DO, ARTVID, ARTSIF,'
      '       BARKOD, KOL, BRPAK, POPUST, DDV, VCENA, ROK'
      'from WS_CS_INS_ISP CIS'
      
        '/*$$IBEC$$ where CIS.TS_UPD > (select COALESCE(max(L.TS_INS),'#39'01' +
        '.01.2018 00:00:00'#39')'
      '                    from WS_CS_LOG L)   '
      ' $$IBEC$$*/  ')
    SelectSQL.Strings = (
      
        'select IZLEZID, DOKGOD, DOKRE, DOKTIP, DOKBROJ, PARTIP, PARSIF, ' +
        'DATUMIZLEZ, TS_UPD, TS_INS, APTID_DO, ARTVID, ARTSIF,'
      '       BARKOD, KOL, BRPAK, POPUST, DDV, VCENA, rok'
      'from WS_CS_INS_ISP CIS'
      
        '/*$$IBEC$$ where CIS.TS_UPD > (select COALESCE(max(L.TS_INS),'#39'01' +
        '.01.2018 00:00:00'#39')'
      '                    from WS_CS_LOG L)   '
      ' $$IBEC$$*/  ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 680
    Top = 328
    object tblIspratniciIZLEZID: TFIBBCDField
      FieldName = 'IZLEZID'
      Size = 0
    end
    object tblIspratniciDOKGOD: TFIBSmallIntField
      FieldName = 'DOKGOD'
    end
    object tblIspratniciDOKRE: TFIBSmallIntField
      FieldName = 'DOKRE'
    end
    object tblIspratniciDOKTIP: TFIBSmallIntField
      FieldName = 'DOKTIP'
    end
    object tblIspratniciDOKBROJ: TFIBIntegerField
      FieldName = 'DOKBROJ'
    end
    object tblIspratniciPARTIP: TFIBIntegerField
      FieldName = 'PARTIP'
    end
    object tblIspratniciPARSIF: TFIBIntegerField
      FieldName = 'PARSIF'
    end
    object tblIspratniciDATUMIZLEZ: TFIBDateField
      FieldName = 'DATUMIZLEZ'
    end
    object tblIspratniciTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblIspratniciTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblIspratniciAPTID_DO: TFIBBCDField
      FieldName = 'APTID_DO'
      Size = 0
    end
    object tblIspratniciARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblIspratniciARTSIF: TFIBIntegerField
      FieldName = 'ARTSIF'
    end
    object tblIspratniciBARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIspratniciKOL: TFIBBCDField
      FieldName = 'KOL'
      Size = 0
    end
    object tblIspratniciBRPAK: TFIBIntegerField
      FieldName = 'BRPAK'
    end
    object tblIspratniciPOPUST: TFIBFloatField
      FieldName = 'POPUST'
    end
    object tblIspratniciDDV: TFIBBCDField
      FieldName = 'DDV'
      Size = 2
    end
    object tblIspratniciVCENA: TFIBBCDField
      FieldName = 'VCENA'
      Size = 2
    end
    object tblIspratniciROK: TFIBDateField
      FieldName = 'ROK'
    end
  end
  object dsIspratnici: TDataSource
    DataSet = tblIspratnici
    Left = 736
    Top = 328
  end
  object qInsert: TpFIBQuery
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO WS_CS_LOG ('
      '    TS_INS_ISP,'
      '    IZLEZID,'
      '    KORISNIKID'
      ') VALUES ('
      '    :TS_INS_ISP,'
      '    :IZLEZID,'
      '    :KORISNIKID'
      ')')
    Left = 808
    Top = 327
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateIsp: TpFIBQuery
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      '    update MT2_IZLEZG G'
      '    set G.INT_ISP = 1'
      '    where G.IZLEZID = :IZLEZID;')
    Left = 856
    Top = 327
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pPubLagerAll: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE WSP_PUBLAGER_ALL ')
    StoredProcName = 'WSP_PUBLAGER_ALL'
    Left = 560
    Top = 448
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pOnaduj: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE AP7_RECEPT_REGENERATOR (:godina,:mesec,:onaduj' +
        ')')
    StoredProcName = 'AP7_RECEPT_REGENERATOR'
    Left = 340
    Top = 106
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
