object FrmCenovnik: TFrmCenovnik
  Left = 0
  Top = 0
  Caption = #1062#1077#1085#1086#1074#1085#1080#1082' ('#1055#1072#1088#1072#1084#1077#1090#1088#1080' '#1079#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072')'
  ClientHeight = 482
  ClientWidth = 614
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 463
    Width = 614
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', ESC - '#1048#1079#1083#1077#1079
  end
  object panelGore: TPanel
    Left = 0
    Top = 0
    Width = 614
    Height = 153
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 1
    object lblMesec: TLabel
      Left = 15
      Top = 50
      Width = 40
      Height = 14
      Caption = #1052#1077#1089#1077#1094
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblGodina: TLabel
      Left = 15
      Top = 25
      Width = 46
      Height = 14
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblMesecLastValue: TLabel
      Left = 342
      Top = 49
      Width = 53
      Height = 13
      Caption = '%'#1052#1077#1089#1077#1094'%'
    end
    object lblMesecLast: TLabel
      Left = 278
      Top = 49
      Width = 36
      Height = 14
      Caption = #1052#1077#1089#1077#1094
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblGodinaLastValue: TLabel
      Left = 342
      Top = 24
      Width = 59
      Height = 13
      Caption = '%'#1043#1086#1076#1080#1085#1072'%'
    end
    object lblGodinaLast: TLabel
      Left = 278
      Top = 24
      Width = 40
      Height = 14
      Caption = #1043#1086#1076#1080#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clActiveCaption
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxComboBoxMesec: TcxComboBox
      Left = 97
      Top = 48
      BeepOnEnter = False
      Properties.DropDownListStyle = lsEditFixedList
      Properties.ImmediatePost = True
      Properties.Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
      TabOrder = 1
      OnEnter = onEnterAll
      OnExit = onExitAll
      OnKeyDown = onKeyDownLookUpAll
      Width = 68
    end
    object cxComboBoxGodini: TcxComboBox
      Left = 97
      Top = 21
      BeepOnEnter = False
      TabOrder = 0
      OnEnter = onEnterAll
      OnExit = onExitAll
      OnKeyDown = onKeyDownLookUpAll
      Width = 68
    end
    object cxButton1: TcxButton
      Left = 272
      Top = 106
      Width = 201
      Height = 31
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112'/'#1087#1088#1080#1082#1072#1078#1080' '#1087#1072#1088#1072#1084#1077#1090#1088#1080
      LookAndFeel.SkinName = 'Black'
      TabOrder = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = cxButton1Click
    end
  end
  object panelDole: TPanel
    Left = 0
    Top = 153
    Width = 614
    Height = 310
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 2
    object panelGrid: TPanel
      Left = 2
      Top = 2
      Width = 610
      Height = 191
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object cxGrid1: TcxGrid
        Left = 2
        Top = 2
        Width = 606
        Height = 187
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = DM1.DSKomCenovnik
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1091#1089#1083'.'
            DataBinding.FieldName = 'VID_USLUGA'
            HeaderAlignmentHorz = taCenter
            Width = 59
          end
          object cxGrid1DBTableView1USLUGA: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1091#1089#1083'.'
            DataBinding.FieldName = 'USLUGA'
            HeaderAlignmentHorz = taCenter
            Width = 60
          end
          object cxGrid1DBTableView1Column1: TcxGridDBColumn
            Caption = #1053#1072#1079#1080#1074' '#1091#1089#1083#1091#1075#1072
            DataBinding.FieldName = 'NAZIV'
            HeaderAlignmentHorz = taCenter
            Width = 154
          end
          object cxGrid1DBTableView1GODINA: TcxGridDBColumn
            Caption = #1043#1086#1076#1080#1085#1072
            DataBinding.FieldName = 'GODINA'
            HeaderAlignmentHorz = taCenter
            Width = 60
          end
          object cxGrid1DBTableView1MESEC: TcxGridDBColumn
            Caption = #1052#1077#1089#1077#1094
            DataBinding.FieldName = 'MESEC'
            HeaderAlignmentHorz = taCenter
            Width = 59
          end
          object cxGrid1DBTableView1CENA: TcxGridDBColumn
            Caption = #1062#1077#1085#1072
            DataBinding.FieldName = 'CENA'
            HeaderAlignmentHorz = taCenter
            Width = 60
          end
          object cxGrid1DBTableView1DANOK: TcxGridDBColumn
            Caption = #1044#1072#1085#1086#1082
            DataBinding.FieldName = 'DANOK'
            HeaderAlignmentHorz = taCenter
            Width = 42
          end
          object cxGrid1DBTableView1NACIN_PRESMETKA: TcxGridDBColumn
            DataBinding.FieldName = 'NACIN_PRESMETKA'
            Visible = False
          end
          object cxGrid1DBTableView1PAUSAL: TcxGridDBColumn
            Caption = #1055#1072#1091#1096#1072#1083
            DataBinding.FieldName = 'PAUSAL'
            HeaderAlignmentHorz = taCenter
            HeaderGlyphAlignmentHorz = taCenter
            Width = 77
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object panelControls: TPanel
      Left = 2
      Top = 193
      Width = 610
      Height = 115
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 1
      object lblCena: TLabel
        Left = 14
        Top = 17
        Width = 28
        Height = 14
        Caption = #1062#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblDanok: TLabel
        Left = 14
        Top = 66
        Width = 34
        Height = 14
        Caption = #1044#1072#1085#1086#1082
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblNacinPresmetka: TLabel
        Left = 262
        Top = 20
        Width = 116
        Height = 14
        Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clActiveCaption
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPausal: TLabel
        Left = 13
        Top = 41
        Width = 56
        Height = 19
        AutoSize = False
        Caption = #1055#1072#1091#1096#1072#1083
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clActiveCaption
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxControlCENA: TcxDBTextEdit
        Left = 75
        Top = 10
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = DM1.DSKomCenovnik
        TabOrder = 0
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 121
      end
      object cxControlDANOK: TcxDBTextEdit
        Left = 75
        Top = 64
        BeepOnEnter = False
        DataBinding.DataField = 'DANOK'
        DataBinding.DataSource = DM1.DSKomCenovnik
        TabOrder = 3
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownAll
        Width = 121
      end
      object cxLookUpNACIN_PRESMETKA: TcxDBLookupComboBox
        Left = 385
        Top = 16
        DataBinding.DataField = 'NACIN_PRESMETKA'
        DataBinding.DataSource = DM1.DSKomCenovnik
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'OPIS'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = DM1.DSKomNacinPresmetka
        TabOrder = 1
        OnKeyDown = onKeyDownAll
        Width = 197
      end
      object cxControlPAUSAL: TcxDBTextEdit
        Left = 75
        Top = 37
        DataBinding.DataField = 'PAUSAL'
        DataBinding.DataSource = DM1.DSKomCenovnik
        TabOrder = 2
        OnKeyDown = onKeyDownAll
        Width = 121
      end
    end
  end
  object ActionList1: TActionList
    Left = 520
    Top = 40
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aPromeni: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ShortCut = 117
      OnExecute = aPromeniExecute
    end
  end
end
