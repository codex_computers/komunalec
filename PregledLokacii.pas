unit PregledLokacii;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery,
  cxGridCustomPopupMenu, cxGridPopupMenu, ActnList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ExtCtrls, ComCtrls, dxRibbon, cxGridExportLink,
  dxStatusBar, dxRibbonStatusBar, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSCore, dxPScxCommon,  dxPgsDlg, dxPrnDlg, dxPSGraphicLnk,
  dxPSTCLnk, dxBar, cxLookAndFeels, cxLookAndFeelPainters, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxSkinsdxBarPainter, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, //dxSkinOffice2013White,
  cxNavigator,//System.Actions,
  cxDBLookupComboBox, Vcl.StdCtrls, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxRibbonCustomizationForm, System.Actions, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TfrmPregledLokacii = class(TForm)
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aZapisi: TAction;
    aIzlez: TAction;
    aPecati: TAction;
    aSnimiVoExcel: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    Panel1: TPanel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxComponentPrinter1: TdxComponentPrinter;
    dxPrintDialog1: TdxPrintDialog;
    dxPrintStyleManager1: TdxPrintStyleManager;
    dxPrintStyleManager1Style1: TdxPSPrintStyle;
    dxComponentPrinter1Link1: TdxGridReportLink;
    aMaticenBroj: TAction;
    aSerBrojVodomer: TAction;
    pcPregledLokacii: TPageControl;
    tsLokaciiVodomeri: TTabSheet;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    tsLokacii: TTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn18: TcxGridDBColumn;
    cxGridDBColumn19: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    cxGridDBColumn22: TcxGridDBColumn;
    cxGridDBColumn23: TcxGridDBColumn;
    cxGridDBColumn24: TcxGridDBColumn;
    cxGridDBColumn25: TcxGridDBColumn;
    cxGridDBColumn26: TcxGridDBColumn;
    cxGridDBColumn27: TcxGridDBColumn;
    cxGridDBColumn28: TcxGridDBColumn;
    cxGridDBColumn29: TcxGridDBColumn;
    cxGridDBColumn30: TcxGridDBColumn;
    cxGridDBColumn31: TcxGridDBColumn;
    cxGridDBColumn32: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView1REON_ID: TcxGridDBColumn;
    cxGridDBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGridDBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn;
    cxGridDBTableView1RB2: TcxGridDBColumn;
    cxGridDBTableView1LOKACIJA: TcxGridDBColumn;
    cxGridDBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridDBTableView1PARTNER_ID: TcxGridDBColumn;
    cxGridDBTableView1NAZIV: TcxGridDBColumn;
    cxGridDBTableView1MATICEN: TcxGridDBColumn;
    cxGridDBTableView1ADRESA: TcxGridDBColumn;
    cxGridDBTableView1VODOMER: TcxGridDBColumn;
    cxGridDBTableView1AKTIVEN: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_TIP_PARTNET: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_MESTO: TcxGridDBColumn;
    dxBarManager1: TdxBarManager;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    aSnimiIzgled: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxPrintStyleManager1Style2: TdxPSPrintStyle;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxGridDBTableView1ID_MERNA_TOCKA: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_MERNA_TOCKA: TcxGridDBColumn;
    aMernaTocka: TAction;
    qMernaTocka: TpFIBQuery;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    aBrisiMT: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    Label5: TLabel;
    cxGridDBTableView1SERISKI_BROJ: TcxGridDBColumn;
    aPratiPoMailGrupno: TAction;
    dsFakturi: TDataSource;
    tblFakruri: TpFIBDataSet;
    tblFakruriREON_ID: TFIBIntegerField;
    tblFakruriCITACKA_KNIGA: TFIBIntegerField;
    tblFakruriCITACKA_KNIGA_RB: TFIBIntegerField;
    tblFakruriRB2: TFIBFloatField;
    tblFakruriLOKACIIN_ID: TFIBIntegerField;
    tblFakruriGODINA: TFIBIntegerField;
    tblFakruriMESEC: TFIBIntegerField;
    tblFakruriTIP_PARTNER: TFIBIntegerField;
    tblFakruriPARTNER_ID: TFIBIntegerField;
    tblFakruriNAZIV: TFIBStringField;
    tblFakruriADRESA: TFIBStringField;
    tblFakruriMAIL: TFIBStringField;
    tblFakruriIZNOS_VKUPNO: TFIBBCDField;
    tblFakruriNAZIV1: TFIBStringField;
    tblFakruriIZNOS_USLUGA_NETO: TFIBBCDField;
    tblFakruriDANOK: TFIBBCDField;
    tblFakruriIZNOS_USLUGA: TFIBBCDField;
    tblFakruriVRABOTEN: TFIBStringField;
    tblFakruriDATUM_PRESMETKA: TFIBDateTimeField;
    tblFakruriBROJ_FAKTURA: TFIBStringField;
    tblFakruriSTANBENA_POVRSINA: TFIBIntegerField;
    tblFakruriDVORNA_POVRSINA: TFIBIntegerField;
    tblFakruriOSNOVICA_DANOK_05: TFIBBCDField;
    tblFakruriOSNOVICA_DANOK_18: TFIBBCDField;
    tblFakruriDANOK_05: TFIBBCDField;
    tblFakruriDANOK_18: TFIBBCDField;
    tblFakruriDA_SE_PLATI: TFIBDateTimeField;
    tblFakruriID: TFIBIntegerField;
    tblFakruriDDO: TFIBDateField;
    tblFakruriVK_POTROSENO: TFIBBCDField;
    procedure FormCreate(Sender: TObject);
    procedure aSnimiVoExcelExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aSerBrojVodomerExecute(Sender: TObject);
    procedure aMaticenBrojExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    constructor Create(Owner : TComponent; tip_partner : string);reintroduce; overload;
    procedure pcPregledLokaciiChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
   //procedure aMernaToclkaExecute(Sender: TObject);
    procedure aMernaTockaExecute(Sender: TObject);
    procedure aBrisiMTExecute(Sender: TObject);
  private
    { Private declarations }
  public
    tip_partner:string;
    { Public declarations }
  end;

var
  frmPregledLokacii: TfrmPregledLokacii;

implementation

uses dmKonekcija, dmMaticni, UnitDM1, UnitMaticenBroj, Utils, UnitMernaTocka,
  DaNe, dmResources;

{$R *.dfm}

procedure TfrmPregledLokacii.aBrisiMTExecute(Sender: TObject);
var i, flag, br:Integer;
    status: TStatusWindowHandle;
    datum, merna_tocka:string;
begin
  frmDaNe := TfrmDaNe.Create(self, '�������', '���� �� �� ������� ������� �����?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
          Abort
       else
   begin


      flag:=-1;
      br:=0;
      status := cxCreateStatusWindow();
      try
            with (cxGridDBTableView1.DataController) do
            for I := 0 to cxGridDBTableView1.Controller.SelectedRecordCount - 1 do
              begin
                flag:=cxGridDBTableView1.Controller.SelectedRecords[i].RecordIndex;
               // if (GetValue(flag,cxGridDBTableView1IZNOS.Index)>0) then
                //   begin
                     qMernaTocka.Close;
                     qMernaTocka.ParamByName('lokacija').Value := GetValue(flag,cxGridDBTableView1LOKACIJA.Index);
                     qMernaTocka.ParamByName('id_merna_tocka').Clear;
                     qMernaTocka.ExecQuery;

                     DM1.tblPregledLokaciiVodomeri.FullRefresh;
               end;
      finally
        cxRemoveStatusWindow(status);

        end;
   end;

end;

procedure TfrmPregledLokacii.aIzlezExecute(Sender: TObject);
begin
   Close;
end;

procedure TfrmPregledLokacii.aMaticenBrojExecute(Sender: TObject);
begin
if pcPregledLokacii.ActivePage=tsLokaciiVodomeri then
begin
     frmMaticenBroj:=TfrmMaticenBroj.Create(Application);
     frmMaticenBroj.Tag:=0;
     frmMaticenBroj.lblKorisnik.Caption:=DM1.tblPregledLokaciiVodomeriNAZIV.Value;
     frmMaticenBroj.lblTip.Caption:='�.�. '+DM1.tblPregledLokaciiVodomeriTIP_PARTNER.AsString;
     frmMaticenBroj.lblId.Caption:='�. '+DM1.tblPregledLokaciiVodomeriPARTNER_ID.AsString;
     frmMaticenBroj.txtMaticen.Text:=DM1.tblPregledLokaciiVodomeriMATICEN.AsString;
     frmMaticenBroj.lblLok.Caption:='(���. ' +DM1.tblPregledLokaciiVodomeriLOKACIJA.AsString+' )';
     frmMaticenBroj.ShowModal;
     if frmMaticenBroj.tag=3 then //napravena e izmena
     begin
        cxGridDBTableView1.DataController.DataSet.Close;
        cxGridDBTableView1.DataController.DataSet.Open;
     end;
     frmMaticenBroj.Free;
end;
end;

procedure TfrmPregledLokacii.aMernaTockaExecute(Sender: TObject);
// �������� �� �������� �� ����� ����� �� �������
var i, flag, br:Integer;
    status: TStatusWindowHandle;
    datum, merna_tocka:string;
begin
flag:=-1;
br:=0;
status := cxCreateStatusWindow();
try
      frmVnesiMernaTocka := TfrmVnesiMernaTocka.Create(self);
      frmVnesiMernaTocka.ShowModal;
      merna_tocka := frmVnesiMernaTocka.txtMernaTocka.Text;
      frmVnesiMernaTocka.Free;

     if merna_tocka <> '' then
     begin
      with (cxGridDBTableView1.DataController) do
      for I := 0 to cxGridDBTableView1.Controller.SelectedRecordCount - 1 do
        begin
          flag:=cxGridDBTableView1.Controller.SelectedRecords[i].RecordIndex;
         // if (GetValue(flag,cxGridDBTableView1IZNOS.Index)>0) then
          //   begin
               qMernaTocka.Close;
               qMernaTocka.ParamByName('lokacija').Value := GetValue(flag,cxGridDBTableView1LOKACIJA.Index);
               qMernaTocka.ParamByName('id_merna_tocka').AsString:=merna_tocka;
               qMernaTocka.ExecQuery;

               DM1.tblPregledLokaciiVodomeri.FullRefresh;
         end;
        end;

finally
 	cxRemoveStatusWindow(status);
end;

end;

procedure TfrmPregledLokacii.aPecatiExecute(Sender: TObject);
begin
if pcPregledLokacii.ActivePage=tsLokaciiVodomeri then
begin
     dxComponentPrinter1.CurrentLink:=dxComponentPrinter1Link1;
     dxComponentPrinter1.CurrentLink.Component:=cxGrid1;
     dxComponentPrinter1.Preview(true,nil);
end
else
begin
     dxComponentPrinter1.CurrentLink:=dxComponentPrinter1Link2;
     dxComponentPrinter1.CurrentLink.Component:=cxGrid2;
     dxComponentPrinter1.Preview(true,nil);
end;
end;

procedure TfrmPregledLokacii.aSerBrojVodomerExecute(Sender: TObject);
begin
if pcPregledLokacii.ActivePage=tsLokaciiVodomeri then
 if DM1.tblPregledLokaciiVodomeriVODOMER.AsString<>'����' then
 begin
     frmMaticenBroj:=TfrmMaticenBroj.Create(Application);
     frmMaticenBroj.Tag:=1;
     frmMaticenBroj.lblKorisnik.Caption:=DM1.tblPregledLokaciiVodomeriNAZIV.Value;
     frmMaticenBroj.lblTip.Caption:='�.�. '+DM1.tblPregledLokaciiVodomeriTIP_PARTNER.AsString;
     frmMaticenBroj.lblId.Caption:='�. '+DM1.tblPregledLokaciiVodomeriPARTNER_ID.AsString;
     frmMaticenBroj.txtMaticen.Text:=DM1.tblPregledLokaciiVodomeriSERISKI_BROJ.AsString;
     frmMaticenBroj.lblLok.Caption:='(���. ' +DM1.tblPregledLokaciiVodomeriLOKACIJA.AsString+' )';
     frmMaticenBroj.Caption:='������� ��� �� �������';
     frmMaticenBroj.ShowModal;
     if frmMaticenBroj.tag=3 then //napravena e izmena
     begin
        cxGridDBTableView1.DataController.DataSet.Close;
        cxGridDBTableView1.DataController.DataSet.Open;
     end;

     frmMaticenBroj.Free;
 end
 else
 begin
   ShowMessage('��������� ������� ���� �������!');
   Abort;
 end;
end;

procedure TfrmPregledLokacii.aSnimiIzgledExecute(Sender: TObject);
begin
 if pcPregledLokacii.ActivePage=tsLokaciiVodomeri then
   cxGridDBTableView1.StoreToIniFile(Name,True,[])
 else
   cxGridDBTableView2.StoreToIniFile(Name,True,[])
end;

procedure TfrmPregledLokacii.aSnimiVoExcelExecute(Sender: TObject);
begin
if pcPregledLokacii.ActivePage=tsLokaciiVodomeri then
begin
   zacuvajVoExcel(cxGrid1, Caption + ' �� �������� -');
//    dmMat.SaveToExcelDialog.FileName := Caption + ' �� �������� -' + dmKon.user;
//    if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid1,true, true, false)
end
else
begin
   zacuvajVoExcel(cxGrid1, Caption + '-');
//    dmMat.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
//    if(dmMat.SaveToExcelDialog.Execute)then
//        ExportGridToExcel(dmMat.SaveToExcelDialog.FileName,cxGrid2,true, true, false);
end;

end;

procedure TfrmPregledLokacii.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm1.tblPregledLokacii.Close;
end;

procedure TfrmPregledLokacii.FormCreate(Sender: TObject);
begin
   dm1.tblPregledLokaciiVodomeri.Close;
   dm1.tblPregledLokaciiVodomeri.ParamByName('tp').AsString:=tip_partner;
   DM1.tblPregledLokaciiVodomeri.Open;
//   DM1.tblPregledLokacii.Close;
 //  dm1.tblPregledLokacii.ParamByName('tp').AsString:=tip_partner;
 //  DM1.tblPregledLokacii.Open;
end;

procedure TfrmPregledLokacii.FormShow(Sender: TObject);
begin
    cxGridDBTableView1.RestoreFromIniFile(Name,false,false,[]);
    cxGridDBTableView2.RestoreFromIniFile(Name,false,false,[]);
    ActiveControl:=cxGrid1;
end;

procedure TfrmPregledLokacii.pcPregledLokaciiChange(Sender: TObject);
begin
      if pcPregledLokacii.ActivePage=tsLokacii then
      begin
        dm1.tblPregledLokacii.ParamByName('tp').AsString:=tip_partner;
        DM1.tblPregledLokacii.Open;
      end;

end;

constructor TfrmPregledLokacii.Create(Owner : TComponent; tip_partner: string);
begin
   inherited Create(Owner);
    Self.tip_partner := tip_partner;
end;

end.
