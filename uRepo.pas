unit uRepo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SHELLAPI, DB, FIBDataSet, pFIBDataSet;

type
  TfrmRepo = class(TForm)
  constructor Create(Owner : TComponent); overload;
    procedure FormDestroy(Sender: TObject);
    function Init(cn:TStrings):boolean;
    function Init1(cn:TStrings;rbd:String):boolean;
    Procedure Spremaj(repid:integer);
    procedure Spremaj2(tg:string;Broj:integer);
    procedure Param(ime:string;val:String);
    procedure SisParam(ime:String;val:String);
    procedure ShowReport;
    procedure ShowReporter;
    procedure ShowParamEdit;
    procedure ShowDesign;
  private
    sispar:TStringList;
    par:TStringList;
    { Private declarations }
  public
    NurciOdPateka, RepciOdPateka : boolean;
    rabdir:String;
    h:THandle;
    tt,rexe:String;
   proc_info: TProcessInformation;
   startinfo: TStartupInfo;
    { Public declarations }
  end;

var
  frmRepo: TfrmRepo;

implementation

uses dmKonekcija, Pateka, DaNe, Firma, dmMaticni,  Math, UnitMain;

{$R *.dfm}

constructor TfrmRepo.Create(Owner : TComponent);
begin
inherited Create(Owner);
    sispar := TStringList.Create;
    par := TStringList.Create;
    Init1(dmKon.fibPateka.DBParams,'c:\\cxCache\\');
end;

procedure TfrmRepo.FormDestroy(Sender: TObject);
begin
   sispar.Clear;
   Par.Clear;
end;

function TfrmRepo.Init(cn:TStrings):boolean;
begin
   Init1(cn,'c:\\cxCache\\');
end;

function TfrmRepo.Init1(cn:TStrings;rbd:String):boolean;
begin
   rabdir:=rbd;
   sispar.Clear;
   sispar.Add('Application_ID='+ IntToStr(Application.Handle));
   sispar.Add('RabotenDir='+rabdir);
   sispar.Add('User_Name='+dmKon.user );
   sispar.Add('password=' +dmKon.password);
end;

Procedure TfrmRepo.Spremaj(repid:Integer);
begin
   par.Add('report_id='+IntToStr(repid));
end;

procedure TfrmRepo.Spremaj2(tg:string;Broj:integer);
begin
   par.Add('rep_tabela_grupa='+tg);
   par.Add('rep_broj='+inttostr(Broj));
end;

procedure TfrmRepo.Param(ime:string;val:String);
begin
   par.Add(ime+'='+val);
end;

procedure TfrmRepo.SisParam(ime:String;val:String);
begin
   sispar.Values[ime]:=val;
end;
//---------------------------------------------------------------------------
procedure TfrmRepo.ShowReport;
var i:Integer;
   ExitCode: longword;
   pom:string;
begin
   tt:='';
   rexe:=ExtractFilePath(Application.ExeName)+'Reporter.exe';

   tt:=tt+'firma='+dmKon.viewFirmiID.AsString+';';

   SisParam('Database',dmKon.pateka );
   SisParam('BazaID',dmKon.tblBaziID.AsString);

   for i:=0 to sispar.Count-1 do
       tt:=tt+sispar.Strings[i]+';';

   for i:=0 to par.Count-1 do
      tt:=tt+par.Strings[i]+';';
      pom:=rexe+' '+tt;
//   Application.Minimize;
   if CreateProcess(PChar(rexe),PChar(pom), nil, nil, false, NORMAL_PRIORITY_CLASS, nil, nil, startinfo, proc_info) then
       begin
          //se ceka duri da zavrsi reporterot
          WaitForSingleObject(proc_info.hProcess, INFINITE);
       end;
   Application.Restore;
   Application.BringToFront;
//      ShellExecute(0, 'open', PChar(rexe), PChar(tt), nil, SW_SHOW);
end;
//---------------------------------------------------------------------------
procedure TfrmRepo.ShowReporter;
begin
	ShowReport;
  //PrintPreparedReport('',1,true,TfrPrintPages(frAll));

end;
//---------------------------------------------------------------------------
procedure TfrmRepo.ShowParamEdit;
begin
	Param('editparam','true');
   ShowReport();
end;
//---------------------------------------------------------------------------
procedure TfrmRepo.ShowDesign;
var tt,rexe:String; i:Integer;
pom:String;
begin
   tt:='';
   rexe:=ExtractFilePath(Application.ExeName)+'Reporter.exe';

   tt:=tt+'firma='+dmKon.viewFirmiID.AsString+';';

   SisParam('design','true');
   SisParam('DataBase',dmKon.pateka );
   SisParam('BazaID',dmKon.tblBaziID.AsString);


   for i:=0 to sispar.Count do
       tt:=tt+sispar.Strings[i]+';';

   for i:=0 to par.Count do
      tt:=tt+par.Strings[i]+';';
      pom:=rexe+''+tt;
    // Application.Minimize;
        if CreateProcess(PChar(rexe),PChar(pom), nil, nil, false, NORMAL_PRIORITY_CLASS, nil, nil, startinfo, proc_info) then
       begin
          //se ceka duri da zavrsi reporterot
          WaitForSingleObject(proc_info.hProcess, INFINITE);
       end;

   // ShellExecute(Handle, 'open', PChar(rexe), PChar(tt), nil, SW_SHOWNORMAL);
    Application.Restore;
    Application.BringToFront;
end;


end.
