unit UnitVidoviUslugi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxLookAndFeels, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxBar, dxPSCore,
  dxPScxCommon,  cxBarEditItem, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip;

type
  TFrmVidoviUslugi = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    lblNaziv: TLabel;
    cxControlNAZIV: TcxDBTextEdit;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure callFrmUslugi();
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmVidoviUslugi: TFrmVidoviUslugi;

implementation

uses UnitDM1, UnitUslugi;

{$R *.dfm}

procedure TFrmVidoviUslugi.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
    callFrmUslugi();
end;

procedure TFrmVidoviUslugi.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
case key of
  VK_RETURN:
  begin
      callFrmUslugi();
  end;
end;
end;

procedure TFrmVidoviUslugi.callFrmUslugi();
var frm:TFrmUslugi;
begin
    DM1.openTblUslugiPoVidUsluga(DM1.TblKomVidoviUslugiID.AsString);

    frm := TFrmUslugi.Create(nil,true);
    frm.setDefaultVidUsluga(DM1.TblKomVidoviUslugiID.AsString);
    frm.ShowModal();
    frm.Free();
end;

end.
