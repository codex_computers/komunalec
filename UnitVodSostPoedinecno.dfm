object frmVodSost: TfrmVodSost
  Left = 0
  Top = 0
  Caption = #1042#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088
  ClientHeight = 359
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 359
    Width = 712
    Height = 0
    Panels = <>
    SimplePanel = True
    SimpleText = 
      'F6 - '#1042#1085#1077#1089#1080'/'#1055#1088#1086#1084#1077#1085#1080' '#1085#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072', F7 - '#1051#1086#1082#1072#1094#1080#1080' '#1079#1072' '#1087#1072#1088#1090#1085#1077#1088#1086#1090', Esc' +
      ' - '#1048#1079#1083#1077#1079
  end
  object panelGlaven: TPanel
    Left = 0
    Top = 0
    Width = 712
    Height = 359
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 1
    object panelDolen: TPanel
      Left = 2
      Top = 2
      Width = 708
      Height = 355
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 0
      object panelDolenGore: TPanel
        Left = 2
        Top = 2
        Width = 704
        Height = 199
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 2
          Top = 2
          Width = 700
          Height = 195
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DSKomVodomeriSostojba
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnSorting = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView1Column5: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'PARTNER'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1Column4: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV'
              HeaderAlignmentHorz = taCenter
              Width = 123
            end
            object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
              Caption = #1051#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'LOKACIIN_ID'
            end
            object cxGrid1DBTableView1Column3: TcxGridDBColumn
              Caption = #1056#1077#1076#1077#1085' '#1073#1088'. '#1074#1086' '#1095#1080#1090'. '#1082#1085#1080#1075#1072
              DataBinding.FieldName = 'CITACKA_KNIGA_RB'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1Column2: TcxGridDBColumn
              Caption = #1051#1086#1082'. '#1072#1082#1090#1080#1074#1085#1072
              DataBinding.FieldName = 'LOKACIJA_AKTIVEN'
              HeaderAlignmentHorz = taCenter
              Width = 70
            end
            object cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn
              Caption = #1042#1086#1076#1086#1084#1077#1088
              DataBinding.FieldName = 'VODOMERI_ID'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn
              Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
              DataBinding.FieldName = 'STARA_SOSTOJBA'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn
              Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
              DataBinding.FieldName = 'NOVA_SOSTOJBA'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              Caption = #1056#1072#1079#1083#1080#1082#1072
              DataBinding.FieldName = 'RAZLIKA'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1MESEC: TcxGridDBColumn
              Caption = #1052#1077#1089#1077#1094
              DataBinding.FieldName = 'MESEC'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
              DataBinding.FieldName = 'REON_ID'
              Visible = False
            end
            object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
              DataBinding.FieldName = 'CITACKA_KNIGA'
              Visible = False
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Visible = False
            end
            object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
              DataBinding.FieldName = 'ADRESA'
              Visible = False
            end
            object cxGrid1DBTableView1VODOMER_STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'VODOMER_STATUS'
              Visible = False
            end
            object cxGrid1DBTableView1PAUSAL: TcxGridDBColumn
              Caption = #1055#1072#1091#1096#1072#1083
              DataBinding.FieldName = 'PAUSAL'
              HeaderAlignmentHorz = taCenter
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object panelDolenDole: TPanel
        Left = 2
        Top = 201
        Width = 704
        Height = 152
        Align = alClient
        BevelInner = bvLowered
        TabOrder = 1
        object lblNovaSostojba: TLabel
          Left = 15
          Top = 100
          Width = 71
          Height = 13
          Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        end
        object lblStaraSostojba: TLabel
          Left = 15
          Top = 75
          Width = 77
          Height = 13
          Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        end
        object lblNaziv: TLabel
          Left = 15
          Top = 25
          Width = 75
          Height = 13
          Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
        end
        object Label5: TLabel
          Left = 15
          Top = 49
          Width = 37
          Height = 13
          Caption = #1040#1076#1088#1077#1089#1072
        end
        object cxControlNOVA_SOSTOJBA: TcxDBTextEdit
          Left = 109
          Top = 99
          BeepOnEnter = False
          DataBinding.DataField = 'NOVA_SOSTOJBA'
          DataBinding.DataSource = DSKomVodomeriSostojba
          TabOrder = 1
          OnExit = cxControlNOVA_SOSTOJBAExit
          OnKeyDown = AllKeyDown
          Width = 84
        end
        object cxControlSTARA_SOSTOJBA: TcxDBTextEdit
          Left = 108
          Top = 72
          BeepOnEnter = False
          DataBinding.DataField = 'STARA_SOSTOJBA'
          DataBinding.DataSource = DSKomVodomeriSostojba
          TabOrder = 0
          OnKeyDown = AllKeyDown
          Width = 84
        end
        object cxControlNAZIV: TcxDBTextEdit
          Left = 108
          Top = 21
          BeepOnEnter = False
          DataBinding.DataField = 'NAZIV'
          DataBinding.DataSource = DSKomVodomeriSostojba
          Enabled = False
          Properties.ReadOnly = True
          TabOrder = 2
          Width = 199
        end
        object cxControlADRESA: TcxDBTextEdit
          Left = 108
          Top = 45
          BeepOnEnter = False
          DataBinding.DataField = 'ADRESA'
          DataBinding.DataSource = DSKomVodomeriSostojba
          Enabled = False
          Properties.ReadOnly = True
          TabOrder = 3
          Width = 199
        end
        object StatusBar2: TStatusBar
          Left = 2
          Top = 131
          Width = 700
          Height = 19
          Panels = <>
          SimplePanel = True
          SimpleText = 'F6 -'#1055#1088#1086#1084#1077#1085#1080' '#1089#1086#1089#1090#1086#1112#1073#1072', Esc - '#1048#1079#1083#1077#1079
        end
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      #1040#1082#1094#1080#1080)
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    PopupMenuLinks = <>
    Style = bmsFlat
    UseSystemFont = False
    Left = 424
    Top = 40
    PixelsPerInch = 96
    object BarButtonIzlez: TdxBarButton
      Action = aIzlez
      Category = 1
    end
    object SubListAkcii: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonZapisiPodatoci'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzlez'
        end>
    end
    object dxBarButtonZapisiPodatoci: TdxBarButton
      Action = aZapisiPodatoci
      Category = 1
    end
    object dxBarButton: TdxBarButton
      Action = aAzuriraj
      Category = 1
    end
  end
  object ActionList1: TActionList
    Left = 392
    Top = 40
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aZapisiPodatoci: TAction
      Caption = #1047#1072#1087#1080#1096#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      Enabled = False
      ShortCut = 120
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
    end
    object aLokacii: TAction
      Caption = #1051#1086#1082#1072#1094#1080#1080
      ShortCut = 118
    end
    object aAzurirajPoedinecno: TAction
      Caption = 'aAzurirajPoedinecno'
      ShortCut = 117
      OnExecute = aAzurirajPoedinecnoExecute
    end
    object aCenovnik: TAction
      Caption = #1062#1077#1085#1086#1074#1085#1080#1082
    end
    object aEdinecnaSostojba: TAction
      Caption = 'aEdinecnaSostojba'
      ShortCut = 116
      OnExecute = aEdinecnaSostojbaExecute
    end
  end
  object DSKomVodomeriSostojba: TDataSource
    DataSet = TblKomVodomeriSostojba
    Left = 575
    Top = 58
  end
  object TblKomVodomeriSostojba: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT'
      '    KL.REON_ID,'
      '    KL.CITACKA_KNIGA,'
      '    KL.CITACKA_KNIGA_RB,'
      '    kl.rb1,'
      '    KS.LOKACIIN_ID,'
      '    KS.VODOMERI_ID,'
      '    KS.STARA_SOSTOJBA,'
      '    KS.NOVA_SOSTOJBA,'
      '    KS.RAZLIKA,'
      '    KS.GODINA,'
      '    KS.MESEC,'
      '    KLa.TIP_PARTNER,'
      '    KLa.PARTNER,'
      '    P.NAZIV ,'
      '    P.ADRESA,'
      
        '    (CASE WHEN KL.AKTIVEN=1 THEN '#39#1044#1040#39' ELSE '#39#1053#1045#39' END) LOKACIJA_AK' +
        'TIVEN,'
      '    KV.STATUS VODOMER_STATUS,'
      '    KV.PAUSAL,'
      '    COALESCE(KL.BROJ_NA_CLENOVI,0) BROJ_NA_CLENOVI'
      'FROM KOM_VODOMERI_SOSTOJBA KS'
      
        '     inner join KOM_VODOMERI KV on KS.VODOMERI_ID=KV.ID and KS.L' +
        'OKACIIN_ID=KV.LOKACIIN_ID'
      '     inner join KOM_LOKACII KL on  KV.LOKACIIN_ID=KL.ID'
      '     inner join kom_lokacija_Arhiva kla on kla.id_lokacija=kl.id'
      
        '     inner join MAT_PARTNER P on KLa.TIP_PARTNER=P.TIP_PARTNER A' +
        'ND KLa.PARTNER=P.ID'
      'WHERE( '
      '    KL.REON_ID=:REON_ID'
      '    AND KL.CITACKA_KNIGA=:CITACKA_KNIGA'
      '    AND KS.GODINA=:GODINA'
      '    AND KS.MESEC=:MESEC'
      '    and kla.tip_partner=:tp'
      '    and kla.partner=:p'
      
        '    and (((encodedate(1,:mesec,:godina) between encodedate(1,kla' +
        '.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.k' +
        'raj_godina)) and kla.kraj_mesec is not null)'
      
        '     or ((encodedate(1,:mesec,:godina) >= encodedate(1,kla.poc_m' +
        'esec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc_' +
        'mesec is not null) ))'
      '     ) and (     KS.LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and KS.VODOMERI_ID = :OLD_VODOMERI_ID'
      '    and KS.GODINA = :OLD_GODINA'
      '    and KS.MESEC = :OLD_MESEC'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'SELECT'
      '    KL.REON_ID,'
      '    KL.CITACKA_KNIGA,'
      '    KL.CITACKA_KNIGA_RB,'
      '    kl.rb1,'
      '    KS.LOKACIIN_ID,'
      '    KS.VODOMERI_ID,'
      '    KS.STARA_SOSTOJBA,'
      '    KS.NOVA_SOSTOJBA,'
      '    KS.RAZLIKA,'
      '    KS.GODINA,'
      '    KS.MESEC,'
      '    KLa.TIP_PARTNER,'
      '    KLa.PARTNER,'
      '    P.NAZIV ,'
      '    P.ADRESA,'
      
        '    (CASE WHEN KL.AKTIVEN=1 THEN '#39#1044#1040#39' ELSE '#39#1053#1045#39' END) LOKACIJA_AK' +
        'TIVEN,'
      '    KV.STATUS VODOMER_STATUS,'
      '    KV.PAUSAL,'
      '    COALESCE(KL.BROJ_NA_CLENOVI,0) BROJ_NA_CLENOVI'
      'FROM KOM_VODOMERI_SOSTOJBA KS'
      
        '     inner join KOM_VODOMERI KV on KS.VODOMERI_ID=KV.ID and KS.L' +
        'OKACIIN_ID=KV.LOKACIIN_ID'
      '     inner join KOM_LOKACII KL on  KV.LOKACIIN_ID=KL.ID'
      '     inner join kom_lokacija_Arhiva kla on kla.id_lokacija=kl.id'
      
        '     inner join MAT_PARTNER P on KLa.TIP_PARTNER=P.TIP_PARTNER A' +
        'ND KLa.PARTNER=P.ID'
      'WHERE'
      '    KL.REON_ID=:REON_ID'
      '    AND KL.CITACKA_KNIGA=:CITACKA_KNIGA'
      '    AND KS.GODINA=:GODINA'
      '    AND KS.MESEC=:MESEC'
      '    and kla.tip_partner=:tp'
      '    and kla.partner=:p'
      
        '    and (((encodedate(1,:mesec,:godina) between encodedate(1,kla' +
        '.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.k' +
        'raj_godina)) and kla.kraj_mesec is not null)'
      
        '     or ((encodedate(1,:mesec,:godina) >= encodedate(1,kla.poc_m' +
        'esec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc_' +
        'mesec is not null) ))'
      '')
    AutoUpdateOptions.UpdateTableName = 'KOM_VODOMERI_SOSTOJBA'
    AutoUpdateOptions.KeyFields = 'LOKACIIN_ID;VODOMERI_ID;GODINA;MESEC'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 536
    Top = 61
    object TblKomVodomeriSostojbaREON_ID: TFIBIntegerField
      FieldName = 'REON_ID'
    end
    object TblKomVodomeriSostojbaCITACKA_KNIGA: TFIBIntegerField
      FieldName = 'CITACKA_KNIGA'
    end
    object TblKomVodomeriSostojbaCITACKA_KNIGA_RB: TFIBIntegerField
      FieldName = 'CITACKA_KNIGA_RB'
    end
    object TblKomVodomeriSostojbaLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object TblKomVodomeriSostojbaVODOMERI_ID: TFIBIntegerField
      FieldName = 'VODOMERI_ID'
    end
    object TblKomVodomeriSostojbaSTARA_SOSTOJBA: TFIBIntegerField
      FieldName = 'STARA_SOSTOJBA'
    end
    object TblKomVodomeriSostojbaNOVA_SOSTOJBA: TFIBIntegerField
      FieldName = 'NOVA_SOSTOJBA'
    end
    object TblKomVodomeriSostojbaRAZLIKA: TFIBBCDField
      FieldName = 'RAZLIKA'
      Size = 0
    end
    object TblKomVodomeriSostojbaGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object TblKomVodomeriSostojbaMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object TblKomVodomeriSostojbaTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object TblKomVodomeriSostojbaNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblKomVodomeriSostojbaADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblKomVodomeriSostojbaLOKACIJA_AKTIVEN: TFIBStringField
      FieldName = 'LOKACIJA_AKTIVEN'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object TblKomVodomeriSostojbaVODOMER_STATUS: TFIBSmallIntField
      FieldName = 'VODOMER_STATUS'
    end
    object TblKomVodomeriSostojbaPAUSAL: TFIBSmallIntField
      FieldName = 'PAUSAL'
    end
    object TblKomVodomeriSostojbaBROJ_NA_CLENOVI: TFIBIntegerField
      FieldName = 'BROJ_NA_CLENOVI'
    end
    object TblKomVodomeriSostojbaRB1: TFIBIntegerField
      FieldName = 'RB1'
    end
    object TblKomVodomeriSostojbaPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
  end
end
