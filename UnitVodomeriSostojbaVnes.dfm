object FrmVodomeriSostojba: TFrmVodomeriSostojba
  Left = 0
  Top = 0
  Caption = #1042#1085#1077#1089' '#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1085#1072' '#1073#1088#1086#1080#1083#1072
  ClientHeight = 587
  ClientWidth = 746
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 568
    Width = 746
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 
      'F6 - '#1042#1085#1077#1089#1080'/'#1055#1088#1086#1084#1077#1085#1080' '#1085#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072', F7 - '#1051#1086#1082#1072#1094#1080#1080' '#1079#1072' '#1087#1072#1088#1090#1085#1077#1088#1086#1090', F8 ' +
      '- '#1041#1088#1080#1096#1080' '#1089#1086#1089#1090#1086#1112#1073#1072', Esc - '#1048#1079#1083#1077#1079
  end
  object panelGlaven: TPanel
    Left = 0
    Top = 0
    Width = 746
    Height = 568
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 1
    object panelGoren: TPanel
      Left = 2
      Top = 2
      Width = 742
      Height = 137
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object lblReon: TLabel
        Left = 8
        Top = 15
        Width = 24
        Height = 13
        Caption = #1056#1077#1086#1085
      end
      object Label1: TLabel
        Left = 8
        Top = 40
        Width = 75
        Height = 13
        Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
      end
      object lblGodina: TLabel
        Left = 8
        Top = 65
        Width = 37
        Height = 13
        Caption = #1043#1086#1076#1080#1085#1072
      end
      object lblMesec: TLabel
        Left = 8
        Top = 92
        Width = 31
        Height = 13
        Caption = #1052#1077#1089#1077#1094
      end
      object img1: TImage
        Left = 629
        Top = 7
        Width = 97
        Height = 115
        Visible = False
      end
      object cxLookupCBReoni: TcxLookupComboBox
        Left = 90
        Top = 11
        BeepOnEnter = False
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            Width = 100
            FieldName = 'ID'
          end
          item
            Caption = #1053#1072#1079#1080#1074
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = DM1.DSKomReoni
        Properties.OnChange = cxLookupCBReoniPropertiesChange
        Properties.OnEditValueChanged = cxLookupCBReoniPropertiesEditValueChanged
        TabOrder = 0
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 105
      end
      object cxCheckBoxZaCitackaKniga: TcxCheckBox
        Left = 236
        Top = 38
        TabStop = False
        Caption = #1047#1072' '#1095#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
        Properties.OnChange = cxCheckBoxZaCitackaKnigaPropertiesChange
        State = cbsChecked
        TabOrder = 5
        Visible = False
        Width = 113
      end
      object cxLookupCBCitackaKniga: TcxLookupComboBox
        Left = 90
        Top = 36
        BeepOnEnter = False
        Properties.DropDownListStyle = lsEditList
        Properties.KeyFieldNames = 'CITACKA_KNIGA'
        Properties.ListColumns = <
          item
            Caption = #1063#1080#1090#1072#1095#1082#1072' '#1082#1085#1080#1075#1072
            FieldName = 'CITACKA_KNIGA'
          end>
        Properties.ListSource = DM1.DSCitackiKnigiPoReon
        TabOrder = 1
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 105
      end
      object cxComboBoxMesec: TcxComboBox
        Left = 90
        Top = 89
        BeepOnEnter = False
        Properties.DropDownListStyle = lsEditFixedList
        Properties.ImmediatePost = True
        Properties.Items.Strings = (
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10'
          '11'
          '12')
        TabOrder = 3
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 104
      end
      object cxComboBoxGodini: TcxComboBox
        Left = 90
        Top = 63
        BeepOnEnter = False
        TabOrder = 2
        OnEnter = onEnterAll
        OnExit = onExitAll
        OnKeyDown = onKeyDownLookUpAll
        Width = 104
      end
      object cxBtnPartneri: TcxButton
        Left = 221
        Top = 86
        Width = 156
        Height = 27
        Caption = #1057#1087#1088#1077#1084#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
        TabOrder = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = cxBtnPartneriClick
      end
      object dbimgIMG: TDBImage
        Left = 453
        Top = 26
        Width = 105
        Height = 105
        DataField = 'IMG'
        DataSource = dsVodSost
        TabOrder = 6
        Visible = False
      end
    end
    object panelDolen: TPanel
      Left = 2
      Top = 139
      Width = 742
      Height = 427
      Align = alClient
      BevelInner = bvLowered
      Enabled = False
      TabOrder = 1
      object panelDolenGore: TPanel
        Left = 2
        Top = 2
        Width = 738
        Height = 283
        Align = alClient
        BevelInner = bvLowered
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 2
          Top = 2
          Width = 734
          Height = 279
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = DM1.DSKomVodomeriSostojba
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnSorting = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView1Column5: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'PARTNER_ID'
              HeaderAlignmentHorz = taCenter
              Width = 61
            end
            object cxGrid1DBTableView1Column4: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV'
              HeaderAlignmentHorz = taCenter
              Width = 170
            end
            object cxGrid1DBTableView1Column3: TcxGridDBColumn
              Caption = #1056#1077#1076#1077#1085' '#1073#1088'. '#1074#1086' '#1095#1080#1090'. '#1082#1085#1080#1075#1072
              DataBinding.FieldName = 'CITACKA_KNIGA_RB'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1Column2: TcxGridDBColumn
              Caption = #1051#1086#1082'. '#1072#1082#1090#1080#1074#1085#1072
              DataBinding.FieldName = 'LOKACIJA_AKTIVEN'
              HeaderAlignmentHorz = taCenter
              Width = 60
            end
            object cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn
              Caption = #1042#1086#1076#1086#1084#1077#1088
              DataBinding.FieldName = 'VODOMERI_ID'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn
              Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
              DataBinding.FieldName = 'STARA_SOSTOJBA'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn
              Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
              DataBinding.FieldName = 'NOVA_SOSTOJBA'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              Caption = #1056#1072#1079#1083#1080#1082#1072
              DataBinding.FieldName = 'RAZLIKA'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1MESEC: TcxGridDBColumn
              Caption = #1052#1077#1089#1077#1094
              DataBinding.FieldName = 'MESEC'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1REON_ID: TcxGridDBColumn
              DataBinding.FieldName = 'REON_ID'
              Visible = False
            end
            object cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn
              DataBinding.FieldName = 'CITACKA_KNIGA'
              Visible = False
            end
            object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
              DataBinding.FieldName = 'LOKACIIN_ID'
              Visible = False
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Visible = False
            end
            object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
              DataBinding.FieldName = 'ADRESA'
              Visible = False
            end
            object cxGrid1DBTableView1VODOMER_STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'VODOMER_STATUS'
              Visible = False
            end
            object cxGrid1DBTableView1PAUSAL: TcxGridDBColumn
              Caption = #1055#1072#1091#1096#1072#1083
              DataBinding.FieldName = 'PAUSAL'
              HeaderAlignmentHorz = taCenter
              Width = 55
            end
            object cxGrid1DBTableView1AVTOMATSKI_VNES: TcxGridDBColumn
              Caption = #1040#1074#1090#1086#1084#1072#1090#1089#1082#1080' '#1074#1085#1077#1089
              DataBinding.FieldName = 'AVTOMATSKI_VNES'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object panelDolenDole: TPanel
        Left = 2
        Top = 285
        Width = 738
        Height = 140
        Align = alBottom
        BevelInner = bvLowered
        TabOrder = 1
        object lblNovaSostojba: TLabel
          Left = 15
          Top = 101
          Width = 71
          Height = 13
          Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        end
        object lblStaraSostojba: TLabel
          Left = 15
          Top = 77
          Width = 77
          Height = 13
          Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        end
        object lblNaziv: TLabel
          Left = 15
          Top = 25
          Width = 75
          Height = 13
          Caption = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
        end
        object Label5: TLabel
          Left = 15
          Top = 48
          Width = 37
          Height = 13
          Caption = #1040#1076#1088#1077#1089#1072
        end
        object cxControlNOVA_SOSTOJBA: TcxDBTextEdit
          Left = 109
          Top = 99
          BeepOnEnter = False
          DataBinding.DataField = 'NOVA_SOSTOJBA'
          DataBinding.DataSource = DM1.DSKomVodomeriSostojba
          TabOrder = 1
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 84
        end
        object cxControlSTARA_SOSTOJBA: TcxDBTextEdit
          Left = 108
          Top = 72
          BeepOnEnter = False
          DataBinding.DataField = 'STARA_SOSTOJBA'
          DataBinding.DataSource = DM1.DSKomVodomeriSostojba
          TabOrder = 0
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 84
        end
        object cxControlNAZIV: TcxDBTextEdit
          Left = 108
          Top = 21
          BeepOnEnter = False
          DataBinding.DataField = 'NAZIV'
          DataBinding.DataSource = DM1.DSKomVodomeriSostojba
          Properties.ReadOnly = True
          TabOrder = 2
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 199
        end
        object GroupBox1: TGroupBox
          Left = 465
          Top = 6
          Width = 231
          Height = 121
          Caption = #1057#1086#1089#1090#1086#1112#1073#1072' '#1079#1072' '#1087#1088#1077#1076#1093#1086#1076#1077#1085' '#1079#1072#1087#1080#1089
          Enabled = False
          TabOrder = 3
          object Label3: TLabel
            Left = 27
            Top = 51
            Width = 71
            Height = 13
            Caption = #1053#1086#1074#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          end
          object Label4: TLabel
            Left = 57
            Top = 97
            Width = 50
            Height = 13
            AutoSize = False
            Caption = #1056#1072#1079#1083#1080#1082#1072
          end
          object Label2: TLabel
            Left = 21
            Top = 74
            Width = 77
            Height = 13
            Caption = #1057#1090#1072#1088#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          end
          object cxTxtNazivPredhoden: TcxTextEdit
            Left = 13
            Top = 16
            TabOrder = 0
            Width = 206
          end
          object cxTxtRazlikaPredhoden: TcxTextEdit
            Left = 110
            Top = 89
            TabOrder = 1
            Width = 86
          end
          object cxTxtStaraPredhoden: TcxTextEdit
            Left = 111
            Top = 66
            TabOrder = 2
            Width = 86
          end
          object cxTxtNovaPredhoden: TcxTextEdit
            Left = 111
            Top = 43
            TabOrder = 3
            Width = 86
          end
        end
        object cxControlADRESA: TcxDBTextEdit
          Left = 108
          Top = 45
          BeepOnEnter = False
          DataBinding.DataField = 'ADRESA'
          DataBinding.DataSource = DM1.DSKomVodomeriSostojba
          Properties.ReadOnly = True
          TabOrder = 4
          OnEnter = onEnterAll
          OnExit = onExitAll
          OnKeyDown = onKeyDownAll
          Width = 199
        end
        object cxBtnZapisiPodatoci: TcxButton
          Left = 219
          Top = 92
          Width = 105
          Height = 25
          Caption = #1047#1072#1087#1080#1096#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
          Enabled = False
          TabOrder = 5
          OnClick = aZapisiPodatociExecute
        end
      end
    end
  end
  object cxButton1: TcxButton
    Left = 608
    Top = 40
    Width = 75
    Height = 25
    Caption = 'aVratenTekst'
    TabOrder = 2
    Visible = False
    OnClick = aVratenTekstExecute
  end
  object cxMemo1: TcxMemo
    Left = 202
    Top = 9
    Lines.Strings = (
      'cxMemo1')
    ParentFont = False
    Properties.ScrollBars = ssBoth
    Properties.WordWrap = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 3
    Visible = False
    Height = 73
    Width = 390
  end
  object cxButton2: TcxButton
    Left = 566
    Top = 88
    Width = 124
    Height = 24
    Action = aPogledVS
    Caption = #1057#1086#1089#1090#1086#1112#1073#1072' '#1085#1072' '#1074#1086#1076#1086#1084#1077#1088#1080
    TabOrder = 4
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      #1040#1082#1094#1080#1080)
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    PopupMenuLinks = <>
    Style = bmsFlat
    UseSystemFont = False
    Left = 597
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1052#1077#1085#1080
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 499
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemLinks = <
        item
          Visible = True
          ItemName = 'SubListAkcii'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = True
      Visible = False
      WholeRow = False
    end
    object BarButtonIzlez: TdxBarButton
      Caption = #1048#1079#1083#1077#1079
      Category = 1
      Visible = ivAlways
      ShortCut = 27
      OnClick = aIzlezExecute
    end
    object SubListAkcii: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonZapisiPodatoci'
        end
        item
          Visible = True
          ItemName = 'BarButtonIzlez'
        end>
    end
    object dxBarButtonZapisiPodatoci: TdxBarButton
      Caption = #1047#1072#1087#1080#1096#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      Category = 1
      Enabled = False
      Visible = ivAlways
      ShortCut = 120
      OnClick = aZapisiPodatociExecute
    end
    object dxBarButton: TdxBarButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 1
      Visible = ivAlways
      ShortCut = 117
      OnClick = aAzurirajExecute
    end
  end
  object CustomizeDlg1: TCustomizeDlg
    StayOnTop = False
    Left = 527
    Top = 16
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.CharSet = 'utf-8'
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.AcceptCharSet = 'utf-8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 702
    Top = 16
  end
  object qUpdateVS: TpFIBUpdateObject
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE KOM_VODOMERI_SOSTOJBA SET '
      '--    LOKACIIN_ID=:LOKACIIN_ID,'
      '--    VODOMERI_ID=:VODOMERI_ID,'
      '--    STARA_SOSTOJBA=:STARA_SOSTOJBA,'
      '    STARA_SOSTOJBA = :stara_sostojba,'
      '    NOVA_SOSTOJBA=:NOVA_SOSTOJBA ,'
      '    AVTOMATSKI_VNES = 1,'
      '    slika = :link_slika,'
      '    zabeleska = :zabeleska,'
      '    img = :img '
      '--  GODINA=:GODINA,'
      '--   MESEC=:MESEC,'
      '--    POSLEDNA_SOSTOJBA=:POSLEDNA_SOSTOJBA,'
      '--    LOKACIIN_ID_T=:LOKACIIN_ID_T,'
      '--    TIP_PARTNER=:TIP_PARTNER,'
      '--    PARTNER=:PARTNER,'
      '--    REON_ID=:REON_ID,'
      '--    RAZLIKA=:RAZLIKA,'
      '--    TS=:TS'
      'WHERE'
      '    LOKACIIN_ID=:LOKACIIN_ID AND '
      '    VODOMERI_ID=:VODOMERI_ID AND '
      '    GODINA=:GODINA AND '
      '    MESEC=:MESEC')
    OrderInList = 0
    Left = 632
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object IdIcmpClient1: TIdIcmpClient
    Host = 'www.google.com'
    Protocol = 1
    ProtocolIPv6 = 0
    IPVersion = Id_IPv4
    PacketSize = 1024
    OnReply = IdIcmpClient1Reply
    Left = 492
    Top = 51
  end
  object ActionList1: TActionList
    Left = 492
    Top = 16
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aZapisiPodatoci: TAction
      Caption = #1047#1072#1087#1080#1096#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      Enabled = False
      ShortCut = 120
      OnExecute = aZapisiPodatociExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aLokacii: TAction
      Caption = #1051#1086#1082#1072#1094#1080#1080
      ShortCut = 118
      OnExecute = aLokaciiExecute
    end
    object aAzurirajPoedinecno: TAction
      Caption = 'aAzurirajPoedinecno'
      ShortCut = 16501
      OnExecute = aAzurirajPoedinecnoExecute
    end
    object aCenovnik: TAction
      Caption = #1062#1077#1085#1086#1074#1085#1080#1082
    end
    object aBrisi: TAction
      Caption = 'aBrisi'
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aVratenTekst: TAction
      Caption = 'aVratenTekst'
      OnExecute = aVratenTekstExecute
    end
    object aPogledVS: TAction
      Caption = 'aPogledVS'
      OnExecute = aPogledVSExecute
    end
  end
  object qProveriVnes: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'SELECT'
      '  first 1 avtomatski_vnes'
      'FROM'
      '    KOM_VODOMERI_SOSTOJBA vs'
      '   inner join kom_lokacii l on l.id = vs.lokaciin_id'
      'where vs.mesec = :mesec'
      'and vs.godina = :godina'
      'and vs.avtomatski_vnes >= 1'
      'and l.reon_id = :reon_id'
      'and l.citacka_kniga = :citacka_kniga')
    Left = 562
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qProveriSostojba: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'SELECT'
      '  v.stara_sostojba'
      'FROM'
      '    KOM_VODOMERI_SOSTOJBA v'
      'where v.mesec = :mesec'
      'and v.godina = :godina '
      'and v.lokaciin_id = :lokacija'
      'and v.vodomeri_id  = :vodomer')
    Left = 597
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object XMLServis: TXMLDocument
    Left = 702
    Top = 51
    DOMVendorDesc = 'MSXML'
  end
  object tblVodSost: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_VODOMERI_SOSTOJBA'
      'SET '
      '    IMG = :IMG'
      'WHERE'
      '    LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and VODOMERI_ID = :OLD_VODOMERI_ID'
      '    ')
    RefreshSQL.Strings = (
      'select *'
      'from kom_vodomeri_sostojba v'
      '--inner join kom_lokacii l on l.id=v.lokaciin_id'
      
        'where(  v.lokaciin_id=:mas_lokacija  and v.mesec=:MES and v.godi' +
        'na=:GOD'
      '--order by l.reon_id,l.citacka_kniga, l.citacka_kniga_rb,l.rb2'
      '     ) and (     V.LOKACIIN_ID = :OLD_LOKACIIN_ID'
      '    and V.VODOMERI_ID = :OLD_VODOMERI_ID'
      '    and V.GODINA = :OLD_GODINA'
      '    and V.MESEC = :OLD_MESEC'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select v.img, v.lokaciin_id, v.vodomeri_id'
      'from kom_vodomeri_sostojba v'
      
        'where v.lokaciin_id=:lokacija  and v.mesec=:MESEC and v.godina=:' +
        'GODINA and v.vodomeri_id = :vodomer')
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 527
    Top = 51
    oRefreshDeletedRecord = True
    object tblVodSostIMG: TFIBBlobField
      FieldName = 'IMG'
      Size = 8
    end
    object tblVodSostLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object tblVodSostVODOMERI_ID: TFIBIntegerField
      FieldName = 'VODOMERI_ID'
    end
  end
  object dsVodSost: TDataSource
    DataSet = tblVodSost
    Left = 562
    Top = 16
  end
  object IdHTTP2: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 667
    Top = 16
  end
  object IdDecoderMIME1: TIdDecoderMIME
    FillChar = '='
    Left = 632
    Top = 16
  end
  object dlgSave1: TSaveDialog
    DefaultExt = 'txt'
    FileName = 'MyImage'
    InitialDir = 'C:\cxCache'
    Left = 667
    Top = 51
  end
  object qUpdateVSSS: TpFIBUpdateObject
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE KOM_VODOMERI_SOSTOJBA SET '
      '--    LOKACIIN_ID=:LOKACIIN_ID,'
      '--    VODOMERI_ID=:VODOMERI_ID,'
      '    STARA_SOSTOJBA=:STARA_SOSTOJBA,'
      '    NOVA_SOSTOJBA=:NOVA_SOSTOJBA ,'
      '    AVTOMATSKI_VNES = 1,'
      '    slika = :link_slika,'
      '    zabeleska = :zabeleska,'
      '    img = :img '
      '--  GODINA=:GODINA,'
      '--   MESEC=:MESEC,'
      '--    POSLEDNA_SOSTOJBA=:POSLEDNA_SOSTOJBA,'
      '--    LOKACIIN_ID_T=:LOKACIIN_ID_T,'
      '--    TIP_PARTNER=:TIP_PARTNER,'
      '--    PARTNER=:PARTNER,'
      '--    REON_ID=:REON_ID,'
      '--    RAZLIKA=:RAZLIKA,'
      '--    TS=:TS'
      'WHERE'
      '    LOKACIIN_ID=:LOKACIIN_ID AND '
      '    VODOMERI_ID=:VODOMERI_ID AND '
      '    GODINA=:GODINA AND '
      '    MESEC=:MESEC')
    OrderInList = 0
    Left = 640
    Top = 91
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateImaSlika: TpFIBQuery
    Transaction = DM1.TransakcijaQ
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_vodomeri_sostojba_s G'
      'set G.ima_SLIKA = :IMA_SLIKA'
      'where G.LOKACIIN_ID = :LOKACIJA and'
      '      G.MESEC = :MESEC and'
      '      G.GODINA = :GODINA')
    Left = 718
    Top = 121
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
