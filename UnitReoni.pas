unit UnitReoni;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxLookAndFeels, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxBar, dxPSCore,
  dxPScxCommon, //dxPScxGrid6Lnk,
   cxBarEditItem, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator,
  dxRibbonCustomizationForm, FIBDataSet, pFIBDataSet, System.Actions,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxLabel,
  cxDBLookupComboBox;

type
  TFrmReoni = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    lblNAZIV: TLabel;
    cxControlNAZIV: TcxDBTextEdit;
    tblInkasant: TpFIBDataSet;
    dsInkasant: TDataSource;
    tblInkasantTIP_PARTNER: TFIBIntegerField;
    tblInkasantNAZIV: TFIBStringField;
    cxLabel8: TcxLabel;
    txtTipInkasant: TcxDBTextEdit;
    txtInkasant: TcxDBTextEdit;
    cbInkasant: TcxLookupComboBox;
    tblInkasantID: TFIBStringField;
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure callFrmUlici();
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmReoni: TFrmReoni;

implementation

uses UnitDM1, UnitUlici, dmKonekcija;

{$R *.dfm}

procedure TFrmReoni.cxDBTextEditAllExit(Sender: TObject);
begin
    inherited;
    if ((Sender as TWinControl)= txtinkasant) or ((Sender as TWinControl)= txtTipinkasant) then
         begin
              dm1.SetirajLukap(sender,tblInkasant,txtTipInkasant,txtInkasant, cbInkasant);
         end
         else
         if ((Sender as TWinControl)= cbInkasant) then
         begin
            if (cbInkasant.Text <>'') then
            begin
              txtTipInkasant.Text := tblInkasantTIP_PARTNER.AsString;
              txtInkasant.Text := tblInkasantID.AsString;
            end
            else
            begin
              DM1.TblKomReoniTIP_INKASATOR.Clear;
              dm1.TblKomReoniID_INKASATOR.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtInkasant) or ((Sender as TWinControl)= txtTipInkasant) then
         begin
              dm1.SetirajLukap(sender,tblInkasant,txtTipInkasant,txtInkasant, cbInkasant);
         end;
end;

procedure TFrmReoni.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
    callFrmUlici();
end;

procedure TFrmReoni.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
   if cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse then
   begin
     cbInkasant.Text := DM1.TblKomReoniNAZIV.Value;
     //([ StrToInt(txtTipInkasant.Text), StrToFloat(DM1.TblKomReoniID.Value)])  //cxGrid1DBTableView1NAZIV.EditValue;
   end;
end;

procedure TFrmReoni.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
case key of
  VK_RETURN:
  begin
      callFrmUlici();
  end;
end;
end;

procedure TFrmReoni.FormShow(Sender: TObject);
begin
  inherited;
  tblInkasant.Open;
  if cxGrid1DBTableView1.DataController.DataSet.State = dsBrowse then
   begin
     cbInkasant.Text := tblInkasantNAZIV.Value;
   end;
end;

procedure TFrmReoni.callFrmUlici;
var frm:TFrmUlici;
begin
        DM1.TblKomUlici.Close();
        DM1.TblKomUlici.SelectSQL.Text := DM1.selectUliciPoReon+DM1.TblKomReoniID.AsString;
        DM1.TblKomUlici.Open();

        frm := TFrmUlici.Create(nil);
        frm.setDefaultReon(DM1.TblKomReoniID.AsInteger);
        frm.ShowModal();
        frm.Free();
end;

end.
