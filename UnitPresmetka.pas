unit UnitPresmetka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, ComCtrls, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxGraphics, dxSkinsCore, dxSkinsDefaultPainters, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, DateUtils,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxSkinscxPCPainter,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, frxClass, frxDBSet, frxDesgn,
  frxBarcode, dxSkinsdxBarPainter, dxBar, cxGridCustomPopupMenu, cxGridPopupMenu,
  cxGroupBox, cxRadioGroup, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust,
  dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue, pFIBScripter,
  cxLookAndFeels, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSpringTime, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  cxNavigator, System.Actions, idcoderMIME,System.IOUtils, cxmemo, FIBQuery,
  pFIBQuery, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, Vcl.DBCtrls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP , jpeg;

type
  TFrmKomPresmetka = class(TForm)
    panelGoren: TPanel;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    cxBtnPresmetaj: TcxButton;
    cxComboBoxGodini: TcxComboBox;
    cxComboBoxMesec: TcxComboBox;
    lblMesec: TLabel;
    lblGodina: TLabel;
    GroupBoxPoslednaPresmetka: TGroupBox;
    lblMesecLast: TLabel;
    lblMesecLastValue: TLabel;
    lblGodinaLast: TLabel;
    lblGodinaLastValue: TLabel;
    cxBtnIzbrisiPoslednaPresmetka: TcxButton;
    aIzbrisiLastPresmetka: TAction;
    cxLookupCBReoni: TcxLookupComboBox;
    lblReon: TLabel;
    panelDolen: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn;
    frxReport1: TfrxReport;
    frxDSPresmetkaG: TfrxDBDataset;
    frxDSPresmetkaS: TfrxDBDataset;
    frxDSPresmetkaVodomeriSostojba: TfrxDBDataset;
    aCallFrfBeleskiPrint: TAction;
    frxDesigner1: TfrxDesigner;
    frxBarCodeObject1: TfrxBarCodeObject;
    aPrintAllBeleskiFizicki: TAction;
    aPrintBeleskaOdberenFizicki: TAction;
    dxBarManager1: TdxBarManager;
    SubMenuAkcii: TdxBarSubItem;
    BarButtonIzlez: TdxBarButton;
    BarButtonPrintBeleskiAll: TdxBarButton;
    BarButtonPrintBeleskaSelected: TdxBarButton;
    aPrintAllFakturi: TAction;
    BarButtonPrintFakturiAll: TdxBarButton;
    BarButtonIzbrisiPresmetka: TdxBarButton;
    frxDSPresmetkaStavki1: TfrxDBDataset;
    frxDSPresmetkaStavki2: TfrxDBDataset;
    frxDSPresmetkaStavki3: TfrxDBDataset;
    frxDSPresmetkaStavki4: TfrxDBDataset;
    frxDSPresmetkaStavki5: TfrxDBDataset;
    frxDSPresmetkaStavki6: TfrxDBDataset;
    frxDSPresmetkaStavki7: TfrxDBDataset;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    frxDSSaldo: TfrxDBDataset;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    aEdnaFaktura: TAction;
    cxRadioGroup1: TcxRadioGroup;
    frxDSPresmetkaStavki8: TfrxDBDataset;
    frxDSPresmetkaStavki10: TfrxDBDataset;
    frxDSPresmetkaStavki61: TfrxDBDataset;
    frxDSPresmetkaStavki11: TfrxDBDataset;
    frxDSPresmetkaStavki12: TfrxDBDataset;
    frxDSPresmetkaStavki121: TfrxDBDataset;
    aMailFaktura: TAction;
    aMailFakturaDizajn: TAction;
    aNovIzgledB: TAction;
    aNovIzgledBDizajn: TAction;
    aNovIzgledSiteB: TAction;
    cxMemo1: TcxMemo;
    qUpdateImaSlika: TpFIBQuery;
    IdHTTP2: TIdHTTP;
    dbimgIMG: TDBImage;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function podatociOk():boolean;
    procedure cxBtnPresmetajClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure getLastPresmetka();
    procedure aIzbrisiLastPresmetkaExecute(Sender: TObject);
    procedure aCallFrfBeleskiPrintExecute(Sender: TObject);
    procedure closePresmetkaDatasets();
    procedure aPrintAllBeleskiFizickiExecute(Sender: TObject);
    procedure aPrintBeleskaOdberenFizickiExecute(Sender: TObject);
    procedure aPrintAllFakturiExecute(Sender: TObject);
    procedure aEdnaFakturaExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aMailFakturaExecute(Sender: TObject);
    procedure aMailFakturaDizajnExecute(Sender: TObject);
    procedure aNovIzgledBExecute(Sender: TObject);
    procedure aNovIzgledBDizajnExecute(Sender: TObject);
    procedure aNovIzgledSiteBExecute(Sender: TObject);
    function CompressJpeg(OutJPG: TJPEGImage): Integer;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmKomPresmetka: TFrmKomPresmetka;

implementation

uses UnitDM1, DaNe, dmKonekcija, //dmLook,
dmMaticni, UnitNevneseniSostojbi, utils, dmResources;

{$R *.dfm}

procedure TFrmKomPresmetka.aNovIzgledBDizajnExecute(Sender: TObject);
var mesec, line, ima_slika:string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i:integer;
    MS : TMemoryStream;
    GIf: TJPEGImage;
begin
   if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,35);
     // dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.TblPresmetkaGBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := DM1.TblPresmetkaGGODINA.AsString;  // DM1.TblPresmetkaGBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := DM1.TblPresmetkaGMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := DM1.TblPresmetkaGTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := DM1.TblPresmetkaGREON_ID.AsString;
      dmKon.tblSqlReport.Open;
    //  frxReportKP.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(dm1.tblKarParPOBARUVA.asstring));
    //  frxReportKP.DesignReport();
     // dmRes.frxReport1.DesignReport;
     if servis = '0' then
     begin
     try
      if not dm1.IsInternetConnected then
    //  IdIcmpClient1.Ping ('www.google.com');

    Except on E:Exception do
      begin
         ShowMessage('���� �������� ��������!');
        Abort;
      end
    end;
    if length(DM1.TblPresmetkaGMESEC.AsString) = 1 then
      mesec := '0'+DM1.TblPresmetkaGMESEC.AsString
    else mesec := DM1.TblPresmetkaGMESEC.AsString;

     dm1.ZemiTextFile(link_zemi+DM1.TblPresmetkaGGODINA.AsString+'-'+mesec+'&where=LOKACISKI_BROJ%20%3D%27'+DM1.TblPresmetkaGLOKACIIN_ID.asstring //+'%27%20AND%20CITACKA_KNIGA%20%3D%27'+cxLookupCBCitackaKniga.Text
          +'%27&select=SLIKA,MESEC_NA_CITANJE');

             cel_zapis := '';
             cxMemo1.Text := vraten_text;

             for i:=1 to cxMemo1.lines.Count do
             begin
               line := cxMemo1.Lines[i];
               cel_zapis := cel_zapis+line;
             end;
             if cel_zapis <> '' then
             begin
                cel_zapis := Trim(cel_zapis);
                AssignFile(zapis, 'C:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.txt');
                Rewrite(zapis);
                WriteLn(zapis, cel_zapis);
                CloseFile(zapis);

                sData := TFile.ReadAllText('C:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.txt'); // <- I also tried this line with , TEncoding.UTF8);
                streamImage := TFileStream.Create('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.png', fmOpenReadWrite + fmCreate);
              try
                 MIMEDecoder := TidDecoderMIME.Create(nil);
              try
                  MIMEDecoder.DecodeBegin(streamImage);
              try
                  MIMEDecoder.Decode(sData);

              finally
                  MIMEDecoder.DecodeEnd;
              end;
              finally
                  FreeAndNil(MIMEDecoder);
              end;
              finally
                  FreeAndNil(streamImage);
              end;
              ima_slika := '1';
             end
             else
              ima_slika := '0';

    end;
    end;
      dmres.frxReport1.Variables.AddVariable('Promenlivi','ima_slika',ima_slika);
      dmRes.frxReport1.DesignReport;
    if servis = '0' then
      begin
      if FileExists('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.png') then
      begin
         DeleteFile('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.png');
         DeleteFile('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.txt')
      end;
      end;

    //end;

end;

procedure TFrmKomPresmetka.aNovIzgledBExecute(Sender: TObject);
var mesec, line, ima_slika:string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i:integer;
    MS : TMemoryStream;
    GIf: TJPEGImage;
begin
   ima_slika := '0';
   if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,35);
      //dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.TblPresmetkaGBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.TblPresmetkaGGODINA.AsString;  // DM1.TblPresmetkaGBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.TblPresmetkaGMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := DM1.TblPresmetkaGTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := DM1.TblPresmetkaGREON_ID.AsString;
      dmKon.tblSqlReport.Open;
    //  frxReportKP.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(dm1.tblKarParPOBARUVA.asstring));
    //  frxReportKP.DesignReport();
    if servis = '1' then
    begin
    try
      if not dm1.IsInternetConnected then
    //  IdIcmpClient1.Ping ('www.google.com');

    Except on E:Exception do
      begin
         ShowMessage('���� �������� ��������!');
        Abort;
      end
    end;
        if dm1.TblPresmetkaGLINK_SLIKA.AsString <> '' then
            begin
                  try
                      MS := TMemoryStream.Create;
                      GIf := TJPEGImage.Create;

                      IdHTTP2.get(dm1.TblPresmetkaGLINK_SLIKA.AsString,MS);
                      Ms.Seek(0,soFromBeginning);
                      Gif.LoadFromStream(MS);
                      dbimgIMG.Picture.Graphic.Assign(GIf);
                      CompressJpeg(gif);
                   //   dbimgIMG.Picture.Bitmap.SaveToFile('c:\cxCache\MyImage'+dm1.tblPecDukaniLOKACIIN_ID.AsString+'.jpg');
                 finally
                        FreeAndNil(GIF);
                        FreeAndNil(MS);;
                end;
                 ima_slika := '1';
               end
               else
                ima_slika := '0';


       end;
      dmres.frxReport1.Variables.AddVariable('Promenlivi','erk', servis);
      dmres.frxReport1.Variables.AddVariable('Promenlivi','ima_slika',ima_slika);
      dmRes.frxReport1.ShowReport;

      if FileExists('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.jpg') then
          begin
             DeleteFile('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.jpg');
             //DeleteFile('c:\cxCache\MyImage'+DM1.tblPecDukaniLOKACIIN_ID.AsString+'.txt')
          end;

      end;
end;

procedure TFrmKomPresmetka.aNovIzgledSiteBExecute(Sender: TObject);
var mesec, line, ima_slika,lokacija,tf_slika:string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis, pos_red : WideString;
    i:integer;
    MS : TMemoryStream;
    GIf: TJPEGImage;
begin
//�� �� �������� ������� �� ��������� ����
  ima_slika := '0';
//�� �� ����� ������� �� ����
 if servis = '1' then
  begin
    try
      if not dm1.IsInternetConnected then

      Except on E:Exception do
        begin
           ShowMessage('���� �������� ��������!');
          Abort;
        end
      end;
      dm1.TblPresmetkaG.First;
    while not dm1.TblPresmetkaG.Eof do
    begin
    if length(DM1.TblPresmetkaGMESEC.AsString) = 1 then
      mesec := '0'+DM1.TblPresmetkaGMESEC.AsString
    else mesec := DM1.TblPresmetkaGMESEC.AsString;

        if dm1.TblPresmetkaGLINK_SLIKA.AsString <> '' then
            begin
                  try
                      MS := TMemoryStream.Create;
                      GIf := TJPEGImage.Create;

                      IdHTTP2.get(dm1.TblPresmetkaGLINK_SLIKA.AsString,MS);
                      Ms.Seek(0,soFromBeginning);
                      Gif.LoadFromStream(MS);
                      dbimgIMG.Picture.Graphic.Assign(GIf);
                      CompressJpeg(gif);
                 finally
                        FreeAndNil(GIF);
                        FreeAndNil(MS);;
                end;
                 ima_slika := '1';
                  qUpdateImaSlika.Close;
                  qUpdateImaSlika.ParamByName('ima_slika').Value := 1;
                  qUpdateImaSlika.ParamByName('lokacija').AsString := dm1.TblPresmetkaGLOKACIIN_ID.AsString;
                  qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
                  qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
                  qUpdateImaSlika.ExecQuery;
               end
               else
               begin
                ima_slika := '0';
                 qUpdateImaSlika.Close;
                 qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
                 qUpdateImaSlika.ParamByName('lokacija').AsString := dm1.TblPresmetkaGLOKACIIN_ID.AsString;
                 qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
                 qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                 qUpdateImaSlika.ParamByName('id').Value := DM1.TblPresmetkaGID.Value;
                 qUpdateImaSlika.ExecQuery;
                end;
           //     end;
         //    end;
          //    end ;

//             else
//             begin
//                 qUpdateImaSlika.Close;
//                 qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
//                 qUpdateImaSlika.ParamByName('lokacija').AsString := lokacija;
//                 qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                 qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
////                 qUpdateImaSlika.ParamByName('id').Value := DM1.TblPresmetkaGID.Value;
//                 qUpdateImaSlika.ExecQuery;
//
////              ima_slika := '0';
           //  end;
               i := i+1;
               DM1.TblPresmetkaG.Next;
           end;
             //  cel_zapis := '';
             //  DM1.TblPresmetkaG.Next;
             dm1.TblPresmetkaG.FullRefresh;
             end;
 //      end;

//
// end;
//
// end;
   if dm1.TblPresmetkaG.RecordCount > 0 then
    begin
      dmRes.Spremi(dmKon.aplikacija,35);
      dmKon.tblSqlReport.Params.ParamByName('godina').AsString := dm1.TblPresmetkaGGODINA.AsString;  // DM1.tblPecDukaniBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Params.ParamByName('mesec').AsString := dm1.TblPresmetkaGMESEC.AsString;
      dmKon.tblSqlReport.Params.ParamByName('tp').AsString := dm1.TblPresmetkaGTIP_PARTNER.AsString;
      dmKon.tblSqlReport.Params.ParamByName('reon_id').AsString := dm1.TblPresmetkaGREON_ID.AsString;
      dmKon.tblSqlReport.Open;
      dmres.frxReport1.Variables.AddVariable('Promenlivi','ima_slika','0');
      dmres.frxReport1.Variables.AddVariable('Promenlivi','erk',servis);

      dmRes.frxReport1.PrepareReport(true);
      dmRes.frxReport1.ShowPreparedReport;
  //   if servis = '0' then
  //    begin
      dm1.TblPresmetkaG.First;
      while not dm1.TblPresmetkaG.eof do
      begin
        qUpdateImaSlika.Close;
        qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
        qUpdateImaSlika.ParamByName('lokacija').Value := dm1.TblPresmetkaGLOKACIIN_ID.Value;
        qUpdateImaSlika.ParamByName('mesec').Value := dm1.TblPresmetkaGMESEC.Value;
        qUpdateImaSlika.ParamByName('godina').Value := dm1.TblPresmetkaGGODINA.Value;
        qUpdateImaSlika.ExecQuery;
        if FileExists('c:\cxCache\MyImage'+dm1.TblPresmetkaGLOKACIIN_ID.AsString+'.jpg') then
      begin
         DeleteFile('c:\cxCache\MyImage'+dm1.TblPresmetkaGLOKACIIN_ID.AsString+'.jpg');
      end;
        dm1.TblPresmetkaG.Next;
      end;

    //  end;
    end;



end;

procedure TFrmKomPresmetka.aCallFrfBeleskiPrintExecute(Sender: TObject);
begin
    frxReport1.DesignReport();
end;

procedure TFrmKomPresmetka.aEdnaFakturaExecute(Sender: TObject);
var uplateno:real;
begin
    uplateno := 0;//frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);
 //   if (frmDaNe.ShowModal <> mrYes) then
  //  begin
  //      dm1.TblSaldo.ParamByName('datum_od').AsDate:=(EndOfAMonth(dm1.TblPresmetkaGGODINA.Value,dm1.TblPresmetkaGMESEC.Value)+15);
 //       dm1.TblSaldo.Open;
 //   end
  //  else
  //  begin
//      dm1.TblSaldo.Close;
//      dm1.TblSaldo.ParamByName('datum_od').AsDate:=pravni_od;
//      dm1.TblSaldo.Open;
   // end;
    if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TFrmKomPresmetka.aIzbrisiLastPresmetkaExecute(Sender: TObject);
begin
    if ((DM1.TblPresmetkaG.Active = false) or (DM1.TblPresmetkaG.RecordCount = 0)) then
    begin
        ShowMessage('���� ��������� �� ������!');
        Abort();
    end;

    if (dmKon.user <> DM1.TblPresmetkaG.FieldByName('VRABOTEN').AsString) then
    begin
        ShowMessage('����������� � ��������� �� '+DM1.TblPresmetkaG.FieldByName('VRABOTEN').AsString+'!������ ����� �� �� ���������!');
        Abort();
    end;

    frmDaNe := TfrmDaNe.Create(self,'������ �� ��������','���� ��� ������� ���� ������ �� �� ��������� ���������?',1);
    if (frmDaNe.ShowModal = mrYes) then
    begin
        //DM1.QDeleteLastPresmetka.ParamByName('GODINA').AsString := lblGodinaLastValue.Caption;
        //DM1.QDeleteLastPresmetka.ParamByName('MESEC').AsString := lblMesecLastValue.Caption;

        DM1.QDeleteLastPresmetka.ParamByName('GODINA').AsString := cxComboBoxGodini.EditValue;
        DM1.QDeleteLastPresmetka.ParamByName('MESEC').AsString := cxComboBoxMesec.EditValue;
        DM1.QDeleteLastPresmetka.ParamByName('REON_ID').AsString := cxLookupCBReoni.EditValue;
        //ShowMessage(cxComboBoxGodini.EditValue+' '+cxComboBoxMesec.EditValue+' '+cxLookupCBReoni.EditValue);

        DM1.QDeleteLastPresmetka.ExecQuery();
        DM1.QDeleteLastPresmetka.Transaction.CommitRetaining();

       // DM1.TblPresmetkaG.CloseOpen(true);

        //getLastPresmetka();
    end;
end;

procedure TFrmKomPresmetka.aIzlezExecute(Sender: TObject);
begin
    Close();
end;

procedure TFrmKomPresmetka.aMailFakturaDizajnExecute(Sender: TObject);
begin
   if dm1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,32);
      //dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
      dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.TblPresmetkaGBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Open;
      dmRes.frxReport1.DesignReport();
    end;

end;

procedure TFrmKomPresmetka.aMailFakturaExecute(Sender: TObject);
var memStream: TMemoryStream;
status: TStatusWindowHandle;
begin
//    DM1.TblPresmetkaG.close;
//    TblPresmetkaG.SelectSQL.Text:=PresmetkaPoFaktura+' and kla.partner='+cxTxtPartnerID.Text+
//                                                      ' and kla.id_lokacija='+cxGrid1DBTableView1LOKACIJA.EditValue+
//                                                      ' and kpg.broj_faktura ='''+cxGrid1DBTableView1BROJ_FAKTURA.EditValue+
//                                                      ''' and kla.tip_partner = '+cxLookUpComboBoxTipPartner.Text;
//
//    TblPresmetkaG.open;
//    TblPresmetkaS.Open;
//    TblPresmetkaVodomeriSostojba.Open;
//
//   TblSaldo.Close;
  // frmDaNe := TfrmDaNe.Create(self,'����������','���� �� �� ������� ������� �� ����������?',1);

   if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

      dmRes.Spremi(dmKon.aplikacija,32);
      //dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
      dmKon.tblSqlReport.Params.ParamByName('broj').AsString:= DM1.TblPresmetkaGBROJ_FAKTURA.Value;;
      dmKon.tblSqlReport.Open;
      status := cxCreateStatusWindow();
     try
          memStream := TMemoryStream.Create;
          dmRes.frxPDFExport1.Stream := memStream;
          memStream.Position := 0;
          dmRes.frxReport1.PrepareReport(True);
          dmRes.frxReport1.Export(dmRes.frxPDFExport1);

      //     if dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([DM1.tblKarParTIP_PARTNER.AsString, DM1.tblKarParID.AsString]) , []) then
              dmRes.SendIndyEmail(dm1.TblPresmetkaGMAIL.Value, '������� �� ������� (' + dm1.TblPresmetkaGNAZIV.Value + ')', '������� �� �������', '', memStream, 'faktura');
            frxDSPresmetkaG.RangeBegin := rbFirst;
            frxDSPresmetkaG.RangeEnd := reLast;
        finally
          FreeAndNil(MemStream);
          dmRes.frxPDFExport1.Stream := nil;
          cxRemoveStatusWindow(status);
        end;

//   frmEFakturi := TfrmEFakturi.Create(Self);
//   // frmEFakturi.aEFakturi.Execute;
//   frmEFakturi.ShowModal;
//    frmEFakturi.Free;

    end;
end;

procedure TFrmKomPresmetka.aPrintAllBeleskiFizickiExecute(Sender: TObject);
begin
    if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxReport1.LoadFromFile('FRBeleskaFizickoLice.fr3');
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TFrmKomPresmetka.aPrintAllFakturiExecute(Sender: TObject);
var uplateno:real;
begin
    uplateno := 0;
    if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxReport1.LoadFromFile('FRFaktura.fr3');
        frxReport1.Variables.AddVariable('Promenlivi','PoslUplata',QuotedStr(floattostr(uplateno)));
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TFrmKomPresmetka.aPrintBeleskaOdberenFizickiExecute(Sender: TObject);
begin
    if DM1.TblPresmetkaG.RecordCount > 0 then
    begin
        frxDSPresmetkaG.RangeBegin := rbCurrent;
        frxDSPresmetkaG.RangeEnd := reCurrent;

        frxReport1.LoadFromFile('FRBeleskaFizickoLice.fr3');
        frxReport1.PrepareReport(true);
        frxReport1.ShowPreparedReport();

        frxDSPresmetkaG.RangeBegin := rbFirst;
        frxDSPresmetkaG.RangeEnd := reLast;
    end
    else
    begin
        ShowMessage('���� �������� �� ��������!');
    end;
end;

procedure TFrmKomPresmetka.cxBtnPresmetajClick(Sender: TObject);
var status: TStatusWindowHandle;
begin
   status := cxCreateStatusWindow();
   try

    if podatociOk then
    begin
        DM1.procKomPresmetka.close;
       if cxLookupCBReoni.Text='' then
          DM1.procKomPresmetka.ParamByName('REON_ID').AsString := '%'
        else
          DM1.procKomPresmetka.ParamByName('REON_ID').AsString := cxLookupCBReoni.text;
        DM1.procKomPresmetka.ParamByName('GODINA').AsString := cxComboBoxGodini.EditValue;
        DM1.procKomPresmetka.ParamByName('MESEC').AsString := cxComboBoxMesec.EditValue;
        DM1.procKomPresmetka.ParamByName('AKTIVEN').AsInteger := 1;
        DM1.procKomPresmetka.ParamByName('VRABOTEN').AsString := dmKon.user;
       // DM1.procKomPresmetka.ParamByName('LOKACIJA').AsString := '%';

        DM1.procKomPresmetka.Prepare;
        DM1.procKomPresmetka.ExecProc;

       // ShowMessage('������ ������� �������� ��  - '+DM1.procKomPresmetka.ParamByName('INSERTED').AsString+' �������');

        //getLastPresmetka();

        closePresmetkaDatasets();

        if (cxRadioGroup1.ItemIndex = 0) or (cxRadioGroup1.ItemIndex = -1) then
        begin
               DM1.TblPresmetkaG.ParamByName('TP').AsVariant := '%';

        end;
        if cxRadioGroup1.ItemIndex=1 then
               DM1.TblPresmetkaG.ParamByName('TP').AsVariant := '5';
        if cxRadioGroup1.ItemIndex=2 then
               DM1.TblPresmetkaG.ParamByName('TP').AsVariant := '6';
        if cxRadioGroup1.ItemIndex=3 then
               DM1.TblPresmetkaG.ParamByName('TP').AsVariant := '1';

        if cxLookupCBReoni.Text='' then
           DM1.TblPresmetkaG.ParamByName('REON_ID').AsVariant := '%'
        else
           DM1.TblPresmetkaG.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.Text;
      //  else
       //   begin
        //   if True then

         // end;
        DM1.TblPresmetkaG.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        DM1.TblPresmetkaG.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;

        //DM1.TblPresmetka1.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue;
        //DM1.TblPresmetka1.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        //DM1.TblPresmetka1.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;

        //DM1.TblPresmetka2.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue;
        //DM1.TblPresmetka2.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        //DM1.TblPresmetka2.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;

        DM1.TblPresmetkaG.Open;
        DM1.TblPresmetkaS.Open;
        DM1.TblPresmetkaVodomeriSostojba.Open;
        DM1.TblSaldo.Open;

        if (DM1.procKomPresmetka.ParamByName('INSERTED').AsInteger>0) then
               ShowMessage('����������� � ��������!')
        else
               ShowMessage('��� ��� ��������� ���������!');
        cxGrid1.SetFocus();
    end;
   finally
         cxRemoveStatusWindow(status);
    end;
end;

procedure TFrmKomPresmetka.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if DM1.TblPresmetkaG.Active then
    begin
        DM1.TblPresmetkaG.Close();
    end;

    Action := caFree;
end;

procedure TFrmKomPresmetka.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   case key of
        VK_RETURN:
            begin
            key:=0;
                PostMessage(Handle, WM_NEXTDLGCTL,0,0);
            end;

        end;
end;

procedure TFrmKomPresmetka.FormShow(Sender: TObject);
begin
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)));
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)-1));
    cxRadioGroup1.ItemIndex := 0;

  //  getLastPresmetka();
end;

procedure TFrmKomPresmetka.onKeyDownLookUpAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            begin
                PostMessage(Handle, WM_NEXTDLGCTL,0,0);
            end;

        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;
end;

function TFrmKomPresmetka.podatociOk():boolean;
var ret:boolean;
begin
    ret := false;
    if cxComboBoxGodini.Text = '' then
    begin
        cxComboBoxGodini.SetFocus();
        ShowMessage('������ �� �������� ������!');
    end
    else if cxComboBoxMesec.Text = '' then
    begin
        cxComboBoxMesec.SetFocus();
        ShowMessage('������ �� �������� �����!');
    end
    else if not(DM1.isCenovnikGenerated(cxComboBoxGodini.Text,cxComboBoxMesec.Text)) then
    begin
        ShowMessage('���������� �� ��� ������ �� � ���������!������ �� �� ���������� ������ ���� �� ����������');
    end
    else
    begin
        DM1.QGetNotGeneratedSostojbi.Close;
        DM1.QGetNotGeneratedSostojbi.ParamByName('AKTIVEN').AsShort := 1;
        DM1.QGetNotGeneratedSostojbi.ParamByName('STATUS').AsShort := 1;
        if cxLookupCBReoni.Text='' then
           DM1.QGetNotGeneratedSostojbi.ParamByName('REON_ID').AsString := '%'
         else
           DM1.QGetNotGeneratedSostojbi.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.Text;
        DM1.QGetNotGeneratedSostojbi.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        DM1.QGetNotGeneratedSostojbi.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
        DM1.QGetNotGeneratedSostojbi.ExecQuery;
        if (DM1.QGetNotGeneratedSostojbi.RecordCount > 0) then
        begin
            ret := false;
            ShowMessage('������� �������/�������� �� ��� �� �� ���������� �������! '+DM1.QGetNotGeneratedSostojbi.FieldByName('LOKACIJA').AsString+'/'+DM1.QGetNotGeneratedSostojbi.FieldByName('VODOMER').AsString+' ����� '+DM1.QGetNotGeneratedSostojbi.FieldByName('CITACKA_KNIGA').AsString+'!�� ������ �� ��������� ���������!');
        end
        else
        begin
          //  DM1.QGetNotGeneratedSostojbi.ParamByName('STATUS').AsShort := 1;
{            DM1.QGetNotVneseniSostojbi.ParamByName('AKTIVEN').AsShort := 1;
            DM1.QGetNotVneseniSostojbi.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue;
            DM1.QGetNotVneseniSostojbi.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
            DM1.QGetNotVneseniSostojbi.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
            DM1.QGetNotVneseniSostojbi.ExecQuery;  }
            DM1.tblGetNotVneseniSostojbi.close;
            DM1.tblGetNotVneseniSostojbi.ParamByName('AKTIVEN').AsShort := 1;
            if cxLookupCBReoni.Text='' then
               DM1.tblGetNotVneseniSostojbi.ParamByName('REON_ID').AsString := '%'
            else
               DM1.tblGetNotVneseniSostojbi.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.Text;
            DM1.tblGetNotVneseniSostojbi.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
            DM1.tblGetNotVneseniSostojbi.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
            DM1.tblGetNotVneseniSostojbi.Open;
            if (DM1.tblGetNotVneseniSostojbi.RecordCount > 0) then
            begin
//                frmDaNe := TfrmDaNe.Create(self,'��������� ���� �������','������� �������/�������� �� ��� �� �� ������� �������!'+DM1.QGetNotVneseniSostojbi.FieldByName('LOKACIJA').AsString+'/'+DM1.QGetNotVneseniSostojbi.FieldByName('VODOMER').AsString+' ���� ������� ������ �� ��������� ���������',1);
              //  frmDaNe := TfrmDaNe.Create(self,'��������� ���� �������','������� �������/�������� �� ��� �� �� ������� �������! ���� ������� ������ �� ��������� ���������',1);
              //  if (frmDaNe.ShowModal = mrYes) then
              //  begin
                      //
                      frmNevneseniSostojbi:=TfrmNevneseniSostojbi.Create(Application);
                      frmNevneseniSostojbi.ShowModal;
                      frmNevneseniSostojbi.Free;

                      frmDaNe := TfrmDaNe.Create(self,'��������� ���� �������','������� �������/�������� �� ��� �� �� ������� �������!���� ������� ������ �� ��������� ���������',1);
                     if (frmDaNe.ShowModal = mrYes) then
                     begin
                         ret := true;

                     end
               // end
                else
                {
                begin
                      ret := false;
                end;
                }
                end
            else
            begin
                ret := true;
            end;
        end;
    end;

    podatociOk := ret;
end;

procedure TFrmKomPresmetka.getLastPresmetka;
begin
    DM1.QGetLastPresmetkasGodinaMesec.ExecQuery();
    if DM1.QGetLastPresmetkasGodinaMesec.RecordCount > 0  then
    begin
        lblMesecLastValue.Caption :=  DM1.QGetLastPresmetkasGodinaMesec.FieldByName('MESEC').AsString;
        lblGodinaLastValue.Caption := DM1.QGetLastPresmetkasGodinaMesec.FieldByName('GODINA').AsString;
    end
    else
    begin
        lblMesecLastValue.Caption := '���� ���������';
        lblGodinaLastValue.Caption := '���� ���������';
    end;
end;

procedure TFrmKomPresmetka.closePresmetkaDatasets();
begin
    if DM1.TblPresmetkaG.Active then
    begin
        DM1.TblPresmetkaG.Close;
        DM1.TblPresmetkaS.Close;
        DM1.TblPresmetkaVodomeriSostojba.Close;
        dm1.tblsaldo.close;
    end;
end;

function TFrmKomPresmetka.CompressJpeg(OutJPG: TJPEGImage): Integer;
VAR tmpQStream: TMemoryStream;
begin
 tmpQStream:= TMemoryStream.Create;
 TRY
   OutJPG.Compress;
   OutJPG.SaveToStream(tmpQStream);
   OutJPG.SaveToFile('c:\cxCache\MyImage'+DM1.TblPresmetkaGLOKACIIN_ID.AsString+'.jpg');    // You can remove this line.
   tmpQStream.Position := 0;                //
   OutJPG.LoadFromStream(tmpQStream);       // Reload the jpeg stream to OutJPG
   Result:= tmpQStream.Size;
 FINALLY
   FreeAndNil(tmpQStream);
 END;
end;
end.
