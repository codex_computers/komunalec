inherited frmIzberiFaktura: TfrmIzberiFaktura
  Caption = #1048#1079#1073#1077#1088#1080' '#1060#1072#1082#1090#1091#1088#1072
  ClientHeight = 466
  ClientWidth = 711
  ExplicitWidth = 727
  ExplicitHeight = 504
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 711
    Height = 317
    ExplicitWidth = 711
    ExplicitHeight = 317
    inherited cxGrid1: TcxGrid
      Width = 707
      Height = 313
      ExplicitWidth = 707
      ExplicitHeight = 313
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dsIzberiFaktura
        OptionsView.NoDataToDisplayInfoText = ''
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1BROJ_FAKTURA: TcxGridDBColumn
          Caption = #1041#1088'.'#1060#1072#1082#1090#1091#1088#1072
          DataBinding.FieldName = 'BROJ_FAKTURA'
          HeaderAlignmentHorz = taCenter
          Width = 157
        end
        object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'LOKACIIN_ID'
          HeaderAlignmentHorz = taCenter
          Width = 59
        end
        object cxGrid1DBTableView1DATUM_PRESMETKA: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'DATUM_PRESMETKA'
          HeaderAlignmentHorz = taCenter
          Width = 104
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          HeaderAlignmentHorz = taCenter
          Width = 60
        end
        object cxGrid1DBTableView1MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
          HeaderAlignmentHorz = taCenter
          Width = 59
        end
        object cxGrid1DBTableView1IZNOS_VKUPNO: TcxGridDBColumn
          Caption = #1042#1082'.'#1048#1079#1085#1086#1089
          DataBinding.FieldName = 'IZNOS_VKUPNO'
          HeaderAlignmentHorz = taCenter
          Width = 58
        end
        object cxGrid1DBTableView1PLATENO: TcxGridDBColumn
          Caption = #1055#1083#1072#1090#1077#1085#1086
          DataBinding.FieldName = 'PLATENO'
          HeaderAlignmentHorz = taCenter
          Width = 60
        end
        object cxGrid1DBTableView1RAZLIKA: TcxGridDBColumn
          Caption = #1056#1072#1083#1080#1082#1072
          DataBinding.FieldName = 'RAZLIKA'
          HeaderAlignmentHorz = taCenter
          Width = 59
        end
      end
    end
  end
  inherited dPanel: TPanel
    Left = 692
    Width = 5
    Align = alNone
    ExplicitLeft = 692
    ExplicitWidth = 5
  end
  inherited dxRibbon1: TdxRibbon
    Width = 711
    ExplicitWidth = 711
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 443
    Width = 711
    Panels = <
      item
        Text = 'F7 - '#1054#1089#1074#1077#1078#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 450
      end>
    ExplicitTop = 443
    ExplicitWidth = 711
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 168
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 222
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 114
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40525.437388148150000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object dsIzberiFaktura: TDataSource
    DataSet = tblIzbriFaktura
    Left = 296
    Top = 91
  end
  object tblIzbriFaktura: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      ' g.broj_faktura,'
      ' g.lokaciin_id,'
      ' g.datum_presmetka,'
      ' g.godina,'
      ' g.mesec,'
      'cast(g.iznos_vkupno as integer) iznos_vkupno,'
      'sum(coalesce(ku.iznos,0)) plateno,'
      
        'sum(coalesce(ku.iznos,0))-cast(g.iznos_vkupno as integer) razlik' +
        'a'
      'from kom_presmetka_g g'
      
        'left outer join kom_uplati ku on g.godina=ku.godina and g.mesec=' +
        'ku.mesec and g.lokaciin_id=ku.lokaciin_id'
      
        'where (extract (year from g.datum_presmetka))>=(extract (year fr' +
        'om current_date))-3'
      
        'group by g.broj_faktura,g.lokaciin_id,g.datum_presmetka,g.godina' +
        ',g.mesec,g.iznos_vkupno'
      
        'having (sum(coalesce(ku.iznos,0))-cast(g.iznos_vkupno as integer' +
        '))<-2')
    SelectSQL.Strings = (
      'select'
      ' g.broj_faktura,'
      ' g.lokaciin_id,'
      ' g.datum_presmetka,'
      ' g.godina,'
      ' g.mesec,'
      'cast(g.iznos_vkupno as integer) iznos_vkupno,'
      'sum(coalesce(ku.iznos,0)) plateno,'
      
        'sum(coalesce(ku.iznos,0))-cast(g.iznos_vkupno as integer) razlik' +
        'a'
      'from kom_presmetka_g g'
      
        'left outer join kom_uplati ku on g.godina=ku.godina and g.mesec=' +
        'ku.mesec and g.lokaciin_id=ku.lokaciin_id and ku.flag=1'
      
        'where (extract (year from g.datum_presmetka))>=(extract (year fr' +
        'om current_date))-3'
      
        'group by g.broj_faktura,g.lokaciin_id,g.datum_presmetka,g.godina' +
        ',g.mesec,g.iznos_vkupno'
      
        'having (sum(coalesce(ku.iznos,0))-cast(g.iznos_vkupno as integer' +
        '))<-2'
      'order by g.godina, g.mesec')
    AutoUpdateOptions.UpdateTableName = 'MAT_PARTNER'
    AutoUpdateOptions.AutoReWriteSqls = True
    AllowedUpdateKinds = []
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 91
    oFetchAll = True
    object tblIzbriFakturaBROJ_FAKTURA: TFIBStringField
      FieldName = 'BROJ_FAKTURA'
      Size = 100
      EmptyStrToNull = True
    end
    object tblIzbriFakturaLOKACIIN_ID: TFIBIntegerField
      FieldName = 'LOKACIIN_ID'
    end
    object tblIzbriFakturaDATUM_PRESMETKA: TFIBDateTimeField
      FieldName = 'DATUM_PRESMETKA'
    end
    object tblIzbriFakturaGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblIzbriFakturaMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblIzbriFakturaIZNOS_VKUPNO: TFIBIntegerField
      FieldName = 'IZNOS_VKUPNO'
    end
    object tblIzbriFakturaPLATENO: TFIBBCDField
      FieldName = 'PLATENO'
      Size = 2
      RoundByScale = True
    end
    object tblIzbriFakturaRAZLIKA: TFIBBCDField
      FieldName = 'RAZLIKA'
      Size = 2
      RoundByScale = True
    end
  end
end
