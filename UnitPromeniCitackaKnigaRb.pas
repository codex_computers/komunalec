unit UnitPromeniCitackaKnigaRb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, dxSkinsCore, dxSkinsDefaultPainters, StdCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, ActnList, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinSilver, dxSkinStardust, dxSkinSummer2008, dxSkinValentine,
  dxSkinXmas2008Blue;

type
  TFrmPromeniCitackaKnigaRb = class(TForm)
    StatusBar1: TStatusBar;
    panelGlaven: TPanel;
    cxTxtCitackaKniga: TcxTextEdit;
    cxTxtRedenBrojOld: TcxTextEdit;
    lblRedenBrojOld: TLabel;
    lblCitackaKniga: TLabel;
    ActionList1: TActionList;
    aIzlez: TAction;
    lblCitackaKnigaRbNew: TLabel;
    cxTxtRedenBrojNew: TcxTextEdit;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure onTxtEnter(Sender: TObject);
    procedure onTxtExit(Sender: TObject);
    procedure onTxtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    var citackaKnigaOld, citackaKnigaRb, pred, sled, redenBrojOld :String;
  public
    { Public declarations }
  constructor Create(Owner : TComponent; citackaKnigaOld:String; citackaKnigaRbOld:String; pred:String; sled:String; redenBrojOld:String);reintroduce; overload;
  end;

var
  FrmPromeniCitackaKnigaRb: TFrmPromeniCitackaKnigaRb;

implementation

uses dmMaticni, UnitDM1;

{$R *.dfm}

procedure TFrmPromeniCitackaKnigaRb.aIzlezExecute(Sender: TObject);
begin
    close();
end;

constructor TFrmPromeniCitackaKnigaRb.Create(Owner : TComponent; citackaKnigaOld:String; citackaKnigaRbOld:String; pred:String; sled:String; redenBrojOld:String);
begin
    inherited Create(Owner);

    Self.citackaKnigaOld := citackaKnigaOld;
    Self.citackaKnigaRb := citackaKnigaRbOld;

    Self.pred := pred;
    Self.sled := sled;

    Self.redenBrojOld := redenBrojOld;
end;

procedure TFrmPromeniCitackaKnigaRb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    Action := caFree;
end;

procedure TFrmPromeniCitackaKnigaRb.FormShow(Sender: TObject);
begin
    cxTxtCitackaKniga.Text := citackaKnigaOld;
    cxTxtRedenBrojOld.Text := redenBrojOld;
end;

procedure TFrmPromeniCitackaKnigaRb.onTxtEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TFrmPromeniCitackaKnigaRb.onTxtExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TFrmPromeniCitackaKnigaRb.onTxtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key  of
        VK_RETURN:
        begin

            if cxTxtRedenBrojNew.Text = '' then
            begin
                ShowMessage('������ ������� �������� �� ��� ����� ���!');
                Abort();
            end;

            if cxTxtRedenBrojNew.Text = cxTxtRedenBrojOld.Text then
            begin
                ShowMessage('��������� ����� ��� � ��� �� �����������!');
                Abort();
            end;

            DM1.ProcKomUpdateCitackaKnigaRb.ParamByName('CITACKA_KNIGA_OLD').AsString := citackaKnigaOld;
            DM1.ProcKomUpdateCitackaKnigaRb.ParamByName('CITACKA_KNIGA_RB_OLD').AsString := citackaKnigaRb;
            DM1.ProcKomUpdateCitackaKnigaRb.ParamByName('CITACKA_KNIGA_NEW').AsString := citackaKnigaOld;
            DM1.ProcKomUpdateCitackaKnigaRb.ParamByName('CITACKA_KNIGA_RB_NEW').AsString := cxTxtRedenBrojNew.Text;


            DM1.ProcKomUpdateCitackaKnigaRb.Prepare();
            DM1.ProcKomUpdateCitackaKnigaRb.ExecProc();

            if DM1.ProcKomUpdateCitackaKnigaRb.ParamByName('UPDATED').AsInteger = 1 then
            begin
                ShowMessage('������� ��� � ������� ��������!');
                DM1.TblKomLokacii.CloseOpen(true);
                close();
            end
            else
            begin
                 ShowMessage('������� ��� �� � ��������! Mo�� ����� ������� ��������� ��� ��������� ��������');
            end;
        end;
    end;

end;

end.
