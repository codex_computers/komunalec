object frmOpomena: TfrmOpomena
  Left = 265
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1054#1087#1086#1084#1077#1085#1080
  ClientHeight = 492
  ClientWidth = 795
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object pcOpomeni: TPageControl
    Left = 0
    Top = 0
    Width = 795
    Height = 473
    ActivePage = tsPresmetkiOpomeni
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = pcOpomeniChange
    ExplicitTop = -6
    object tsOpomeni: TTabSheet
      Caption = #1054#1087#1086#1084#1077#1085#1080
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 787
        Height = 193
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object cxLabel1: TcxLabel
          Left = 20
          Top = 24
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1087#1086#1084#1077#1085#1072
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clRed
          Style.Font.Height = -12
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          Height = 20
          Width = 122
        end
        object txtBroj: TcxDBTextEdit
          Left = 133
          Top = 22
          DataBinding.DataField = 'BROJ'
          DataBinding.DataSource = DM1.dsOpomeni
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 1
          OnKeyDown = EnterKakoTab
          Width = 137
        end
        object cxButton1: TcxButton
          Left = 544
          Top = 164
          Width = 61
          Height = 20
          Action = aZapisi
          LookAndFeel.NativeStyle = True
          LookAndFeel.SkinName = 'Office2007Silver'
          TabOrder = 10
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cxButton2: TcxButton
          Left = 621
          Top = 164
          Width = 61
          Height = 20
          Action = aIzlez
          TabOrder = 11
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cxLabel4: TcxLabel
          Left = 478
          Top = 62
          AutoSize = False
          Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clBlue
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Height = 17
          Width = 81
        end
        object cxLabel5: TcxLabel
          Left = 470
          Top = 88
          AutoSize = False
          Caption = #1044#1086#1083#1075' '#1087#1086#1075#1086#1083#1077#1084' '#1086#1076' '
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clBlue
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Height = 17
          Width = 89
        end
        object cxLabel6: TcxLabel
          Left = 416
          Top = 109
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1085#1077#1087#1083#1072#1090#1077#1085#1080' '#1073#1077#1083#1077#1096#1082#1080
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clBlue
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Height = 17
          Width = 143
        end
        object txtDolg: TcxDBTextEdit
          Left = 577
          Top = 85
          DataBinding.DataField = 'DOLG'
          DataBinding.DataSource = DM1.dsOpomeni
          TabOrder = 8
          OnKeyDown = EnterKakoTab
          Width = 105
        end
        object cbTP: TcxDBLookupComboBox
          Left = 577
          Top = 61
          DataBinding.DataField = 'TIP_PARTNER'
          DataBinding.DataSource = DM1.dsOpomeni
          Properties.DropDownAutoSize = True
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'ID'
            end
            item
              FieldName = 'naziv'
            end>
          Properties.ListSource = dmMat.dsTipPartner
          TabOrder = 7
          OnKeyDown = EnterKakoTab
          Width = 105
        end
        object txtBeleski: TcxDBTextEdit
          Left = 577
          Top = 109
          DataBinding.DataField = 'BROJ_NEPLATENI_BEL'
          DataBinding.DataSource = DM1.dsOpomeni
          TabOrder = 9
          OnKeyDown = EnterKakoTab
          Width = 105
        end
        object cxLabel7: TcxLabel
          Left = 62
          Top = 86
          AutoSize = False
          Caption = #1054#1076' '#1084#1077#1089#1077#1094
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clRed
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          Height = 17
          Width = 65
        end
        object cxLabel9: TcxLabel
          Left = 273
          Top = 86
          AutoSize = False
          Caption = #1075#1086#1076#1080#1085#1072
          Height = 17
          Width = 42
        end
        object cxLabel8: TcxLabel
          Left = 62
          Top = 109
          AutoSize = False
          Caption = #1044#1086' '#1084#1077#1089#1077#1094
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clRed
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          Height = 17
          Width = 65
        end
        object cxLabel10: TcxLabel
          Left = 273
          Top = 109
          AutoSize = False
          Caption = #1075#1086#1076#1080#1085#1072
          Height = 17
          Width = 42
        end
        object cxLabel11: TcxLabel
          Left = 192
          Top = 86
          AutoSize = False
          Caption = '/'
          Height = 17
          Width = 12
        end
        object cxLabel12: TcxLabel
          Left = 193
          Top = 109
          AutoSize = False
          Caption = '/'
          Height = 17
          Width = 8
        end
        object cxComboBoxMesec: TcxDBComboBox
          Left = 132
          Top = 85
          DataBinding.DataField = 'M1'
          DataBinding.DataSource = DM1.dsOpomeni
          TabOrder = 3
          OnKeyDown = EnterKakoTab
          Width = 57
        end
        object cxComboBoxGodini: TcxDBComboBox
          Left = 201
          Top = 85
          DataBinding.DataField = 'G1'
          DataBinding.DataSource = DM1.dsOpomeni
          TabOrder = 4
          OnKeyDown = EnterKakoTab
          Width = 69
        end
        object cxComboBox1: TcxDBComboBox
          Left = 133
          Top = 109
          DataBinding.DataField = 'M2'
          DataBinding.DataSource = DM1.dsOpomeni
          TabOrder = 5
          OnKeyDown = EnterKakoTab
          Width = 57
        end
        object cxComboBox2: TcxDBComboBox
          Left = 201
          Top = 109
          DataBinding.DataField = 'G2'
          DataBinding.DataSource = DM1.dsOpomeni
          TabOrder = 6
          OnKeyDown = EnterKakoTab
          Width = 69
        end
        object cxLabel2: TcxLabel
          Left = 93
          Top = 65
          AutoSize = False
          Caption = #1056#1077#1086#1085
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clBlue
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Height = 17
          Width = 34
        end
        object cxComboBoxReon: TcxDBLookupComboBox
          Left = 133
          Top = 61
          DataBinding.DataField = 'REON'
          DataBinding.DataSource = DM1.dsOpomeni
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'ID'
            end>
          Properties.ListSource = DM1.DSKomReoni
          TabOrder = 2
          OnKeyDown = EnterKakoTab
          Width = 137
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 193
        Width = 787
        Height = 252
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 787
          Height = 252
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = DM1.dsOpomeni
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '###,##0.00'
                Kind = skSum
                FieldName = 'DOLG'
                Column = cxGrid1DBTableView1DOLG
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1087#1086#1084#1077#1085#1072
              DataBinding.FieldName = 'BROJ'
              HeaderAlignmentHorz = taCenter
              Width = 84
            end
            object cxGrid1DBTableViewREON: TcxGridDBColumn
              Caption = #1056#1077#1086#1085
              DataBinding.FieldName = 'REON'
              HeaderAlignmentHorz = taCenter
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077
              DataBinding.FieldName = 'DATUM'
              HeaderAlignmentHorz = taCenter
              Width = 122
            end
            object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1086#1076
              DataBinding.FieldName = 'DATUM_OD'
              HeaderAlignmentHorz = taCenter
              Width = 75
            end
            object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084'  '#1076#1086
              DataBinding.FieldName = 'DATUM_DO'
              HeaderAlignmentHorz = taCenter
              Width = 92
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'TIP_PARTNER'
              HeaderAlignmentHorz = taCenter
              Width = 71
            end
            object cxGrid1DBTableView1DOLG: TcxGridDBColumn
              Caption = #1044#1086#1083#1075
              DataBinding.FieldName = 'DOLG'
              HeaderAlignmentHorz = taCenter
              Width = 112
            end
            object cxGrid1DBTableView1BROJ_NEPLATENI_BEL: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1085#1077#1087#1083#1072#1090#1077#1085#1080' '#1073#1077#1083#1077#1096#1082#1080
              DataBinding.FieldName = 'BROJ_NEPLATENI_BEL'
              HeaderAlignmentHorz = taCenter
              Width = 113
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object tsPresmetkiOpomeni: TTabSheet
      Caption = #1055#1088#1077#1089#1084#1077#1090#1082#1080
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 787
        Height = 445
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = DM1.dsPrOpomeni
          DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '###,##0.00'
              Kind = skSum
              FieldName = 'DOLZEN'
              Column = cxGridDBTableView1DOLZEN
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.Footer = True
          object cxGridDBTableView1BROJ: TcxGridDBColumn
            Caption = #1041#1088'.'#1054#1087#1086#1084#1077#1085#1072
            DataBinding.FieldName = 'BROJ'
            HeaderAlignmentHorz = taCenter
            Width = 34
          end
          object cxGridDBTableView1TIP_PARTNER: TcxGridDBColumn
            Caption = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
            DataBinding.FieldName = 'TIP_PARTNER'
            HeaderAlignmentHorz = taCenter
            Width = 36
          end
          object cxGridDBTableView1NAZIV_TP: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV_TP'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
          object cxGridDBTableView1DOLG: TcxGridDBColumn
            Caption = #1044#1086#1083#1075
            DataBinding.FieldName = 'DOLG'
            HeaderAlignmentHorz = taCenter
            Width = 39
          end
          object cxGridDBTableView1BROJ_NEPLATENI_BEL: TcxGridDBColumn
            Caption = #1041#1088'.'#1053#1077#1087#1083'.'#1041#1077#1083#1077#1096#1082#1080
            DataBinding.FieldName = 'BROJ_NEPLATENI_BEL'
            HeaderAlignmentHorz = taCenter
            Width = 34
          end
          object cxGridDBTableView1REON_ID: TcxGridDBColumn
            Caption = #1056#1077#1086#1085
            DataBinding.FieldName = 'REON_ID'
            HeaderAlignmentHorz = taCenter
            Width = 26
          end
          object cxGridDBTableView1ID: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072
            DataBinding.FieldName = 'ID'
            HeaderAlignmentHorz = taCenter
            Width = 68
          end
          object cxGridDBTableView1NAZIV: TcxGridDBColumn
            Caption = #1053#1072#1079#1080#1074
            DataBinding.FieldName = 'NAZIV'
            HeaderAlignmentHorz = taCenter
            Width = 134
          end
          object cxGridDBTableView1ADRESA: TcxGridDBColumn
            Caption = #1040#1076#1088#1077#1089#1072
            DataBinding.FieldName = 'ADRESA'
            HeaderAlignmentHorz = taCenter
            Width = 71
          end
          object cxGridDBTableView1DATUM_OD: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1086#1076
            DataBinding.FieldName = 'DATUM_OD'
            HeaderAlignmentHorz = taCenter
            Width = 35
          end
          object cxGridDBTableView1DATUM_DO: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1076#1086
            DataBinding.FieldName = 'DATUM_DO'
            HeaderAlignmentHorz = taCenter
            Width = 37
          end
          object cxGridDBTableView1LOKACIIN_ID: TcxGridDBColumn
            Caption = #1051#1086#1082#1072#1094#1080#1112#1072
            DataBinding.FieldName = 'LOKACIIN_ID'
            HeaderAlignmentHorz = taCenter
            Width = 34
          end
          object cxGridDBTableView1DOLZEN: TcxGridDBColumn
            Caption = #1044#1086#1083#1078#1080
            DataBinding.FieldName = 'DOLZEN'
            HeaderAlignmentHorz = taCenter
            Width = 64
          end
          object cxGridDBTableView1STARO_SLADO: TcxGridDBColumn
            Caption = #1057#1090#1072#1088#1086' '#1089#1072#1083#1076#1086
            DataBinding.FieldName = 'STARO_SLADO'
            HeaderAlignmentHorz = taCenter
            Width = 56
          end
          object cxGridDBTableView1Column1: TcxGridDBColumn
            Caption = #1042#1086#1076#1086#1084#1077#1088
            DataBinding.FieldName = 'VODOMER'
            HeaderAlignmentHorz = taCenter
            Width = 61
          end
          object cxGridDBTableView1Column2: TcxGridDBColumn
            Caption = #1057#1086#1089#1090#1086#1112#1073#1072
            DataBinding.FieldName = 'SOSTOJBA'
            HeaderAlignmentHorz = taCenter
            Width = 56
          end
          object cxGridDBTableView1Column3: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN'
            Visible = False
            HeaderAlignmentHorz = taCenter
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 473
    Width = 795
    Height = 19
    Panels = <
      item
        Text = 
          'F5 - '#1053#1086#1074#1072' '#1086#1087#1086#1084#1077#1085#1072', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112' '#1086#1087#1086#1084#1077#1085#1072', F8 - '#1041#1088#1080#1096#1080' '#1086#1087#1086#1084#1077#1085#1072', F9' +
          ' - '#1057#1085#1080#1084#1080' '#1074#1086' Excel, F10 - '#1055#1077#1095#1072#1090#1080',  Esc - '#1054#1090#1082#1072#1078#1080'/'#1048#1079#1083#1077#1079' '
        Width = 50
      end>
  end
  object ActionList1: TActionList
    Left = 144
    object aNov: TAction
      Caption = 'aNov'
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = 'aAzuriraj'
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = 'aBrisi'
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      OnExecute = aZapisiExecute
    end
    object aIzlez: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      OnExecute = aIzlezExecute
    end
    object aPecati: TAction
      Caption = 'aPecati'
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aSnimiVoExcel: TAction
      Caption = 'aSnimiVoExcel'
      ShortCut = 120
      OnExecute = aSnimiVoExcelExecute
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 528
    Top = 272
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 616
    Top = 264
  end
  object qBrisiOPPresmetka: TpFIBQuery
    Transaction = DM1.TransTest
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_presmetka_g g'
      'set g.opomena_broj=null'
      'where g.opomena_broj =:opomena')
    Left = 184
    Top = 2
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qImaOpomena: TpFIBQuery
    Transaction = DM1.TransTest
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(count(*),0) ima'
      'from kom_opomena o'
      
        'where ((o.datum_od between :d1 and :d2) or (o.datum_do between :' +
        'd1 and :d2))'
      'and (o.reon like :reon) and (o.tip_partner like :tp)')
    Left = 232
    Top = 2
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblNeplateniPresmetki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE KOM_OPOMENA'
      'SET '
      ' BROJ = :BROJ,'
      ' DATUM = :DATUM,'
      ' DATUM_OD = :DATUM_OD,'
      ' DATUM_DO = :DATUM_DO,'
      ' TIP_PARTNER = :TIP_PARTNER,'
      ' DOLG = :DOLG,'
      ' BROJ_NEPLATENI_BEL = :BROJ_NEPLATENI_BEL,'
      ' REON = :REON'
      'WHERE'
      ' BROJ = :OLD_BROJ'
      ' ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      ' KOM_OPOMENA'
      'WHERE'
      '  BROJ = :OLD_BROJ'
      ' ')
    InsertSQL.Strings = (
      'INSERT INTO KOM_OPOMENA('
      ' BROJ,'
      ' DATUM,'
      ' DATUM_OD,'
      ' DATUM_DO,'
      ' TIP_PARTNER,'
      ' DOLG,'
      ' BROJ_NEPLATENI_BEL,'
      ' REON'
      ')'
      'VALUES('
      ' :BROJ,'
      ' :DATUM,'
      ' :DATUM_OD,'
      ' :DATUM_DO,'
      ' :TIP_PARTNER,'
      ' :DOLG,'
      ' :BROJ_NEPLATENI_BEL,'
      ' :REON'
      ')')
    RefreshSQL.Strings = (
      'select l.id'
      '    from kom_presmetka_g g'
      '    inner join kom_lokacii l on l.id=g.lokaciin_id'
      '    where not exists (select 1 from kom_uplati u'
      '                     where u.presmetka_g_id=g.id)'
      
        '    and firstdaymonth(cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as date' +
        ')) between :d1 and :d2'
      
        '    and l.tip_partner like :tp and g.tip=1 and l.reon_id like :r' +
        'eon'
      '    group by l.id'
      '    having sum(g.iznos_vkupno)>=:suma and count(g.id)>=:broj')
    SelectSQL.Strings = (
      'select l.id'
      '    from kom_presmetka_g g'
      '    inner join kom_lokacii l on l.id=g.lokaciin_id'
      '    where not exists (select 1 from kom_uplati u'
      '                     where u.presmetka_g_id=g.id)'
      
        '    and firstdaymonth(cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as date' +
        ')) between :d1 and :d2'
      
        '    and l.tip_partner like :tp and g.tip=1 and l.reon_id like :r' +
        'eon'
      '    group by l.id'
      '    having sum(g.iznos_vkupno)>=:suma and count(g.id)>=:broj')
    AutoUpdateOptions.UpdateTableName = 'KOM_DOPOLNITELNA_USLUGA'
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 272
    Top = 25
    oFetchAll = True
    object tblNeplateniPresmetkiID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object dsNeplateniBeleski: TDataSource
    DataSet = tblNeplateniPresmetki
    Left = 312
    Top = 25
  end
  object qUpdatePresmetka: TpFIBQuery
    Transaction = DM1.TransTest
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update kom_presmetka_g g'
      '    set g.opomena_broj=:opomena'
      
        '    where g.lokaciin_id=:id and not exists (select 1 from kom_up' +
        'lati u'
      '                  where u.presmetka_g_id=g.id)'
      
        '    and firstdaymonth(cast('#39'01.'#39'||g.mesec||'#39'.'#39'||g.godina as date' +
        ')) between :d1 and :d2'
      '    and g.tip=1 ;')
    Left = 360
    Top = 66
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
