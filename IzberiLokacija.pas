unit IzberiLokacija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ComCtrls, ExtCtrls, ActnList,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, System.Actions;

type
  TfrmIzberiLokacija = class(TForm)
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ULICA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_NA_CLENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1STANBENA_POVRSINA: TcxGridDBColumn;
    cxGrid1DBTableView1DVORNA_POVRSINA: TcxGridDBColumn;
    cxGrid1DBTableView1PRED: TcxGridDBColumn;
    cxGrid1DBTableView1SLED: TcxGridDBColumn;
    cxGrid1DBTableView1KOREN: TcxGridDBColumn;
    cxGrid1DBTableView1RB1: TcxGridDBColumn;
    cxGrid1DBTableView1RB2: TcxGridDBColumn;
    cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGrid1DBTableView1CITACKA_KNIGA_RB: TcxGridDBColumn;
    ActionList1: TActionList;
    aIzberi: TAction;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzberiLokacija: TfrmIzberiLokacija;

implementation

uses dmKonekcija, dmMaticni, UnitDM1;

{$R *.dfm}

procedure TfrmIzberiLokacija.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
     Close;
end;

procedure TfrmIzberiLokacija.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if (key=VK_RETURN)then
     begin
       tag:=0;
       Close;
     end
     else
     if (key=VK_ESCAPE)then
     begin
        tag:=1;
        Close;
     end;
end;

end.
