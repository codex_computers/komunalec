object frmDULista: TfrmDULista
  Left = 0
  Top = 0
  Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1091#1089#1083#1091#1075#1080
  ClientHeight = 596
  ClientWidth = 1085
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1085
    Height = 577
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 571
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = aDopUslExecute
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DM1.dsDULista
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'IZNOS_USLUGA'
          Column = cxGrid1DBTableView1IZNOS_USLUGA
        end>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      FilterRow.ApplyChanges = fracImmediately
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      object cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn
        Caption = #1051#1086#1082#1072#1094#1080#1112#1072
        DataBinding.FieldName = 'LOKACIIN_ID'
        HeaderAlignmentHorz = taCenter
        Width = 73
      end
      object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'TIP_PARTNER'
        HeaderAlignmentHorz = taCenter
        Width = 55
      end
      object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
        Caption = #1064#1080#1092#1088#1072
        DataBinding.FieldName = 'PARTNER'
        HeaderAlignmentHorz = taCenter
        Width = 80
      end
      object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
        Caption = #1053#1072#1079#1080#1074
        DataBinding.FieldName = 'NAZIV'
        HeaderAlignmentHorz = taCenter
        Width = 190
      end
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1044#1059
        DataBinding.FieldName = 'ID'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Width = 42
      end
      object cxGrid1DBTableView1BR_RATA: TcxGridDBColumn
        DataBinding.FieldName = 'BR_RATA'
        Visible = False
        HeaderAlignmentHorz = taCenter
      end
      object cxGrid1DBTableView1GODINA: TcxGridDBColumn
        Caption = #1043#1086#1076#1080#1085#1072
        DataBinding.FieldName = 'GODINA'
        HeaderAlignmentHorz = taCenter
        Width = 64
      end
      object cxGrid1DBTableView1MESEC: TcxGridDBColumn
        Caption = #1052#1077#1089#1077#1094
        DataBinding.FieldName = 'MESEC'
        HeaderAlignmentHorz = taCenter
        Width = 74
      end
      object cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1085#1072' '#1091#1089#1083#1091#1075#1072
        DataBinding.FieldName = 'VID_USLUGA'
        HeaderAlignmentHorz = taCenter
        Width = 42
      end
      object cxGrid1DBTableView1USLUGA: TcxGridDBColumn
        Caption = #1059#1089#1083#1091#1075#1072
        DataBinding.FieldName = 'USLUGA'
        HeaderAlignmentHorz = taCenter
        Width = 75
      end
      object cxGrid1DBTableView1VK_RATI: TcxGridDBColumn
        Caption = #1042#1082'.'#1088#1072#1090#1080
        DataBinding.FieldName = 'VK_RATI'
        HeaderAlignmentHorz = taCenter
        Width = 72
      end
      object cxGrid1DBTableView1VO_PRESMETKA: TcxGridDBColumn
        DataBinding.FieldName = 'VO_PRESMETKA'
        Visible = False
        HeaderAlignmentHorz = taCenter
      end
      object cxGrid1DBTableView1IZNOS_USLUGA_NETO: TcxGridDBColumn
        Caption = #1048#1079#1085#1086#1089' '#1085#1077#1090#1086
        DataBinding.FieldName = 'IZNOS_USLUGA_NETO'
        HeaderAlignmentHorz = taCenter
        Width = 74
      end
      object cxGrid1DBTableView1IZNOS_USLUGA: TcxGridDBColumn
        Caption = #1048#1079#1085#1086#1089
        DataBinding.FieldName = 'IZNOS_USLUGA'
        HeaderAlignmentHorz = taCenter
        Width = 72
      end
      object cxGrid1DBTableView1DATUM: TcxGridDBColumn
        Caption = #1044#1072#1090#1091#1084
        DataBinding.FieldName = 'DATUM'
        HeaderAlignmentHorz = taCenter
        Width = 127
      end
      object cxGrid1DBTableView1DANOK: TcxGridDBColumn
        Caption = #1044#1072#1085#1086#1082
        DataBinding.FieldName = 'DANOK'
        HeaderAlignmentHorz = taCenter
        Width = 73
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 577
    Width = 1085
    Height = 19
    Panels = <
      item
        Text = 
          'Enter - '#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1072' '#1091#1089#1083#1091#1075#1072',  F5 - '#1053#1086#1074#1072' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1072' '#1091#1089#1083#1091#1075#1072', Esc' +
          ' - '#1048#1047#1083#1077#1079
        Width = 50
      end>
    ExplicitTop = 471
    ExplicitWidth = 872
  end
  object ActionList1: TActionList
    Left = 712
    Top = 88
    object aDopUsl: TAction
      Caption = 'aDopUsl'
      ShortCut = 13
      OnExecute = aDopUslExecute
    end
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aNov: TAction
      Caption = 'aNov'
      ShortCut = 116
      OnExecute = aNovExecute
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 720
    Top = 248
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 744
    Top = 152
    PixelsPerInch = 96
  end
end
