unit UnitCenovnik;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ComCtrls, ExtCtrls, cxGraphics, dxSkinsCore,
  dxSkinsDefaultPainters, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, DateUtils, Menus, cxLookAndFeelPainters, cxButtons,
  dxSkinscxPCPainter, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxDBEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxLookAndFeels, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, System.Actions;

type
  TFrmCenovnik = class(TForm)
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aIzlez: TAction;
    panelGore: TPanel;
    cxComboBoxMesec: TcxComboBox;
    lblMesec: TLabel;
    lblGodina: TLabel;
    cxComboBoxGodini: TcxComboBox;
    cxButton1: TcxButton;
    panelDole: TPanel;
    panelGrid: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1VID_USLUGA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1DANOK: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    panelControls: TPanel;
    lblCena: TLabel;
    cxControlCENA: TcxDBTextEdit;
    aPromeni: TAction;
    lblDanok: TLabel;
    cxControlDANOK: TcxDBTextEdit;
    cxLookUpNACIN_PRESMETKA: TcxDBLookupComboBox;
    lblNacinPresmetka: TLabel;
    lblMesecLastValue: TLabel;
    lblMesecLast: TLabel;
    lblGodinaLastValue: TLabel;
    lblGodinaLast: TLabel;
    cxGrid1DBTableView1NACIN_PRESMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1PAUSAL: TcxGridDBColumn;
    lblPausal: TLabel;
    cxControlPAUSAL: TcxDBTextEdit;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure generirajParametri();
    function podatociOK():boolean;
    procedure cxButton1Click(Sender: TObject);
    procedure onEnterAll(Sender: TObject);
    procedure onExitAll(Sender: TObject);
    procedure enablePanels(enable:Boolean);
    procedure aPromeniExecute(Sender: TObject);
    procedure onKeyDownAll(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure enableGridControls(enable:Boolean);
    procedure getLastVnesenCenovnik();
    procedure setNextCenovnik();
  private
    { Private declarations }
    var gridFilled:boolean;
  public
    { Public declarations }
  end;

var
  FrmCenovnik: TFrmCenovnik;

implementation

uses UnitDM1, dmKonekcija,// dmLook,
 dmMaticni;

{$R *.dfm}

procedure TFrmCenovnik.aIzlezExecute(Sender: TObject);
begin
    if gridFilled then
    begin
        if DM1.TblKomCenovnik.State = dsEdit then
        begin
            DM1.TblKomCenovnik.Cancel();
            enableGridControls(false);
            cxGrid1.SetFocus();
        end
        else
        begin
            DM1.TblKomCenovnik.Close();
            DM1.TblKomNacinPresmetka.Close();
            //cxComboBoxMesec.ItemIndex := -1;
            //cxComboBoxGodini.ItemIndex := -1;
            enablePanels(false);
            gridFilled := false;
            cxComboBoxGodini.SetFocus();
        end;
    end
    else
    begin
        Close();
    end;
end;

procedure TFrmCenovnik.aPromeniExecute(Sender: TObject);
begin
     if DM1.TblKomCenovnik.RecordCount > 0 then
     begin
          DM1.TblKomCenovnik.Edit();
          enableGridControls(true);
          cxControlCENA.SetFocus();
          cxControlCENA.SelectAll();
     end
     else
     begin
          ShowMessage('���� �������� �� ���������!');
     end;
     
end;

procedure TFrmCenovnik.cxButton1Click(Sender: TObject);
begin
    generirajParametri();
end;

procedure TFrmCenovnik.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if DM1.TblKomCenovnik.Active then
    begin
        DM1.TblKomCenovnik.Close();
    end;
    if DM1.TblKomNacinPresmetka.Active then
    begin
        DM1.TblKomNacinPresmetka.Close();
    end;

    Action := caFree;
end;

procedure TFrmCenovnik.FormShow(Sender: TObject);
begin
    if DM1.TblKomCenovnik.Active then
    begin
        DM1.TblKomCenovnik.Close();
    end;

    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)));
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)-1));
    getLastVnesenCenovnik();
    {
    if ( (lblMesecLastValue.Caption<>'') and (lblGodinaLastValue.Caption<>'')) then
    begin
         setNextCenovnik();
    end;
    }
    gridFilled := false;
    panelControls.Enabled := false;
end;

procedure TFrmCenovnik.onEnterAll(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TFrmCenovnik.onExitAll(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TFrmCenovnik.onKeyDownAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
             Key := 0;
             if TEdit(sender).Name = 'cxControlDANOK' then
             begin
                  DM1.TblKomCenovnik.Post();
                  enableGridControls(false);
                  cxGrid1.SetFocus();
             end
             else
             begin
                 PostMessage(Handle,WM_NextDlgCtl,0,0);
             end;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
    end;

end;

procedure TFrmCenovnik.onKeyDownLookUpAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            begin
                PostMessage(Handle, WM_NEXTDLGCTL,0,0);
            end;

        end;
        VK_UP:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,1,0);
            Key := 0;
        end;
    end;
end;

procedure TFrmCenovnik.generirajParametri();
begin
    if podatociOK then
    begin
        DM1.ProcKomGenerirajParametriCen.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        DM1.ProcKomGenerirajParametriCen.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
        DM1.ProcKomGenerirajParametriCen.ExecProc();

        DM1.TblKomCenovnik.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
        DM1.TblKomCenovnik.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
        DM1.TblKomCenovnik.Open();
        DM1.TblKomNacinPresmetka.Open();
        getLastVnesenCenovnik();
        //setNextCenovnik();
        gridFilled := true;
        enablePanels(true);
        cxGrid1.SetFocus();
    end;
end;

function TFrmCenovnik.podatociOk():boolean;
var ret:boolean;
begin
    ret := false;
    if cxComboBoxGodini.Text = '' then
    begin
        cxComboBoxGodini.SetFocus();
        ShowMessage('������ �� �������� ������!');
    end
    else if cxComboBoxMesec.Text = '' then
    begin
        cxComboBoxMesec.SetFocus();
        ShowMessage('������ �� �������� �����!');
    end
    else
    begin
        ret := true;
    end;

    podatociOK := ret;
end;

procedure TFrmCenovnik.enablePanels(enable:Boolean);
begin
    panelGore.Enabled := not enable;
    //panelDole.Enabled := enable;
end;

procedure TFrmCenovnik.enableGridControls(enable:Boolean);
begin
    cxGrid1.Enabled := not enable;
    panelControls.Enabled := enable;
end;

procedure TFrmCenovnik.getLastVnesenCenovnik;
begin
    DM1.QGetLastVnesenCenovnik.ExecQuery();
    if DM1.QGetLastVnesenCenovnik.RecordCount > 0 then
    begin
         lblMesecLastValue.Caption := DM1.QGetLastVnesenCenovnik.FieldByName('MESEC').AsString;
         lblGodinaLastValue.Caption := DM1.QGetLastVnesenCenovnik.FieldByName('GODINA').AsString;

         setNextCenovnik();
    end
    else
    begin
         lblMesecLastValue.Caption := '';
         lblGodinaLastValue.Caption := '';
    end;
end;

procedure TFrmCenovnik.setNextCenovnik;
var godina, mesec:integer;
begin
    godina := StrToInt(lblGodinaLastValue.Caption);
    mesec := StrToInt(lblMesecLastValue.Caption);
    if mesec = 12 then
    begin
        cxComboBoxGodini.EditValue := (godina+1);
        cxComboBoxMesec.EditValue := (1);
    end
    else
    begin
        cxComboBoxGodini.EditValue := (godina);
        cxComboBoxMesec.EditValue := (mesec+1);
    end
end;

end.
