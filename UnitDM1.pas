unit UnitDM1;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery,Dialogs
  ,cxDBData,StdCtrls,Graphics, ActnList, pFIBStoredProc, FIBDatabase,
  pFIBDatabase, cxStyles,Variants,Menus,Messages,cxButtons,ExtCtrls,
  fs_iinterpreter, pFIBScripter, frxClass, frxDBSet, frxCross, frxDesgn,
  ImgList, Controls, cxGraphics, cxClasses, WinInet, Windows, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, cxdbedit, cxDBLookupComboBox,
  System.ImageList, cxImageList, Vcl.Imaging.jpeg;

type
  TDM1 = class(TDataModule)
    TblKomLokacii: TpFIBDataSet;
    TblKomLokaciiCITACKA_KNIGA: TFIBIntegerField;
    TblKomLokaciiCITACKA_KNIGA_RB: TFIBIntegerField;
    TblKomLokaciiID: TFIBIntegerField;
    TblKomLokaciiPARTNER_ID: TFIBIntegerField;
    TblKomLokaciiTIP_PARTNER: TFIBIntegerField;
    TblKomLokaciiAKTIVEN: TFIBSmallIntField;
    TblKomLokaciiREON_ID: TFIBIntegerField;
    TblKomLokaciiULICA_ID: TFIBIntegerField;
    TblKomLokaciiBROJ: TFIBStringField;
    TblKomLokaciiBROJ_NA_CLENOVI: TFIBIntegerField;
    TblKomLokaciiSTANBENA_POVRSINA: TFIBIntegerField;
    TblKomLokaciiDVORNA_POVRSINA: TFIBIntegerField;
    QGetPartner: TpFIBQuery;
    DSKomLokacii: TDataSource;
    TblKomReoni: TpFIBDataSet;
    TblKomReoniID: TFIBIntegerField;
    TblKomReoniNAZIV: TFIBStringField;
    DSKomReoni: TDataSource;
    TblKomUlici: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    TblKomUliciREON_ID: TFIBIntegerField;
    DSKomUlici: TDataSource;
    TblKomVodomeri: TpFIBDataSet;
    DSKomVodomeri: TDataSource;
    TblKomVodomeriLOKACIIN_ID: TFIBIntegerField;
    TblKomVodomeriID: TFIBIntegerField;
    TblKomVodomeriSTATUS: TFIBSmallIntField;
    ProcKomUpdateRedBr: TpFIBStoredProc;
    TblKomLokaciiPRED: TFIBIntegerField;
    TblKomLokaciiSLED: TFIBIntegerField;
    TransProcs: TpFIBTransaction;
    TblKomVodomeriSostojba: TpFIBDataSet;
    TblCitackiKnigiPoReon: TpFIBDataSet;
    TblCitackiKnigiPoReonCITACKA_KNIGA: TFIBIntegerField;
    DSCitackiKnigiPoReon: TDataSource;
    ProcKomPopolniVodomeriSostojba: TpFIBStoredProc;
    DSKomVodomeriSostojba: TDataSource;
    TblKomVidoviUslugi: TpFIBDataSet;
    TblKomVidoviUslugiID: TFIBIntegerField;
    TblKomVidoviUslugiNAZIV: TFIBStringField;
    DSKomVidoviUslugi: TDataSource;
    TblKomUslugi: TpFIBDataSet;
    DSKomUslugi: TDataSource;
    TblKomUslugiID: TFIBIntegerField;
    TblKomUslugiVID_USLUGA: TFIBIntegerField;
    TblKomUslugiNAZIV: TFIBStringField;
    cxStyleRepository1: TcxStyleRepository;
    cxStyleAktivni: TcxStyle;
    cxStyleNeAktivni: TcxStyle;
    TblKomVodomeriPAUSAL: TFIBSmallIntField;
    TblKomCenovnik: TpFIBDataSet;
    DSKomCenovnik: TDataSource;
    ProcKomGenerirajParametriCen: TpFIBStoredProc;
    TblKomCenovnikUSLUGA: TFIBIntegerField;
    TblKomCenovnikVID_USLUGA: TFIBIntegerField;
    TblKomCenovnikGODINA: TFIBIntegerField;
    TblKomCenovnikMESEC: TFIBIntegerField;
    TblKomCenovnikCENA: TFIBBCDField;
    TblKomCenovnikDANOK: TFIBIntegerField;
    TblKomCenovnikNAZIV: TFIBStringField;
    QGetFirstNotSetSostojba: TpFIBQuery;
    QTest: TpFIBQuery;
    TransTest: TpFIBTransaction;
    QTest1: TpFIBQuery;
    QTest2: TpFIBQuery;
    TblKomNacinPresmetka: TpFIBDataSet;
    TblKomCenovnikNACIN_PRESMETKA: TFIBSmallIntField;
    DSKomNacinPresmetka: TDataSource;
    ProcKomUpdateCitackaKnigaRb: TpFIBStoredProc;
    TblKomLokacijaUslugi: TpFIBDataSet;
    DSKomLokacijaUslugi: TDataSource;
    TblKomLokacijaUslugiLOKACIIN_ID: TFIBIntegerField;
    TblKomLokacijaUslugiVID_USLUGA: TFIBIntegerField;
    TblKomLokacijaUslugiUSLUGA: TFIBIntegerField;
    procKomPresmetka: TpFIBStoredProc;
    QGetLastPresmetkasGodinaMesec: TpFIBQuery;
    QDeleteLastPresmetka: TpFIBQuery;
    QGetLastVnesenCenovnik: TpFIBQuery;
    TblPresmetkaG: TpFIBDataSet;
    DSPresmetka: TDataSource;
    TblPresmetkaGTIP_PARTNER: TFIBIntegerField;
    TblPresmetkaGPARTNER_ID: TFIBIntegerField;
    TblPresmetkaGNAZIV: TFIBStringField;
    TblPresmetkaGIZNOS_VKUPNO: TFIBBCDField;
    TblPresmetkaGVRABOTEN: TFIBStringField;
    TblPresmetkaGLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaGGODINA: TFIBIntegerField;
    TblPresmetkaGMESEC: TFIBIntegerField;
    TblPresmetkaS: TpFIBDataSet;
    DSPresmetka1: TDataSource;
    TblPresmetkaVodomeriSostojba: TpFIBDataSet;
    DSPresmetka2: TDataSource;
    TblPresmetkaSNAZIV: TFIBStringField;
    TblPresmetkaSIZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaSIZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaSLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaLOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaVODOMERI_ID: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaNOVA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaGREON_ID: TFIBIntegerField;
    TblPresmetkaGCITACKA_KNIGA: TFIBIntegerField;
    TblPresmetkaVodomeriSostojbaSTARA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaSCENA: TFIBBCDField;
    TblPresmetkaGDANOK_05: TFIBBCDField;
    TblPresmetkaGDANOK_18: TFIBBCDField;
    TblPresmetkaGOSNOVICA_DANOK_05: TFIBBCDField;
    TblPresmetkaGOSNOVICA_DANOK_18: TFIBBCDField;
    QGetNotGeneratedSostojbi: TpFIBQuery;
    QGetNotVneseniSostojbi: TpFIBQuery;
    QGetNotGeneratedCenovnik: TpFIBQuery;
    TblPresmetkaGSTANBENA_POVRSINA: TFIBIntegerField;
    TblPresmetkaGDVORNA_POVRSINA: TFIBIntegerField;
    QGetPresmetkaPoBrojUplata: TpFIBQuery;
    TblKomUplati: TpFIBDataSet;
    DSKomUplati: TDataSource;
    TblKomNacinPresmetkaID: TFIBIntegerField;
    TblKomNacinPresmetkaOPIS: TFIBStringField;
    TblKomUplatiID: TFIBIntegerField;
    TblKomUplatiMESEC: TFIBIntegerField;
    TblKomUplatiGODINA: TFIBIntegerField;
    TblKomUplatiLOKACIIN_ID: TFIBIntegerField;
    TblKomUplatiDATUM_UPLATA: TFIBDateField;
    TblKomUplatiTIP: TFIBSmallIntField;
    TblKomUplatiIZNOS: TFIBBCDField;
    TblKomUplatiVRABOTEN: TFIBStringField;
    TblKomUplatiTS: TFIBDateTimeField;
    TblKomLokaciiPoPartner: TpFIBDataSet;
    DSKomLokaciiPoPartner: TDataSource;
    TblKomLokaciiPoPartnerID: TFIBIntegerField;
    TblKomLokaciiPoPartnerREON_ID: TFIBIntegerField;
    TblKomLokaciiPoPartnerNAZIV_REON: TFIBStringField;
    TblKomLokaciiPoPartnerCITACKA_KNIGA: TFIBIntegerField;
    TblKomLokaciiPoPartnerULICA_ID: TFIBIntegerField;
    TblKomLokaciiPoPartnerNAZIV_ULICA: TFIBStringField;
    TblKomLokaciiPoPartnerBROJ: TFIBStringField;
    TblPresmetkaGBROJ_FAKTURA: TFIBStringField;
    TblNeplateniZadolzuvanjaPoLokacija: TpFIBDataSet;
    DSNeplateniZadolzuvanjePoLokacija: TDataSource;
    TblPresmetkaSVID_USLUGA: TFIBIntegerField;
    TblPresmetkaSUSLUGA: TFIBIntegerField;
    QInsertUplati: TpFIBQuery;
    TblPrUplatiDatumski: TpFIBDataSet;
    DSTblPrUplatiDatumski: TDataSource;
    TblPrUplatiDatumskiDATUM_UPLATA: TFIBDateField;
    TblPrUplatiDatumskiIZNOS: TFIBBCDField;
    TblPrUplatiDatumskiVRABOTEN: TFIBStringField;
    TblPrUplatiDatumskiID: TFIBIntegerField;
    TblPrUplatiDatumskiTIP_UPLATA: TFIBStringField;
    TblKomTipUplata: TpFIBDataSet;
    DSKomTipUplata: TDataSource;
    TblKomTipUplataID: TFIBIntegerField;
    TblKomTipUplataNAZIV: TFIBStringField;
    TblPresmetkaPoUslugi1: TpFIBDataSet;
    DSTblPresmetkaPoUslugu1: TDataSource;
    TblPresmetkaPoUslugi2: TpFIBDataSet;
    DSTblPresmetkaPoUslugi2: TDataSource;
    TblPresmetkaPoUsluga3: TpFIBDataSet;
    DSTblPresmetkaPoUsluva3: TDataSource;
    TblPresmetkaPoUsluga4: TpFIBDataSet;
    DSTblPresmetkaPoUsluva4: TDataSource;
    TblPresmetkaPoUsluga5: TpFIBDataSet;
    DSTblPresmetkaPoUsluva5: TDataSource;
    TblPresmetkaPoUsluga6: TpFIBDataSet;
    DSTblPresmetkaPoUsluva6: TDataSource;
    TblPresmetkaPoUsluga7: TpFIBDataSet;
    DSTblPresmetkaPoUsluva7: TDataSource;
    QGetPresmetkaPoLokacijaMesecGodina: TpFIBQuery;
    TblPresmetkaPoUslugi2IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUslugi2IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUslugi2DANOK: TFIBBCDField;
    TblPresmetkaPoUslugi2CENA_KAN: TFIBBCDField;
    TblPresmetkaPoUsluga3VID_USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga3USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga3IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga3IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga3DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga3CENA_STAN: TFIBBCDField;
    TblPresmetkaPoUsluga5IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga5IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga5DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga5CENA_MANIPULATIVEN: TFIBBCDField;
    TblPresmetkaPoUsluga6IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga6IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga6DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga6CENA_FVODA: TFIBBCDField;
    TblPresmetkaPoUsluga7IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga7IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga7DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga7CENA_FKAN: TFIBBCDField;
    TblPresmetkaPoUsluga4VID_USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga4USLUGA: TFIBIntegerField;
    TblPresmetkaPoUsluga4IZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaPoUsluga4IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUsluga4DANOK: TFIBBCDField;
    TblPresmetkaPoUsluga4CENA_DVOR: TFIBBCDField;
    TblPresmetkaPoUslugi1LOKACIIN_ID: TFIBIntegerField;
    TblPresmetkaPoUslugi1VODOMERI_ID: TFIBIntegerField;
    TblPresmetkaPoUslugi1NOVA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaPoUslugi1STARA_SOSTOJBA: TFIBIntegerField;
    TblPresmetkaPoUslugi1RAZLIKA: TFIBBCDField;
    TblPresmetkaPoUslugi1CENA_VODA: TFIBBCDField;
    TblPresmetkaPoUslugi1DDV: TFIBIntegerField;
    TblPresmetkaPoUslugi1IZNOS_USLUGA: TFIBBCDField;
    TblPresmetkaPoUslugi1IZNOS_USLUGA_NETO: TFIBBCDField;
    TblKomVodomeriSostojbaREON_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaCITACKA_KNIGA: TFIBIntegerField;
    TblKomVodomeriSostojbaCITACKA_KNIGA_RB: TFIBIntegerField;
    TblKomVodomeriSostojbaLOKACIIN_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaVODOMERI_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaSTARA_SOSTOJBA: TFIBIntegerField;
    TblKomVodomeriSostojbaNOVA_SOSTOJBA: TFIBIntegerField;
    TblKomVodomeriSostojbaRAZLIKA: TFIBBCDField;
    TblKomVodomeriSostojbaGODINA: TFIBIntegerField;
    TblKomVodomeriSostojbaMESEC: TFIBIntegerField;
    TblKomVodomeriSostojbaTIP_PARTNER: TFIBIntegerField;
    TblKomVodomeriSostojbaPARTNER_ID: TFIBIntegerField;
    TblKomVodomeriSostojbaNAZIV: TFIBStringField;
    TblKomVodomeriSostojbaADRESA: TFIBStringField;
    TblKomVodomeriSostojbaLOKACIJA_AKTIVEN: TFIBStringField;
    TblKomVodomeriSostojbaVODOMER_STATUS: TFIBSmallIntField;
    DSPresmetka3: TDataSource;
    TblSaldo: TpFIBDataSet;
    TblDopUslugi: TpFIBDataSet;
    dsDopUslugi: TDataSource;
    TblDopUslugiBR_RATA: TFIBIntegerField;
    TblDopUslugiTIP_PARTNER: TFIBIntegerField;
    TblDopUslugiPARTNER: TFIBIntegerField;
    TblDopUslugiLOKACIIN_ID: TFIBIntegerField;
    TblDopUslugiMESEC: TFIBIntegerField;
    TblDopUslugiGODINA: TFIBIntegerField;
    TblDopUslugiVID_USLUGA: TFIBIntegerField;
    TblDopUslugiUSLUGA: TFIBIntegerField;
    TblDopUslugiVK_RATI: TFIBIntegerField;
    TblDopUslugiIZNOS_USLUGA_NETO: TFIBBCDField;
    TblDopUslugiIZNOS_USLUGA: TFIBBCDField;
    TblDopUslugiDATUM: TFIBDateTimeField;
    TblDopUslugiNAZIV_PARTNER: TFIBStringField;
    TblDopUslugiNAZIV_USLUGA: TFIBStringField;
    TblDopUslugiID: TFIBIntegerField;
    TblRati: TpFIBDataSet;
    dsRati: TDataSource;
    ProcSetOrder: TpFIBStoredProc;
    TblKomLokaciiKOREN: TFIBIntegerField;
    TblKomVodomeriTIP_PARTNER: TFIBIntegerField;
    TblKomVodomeriPARTNER: TFIBIntegerField;
    TblKomVodomeriREON_ID: TFIBIntegerField;
    TblKomVodomeriLOKACIIN_ID_T: TFIBIntegerField;
    ProcUpdKoren: TpFIBStoredProc;
    tblKarPar: TpFIBDataSet;
    dsKarPar: TDataSource;
    TblDopUslugiUslugaLookup: TStringField;
    PMaxDopUsluga: TpFIBStoredProc;
    TblPresmetkaSPROCENT_DANOK: TFIBIntegerField;
    TblPresmetkaSDANOK: TFIBBCDField;
    TblDopUslugiDANOK: TFIBIntegerField;
    TblRatiBR_RATA: TFIBIntegerField;
    TblRatiMESEC: TFIBStringField;
    TblRatiIZNOS_USLUGA: TFIBBCDField;
    TblRatiID: TFIBIntegerField;
    TblRatiTIP_PARTNER: TFIBIntegerField;
    TblRatiPARTNER: TFIBIntegerField;
    TblRatiLOKACIIN_ID: TFIBIntegerField;
    TblRatiMESEC1: TFIBIntegerField;
    TblRatiGODINA: TFIBIntegerField;
    TblRatiVID_USLUGA: TFIBIntegerField;
    TblRatiUSLUGA: TFIBIntegerField;
    TblRatiVK_RATI: TFIBIntegerField;
    TblRatiIZNOS_USLUGA_NETO: TFIBBCDField;
    TblRatiDATUM: TFIBDateTimeField;
    TblRatiDANOK: TFIBIntegerField;
    TblKomVodomeriSostojbaPAUSAL: TFIBSmallIntField;
    tblPausal: TpFIBDataSet;
    dsPausal: TDataSource;
    TblKomCenovnikPAUSAL: TFIBIntegerField;
    tblPausalGODINA: TFIBIntegerField;
    tblPausalMESEC: TFIBIntegerField;
    tblPausalCENA: TFIBBCDField;
    tblPausalDANOK: TFIBIntegerField;
    tblPausalNACIN_PRESMETKA: TFIBSmallIntField;
    tblPausalPAUSAL: TFIBIntegerField;
    TblKomVodomeriSostojbaBROJ_NA_CLENOVI: TFIBIntegerField;
    TblDopUslugiVO_PRESMETKA: TFIBIntegerField;
    TblRatiVO_PRESMETKA: TFIBIntegerField;
    TblPrUplatiDatumskiNAZIV: TFIBStringField;
    TblPrUplatiDatumskiID_TU: TFIBIntegerField;
    tblBrisiUplati: TpFIBDataSet;
    dsBrisiUplati: TDataSource;
    tblTipUplata: TpFIBDataSet;
    dsTipUplata: TDataSource;
    tblTipUplataID: TFIBIntegerField;
    tblTipUplataNAZIV: TFIBStringField;
    pMaxTipUplata: TpFIBStoredProc;
    TblKomLokaciiPoPartnerTIP_PARTNER: TFIBIntegerField;
    TblKomLokaciiPoPartnerPARTNER_ID: TFIBIntegerField;
    TblKomLokacijaUslugiNAZIV: TFIBStringField;
    qDaliImaPresmetka: TpFIBQuery;
    TblPresmetkaGDATUM_PRESMETKA: TFIBDateTimeField;
    TblPresmetkaPoUsluga3DDV: TFIBIntegerField;
    TblPresmetkaPoUslugi2DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga4DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga5DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga6DDV: TFIBIntegerField;
    TblPresmetkaPoUsluga7DDV: TFIBIntegerField;
    pMaxLokacii: TpFIBStoredProc;
    TblKomLokaciiRB1: TFIBIntegerField;
    pUpdate: TpFIBQuery;
    pUpdatePS: TpFIBQuery;
    pUpdateP: TpFIBQuery;
    pUpdateS: TpFIBQuery;
    TransakcijaQ: TpFIBTransaction;
    pKoren: TpFIBStoredProc;
    cxStyle1: TcxStyle;
    TblSaldoSALDO: TFIBBCDField;
    tblPecDukani: TpFIBDataSet;
    tblPecDukaniIZNOS_VKUPNO: TFIBBCDField;
    FIBStringField3: TFIBStringField;
    FIBBCDField2: TFIBBCDField;
    FIBBCDField3: TFIBBCDField;
    FIBBCDField4: TFIBBCDField;
    FIBStringField4: TFIBStringField;
    FIBStringField5: TFIBStringField;
    FIBIntegerField8: TFIBIntegerField;
    FIBBCDField5: TFIBBCDField;
    FIBBCDField6: TFIBBCDField;
    FIBBCDField7: TFIBBCDField;
    FIBBCDField8: TFIBBCDField;
    FIBIntegerField9: TFIBIntegerField;
    FIBIntegerField10: TFIBIntegerField;
    FIBDateTimeField1: TFIBDateTimeField;
    FIBDateTimeField2: TFIBDateTimeField;
    dsPecDukani: TDataSource;
    tblDPresmetkaS: TpFIBDataSet;
    FIBStringField7: TFIBStringField;
    FIBBCDField9: TFIBBCDField;
    FIBBCDField10: TFIBBCDField;
    FIBIntegerField11: TFIBIntegerField;
    FIBBCDField11: TFIBBCDField;
    FIBIntegerField12: TFIBIntegerField;
    FIBIntegerField13: TFIBIntegerField;
    FIBBCDField12: TFIBBCDField;
    dsDPresmetkaS: TDataSource;
    tblDPrsmetkaVodomeriSostojba: TpFIBDataSet;
    FIBIntegerField15: TFIBIntegerField;
    FIBIntegerField16: TFIBIntegerField;
    FIBIntegerField17: TFIBIntegerField;
    FIBIntegerField18: TFIBIntegerField;
    dsDVodomeriSostojba: TDataSource;
    tblDSaldo: TpFIBDataSet;
    FIBBCDField13: TFIBBCDField;
    dsDSaldo: TDataSource;
    tblGetNotVneseniSostojbi: TpFIBDataSet;
    dsGetNotVneseniSostojbi: TDataSource;
    tblGetNotVneseniSostojbiREON_ID: TFIBIntegerField;
    tblGetNotVneseniSostojbiLOKACIJA: TFIBIntegerField;
    tblGetNotVneseniSostojbiID: TFIBIntegerField;
    tblGetNotVneseniSostojbiNAZIV: TFIBStringField;
    tblGetNotVneseniSostojbiADRESA: TFIBStringField;
    tblGetNotVneseniSostojbiNOVA_SOSTOJBA: TFIBIntegerField;
    tblGetNotVneseniSostojbiSTARA_SOSTOJBA: TFIBIntegerField;
    tblGetNotVneseniSostojbiCITACKA_KNIGA: TFIBIntegerField;
    tblGetNotVneseniSostojbiCITACKA_KNIGA_RB: TFIBIntegerField;
    tblGetNotVneseniSostojbiVODOMER: TFIBIntegerField;
    ProcPromCitKn: TpFIBStoredProc;
    TblKomLokaciiPoPartnerCITACKA_KNIGA_RB: TFIBIntegerField;
    ProcSetOrder1: TpFIBStoredProc;
    TblPresmetkaGCITACKA_KNIGA_RB: TFIBIntegerField;
    TblPresmetkaGADRESA: TFIBStringField;
    tblPecDukaniCITACKA_KNIGA_RB: TFIBIntegerField;
    qDaliImaStavki: TpFIBQuery;
    tblKarParLOKACIJA: TFIBIntegerField;
    tblKarParBROJ_FAKTURA: TFIBStringField;
    tblKarParTIP_PARTNER: TFIBIntegerField;
    tblKarParID: TFIBIntegerField;
    tblKarParNAZIV: TFIBStringField;
    tblKarParADRESA: TFIBStringField;
    tblKarParDATUM_PRESMETKA: TFIBDateTimeField;
    tblKarParDOLZI: TFIBBCDField;
    tblKarParPOBARUVA: TFIBBCDField;
    tblKarParUPLATENO: TFIBDateField;
    tblKarParSALDO: TFIBBCDField;
    tblKarParMESEC: TFIBIntegerField;
    tblKarParGODINA: TFIBIntegerField;
    tblKarParRE: TFIBIntegerField;
    tblKarParTIP_NALOG: TFIBStringField;
    tblKarParNALOG: TFIBIntegerField;
    tblKarParREON_ID: TFIBIntegerField;
    tblKarParPID: TFIBIntegerField;
    tblKarParCITACKA_KNIGA: TFIBIntegerField;
    tblTipUplataKONTO: TFIBStringField;
    tblTipUplataVID: TFIBIntegerField;
    tblPecDukaniID: TFIBIntegerField;
    TblPresmetkaGID: TFIBIntegerField;
    TblDPresmetkaPoUsluga1: TpFIBDataSet;
    FIBIntegerField19: TFIBIntegerField;
    FIBIntegerField20: TFIBIntegerField;
    FIBIntegerField21: TFIBIntegerField;
    FIBIntegerField22: TFIBIntegerField;
    FIBBCDField14: TFIBBCDField;
    FIBBCDField15: TFIBBCDField;
    FIBIntegerField23: TFIBIntegerField;
    FIBBCDField16: TFIBBCDField;
    FIBBCDField17: TFIBBCDField;
    DSDPresmetkaPoUsluva1: TDataSource;
    TblDPresmetkaPoUsluga2: TpFIBDataSet;
    FIBBCDField18: TFIBBCDField;
    FIBBCDField19: TFIBBCDField;
    FIBBCDField20: TFIBBCDField;
    FIBBCDField21: TFIBBCDField;
    FIBIntegerField24: TFIBIntegerField;
    DSDPresmetkaPoUsluva2: TDataSource;
    TblDPresmetkaPoUsluga3: TpFIBDataSet;
    FIBIntegerField25: TFIBIntegerField;
    FIBIntegerField26: TFIBIntegerField;
    FIBBCDField22: TFIBBCDField;
    FIBBCDField23: TFIBBCDField;
    FIBBCDField24: TFIBBCDField;
    FIBBCDField25: TFIBBCDField;
    FIBIntegerField27: TFIBIntegerField;
    DSDPresmetkaPoUsluva3: TDataSource;
    TblDPresmetkaPoUsluga4: TpFIBDataSet;
    FIBIntegerField28: TFIBIntegerField;
    FIBIntegerField29: TFIBIntegerField;
    FIBBCDField26: TFIBBCDField;
    FIBBCDField27: TFIBBCDField;
    FIBBCDField28: TFIBBCDField;
    FIBBCDField29: TFIBBCDField;
    FIBIntegerField30: TFIBIntegerField;
    DSDPresmetkaPoUsluva4: TDataSource;
    TblDPresmetkaPoUsluga5: TpFIBDataSet;
    FIBBCDField30: TFIBBCDField;
    FIBBCDField31: TFIBBCDField;
    FIBBCDField32: TFIBBCDField;
    FIBBCDField33: TFIBBCDField;
    FIBIntegerField31: TFIBIntegerField;
    DSDPresmetkaPoUsluva5: TDataSource;
    TblDPresmetkaPoUsluga6: TpFIBDataSet;
    FIBBCDField34: TFIBBCDField;
    FIBBCDField35: TFIBBCDField;
    FIBBCDField36: TFIBBCDField;
    FIBBCDField37: TFIBBCDField;
    FIBIntegerField32: TFIBIntegerField;
    DSDPresmetkaPoUsluva6: TDataSource;
    TblDPresmetkaPoUsluga7: TpFIBDataSet;
    FIBBCDField38: TFIBBCDField;
    FIBBCDField39: TFIBBCDField;
    FIBBCDField40: TFIBBCDField;
    FIBBCDField41: TFIBBCDField;
    FIBIntegerField33: TFIBIntegerField;
    DSDPresmetkaPoUsluva7: TDataSource;
    ProcKomPresmetkaUpdate: TpFIBStoredProc;
    ProcKomPresmetkaEdinecna: TpFIBStoredProc;
    TblKomUplatiPRESMETKA_G_ID: TFIBIntegerField;
    tblDPresmetkaSPROCENT_DANOK: TFIBIntegerField;
    tblKarParAKTIVEN: TFIBSmallIntField;
    qUpdateTip: TpFIBQuery;
    pKomVSEdinecna: TpFIBStoredProc;
    tblDULista: TpFIBDataSet;
    dsDULista: TDataSource;
    tblDUListaNAZIV: TFIBStringField;
    tblDUListaID: TFIBIntegerField;
    tblDUListaBR_RATA: TFIBIntegerField;
    tblDUListaTIP_PARTNER: TFIBIntegerField;
    tblDUListaPARTNER: TFIBIntegerField;
    tblDUListaLOKACIIN_ID: TFIBIntegerField;
    tblDUListaMESEC: TFIBIntegerField;
    tblDUListaGODINA: TFIBIntegerField;
    tblDUListaVID_USLUGA: TFIBIntegerField;
    tblDUListaUSLUGA: TFIBIntegerField;
    tblDUListaVK_RATI: TFIBIntegerField;
    tblDUListaVO_PRESMETKA: TFIBIntegerField;
    tblDUListaIZNOS_USLUGA_NETO: TFIBBCDField;
    tblDUListaIZNOS_USLUGA: TFIBBCDField;
    tblDUListaDATUM: TFIBDateTimeField;
    tblDUListaDANOK: TFIBIntegerField;
    ProcPromCK: TpFIBStoredProc;
    qRB2: TpFIBQuery;
    ProcNovRb2: TpFIBStoredProc;
    TblPresmetkaGNAZIV1: TFIBStringField;
    TblPresmetkaGIZNOS_USLUGA_NETO: TFIBBCDField;
    TblPresmetkaGDANOK: TFIBBCDField;
    TblPresmetkaGIZNOS_USLUGA: TFIBBCDField;
    tblOpomeni: TpFIBDataSet;
    dsOpomeni: TDataSource;
    tblOpomeniBROJ: TFIBStringField;
    tblOpomeniDATUM: TFIBDateTimeField;
    tblOpomeniDATUM_OD: TFIBDateField;
    tblOpomeniDATUM_DO: TFIBDateField;
    tblPresmetkiVoOpomena: TpFIBDataSet;
    dsPresmetkiVoOpomena: TDataSource;
    pOpomena: TpFIBStoredProc;
    tblPresmetkiVoOpomenaREON: TFIBIntegerField;
    tblPresmetkiVoOpomenaLOKACIJA: TFIBIntegerField;
    tblPresmetkiVoOpomenaTIP_PARTNER: TFIBIntegerField;
    tblPresmetkiVoOpomenaPARTNER_ID: TFIBIntegerField;
    tblPresmetkiVoOpomenaIME_PREZIME: TFIBStringField;
    tblPresmetkiVoOpomenaADRESA: TFIBStringField;
    tblPresmetkiVoOpomenaIZNOS_VKUPNO: TFIBBCDField;
    tblPresmetkiVoOpomenaBR_BELESKI: TFIBIntegerField;
    tblOpomeniTIP_PARTNER: TFIBIntegerField;
    tblOpomeniDOLG: TFIBFloatField;
    tblOpomeniBROJ_NEPLATENI_BEL: TFIBIntegerField;
    tblOpomeniM1: TFIBSmallIntField;
    tblOpomeniG1: TFIBSmallIntField;
    tblOpomeniM2: TFIBSmallIntField;
    tblOpomeniG2: TFIBSmallIntField;
    tblOpomeniREON: TFIBIntegerField;
    tblPrOpomeni: TpFIBDataSet;
    dsPrOpomeni: TDataSource;
    tblPrOpomeniBROJ: TFIBStringField;
    tblPrOpomeniTIP_PARTNER: TFIBIntegerField;
    tblPrOpomeniNAZIV_TP: TFIBStringField;
    tblPrOpomeniDOLG: TFIBFloatField;
    tblPrOpomeniBROJ_NEPLATENI_BEL: TFIBIntegerField;
    tblPrOpomeniREON_ID: TFIBIntegerField;
    tblPrOpomeniID: TFIBIntegerField;
    tblPrOpomeniNAZIV: TFIBStringField;
    tblPrOpomeniDATUM_OD: TFIBDateField;
    tblPrOpomeniDATUM_DO: TFIBDateField;
    tblPrOpomeniLOKACIIN_ID: TFIBIntegerField;
    tblPrOpomeniDOLZEN: TFIBBCDField;
    tblPrOpomeniSTARO_SLADO: TFIBBCDField;
    tblPrOpomeniADRESA: TFIBStringField;
    tblPrOpomeniVODOMER: TFIBStringField;
    tblPrOpomeniSOSTOJBA: TFIBStringField;
    tblPrOpomeniAKTIVEN: TFIBStringField;
    dsPartner: TDataSource;
    tblPartner: TpFIBDataSet;
    tblPartnerID: TFIBIntegerField;
    tblPartnerNAZIV: TFIBStringField;
    tblPartnerADRESA: TFIBStringField;
    tblPartnerTEL: TFIBStringField;
    tblPartnerFAX: TFIBStringField;
    tblPartnerDANOCEN: TFIBStringField;
    tblPartnerMESTO: TFIBIntegerField;
    tblPartnerIME: TFIBStringField;
    tblPartnerPREZIME: TFIBStringField;
    tblPartnerTATKO: TFIBStringField;
    tblPartnerLOGO: TFIBBlobField;
    tblPartnerRE: TFIBIntegerField;
    tblPartnerLOGOTEXT: TFIBBlobField;
    tblPartnerPARTNER: TIntegerField;
    tblPartnerTipPartnerNaziv: TStringField;
    tblPartnerMestoNaziv: TStringField;
    tblPartnerNazivSoMesto: TStringField;
    tblPartnerTIP_PARTNER: TFIBIntegerField;
    qProveriVodomer: TpFIBQuery;
    qBrisiVodomeriSostojba: TpFIBQuery;
    tblPregledLokacii: TpFIBDataSet;
    dsPregledLokacii: TDataSource;
    tblPregledLokaciiREON_ID: TFIBIntegerField;
    tblPregledLokaciiLOKACIJA: TFIBIntegerField;
    tblPregledLokaciiTIP_PARTNER: TFIBIntegerField;
    tblPregledLokaciiPARTNER_ID: TFIBIntegerField;
    tblPregledLokaciiNAZIV_PARTNER: TFIBStringField;
    tblPregledLokaciiULICA_ID: TFIBIntegerField;
    tblPregledLokaciiADRESA: TFIBStringField;
    tblPregledLokaciiSTATUS: TFIBStringField;
    tblPregledLokaciiMATICEN: TFIBStringField;
    tblPregledLokaciiBROJ_NA_CLENOVI: TFIBIntegerField;
    tblPregledLokaciiCITACKA_KNIGA: TFIBIntegerField;
    tblPregledLokaciiCITACKA_KNIGA_RB: TFIBIntegerField;
    tblPregledLokaciiSTANBENA_POVRSINA: TFIBIntegerField;
    tblPregledLokaciiDVORNA_POVRSINA: TFIBIntegerField;
    tblPregledLokaciiNAZIV_MESTO: TFIBStringField;
    dsPregledLokaciiVodomeri: TDataSource;
    tblPregledLokaciiVodomeri: TpFIBDataSet;
    tblPregledLokaciiVodomeriREON_ID: TFIBIntegerField;
    tblPregledLokaciiVodomeriCITACKA_KNIGA: TFIBIntegerField;
    tblPregledLokaciiVodomeriCITACKA_KNIGA_RB: TFIBIntegerField;
    tblPregledLokaciiVodomeriLOKACIJA: TFIBIntegerField;
    tblPregledLokaciiVodomeriTIP_PARTNER: TFIBIntegerField;
    tblPregledLokaciiVodomeriPARTNER_ID: TFIBIntegerField;
    tblPregledLokaciiVodomeriNAZIV: TFIBStringField;
    tblPregledLokaciiVodomeriMATICEN: TFIBStringField;
    tblPregledLokaciiVodomeriADRESA: TFIBStringField;
    tblPregledLokaciiVodomeriVODOMER: TFIBStringField;
    tblPregledLokaciiVodomeriAKTIVEN: TFIBStringField;
    tblPregledLokaciiVodomeriNAZIV_TIP_PARTNER: TFIBStringField;
    tblPregledLokaciiVodomeriNAZIV_MESTO: TFIBStringField;
    tblPregledLokaciiVodomeriSERISKI_BROJ: TFIBStringField;
    tblCitackiKartoni: TpFIBDataSet;
    dsCitackiKartoni: TDataSource;
    tblCitackiKartoniLOKACIJA: TFIBIntegerField;
    tblCitackiKartoniTIP: TFIBIntegerField;
    tblCitackiKartoniPARTNER_ID: TFIBIntegerField;
    tblCitackiKartoniNAZ: TFIBStringField;
    tblCitackiKartoniULICA: TFIBStringField;
    tblCitackiKartoniBROJ: TFIBStringField;
    tblCitackiKartoniAKTIVEN: TFIBStringField;
    tblCitackiKartoniREON: TFIBIntegerField;
    tblCitackiKartoniCLENOVI: TFIBIntegerField;
    tblCitackiKartoniKNIGA: TFIBIntegerField;
    tblCitackiKartoniREDEN_BROJ: TFIBIntegerField;
    tblCitackiKartoniSTAN: TFIBIntegerField;
    tblCitackiKartoniDVOR: TFIBIntegerField;
    tblCitackiKartoniPAUSAL: TFIBStringField;
    frxCrossObject1: TfrxCrossObject;
    tblVodSost: TpFIBDataSet;
    dsVodSost: TDataSource;
    tblVodSostLOKACIIN_ID: TFIBIntegerField;
    tblVodSostVODOMERI_ID: TFIBIntegerField;
    tblVodSostSTARA_SOSTOJBA: TFIBIntegerField;
    tblVodSostNOVA_SOSTOJBA: TFIBIntegerField;
    tblVodSostGODINA: TFIBIntegerField;
    tblVodSostMESEC: TFIBIntegerField;
    tblVodSostPOSLEDNA_SOSTOJBA: TFIBIntegerField;
    tblVodSostLOKACIIN_ID_T: TFIBIntegerField;
    tblVodSostTIP_PARTNER: TFIBIntegerField;
    tblVodSostPARTNER: TFIBIntegerField;
    tblVodSostREON_ID: TFIBIntegerField;
    tblVodSostRAZLIKA: TFIBBCDField;
    tblVodSostTS: TFIBDateTimeField;
    frxDBDataset1: TfrxDBDataset;
    tblCitackiKartoniBR_VODOMERI: TFIBIntegerField;
    tblCitackiKartoniSOSTOJBA: TFIBIntegerField;
    TblPresmetkaGDDO: TFIBDateField;
    tblPecDukaniDDO: TFIBDateField;
    qSetupV: TpFIBQuery;
    tblProveriPromena: TpFIBDataSet;
    FIBIntegerField14: TFIBIntegerField;
    FIBIntegerField34: TFIBIntegerField;
    FIBIntegerField35: TFIBIntegerField;
    FIBIntegerField36: TFIBIntegerField;
    FIBDateField1: TFIBDateField;
    FIBSmallIntField1: TFIBSmallIntField;
    FIBBCDField42: TFIBBCDField;
    FIBStringField8: TFIBStringField;
    FIBDateTimeField3: TFIBDateTimeField;
    FIBIntegerField37: TFIBIntegerField;
    dsProveriPromena: TDataSource;
    TblPresmetkaPoUsluga8: TpFIBDataSet;
    FIBBCDField43: TFIBBCDField;
    FIBBCDField44: TFIBBCDField;
    FIBBCDField45: TFIBBCDField;
    FIBBCDField46: TFIBBCDField;
    FIBIntegerField38: TFIBIntegerField;
    dsPresmetkaPoUsluga8: TDataSource;
    TblDPresmetkaPoUsluga8: TpFIBDataSet;
    FIBBCDField47: TFIBBCDField;
    FIBBCDField48: TFIBBCDField;
    FIBBCDField49: TFIBBCDField;
    FIBBCDField50: TFIBBCDField;
    FIBIntegerField39: TFIBIntegerField;
    dsDPresmetkaPoUsluga8: TDataSource;
    TblKomLokaciiRB2: TFIBFloatField;
    tblPecDukaniRB2: TFIBFloatField;
    tblPregledLokaciiVodomeriRB2: TFIBFloatField;
    TblPresmetkaGRB2: TFIBFloatField;
    tblGetNotVneseniSostojbiRB2: TFIBFloatField;
    tblBrisiSaldo: TpFIBDataSet;
    dsBrisiSaldo: TDataSource;
    tblBrisiSaldoID: TFIBIntegerField;
    tblBrisiSaldoTIP_PARTNER: TFIBIntegerField;
    tblBrisiSaldoPARTNER: TFIBIntegerField;
    tblBrisiSaldoNAZIV_PARTNER: TFIBStringField;
    tblBrisiSaldoIZNOS: TFIBBCDField;
    tblBrisiSaldoTIP: TFIBIntegerField;
    tblBrisiSaldoDOKUMENT: TFIBStringField;
    tblBrisiSaldoDATUM_UPLATA: TFIBDateField;
    tblBrisiSaldoTIP_NALOG: TFIBStringField;
    tblBrisiSaldoNALOG: TFIBIntegerField;
    tblBrisiSaldoID_SALDO: TFIBIntegerField;
    tblBrisiSaldoNAZIV_TIP_UPLATA: TFIBStringField;
    tblLokacija: TpFIBDataSet;
    dsLokacija: TDataSource;
    tblLokacijaArhiva: TpFIBDataSet;
    dsLokacijaArhiva: TDataSource;
    tblLokacijaArhivaID: TFIBIntegerField;
    tblLokacijaArhivaID_LOKACIJA: TFIBIntegerField;
    tblLokacijaArhivaTIP_PARTNER: TFIBIntegerField;
    tblLokacijaArhivaPARTNER: TFIBIntegerField;
    tblLokacijaArhivaNAZIV_PARTNER: TFIBStringField;
    tblLokacijaArhivaDATUM: TFIBDateTimeField;
    tblLokacijaCITACKA_KNIGA_RB: TFIBIntegerField;
    tblLokacijaID: TFIBIntegerField;
    tblLokacijaPARTNER_ID: TFIBIntegerField;
    tblLokacijaTIP_PARTNER: TFIBIntegerField;
    tblLokacijaAKTIVEN: TFIBSmallIntField;
    tblLokacijaREON_ID: TFIBIntegerField;
    tblLokacijaNAZIV_REON: TFIBStringField;
    tblLokacijaULICA_ID: TFIBIntegerField;
    tblLokacijaBROJ: TFIBStringField;
    tblLokacijaBROJ_NA_CLENOVI: TFIBIntegerField;
    tblLokacijaSTANBENA_POVRSINA: TFIBIntegerField;
    tblLokacijaDVORNA_POVRSINA: TFIBIntegerField;
    tblLokacijaRB2: TFIBFloatField;
    tblLokacijaADRESA: TFIBStringField;
    tblLokacijaNAZIV_PARTNER: TFIBStringField;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    pLokArhiva: TpFIBStoredProc;
    cxLargeImages: TcxImageList;
    tblLokacijaArhivaPOC_MESEC: TFIBIntegerField;
    tblLokacijaArhivaPOC_GODINA: TFIBIntegerField;
    tblLokacijaArhivaKRAJ_MESEC: TFIBIntegerField;
    tblLokacijaArhivaKRAJ_GODINA: TFIBIntegerField;
    qSetupFizicki: TpFIBQuery;
    qSetupDukani: TpFIBQuery;
    tblKarParNAZIV_REON: TFIBStringField;
    tblLokacijaCITACKA_KNIGA: TFIBStringField;
    tblKarParOPIS_LOKACIJA: TFIBStringField;
    cxStyle4: TcxStyle;
    TblKomVodomeriSostojbaRB2: TFIBFloatField;
    tblKontenPlan: TpFIBDataSet;
    tblKontenPlanSIFRA: TFIBStringField;
    tblKontenPlanNAZIV: TFIBStringField;
    tblKontenPlanANALITIKA_ZA: TFIBStringField;
    tblKontenPlanSINTETIKA: TFIBIntegerField;
    tblKontenPlanSALDIRANJE: TFIBIntegerField;
    tblKontenPlanPARTNER: TFIBIntegerField;
    tblKontenPlanDOKUMENT: TFIBIntegerField;
    tblKontenPlanDATUM_DOKUMENT: TFIBIntegerField;
    tblKontenPlanDDO: TFIBIntegerField;
    tblKontenPlanAP: TFIBIntegerField;
    tblKontenPlanOS: TFIBIntegerField;
    tblKontenPlanKR: TFIBIntegerField;
    tblKontenPlanAVANS: TFIBStringField;
    tblKontenPlanPKR: TFIBStringField;
    tblKontenPlanNKR: TFIBStringField;
    dsKontenPlan: TDataSource;
    TblPresmetkaPoUsluga10: TpFIBDataSet;
    FIBBCDField51: TFIBBCDField;
    FIBBCDField52: TFIBBCDField;
    FIBBCDField53: TFIBBCDField;
    FIBIntegerField40: TFIBIntegerField;
    DSTblPresmetkaPoUsluva10: TDataSource;
    TblPresmetkaPoUsluga10CENA_NADOMESTOK: TFIBBCDField;
    TblDPresmetkaPoUsluga10: TpFIBDataSet;
    FIBBCDField54: TFIBBCDField;
    FIBBCDField55: TFIBBCDField;
    FIBBCDField56: TFIBBCDField;
    FIBIntegerField41: TFIBIntegerField;
    FIBBCDField57: TFIBBCDField;
    dsDPresmetkaPoUsluga10: TDataSource;
    TblDPresmetkaPoUsluga10NAZIV_VID_USLUGA: TFIBStringField;
    TblDPresmetkaPoUsluga10NAZIV_USLUGA: TFIBStringField;
    TblPresmetkaPoUsluga10NAZIV_VID_USLUGA: TFIBStringField;
    TblPresmetkaPoUsluga10NAZIV_USLUGA: TFIBStringField;
    TblKomUslugiKFL: TFIBStringField;
    TblKomUslugiKD: TFIBStringField;
    TblKomUslugiKPL: TFIBStringField;
    TblKomUslugiKU: TFIBStringField;
    TblKomUslugiRE: TFIBIntegerField;
    tblPartnerSTATUS: TFIBIntegerField;
    TblPrUplatiDatumskiTIP: TFIBSmallIntField;
    TblPrUplatiDatumskiRE: TFIBIntegerField;
    TblPrUplatiDatumskiTIP_NALOG: TFIBStringField;
    TblPrUplatiDatumskiNALOG: TFIBIntegerField;
    TblPrUplatiDatumskiTS_KNIZENJE: TFIBDateTimeField;
    TblPrUplatiDatumskiDOKUMENT: TFIBStringField;
    tblSiteUplati: TpFIBDataSet;
    dsSiteUplati: TDataSource;
    tblKamatnaStapka: TpFIBDataSet;
    dsKamatnaStapka: TDataSource;
    tblKamatnaStapkaOD_DATA: TFIBDateField;
    tblKamatnaStapkaDO_DATA: TFIBDateField;
    tblKamatnaStapkaSTAPKA: TFIBBCDField;
    qImaBeleska: TpFIBQuery;
    pBrisiUplata: TpFIBStoredProc;
    TblDPresmetkaPoUsluga61: TpFIBDataSet;
    FIBBCDField58: TFIBBCDField;
    FIBBCDField59: TFIBBCDField;
    FIBBCDField60: TFIBBCDField;
    FIBIntegerField42: TFIBIntegerField;
    FIBBCDField61: TFIBBCDField;
    FIBStringField9: TFIBStringField;
    FIBStringField10: TFIBStringField;
    DSTblDPresmetkaPoUsluga61: TDataSource;
    pFIBDataSet1: TpFIBDataSet;
    FIBIntegerField43: TFIBIntegerField;
    FIBIntegerField44: TFIBIntegerField;
    FIBIntegerField45: TFIBIntegerField;
    FIBIntegerField46: TFIBIntegerField;
    FIBDateField2: TFIBDateField;
    FIBSmallIntField2: TFIBSmallIntField;
    FIBBCDField62: TFIBBCDField;
    FIBStringField11: TFIBStringField;
    FIBDateTimeField4: TFIBDateTimeField;
    FIBIntegerField47: TFIBIntegerField;
    pFIBDataSet2: TpFIBDataSet;
    FIBIntegerField48: TFIBIntegerField;
    FIBIntegerField49: TFIBIntegerField;
    FIBIntegerField50: TFIBIntegerField;
    FIBIntegerField51: TFIBIntegerField;
    FIBDateField3: TFIBDateField;
    FIBSmallIntField3: TFIBSmallIntField;
    FIBBCDField63: TFIBBCDField;
    FIBStringField12: TFIBStringField;
    FIBDateTimeField5: TFIBDateTimeField;
    FIBIntegerField52: TFIBIntegerField;
    tblPogled: TpFIBDataSet;
    dsPogled: TDataSource;
    tblPogledID: TFIBIntegerField;
    tblPogledLOKACIIN_ID: TFIBIntegerField;
    tblPogledFAKTURA: TFIBStringField;
    tblPogledVID_USLUGA: TFIBIntegerField;
    tblPogledNAZIV: TFIBStringField;
    tblPogledUSLUGA: TFIBIntegerField;
    tblPogledIZNOS_USLUGA_NETO: TFIBBCDField;
    tblPogledIZNOS_USLUGA: TFIBBCDField;
    tblPrKorekcija: TpFIBDataSet;
    dsPrKorekcija: TDataSource;
    tblPrKorekcijaID: TFIBIntegerField;
    tblPrKorekcijaID_PRESMETKA: TFIBIntegerField;
    tblPrKorekcijaVID_USLUGA: TFIBIntegerField;
    tblPrKorekcijaUSLUGA: TFIBIntegerField;
    tblPrKorekcijaIZNOS_USLUGA_NETO: TFIBBCDField;
    tblPrKorekcijaIZNOS_USLUGA: TFIBBCDField;
    tblPrKorekcijaDATUM: TFIBDateTimeField;
    tblPrKorekcijaVO_PRESMETKA: TFIBIntegerField;
    tblPrKorekcijaDANOK: TFIBBCDField;
    tblPrKorekcijaTS_INS: TFIBDateTimeField;
    tblPrKorekcijaTS_UPD: TFIBDateTimeField;
    tblPrKorekcijaUSR_INS: TFIBStringField;
    tblPrKorekcijaUSR_UPD: TFIBStringField;
    tblPogledIZNOS_DANOK: TFIBBCDField;
    tblPogledDANOK: TFIBIntegerField;
    tblPrKorekcijaID_LOKACIJA: TFIBIntegerField;
    tblPogledVO_PRESMETKA: TFIBIntegerField;
    tblPogledSIFRA_STAVKA: TFIBIntegerField;
    tblPogledTIP: TFIBSmallIntField;
    tblPrKorekcijaTIP: TFIBSmallIntField;
    tblPogledPROMENI: TFIBStringField;
    tblSiteUplatiID: TFIBIntegerField;
    tblSiteUplatiGODINA: TFIBIntegerField;
    tblSiteUplatiMESEC: TFIBIntegerField;
    tblSiteUplatiDOLZI: TFIBBCDField;
    tblSiteUplatiUPLATI: TFIBBCDField;
    tblSiteUplatiUPLATENO: TFIBDateField;
    tblSiteUplatiTIP: TFIBSmallIntField;
    tblSiteUplatiNAZIV_UPLATA: TFIBStringField;
    tblSiteUplatiRE: TFIBIntegerField;
    tblSiteUplatiTIP_NALOG: TFIBStringField;
    tblSiteUplatiNALOG: TFIBIntegerField;
    tblSiteUplatiTS_KNIZENJE: TFIBDateTimeField;
    tblSiteUplatiDOKUMENT: TFIBStringField;
    tblSiteUplatiNACIN_PLAKJANJE: TFIBStringField;
    tblSiteStavki: TpFIBDataSet;
    dsSiteStavki: TDataSource;
    tblSiteStavkiVID_USLUGA: TFIBIntegerField;
    tblSiteStavkiUSLUGA: TFIBIntegerField;
    tblSiteStavkiNAZIV_USLUGA: TFIBStringField;
    tblSiteStavkiIZNOS_USLUGA_NETO: TFIBBCDField;
    tblSiteStavkiIZNOS_USLUGA: TFIBBCDField;
    tblSiteStavkiDATUM_PRESMETKA: TFIBDateTimeField;
    tblSiteStavkiVRABOTEN: TFIBStringField;
    tblSiteStavkiDANOK: TFIBBCDField;
    tblSiteStavkiCENA: TFIBBCDField;
    tblSiteStavkiPRESMETKA_G_ID: TFIBIntegerField;
    tblPromena: TpFIBDataSet;
    dsPromena: TDataSource;
    tblPromenaID: TFIBIntegerField;
    tblPromenaLINK: TFIBStringField;
    tblPromenaPROMENA: TFIBIntegerField;
    tblPromenaZABELESKA: TFIBStringField;
    qSetupLink: TpFIBQuery;
    IdHTTP1: TIdHTTP;
    qSetupLinkEdit: TpFIBQuery;
    TblKomLokaciiNAZIV_PARTNER: TFIBStringField;
    TblKomLokaciiNAZIV_REON: TFIBStringField;
    TblKomLokaciiNAZIV_ULICA: TFIBStringField;
    qSetupLinkZemi: TpFIBQuery;
    TblPresmetkaPoUsluga11: TpFIBDataSet;
    FIBBCDField64: TFIBBCDField;
    FIBBCDField65: TFIBBCDField;
    FIBBCDField66: TFIBBCDField;
    FIBIntegerField53: TFIBIntegerField;
    FIBBCDField67: TFIBBCDField;
    FIBStringField13: TFIBStringField;
    FIBStringField14: TFIBStringField;
    DSTblPresmetkaPoUsluva11: TDataSource;
    TblDPresmetkaPoUsluga11: TpFIBDataSet;
    FIBBCDField68: TFIBBCDField;
    FIBBCDField69: TFIBBCDField;
    FIBBCDField70: TFIBBCDField;
    FIBIntegerField54: TFIBIntegerField;
    FIBBCDField71: TFIBBCDField;
    FIBStringField15: TFIBStringField;
    FIBStringField16: TFIBStringField;
    dsDPresmetkaPoUsluga11: TDataSource;
    TblKomVodomeriSostojbaAVTOMATSKI_VNES: TFIBSmallIntField;
    tblPromeniPartner: TpFIBDataSet;
    dsPromeniPartner: TDataSource;
    tblPromeniPartnerLOKACIJA: TFIBIntegerField;
    tblPromeniPartnerTIP_PARTNER: TFIBIntegerField;
    tblPromeniPartnerPARTNER_ID: TFIBIntegerField;
    tblPromeniPartnerNAZIV: TFIBStringField;
    tblPartnerKamata: TpFIBDataSet;
    dsPartnerKamata: TDataSource;
    tblPartnerKamataID: TFIBIntegerField;
    tblPartnerKamataTIP_PARTNER: TFIBIntegerField;
    tblPartnerKamataPARTNER: TFIBIntegerField;
    tblPartnerKamataOD_MESEC: TFIBIntegerField;
    tblPartnerKamataOD_GODINA: TFIBIntegerField;
    tblPartnerKamataDO_MESEC: TFIBIntegerField;
    tblPartnerKamataDO_GODINA: TFIBIntegerField;
    tblPartnerKamataKAMATA: TFIBIntegerField;
    tblPartnerKamataNAZIV_PARTNER: TFIBStringField;
    tblPregledLokaciiVodomeriID_MERNA_TOCKA: TFIBIntegerField;
    tblPregledLokaciiVodomeriNAZIV_MERNA_TOCKA: TFIBStringField;
    tblMernaTocka: TpFIBDataSet;
    dsMernaTocka: TDataSource;
    tblMernaTockaID: TFIBIntegerField;
    tblMernaTockaNAZIV: TFIBStringField;
    TblKomLokaciiID_MERNA_TOCKA: TFIBIntegerField;
    TblKomLokaciiNAZIV_MERNA_TOCKA: TFIBStringField;
    TblDPresmetkaPoUsluga12: TpFIBDataSet;
    FIBIntegerField55: TFIBIntegerField;
    FIBIntegerField56: TFIBIntegerField;
    FIBBCDField72: TFIBBCDField;
    FIBBCDField73: TFIBBCDField;
    FIBBCDField74: TFIBBCDField;
    FIBBCDField75: TFIBBCDField;
    FIBIntegerField57: TFIBIntegerField;
    DSDPresmetkaPoUsluva12: TDataSource;
    DSTblPresmetkaPoUsluva12: TDataSource;
    TblPresmetkaPoUsluga12: TpFIBDataSet;
    FIBIntegerField58: TFIBIntegerField;
    FIBIntegerField59: TFIBIntegerField;
    FIBBCDField76: TFIBBCDField;
    FIBBCDField77: TFIBBCDField;
    FIBBCDField78: TFIBBCDField;
    FIBBCDField79: TFIBBCDField;
    FIBIntegerField60: TFIBIntegerField;
    TblDPresmetkaPoUsluga121: TpFIBDataSet;
    FIBIntegerField61: TFIBIntegerField;
    FIBIntegerField62: TFIBIntegerField;
    FIBBCDField80: TFIBBCDField;
    FIBBCDField81: TFIBBCDField;
    FIBBCDField82: TFIBBCDField;
    FIBBCDField83: TFIBBCDField;
    FIBIntegerField63: TFIBIntegerField;
    DSDPresmetkaPoUsluva121: TDataSource;
    TblPresmetkaPoUsluga121: TpFIBDataSet;
    FIBIntegerField64: TFIBIntegerField;
    FIBIntegerField65: TFIBIntegerField;
    FIBBCDField84: TFIBBCDField;
    FIBBCDField85: TFIBBCDField;
    FIBBCDField86: TFIBBCDField;
    FIBBCDField87: TFIBBCDField;
    FIBIntegerField66: TFIBIntegerField;
    DSTblPresmetkaPoUsluva121: TDataSource;
    tblKarParID_FNG: TFIBBCDField;
    tblBrisiSaldoID_FNG: TFIBBCDField;
    tblBrisiSaldoRE_FNG: TFIBIntegerField;
    tblBrisiSaldoGODINA_FNG: TFIBIntegerField;
    tblBrisiSaldoTIP_NALOG_FNG: TFIBStringField;
    tblBrisiSaldoNALOG_FNG: TFIBIntegerField;
    tblKarParMAIL: TFIBStringField;
    qSosInt: TpFIBQuery;
    tblPecDukaniTIP_PARTNER: TFIBIntegerField;
    tblPecDukaniPARTNER_ID: TFIBIntegerField;
    tblPecDukaniNAZIV: TFIBStringField;
    tblPecDukaniMAIL: TFIBStringField;
    tblPecDukaniBROJ_FAKTURA: TFIBStringField;
    tblPresmetkaGEMAIL: TFIBStringField;
    TblPresmetkaGMAIL: TFIBStringField;
    fbcdfldPecDukaniVK_POTROSENO: TFIBBCDField;
    tblPecDukaniGODINA: TFIBIntegerField;
    tblPecDukaniMESEC: TFIBIntegerField;
    tblPecDukaniREON_ID: TFIBIntegerField;
    qSetupServis: TpFIBQuery;
    qSetupFirma: TpFIBQuery;
    tblDPrsmetkaVodomeriSostojbaSLIKA: TFIBStringField;
    tblDPrsmetkaVodomeriSostojbaIMG: TFIBBlobField;
    fbcdfldTblPresmetkaGVK_POTROSENO: TFIBBCDField;
    TblPresmetkaVodomeriSostojbaSLIKA: TFIBStringField;
    TblPresmetkaVodomeriSostojbaIMG: TFIBBlobField;
    tblPecDukaniLOKACIIN_ID: TFIBIntegerField;
    tblPecDukaniIMA_SLIKA: TFIBSmallIntField;
    TblPresmetkaGIMA_SLIKA: TFIBSmallIntField;
    tblSlikiVovdomeri: TpFIBDataSet;
    dsSlikiVodomeri: TDataSource;
    tblSlikiVovdomeriID: TFIBIntegerField;
    tblSlikiVovdomeriTIP_PARTNER: TFIBIntegerField;
    tblSlikiVovdomeriPARTNER_ID: TFIBIntegerField;
    fbstrngfldSlikiVovdomeriNAZIV_PARTNER: TFIBStringField;
    tblSlikiVovdomeriREON_ID: TFIBIntegerField;
    fbsmlntfldSlikiVovdomeriAKTIVEN: TFIBSmallIntField;
    tblSlikiVovdomeriVODOMERI_ID: TFIBIntegerField;
    tblSlikiVovdomeriSTARA_SOSTOJBA: TFIBIntegerField;
    tblSlikiVovdomeriNOVA_SOSTOJBA: TFIBIntegerField;
    fbcdfldSlikiVovdomeriRAZLIKA: TFIBBCDField;
    tblSlikiVovdomeriGODINA: TFIBIntegerField;
    tblSlikiVovdomeriMESEC: TFIBIntegerField;
    tblSlikiVovdomeriULICA_ID: TFIBIntegerField;
    fbstrngfldSlikiVovdomeriULICA: TFIBStringField;
    fbstrngfldSlikiVovdomeriBROJ: TFIBStringField;
    TblKomVodomeriSERISKI_BROJ: TFIBStringField;
    TblKomVodomeriDATUM_OD: TFIBDateField;
    TblKomVodomeriDATUM_DO: TFIBDateField;
    TblKomReoniTIP_INKASATOR: TFIBIntegerField;
    TblKomReoniPASS: TFIBStringField;
    TblKomReoniUSERNAME: TFIBStringField;
    tblPartnerTS_INS: TFIBDateTimeField;
    tblPartnerTS_UPD: TFIBDateTimeField;
    qSetupGG100: TpFIBQuery;
    TblKomVodomeriVODOMERID: TFIBStringField;
    TblKomReoniID_INKASATOR: TFIBBCDField;
    qSetupGG500: TpFIBQuery;
    TblKomVodomeriSostojbaBR_VODOMER: TFIBStringField;
    TblPresmetkaVodomeriSostojbaBR_VODOMER: TFIBStringField;
    tblDPrsmetkaVodomeriSostojbaBR_VODOMER: TFIBStringField;
    TblKomLokaciiID_INKASATOR: TFIBBCDField;
    dsData: TDataSource;
    tblData: TpFIBDataSet;
    tblDataID_PARTNER: TFIBStringField;
    tblDataIME_PREZIME: TFIBStringField;
    tblDataULICA: TFIBStringField;
    tblDataBROJ_ULICA: TFIBStringField;
    tblDataPAUSALNO: TFIBSmallIntField;
    tblDataID_REON: TFIBIntegerField;
    tblDataNAZIV_REON: TFIBStringField;
    tblDataAKTIVEN: TFIBSmallIntField;
    tblDataID_LOKACIJA: TFIBIntegerField;
    tblDataCITACKA_KNIGA: TFIBIntegerField;
    tblDataID_FIRMA: TFIBStringField;
    tblDataAKTIVNA_LOKACIJA: TFIBSmallIntField;
    tblDataID_INKASATOR: TFIBBCDField;
    tblDataID_VODOMER: TFIBIntegerField;
    tblDataBR_VODOMER: TFIBStringField;
    tblDataCITACKA_KNIGA_RB: TFIBIntegerField;
    tblDataRB2: TFIBFloatField;
    tblInkasatori: TpFIBDataSet;
    tblInkasatoriTIP_PARTNER: TFIBIntegerField;
    tblInkasatoriID: TFIBIntegerField;
    tblInkasatoriNAZIV: TFIBStringField;
    fbstrngfldInkasatoriADRESA: TFIBStringField;
    fbstrngfldInkasatoriTEL: TFIBStringField;
    fbstrngfldInkasatoriFAX: TFIBStringField;
    fbstrngfldInkasatoriDANOCEN: TFIBStringField;
    fbntgrfldInkasatoriMESTO: TFIBIntegerField;
    tblInkasatoriIME: TFIBStringField;
    tblInkasatoriPREZIME: TFIBStringField;
    fbstrngfldInkasatoriTATKO: TFIBStringField;
    tblInkasatoriLOGO: TFIBBlobField;
    fbntgrfldInkasatoriRE: TFIBIntegerField;
    tblInkasatoriLOGOTEXT: TFIBBlobField;
    fbntgrfldInkasatoriSTATUS: TFIBIntegerField;
    tblInkasatoriUSERNAME: TFIBStringField;
    tblInkasatoriPASS: TFIBStringField;
    TBLInkasatoriID_INKASATOR: TFIBStringField;
    tblInkasatoriID_FIRMA: TFIBStringField;
    dsInkasator: TDataSource;
    dsVodomer: TDataSource;
    tblVodomer: TpFIBDataSet;
    tblVodomerID_VODOMER: TFIBIntegerField;
    tblVodomerSTARA_SOSTOJBA: TFIBIntegerField;
    tblVodomerNOVA_SOSTOJBA: TFIBIntegerField;
    tblVodomerGODINA: TFIBIntegerField;
    tblVodomerMESEC: TFIBIntegerField;
    tblPrKorekcijaDANOK_PR: TFIBSmallIntField;
    qSetupLinkPrezemi: TpFIBQuery;
    qSetupLinkSlika: TpFIBQuery;
    tblPecDukaniLINK_SLIKA: TFIBStringField;
    tblPecDukaniIMG: TFIBBlobField;
    TblPresmetkaGLINK_SLIKA: TFIBStringField;
    TblPresmetkaGIMG: TFIBBlobField;
    tblSlikiVovdomeriIMA_SLIKA: TFIBIntegerField;
    tblSlikiVovdomeriIMG: TFIBBlobField;
    tblPregledLokaciiVodomeriID: TFIBIntegerField;
    TblNeplateniZadolzuvanjaPoLokacijaBROJ_FAKTURA: TFIBStringField;
    TblNeplateniZadolzuvanjaPoLokacijaDATUM_PRESMETKA: TFIBDateField;
    TblNeplateniZadolzuvanjaPoLokacijaGODINA: TFIBIntegerField;
    TblNeplateniZadolzuvanjaPoLokacijaMESEC: TFIBIntegerField;
    TblNeplateniZadolzuvanjaPoLokacijaIZNOS_VKUPNO: TFIBBCDField;
    TblNeplateniZadolzuvanjaPoLokacijaPLATENO: TFIBBCDField;
    TblNeplateniZadolzuvanjaPoLokacijaRAZLIKA: TFIBBCDField;
    TblNeplateniZadolzuvanjaPoLokacijaLOKACIJA: TFIBIntegerField;
    TblNeplateniZadolzuvanjaPoLokacijaID: TFIBIntegerField;
    qInsertUplatiTemp: TpFIBQuery;
    tblBrisiUplatiBROJ_FAKTURA: TFIBStringField;
    tblBrisiUplatiDATUM_UPLATA: TFIBDateField;
    tblBrisiUplatiIZNOS: TFIBBCDField;
    tblBrisiUplatiLOKACIJA: TFIBIntegerField;
    tblBrisiUplatiID_TU: TFIBIntegerField;
    tblBrisiUplatiTIP_UPLATA: TFIBStringField;
    tblBrisiUplatiDOKUMENT: TFIBStringField;
    tblBrisiUplatiTIP_PARTNER: TFIBIntegerField;
    tblBrisiUplatiPARTNER_ID: TFIBIntegerField;
    tblBrisiUplatiNAZIV_PARTNER: TFIBStringField;
    tblBrisiUplatiID_FNG: TFIBBCDField;
    tblBrisiUplatiO_REF_ID: TFIBBCDField;
    tblBrisiUplatiO_REF_ID2: TFIBBCDField;
    TblNeplateniZadolzuvanjaPoLokacijaO_REF_ID: TFIBBCDField;
    TblNeplateniZadolzuvanjaPoLokacijaO_REF_ID2: TFIBBCDField;
    TblNeplateniZadolzuvanjaPoLokacijaKONTO: TFIBStringField;
    tblTipUplataKAMATA: TFIBSmallIntField;
    TblNeplateniZadolzuvanjaPoLokacijaTUZBA: TFIBStringField;
    TblNeplateniZadolzuvanjaPoLokacijaDOGOVOR: TFIBStringField;
    TblKomUplatiRE: TFIBIntegerField;
    TblKomUplatiTIP_NALOG: TFIBStringField;
    TblKomUplatiNALOG: TFIBIntegerField;
    TblKomUplatiTS_KNIZENJE: TFIBDateTimeField;
    TblKomUplatiID_FNG: TFIBBCDField;
    qInsertUplTemp: TpFIBStoredProc;
    tblBrisiUplatiID: TFIBBCDField;
    tblBrisiUplatiID_TEMP: TFIBIntegerField;
    tblBrisiUplatiO_KONTO: TFIBStringField;
    tblBrisiUplatiTIP_NALOG: TFIBStringField;
    tblBrisiUplatiNALOG: TFIBIntegerField;
    tblKarParFin: TpFIBDataSet;
    dsKarParFin: TDataSource;
    tblKarParFinPID: TFIBIntegerField;
    tblKarParFinLOKACIJA: TFIBIntegerField;
    tblKarParFinOPIS_LOKACIJA: TFIBStringField;
    tblKarParFinBROJ_FAKTURA: TFIBStringField;
    tblKarParFinTIP_PARTNER: TFIBIntegerField;
    tblKarParFinID: TFIBIntegerField;
    tblKarParFinNAZIV: TFIBStringField;
    tblKarParFinADRESA: TFIBStringField;
    tblKarParFinMAIL: TFIBStringField;
    tblKarParFinDOLZI: TFIBBCDField;
    tblKarParFinPOBARUVA: TFIBBCDField;
    tblKarParFinUPLATENO: TFIBDateField;
    tblKarParFinSALDO: TFIBBCDField;
    tblKarParFinMESEC: TFIBIntegerField;
    tblKarParFinGODINA: TFIBIntegerField;
    tblKarParFinRE: TFIBIntegerField;
    tblKarParFinTIP_NALOG: TFIBStringField;
    tblKarParFinNALOG: TFIBIntegerField;
    tblKarParFinID_FNG: TFIBBCDField;
    tblKarParFinREON_ID: TFIBIntegerField;
    tblKarParFinNAZIV_REON: TFIBStringField;
    tblKarParFinCITACKA_KNIGA: TFIBIntegerField;
    tblKarParFinAKTIVEN: TFIBSmallIntField;
    tblKarParFinDATUM_PRESMETKA: TFIBDateField;
    tblPecDukaniLOK_ULICA: TFIBStringField;
    tblPecDukaniLOK_BROJ: TFIBStringField;
    TblPresmetkaGLOK_ULICA: TFIBStringField;
    TblPresmetkaGLOK_BROJ: TFIBStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure openTblUliciAll();
    procedure openTblUliciPoReon(reon:String);
    procedure openTblUslugiAll();
    procedure openTblUslugiPoVidUsluga(vidUsluga:String);
    procedure TblKomLokaciiAfterScroll(DataSet: TDataSet);
    procedure TblKomLokaciiREON_IDChange(Sender: TField);
    //procedure cxDBTextAllEnter(Sender: TObject);
    //procedure openTblPresmetkaPoReon(reon:String);
    function isCenovnikGenerated(godina:String; mesec:String):boolean;
    procedure closeDataSetIfActive(dataSet:TDataSet);
    procedure ShowReporter;
    procedure ShowParam(tbl:String;br:Integer);
    procedure ShowReport(tbl:String;br:integer);
    procedure ShowReport2(tbl:String;br:integer; p1:String; v1: String);
    procedure ShowReport4(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String);
    procedure ShowReport6(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String; p3:String; v3:String);
    procedure ShowReport8(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String; p3:String; v3:String; p4:String; v4:String);
    procedure ShowReport10(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String; p3:String; v3:String; p4:String; v4:String;  p5:String; v5:String);
    procedure TblDopUslugiNewRecord(DataSet: TDataSet);
    function zemiMax(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, max : Variant):Variant;
    procedure TableBiforeDelete(DataSet: TDataSet);
    procedure tblTipUplataNewRecord(DataSet: TDataSet);
    procedure TblKomLokaciiNewRecord(DataSet: TDataSet);
    procedure TblKomLokaciiAfterPost(DataSet: TDataSet);
    procedure TblKomVodomeriBeforeDelete(DataSet: TDataSet);
    function IsInternetConnected: boolean;
    function GetURLAsString(aURL: string): string;
    procedure ZemiTextFile(url: String);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
    function CompressJpeg(OutJPG: TJPEGImage): Integer;
    //procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
  private
    { Private declarations }
  public
    { Public declarations }
    var selectUliciAll:String;
        selectUliciPoReon:String;
        selectVodomeriPoLokacija:String;
        selectVodomeriSostojbaPoReonCitackaKniga:String;
        selectUslugi:String;
        selectUslugiPoLokacija:String;
        selectLokacijaPoPartner:String;
        //listMeseci:TStrings;
        listMeseci:TStringList;
//        const link = 'http://www.myhtmldatabase.com/cgi-bin/print.cgi?client=kodeksTest&action0=saveImport&readMemory=0&refreshRows=no&formN=1';

  end;

var
  DM1: TDM1;
  fizicki_od,pravni_od:TDate;
  link_insert, CelLink, vraten_text, link_edit, link_zemi, link_prezemi, link_sliki, sos_int, servis, firma, gorna_granica1, gorna_granica5,
  ima_promena : string;


implementation

uses dmMaticni, dmKonekcija, uRepo, DaNe;

{$R *.dfm}

procedure TDM1.DataModuleCreate(Sender: TObject);
begin

    selectUliciAll := 'SELECT ID, NAZIV,REON_ID FROM KOM_ULICI';
    selectUliciPoReon := 'SELECT ID, NAZIV,REON_ID FROM KOM_ULICI WHERE REON_ID=';

    selectVodomeriPoLokacija := 'SELECT LOKACIIN_ID,ID,STATUS, PAUSAL,tip_partner,partner,seriski_broj,reon_id,LOKACIIN_ID_T, datum_od, datum_do,vodomerid FROM KOM_VODOMERI WHERE LOKACIIN_ID=';
     {
    selectVodomeriSostojbaPoReonCitackaKniga := 'SELECT (SELECT KOM_GET_REDEN_BROJ_PO_KNIGA.REDEN_BROJ '+
                                                'FROM KOM_GET_REDEN_BROJ_PO_KNIGA(KL.CITACKA_KNIGA,KL.CITACKA_KNIGA_RB,0)),KL.REON_ID,KL.CITACKA_KNIGA,KL.CITACKA_KNIGA_RB '+
                                                ',KS.LOKACIIN_ID,KS.VODOMERI_ID, KS.STARA_SOSTOJBA, KS.NOVA_SOSTOJBA, KS.RAZLIKA, KS.GODINA,KS.MESEC '+
                                                ',KL.TIP_PARTNER, KL.PARTNER_ID, P.NAZIV,(CASE WHEN KL.AKTIVEN=1 THEN ''��'' ELSE ''��'' END) LOKACIJA_AKTIVEN, KV.STATUS VODOMER_STATUS '+
                                                'FROM KOM_VODOMERI_SOSTOJBA KS, KOM_VODOMERI KV, KOM_LOKACII KL, MAT_PARTNER P '+
                                                'WHERE KS.VODOMERI_ID=KV.ID '+
                                                'AND KS.LOKACIIN_ID=KV.LOKACIIN_ID '+
                                                'AND KV.LOKACIIN_ID=KL.ID '+
                                                'AND KL.TIP_PARTNER=P.TIP_PARTNER '+
                                                'AND KL.PARTNER_ID=P.ID';
    }

    selectVodomeriSostojbaPoReonCitackaKniga := 'SELECT KL.REON_ID,KL.CITACKA_KNIGA, KL.CITACKA_KNIGA_RB, KL.RB1'+
                                                ',KS.LOKACIIN_ID,KS.VODOMERI_ID, KS.STARA_SOSTOJBA, KS.NOVA_SOSTOJBA, KS.RAZLIKA, KS.GODINA,KS.MESEC '+
                                                ',KL.TIP_PARTNER, KL.PARTNER_ID, P.NAZIV,(CASE WHEN KL.AKTIVEN=1 THEN ''��'' ELSE ''��'' END) LOKACIJA_AKTIVEN, KV.STATUS VODOMER_STATUS, P.ADRESA, KV.PAUSAL ,COALESCE(KL.BROJ_NA_CLENOVI,0) BROJ_NA_CLENOVI '+
                                                'FROM KOM_VODOMERI_SOSTOJBA KS, KOM_VODOMERI KV, KOM_LOKACII KL, MAT_PARTNER P , KOM_LOKACIJA_ARHIVA KLA'+
                                                'WHERE KS.VODOMERI_ID=KV.ID '+
                                                'AND KS.LOKACIIN_ID=KV.LOKACIIN_ID '+
                                                'AND KLA.ID_LOKACIJA=KL.ID '+
                                                'AND KV.LOKACIIN_ID=KLA.ID_LOKACIJA '+
                                                'AND KLA.TIP_PARTNER=P.TIP_PARTNER '+
                                                'AND KLA.PARTNER=P.ID '+
                                                ' and (((encodedate(1,ks.mesec,ks.godina) between encodedate(1,kla.poc_mesec,kla.poc_godina) and encodedate(1,kla.kraj_mesec,kla.kraj_godina)) and kla.kraj_mesec is not null) '+
                                                ' or ((encodedate(1,ks.mesec, ks.godina) >= encodedate(1,kla.poc_mesec,kla.poc_godina)) and (kla.kraj_mesec is null) and (kla.poc_mesec is not null) )) ';


    selectUslugi := 'SELECT ID, VID_USLUGA, NAZIV FROM KOM_USLUGI';

    selectUslugiPoLokacija :='SELECT KU.LOKACIIN_ID,KU.VID_USLUGA,KU.USLUGA,NAZIV FROM KOM_LOKACII_USLUGI KU INNER JOIN KOM_USLUGI U ON U.ID=KU.USLUGA AND U.VID_USLUGA=KU.VID_USLUGA WHERE LOKACIIN_ID=';
     {
    listMeseci := TStringList.Create();
    listMeseci.AddObject('1','jan');
    listMeseci.Add('1 - ���');
    listMeseci.Add('2 - ���');
    }
    selectLokacijaPoPartner := 'SELECT '
end;

procedure TDM1.openTblUliciAll();
begin
    TblKomUlici.Close();
    TblKomUlici.SelectSQL.Text := DM1.selectUliciAll;
    TblKomUlici.Open;
end;

procedure TDM1.TableBiforeDelete(DataSet: TDataSet);
begin

    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal = 7) then
        Abort;
end;

procedure TDM1.TblDopUslugiNewRecord(DataSet: TDataSet);
begin
   //TblDopUslugiID.Value:=zemiMax(PMaxDopUsluga,Null,Null,Null,Null,Null,Null,'MAKS')
end;

procedure TDM1.TblKomLokaciiAfterPost(DataSet: TDataSet);
begin
       // TransakcijaQ.StartTransaction;

     {    pUpdate.Close;
         pUpdate.ParamByName('MAS_ID').Value:= TblKomLokaciiID.AsString;
         pUpdate.ExecQuery;

        // TransakcijaQ.StartTransaction;
         pUpdatePS.Close;
         pUpdatePS.ParamByName('MAS_ID').Value:= TblKomLokaciiID.AsString;
         pUpdatePS.ExecQuery;

        // TransakcijaQ.StartTransaction;
         pUpdateP.Close;
         pUpdateP.ParamByName('MAS_ID').Value:= TblKomLokaciiID.AsString;
         pUpdateP.ExecQuery;

       //  TransakcijaQ.StartTransaction;
         pUpdateS.Close;
         pUpdateS.ParamByName('MAS_ID').Value:= TblKomLokaciiID.AsString;
         pUpdateS.ExecQuery;

         pKoren.Close;
         pKoren.ParamByName('l').Value:=TblKomLokaciiID.AsString;
         pKoren.ExecProc;  }

         ProcNovRb2.ExecProc;
   end;

procedure TDM1.TblKomLokaciiAfterScroll(DataSet: TDataSet);
begin
    if (TblKomLokaciiREON_ID.AsString <> '') then
    begin
        openTblUliciPoReon(TblKomLokaciiREON_ID.AsString);
    end;
end;

procedure TDM1.TblKomLokaciiNewRecord(DataSet: TDataSet);
begin
  //06.05.2019 - nova lokacija na trigger
    TblKomLokaciiID.Value := zemiMax(pMaxLokacii,Null,Null,Null,Null,Null,Null,'MAKS');

end;

procedure TDM1.TblKomLokaciiREON_IDChange(Sender: TField);
begin
    //ShowMessage('ce otvara na onchange so '+TblKomLokaciiREON_ID.AsString);
    openTblUliciPoReon(TblKomLokaciiREON_ID.AsString);
end;

procedure TDM1.TblKomVodomeriBeforeDelete(DataSet: TDataSet);
var mess:string;
begin
  qProveriVodomer.Close;
  qProveriVodomer.ParamByName('v').Value:=TblKomVodomeriID.Value;
  qProveriVodomer.ExecQuery;
  if qProveriVodomer.RecordCount=1 then
  begin
      mess:='�� ��������� ����� ������� �������. '+#10#13;
      mess:=mess+'���� ��������� �� ��������� �������!';
      ShowMessage(mess);
      Abort;
  end;

end;

procedure TDM1.tblTipUplataNewRecord(DataSet: TDataSet);
begin
    dm1.pMaxTipUplata.Close;
    dm1.pMaxTipUplata.ExecProc;
    dm1.tblTipUplataID.Value:=dm1.pMaxTipUplata.FldByName['MAKS'].Value+1;
end;

procedure TDM1.openTblUliciPoReon(reon:string);
begin
    TblKomUlici.Close();
    TblKomUlici.SelectSQL.Text := selectUliciPoReon+reon;
    TblKomUlici.Open();
end;

procedure TDM1.openTblUslugiAll();
begin
    if TblKomUslugi.Active then
    begin
        TblKomUslugi.Close();
    end;
    TblKomUslugi.SelectSQL.Text := selectUslugi;
    TblKomUslugi.Open();
end;

procedure TDM1.openTblUslugiPoVidUsluga(vidUsluga:String);
begin
    if TblKomUslugi.Active then
    begin
        TblKomUslugi.Close();
    end;
    TblKomUslugi.SelectSQL.Text := selectUslugi+' WHERE VID_USLUGA='+vidUsluga;
    TblKomUslugi.Open();
end;

function TDM1.isCenovnikGenerated(godina:String; mesec:String):boolean;
var ret:boolean;
begin
    ret := true;
    
    QGetNotGeneratedCenovnik.ParamByName('GODINA').AsString := godina;
    QGetNotGeneratedCenovnik.ParamByName('MESEC').AsString := mesec;
    QGetNotGeneratedCenovnik.ExecQuery;
    if (QGetNotGeneratedCenovnik.RecordCount > 0) then
    begin
        ret := false;
    end;

    isCenovnikGenerated := ret;
end;


procedure TDM1.closeDataSetIfActive(dataSet:TDataSet);
begin
    if dataSet.Active then
    begin
        dataSet.Close;
    end;

end;

procedure TDM1.ShowReporter;
begin
    frmRepo := TfrmRepo.Create(Self);
    frmRepo.ShowReport;
    frmRepo.Free;
end;

procedure TDM1.ShowParam(tbl:String;br:Integer);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.ShowParamEdit;
   frmRepo.Free;
end;

procedure TDM1.ShowReport(tbl:String;br:integer);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.ShowReport;
   frmRepo.Free;
end;

procedure TDM1.ShowReport2(tbl:String;br:integer; p1:String; v1: String);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.Param(p1,v1);
   frmRepo.ShowReport;
   frmRepo.Free;
end;

procedure TDM1.ShowReport4(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.Param(p1,v1);
   frmRepo.Param(p2,v2);
   frmRepo.ShowReport;
   frmRepo.Free;
end;

procedure TDM1.ShowReport6(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String; p3:String; v3:String);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.Param(p1,v1);
   frmRepo.Param(p2,v2);
   frmRepo.Param(p3,v3);
   frmRepo.ShowReport;
   frmRepo.Free;
end;



procedure TDM1.ShowReport8(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String; p3:String; v3:String; p4:String; v4:String);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.Param(p1,v1);
   frmRepo.Param(p2,v2);
   frmRepo.Param(p3,v3);
   frmRepo.Param(p4,v4);
   frmRepo.ShowReport;
   frmRepo.Free;
end;


procedure TDM1.ShowReport10(tbl:String;br:integer; p1:String; v1: String; p2:String; v2:String; p3:String; v3:String; p4:String; v4:String;  p5:String; v5:String);
begin
   frmRepo:=TfrmRepo.Create(Self);
   frmRepo.Spremaj2(tbl,br);
   frmRepo.Param(p1,v1);
   frmRepo.Param(p2,v2);
   frmRepo.Param(p3,v3);
   frmRepo.Param(p4,v4);
   frmRepo.Param(p5,v5);
   frmRepo.ShowReport;
   frmRepo.Free;
end;

function TDM1.zemiMax(Proc : TpFIBStoredProc; p1, p2, p3, v1, v2, v3, max : Variant):Variant;
var
    ret : integer;
begin
    if(not VarIsNull(p1)) then
        Proc.ParamByName(VarToStr(p1)).Value := v1;
    if(not VarIsNull(p2)) then
        Proc.ParamByName(VarToStr(p2)).Value := v2;
    if(not VarIsNull(p3)) then
        Proc.ParamByName(VarToStr(p3)).Value := v3;
    Proc.ExecProc;
    if(Proc.ParamByName(max).Value = Null) then
        ret := 1
    else
        ret := Proc.ParamByName(max).Value + 1;
    result := ret;
end;
function Tdm1.IsInternetConnected: boolean;
 var
    dw_ConnectionTypes : DWORD;
    begin
    dw_ConnectionTypes:=
    INTERNET_CONNECTION_LAN +
    INTERNET_CONNECTION_MODEM +
    INTERNET_CONNECTION_PROXY;
    Result:=InternetGetConnectedState(@dw_ConnectionTypes,0);
    end;

function Tdm1.GetURLAsString(aURL: string): string;
var
  lHTTP: TIdHTTP;
begin
  lHTTP := TIdHTTP.Create(nil);
  try
    Result := lHTTP.Get(aURL);
  finally
    FreeAndNil(lHTTP);
  end;
end;

procedure Tdm1.ZemiTextFile(url: String);
//var vraten_text : string;
begin
//-----------------------------------------------------------------------
// ��������� �� �������� �� URL, �������� ��������� ������ ���������,
// �� ������ �����, ����, ������ � �����
//-----------------------------------------------------------------------
  vraten_text := GetURLAsString(url);
//  ShowMessage(vraten_text);
end;

procedure Tdm1.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
//          if(tabela.Locate('TIP_PARTNER;ID',VarArrayOf([tip.text,sifra.text]),[])) then
//               lukap.Text:=tabela.FieldByName('NAZIV').Value;
            lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
         end
         else
         begin
            lukap.Clear;
         end;
end;

function tdm1.CompressJpeg(OutJPG: TJPEGImage): Integer;
VAR tmpQStream: TMemoryStream;
begin
 tmpQStream:= TMemoryStream.Create;
 TRY
   OutJPG.Compress;
   OutJPG.SaveToStream(tmpQStream);
   OutJPG.SaveToFile('c:\cxCache\MyImage'+dm1.tblKarParLOKACIJA.AsString+'.jpg');    // You can remove this line.
   tmpQStream.Position := 0;                //
   OutJPG.LoadFromStream(tmpQStream);       // Reload the jpeg stream to OutJPG
   Result:= tmpQStream.Size;
 FINALLY
   FreeAndNil(tmpQStream);
 END;
end;


end.
