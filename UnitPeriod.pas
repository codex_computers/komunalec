unit UnitPeriod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Menus, cxLabel, StdCtrls,
  cxButtons, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxGroupBox,
  cxImageComboBox, ActnList, PlatformDefaultStyleActnCtrls, ActnMan;

type
  TfrmPeriod = class(TForm)
    Panel1: TPanel;
    cxGroupBox1: TcxGroupBox;
    txtOdDatum: TcxDateEdit;
    txtDoDatum: TcxDateEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    ActionManager1: TActionManager;
    aPecati: TAction;
    txtTp: TcxImageComboBox;
    aDizajner: TAction;
    aIzlez: TAction;
    procedure aPecatiExecute(Sender: TObject);
    procedure aDizajnerExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aIzlezExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPeriod: TfrmPeriod;

implementation

uses dmKonekcija, dmMaticni, dmReportUnit, dmResources;

{$R *.dfm}

procedure TfrmPeriod.aDizajnerExecute(Sender: TObject);
begin
  if (txtOdDatum.Text='') or (txtDoDatum.Text='') then
   begin
       ShowMessage('��������� �� �������������� ����� �� ������');
       txtOdDatum.SetFocus;
       Abort;
   end
   else
   if (txtOdDatum.EditValue>txtDoDatum.EditValue) then
   begin
       ShowMessage('���������� ������ ������!');
       txtOdDatum.SetFocus;
       Abort;
   end
   else
   begin
      dmReport.Spremi(17,dmKon.aplikacija);
      dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
      dmReport.tblSqlReport.Params.ParamByName('do_datum').AsString:= txtDoDatum.Text;
      if txtTp.Text='' then
          dmReport.tblSqlReport.Params.ParamByName('tp').AsString:= '%'
      else
         dmReport.tblSqlReport.Params.ParamByName('tp').AsInteger:= txtTp.Properties.Items.Items[txtTp.ItemIndex].Tag;
      dmReport.tblSqlReport.Open;
      dmReport.frxReport1.Variables.Items[1].Value:=QuotedStr(txtOdDatum.Text);
      dmReport.frxReport1.Variables.Items[2].Value:=QuotedStr(txtDoDatum.Text);
      dmReport.frxReport1.Variables.Items[3].Value:=QuotedStr(txttp.Text);
      dmReport.frxReport1.DesignReport();
   end;
end;

procedure TfrmPeriod.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmPeriod.aPecatiExecute(Sender: TObject);
begin
   if (txtOdDatum.Text='') or (txtDoDatum.Text='') then
   begin
       ShowMessage('��������� �� �������������� ����� �� ������');
       txtOdDatum.SetFocus;
       Abort;
   end
   else
   if (txtOdDatum.EditValue>txtDoDatum.EditValue) then
   begin
       ShowMessage('���������� ������ ������!');
       txtOdDatum.SetFocus;
       Abort;
   end
   else
   begin
   dmReport.Spremi(17,dmKon.aplikacija);
   dmReport.tblSqlReport.Params.ParamByName('od_datum').AsString:= txtOdDatum.Text;
   dmReport.tblSqlReport.Params.ParamByName('do_datum').AsString:= txtDoDatum.Text;
    if txtTp.Text='' then
          dmReport.tblSqlReport.Params.ParamByName('tp').AsString:= '%'
      else
         dmReport.tblSqlReport.Params.ParamByName('tp').AsInteger:= txtTp.Properties.Items.Items[txtTp.ItemIndex].Tag;
   dmReport.tblSqlReport.Open;
   dmReport.frxReport1.Variables.Items[1].Value:=QuotedStr(txtOdDatum.Text);
   dmReport.frxReport1.Variables.Items[2].Value:=QuotedStr(txtDoDatum.Text);
   dmReport.frxReport1.Variables.Items[3].Value:=QuotedStr(txttp.Text);
   dmReport.frxReport1.ShowReport();
   end;
end;

procedure TfrmPeriod.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

end.
