unit UnitVodomeriSostojbaVnes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinsdxBarPainter, cxClasses, dxBar, cxGraphics, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, ActnList, StdCtrls, cxCheckBox, DateUtils, Menus,
  cxLookAndFeelPainters, cxButtons, dxSkinscxPCPainter, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxGridCustomView, cxGrid,
  cxDBEdit, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinSilver, dxSkinStardust, dxSkinSummer2008,
  dxSkinValentine, dxSkinXmas2008Blue, cxLookAndFeels, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp,
  dxSkinSpringTime, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  Vcl.CustomizeDlg, cxMemo, StrUtils, FIBQuery, pFIBQuery, WinInet, httputil,iduri,
  IdCoder, IdCoder3to4, IdCoderBinHex4,IdIcmpClient, IdRawBase, IdRawClient,
 // dxSkinOffice2013White,
  cxNavigator, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, System.Actions, Vcl.Imaging.jpeg,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc,  FIBDataSet,
  pFIBDataSet, Vcl.DBCtrls, Soap.EncdDecd, IdCoderMIME, System.IOUtils,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light; //, System.Actions;

type
  TFrmVodomeriSostojba = class(TForm)
    StatusBar1: TStatusBar;
    panelGlaven: TPanel;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    panelGoren: TPanel;
    cxLookupCBReoni: TcxLookupComboBox;
    BarButtonIzlez: TdxBarButton;
    SubListAkcii: TdxBarSubItem;
    lblReon: TLabel;
    cxCheckBoxZaCitackaKniga: TcxCheckBox;
    cxLookupCBCitackaKniga: TcxLookupComboBox;
    Label1: TLabel;
    lblGodina: TLabel;
    lblMesec: TLabel;
    cxComboBoxMesec: TcxComboBox;
    cxComboBoxGodini: TcxComboBox;
    cxBtnPartneri: TcxButton;
    panelDolen: TPanel;
    panelDolenGore: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1VODOMERI_ID: TcxGridDBColumn;
    cxGrid1DBTableView1STARA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1NOVA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    dxBarButtonZapisiPodatoci: TdxBarButton;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    panelDolenDole: TPanel;
    lblNovaSostojba: TLabel;
    cxControlNOVA_SOSTOJBA: TcxDBTextEdit;
    lblStaraSostojba: TLabel;
    cxControlSTARA_SOSTOJBA: TcxDBTextEdit;
    dxBarButton: TdxBarButton;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    lblNaziv: TLabel;
    cxControlNAZIV: TcxDBTextEdit;
    GroupBox1: TGroupBox;
    cxTxtNazivPredhoden: TcxTextEdit;
    cxTxtRazlikaPredhoden: TcxTextEdit;
    cxTxtStaraPredhoden: TcxTextEdit;
    cxTxtNovaPredhoden: TcxTextEdit;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxControlADRESA: TcxDBTextEdit;
    Label2: TLabel;
    cxBtnZapisiPodatoci: TcxButton;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CITACKA_KNIGA: TcxGridDBColumn;
    cxGrid1DBTableView1LOKACIIN_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1VODOMER_STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1PAUSAL: TcxGridDBColumn;
    CustomizeDlg1: TCustomizeDlg;
    IdHTTP1: TIdHTTP;
    cxButton1: TcxButton;
    cxMemo1: TcxMemo;
    qUpdateVS: TpFIBUpdateObject;
    IdIcmpClient1: TIdIcmpClient;
    ActionList1: TActionList;
    aIzlez: TAction;
    aZapisiPodatoci: TAction;
    aAzuriraj: TAction;
    aLokacii: TAction;
    aAzurirajPoedinecno: TAction;
    aCenovnik: TAction;
    aBrisi: TAction;
    aVratenTekst: TAction;
    qProveriVnes: TpFIBQuery;
    cxGrid1DBTableView1AVTOMATSKI_VNES: TcxGridDBColumn;
    qProveriSostojba: TpFIBQuery;
    aPogledVS: TAction;
    cxButton2: TcxButton;
    XMLServis: TXMLDocument;
    tblVodSost: TpFIBDataSet;
    dsVodSost: TDataSource;
    tblVodSostIMG: TFIBBlobField;
    tblVodSostLOKACIIN_ID: TFIBIntegerField;
    tblVodSostVODOMERI_ID: TFIBIntegerField;
    img1: TImage;
    dbimgIMG: TDBImage;
    IdHTTP2: TIdHTTP;
    IdDecoderMIME1: TIdDecoderMIME;
    dlgSave1: TSaveDialog;
    qUpdateVSSS: TpFIBUpdateObject;
    qUpdateImaSlika: TpFIBQuery;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxCheckBoxZaCitackaKnigaPropertiesChange(Sender: TObject);
    procedure cxLookupCBReoniPropertiesChange(Sender: TObject);
    procedure setCitackaKnigaLista(reon:String);
    procedure onKeyDownLookUpAll(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure cxBtnPartneriClick(Sender: TObject);
    function podatociOK():boolean;
    procedure zapisiPodatoci();
    procedure noviPodatoci();
    procedure aZapisiPodatociExecute(Sender: TObject);
    procedure enablePanelGore(enable:Boolean);
    procedure aAzurirajExecute(Sender: TObject);
    procedure enableControlsSostojba(enable:Boolean);
    procedure onEnterAll(Sender: TObject);
    procedure onExitAll(Sender: TObject);
    procedure onKeyDownAll(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure callFrmLokacii();
    procedure aLokaciiExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure getFirstNotSetSostojba();
    procedure cxLookupCBReoniPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure setPredhodenSostojba(reden_broj: integer);
    procedure clearPredhodenSostojba();
    procedure aAzurirajPoedinecnoExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure ZemiTextFile(url: String);
    procedure aVratenTekstExecute(Sender: TObject);
    function GetURLAsString(aURL: string): string;
    function ImaBeleska : boolean;
    function PostURLAsString(aURL: string): string;
    procedure IdIcmpClient1Reply(ASender: TComponent;
      const AReplyStatus: TReplyStatus);
    procedure aPogledVSExecute(Sender: TObject);
    function Base64ToBitmap(const S: string): TJPEGImage;
    function Decode(const Input: AnsiString): string;
  private
    { Private declarations }
    var reon_id,citacka_kniga,godina,mesec:String;
  public
    { Public declarations }
    var gridFilled, poedinecno:boolean;
        vraten_text:string;
        ima:Boolean;
  end;

var
  FrmVodomeriSostojba: TFrmVodomeriSostojba;

implementation

uses UnitDM1, DaNe, UnitLokacii, UnitThreadAccessDataBase, dmKonekcija, //dmLook,
  dmMaticni, ProveriSostojbi;

{$R *.dfm}

function threadProcedure(P : Pointer) : LongInt; stdcall;
var vodomer_id,lokaciin_id:String;
begin
    DM1.TblKomVodomeriSostojba.Open;
    //FrmVodomeriSostojba.getFirstNotSetSostojba();
    //TMultiReadExclusiveWriteSynchronizer.
    //TThread.Synchronize(P,nil);   ]
    //trtl
    {
    DM1.QGetFirstNotSetSostojba.ParamByName('REON_ID').AsString := FrmVodomeriSostojba.cxLookupCBReoni.EditValue;
    DM1.QGetFirstNotSetSostojba.ParamByName('CITACKA_KNIGA').AsString := FrmVodomeriSostojba.cxLookupCBCitackaKniga.EditValue;
    DM1.QGetFirstNotSetSostojba.ParamByName('GODINA').AsString := FrmVodomeriSostojba.cxComboBoxGodini.EditValue;
    DM1.QGetFirstNotSetSostojba.ParamByName('MESEC').AsString := FrmVodomeriSostojba.cxComboBoxMesec.EditValue;
    }
    //initi
    DM1.QGetFirstNotSetSostojba.ParamByName('REON_ID').AsString := FrmVodomeriSostojba.reon_id;
    DM1.QGetFirstNotSetSostojba.ParamByName('CITACKA_KNIGA').AsString := FrmVodomeriSostojba.citacka_kniga;
    DM1.QGetFirstNotSetSostojba.ParamByName('GODINA').AsString := FrmVodomeriSostojba.godina;
    DM1.QGetFirstNotSetSostojba.ParamByName('MESEC').AsString := FrmVodomeriSostojba.mesec;

    DM1.QGetFirstNotSetSostojba.ExecQuery();

    vodomer_id := DM1.QGetFirstNotSetSostojba.FieldByName('VODOMERI_ID').AsString;
    lokaciin_id := DM1.QGetFirstNotSetSostojba.FieldByName('LOKACIIN_ID').AsString;

    DM1.TblKomVodomeriSostojba.Locate('LOKACIIN_ID;VODOMERI_ID;GODINA;MESEC',VarArrayOf([lokaciin_id,vodomer_id,VarToStr(FrmVodomeriSostojba.cxComboBoxGodini.EditValue),VarToStr(FrmVodomeriSostojba.cxComboBoxMesec.EditValue)]),[]);

    threadProcedure := 1;
end;


procedure TFrmVodomeriSostojba.aAzurirajExecute(Sender: TObject);
begin
//---------------------------------------------------------------------------------------------
// �� �� ������� ���� �� ���������, ������� � �������� ��� ��� ��������� ������� (���������)
//---------------------------------------------------------------------------------------------
 if not ImaBeleska then
 begin
   if ((DM1.TblKomVodomeriSostojbaLOKACIJA_AKTIVEN.AsString = '��') or (DM1.TblKomVodomeriSostojbaVODOMER_STATUS.Value <> 1)) then
   begin

    if DM1.TblKomVodomeriSostojbaLOKACIJA_AKTIVEN.AsString = '��' then
    begin
        frmDaNe := TfrmDaNe.Create(self,'��������� �������','��������� � �������� ���� ���������. ���� ������� ������ �� ��� �� �������� ���� �������?',1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            if (DM1.TblKomVodomeriSostojba.RecNo = DM1.TblKomVodomeriSostojba.RecordCount) then
            begin
                if DM1.tblPausalPAUSAL.AsString = '' then
                      enableControlsSostojba(false);
                cxGrid1.SetFocus();
            end
            else
            begin
                DM1.TblKomVodomeriSostojba.Next();
                aAzuriraj.Execute();
            end;
            Abort();
        end;
    end
   else
   if DM1.TblKomVodomeriSostojbaVODOMER_STATUS.Value <> 1 then
   begin
       ShowMessage('�� ������� �������!');
       DM1.TblKomVodomeriSostojba.Next();
       aAzuriraj.Execute();
   end;
  end;


    if cxControlNOVA_SOSTOJBA.Enabled = false then
    begin
        enableControlsSostojba(true);
    end;

    if cxGrid1.Enabled then
    begin
        cxGrid1.Enabled := false;
    end;

     {
    if cxGrid1DBTableView1.DataController.DataSet.RecNo > 2 then
    begin
        ShowMessage(cxGrid1DBTableView1.DataController.Values[cxGrid1DBTableView1.DataController.DataSet.RecNo-2,0]);
    end;
    }

   { if poedinecno then
    begin
          dm1.qDaliImaPresmetka.Close;
          dm1.qDaliImaPresmetka.ParamByName('l').AsString:=DM1.TblKomVodomeriSostojbaLOKACIIN_ID.AsString;
          dm1.qDaliImaPresmetka.ParamByName('m').AsString:=cxComboBoxMesec.Text;
          dm1.qDaliImaPresmetka.ParamByName('g').AsString:=cxComboBoxGodini.text;
          dm1.qDaliImaPresmetka.ExecQuery;

     if dm1.qDaliImaPresmetka.FldByName['BROJ_FAKTURA'].IsNull then

    end;   }

     DM1.TblKomVodomeriSostojba.Edit;
    if dm1.TblKomVodomeriSostojbaPAUSAL.Value=1 then
    begin
        dm1.tblPausal.Close;
        dm1.tblPausal.Open;
        if DM1.tblPausalPAUSAL.AsString = '' then
        begin
            ShowMessage('��������� �� ��������� � ������! ������� �������� �� ������  �� ����������, �� ����� '+dm1.TblKomVodomeriSostojbaMESEC.AsString+' � '+dm1.TblKomVodomeriSostojbaGODINA.AsString+' ������!');
            enableControlsSostojba(false);
             cxGrid1.Enabled := true;
             cxGrid1.SetFocus();
        end
        else
        begin
            ShowMessage('��������� �� ��������� � ������!');
            dm1.TblKomVodomeriSostojbaSTARA_SOSTOJBA.Value:=0;
            dm1.TblKomVodomeriSostojbaNOVA_SOSTOJBA.Value:=DM1.tblPausalPAUSAL.Value*dm1.TblKomVodomeriSostojbaBROJ_NA_CLENOVI.value;
            //����� �� �������� �������� ���� ������� � ��������
            if(DM1.TblKomVodomeriSostojba.RecNo < DM1.TblKomVodomeriSostojba.RecordCount) then
             begin
                DM1.TblKomVodomeriSostojba.Next();
                aAzuriraj.Execute();
             end;

        end;
    end
    else
    begin
        cxControlNOVA_SOSTOJBA.SetFocus();
        cxControlNOVA_SOSTOJBA.SelectAll();
    end;
 end
 else
 begin
   ShowMessage('�� ���������� '+dm1.TblKomVodomeriSostojbaNAZIV.AsString+', �� '+  DM1.TblKomVodomeriSostojbaMESEC.AsString+ ' �����, '+DM1.TblKomVodomeriSostojbaGODINA.AsString+' ������ ��� ��������� ���������!'+#10#13+
               ' �� ����� ���� �������, �������� � ���� �� �� ������� �����������.');
   if (not DM1.TblKomVodomeriSostojba.Eof) then
   begin
//   if  then

     DM1.TblKomVodomeriSostojba.Next;
     if (dm1.TblKomVodomeriSostojbaRAZLIKA.AsInteger = 0) then
         aAzuriraj.Execute();
   end
   else
   begin
     //dgdsg
   end;
 end;
end;

procedure TFrmVodomeriSostojba.aAzurirajPoedinecnoExecute(Sender: TObject);
begin
   poedinecno:=True;
   aAzurirajExecute(Sender);
end;

procedure TFrmVodomeriSostojba.aBrisiExecute(Sender: TObject);
begin
if not ImaBeleska then
      DM1.TblKomVodomeriSostojba.delete
   else
   begin
        ShowMessage('�� ���������� '+dm1.TblKomVodomeriSostojbaNAZIV.AsString+',  �� '+  DM1.TblKomVodomeriSostojbaMESEC.AsString+ ' �����, '+DM1.TblKomVodomeriSostojbaGODINA.AsString+' ������ ��� ��������� ���������!'+#10#13+
               ' �� ����� ���� �������, �������� � ���� �� �� ������� �����������.');
        Abort;
   end;

end;

procedure TFrmVodomeriSostojba.aIzlezExecute(Sender: TObject);
begin
    if gridFilled then
    begin
    {
        frmDaNe := TfrmDaNe.Create(self,'���������� ��������','���� ������ �� �� �������� ����������?',1);
        if (frmDaNe.ShowModal = mrYes) then
        begin
             zapisiPodatoci();
        end;
        }
        //gridFilled := false;
        if DM1.TblKomVodomeriSostojba.State = dsEdit then
        begin
            DM1.TblKomVodomeriSostojba.Cancel;
            cxGrid1.Enabled := true;
            cxGrid1.SetFocus();
            enableControlsSostojba(false);
        end
        else
        begin
            enablePanelGore(true);
            noviPodatoci();
        end;
      //Abort();
    end
    else
     close;
end;

procedure TFrmVodomeriSostojba.aLokaciiExecute(Sender: TObject);
begin
    if gridFilled then
    begin
        callFrmLokacii();
    end
    else
    begin
        ShowMessage('������ �������� �������!');
    end;
end;

procedure TFrmVodomeriSostojba.aPogledVSExecute(Sender: TObject);
begin
    if (podatociOK) then
    begin

//         poedinecno:=False;
//
//         DM1.ProcKomPopolniVodomeriSostojba.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue;
//         if cxCheckBoxZaCitackaKniga.Checked = true then
//         begin
//              DM1.ProcKomPopolniVodomeriSostojba.ParamByName('CITACKA_KNIGA').AsVariant := cxLookupCBCitackaKniga.Text;
//         end
//         else
//         begin
//              DM1.ProcKomPopolniVodomeriSostojba.ParamByName('CITACKA_KNIGA').AsVariant := -1;
//         end;
//
//         DM1.ProcKomPopolniVodomeriSostojba.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
//         DM1.ProcKomPopolniVodomeriSostojba.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;
//
//         DM1.ProcKomPopolniVodomeriSostojba.Prepare();
//         DM1.ProcKomPopolniVodomeriSostojba.ExecProc();
         cxgrid1.Visible := false;
         if cxCheckBoxZaCitackaKniga.Checked = true then
         begin

          dm1.TblKomVodomeriSostojba.close;
          dm1.TblKomVodomeriSostojba.ParamByName('REON_ID').AsString:=cxLookupCBReoni.Text;
          dm1.TblKomVodomeriSostojba.ParamByName('CITACKA_KNIGA').AsString:=cxLookupCBCitackaKniga.Text;
          dm1.TblKomVodomeriSostojba.ParamByName('GODINA').AsString:=cxComboBoxGodini.Text;
          dm1.TblKomVodomeriSostojba.ParamByName('MESEC').AsString:=cxComboBoxMesec.Text;
         end
         else
         begin
              DM1.TblKomVodomeriSostojba.SelectSQL.Text := DM1.selectVodomeriSostojbaPoReonCitackaKniga+
                                                           ' AND KL.REON_ID='+VarToStr(cxLookupCBReoni.EditValue)+
                                                           ' AND KS.GODINA='+VarToStr(cxComboBoxGodini.EditValue)+
                                                           ' AND KS.MESEC='+VarToStr(cxComboBoxMesec.EditValue)+
                                                           ' ORDER BY KL.CITACKA_KNIGA,1';
         end;
         DM1.TblKomVodomeriSostojba.Open;
         frmProveriSostojba := TfrmProveriSostojba.Create(Self);
         frmProveriSostojba.ShowModal;
         frmProveriSostojba.Free;

         dm1.TblKomVodomeriSostojba.Close;
         cxGrid1.Visible := true;
    end;


end;

procedure TFrmVodomeriSostojba.aVratenTekstExecute(Sender: TObject);
 var line, lokacija, vodomer, sostojba, vod, mesec, citacka_kniga, link_slika, zabeleska,stara_sostojba, st_sostojba:string;
     i,j, l,s,v:integer;  root,node, node1 : IXMLNode;
     ms:TMemoryStream; gif:TjpegImage;
     StrStream: TStringStream; stream: TBytesStream;
     sli,slika:WideString; imgStm : TStream;
     Bitmap: TJPEGImage;
  //   base64Stream : TIdReadFileExclusiveStream;
  //   pngStream : TIdFileCreateStream;
  //   MIMEDecoder : TidDecoderMIME;
  //   sData: string;
    // s: string;
    sImageStr : String;
    streamImage : TFileStream;
    MIMEDecoder : TidDecoderMIME;
    sData : String;
    zapis : TextFile;
    cel_zapis : WideString;
    ima_slika:string;
begin
    try
      if not dm1.IsInternetConnected then
    //  IdIcmpClient1.Ping ('www.google.com');

    Except on E:Exception do
//      if not ima then
      begin
         ShowMessage('���� �������� ��������!');
        Abort;
      end


    end;

//    begin
    //��������� �� �� �������� ������� , ����� � ������
    if length(cxComboBoxMesec.Text) = 1 then
      mesec := '0'+cxComboBoxMesec.Text
    else mesec := cxComboBoxMesec.Text;

    // ������ ������
    if servis = '1' then
    begin
//     ZemiTextFile('http://ws.codex.mk:170/eInkasator.svc/getXMLeInkasator/'+firma+'/'+mesec+'/'+cxComboBoxGodini.Text+'/'+cxLookupCBReoni.Text+'/'+cxLookupCBCitackaKniga.Text);
     ZemiTextFile(link_prezemi+'/'+firma+'/'+mesec+'/'+cxComboBoxGodini.Text+'/'+cxLookupCBReoni.Text+'/'+cxLookupCBCitackaKniga.Text);
     cxMemo1.Text := vraten_text;
     XMLServis.XML.Clear;
     XMLServis.XML.Add(vraten_text);
     XMLServis.Active:=true;
   try
     root:=XMLServis.DocumentElement;
    // if vraten_text<>'<getXMLeInkasator></getXMLeInkasator>' then

     //node := root.ChildNodes[0];
     //ShowMessage(inttostr(node.ChildNodes.Count));

     for i := 0 to root.ChildNodes.Count - 1 do
     begin
           node := root.ChildNodes[i];

           node1 := node.ChildNodes['ID_LOKACIJA'];
           lokacija := node1.Text;
           node1 := node.ChildNodes['STARA_SOSTOJBA'];
           st_sostojba := node1.text;
           node1 := node.ChildNodes['NOVA_SOSTOJBA'];
           sostojba := node1.text;
           node1 := node.ChildNodes['BROJ_VODOMER'];
           vodomer := node1.text;
           node1 := node.ChildNodes['SLIKA'];
           link_slika := node1.text;
           node1 := node.ChildNodes['ZABELESKA'];
           zabeleska := node1.text;

           if (lokacija<>'') and (vodomer<>'') then
             begin
                   qProveriSostojba.Close;
                   qProveriSostojba.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
                   qProveriSostojba.ParamByName('godina').AsString := cxComboBoxGodini.Text;
                   qProveriSostojba.ParamByName('lokacija').AsString := lokacija;
                   qProveriSostojba.ParamByName('vodomer').AsString := vodomer;
                   qProveriSostojba.ExecQuery;

               if (StrToInt(sostojba))-(qProveriSostojba.FldByName['stara_sostojba'].AsInteger) < 0 then
               begin
                 sostojba := qProveriSostojba.FldByName['stara_sostojba'].AsString;
               end;


               if (vodomer <> '') and (vodomer[1] <>'0') and  (lokacija <> '') and (lokacija <> '0') and (sostojba <> '') and (sostojba[1] <> '0') then
                begin
                     //�� �� ������� ���� �� �������� �� VODOMERI SOSTOJBA


                      qUpdateVS.Close;
                      qUpdateVS.ParamByName('LOKACIIN_ID').AsString := lokacija;
                      qUpdateVS.ParamByName('VODOMERI_ID').AsString := vodomer;
                      qUpdateVS.ParamByName('MESEC').AsString := cxComboBoxMesec.Text;
                      qUpdateVS.ParamByName('GODINA').AsString := cxComboBoxGodini.Text;
                      qUpdateVS.ParamByName('STARA_SOSTOJBA').AsString := st_sostojba;
                      qUpdateVS.ParamByName('NOVA_SOSTOJBA').AsString := sostojba;
                      qUpdateVS.ParamByName('LINK_SLIKA').AsString := link_slika;
                      qUpdateVS.ParamByName('ZABELESKA').AsString := zabeleska;
                      qUpdateVS.ExecQuery;
                end;




//                    ima_slika := '0';
//
//                   if servis = '1' then
//                    begin
//                      try
//                        if not dm1.IsInternetConnected then
//
//                        Except on E:Exception do
//                          begin
//                             ShowMessage('���� �������� ��������!');
//                            Abort;
//                          end
//                        end;
//                      //  dm1.tblPecDukani.First;
//                      //while not dm1.tblPecDukani.Eof do
//                      //begin
//                      //if length(DM1.tblPecDukaniMESEC.AsString) = 1 then
//                       // mesec := '0'+DM1.tblPecDukaniMESEC.AsString
//                      //else mesec := DM1.tblPecDukaniMESEC.AsString;
//
//                     //        if servis = '1' then
//                     //    begin
//                          if link_slika <> '' then
//                              begin
//                                    try
//                                        MS := TMemoryStream.Create;
//                                        GIf := TJPEGImage.Create;
//
//                                        IdHTTP2.get(link_slika,MS);
//                                        Ms.Seek(0,soFromBeginning);
//                                        Gif.LoadFromStream(MS);
//                                        dbimgIMG.Picture.Graphic.Assign(GIf);
//                                        dm1.CompressJpeg(gif);
//                                        dbimgIMG.Picture.Bitmap.SaveToFile('c:\cxCache\MyImage'+lokacija+'.jpg');
//                                   finally
//                                          FreeAndNil(GIF);
//                                          FreeAndNil(MS);;
//                                  end;
//                                   ima_slika := '1';
//                                    qUpdateImaSlika.Close;
//                                    qUpdateImaSlika.ParamByName('ima_slika').Value := 1;
//                                    qUpdateImaSlika.ParamByName('lokacija').AsString := dm1.tblPecDukaniLOKACIIN_ID.AsString;
//                                    qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                                    qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                                    qUpdateImaSlika.ExecQuery;
//                                 end
//                                 else
//                                 begin
//                                  ima_slika := '0';
//                                   qUpdateImaSlika.Close;
//                                   qUpdateImaSlika.ParamByName('ima_slika').Value := 0;
//                                   qUpdateImaSlika.ParamByName('lokacija').AsString := dm1.tblPecDukaniLOKACIIN_ID.AsString;
//                                   qUpdateImaSlika.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                                   qUpdateImaSlika.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                  //                 qUpdateImaSlika.ParamByName('id').Value := DM1.TblPresmetkaGID.Value;
//                                   qUpdateImaSlika.ExecQuery;
//                                  end;
//
//                    end;
                end;
          end;
//                //        end;
//                  //
            finally
             end;
//                  //
       end;


//                      tblVodSost.Close;
//                      tblVodSost.ParamByName('lokacija').AsString := lokacija;
//                      tblVodSost.ParamByName('vodomer').AsString := vodomer;
//                      tblVodSost.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                      tblVodSost.ParamByName('GODINA').AsString := cxComboBoxGodini.Text;
//                      tblVodSost.Open;
//
//                      tblVodSost.Edit;
//
//                      MS := TMemoryStream.Create;
//                      GIf := TJPEGImage.Create;
//
//                      IdHTTP2.get(link_slika,MS);
//                      Ms.Seek(0,soFromBeginning);
//                      Gif.LoadFromStream(MS);
//                     // dbimgSlika.Picture.Assign(GIF);
//                      img1.Picture.Assign(GIF);
//                      dbimgIMG.Picture.Graphic.Assign(gif);
//
//                   //   tblVodSostIMG.
//
//                    //  tblVodSost.ParamByName('lokacija').AsString := lokacija;
//                    //  tblVodSost.ParamByName('vodomer').AsString := vodomer;
//                    //  tblVodSost.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                    //  tblVodSost.ParamByName('GODINA').AsString := cxComboBoxGodini.Text;
//                      tblVodSost.post;


              //  end;
            //  end;
    // end;
  // finally

  //   XMLServis.Active := False;
 //  end;

   // end
    //�������� �� ����-���� - �����
//    else
//  begin
//         //USLOVOT ZA CITACKA KNIGA NE GO FAKJA -------------------------------------------------------------------------------
//          ZemiTextFile(link_zemi+cxComboBoxGodini.Text+'-'+mesec+'&where=REON%20%3D%27'+cxLookupCBReoni.Text//+'%27%20AND%20CITACKA_KNIGA%20%3D%27'+cxLookupCBCitackaKniga.Text
//          +'%27&select=LOKACISKI_BROJ,MESEC_NA_CITANJE,ID,Vodomer,CITANJE');
//
//      //  www.myhtmldatabase.com/cgi-bin/print.cgi?client=kodeks&action0=printExport&exportDate=2013-09&where=REON%20%3D%271%27%20AND%20CITACKA_KNIGA%3D%271%27&select=CITACKA_KNIGA,SIFRA_NA_KORISNIK,MESEC_NA_CITANJE,ID,CITANJE
//      //    Showmessage(link_zemi+cxComboBoxGodini.Text+'-'+mesec+'&where=REON%20%3D%27'+cxLookupCBReoni.Text+'%27%20AND%20CITACKA_KNIGA%20%3D%27'+cxLookupCBCitackaKniga.Text+'%27&select=SIFRA_NA_KORISNIK,MESEC_NA_CITANJE,ID,CITANJE');
//             // ZemiTextFile('http://www.myhtmldatabase.com/cgi-bin/print.cgi?client=kodeks&action0=printExport&exportDate=current&where=REON%20%3D%271%27&select=SIFRA_NA_KORISNIK,MESEC_NA_CITANJE,ID,CITANJE');
//          cxMemo1.Text := vraten_text;
//    for i := 1 to cxMemo1.Lines.Count do
//    begin
//      vodomer := '';
//      line := cxmemo1.Lines[i];
//
//      if Pos('|',line) <> 0 then
//
//      //��� ���� �����, ���� ������� � �� �� ��� �� ������ �����
//       if Pos('-',line) <> 0 then
//        begin
//           //������� --------------------------------------
//         lokacija := Trim(copy(line, 1, Pos('-',line)-5));
//          //������� ����� ---------------------------------
//         //  citacka_kniga := Trim(copy(line,Pos('/',line)+1,Pos('*',line)-length(lokacija)-3));
//           //���� ������� �� ������� ---------------------
//          // sostojba :=Trim(ReverseString( Copy( ReverseString(line), 1, pos(',', ReverseString(line))-4)));
//           sostojba :=Trim(ReverseString( Copy( ReverseString(line), 1, pos(#9, ReverseString(line))-1)));
//          // if sostojba in ['015','030','045','060'] then
//           stara_sostojba := '';
//           if sostojba = '015'then
//                 begin
//                   stara_sostojba := '0';
//                   sostojba := '15';
//                  end;
//           if sostojba = '030'then
//                 begin
//                   stara_sostojba := '0';
//                   sostojba := '30';
//                  end;
//           if sostojba = '045'then
//                 begin
//                   stara_sostojba := '0';
//                   sostojba := '45';
//                  end;
//           if sostojba = '060'then
//                 begin
//                   stara_sostojba := '0';
//                   sostojba := '60';
//                  end;
//          // end;
//
//           if sostojba = '' then sostojba := '0';
//
//           //��� �� ������� ------------------------------
//           vodomer := Trim(ReverseString( Copy( ReverseString(line), Pos(#9, ReverseString(line))+1, length(copy(line,pos('|', line)+1, length(line)))-length(sostojba))));
//           vodomer := trim(ReverseString(copy((reversestring(vodomer)),1,pos(#9,reversestring(vodomer)))));
////            length(line)-(length(line)-length(sostojba)))));
//
////           vod := Trim(copy( line, pos('|', line)+1, length(copy(line,pos('|', line)+1, length(line)))-length(sostojba)));
////           //�� �� ������ ��������� �� ��� �� ��������� --
////           for j := 1 to length(vod) do
////             begin
////               if vod[j] <> ',' then
////                  vodomer := vodomer + vod[j];
////             end;
//
//
//                // ShowMessage(lokacija+' \ '+sostojba+' \ '+vodomer);
//              // if True then
//               end;
//               if (lokacija<>'') and (vodomer<>'') then
//               begin
//                   qProveriSostojba.Close;
//                   qProveriSostojba.ParamByName('mesec').AsString := cxComboBoxMesec.Text;
//                   qProveriSostojba.ParamByName('godina').AsString := cxComboBoxGodini.Text;
//                   qProveriSostojba.ParamByName('lokacija').AsString := lokacija;
//                   qProveriSostojba.ParamByName('vodomer').AsString := vodomer;
//                   qProveriSostojba.ExecQuery;
//
//               if (StrToInt(sostojba))-(qProveriSostojba.FldByName['stara_sostojba'].AsInteger) < 0 then
//               begin
//                 sostojba := qProveriSostojba.FldByName['stara_sostojba'].AsString;
//               end;
//
//
//               if (vodomer <> '') and (vodomer[1] <>'0') and  (lokacija <> '') and (lokacija <> '0') and (sostojba <> '') and (sostojba[1] <> '0') then
//                begin
//                 if stara_sostojba = '' then
//                 begin
//                     //�� �� ������� ���� �� �������� �� VODOMERI SOSTOJBA
//                      qUpdateVS.Close;
//                      qUpdateVS.ParamByName('LOKACIIN_ID').AsString := lokacija;
//                      qUpdateVS.ParamByName('VODOMERI_ID').AsString := vodomer;
//                      qUpdateVS.ParamByName('MESEC').AsString := cxComboBoxMesec.Text;
//                      qUpdateVS.ParamByName('GODINA').AsString := cxComboBoxGodini.Text;
//                      qUpdateVS.ParamByName('NOVA_SOSTOJBA').AsString := sostojba;
//                      qUpdateVS.ParamByName('LINK_SLIKA').AsString := link_slika;
//                      qUpdateVS.ParamByName('ZABELESKA').AsString := zabeleska;
//
//                      //   qUpdateVS.ParamByName('STARA_SOSTOJBA').AsString := stara_sostojba;
//                      qUpdateVS.ExecQuery;
//                 end
//                 else
//                 begin
//                    //�� �� ������� ���� �� �������� �� VODOMERI SOSTOJBA
//                      qUpdateVSSS.Close;
//                      qUpdateVSSS.ParamByName('LOKACIIN_ID').AsString := lokacija;
//                      qUpdateVSSS.ParamByName('VODOMERI_ID').AsString := vodomer;
//                      qUpdateVSSS.ParamByName('MESEC').AsString := cxComboBoxMesec.Text;
//                      qUpdateVSSS.ParamByName('GODINA').AsString := cxComboBoxGodini.Text;
//                      qUpdateVSSS.ParamByName('NOVA_SOSTOJBA').AsString := sostojba;
//                      qUpdateVSSS.ParamByName('LINK_SLIKA').AsString := link_slika;
//                      qUpdateVSSS.ParamByName('ZABELESKA').AsString := zabeleska;
//                     // if stara_sostojba <> '' then
//                      qUpdateVSSS.ParamByName('STARA_SOSTOJBA').AsString := stara_sostojba;
//                      qUpdateVSSS.ExecQuery;
//                 end;
//                end;
//              end;
//      //
//           end;
 //  end;
 //   end;
 //    end;

  //          end;
 //end;

end;



procedure TFrmVodomeriSostojba.aZapisiPodatociExecute(Sender: TObject);
begin
    zapisiPodatoci();
end;

procedure TFrmVodomeriSostojba.cxBtnPartneriClick(Sender: TObject);
var  thr : THandle;
     thrID : DWORD;
     tt : TThreadDataSet;
     sos:Integer;
    // vnes : Boolean;
begin
sos := 0;
if sos_int = '1' then
   begin
    ima := true;
    try
     if not dm1.IsInternetConnected then
     // IdIcmpClient1.Ping('www.google.com');

    Except on E:Exception do
//      if not ima then
      begin
         ShowMessage('���� �������� ��������, �� ���� ���������� �� �� �������� ���������!');
//        Abort;
       ima := false
      end

    end;


     frmDaNe := TfrmDaNe.Create(self,'���� �� ������� �� ��������','���� �� �� ��������� ���������?',1);
 //   if (frmDaNe.ShowModal <> mrYes) then


     if ( frmDaNe.ShowModal = mrYes) then
     begin
     sos:=1;
     if ( podatociOK)
      then
     end;

   end;
  // else
    //begin
    if (podatociOK) //and (frmDaNe.ShowModal = mrYes)
    then

    begin

         poedinecno:=False;

         DM1.ProcKomPopolniVodomeriSostojba.ParamByName('REON_ID').AsVariant := cxLookupCBReoni.EditValue;
         if cxCheckBoxZaCitackaKniga.Checked = true then
         begin
              DM1.ProcKomPopolniVodomeriSostojba.ParamByName('CITACKA_KNIGA').AsVariant := cxLookupCBCitackaKniga.Text;
         end
         else
         begin
              DM1.ProcKomPopolniVodomeriSostojba.ParamByName('CITACKA_KNIGA').AsVariant := -1;
         end;

         DM1.ProcKomPopolniVodomeriSostojba.ParamByName('GODINA').AsVariant := cxComboBoxGodini.EditValue;
         DM1.ProcKomPopolniVodomeriSostojba.ParamByName('MESEC').AsVariant := cxComboBoxMesec.EditValue;

         DM1.ProcKomPopolniVodomeriSostojba.Prepare();
         DM1.ProcKomPopolniVodomeriSostojba.ExecProc();

        if (sos_int = '1') and (sos=1) then
        begin
           //�� �������� ���� ��� ������ �� ��� ��������� �������� ����������
         qProveriVnes.Close;
         qProveriVnes.ParamByName('mesec').Value := cxComboBoxMesec.EditValue;
         qProveriVnes.ParamByName('godina').Value := cxComboBoxGodini.EditValue;
         qProveriVnes.ParamByName('reon_id').Value := cxLookupCBReoni.EditValue;
         qProveriVnes.ParamByName('citacka_kniga').Value := cxLookupCBCitackaKniga.EditValue;
         qProveriVnes.ExecQuery;
         if ima and qProveriVnes.FldByName['avtomatski_vnes'].IsNull then
            aVratenTekst.Execute
         else
         begin
           //�� �� �������� �������� � �� ��������� ����

         end;
        end;
        // ShowMessage('������ ���� ���������� ������ �� ��������� - '+DM1.ProcKomPopolniVodomeriSostojba.ParamByName('INSERTED').AsString);

         if DM1.TblKomVodomeriSostojba.Active then
         begin
              DM1.TblKomVodomeriSostojba.Close();
         end;

         if cxCheckBoxZaCitackaKniga.Checked = true then
         begin
//              DM1.TblKomVodomeriSostojba.SelectSQL.Text := DM1.selectVodomeriSostojbaPoReonCitackaKniga+
//                                                           ' AND KL.REON_ID='+VarToStr(cxLookupCBReoni.EditValue)+
//                                                           ' AND KL.CITACKA_KNIGA='+VarToStr(cxLookupCBCitackaKniga.EditValue)+
//                                                           ' AND KS.GODINA='+VarToStr(cxComboBoxGodini.EditValue)+
//                                                           ' AND KS.MESEC='+VarToStr(cxComboBoxMesec.EditValue)+
//                                                           ' ORDER BY kl.citacka_kniga_rb,KL.RB2';
          dm1.TblKomVodomeriSostojba.close;
          dm1.TblKomVodomeriSostojba.ParamByName('REON_ID').AsString:=cxLookupCBReoni.Text;
          dm1.TblKomVodomeriSostojba.ParamByName('CITACKA_KNIGA').AsString:=cxLookupCBCitackaKniga.Text;
          dm1.TblKomVodomeriSostojba.ParamByName('GODINA').AsString:=cxComboBoxGodini.Text;
          dm1.TblKomVodomeriSostojba.ParamByName('MESEC').AsString:=cxComboBoxMesec.Text;
         end
         else
         begin
              DM1.TblKomVodomeriSostojba.SelectSQL.Text := DM1.selectVodomeriSostojbaPoReonCitackaKniga+
                                                           ' AND KL.REON_ID='+VarToStr(cxLookupCBReoni.EditValue)+
                                                           ' AND KS.GODINA='+VarToStr(cxComboBoxGodini.EditValue)+
                                                           ' AND KS.MESEC='+VarToStr(cxComboBoxMesec.EditValue)+
                                                           ' ORDER BY KL.CITACKA_KNIGA,1';
         end;

         DM1.TblKomVodomeriSostojba.Open;
         //tt  := TThreadDataSet.Create(False);
         //tt.Open(DM1.TblKomVodomeriSostojba);

        // od mene komentiran
      //   getFirstNotSetSostojba();

         {
         ShowMessage('pocna threadot');
         thr := CreateThread(nil, 0, @threadProcedure, nil, 0, thrID);
         if (thr = 0) then
            ShowMessage('Thread not created')
         else
         begin
            ShowMessage('zavrsi threadot');
            //Abort();
         end;
         }

     //    aVratenTekst.Execute;

         enablePanelGore(false);
         panelDolen.Enabled := true;
         cxGrid1.SetFocus();
    end;
   // end;
end;

procedure TFrmVodomeriSostojba.cxCheckBoxZaCitackaKnigaPropertiesChange(
  Sender: TObject);
begin
    if cxCheckBoxZaCitackaKniga.Checked = false then
    begin
        DM1.TblCitackiKnigiPoReon.Close();
        cxLookupCBCitackaKniga.Enabled := false;
    end
    else
    begin
        setCitackaKnigaLista(cxLookupCBReoni.EditValue);
        cxLookupCBCitackaKniga.Enabled := true;
    end;

end;

procedure TFrmVodomeriSostojba.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
    if (AFocusedRecord <> nil) then
    begin
        if cxGrid1DBTableView1.DataController.DataSet.RecNo > 1 then
        begin
            setPredhodenSostojba(cxGrid1DBTableView1.DataController.DataSet.RecNo-2);
        end
        else
        begin
            clearPredhodenSostojba();
        end;
    end
end;

procedure TFrmVodomeriSostojba.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var  AStatus:String;
begin
  AStatus:='';
  if (ARecord<>nil) and (AItem<>nil) then
    if not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1Column2.Index])
     then
     begin
       AStatus:=VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1Column2.Index], varString);
     end;

    if AStatus = '��' then
    begin
        AStyle:=DM1.cxStyleNeAktivni;
    end
    else
    begin
        AStyle:=DM1.cxStyleAktivni;
    end;
end;

procedure TFrmVodomeriSostojba.cxLookupCBReoniPropertiesChange(Sender: TObject);
begin
//
end;

procedure TFrmVodomeriSostojba.cxLookupCBReoniPropertiesEditValueChanged(
  Sender: TObject);
begin
    //ShowMessage(VarToStr(cxLookupCBReoni.EditValue));
    if Length(VarToStr(cxLookupCBReoni.EditValue)) > 0 then
    begin
        setCitackaKnigaLista(cxLookupCBReoni.EditValue);
        cxLookupCBCitackaKniga.ItemIndex := -1;
        //ShowMessage(VarToStr(cxLookupCBReoni.EditValue));
    end;
end;

procedure TFrmVodomeriSostojba.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if DM1.TblKomVodomeriSostojba.State in [dsEdit, dsInsert] then
    begin
        DM1.TblKomVodomeriSostojba.Cancel();
    end;
   // Action := caFree;
   //IdHTTP1.Destroy;
   //IdHTTP2.Destroy;
end;

procedure TFrmVodomeriSostojba.FormShow(Sender: TObject);
begin
    ima := false;
    DM1.TblKomVodomeriSostojba.Close();

    enableControlsSostojba(false);
    gridFilled := false;
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)));
    cxComboBoxGodini.Properties.Items.Add(IntToStr(YearOf(Now)-1));
end;

procedure TFrmVodomeriSostojba.onEnterAll(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TFrmVodomeriSostojba.onExitAll(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
      if (TEdit(sender).Name='cxControlSTARA_SOSTOJBA') or (TEdit(sender).Name='cxControlNOVA_SOSTOJBA') then
      BEGIN
        if cxControlSTARA_SOSTOJBA.Text<'0' then
            begin
              ActiveControl:=cxControlSTARA_SOSTOJBA;
              ShowMessage('�� ���� �� �� ����� ��������� ��������!');
              Abort;

            end
        else
        if cxControlNOVA_SOSTOJBA.Text<'0' then
            begin
              ActiveControl:=cxControlNOVA_SOSTOJBA;
              ShowMessage('�� ���� �� �� ����� ��������� ��������!');
              Abort;
            end;
//       else
//        if (StrToInt(cxControlNOVA_SOSTOJBA.Text)-StrToInt(cxControlSTARA_SOSTOJBA.Text))>StrToInt(gorna_granica) then
//            begin
//              ActiveControl:=cxControlNOVA_SOSTOJBA;
//              ShowMessage('��������� �� ������ �������, ��������� � �������� �� '+gorna_granica+'!');
//              Abort;
//            end;
       END;

end;

procedure TFrmVodomeriSostojba.onKeyDownAll(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            if TEdit(sender).Name='cxControlNOVA_SOSTOJBA' then
            begin
                if (DM1.TblKomVodomeriSostojba.RecNo = DM1.TblKomVodomeriSostojba.RecordCount) then
                begin
                    DM1.TblKomVodomeriSostojba.Post();
                    enableControlsSostojba(false);
                    cxGrid1.Enabled := true;
                    cxGrid1.SetFocus();
                end
                else
                begin
                    cxGrid1DBTableView1Column1.EditValue:=strtoint(cxControlNOVA_SOSTOJBA.Text)-strtoint(cxControlSTARA_SOSTOJBA.Text);
                    //��� ����� � ������ �� 06.12.2015, �� ���, ��� ������� ���� ������� � �������� (���. ���� = 1) �� ����
                    //��� � ������ �� �������� (���.���� = 0), �� �� ������� ���. ���� �� 2.
                    //����� ����� ������ �����, �� ����� �� �� ������������ ����������
                   // if cxGrid1DBTableView1AVTOMATSKI_VNES.EditValue = 1 then

                if (((StrToInt(cxControlNOVA_SOSTOJBA.Text)-StrToInt(cxControlSTARA_SOSTOJBA.Text))>StrToInt(gorna_granica1))
                and (dm1.TblKomVodomeriSostojbaTIP_PARTNER.Value in [5,6])
                and ((StrToInt(cxControlNOVA_SOSTOJBA.Text)-StrToInt(cxControlSTARA_SOSTOJBA.Text))<=StrToInt(gorna_granica5))) then
                    begin
                         frmDaNe := TfrmDaNe.Create(self,'������� �������','��������� ���� - ����� ������� � �������� �� '+gorna_granica1+'! ���� ������������ ��������?',1);
                         if (frmDaNe.ShowModal <> mrYes) then
                           begin
                            //  ActiveControl:=cxControlNOVA_SOSTOJBA;
                              cxControlNOVA_SOSTOJBA.SetFocus;
                              Abort;
                           end;
                    end;

                if ((dm1.TblKomVodomeriSostojbaTIP_PARTNER.Value in [5,6])
                and ((StrToInt(cxControlNOVA_SOSTOJBA.Text)-StrToInt(cxControlSTARA_SOSTOJBA.Text))>StrToInt(gorna_granica5))) then
                    begin
                              ShowMessage('��������� ���� - ����� ������� � �������� �� '+gorna_granica5+'!');
                             // ActiveControl:=cxControlNOVA_SOSTOJBA;
                              cxControlNOVA_SOSTOJBA.SetFocus;
                              Abort;
                    end;

                    cxGrid1DBTableView1AVTOMATSKI_VNES.EditValue := 2;

                    DM1.TblKomVodomeriSostojba.Next();
                    aAzuriraj.Execute();
                end;
            end
            else
            begin
                PostMessage(Handle,WM_NextDlgCtl,0,0)
            end;
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
            Key := 0;
        end;
    end;
end;

procedure TFrmVodomeriSostojba.onKeyDownLookUpAll(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    case key of
        VK_RETURN:
        begin
            PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
        VK_UP:
        begin
            if (Sender.ClassName = 'TcxComboBox') then
            begin

                if TcxComboBox(Sender).DroppedDown = false then
                begin
                    PostMessage(Handle, WM_NEXTDLGCTL,1,0);
                    Key := 0;
                end;
            end
            else
            begin
                PostMessage(Handle, WM_NEXTDLGCTL,1,0);
                Key := 0;
            end;
        end;
    end;
end;

procedure TFrmVodomeriSostojba.setCitackaKnigaLista(reon:String);
begin
    DM1.TblCitackiKnigiPoReon.Close();
    DM1.TblCitackiKnigiPoReon.ParamByName('REON_ID').AsString := reon;
    DM1.TblCitackiKnigiPoReon.Open();
end;

function TFrmVodomeriSostojba.podatociOk():boolean;
var ret:boolean;
begin
    ret := false;
    if (cxLookupCBReoni.Text = '') then
    begin
        cxLookupCBReoni.SetFocus();
        ShowMessage('������ �� �������� ����!');
    end
    else if (cxComboBoxMesec.Text = '') then
    begin
        cxComboBoxMesec.SetFocus();
        ShowMessage('������ �� �������� �����!');
    end
    else if (cxComboBoxGodini.Text = '') then
    begin
        cxComboBoxGodini.SetFocus();
        ShowMessage('������ �� �������� ������!');
    end
    else
    begin
        ret := true;
    end;

    podatociOK := ret;
end;


procedure TFrmVodomeriSostojba.zapisiPodatoci();
begin
    DM1.TransProcs.CommitRetaining();
    gridFilled := false;
end;

procedure TFrmVodomeriSostojba.noviPodatoci();
begin
    DM1.TblKomVodomeriSostojba.Close();
    DM1.TblCitackiKnigiPoReon.Close();

    clearPredhodenSostojba();

    cxComboBoxMesec.ItemIndex := -1;
    cxComboBoxGodini.ItemIndex := -1;
    cxLookupCBCitackaKniga.ItemIndex := -1;
    cxLookupCBReoni.ItemIndex := 0;
    cxLookupCBReoni.Clear;
    cxLookupCBReoni.SetFocus();
   // Abort;
end;


procedure TFrmVodomeriSostojba.enablePanelGore(enable:boolean);
begin
    panelGoren.Enabled := enable;
    gridFilled :=not enable;

    panelDolen.Enabled := not enable;
end;


procedure TFrmVodomeriSostojba.enableControlsSostojba(enable:Boolean);
begin
    //cxControlSTARA_SOSTOJBA.Enabled := enable;
    //cxControlNAZIV.Enabled := enable;


    cxControlNOVA_SOSTOJBA.Enabled := enable;
    cxControlStara_SOSTOJBA.Enabled := enable;

//    cxControlNAZIV.Properties.ReadOnly := not(enable);
//    cxControlADRESA.Properties.ReadOnly := not(enable);
//    cxControlNOVA_SOSTOJBA.Properties.ReadOnly := not(enable);
//    cxControlStara_SOSTOJBA.Properties.ReadOnly := not(enable);
end;

procedure TFrmVodomeriSostojba.callFrmLokacii();
var frm:TFrmLokacija;
begin
    frm := TFrmLokacija.Create(nil,DM1.TblKomVodomeriSostojbaPARTNER_ID.AsString,DM1.TblKomVodomeriSostojbaTIP_PARTNER.AsString,DM1.TblKomVodomeriSostojbaNAZIV.AsString);
    frm.ShowModal();
    frm.Free();
end;


procedure TFrmVodomeriSostojba.getFirstNotSetSostojba();
var vodomer_id,lokaciin_id:String;
begin
    DM1.QGetFirstNotSetSostojba.ParamByName('REON_ID').AsString := cxLookupCBReoni.EditValue;
    DM1.QGetFirstNotSetSostojba.ParamByName('CITACKA_KNIGA').AsString := cxLookupCBCitackaKniga.EditValue;
    DM1.QGetFirstNotSetSostojba.ParamByName('GODINA').AsString := cxComboBoxGodini.EditValue;
    DM1.QGetFirstNotSetSostojba.ParamByName('MESEC').AsString := cxComboBoxMesec.EditValue;

    DM1.QGetFirstNotSetSostojba.ExecQuery();

    vodomer_id := DM1.QGetFirstNotSetSostojba.FieldByName('VODOMERI_ID').AsString;
    lokaciin_id := DM1.QGetFirstNotSetSostojba.FieldByName('LOKACIIN_ID').AsString;

    DM1.TblKomVodomeriSostojba.Locate('LOKACIIN_ID;VODOMERI_ID;GODINA;MESEC',VarArrayOf([lokaciin_id,vodomer_id,VarToStr(cxComboBoxGodini.EditValue),VarToStr(cxComboBoxMesec.EditValue)]),[]);
end;


procedure TFrmVodomeriSostojba.setPredhodenSostojba(reden_broj:integer);
begin
    cxTxtNazivPredhoden.Text := VarToStr(cxGrid1DBTableView1.DataController.Values[reden_broj,1]);
    cxTxtRazlikaPredhoden.Text := VarToStr(cxGrid1DBTableView1.DataController.Values[reden_broj,7]);
    cxTxtStaraPredhoden.Text := VarToStr(cxGrid1DBTableView1.DataController.Values[reden_broj,5]);
    cxTxtNovaPredhoden.Text := VarToStr(cxGrid1DBTableView1.DataController.Values[reden_broj,6]);

end;

procedure TFrmVodomeriSostojba.clearPredhodenSostojba;
begin
    cxTxtNazivPredhoden.Text := '';
    cxTxtRazlikaPredhoden.Text := '';
    cxTxtStaraPredhoden.Text := '';
    cxTxtNovaPredhoden.Text := '';
end;

procedure TFrmVodomeriSostojba.IdIcmpClient1Reply(ASender: TComponent;
  const AReplyStatus: TReplyStatus);
begin
      ima :=(AReplyStatus.BytesReceived>0)

//end;
end;

function TfrmVodomeriSostojba.ImaBeleska : boolean;
var rez : boolean;
begin
    rez := false;
    dm1.qImaBeleska.Close;
    dm1.qImaBeleska.ParamByName('lokacija').AsInteger := DM1.TblKomVodomeriSostojbaLOKACIIN_ID.AsInteger;
    dm1.qImaBeleska.ParamByName('mesec').AsInteger := DM1.TblKomVodomeriSostojbaMESEC.AsInteger;
    dm1.qImaBeleska.ParamByName('godina').AsInteger := DM1.TblKomVodomeriSostojbaGODINA.AsInteger;
    dm1.qImaBeleska.ExecQuery;

    if dm1.qImaBeleska.FldByName['ImaBeleska'].IsNull then
      rez := false
    else
      rez := true;
    ImaBeleska := rez;
end;

function TfrmVodomeriSostojba.GetURLAsString(aURL: string): string;
var
  lHTTP: TIdHTTP;
begin
  lHTTP := TIdHTTP.Create(nil);
  try
    Result := lHTTP.Get(aURL);
  finally
    FreeAndNil(lHTTP);
  end;
end;

function TfrmVodomeriSostojba.PostURLAsString(aURL: string): string;
var
  lHTTP: TIdHTTP;
begin
//
//  lHTTP := TIdHTTP.Create(nil);
//  try
//    Result := lHTTP.Post(aURL,);
//  finally
//    FreeAndNil(lHTTP);
//  end;
end;

procedure TfrmVodomeriSostojba.ZemiTextFile(url: String);
//var vraten_text : string;
begin
//-----------------------------------------------------------------------
// ��������� �� �������� �� URL, �������� ��������� ������ ���������,
// �� ������ �����, ����, ������ � �����
//-----------------------------------------------------------------------
  vraten_text := GetURLAsString(url);
//  ShowMessage(vraten_text);
end;

function TfrmVodomeriSostojba.Base64ToBitmap(const S: string): TJPEGImage;
var
SS: TStringStream;
V: string;
begin
V := Decode(S);
SS := TStringStream.Create(V);
try
Result := TJPEGImage.Create;
Result.LoadFromStream(SS);
finally
SS.Free;
end;
end;

function TfrmVodomeriSostojba.Decode(const Input: AnsiString): string;
var
  bytes: TBytes;
  utf8: UTF8String;
begin
  bytes := Soap.EncdDecd.DecodeBase64(Input);
  SetLength(utf8, Length(bytes));
  Move(Pointer(bytes)^, Pointer(utf8)^, Length(bytes));
  Result := string(utf8);
end;

end.
