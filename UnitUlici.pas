unit UnitUlici;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, Menus, cxLookAndFeelPainters, ActnList,
  cxGridCustomPopupMenu, cxGridPopupMenu, ComCtrls, StdCtrls, cxButtons,
  cxContainer, cxTextEdit, cxDBEdit, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxLookAndFeels, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxBar, dxPSCore,
 // dxPScxCommon, dxPScxGrid6Lnk, cxBarEditItem, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinValentine, dxSkinXmas2008Blue,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxPScxCommon, cxBarEditItem,
  dxStatusBar, dxRibbonStatusBar, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint;

type
  TFrmUlici = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1REON_ID: TcxGridDBColumn;
    lblReon: TLabel;
    cxControlREON: TcxDBTextEdit;
    cxLookUpREON: TcxDBLookupComboBox;
    lblNaziv: TLabel;
    cxControlNAZIV: TcxDBTextEdit;
    procedure OnKeyDownAllLookUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure setDefaultReon(defaultReon:integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    var defaultReon:integer;
  public
    { Public declarations }

  end;

var
  FrmUlici: TFrmUlici;

implementation

uses UnitDM1, dmMaticni;

{$R *.dfm}

procedure TFrmUlici.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    case Key of
        VK_F5:
        begin
          if defaultReon <> 0 then
          begin
            DM1. TblKomUliciREON_ID.AsInteger := defaultReon;
          end;
        end;
    end;
end;

procedure TFrmUlici.FormShow(Sender: TObject);
begin
  inherited;
    if ( defaultReon = 0 ) then
    begin
        DM1.openTblUliciAll();
    end;

end;

procedure TFrmUlici.OnKeyDownAllLookUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    case key of
        VK_RETURN:
        begin
          PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end;
    end;
end;

procedure TFrmUlici.setDefaultReon(defaultReon:integer);
begin
    self.defaultReon := defaultReon;
end;

end.
